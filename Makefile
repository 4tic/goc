.PHONY: help
help:
	@echo "make help	Show this help message"
	@echo "make docker      Run maven wrapper to build war and build docker image and save in /tmp"
	@echo "make clean       Clean project build directory"
	@echo "make test        Run the app's test"

.PHONY: docker
docker: 
	@docker build -t $(DOCKER_IMAGE_NAME) .
	@echo "Generated docker image at /tmp/"
	@docker save -o /tmp/agora.tar $(DOCKER_IMAGE_NAME)

.PHONY: build
build: 
	@mvn clean install

.PHONY: clean
clean: 
	@mvn clean

.PHONY: test
test: 
	@mvn verify

DOCKER_IMAGE_NAME = cuatroochenta.com/agora
DOCKER_TAG = latest
GOC_PROFILE = dev
