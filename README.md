# Ágora

Gestión de órganos colegiados (Ágora) es una aplicación pensada para convocar y gestionar las reuniones de los órganos colegiados de las 
universidades. Algunos ejemplos de usuarios de esta aplicación son el Claustro, Consejo de Gobierno, Consejo de Dirección, Consejo Social, 
Consejo de Estudiantes, Juntas de Centro, entre otras. Esta herramienta cubre todo el proceso de organización de una reunión, desde su
convocatoria hasta el cierre del acta. De esta forma, la aplicación facilita los trámites y posibilita, desde una misma plataforma, 
convocar una reunión, añadir puntos del orden del día, adjuntar documentación y enviar la convocatoria a los miembros de un órgano
colegiado para que reciban la información de la reunión y confirmen su asistencia. Adicionalmente, una vez realizada la reunión, permite
incluir el resultado de los acuerdos y deliberaciones y, seguidamente, cerrar el acta, validarla, firmarla digitalmente y archivarla para
que quede guardada en un histórico. 

Para más detalles, puedes consultar el manual de uso [en esta dirección](docs/manual/manual.md).

# Arquitectura y tecnologías utilizadas

Como resume el documento correspondiente al Marco tecnológico para el desarrollo de aplicaciones en la Universitat Jaume I, la 
aplicación del GOC se ha desarrollado en Java 8 siguiendo el siguiente esquema de separación de responsabilidades:

- **Interfaz de usuario**. Desarrollada en ExtJS 6, un framework rico de desarrollo de aplicaciones en JavaScript.
- **Aplicación web**. Conjunto de servicios REST que intercambian datos en formato JSON con la aplicación cliente. Desarrollada con Jersey, Spring y JPA, permite implementar la lógica de negocio correspondiente a los distintos procesos analizados en el documento funcional del proyecto.
- **Backend de datos**. Aunque se ha utilizado Oracle para la persistencia de datos, gracias a JPA sería factible utilizar cualquier sistema de gestión de base de datos relacional.

## Requisitos

Para compilar y ejecutar el proyecto necesitaremos:

- **Cliente de Git**. En Linux/Mac hay habitualmente paquetes de git para instalar, pero si usamos Windows, una buena opción puede ser TortoiseGit.
- **Java JDK 8**. Ojo!! Necesitamos JDK para compilar, no JRE. Disponible aquí.
- **Apache Maven**. Necesario para la compilación y empaquetado del código. Disponible aquí.
- **Apache Tomcat**. Únicamente para el despliegue de pre o de producción. Para desarrollo usaremos jetty desde Maven, por lo que no hace falta nada más.