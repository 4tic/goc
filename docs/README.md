# Propiedades 
## [release-4tic-2.11.0]

En este documento se detallan las propiedades que pueden ser activadas y la configuración para el funcionamiento de la aplicación. El archivo se encuentra en la ruta  `/etc/uji/goc/app.properties`.

## Funcionalidad
### Idioma
- `goc.mainLanguage=[es|ca]` Idioma principal de la aplicación marcado con el [código de lenguaje ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).
- `goc.mainLanguageDescription` Etiqueta mostrada del idioma principal de la aplicación como por ejemplo 'Español, Castellano, ESP'.
- `goc.alternativeLanguage=[es|ca| ]` Idioma alternativo de la aplicación marcado con el [código de lenguaje ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes). Si está vacío no se muestra la opción de un idioma alternativo.
- `goc.alternativeLanguageDescription` Etiqueta mostrada del idioma alternativo de la aplicación como por ejemplo 'Catalá, CAT'.

### Diseño y personalización
- `goc.logo=[{url}|{base64}]` Imagen mostrada en la cabecera de la pantalla de gestión.
- `goc.logoPublic=[{url}|{base64}]` Imagen mostrada en la cabecera de la pantalla de publicación de la reuniones.
- `goc.logoDocumentos=[{url}|{base64}]` Imagen mostrada en la cabecera de los documentos.
- `goc.nombreInstitucion={string}` Etiqueta que aparece en las cabeceras de los documentos.
- `goc.ciudadInstitucion={string}` Etiqueta que aparece en el pie de los documentos como "3 de marzo de 2021, Castellón".
- `goc.customCSS={url}` URL al archivo personalizado de estilos [CSS](https://www.w3.org/Style/CSS/specs.en.html) para las pantallas de publicación.
- `goc.privateCustomCSS={url}` URL al archivo personalizado de estilos [CSS](https://www.w3.org/Style/CSS/specs.en.html) para las pantallas de gestión.  
- `goc.tituloDeAplicacion={string}` Permite modificar el título de la aplicación de gestión (no la de publicaciones).

### Opcionalidades
- `goc.showAusentes=[true|false*]` Permite mostrar los miembros que no han confirmado la asistencia a la reunión.
- `goc.reunion.mostrarEmails=[true*|false]` Permite ocultar las direcciones electrónicas de los miembros.
- `goc.acuerdos.mostrarEnlaceCertificados=[true*|false]` Permite ocultar los enclaces a certificados de asistencias mostrados en las publicaciones.
- `goc.reunion.mostrarObservaciones=[true|false*]` Permite mostrar un campo observaciones en el cierre de las reuniones.
- `goc.reunion.mostrarExtraordinaria=[true|false*]` Permite mostrar la opción de marcar como convocatoria extraordinaria en el formulario de la reunión.
- `goc.miembros.mostrarDescripcion=[true|false*]` Permite habilitar un campo descripción para los miembros.
- `goc.invitados.mostrarDescripcion=[true|false*]` Permite habilitar un campo descripción para los invitados.
- `goc.reunion.mostrarConvocante=[true*|false]` Permite ocultar el convocante de la reunión.
- `goc.publicacion.actasPublicas=[true|false*]` Permite mostrar en el buscador de acuerdos de manera pública enlaces a las actas PDF. En esta acta los puntos privados aparecerán como "Puntos privados".
- `goc.menuPersonasIsEnabled=[true|false*]` Permite mostrar una nueva entrada de menú en la pantalla de gestión para el control y edición de personas.
- `goc.email.useAlwaysDefaultSender=[true*|false]` Permite indicar que siempre se utilice la propiedad `defaultSender` como remitente de los correos electrónicos enviados por la aplicación.
- `goc.bloquearRemitenteConvocatoria=[true*|false]` Permite habilitar el cambio de dirección de correo del órgano que aparecerá como dirección en las notificaciones por correo electrónico.
- `goc.notificarActaDisponible=[true|false*]` Permite habilitar la notificación mediante correo electrónico cuando cerramos un acta.

## Configuración

### Base de datos
- `uji.db.driverClass=[oracle.jdbc.OracleDriver|org.postgresql.Driver]` Package referenciando a la clase usada como driver de conexión.
- `uji.db.jdbcUrl={URL}` URL JDBC.
- `uji.db.username={string}` Usuario de base de datos.
- `uji.db.password={string}` Contraseña del usuario de base de datos.
- `uji.db.databaseId=[ORACLE|POSTGRESQL]` Identificador del gestor usado en base de datos.
- `uji.db.dialect=[org.hibernate.dialect.Oracle10gDialect|org.hibernate.dialect.PostgreSQLDialect]` Package referenciando a la clase usada como dialecto de base de datos.
- `uji.db.preferredTestQuery=[select 1 from dual|select 1;]` Consulta periódica ejecutada para la verificación de las conexiones.

### Autenticación
#### SAML
- `goc.saml.keystore.path={string}` Ruta al keystore donde se almacenan los certificados de SAML.
- `goc.saml.keystore.keyName={string}` Key del certificado utilizado.
- `goc.saml.keystore.keyPassword={string}` Contraseña del keystore.
- `goc.saml.idp.path={string}` Rutal al archivo XML del IDP.
- `goc.saml.metadata.username` Atributo especificado en la respuesta SAML donde viene el nombre de usuario.

#### CAS
- `goc.webapp.service.url={URL}` URL del servicio CAS.
- `goc.cas.server.location={URL}` Localización del servicio.
- `goc.cas.logout.success.url={URL}` URL de éxito.
- `goc.cas.login.url={URL}` URL de cierre de sesión.
- `goc.cas.metadata.username={string}` Atributo especificado en la respuesta CAS donde viene el nombre de usuario.

#### OAUTH2
- `goc.oauth.key={string}` Key de la configuración OAUTH.
- `goc.oauth.secret={string}` Secret de la configuración OAUTH.
- `goc.oauth.scope={string}` Scopte de la configuración OAUTH.
- `goc.oauth.baseUrl={URL}` URL del servicio.
- `goc.oauth.logout={URL}` URL de cierre de sesión.

### Envío de correo
- `goc.smtp.enabled=[true*|false]` Permite deshabilitar el envío de correos.
- `uji.smtp.host={URL}` URL del host SMTP.
- `uji.smtp.starttls.enable=[true|false*]` Permite habilitar la configuración TTLS para la comunicación con el SMTP.
- `uji.smtp.auth={string}` Inica el tipo de autenticación del SMTP.
- `uji.smtp.port={number}` Indica el puerto del SMTP.
- `uji.smtp.socketFactory.class=javax.net.ssl.SSLSocketFactory`
- `uji.smtp.username={string}` Indica el usuario del SMTP.
- `uji.smtp.password={string}` Indica la contraseña del usuario del SMTP.
- `uji.smtp.defaultSender={string}` Indica una dirección usada como remitente de los correos electrónicos si se activa `goc.email.useAlwaysDefaultSender` o `goc.bloquearRemitenteConvocatoria`.

### LDAP

- `goc.ldap.host={URL}` URL del servicio LDAP.
- `goc.ldap.port={number}` Puerto del servicio LDAP.
- `goc.ldap.base={URL}` Ruta añadida al host para acceder al servicio LDAP.
- `goc.ldap.username={string}` Usuario del servicio LDAP.
- `goc.ldap.password={string}` Contraseña del usuario del servicio LDAP.
- `goc.ldap.atributo.nombre={string}` Atributo especificado en la respuesta LDAP donde viene el nombre de usuario.
- `goc.ldap.atributo.apellidos={string}` Atributo especificado en la respuesta LDAP donde viene el apellido de usuario.
- `goc.ldap.atributo.nombreCompleto={string}` Atributo especificado en la respuesta LDAP donde viene el nombre completo del usuario.
- `goc.ldap.atributo.email={string}` Atributo especificado en la respuesta LDAP donde viene la dirección electrónica.

- `goc.servicios.busquedaPersonas=[true|false]` Permite mostrar u ocultar el botón para importar personas desde el servicio definido por `goc.servicios.busquedaPersonasURL`
- `goc.servicios.busquedaPersonas.secret={string}` Secret del endpoint al servicio de búsqueda de personas.
- `goc.servicios.busquedaPersonasURL={URL}` URL del endpoint al servicio de búsqueda de personas.

### Otros ajustes

- `goc.baseUrl={string}` URL base de los endpoints de la aplicación.
- `goc.charset=UTF-8` Charset utilizado.
- `goc.publicUrl={URL}` URL de la aplicación.
- `goc.log.path={string}` Ruta donde almacenar los archivos de log.
- `goc.templates.path={string}` Si se define se sobreescriben las plantillas de documentos.
- `goc.templates.html.path={string}` Si se define se sobreescriben las plantillas HTML de las publicaciones.
- `goc.reunion.basePathDocumentacion` Ruta donde se almacerará los archivos adjuntos.
- `goc.staticsUrl={URL}` URL donde se encuentran alojados los recursos estáticos de la aplicación.
- `goc.reunion.enviarBase64AFirma=[true|false*]` Permite enviar el base64 del documento PDF al cerrar el acta al servicio de firma.

## Desarrollo y depuración

### Autenticación
- `uji.deploy.defaultUserName={string}` Etiqueta del usuario conectado.
- `uji.deploy.defaultUserId={id}` Identificador de usuario.

### Entorno
- `goc.debug=[true|false*]` Permite indicar entorno de depuración.