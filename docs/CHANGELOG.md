# Changelog

Todos los cambios significativos en este proyecto estarán documentados en este archivo.

El formato se basa en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
y este proyecto se versiona respetando la especificación de [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [release-4tic-2.11.0] 2021-10-20
### Added
- Se permite la carga de cargos externos.
- Se envían los documentnos de acuerdos codificados al _endpoint_ de firma.

## [release-4tic-2.10.5] 2021-10-29
### Fixed
- Se corrige la visualización de las actas PDF enviadas a firmar con texto HTML.

## [release-4tic-2.10.4] 2021-07-12
### Fixed
- Se corrige la visualización de algunos saltos de línea en las actas PDF que no aparecían.

## [release-4tic-2.10.3] 2021-07-08
### Fixed
- Se muestran los saltos de línea en las actas PDF.

## [release-4tic-2.10.2] 2021-06-25
### Fixed
- Corregidos los enlaces rotos a los acuerdos que se presentan en el acta PDF.

## [release-4tic-2.10.1] 2021-05-17
### Fixed
- Corregida la edición de la descripción de los miembros.

## [release-4tic-2.10.0] 2021-05-13
### Added
- Se audita la autenticación con éxito y fallida en la aplicación.
### Fixed
- Cuando se añade un autorizado o invitado existente en el órgano se abre la fila en modo edición.

## [release-4tic-2.9.1] 2021-05-12
### Fixed
- Ajustado el logo de los documentos al ancho con un alto máximo.

## [release-4tic-2.9.0] 2021-05-04
### Added
- Nivel AAA de accesibilidad en la web de las publicaciones.
### Fixed
- Mejorados aspectos de la interfaz de usuario en web de las publicaciones.

## [release-4tic-2.8.0] - 2021-04-21
### Added
- Se permite la reordenación de miembros de un órgano especificando la posición concreta.
### Fixed
- Se respeta el orden de visualización de los miembros de la reunión definido en el órgano.
- Se permite seleccionar los miembros del órgano que actuarán como votantes y los que no.
- El tíulo de la web, tanto la parte de administración como la de publicaciones, viene definido en el archivo de
  configuración `app.properties` mediante la propiedad `goc.tituloDeAplicacion`. Ya existía pero en la parte de 
  publicaciones no se leía este valor. 

## [release-4tic-2.7.1] - 2021-03-25
### Fixed
- Uso de UUID con formato 36 caracteres en Oracle siguiendo el [RFC 4122](https://tools.ietf.org/html/rfc4122#page-4). 

## [release-4tic-2.7.0] - 2021-03-11
### Added
- El administrador de la reunión puede confirmar o deshabilitar firmantes y presidentes de votación en una reunión desde
  la gestión de miembros de la misma.

### Fixed
- Muestra diferente error al cerrar el acta cuando un firmante no ha confirmado la asistencia y no tiene suplente 
  y cuando no hay ningún firmante definido en el órgano de la reunión.

## [release-4tic-2.6.0] - 2021-03-10
### Added
- Permite la integración con servicios externos de firma asíncronos.

## [release-4tic-2.5.2] - 2021-03-09
### Fixed
- No se permite delegar el voto en miembros que no asistirán. 

## [release-4tic-2.5.1] - 2021-03-08
### Fixed
- Corrige la visibilidad de acuerdos y deliberaciones en las actas en PDF cuando el texto está editado con formato enriquezido HTML.
- Corrige hora de los eventos mostrados en las notificaciones cuando el TimeZone local era diferente de Europe/Madrid.

## [release-4tic-2.5.0] - 2021-03-03
### Added
- Nueva propiedad de configuración `goc.publicacion.actasPublicas=[true|false*]` que permite mostrar enlaces a PDF de actas 
  en el buscador de acuerdos de manera pública haciendo que los puntos privados de orden del día aparezcan como
  "Punto privado".