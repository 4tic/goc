package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.goc.dto.QPersonaToken;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CalendarioDAO extends BaseDAODatabaseImpl
{
    QPersonaToken qPersonaToken = QPersonaToken.personaToken;

    public Long getIdPersonaByToken(String token)
    {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        return jpaQuery.from(qPersonaToken)
            .where(qPersonaToken.id.token.equalsIgnoreCase(token))
            .singleResult(qPersonaToken.id.idPersona);
    }

    @Transactional
    public void deleteByIdPersona(Long connectedUserId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qPersonaToken);
        deleteClause.where(qPersonaToken.id.idPersona.eq(connectedUserId))
            .execute();
    }
}
