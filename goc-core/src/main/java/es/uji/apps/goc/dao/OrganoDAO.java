package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.adapter.OrganoCargoAdapter;
import es.uji.apps.goc.dto.Convocante;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoLocalCargo;
import es.uji.apps.goc.dto.OrganoLogo;
import es.uji.apps.goc.dto.OrganoParametro;
import es.uji.apps.goc.dto.OrganoParametroPK;
import es.uji.apps.goc.dto.QMiembroLocal;
import es.uji.apps.goc.dto.QOrganoAutorizado;
import es.uji.apps.goc.dto.QOrganoLocal;
import es.uji.apps.goc.dto.QOrganoLocalCargo;
import es.uji.apps.goc.dto.QOrganoLogo;
import es.uji.apps.goc.dto.QOrganoParametro;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.apps.goc.dto.QReunion;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.OrganoCargo;
import es.uji.apps.goc.model.TipoOrgano;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrganoDAO extends BaseDAODatabaseImpl {
    private QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;
    private QOrganoAutorizado qOrganoAutorizado = QOrganoAutorizado.organoAutorizado;
    private QReunion qReunion = QReunion.reunion;
    private QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
    private QOrganoLogo qOrganoLogo = QOrganoLogo.organoLogo;
    private QOrganoParametro qOrganoParametro = QOrganoParametro.organoParametro;
    private QOrganoLocalCargo qOrganoLocalCargo = QOrganoLocalCargo.organoLocalCargo;

    public List<Organo> getOrganosByUserId(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoLocal> organos = query.from(qOrganoLocal)
                .where(qOrganoLocal.creadorId.eq(connectedUserId))
                .orderBy(qOrganoLocal.fechaCreacion.desc())
                .list(qOrganoLocal);

        return organosLocalesToOrganos(organos);
    }

    public List<Organo> getOrganosByAutorizadoId(Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoLocal> organos = query.from(qOrganoLocal, qOrganoAutorizado)
                .where(qOrganoAutorizado.organoExterno.eq(false)
                        .and(qOrganoAutorizado.organoId.eq(qOrganoLocal.id.stringValue()))
                        .and(qOrganoAutorizado.personaId.eq(connectedUserId)))
                .orderBy(qOrganoLocal.fechaCreacion.desc())
                .list(qOrganoLocal);

        return organosLocalesToOrganos(organos);
    }

    private List<Organo> organosLocalesToOrganos(List<OrganoLocal> organosDTO) {
        List<Organo> organos = new ArrayList<>();

        for (OrganoLocal organoLocalDTO : organosDTO) {
            Organo organo = organoLocalToOrgano(organoLocalDTO);
            organos.add(organo);
        }

        return organos;
    }

    private Organo organoLocalToOrgano(OrganoLocal organoLocalDTO) {
        Organo organo = new Organo();

        organo.setId(organoLocalDTO.getId().toString());
        organo.setNombre(organoLocalDTO.getNombre());
        organo.setNombreAlternativo(organoLocalDTO.getNombreAlternativo());
        organo.setInactivo(organoLocalDTO.isInactivo());
        organo.setCreadorId(organoLocalDTO.getCreadorId());
        organo.setFechaCreacion(organoLocalDTO.getFechaCreacion());
        organo.setOrdenado(organoLocalDTO.getOrdenado());
        organo.setHasLogo(this.hasLogo(organoLocalDTO.getId().toString(), false));
        OrganoParametro organoParametro = this.getOrganoParametro(organo.getId(), organo.isExterno());
        String emailOrgano = null;
        if (organoParametro != null) {
            if (organoParametro.getEmail() != null)
                emailOrgano = organoParametro.getEmail();
        }
        organo.setEmail(emailOrgano);

        TipoOrgano tipoOrgano = new TipoOrgano(organoLocalDTO.getTipoOrganoLocal().getId());
        organo.setTipoOrgano(tipoOrgano);

        return organo;
    }

    public Organo getOrganoById(Long organoId) {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoLocal> organos = query.from(qOrganoLocal).where(qOrganoLocal.id.eq(organoId)).list(qOrganoLocal);

        if (organos.size() == 0) {
            return null;
        }

        return organoLocalToOrgano(organos.get(0));
    }

    @Transactional
    public Organo insertOrgano(Organo organo, Long connectedUserId) {
        OrganoLocal organoLocal = organoLocalDesdeOrgano(organo);
        organoLocal.setCreadorId(connectedUserId);
        organoLocal.setFechaCreacion(new Date());
        organoLocal = this.insert(organoLocal);

        insertOrUpdateParametros(organoLocal.getId().toString(), false, organo.getEmail());
        return organoLocalToOrgano(organoLocal);
    }

    @Transactional
    public Organo updateOrgano(Organo organo) {
        OrganoLocal organoLocal = organoLocalDesdeOrgano(organo);
        organoLocal = this.update(organoLocal);

        return organoLocalToOrgano(organoLocal);
    }

    private OrganoLocal organoLocalDesdeOrgano(Organo organo) {
        OrganoLocal organoLocal = new OrganoLocal();

        if (organo.getId() != null) {
            organoLocal.setId(Long.parseLong(organo.getId()));
        }

        organoLocal.setInactivo(organo.isInactivo());
        organoLocal.setNombre(organo.getNombre());
        organoLocal.setNombreAlternativo(organo.getNombreAlternativo());
        organoLocal.setCreadorId(organo.getCreadorId());
        organoLocal.setFechaCreacion(organo.getFechaCreacion());
        organoLocal.setOrdenado(organo.getOrdenado());

        if (organo.getTipoOrgano() != null) {
            TipoOrganoLocal tipoOrganoLocal = new TipoOrganoLocal(organo.getTipoOrgano().getId());
            organoLocal.setTipoOrganoLocal(tipoOrganoLocal);
        }

        return organoLocal;
    }

    public List<Tuple> getOrganosConReunionesPublicas(Long tipoOrganoId, Integer anyo) {
        BooleanExpression beWhere = null;
        BooleanExpression whereTipo = null;
        if (anyo != null) {
            beWhere = qReunion.fecha.year().eq(anyo);
        }
        if(tipoOrganoId != null){
            whereTipo = qOrganoReunion.tipoOrganoId.eq(tipoOrganoId);
        }
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoReunion)
                .join(qOrganoReunion.reunion, qReunion)
                .where(qReunion.completada.isTrue()
                        .and(qReunion.publica.isTrue())
                        .and(beWhere)
                        .and(whereTipo))
                .distinct()
                .list(qOrganoReunion.organoId, qOrganoReunion.organoNombre, qOrganoReunion.organoNombreAlternativo);
    }

    public List<Organo> getOrganosByAdmin(boolean soloActivos) {
        JPAQuery query = new JPAQuery(entityManager);

        query = query.from(qOrganoLocal);

        if (soloActivos) {
            query = query.where(qOrganoLocal.inactivo.eq(false).or(qOrganoLocal.inactivo.isNull()));
        }

        List<OrganoLocal> organos = query.orderBy(qOrganoLocal.fechaCreacion.desc()).list(qOrganoLocal);

        return organosLocalesToOrganos(organos);
    }

    public Long getOrganoIdByMiembroId(Long miembroId) {
        QMiembroLocal qMiembroLocal = new QMiembroLocal("qMiembroLocal");
        QOrganoLocal qOrganoLocal = new QOrganoLocal("qOrganoLocal");
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qMiembroLocal).join(qMiembroLocal.organo, qOrganoLocal)
                .where(qOrganoLocal.inactivo.eq(false).or(qOrganoLocal.inactivo.isNull())
                        .and(qMiembroLocal.id.eq(miembroId)))
                .uniqueResult(qOrganoLocal.id);
    }

    public OrganoLogo getLogo(
            String organoId,
            boolean externo
    ) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoLogo)
                .where(qOrganoLogo.organoLogoPK.organoId.eq(organoId)
                        .and(qOrganoLogo.organoLogoPK.externo.eq(externo))
                )
                .uniqueResult(qOrganoLogo);
    }

    public OrganoParametro getOrganoParametro(String organoId, Boolean ordinario) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoParametro)
                .where(qOrganoParametro.organoParametroPK.organoId.eq(organoId)
                        .and(qOrganoParametro.organoParametroPK.ordinario.eq(ordinario))
                )
                .uniqueResult(qOrganoParametro);
    }

    @Transactional
    public void insertOrUpdateParametros(
            String organoId,
            Boolean externo,
            String email
    ) {
        OrganoParametro organoParametro = new OrganoParametro();
        OrganoParametroPK organoParametroPK = new OrganoParametroPK(organoId, externo);
        organoParametro.setOrganoParametroPK(organoParametroPK);
        organoParametro.setEmail(email);

        entityManager.merge(organoParametro);
    }

    public List<Convocante> getOrganosReunionWithEmailByReunionId(Long reunionId) {
        JPAQuery query = new JPAQuery(entityManager);
        List<Tuple> organos = query.from(qOrganoParametro).rightJoin(qOrganoReunion).on(qOrganoParametro.organoParametroPK.organoId.eq(qOrganoReunion.organoId))
                .where(qOrganoReunion.reunion.id.eq(reunionId)).list(qOrganoReunion.organoNombre, qOrganoParametro.email);

        return organos.stream().map(organo -> new Convocante(organo.get(qOrganoReunion.organoNombre), organo.get(qOrganoParametro.email))).collect(Collectors.toList());
    }

    public List<Reunion> reunionesByOrganoId(String organoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunion).join(qOrganoReunion).on(qReunion.id.eq(qOrganoReunion.reunion.id))
                .where(qOrganoReunion.organoId.eq(organoId)).list(qReunion);
    }

    public List<OrganoLocal> getOrganosByNombre(String organoNombre) {
        JPAQuery query = new JPAQuery(entityManager);
        String searchExpression = "%" + organoNombre + "%";

        return query.from(qOrganoLocal)
                .where(qOrganoLocal.nombre.likeIgnoreCase(searchExpression)
                        .or(qOrganoLocal.nombreAlternativo.likeIgnoreCase(searchExpression)))
                .list(qOrganoLocal);
    }

    public boolean hasLogo(String organoId, boolean externo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoLogo)
                .where(qOrganoLogo.organoLogoPK.organoId.eq(organoId)
                        .and(qOrganoLogo.organoLogoPK.externo.eq(externo))
                ).exists();
    }

    public boolean isExterno(Long organoId) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoLocal).where(qOrganoLocal.id.eq(organoId)).count() == 0;
    }

    public boolean isTipoOrganoBeingUsed(Long tipoOrganoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoLocal).where(qOrganoLocal.tipoOrgano.id.eq(tipoOrganoId)).exists();
    }

    public List<OrganoLocalCargo> getCargoByOrgano(String organoId) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoLocalCargo).where(qOrganoLocalCargo.organoId.eq(organoId)).list(qOrganoLocalCargo);
    }

    @Transactional
    public void deleteCargoById(Long organoCargoId) {
        JPADeleteClause query = new JPADeleteClause(entityManager, qOrganoLocalCargo);
        query.where(qOrganoLocalCargo.id.eq(organoCargoId)).execute();
    }

    @Transactional
    public OrganoCargo insertOrganoCargo(OrganoCargo organoCargo) {
        OrganoLocalCargo organoLocalCargo = OrganoCargoAdapter.toEntity(organoCargo);
        organoLocalCargo = entityManager.merge(organoLocalCargo);
        organoCargo.setId(organoLocalCargo.getId());
        return organoCargo;
    }
}
