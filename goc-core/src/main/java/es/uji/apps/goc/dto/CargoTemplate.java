package es.uji.apps.goc.dto;

import java.util.Set;

import es.uji.apps.goc.model.Miembro;

public class CargoTemplate
{
    private String id;

    private String nombre;

    private String codigo;

    private Set<Miembro> miembros;
    private Boolean responsableActa;

    public CargoTemplate() {}

    public CargoTemplate(String id)
    {
        this.id = id;
    }

    public CargoTemplate(String id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
        this.codigo = "";
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public void setResponsableActa(Boolean responsableActa)
    {
        this.responsableActa = responsableActa;
    }

    public Boolean getResponsableActa()
    {
        return responsableActa;
    }
}
