package es.uji.apps.goc.dto;

import org.hibernate.annotations.Type;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_VW_RESULTADOS_TODOS_VOTOS")
public class ResultadosTodosVotosDTO {

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    private UUID id;

    @Column(name = "REUNION_ID")
    private String reunionId;

    @Column(name = "PUNTO_ID")
    private String puntoOrdenDiaId;

    @Column(name = "VOTO")
    private String voto;

    @Column(name = "NUMERO_VOTOS")
    private Long numeroVotos;

    public UUID getId()
    {
        return id;
    }

    public void setId(UUID id)
    {
        this.id = id;
    }

    public String getReunionId()
    {
        return reunionId;
    }

    public void setReunionId(String reunionId)
    {
        this.reunionId = reunionId;
    }

    public String getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(String puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public String getVoto()
    {
        return voto;
    }

    public void setVoto(String voto)
    {
        this.voto = voto;
    }

    public Long getNumeroVotos()
    {
        return numeroVotos;
    }

    public void setNumeroVotos(Long numeroVotos)
    {
        this.numeroVotos = numeroVotos;
    }
}
