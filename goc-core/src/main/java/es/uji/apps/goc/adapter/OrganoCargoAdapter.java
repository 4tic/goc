package es.uji.apps.goc.adapter;

import es.uji.apps.goc.dto.OrganoLocalCargo;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.OrganoCargo;

public class OrganoCargoAdapter {
    public static OrganoCargo fromEntity(OrganoLocalCargo entity, Cargo cargo) {
        return new OrganoCargo(entity.getId(), entity.getCargoId(), cargo.getNombre(), cargo.getCodigo(), entity.getOrganoId());
    }

    public static OrganoLocalCargo toEntity(OrganoCargo organoCargo) {
        OrganoLocalCargo organoLocalCargo = new OrganoLocalCargo();
        organoLocalCargo.setCargoId(organoCargo.getCargoId());
        organoLocalCargo.setOrganoId(organoCargo.getOrganoId());

        return organoLocalCargo;
    }
}
