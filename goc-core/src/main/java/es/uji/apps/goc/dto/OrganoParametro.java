package es.uji.apps.goc.dto;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_ORGANOS_PARAMETROS")
public class OrganoParametro
{

    @EmbeddedId
    OrganoParametroPK organoParametroPK;

    @Column(name = "CONVOCAR_SIN_ORDEN_DIA")
    private Boolean convocarSinOrdenDia;

    @Column(name = "EMAIL")
    private String email;

    public OrganoParametroPK getOrganoParametroPK()
    {
        return organoParametroPK;
    }

    public void setOrganoParametroPK(OrganoParametroPK organoParametroPK)
    {
        this.organoParametroPK = organoParametroPK;
    }

    public Boolean getConvocarSinOrdenDia()
    {
        return convocarSinOrdenDia;
    }

    public void setConvocarSinOrdenDia(Boolean convocarSinOrdenDia)
    {
        this.convocarSinOrdenDia = convocarSinOrdenDia;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean hasEmail() {
        return (this.email != null && !this.email.trim().isEmpty());
    }
}
