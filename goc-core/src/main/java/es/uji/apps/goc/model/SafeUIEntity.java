package es.uji.apps.goc.model;

import com.google.common.base.Strings;

import es.uji.commons.rest.UIEntity;

abstract public class SafeUIEntity extends UIEntity {
    static public Long getSafeLong(UIEntity uiEntity, String key) {
        String attr = uiEntity.get(key);
        return Strings.isNullOrEmpty(attr) ? null : uiEntity.getLong(key);
    }
}
