package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class MiembrosExternosException extends CoreDataBaseException
{
    public MiembrosExternosException()
    {
        super("appI18N.excepciones.noseHaPodidodescargarMiembros");
    }

    public MiembrosExternosException(String message)
    {
        super(message);
    }
}
