package es.uji.apps.goc.templates;

import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

public class TemplateResolverFactory
{

    private final static String CLASSPATH = "classpath:";

    public static ITemplateResolver getTemplateResolver(
        String prefix,
        String templateMode,
        String sufix,
        boolean cacheable,
        Long timeToLive
    )
    {
        if (prefix.startsWith(CLASSPATH))
        {
            ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
            templateResolver.setCharacterEncoding("UTF-8");
            templateResolver.setTemplateMode(templateMode);
            templateResolver.setPrefix(formatPrefix(prefix));
            templateResolver.setSuffix(sufix);
            templateResolver.setCacheable(cacheable);
            templateResolver.setCacheTTLMs(timeToLive);
            return templateResolver;
        }
        FileTemplateResolver fileTemplateResolver = new FileTemplateResolver();
        fileTemplateResolver.setCharacterEncoding("UTF-8");
        fileTemplateResolver.setTemplateMode(templateMode);
        fileTemplateResolver.setPrefix(formatPrefix(prefix));
        fileTemplateResolver.setSuffix(sufix);
        fileTemplateResolver.setCacheable(cacheable);
        fileTemplateResolver.setCacheTTLMs(timeToLive);
        return fileTemplateResolver;
    }

    private static String formatPrefix(String prefix)
    {
        if (prefix.contains("classpath:"))
        {
            return prefix.replaceAll("^classpath:", "");
        }
        if (prefix.contains("file://"))
        {
            return prefix.replaceAll("^file://", "");
        }
        if (prefix.contains("file:/"))
        {
            return prefix.replaceAll("^file:/", "");
        }

        return prefix;
    }
}
