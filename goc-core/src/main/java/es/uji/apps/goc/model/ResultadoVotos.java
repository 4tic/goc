package es.uji.apps.goc.model;

public class ResultadoVotos {
    private Long puntoId;
    private Long aFavor;
    private Long enContra;
    private Long abstenciones;

    public ResultadoVotos(Long puntoId, Long aFavor, Long enContra, Long abstenciones)
    {
        this.puntoId = puntoId;
        this.aFavor = aFavor;
        this.enContra = enContra;
        this.abstenciones = abstenciones;
    }

    public ResultadoVotos()
    {
    }

    public Long getaFavor()
    {
        return aFavor;
    }

    public void setaFavor(Long aFavor)
    {
        this.aFavor = aFavor;
    }

    public Long getEnContra()
    {
        return enContra;
    }

    public void setEnContra(Long enContra)
    {
        this.enContra = enContra;
    }

    public Long getAbstenciones()
    {
        return abstenciones;
    }

    public void setAbstenciones(Long abstenciones)
    {
        this.abstenciones = abstenciones;
    }

    public Long getPuntoId()
    {
        return puntoId;
    }

    public void setPuntoId(Long puntoId)
    {
        this.puntoId = puntoId;
    }
}
