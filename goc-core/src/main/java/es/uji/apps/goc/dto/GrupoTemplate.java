package es.uji.apps.goc.dto;

import java.util.ArrayList;
import java.util.List;

public class GrupoTemplate
{
    private String nombre;
    private String OrganoId;

    private List<MiembroTemplate> asistentes;
    private List<MiembroTemplate> ausentes;

    public GrupoTemplate()
    {
    }

    public GrupoTemplate(String nombre, String organoId)
    {
        this.nombre = nombre;
        OrganoId = organoId;
        asistentes = new ArrayList<>();
        ausentes = new ArrayList<>();
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getOrganoId()
    {
        return OrganoId;
    }

    public void setOrganoId(String organoId)
    {
        OrganoId = organoId;
    }

    public List<MiembroTemplate> getAsistentes()
    {
        return asistentes;
    }

    public void setAsistentes(List<MiembroTemplate> asistentes)
    {
        this.asistentes = asistentes;
    }

    public List<MiembroTemplate> getAusentes()
    {
        return ausentes;
    }

    public void setAusentes(List<MiembroTemplate> ausentes)
    {
        this.ausentes = ausentes;
    }
}
