package es.uji.apps.goc.templates;

public interface CommonPropertiesConfig {
    String getAppContext();

    String getAppTitle();

    String getTemplatesHtmlPath();

    String getCharset();

    String getPublicUrl();

    String getCustomCSS();

    String getLogo();

    String getLogoPublic();

    String getStaticsUrl();
}
