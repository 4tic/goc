package es.uji.apps.goc.model;

public class EstadoPunto {
    private Long puntoId;
    private boolean abierta;
    private boolean votable;

    public EstadoPunto(
        Long puntoId,
        boolean abierta,
        boolean votable
    ) {
        this.puntoId = puntoId;
        this.abierta = abierta;
        this.votable = votable;
    }

    public Long getPuntoId() {
        return puntoId;
    }

    public void setPuntoId(Long puntoId) {
        this.puntoId = puntoId;
    }

    public boolean isAbierta() {
        return abierta;
    }

    public void setAbierta(boolean abierta) {
        this.abierta = abierta;
    }

    public boolean isVotable() {
        return votable;
    }

    public void setVotable(boolean votable) {
        this.votable = votable;
    }
}
