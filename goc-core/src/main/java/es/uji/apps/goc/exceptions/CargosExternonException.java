package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class CargosExternonException extends CoreDataBaseException
{
    public CargosExternonException(String message)
    {
        super(message);
    }
}
