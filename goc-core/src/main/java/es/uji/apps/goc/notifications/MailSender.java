package es.uji.apps.goc.notifications;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;


@Component
public class MailSender
{
    private static Logger log = Logger.getLogger(MailSender.class);

    private String smtpHost;
    private Boolean smtpStartTLS;
    private Boolean smtpAuthRequired;
    private Integer smtpPort;
    private String smtpUsername;
    private String smtpPassword;
    private String defaultSender;
    private Boolean smtpEnabled;

    @Autowired
    public MailSender(
            @Value("${uji.smtp.host}") String smtpHost,
            @Value("${uji.smtp.starttls.enable}") String smtpStartTLS,
            @Value("${uji.smtp.auth}") String smtpAuthRequired,
            @Value("${uji.smtp.port}") String smtpPort,
            @Value("${uji.smtp.username}") String smtpUsername,
            @Value("${uji.smtp.password}") String smtpPassword,
            @Value("${uji.smtp.defaultSender}") String defaultSender,
            @Value("${goc.smtp.enabled:true}") String smtpEnabled)
    {
        this.smtpHost = smtpHost;
        this.smtpStartTLS = Boolean.parseBoolean(smtpStartTLS);
        this.smtpAuthRequired = Boolean.parseBoolean(smtpAuthRequired);
        this.smtpPort = Integer.parseInt(smtpPort);
        this.smtpUsername = smtpUsername;
        this.smtpPassword = smtpPassword;
        this.defaultSender = defaultSender;
        this.smtpEnabled = Boolean.parseBoolean(smtpEnabled);
    }

    public List<String> send(Mensaje message) throws CanNotSendException
    {
        List<String> emailsNoValidos = new ArrayList<>();

        if (smtpEnabled && message.getDestinos().size() > 0) {
            Session session = getMailSession();

            List<String> destinos = message.getDestinos();

            List<String> enviados = new ArrayList<>();

            for (String destino : destinos) {
                if (!enviados.contains(destino))
                {
                    enviados.add(destino);
                    Message mimeMessage = new MimeMessage(session);
                    try
                    {
                        defineMessageHeaders(mimeMessage);
                        setSubject(message, mimeMessage);
                        setMessageContent(message, mimeMessage);
                    } catch (MessagingException e)
                    {
                        log.error(e);
                    }

                    message.setDestinos(Arrays.asList(destino));
                    try
                    {
                        setRecipients(message, mimeMessage);
                        Transport.send(mimeMessage);
                    } catch (Exception e)
                    {
                        log.error(e);

                        if (destino != null)
                            emailsNoValidos.add(destino);
                    }
                }
            }
        }
        return emailsNoValidos;
    }

    private Authenticator getAuthenticator()
    {
        if (smtpUsername == null || smtpPassword == null)
        {
            return null;
        }

        return new Authenticator()
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(smtpUsername, smtpPassword);
            }
        };
    }

    private Properties getEmailProperties()
    {
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.starttls.enable", smtpStartTLS);
        props.put("mail.smtp.auth", smtpAuthRequired);
        props.put("mail.smtp.port", smtpPort);


        return props;
    }

    private Session getMailSession()
    {
        return Session.getInstance(getEmailProperties(), getAuthenticator());
    }

    private void defineMessageHeaders(Message message) throws MessagingException
    {
        message.addHeader("Auto-Submitted", "auto-generated");
    }

    private void setSubject(Mensaje mensaje, Message message) throws MessagingException
    {
        message.setSubject(mensaje.getAsunto());
    }

    private void setRecipients(Mensaje mensaje, Message message) throws MessagingException
    {
        message.setFrom(new InternetAddress(mensaje.getFrom()));

        if (mensaje.getReplyTo() != null && !mensaje.getReplyTo().isEmpty())
        {
            message.setReplyTo(new Address[]{getAddress(mensaje.getReplyTo())});
        }

        for (String destino : mensaje.getDestinos())
        {
            Message.RecipientType recipientType = Message.RecipientType.BCC;
            message.addRecipient(recipientType, getAddress(destino));
        }
    }

    private Address getAddress(String address) throws AddressException
    {
        if (address == null || address.equals(""))
        {
            return new InternetAddress();
        }

        return new InternetAddress(address.trim());
    }

    private void setMessageContent(Mensaje mensaje, Message message) throws MessagingException
    {
        MimeMultipart multipart = new MimeMultipart();

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(mensaje.getCuerpo(), mensaje.getContentType());
        multipart.addBodyPart(messageBodyPart);

        if(mensaje.getAdjuntos() != null)
        {
            for (ArchivoAdjunto archivoAdjunto : mensaje.getAdjuntos())
            {

                messageBodyPart = new MimeBodyPart();
                byte[] contentDecoded = Base64.getDecoder().decode(archivoAdjunto.getContent());
                ByteArrayDataSource byteArrayDataSource =
                    new ByteArrayDataSource(contentDecoded, archivoAdjunto.getcontentType());
                messageBodyPart.setDataHandler(new DataHandler(byteArrayDataSource));
                messageBodyPart.setFileName(archivoAdjunto.getFilename());
                multipart.addBodyPart(messageBodyPart);
            }
        }

        if(mensaje.getFileBase64() != null && mensaje.getFileBase64().length > 0){
            byte[] fileDecoded = Base64.getDecoder().decode(mensaje.getFileBase64());
            MimeBodyPart att = new MimeBodyPart();
            ByteArrayDataSource bds = new ByteArrayDataSource(fileDecoded, mensaje.getFileContentType());
            bds.setName("reunion.ics");
            att.setDataHandler(new DataHandler(bds));
            att.setFileName(bds.getName());
            multipart.addBodyPart(att);
        }

        message.setContent(multipart);
    }
}