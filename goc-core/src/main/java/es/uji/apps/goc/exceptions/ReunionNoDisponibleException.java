package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ReunionNoDisponibleException extends CoreDataBaseException
{
    public ReunionNoDisponibleException()
    {
        super("appI18N.excepciones.reunionNoDisponible");
    }

    public ReunionNoDisponibleException(String message)
    {
        super(message);
    }
}
