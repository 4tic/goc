package es.uji.apps.goc.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_MIEMBROS")
public class MiembroLocal implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    private String nombre;

    private String email;

    private String descripcion;

    @Column(name = "DESCRIPCION_ALT")
    private String descripcionAlternativa;

    @ManyToOne
    @JoinColumn(name = "ORGANO_ID")
    private OrganoLocal organo;

    @Column(name = "CARGO_ID_NEW")
    private String cargoId;

    private Long orden;

    private String grupo;

    @Column(name = "ORDEN_GRUPO")
    private Long ordenGrupo;

    @Column(name="FIRMANTE")
    private Boolean firmante;

    @Column(name="PRESIDE_VOTACION")
    private Boolean presideVotacion;

    @Column(name="VOTANTE")
    private Boolean votante;

    public MiembroLocal() {}

    public MiembroLocal(Long id, String nombre, String email, OrganoLocal organo) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.organo = organo;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public OrganoLocal getOrgano()
    {
        return organo;
    }

    public void setOrgano(OrganoLocal organo)
    {
        this.organo = organo;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCargoId()
    {
        return cargoId;
    }

    public void setCargoId(String cargo)
    {
        this.cargoId = cargo;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDescripcionAlternativa()
    {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa)
    {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public Long getOrdenGrupo()
    {
        return ordenGrupo;
    }

    public void setOrdenGrupo(Long ordenGrupo)
    {
        this.ordenGrupo = ordenGrupo;
    }

    public Boolean getFirmante() {
        return (this.firmante != null && this.firmante == true);
    }

    public void setFirmante(Boolean firmante) {
        this.firmante = firmante;
    }

    public Boolean isFirmante() {
        return (this.firmante != null && this.firmante == true);
    }

    public Boolean isPresideVotacion() {
        return presideVotacion != null ? presideVotacion : false;
    }

    public void setPresideVotacion(Boolean presideVotacion) {
        this.presideVotacion = presideVotacion;
    }

    public boolean isVotante() {
        return votante != null ? votante : false;
    }

    public void setVotante(Boolean votante) {
        this.votante = votante;
    }
}