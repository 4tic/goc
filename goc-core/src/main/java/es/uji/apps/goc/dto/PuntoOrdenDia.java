package es.uji.apps.goc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import es.uji.apps.goc.HTMLUtils;

@Entity
@Table(name = "GOC_REUNIONES_PUNTOS_ORDEN_DIA")
public class PuntoOrdenDia implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 4000)
    private String titulo;

    @Column(name = "TITULO_ALT")
    private String tituloAlternativo;

    private String descripcion;

    @Column(name = "DESCRIPCION_ALT")
    private String descripcionAlternativa;

    private Long orden;

    private String acuerdos;

    @Column(name = "ACUERDOS_ALT")
    private String acuerdosAlternativos;

    private Boolean publico;

    private String deliberaciones;

    @Column(name = "DELIBERACIONES_ALT")
    private String deliberacionesAlternativas;

    @Column(name = "URL_ACTA")
    private String urlActa;

    @Column(name = "URL_ACTA_ALT")
    private String urlActaAlternativa;

    @Column(name = "URL_ACTA_ANTERIOR")
    private String urlActaAnterior;

    @Column(name = "URL_ACTA_ANTERIOR_ALT")
    private String urlActaAnteriorAlt;

    @Column(name = "EDITADO_EN_REAPERTURA")
    private Boolean editado;

    @Column(name = "VOTO_PUBLICO")
    private Boolean votoPublico;

    @Column(name = "FECHA_APERTURA_VOTACION")
    private Date fechaAperturaVotacion;

    @ManyToOne
    @JoinColumn(name = "REUNION_ID")
    private Reunion reunion;

    @OneToMany(mappedBy = "puntoOrdenDia", cascade = CascadeType.REMOVE)
    private Set<PuntoOrdenDiaDocumento> puntoOrdenDiaDocumentos;

    @OneToMany(mappedBy = "puntoOrdenDia", cascade = CascadeType.REMOVE)
    private Set<PuntoOrdenDiaAcuerdo> puntoOrdenDiaAcuerdos;

    @OneToMany(mappedBy = "puntoOrdenDia", cascade = CascadeType.ALL)
    private Set<PuntoOrdenDiaDescriptor> puntoOrdenDiaDescriptores;

    @ManyToOne
    @JoinColumn(name = "ID_PUNTO_SUPERIOR", referencedColumnName = "id")
    private PuntoOrdenDia puntoSuperior;

    @OneToMany(mappedBy = "puntoSuperior", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<PuntoOrdenDia> puntosInferiores;

    public PuntoOrdenDia() {}
    public PuntoOrdenDia(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public Reunion getReunion() {
        return reunion;
    }

    public void setReunion(Reunion reunion) {
        this.reunion = reunion;
    }

    public Set<PuntoOrdenDiaDocumento> getPuntoOrdenDiaDocumentos()
    {
        return puntoOrdenDiaDocumentos;
    }

    public void setPuntoOrdenDiaDocumentos(Set<PuntoOrdenDiaDocumento> puntoOrdenDiaDocumentos)
    {
        this.puntoOrdenDiaDocumentos = puntoOrdenDiaDocumentos;
    }

    public String getAcuerdos()
    {
        return (acuerdos != null) ? HTMLUtils.forceCloseHTMLTags(acuerdos) : acuerdos;
    }

    public void setAcuerdos(String acuerdos)
    {
        this.acuerdos = acuerdos;
    }

    public String getDeliberaciones() {
        return (deliberaciones != null) ? HTMLUtils.forceCloseHTMLTags(deliberaciones) : deliberaciones;
    }

    public void setDeliberaciones(String deliberaciones) {
        this.deliberaciones = deliberaciones;
    }

    public Boolean isPublico()
    {
        return publico;
    }

    public void setPublico(Boolean publico)
    {
        this.publico = publico;
    }


    public String getTituloAlternativo()
    {
        return tituloAlternativo;
    }

    public void setTituloAlternativo(String tituloAlternativo)
    {
        this.tituloAlternativo = tituloAlternativo;
    }

    public String getDescripcionAlternativa()
    {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa)
    {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public String getAcuerdosAlternativos()
    {
        return acuerdosAlternativos;
    }

    public void setAcuerdosAlternativos(String acuerdosAlternativos)
    {
        this.acuerdosAlternativos = acuerdosAlternativos;
    }

    public String getDeliberacionesAlternativas()
    {
        return deliberacionesAlternativas;
    }

    public void setDeliberacionesAlternativas(String deliberacionesAlternativas)
    {
        this.deliberacionesAlternativas = deliberacionesAlternativas;
    }

    public Set<PuntoOrdenDiaDescriptor> getPuntoOrdenDiaDescriptores() {
        return puntoOrdenDiaDescriptores;
    }

    public void setPuntoOrdenDiaDescriptores(Set<PuntoOrdenDiaDescriptor> puntoOrdenDiaDescriptores) {
        this.puntoOrdenDiaDescriptores = puntoOrdenDiaDescriptores;
    }

    public Set<PuntoOrdenDiaAcuerdo> getPuntoOrdenDiaAcuerdos()
    {
        return puntoOrdenDiaAcuerdos;
    }

    public void setPuntoOrdenDiaAcuerdos(Set<PuntoOrdenDiaAcuerdo> puntoOrdenDiaAcuerdos)
    {
        this.puntoOrdenDiaAcuerdos = puntoOrdenDiaAcuerdos;
    }

    public String getUrlActa()
    {
        return urlActa;
    }

    public void setUrlActa(String urlActa)
    {
        this.urlActa = urlActa;
    }

    public String getUrlActaAlternativa()
    {
        return urlActaAlternativa;
    }

    public void setUrlActaAlternativa(String urlActaAlternativa)
    {
        this.urlActaAlternativa = urlActaAlternativa;
    }

    public PuntoOrdenDia getPuntoSuperior()
    {
        return puntoSuperior;
    }

    public void setPuntoSuperior(PuntoOrdenDia puntoSuperior)
    {
        this.puntoSuperior = puntoSuperior;
    }

    public Set<PuntoOrdenDia> getPuntosInferiores()
    {
        return puntosInferiores;
    }

    public void setPuntosInferiores(Set<PuntoOrdenDia> puntosInferiores)
    {
        this.puntosInferiores = puntosInferiores;
    }

    public String getUrlActaAnterior()
    {
        return urlActaAnterior;
    }

    public void setUrlActaAnterior(String urlActaAnterior)
    {
        this.urlActaAnterior = urlActaAnterior;
    }

    public String getUrlActaAnteriorAlt()
    {
        return urlActaAnteriorAlt;
    }

    public void setUrlActaAnteriorAlt(String urlActaAnteriorAlt)
    {
        this.urlActaAnteriorAlt = urlActaAnteriorAlt;
    }

    public Boolean getEditado() {
        return editado;
    }

    public void setEditado(Boolean editado) {
        this.editado = editado;
    }

    public Boolean isEditado() {
        return this.editado != null && this.editado == true;
    }

    public void cleanBeforeDuplicate()
    {
        this.setAcuerdos(null);
        this.setAcuerdosAlternativos(null);
        this.setDeliberaciones(null);
        this.setId(null);
        this.setDeliberacionesAlternativas(null);
        this.setPuntoOrdenDiaDocumentos(null);
        this.setPuntoOrdenDiaAcuerdos(null);
        this.setUrlActa(null);
        this.setUrlActaAlternativa(null);
        this.setUrlActaAnterior(null);
        this.setUrlActaAnteriorAlt(null);
    }

    public Boolean getVotoPublico()
    {
        return votoPublico;
    }

    public void setVotoPublico(Boolean votoPublico)
    {
        this.votoPublico = votoPublico;
    }

    public Date getFechaAperturaVotacion()
    {
        return fechaAperturaVotacion;
    }

    public void setFechaAperturaVotacion(Date fechaAperturaVotacion)
    {
        this.fechaAperturaVotacion = fechaAperturaVotacion;
    }
}