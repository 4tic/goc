package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.QOrganoAutorizado;
import es.uji.apps.goc.dto.QOrganoReunionMiembro;
import es.uji.apps.goc.dto.QReunionExternos;
import es.uji.apps.goc.dto.QReunionInvitado;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionInvitado;
import es.uji.apps.goc.model.Miembro;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrganoReunionMiembroDAO extends BaseDAODatabaseImpl
{
    private QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;
    private QOrganoAutorizado qOrganoAutorizado = QOrganoAutorizado.organoAutorizado;
    private QReunionExternos qReunionExternos = QReunionExternos.reunionExternos;
    private QReunionInvitado qReunionInvitado = QReunionInvitado.reunionInvitado;
    public final String EMPTY_CARGO_ID = "0";

    public List<OrganoReunionMiembro> getOrganoReunionMiembroByOrganoReunionId(Long organoReunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.organoReunion.id.eq(organoReunionId))
                .orderBy(qOrganoReunionMiembro.ordenGrupo.asc(), qOrganoReunionMiembro.orden.asc(), qOrganoReunionMiembro.id.asc())
                .list(qOrganoReunionMiembro);
    }

    public OrganoReunionMiembro getMiembroById(Long miembroId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoReunionMiembro> resultado = query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.id.eq(miembroId))
                .list(qOrganoReunionMiembro);

        if (resultado.size() == 0)
        {
            return null;
        }

        return resultado.get(0);
    }

    public List<OrganoReunionMiembro> getMiembroReunionByOrganoAndReunionId(String organoId, Boolean externo,
            Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.organoExterno.eq(externo)
                        .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                .and(qOrganoReunionMiembro.reunionId.eq(reunionId))))
                .orderBy(qOrganoReunionMiembro.ordenGrupo.asc(), qOrganoReunionMiembro.orden.asc(), qOrganoReunionMiembro.id.asc())
                .list(qOrganoReunionMiembro);
    }

    public List<OrganoReunionMiembro> getAsistenteReunionByOrganoAndReunionId(String organoId, Boolean externo,
            Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.organoExterno.eq(externo)
                        .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                                .and(qOrganoReunionMiembro.asistencia.isTrue())))
                .orderBy(qOrganoReunionMiembro.ordenGrupo.asc(), qOrganoReunionMiembro.orden.asc(), qOrganoReunionMiembro.id.asc())
                .list(qOrganoReunionMiembro);
    }

    @Transactional
    public void updateAsistenteReunionByEmail(Long reunionId, String organoId, Boolean externo, String asistenteEmail,
            Boolean asistencia, Long suplenteId, String suplenteNombre, String suplenteEmail, Boolean guardarAsistencia,
            Boolean justificaAusencia, Boolean firmante, Boolean presideVotacion)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        if (guardarAsistencia)
        {
            update.set(qOrganoReunionMiembro.asistencia, asistencia);
        }

        update.set(qOrganoReunionMiembro.suplenteId, suplenteId)
                .set(qOrganoReunionMiembro.suplenteNombre, suplenteNombre)
                .set(qOrganoReunionMiembro.suplenteEmail, suplenteEmail)
                .set(qOrganoReunionMiembro.justificaAusencia, justificaAusencia)
                .set(qOrganoReunionMiembro.firmante, firmante)
                .set(qOrganoReunionMiembro.presideVotacion, presideVotacion)
                .where(qOrganoReunionMiembro.email.eq(asistenteEmail)
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId)
                                .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                        .and(qOrganoReunionMiembro.organoExterno.eq(externo)))));
        update.execute();
    }

    public Optional<OrganoReunionMiembro> getMiembroByAsistenteIdOrSuplenteId(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.suplenteId.eq(connectedUserId)
                                .or(qOrganoReunionMiembro.miembroId.eq(connectedUserId))))
                .list(qOrganoReunionMiembro).stream().findFirst();
    }

    public List<OrganoReunionMiembro> getAsistentesByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.asistencia.isNull().or(qOrganoReunionMiembro.asistencia.eq(true))))
                .list(qOrganoReunionMiembro);
    }

    public List<OrganoReunionMiembro> getMiembrosByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId))
                .orderBy(qOrganoReunionMiembro.ordenGrupo.asc(), qOrganoReunionMiembro.orden.asc(), qOrganoReunionMiembro.id.asc())
                .list(qOrganoReunionMiembro);
    }

    public OrganoReunionMiembro getByReunionAndOrganoAndEmail(Long reunionId, String organoId, Boolean externo,
            String email)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoReunionMiembro> miembros = query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.email.eq(email)
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId)
                                .and(qOrganoReunionMiembro.organoId.eq(organoId)
                                        .and(qOrganoReunionMiembro.organoExterno.eq(externo)))))
                .list(qOrganoReunionMiembro);

        if (miembros.size() == 0) return null;

        return miembros.get(0);
    }

    @Transactional
    public void deleteByOrganoReunionIdPersonaIdAndCargoId(Long organoReunionId, String personaId, String cargoId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qOrganoReunionMiembro);

        deleteClause.where(qOrganoReunionMiembro.organoReunion.id.eq(organoReunionId)
                .and(qOrganoReunionMiembro.miembroId.eq(Long.parseLong(personaId)).and(qOrganoReunionMiembro.cargoId.eq(cargoId))))
                .execute();
    }

    @Transactional
    public void updateReunionMiembroWithMiembro(Long organoReunionId, String personaId, String cargoId, Miembro miembro)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.nombre, miembro.getNombre()).
            set(qOrganoReunionMiembro.email, miembro.getEmail()).
            set(qOrganoReunionMiembro.cargoId,
                miembro.getCargo() != null ? miembro.getCargo().getId() : null).
            set(qOrganoReunionMiembro.cargoCodigo, miembro.getCargo() != null ? miembro.getCargo().getCodigo() : null).
            set(qOrganoReunionMiembro.cargoNombre, miembro.getCargo() != null ? miembro.getCargo().getNombre() : null).
            set(qOrganoReunionMiembro.cargoNombreAlternativo,
                miembro.getCargo() != null ? miembro.getCargo().getNombreAlternativo() : null).
            set(qOrganoReunionMiembro.descripcion, miembro.getDescripcion()).
            set(qOrganoReunionMiembro.descripcionAlternativa, miembro.getDescripcionAlternativa()).
            set(qOrganoReunionMiembro.orden, miembro.getOrden()).
            set(qOrganoReunionMiembro.ordenGrupo, miembro.getOrdenGrupo()).
            set(qOrganoReunionMiembro.grupo, miembro.getGrupo()).
            set(qOrganoReunionMiembro.firmante, miembro.getFirmante()).
            set(qOrganoReunionMiembro.presideVotacion, miembro.isPresideVotacion()).
            set(qOrganoReunionMiembro.votante, miembro.isVotante());

        BooleanExpression condition = qOrganoReunionMiembro.organoReunion.id.eq(organoReunionId).and(qOrganoReunionMiembro.miembroId.eq(Long.parseLong(personaId)));

        if(cargoId != null){
            if (cargoId.equals(EMPTY_CARGO_ID)) {
                condition = condition.and(qOrganoReunionMiembro.cargoId.isNull());
            }
            else {
                condition = condition.and(qOrganoReunionMiembro.cargoId.eq(cargoId));
            }
        }

        update.where(condition).execute();
    }

    @Transactional
    public List<String> getMailsAsistenciaConfirmadaReunion(Reunion reunion){
        return getMailsMiembrosAsistentesInvitadosAutorizadosYExternosByReunion(reunion, true);
    }

    @Transactional
    public List<String> getMailsReunion(Reunion reunion){
        return getMailsMiembrosAsistentesInvitadosAutorizadosYExternosByReunion(reunion, false);
    }

    private List<String> getMailsMiembrosAsistentesInvitadosAutorizadosYExternosByReunion(Reunion reunion, boolean asistenciaMiembros) {
        List<String> emailsMiembrosAutorizadosInvitadosYExternos = new ArrayList<>();
        JPAQuery query = new JPAQuery(entityManager);

        if (reunion.getReunionOrganos() != null) {
            reunion.getReunionOrganos().stream().forEach(organo -> {
                organo.getMiembros().stream().forEach(miembro -> {
                    if (miembro.getAsistencia() != null && miembro.getAsistencia() && asistenciaMiembros || !asistenciaMiembros) {
                        if (miembro.getSuplenteEmail() != null) {
                            emailsMiembrosAutorizadosInvitadosYExternos.add(miembro.getSuplenteEmail());
                        } else {
                            emailsMiembrosAutorizadosInvitadosYExternos.add(miembro.getEmail());
                        }
                    }
                });
                organo.getInvitados().stream().forEach(invitado -> emailsMiembrosAutorizadosInvitadosYExternos.add(invitado.getEmail()));
            });


            ArrayList<String> organosIds = new ArrayList<>();
            reunion.getReunionOrganos().stream().forEach(organo -> organosIds.add(organo.getOrganoId()));

            List<String> autorizaodsMails =
                query.from(qOrganoAutorizado).where(qOrganoAutorizado.organoId.in(organosIds)).list(qOrganoAutorizado.personaEmail);

            emailsMiembrosAutorizadosInvitadosYExternos.addAll(autorizaodsMails);
        }

        List<String> externosMails = query.from(qReunionExternos)
                .where(qReunionExternos.reunionId.in(reunion.getId()))
                .list(qReunionExternos.email);

        emailsMiembrosAutorizadosInvitadosYExternos.addAll(externosMails);

        if (reunion.getReunionInvitados() != null) {
            emailsMiembrosAutorizadosInvitadosYExternos.addAll(
                reunion.getReunionInvitados().stream().map(ReunionInvitado::getPersonaEmail).collect(Collectors.toList()));
        }

        return emailsMiembrosAutorizadosInvitadosYExternos;
    }
    public List<OrganoReunionMiembro> getOrganoReunionMiembrosPrincipalesFromSuplenteOrDelegadoVotoId(Long reunionId, Long personaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Tuple> miembrosPrincipales = query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.suplenteId.eq(personaId)
                                .or(qOrganoReunionMiembro.delegadoVotoId.eq(personaId)))).
                        list(qOrganoReunionMiembro.nombre, qOrganoReunionMiembro.miembroId);

        return miembrosOrganoReunionFromTuplas(miembrosPrincipales);
    }

    private List<OrganoReunionMiembro> miembrosOrganoReunionFromTuplas(List<Tuple> miembrosPrincipales)
    {
        ArrayList<OrganoReunionMiembro> organoReunionMiembrosPrincipales = new ArrayList<>();
        miembrosPrincipales.stream().forEach( miembroPrincipal -> {
            OrganoReunionMiembro organoReunionMiembro = new OrganoReunionMiembro();
            organoReunionMiembro.setNombre(miembroPrincipal.get(qOrganoReunionMiembro.nombre));
            organoReunionMiembro.setMiembroId(miembroPrincipal.get(qOrganoReunionMiembro.miembroId));
            organoReunionMiembrosPrincipales.add(organoReunionMiembro);
        });
        return organoReunionMiembrosPrincipales;
    }

    public boolean isPresideVotacion(
        Long reunionId,
        Long personaId
    ) {
        JPAQuery query = new JPAQuery(entityManager);

        Boolean preside = query.from(qOrganoReunionMiembro)
            .where(qOrganoReunionMiembro.reunionId.eq(reunionId).and(qOrganoReunionMiembro.miembroId.eq(personaId))).
                singleResult(qOrganoReunionMiembro.presideVotacion);
        return preside != null && preside;
    }
}
