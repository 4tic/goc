package es.uji.apps.goc.model;

import java.util.Date;
import java.util.List;

import es.uji.apps.goc.dto.PuntoOrdenDiaComentario;

public class ComentarioPuntoOrdenDia
{
    private Long id;
    private String comentario;
    private Long puntoOrdenDiaId;
    private Long comentarioSuperiorId;
    private Long creadorId;
    private String creadorNombre;
    private Date fecha;
    private List<ComentarioPuntoOrdenDia> respuestas;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public Long getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(Long puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public Long getComentarioSuperiorId()
    {
        return comentarioSuperiorId;
    }

    public void setComentarioSuperiorId(Long comentarioSuperiorId)
    {
        this.comentarioSuperiorId = comentarioSuperiorId;
    }

    public Long getCreadorId()
    {
        return creadorId;
    }

    public void setCreadorId(Long creadorId)
    {
        this.creadorId = creadorId;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getCreadorNombre()
    {
        return creadorNombre;
    }

    public void setCreadorNombre(String creadorNombre)
    {
        this.creadorNombre = creadorNombre;
    }

    public List<ComentarioPuntoOrdenDia> getRespuestas()
    {
        return respuestas;
    }

    public void setRespuestas(List<ComentarioPuntoOrdenDia> respuestas)
    {
        this.respuestas = respuestas;
    }

    public static ComentarioPuntoOrdenDia dtoToModel(PuntoOrdenDiaComentario puntoOrdenDiaComentario)
    {
        ComentarioPuntoOrdenDia comentarioPuntoOrdenDia = new ComentarioPuntoOrdenDia();
        comentarioPuntoOrdenDia.setId(puntoOrdenDiaComentario.getId());
        comentarioPuntoOrdenDia.setComentario(puntoOrdenDiaComentario.getComentario());
        comentarioPuntoOrdenDia.setCreadorId(puntoOrdenDiaComentario.getCreadorId());
        comentarioPuntoOrdenDia.setFecha(puntoOrdenDiaComentario.getFecha());
        comentarioPuntoOrdenDia.setPuntoOrdenDiaId(puntoOrdenDiaComentario.getPuntoOrdenDia().getId());
        comentarioPuntoOrdenDia.setCreadorNombre(puntoOrdenDiaComentario.getCreadorNombre());
        if(puntoOrdenDiaComentario.getComentarioPadre() != null)
        {
            comentarioPuntoOrdenDia.setComentarioSuperiorId(puntoOrdenDiaComentario.getComentarioPadre().getId());
        }
        return comentarioPuntoOrdenDia;
    }
}
