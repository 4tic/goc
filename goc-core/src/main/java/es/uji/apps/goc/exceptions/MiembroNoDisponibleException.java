package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class MiembroNoDisponibleException extends CoreDataBaseException
{
    public MiembroNoDisponibleException()
    {
        super("appI18N.excepciones.miembrorequeridoNoDisponible");
    }

    public MiembroNoDisponibleException(String message)
    {
        super(message);
    }
}
