package es.uji.apps.goc.dao;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.exceptions.MailsIncorrectosException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.exceptions.NotificacionesException;
import es.uji.apps.goc.notifications.Mensaje;

@Repository
public class NotificacionesDAO
{
    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.external.notificacionesEndpoint}")
    private String notificacionesEndpoint;

    @Value("${goc.mainLanguage}")
    public String mainLanguage;

    @Transactional
    public void enviaNotificacion(Mensaje mensaje) throws NotificacionesException, MailsIncorrectosException
    {
        WebResource resource = Client.create().resource(this.notificacionesEndpoint);

        ClientResponse response = resource
                .type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .post(ClientResponse.class, mensaje);

        if (response.getStatus() != 200 && response.getStatus() != 204) {
            if (response.getStatus() != 501)
                throw new NotificacionesException();
            else {
                ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18n", mainLanguage);
                throw new MailsIncorrectosException(resourceBundle.getString("exception.noseHaEnviadoNotificacion") + " a: " + cleanResponseFailedNotificationElements(response.getEntity(String.class)));
            }
        }
    }

    private String cleanResponseFailedNotificationElements(String elements)
    {
        return elements.replace("[\"", "\n").replace("\"]", "").replace("\",\"", "\n");
    }
}
