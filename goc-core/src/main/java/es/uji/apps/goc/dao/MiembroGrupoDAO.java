package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.QMiembroLocal;
import es.uji.apps.goc.dto.QOrganoLocal;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MiembroGrupoDAO extends BaseDAODatabaseImpl
{
    QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
    QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;

    public Long getOrdenGrupo(String nombre, Long organoId)
    {
        Long ordenGrupo = getOrdenByGrupoAndOrgano(nombre, organoId);

        if(ordenGrupo == null)
        {
            JPAQuery query = new JPAQuery(entityManager);
            ordenGrupo = query.from(qMiembroLocal).join(qMiembroLocal.organo, qOrganoLocal)
                .where(qOrganoLocal.id.eq(organoId))
                .singleResult(qMiembroLocal.ordenGrupo.max());
            return ordenGrupo != null ? ordenGrupo + 1 : 1;
        } else {
            return ordenGrupo;
        }
    }

    private Long getOrdenByGrupoAndOrgano(String nombre, Long organoId){
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.grupo.eq(nombre)
                .and(qOrganoLocal.id.eq(organoId))
            )
            .singleResult(qMiembroLocal.ordenGrupo);
    }

    @Transactional
    public void asignarGrupo(
        Long miembroId,
        String nombre,
        Long ordenGrupo
    )
    {
        MiembroLocal miembroLocal = this.get(MiembroLocal.class, miembroId).get(0);
        miembroLocal.setGrupo(nombre);
        miembroLocal.setOrdenGrupo(ordenGrupo);
        this.update(miembroLocal);
    }

    @Transactional
    public void desagrupar(List<Long> ids)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qMiembroLocal);
        update.setNull(qMiembroLocal.ordenGrupo).setNull(qMiembroLocal.grupo)
            .where(qMiembroLocal.id.in(ids))
            .execute();
    }

    public Long getOrganoIdFromMiembroId(Long miembroId)
    {
        MiembroLocal miembroLocal = this.get(MiembroLocal.class, miembroId).get(0);
        return miembroLocal.getOrgano().getId();
    }


    public void subirGrupo(
        String grupo,
        Long organoId
    )
    {
        List<MiembroLocal> miembrosGrupo = getMiembrosByGrupoAndOrgano(grupo, organoId);
        List<MiembroLocal> miembrosGrupoSuperior = getMiembrosGrupoSuperior(grupo, organoId);

        if(miembrosGrupoSuperior != null)
        {
            Long ordenGrupoAMover = miembrosGrupo.get(0).getOrdenGrupo();
            Long ordenMiembroSuperiorActual = miembrosGrupoSuperior.get(0).getOrdenGrupo();
            for (MiembroLocal miembroSuperior : miembrosGrupoSuperior)
            {
                miembroSuperior.setOrdenGrupo(ordenGrupoAMover);
                this.update(miembroSuperior);

            }

            for (MiembroLocal miembro : miembrosGrupo)
            {
                miembro.setOrdenGrupo(ordenMiembroSuperiorActual);
                this.update(miembro);
            }
        }
    }

    private List<MiembroLocal> getMiembrosByGrupoAndOrgano(String grupo, Long organoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.grupo.eq(grupo)
                .and(qOrganoLocal.id.eq(organoId))
            )
            .list(qMiembroLocal);
    }

    private List<MiembroLocal> getMiembrosGrupoSuperior(
        String grupo,
        Long organoId
    )
    {
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;
        Long ordenGrupo = getOrdenGrupo(grupo, organoId);

        if(ordenGrupo == null)
        {
            return null;
        }

        JPAQuery queryUltimoOrdenAnterior = new JPAQuery(entityManager);
        Long ultimoOrdenAnterior = queryUltimoOrdenAnterior.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.ordenGrupo.lt(ordenGrupo)
                .and(qOrganoLocal.id.eq(organoId))
            )
            .singleResult(qMiembroLocal.ordenGrupo.max());

        if(ultimoOrdenAnterior == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.ordenGrupo.eq(ultimoOrdenAnterior)
                .and(qOrganoLocal.id.eq(organoId)))
            .list(qMiembroLocal);
    }

    private List<MiembroLocal> getMiembrosGrupoInferior(
        String grupo,
        Long organoId
    )
    {
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;
        Long ordenGrupo = getOrdenGrupo(grupo, organoId);

        if(ordenGrupo == null)
        {
            return null;
        }

        JPAQuery queryUltimoOrdenPosterior = new JPAQuery(entityManager);
        Long ultimoOrdenPosterior = queryUltimoOrdenPosterior.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.ordenGrupo.gt(ordenGrupo)
                .and(qOrganoLocal.id.eq(organoId))
            )
            .singleResult(qMiembroLocal.ordenGrupo.max());

        if(ultimoOrdenPosterior == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.ordenGrupo.eq(ultimoOrdenPosterior)
                .and(qOrganoLocal.id.eq(organoId)))
            .list(qMiembroLocal);
    }

    public void bajarGrupo(
        String grupo,
        Long organoId
    )
    {
        List<MiembroLocal> miembrosGrupo = getMiembrosByGrupoAndOrgano(grupo, organoId);
        List<MiembroLocal> miembrosGrupoInferior = getMiembrosGrupoInferior(grupo, organoId);

        if(miembrosGrupoInferior != null)
        {
            Long ordenGrupoAMover = miembrosGrupo.get(0).getOrdenGrupo();
            Long ordenMiembroInferiorActual = miembrosGrupoInferior.get(0).getOrdenGrupo();
            for (MiembroLocal miembroInferior : miembrosGrupoInferior)
            {
                miembroInferior.setOrdenGrupo(ordenGrupoAMover);
                this.update(miembroInferior);

            }

            for (MiembroLocal miembro : miembrosGrupo)
            {
                miembro.setOrdenGrupo(ordenMiembroInferiorActual);
                this.update(miembro);
            }
        }
    }
}
