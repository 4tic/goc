package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class PuntoOrdenDiaNoDisponibleException extends CoreDataBaseException
{
    public PuntoOrdenDiaNoDisponibleException()
    {
        super("appI18N.excepciones.puntoNodisponible");
    }

    public PuntoOrdenDiaNoDisponibleException(String message)
    {
        super(message);
    }
}
