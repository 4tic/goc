package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class OrganoConvocadoNoPermitidoException extends CoreDataBaseException
{
    public OrganoConvocadoNoPermitidoException()
    {
        super("appI18N.excepciones.permisosInsuficientesParaconvocar");
    }

    public OrganoConvocadoNoPermitidoException(String message)
    {
        super(message);
    }
}
