package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.StringPath;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import es.uji.apps.goc.dto.QPuntoOrdenDia;
import es.uji.apps.goc.dto.QResultadosTodosVotosDTO;
import es.uji.apps.goc.dto.QVotantePrivadoDTO;
import es.uji.apps.goc.dto.QVotoPrivadoDTO;
import es.uji.apps.goc.dto.QVotoPublicoDTO;
import es.uji.apps.goc.dto.QVotosTodosDTO;
import es.uji.apps.goc.dto.VotantePrivadoDTO;
import es.uji.apps.goc.dto.VotoPublicoDTO;
import es.uji.apps.goc.model.ResultadoVotos;
import es.uji.apps.goc.model.VoteType;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VotoDAO extends BaseDAODatabaseImpl {

    private QVotoPublicoDTO qVotoPublicoDTO = QVotoPublicoDTO.votoPublicoDTO;
    private QVotantePrivadoDTO qVotantePrivadoDTO = QVotantePrivadoDTO.votantePrivadoDTO;
    private QVotoPrivadoDTO qVotoPrivadoDTO = QVotoPrivadoDTO.votoPrivadoDTO;
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QResultadosTodosVotosDTO qResultadosTodosVotosDTO = QResultadosTodosVotosDTO.resultadosTodosVotosDTO;
    private QVotosTodosDTO qVotosTodosDTO = QVotosTodosDTO.votosTodosDTO;

    public Long getVotosAFavor(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPublicoDTO)
                .where(qVotoPublicoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString())
                        .and(qVotoPublicoDTO.voto.eq(VoteType.FAVOR.name()))).count();
    }

    public Long getVotosEnContra(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPublicoDTO)
                .where(qVotoPublicoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString())
                        .and(qVotoPublicoDTO.voto.eq(VoteType.CONTRA.name()))).count();
    }

    public Long getVotosAbstencion(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPublicoDTO)
                .where(qVotoPublicoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString())
                        .and(qVotoPublicoDTO.voto.eq(VoteType.ABSTENCION.name()))).count();
    }

    public List<VotoPublicoDTO> getVotosProvisionales(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPublicoDTO)
                .where(qVotoPublicoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString()))
                .list(qVotoPublicoDTO);
    }

    public List<String> getIdsPuntosWhereUserHasVotedPublic(Long reunionId, Long connectedUserId, Long personaId)
    {
        return getIdsPuntosWhereUserHasVoted(qVotoPublicoDTO, reunionId, connectedUserId, personaId);
    }

    public List<String> getIdsPuntosWhereUserHasVotedPrivate(Long reunionId, Long connectedUserId, Long personaId)
    {
        return getIdsPuntosWhereUserHasVoted(qVotantePrivadoDTO, reunionId, connectedUserId, personaId);
    }

    private List<String> getIdsPuntosWhereUserHasVoted(EntityPath entity, Long reunionId, Long connectedUserId, Long personaId)
    {
        BooleanExpression votoDelegado;
        StringPath votoEnNombreDePersonaId = new StringPath(entity, "votoEnNombreDePersonaId");
        if (!personaId.equals(connectedUserId)) {
            votoDelegado = votoEnNombreDePersonaId.eq(personaId.toString());
        }
        else {
            votoDelegado = votoEnNombreDePersonaId.isNull();
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(entity)
            .where(new StringPath(entity, "reunionId").eq(reunionId.toString())
                .and(new StringPath(entity, "personaId").eq(connectedUserId.toString())).and(votoDelegado)).list(new StringPath(entity, "puntoOrdenDiaId"));
    }


    public List<VotoPublicoDTO> getVotosPublicosUsuarioEnReunion(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPublicoDTO)
                .where(qVotoPublicoDTO.reunionId.eq(reunionId.toString())
                        .and(qVotoPublicoDTO.personaId.eq(connectedUserId.toString()))).list(qVotoPublicoDTO);

    }

    public List<VotantePrivadoDTO> getVotosPrivadosUsuarioEnReunion(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotantePrivadoDTO)
                .where(qVotantePrivadoDTO.reunionId.eq(reunionId.toString())
                        .and(qVotantePrivadoDTO.personaId.eq(connectedUserId.toString()))).list(qVotantePrivadoDTO);
    }

    public Long getVotosPrivadosAFavor(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPrivadoDTO)
                .where(qVotoPrivadoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString())
                        .and(qVotoPrivadoDTO.voto.eq(VoteType.FAVOR.name()))).count();
    }

    public Long getVotosPrivadosAContra(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPrivadoDTO)
                .where(qVotoPrivadoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString())
                        .and(qVotoPrivadoDTO.voto.eq(VoteType.CONTRA.name()))).count();
    }

    public Long getVotosPrivadosAbstencion(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotoPrivadoDTO)
                .where(qVotoPrivadoDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString())
                        .and(qVotoPrivadoDTO.voto.eq(VoteType.ABSTENCION.name()))).count();
    }

    public ResultadoVotos getResultadoVotosPunto(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<Tuple> numeroVotosPunto = query.from(qResultadosTodosVotosDTO)
            .where(qResultadosTodosVotosDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString()))
            .groupBy(qResultadosTodosVotosDTO.voto).list(qResultadosTodosVotosDTO.voto, qResultadosTodosVotosDTO.numeroVotos.sum());

        return resultadosVotosFromTupla(puntoOrdenDiaId, numeroVotosPunto);
    }

    private ResultadoVotos resultadosVotosFromTupla(Long puntoOrdenDiaId, List<Tuple> numeroVotosPunto)
    {
        Long votosFavor = 0L;
        Long votosContra = 0L;
        Long votoAbstencion = 0L;

        for (Tuple votos : numeroVotosPunto) {
            String sentidoVoto = votos.get(qResultadosTodosVotosDTO.voto);
            if (sentidoVoto.equals(VoteType.FAVOR.name())) {
                votosFavor = votos.get(1, Long.class);
            } else if (sentidoVoto.equals(VoteType.CONTRA.name())) {
                votosContra = votos.get(1, Long.class);
            } else {
                votoAbstencion = votos.get(1, Long.class);
            }
        }

        return new ResultadoVotos(
                puntoOrdenDiaId,
                votosFavor,
                votosContra,
                votoAbstencion
        );
    }

    public List<ResultadoVotos> getResultadoVotosTodosPuntosReunion(Long reunionId)
    {

        JPAQuery query = new JPAQuery(entityManager);
        List<Tuple> numeroVotosPunto = query.from(qResultadosTodosVotosDTO)
                .where(qResultadosTodosVotosDTO.reunionId.eq(reunionId.toString()))
                .list(qResultadosTodosVotosDTO.puntoOrdenDiaId, qResultadosTodosVotosDTO.voto, qResultadosTodosVotosDTO.numeroVotos);

        return resultadosTodosVotosReunionFromTupla(numeroVotosPunto,  reunionId);
    }

    public List<ResultadoVotos> resultadosTodosVotosReunionFromTupla(List<Tuple> resultadosTodosPuntos, Long reunionId){
        HashMap<Long, ResultadoVotos> resultadosTodosPuntosTransformados = new HashMap<Long,ResultadoVotos>();

        for (Tuple resultadosPunto : resultadosTodosPuntos) {
            String idPunto = resultadosPunto.get(qResultadosTodosVotosDTO.puntoOrdenDiaId);
            String tipoVoto = resultadosPunto.get(qResultadosTodosVotosDTO.voto);

            if(!resultadosTodosPuntosTransformados.containsKey(Long.parseLong(idPunto))){
                ResultadoVotos resultadoVotos = getResultadoVotoFromTuple(resultadosPunto, idPunto, tipoVoto);
                resultadosTodosPuntosTransformados.put(Long.parseLong(idPunto),resultadoVotos);
            } else {
                ResultadoVotos resultadoVotos = resultadosTodosPuntosTransformados.get(Long.parseLong(idPunto));
                setSentidosVoto(resultadosPunto, tipoVoto, resultadoVotos);
            }
        }
        ArrayList<ResultadoVotos> resultadoVotos = new ArrayList<>(resultadosTodosPuntosTransformados.values());
        resultadoVotos.addAll(getResultadosVotosFromPuntosSinVotosenReunion(reunionId, resultadosTodosPuntosTransformados.keySet()));
        return resultadoVotos;
    }

    private List<ResultadoVotos> getResultadosVotosFromPuntosSinVotosenReunion(Long reunionId, Set<Long> puntosConVotos)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<Long> idsPuntosSinvotos = query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId)
                        .and(qPuntoOrdenDia.id.notIn(puntosConVotos)))
                .list(qPuntoOrdenDia.id);
        return getResultadoVotosInitial(idsPuntosSinvotos);
    }

    public List<ResultadoVotos> getResultadoVotosInitial(List<Long> puntosId)
    {
        ArrayList<ResultadoVotos> puntosResultadosInitialized= new ArrayList<>();
        for (Long puntoId : puntosId) {
            ResultadoVotos resultadoVotos = new ResultadoVotos(
                    puntoId,
                    0L,
                    0L,
                    0L
            );
            puntosResultadosInitialized.add(resultadoVotos);
        }
        return puntosResultadosInitialized;
    }


    private ResultadoVotos getResultadoVotoFromTuple(Tuple resultadosPunto, String idPunto, String tipoVoto)
    {
        Long votosFavor = 0L;
        Long votosContra = 0L;
        Long votoAbstencion = 0L;

        if (tipoVoto.equals(VoteType.FAVOR.name())) {
            votosFavor = resultadosPunto.get(qResultadosTodosVotosDTO.numeroVotos);
        } else if (tipoVoto.equals(VoteType.CONTRA.name())) {
            votosContra = resultadosPunto.get(qResultadosTodosVotosDTO.numeroVotos);
        } else {
            votoAbstencion = resultadosPunto.get(qResultadosTodosVotosDTO.numeroVotos);
        }
        return new ResultadoVotos(
                Long.parseLong(idPunto),
                votosFavor,
                votosContra,
                votoAbstencion
        );
    }

    private void setSentidosVoto(Tuple resultadosPunto, String tipoVoto, ResultadoVotos resultadoVotos)
    {
        if (tipoVoto.equals(VoteType.FAVOR.name())) {
            resultadoVotos.setaFavor(resultadosPunto.get(qResultadosTodosVotosDTO.numeroVotos));
        } else if (tipoVoto.equals(VoteType.CONTRA.name())) {
            resultadoVotos.setEnContra(resultadosPunto.get(qResultadosTodosVotosDTO.numeroVotos));
        } else {
            resultadoVotos.setAbstenciones(resultadosPunto.get(qResultadosTodosVotosDTO.numeroVotos));
        }
    }

    public Boolean puntoHasVotos(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotosTodosDTO).
            where(qVotosTodosDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString()))
            .exists();
    }

    public Boolean reunionHasVotos(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotosTodosDTO).
            where(qVotosTodosDTO.reunionId.eq(reunionId.toString()))
            .exists();
    }

    public boolean existsVote(Long puntoOrdenDiaId, String miembrohaDelegadoOesSustituidoId, Long votanteId)
    {
        BooleanExpression whereMiembroSustituidoIsEqual;
        if(miembrohaDelegadoOesSustituidoId != null && !miembrohaDelegadoOesSustituidoId.equals(votanteId.toString())){
            whereMiembroSustituidoIsEqual = qVotosTodosDTO.votoEnNombrePersonaId.eq(miembrohaDelegadoOesSustituidoId);
        }
        else {
            whereMiembroSustituidoIsEqual = qVotosTodosDTO.votoEnNombrePersonaId.isNull().or(qVotosTodosDTO.votoEnNombrePersonaId.eq(votanteId.toString()));
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotosTodosDTO).
            where(qVotosTodosDTO.puntoOrdenDiaId.eq(puntoOrdenDiaId.toString()).
                and(qVotosTodosDTO.personaId.eq(votanteId.toString())).and(whereMiembroSustituidoIsEqual))
            .exists();
    }

    public boolean existsVoterOnReunion(Long reunionId, Long votanteId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVotosTodosDTO).
            where(qVotosTodosDTO.reunionId.eq(reunionId.toString()).
                and(qVotosTodosDTO.personaId.eq(votanteId.toString()))).exists();
    }
}
