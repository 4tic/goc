package es.uji.apps.goc.dto;

import es.uji.apps.goc.HTMLUtils;
import java.util.List;

import es.uji.apps.goc.model.ComentarioPuntoOrdenDia;

public class PuntoOrdenDiaTemplate
{
    private Long id;

    private String titulo;

    private String descripcion;

    private Long orden;

    private String acuerdos;

    private String deliberaciones;

    private boolean publico;

    private List<DocumentoTemplate> documentos;

    private List<DocumentoTemplate> documentosAcuerdos;

    private List<DescriptorTemplate> descriptores;

    private String urlActa;

    private String urlActaAnterior;

    private List<PuntoOrdenDiaTemplate> subpuntos;

    private List<ComentarioPuntoOrdenDia> comentarios;

    private Boolean votoPublico;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescripcion()
    {
        return (descripcion != null) ? HTMLUtils.forceCloseHTMLTags(descripcion) : descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getAcuerdos()
    {
        return (acuerdos != null) ? HTMLUtils.forceCloseHTMLTags(acuerdos) : acuerdos;
    }

    public void setAcuerdos(String acuerdos)
    {
        this.acuerdos = acuerdos;
    }

    public String getDeliberaciones()
    {
        return (deliberaciones != null) ? HTMLUtils.forceCloseHTMLTags(deliberaciones) : deliberaciones;
    }

    public void setDeliberaciones(String deliberaciones)
    {
        this.deliberaciones = deliberaciones;
    }

    public List<DocumentoTemplate> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoTemplate> documentos) {
        this.documentos = documentos;
    }

    public boolean isPublico() {
        return publico;
    }

    public void setPublico(boolean publico) {
        this.publico = publico;
    }

    public List<DocumentoTemplate> getDocumentosAcuerdos()
    {
        return documentosAcuerdos;
    }

    public void setDocumentosAcuerdos(List<DocumentoTemplate> documentosAcuerdos)
    {
        this.documentosAcuerdos = documentosAcuerdos;
    }

    public List<DescriptorTemplate> getDescriptores()
    {
        return descriptores;
    }

    public void setDescriptores(List<DescriptorTemplate> descriptores)
    {
        this.descriptores = descriptores;
    }

    public String getUrlActa()
    {
        return urlActa;
    }

    public void setUrlActa(String urlActa)
    {
        this.urlActa = urlActa;
    }

    public List<PuntoOrdenDiaTemplate> getSubpuntos()
    {
        return subpuntos;
    }

    public void setSubpuntos(List<PuntoOrdenDiaTemplate> subpuntos)
    {
        this.subpuntos = subpuntos;
    }

    public List<ComentarioPuntoOrdenDia> getComentarios()
    {
        return comentarios;
    }

    public void setComentarios(List<ComentarioPuntoOrdenDia> comentarios)
    {
        this.comentarios = comentarios;
    }

    public String getUrlActaAnterior()
    {
        return urlActaAnterior;
    }

    public void setUrlActaAnterior(String urlActaAnterior)
    {
        this.urlActaAnterior = urlActaAnterior;
    }

    public Boolean getVotoPublico()
    {
        return votoPublico;
    }

    public void setVotoPublico(Boolean votoPublico)
    {
        this.votoPublico = votoPublico;
    }
}
