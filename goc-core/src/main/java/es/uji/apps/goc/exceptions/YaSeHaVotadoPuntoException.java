package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class YaSeHaVotadoPuntoException extends CoreDataBaseException {
    public YaSeHaVotadoPuntoException()
    {
        super("appI18N.excepciones.YaSeHaVotadoPunto");
    }

    public YaSeHaVotadoPuntoException(String message)
    {
        super(message);
    }

}
