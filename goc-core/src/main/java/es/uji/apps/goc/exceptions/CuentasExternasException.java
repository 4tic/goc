package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class CuentasExternasException extends CoreDataBaseException
{
    public CuentasExternasException()
    {
        super("appI18N.excepciones.personaCuenta");
    }

    public CuentasExternasException(String message)
    {
        super(message);
    }
}
