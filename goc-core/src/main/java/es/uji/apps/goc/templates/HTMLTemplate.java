package es.uji.apps.goc.templates;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

public class HTMLTemplate extends GenericTemplate implements Template
{
    private final Locale locale;
    private String application;

    private HTMLTemplate(String name, Locale locale, String path)
    {
        super(name, path);

        this.locale = locale;
    }

    public HTMLTemplate(String name, CommonPropertiesConfig commonPropertiesConfig)
    {
        this(name, new Locale("ca"), commonPropertiesConfig.getTemplatesHtmlPath());

        this.put("appContext", commonPropertiesConfig.getAppContext());
        this.put("charset", commonPropertiesConfig.getCharset());
        this.put("publicUrl", commonPropertiesConfig.getPublicUrl());
        this.put("customCSS", (commonPropertiesConfig.getCustomCSS() != null) ? commonPropertiesConfig.getCustomCSS() : "");
        this.put("logo", commonPropertiesConfig.getLogo());
        this.put("logoPublic", (commonPropertiesConfig.getLogoPublic() != null) ? commonPropertiesConfig.getLogoPublic() : commonPropertiesConfig.getLogo());
        this.put("appTitle", commonPropertiesConfig.getAppTitle());
        this.put("staticsUrl", commonPropertiesConfig.getStaticsUrl());
    }

    @Override
    public byte[] process()
    {
        TemplateEngine templateEngine = TemplateEngineFactory.getTemplateEngine("HTML5",
            this.path, ".html", application);

        Context context = new Context(locale);
        context.setVariables(properties);

        return templateEngine.process(name, context).getBytes();
    }
}