package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ReunionNoConvocadaException extends CoreDataBaseException
{
    public ReunionNoConvocadaException()
    {
        super("");
    }
}
