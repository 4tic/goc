package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class OrganosExternosException extends CoreDataBaseException
{
    public OrganosExternosException()
    {
        super("appI18N.excepciones.organosExternos");
    }

    public OrganosExternosException(String message)
    {
        super(message);
    }
}
