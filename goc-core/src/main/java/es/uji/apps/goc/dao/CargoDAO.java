package es.uji.apps.goc.dao;

import com.google.common.base.Strings;

import com.mysema.query.jpa.impl.JPAQuery;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.adapter.CargoAdapter;
import es.uji.apps.goc.dto.QCargo;
import es.uji.apps.goc.exceptions.CargosExternonException;
import es.uji.apps.goc.model.Cargo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CargoDAO extends BaseDAODatabaseImpl
{
    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.external.cargosEndpoint:}")
    private String cargosExternosEndpoint;

    private List<Cargo> getCargosExternos()
        throws CargosExternonException
    {
        WebResource getOrganosResource =
            Client.create().resource(this.cargosExternosEndpoint);

        ClientResponse response = getOrganosResource.type(MediaType.APPLICATION_JSON)
            .header("X-UJI-AuthToken", authToken)
            .get(ClientResponse.class);

        if (response.getStatus() != 200)
        {
            throw new CargosExternonException(response.toString());
        }

        return response.getEntity(new GenericType<List<Cargo>>() {
        });
    }

    public String getCargoCodigoById(String cargoId)
    {
        Optional<Cargo> cargo = getCargoById(cargoId);
        return cargo.map(Cargo::getCodigo).orElse(null);
    }

    public Optional<Cargo> getCargoById(String cargoId) {
        return getCargos().stream().filter(cargo -> cargo.getId().equals(cargoId)).findAny();
    }

    public List<Cargo> getCargos() {
        if (!Strings.isNullOrEmpty(cargosExternosEndpoint)) {
            try {
                return getCargosExternos();
            } catch (CargosExternonException e) {
                return Collections.emptyList();
            }
        }
        else {
            QCargo qCargo = QCargo.cargo;
            JPAQuery query = new JPAQuery(entityManager);

            return query.from(qCargo).list(qCargo).stream().map(CargoAdapter::fromEntity).collect(Collectors.toList());
        }
    }
}
