package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class SinPermisoException extends CoreDataBaseException
{
    public SinPermisoException()
    {
        super("appI18N.excepciones.sinPermiso");
    }

    public SinPermisoException(String message)
    {
        super(message);
    }
}
