package es.uji.apps.goc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.Miembro;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MiembroFirma implements Firmante
{
    private String id;

    private String nombre;

    private String email;

    private String suplente;
    private Long suplenteId;
    private Boolean asistencia;
    private String delegadoVoto;
    private Long delegadoVotoId;

    private Cargo cargo;
    private String cargoNombreDescripcion;

    private List<String> delegacionesDeVoto;
    private String nombresDelegacionesDeVoto;
    private Boolean justificaAusencia;
    private Long organoReunionMiembroId;
    private Boolean firmante;

    public MiembroFirma (Miembro miembro){
        this.id = miembro.getId().toString();
        this.nombre = miembro.getNombre();
        this.email = miembro.getEmail();
        this.firmante = miembro.getFirmante();
        this.cargo = miembro.getCargo();
        this.cargoNombreDescripcion = miembro.getCargo().getNombre();
    }



    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public MiembroFirma()
    {
    }

    public Cargo getCargo()
    {
        return cargo;
    }

    public void setCargo(Cargo cargo)
    {
        this.cargo = cargo;
    }

    public String getSuplente()
    {
        return suplente;
    }

    public void setSuplente(String suplente)
    {
        this.suplente = suplente;
    }

    public Long getSuplenteId()
    {
        return suplenteId;
    }

    public void setSuplenteId(Long suplenteId)
    {
        this.suplenteId = suplenteId;
    }

    public String getDelegadoVoto()
    {
        return delegadoVoto;
    }

    public void setDelegadoVoto(String delegadoVoto)
    {
        this.delegadoVoto = delegadoVoto;
    }

    public Long getDelegadoVotoId()
    {
        return delegadoVotoId;
    }

    public void setDelegadoVotoId(Long delegadoVotoId)
    {
        this.delegadoVotoId = delegadoVotoId;
    }

    public List<String> getDelegacionesDeVoto()
    {
        return delegacionesDeVoto;
    }

    public void setDelegacionesDeVoto(List<String> delegacionesDeVoto)
    {
        this.delegacionesDeVoto = delegacionesDeVoto;
    }

    public void addDelegacionDeVoto(String delegadoVoto)
    {
        if (this.delegacionesDeVoto == null) delegacionesDeVoto = new ArrayList<>();

        delegacionesDeVoto.add(delegadoVoto);
    }

    public String getNombresDelegacionesDeVoto()
    {
        return nombresDelegacionesDeVoto;
    }

    public void setNombresDelegacionesDeVoto(String nombresDelegacionesDeVoto)
    {
        this.nombresDelegacionesDeVoto = nombresDelegacionesDeVoto;
    }

    public void buildNombresDelegacionesDeVoto()
    {
        if (this.delegacionesDeVoto == null || this.delegacionesDeVoto.isEmpty())
        {
            nombresDelegacionesDeVoto = "";
            return;
        }

        nombresDelegacionesDeVoto = StringUtils.join(this.delegacionesDeVoto, ", ");
    }

    public Boolean getAsistencia()
    {
        return asistencia;
    }

    public void setAsistencia(Boolean asistencia)
    {
        this.asistencia = asistencia;
    }

    public void setJustificaAusencia(Boolean justificaAusencia)
    {
        this.justificaAusencia = justificaAusencia;
    }

    public Boolean getJustificaAusencia()
    {
        return justificaAusencia;
    }

    public Boolean getFirmante() {
        return this.firmante != null && this.firmante == true;
    }

    public void setFirmante(Boolean firmante) {
        this.firmante = firmante;
    }

    public boolean isFirmante() {
        return this.getFirmante() != null ? this.getFirmante() : false;
    }

    public String getCargoNombreDescripcion() {
        return this.cargoNombreDescripcion;
    }

    public String getCargoCodigo() {
        return this.cargo != null ? this.cargo.getCodigo() : null;
    }

    public void setCargoNombreDescripcion(String cargoNombreDescripcion) {
        this.cargoNombreDescripcion = cargoNombreDescripcion;
    }

    public Long getOrganoReunionMiembroId()
    {
        return organoReunionMiembroId;
    }

    public void setOrganoReunionMiembroId(Long organoReunionMiembroId)
    {
        this.organoReunionMiembroId = organoReunionMiembroId;
    }
}


