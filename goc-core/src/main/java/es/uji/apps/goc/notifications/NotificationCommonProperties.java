package es.uji.apps.goc.notifications;

import es.uji.apps.goc.templates.CommonPropertiesConfig;

public class NotificationCommonProperties implements CommonPropertiesConfig {
    private final String templatesPath;
    private final String publicUrl;
    private final String appContext;

    public NotificationCommonProperties(String templatesPath, String publicUrl, String appContext) {
        this.templatesPath = templatesPath;
        this.publicUrl = publicUrl;
        this.appContext = appContext;
    }

    @Override
    public String getAppContext() {
        return appContext;
    }

    @Override
    public String getAppTitle() {
        return null;
    }

    @Override
    public String getTemplatesHtmlPath() {
        return templatesPath;
    }

    @Override
    public String getCharset() {
        return null;
    }

    @Override
    public String getPublicUrl() {
        return publicUrl;
    }

    @Override
    public String getCustomCSS() {
        return null;
    }

    @Override
    public String getLogo() {
        return null;
    }

    @Override
    public String getLogoPublic() {
        return null;
    }

    @Override
    public String getStaticsUrl() {
        return null;
    }
}
