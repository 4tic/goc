package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class AsistenteNoEncontradoException extends CoreDataBaseException
{
    public AsistenteNoEncontradoException()
    {
        super("appI18N.excepciones.asistenteNoEncontrado");
    }

    public AsistenteNoEncontradoException(String message)
    {
        super(message);
    }
}
