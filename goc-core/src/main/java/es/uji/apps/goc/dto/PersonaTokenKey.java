package es.uji.apps.goc.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersonaTokenKey implements Serializable
{
    @Column(name = "ID_PERSONA")
    private long idPersona;

    @Column(name = "TOKEN")
    private String token;

    public long getIdPersona()
    {
        return idPersona;
    }

    public void setIdPersona(long idPersona)
    {
        this.idPersona = idPersona;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }
}
