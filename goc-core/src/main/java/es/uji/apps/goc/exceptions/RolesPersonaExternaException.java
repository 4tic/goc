package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class RolesPersonaExternaException extends CoreDataBaseException
{
    public RolesPersonaExternaException()
    {
        super("appI18N.excepciones.rolesPersona");
    }

    public RolesPersonaExternaException(String message)
    {
        super(message);
    }
}
