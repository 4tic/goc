package es.uji.apps.goc.dto;

import java.util.Date;

public interface Documentable
{
    Long getId();
    String getDescripcion();
    String getMimeType();
    String getNombreFichero();
    Date getFechaAdicion();
    byte[] getDatos();
    Long getCreadorId();
    PuntoOrdenDia getPuntoOrdenDia();
    String getDescripcionAlternativa();
    Boolean getPublico();
    String getUrlActa();
    String getUrlActaAlternativa();
}
