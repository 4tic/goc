package es.uji.apps.goc.exceptions;

public class FirmantesNecesariosException extends Exception {
    public FirmantesNecesariosException()
    {
        super("appI18N.exception.firmanteNecesario");
    }
}
