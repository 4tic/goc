package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ReunionYaCompletadaException extends CoreDataBaseException
{
    public ReunionYaCompletadaException()
    {
        super("appI18N.excepciones.reunionYaCompletada");
    }

    public ReunionYaCompletadaException(String message)
    {
        super(message);
    }
}
