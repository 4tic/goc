package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class UsuarioYaHaVotadoException extends CoreDataBaseException {
    public UsuarioYaHaVotadoException()
    {
        super("appI18N.excepciones.usuarioYaHaVotado");
    }

    public UsuarioYaHaVotadoException(String message)
    {
        super(message != null ? message : "appI18N.excepciones.usuarioYaHaVotado");
    }

}
