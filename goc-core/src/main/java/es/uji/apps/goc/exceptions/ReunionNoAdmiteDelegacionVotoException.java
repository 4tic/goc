package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ReunionNoAdmiteDelegacionVotoException extends CoreDataBaseException
{
    public ReunionNoAdmiteDelegacionVotoException()
    {
        super("appI18N.excepciones.reunionNoDelegancia");
    }

    public ReunionNoAdmiteDelegacionVotoException(String message)
    {
        super(message);
    }
}
