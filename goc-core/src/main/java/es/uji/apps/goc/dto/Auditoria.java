package es.uji.apps.goc.dto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="GOC_AUDITORIA")
public class Auditoria {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="RESPONSABLE")
    private String responsable;
    @Column(name="USUARIO_DESTINO")
    private String usuarioDestino;
    @Column(name="REUNION_ID")
    private Long reunionId;
    @Column(name="ACCION")
    private String accion;
    @Column(name = "FECHA_ACCION")
    private Date fechaAccion;

    public Auditoria(String responsable, String usuarioDestino, Long reunionId, String accion) {
        this.responsable = responsable;
        this.usuarioDestino = usuarioDestino;
        this.reunionId = reunionId;
        this.accion = accion;
        this.fechaAccion = new Date();
    }

    public Auditoria() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getUsuarioDestino() {
        return usuarioDestino;
    }

    public void setUsuarioDestino(String usuarioDestino) {
        this.usuarioDestino = usuarioDestino;
    }

    public Long getReunionId() {
        return reunionId;
    }

    public void setReunionId(Long reunionId) {
        this.reunionId = reunionId;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Date getFechaAccion() {
        return fechaAccion;
    }

    public void setFechaAccion(Date fechaAccion) {
        this.fechaAccion = fechaAccion;
    }
}
