package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class VotacionNoAbiertaException extends CoreDataBaseException {
    public VotacionNoAbiertaException()
    {
        super("appI18N.votos.votacionNoAbierta");
    }

    public VotacionNoAbiertaException(String message)
    {
        super(message);
    }

}
