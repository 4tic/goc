package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;

import java.util.List;

import es.uji.apps.goc.dto.EnvioOficio;
import es.uji.apps.goc.dto.QEnvioOficio;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.apps.goc.dto.QOrganoReunionMiembro;
import es.uji.apps.goc.dto.QPuntoOrdenDia;
import es.uji.apps.goc.dto.QReunion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OficioDAO extends BaseDAODatabaseImpl
{

    QEnvioOficio qEnvioOficio = QEnvioOficio.envioOficio;
    QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    QReunion qReunion = QReunion.reunion;
    QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
    QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;

    public List<EnvioOficio> getOficios(
        Long connectedUserId
    )
    {
        com.mysema.query.types.expr.BooleanExpression where = (qOrganoReunionMiembro.miembroId.eq(connectedUserId)
            .or(qReunion.creadorId.eq(connectedUserId)));

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEnvioOficio)
            .join(qEnvioOficio.puntoOrdenDia, qPuntoOrdenDia)
            .join(qPuntoOrdenDia.reunion, qReunion)
            .join(qReunion.reunionOrganos, qOrganoReunion)
            .join(qOrganoReunion.miembros, qOrganoReunionMiembro)
            .where(where)
            .orderBy(qEnvioOficio.fechaEnvio.desc())
            .distinct()
            .list(qEnvioOficio);
    }

}
