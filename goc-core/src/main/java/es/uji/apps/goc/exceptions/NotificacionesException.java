package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class NotificacionesException extends CoreDataBaseException
{
    public NotificacionesException()
    {
        super("appI18N.excepciones.NoseHaEnviadoNotificacion");
    }

    public NotificacionesException(String message)
    {
        super(message);
    }
}
