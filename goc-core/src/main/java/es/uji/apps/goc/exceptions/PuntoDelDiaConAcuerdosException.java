package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class PuntoDelDiaConAcuerdosException extends CoreDataBaseException
{
    public PuntoDelDiaConAcuerdosException()
    {
        super("appI18N.excepciones.puntoConAcuerdos");
    }
}
