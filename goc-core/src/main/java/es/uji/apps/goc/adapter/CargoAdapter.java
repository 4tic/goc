package es.uji.apps.goc.adapter;

import es.uji.apps.goc.model.Cargo;

public class CargoAdapter {
    public static Cargo fromEntity(es.uji.apps.goc.dto.Cargo entity) {
        Cargo cargo = new Cargo();
        cargo.setId(entity.getId().toString());
        cargo.setCodigo(entity.getCodigo());
        cargo.setNombre(entity.getNombre());
        cargo.setNombreAlternativo(entity.getNombreAlternativo());
        cargo.setResponsableActa(entity.isResponsableActa());

        return cargo;
    }
}
