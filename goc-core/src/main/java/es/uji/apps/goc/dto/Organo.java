package es.uji.apps.goc.dto;

public interface Organo {
    String getId();
    String getNombre();
    String getNombreAlternativo();
}
