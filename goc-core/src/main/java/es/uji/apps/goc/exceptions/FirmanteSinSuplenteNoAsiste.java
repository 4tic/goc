package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class FirmanteSinSuplenteNoAsiste extends CoreBaseException {
    public FirmanteSinSuplenteNoAsiste(String message){
        super(message);
    }
}
