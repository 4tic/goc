package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class OrganoNoDisponibleException extends CoreDataBaseException
{
    public OrganoNoDisponibleException()
    {
        super("appI18N.excepciones.organoNoDisponible");
    }
    public OrganoNoDisponibleException(String message)
    {
        super(message);
    }
}
