package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;

import java.util.List;

import es.uji.apps.goc.dto.OrganoAutorizado;
import es.uji.apps.goc.dto.QOrganoAutorizado;
import es.uji.apps.goc.dto.QOrganoLocal;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrganoAutorizadoDAO extends BaseDAODatabaseImpl
{
    QOrganoAutorizado qOrganoAutorizado = QOrganoAutorizado.organoAutorizado;
    QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
    QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;

    public List<OrganoAutorizado> getAutorizadosByUserId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoAutorizado)
                .where(qOrganoAutorizado.personaId.eq(connectedUserId))
                .list(qOrganoAutorizado);
    }

    public boolean existAutorizadosByUserId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoAutorizado)
            .where(qOrganoAutorizado.personaId.eq(connectedUserId))
            .exists();
    }

    public List<OrganoAutorizado> getAutorizadosByOrgano(String organoId, Boolean externo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoAutorizado)
                .where(qOrganoAutorizado.organoId.eq(organoId).and(qOrganoAutorizado.organoExterno.eq(externo)))
                .list(qOrganoAutorizado);
    }

    public List<OrganoAutorizado> getAutorizadosByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPAQuery queryOrganos = new JPAQuery(entityManager);

        List<String> organosId = queryOrganos.from(qOrganoReunion)
                .where(qOrganoReunion.reunion.id.eq(reunionId))
                .list(qOrganoReunion.organoId);

        return query.from(qOrganoAutorizado).
                where(qOrganoAutorizado.organoId.in(organosId)).list(qOrganoAutorizado);
    }

    public Boolean isAutorizado(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoAutorizado)
                .join(qOrganoLocal).on(qOrganoAutorizado.organoId.castToNum(Long.class).eq(qOrganoLocal.id))
                .where(qOrganoLocal.inactivo.isFalse()
                        .and(qOrganoAutorizado.personaId.eq(personaId))).exists();
    }
}
