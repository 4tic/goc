package es.uji.apps.goc.model;

public enum VoteType {
    FAVOR, CONTRA, ABSTENCION
}
