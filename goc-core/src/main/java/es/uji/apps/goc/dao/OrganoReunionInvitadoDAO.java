package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.uji.apps.goc.dto.OrganoReunionInvitado;
import es.uji.apps.goc.dto.QOrganoReunionInvitado;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrganoReunionInvitadoDAO extends BaseDAODatabaseImpl
{
    private QOrganoReunionInvitado qOrganoReunionInvitado = QOrganoReunionInvitado.organoReunionInvitado;

    @Autowired
    OrganoReunionDAO organoReunionDAO;

    @Transactional
    public void deleteAllByReunionId(Long reunionId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qOrganoReunionInvitado);

        deleteClause.where(qOrganoReunionInvitado.reunionId.eq(reunionId)).execute();
    }

    @Transactional
    public List<OrganoReunionInvitado> getInvitadosOrganoByReunionId(Long reunionId){
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionInvitado).where(qOrganoReunionInvitado.reunionId.eq(reunionId)).list(qOrganoReunionInvitado);
    }

    public boolean existsOrganoReunionInvitadoByPersonaId(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionInvitado)
                .where(qOrganoReunionInvitado.personaId.eq(personaId)).exists();
    }

    @Transactional
    public void removeInvitadoFromReuniones(Long personaId, String organoId) {
        if (personaId != null && personaId.intValue() != 0)
        {
            List<Long> idsReunionesNoConvocadasConOrgano = organoReunionDAO.getIdsReunionesNoConvocadasByOrganoId(new Long(organoId));

            if (idsReunionesNoConvocadasConOrgano != null && idsReunionesNoConvocadasConOrgano.size() > 0)
            {
                JPADeleteClause delete = new JPADeleteClause(entityManager, qOrganoReunionInvitado);
                delete.where(qOrganoReunionInvitado.personaId.eq(personaId).and(qOrganoReunionInvitado.reunionId.in(idsReunionesNoConvocadasConOrgano))).execute();
            }
        }
    }
}
