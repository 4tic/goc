package es.uji.apps.goc.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrganoLogoPK implements Serializable
{

    @Column(name = "ORGANO_ID")
    private String organoId;

    @Column(name = "EXTERNO")
    private Boolean externo;



    public String getOrganoId()
    {
        return organoId;
    }

    public void setOrganoId(String organoId)
    {
        this.organoId = organoId;
    }

    public Boolean getExterno()
    {
        return externo;
    }

    public void setExterno(Boolean externo)
    {
        this.externo = externo;
    }
}
