package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class UsuarioNoPresideVotacionException extends CoreDataBaseException
{
    public UsuarioNoPresideVotacionException()
    {
        super("appI18N.excepciones.usuarioNoPresideVotacion");
    }

    public UsuarioNoPresideVotacionException(String message)
    {
        super(message);
    }
}
