package es.uji.apps.goc.model;

public class EstadoVotacionPunto extends EstadoPunto {
    private boolean votado;

    public EstadoVotacionPunto(
        Long puntoId,
        boolean abierta,
        boolean votable
    ) {
        super(puntoId, abierta, votable);
    }

    public EstadoVotacionPunto(EstadoPunto estadoPunto) {
        super(estadoPunto.getPuntoId(), estadoPunto.isAbierta(), estadoPunto.isVotable());
    }

    public boolean isVotado() {
        return votado;
    }

    public void setVotado(boolean votado) {
        this.votado = votado;
    }
}
