package es.uji.apps.goc.model;

import java.util.List;

public class Grupo
{
    private String nombre;
    private List<Long> ids;

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public List<Long> getIds()
    {
        return ids;
    }

    public void setIds(List<Long> ids)
    {
        this.ids = ids;
    }
}
