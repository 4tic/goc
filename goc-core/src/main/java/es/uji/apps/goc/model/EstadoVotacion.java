package es.uji.apps.goc.model;

import java.util.List;

public class EstadoVotacion {
    private boolean votante;
    private boolean presideVotacion;
    private boolean asistencia;
    private List<EstadoVotacionPunto> puntos;

    public boolean isVotante() {
        return votante;
    }

    public void setVotante(boolean votante) {
        this.votante = votante;
    }

    public boolean isPresideVotacion() {
        return presideVotacion;
    }

    public void setPresideVotacion(boolean presideVotacion) {
        this.presideVotacion = presideVotacion;
    }

    public boolean isAsistencia() {
        return asistencia;
    }

    public void setAsistencia(boolean asistencia) {
        this.asistencia = asistencia;
    }

    public List<EstadoVotacionPunto> getPuntos() {
        return puntos;
    }

    public void setPuntos(List<EstadoVotacionPunto> puntos) {
        this.puntos = puntos;
    }
}
