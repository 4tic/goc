package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class ReunionNoAdmiteSuplenciaException extends CoreDataBaseException
{
    public ReunionNoAdmiteSuplenciaException()
    {
        super("appI18N.excepciones.reunionNoAdmiteSuplencia");
    }

    public ReunionNoAdmiteSuplenciaException(String message)
    {
        super(message);
    }
}
