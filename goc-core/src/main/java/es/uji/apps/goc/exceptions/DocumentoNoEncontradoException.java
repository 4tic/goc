package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class DocumentoNoEncontradoException extends CoreDataBaseException
{
    public DocumentoNoEncontradoException()
    {
        super("appI18N.excepciones.documentoNoEncontrado");
    }

    public DocumentoNoEncontradoException(String message)
    {
        super(message);
    }
}
