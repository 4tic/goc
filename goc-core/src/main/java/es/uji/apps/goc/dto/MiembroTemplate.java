package es.uji.apps.goc.dto;

import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class MiembroTemplate implements Firmante
{
    private String id;

    private String nombre;

    private String email;

    private String suplente;
    private Long suplenteId;
    private String delegadoVoto;
    private Long delegadoVotoId;
    private String miembroId;

    private Boolean asistencia;
    private Boolean justificacionAusencia;

    private CargoTemplate cargo;

    private List<String> delegacionesDeVoto;

    private String condicion;

    private String descripcion;

    private String grupo;

    private Boolean firmante;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public MiembroTemplate()
    {
    }

    public CargoTemplate getCargo()
    {
        return cargo != null && cargo.getId() != null && !cargo.getId().equals("0") ? cargo : new CargoTemplate("","");
    }

    public void setCargo(CargoTemplate cargo)
    {
        this.cargo = cargo;
    }

    public String getSuplente()
    {
        return suplente;
    }

    public void setSuplente(String suplente)
    {
        this.suplente = suplente;
    }

    public Long getSuplenteId()
    {
        return suplenteId;
    }

    public void setSuplenteId(Long suplenteId)
    {
        this.suplenteId = suplenteId;
    }

    public Boolean getJustificacionAusencia() {
        return justificacionAusencia;
    }

    public void setJustificacionAusencia(Boolean justificacionAusencia) {
        this.justificacionAusencia = justificacionAusencia;
    }

    public String getMiembroId()
    {
        return miembroId;
    }

    public void setMiembroId(String miembroId)
    {
        this.miembroId = miembroId;
    }

    public Boolean getAsistencia()
    {
        return asistencia;
    }

    public void setAsistencia(Boolean asistencia)
    {
        this.asistencia = asistencia;
    }

    public String getDelegadoVoto()
    {
        return delegadoVoto;
    }

    public void setDelegadoVoto(String delegadoVoto)
    {
        this.delegadoVoto = delegadoVoto;
    }

    public Long getDelegadoVotoId()
    {
        return delegadoVotoId;
    }

    public void setDelegadoVotoId(Long delegadoVotoId)
    {
        this.delegadoVotoId = delegadoVotoId;
    }

    public List<String> getDelegacionesDeVoto()
    {
        return delegacionesDeVoto;
    }

    public void setDelegacionesDeVoto(List<String> delegacionesDeVoto)
    {
        this.delegacionesDeVoto = delegacionesDeVoto;
    }

    public void addDelegacionDeVoto(String delegadoVoto)
    {
        if (this.delegacionesDeVoto == null) delegacionesDeVoto = new ArrayList<>();

        delegacionesDeVoto.add(delegadoVoto);
    }

    public String getNombresDelegacionesDeVoto()
    {
        if (this.delegacionesDeVoto == null || this.delegacionesDeVoto.isEmpty()) return "";

        return StringUtils.join(this.delegacionesDeVoto, ", ");
    }

    public String getCondicion()
    {
        return condicion;
    }

    public void setCondicion(String condicion)
    {
        this.condicion = condicion;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getCargoNombreDescripcion()
    {
        if(getCargo() != null && getCargo().getNombre()!= null)
        {
            return getCargo().getNombre();
        }
        else if(getDescripcion() != null)
        {
            return getDescripcion();
        }
        else
        {
            return "";
        }
    }

    public String getCargoCodigo() {
        return this.cargo != null ? this.cargo.getCodigo() : null;
    }

    public Boolean getFirmante() {
        return this.firmante != null && this.firmante == true;
    }

    public void setFirmante(Boolean firmante) {
        this.firmante = firmante;
    }

    public boolean isFirmante() {
        return this.getFirmante();
    }
}
