package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.goc.dto.OrganoInvitado;
import es.uji.apps.goc.dto.QOrganoInvitado;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrganoInvitadoDAO extends BaseDAODatabaseImpl
{
    QOrganoInvitado qOrganoInvitado = QOrganoInvitado.organoInvitado;

    public List<OrganoInvitado> getInvitadosByOrgano(String organoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoInvitado).where(qOrganoInvitado.organoId.eq(organoId))
            .orderBy(qOrganoInvitado.orden.asc(), qOrganoInvitado.id.asc()).list(qOrganoInvitado);
    }

    public void subeInvitado(Long organoInvitadoId)
    {
        compruebaOrganoOrdenInicializadoByInvitado(organoInvitadoId);
        OrganoInvitado organoInvitado = this.get(OrganoInvitado.class, organoInvitadoId).get(0);
        OrganoInvitado invitadoSuperior = getInvitadoSuperior(organoInvitadoId);

        if(invitadoSuperior != null)
        {
            Long ordenInvitadoSuperiorActual = invitadoSuperior.getOrden();
            invitadoSuperior.setOrden(organoInvitado.getOrden());
            this.update(invitadoSuperior);

            organoInvitado.setOrden(ordenInvitadoSuperiorActual);
            this.update(organoInvitado);
        }
    }

    private Long getUltimoInvitadoConOrdenByOrgano(String organoId)
    {
        QOrganoInvitado qOrganoInvitado = QOrganoInvitado.organoInvitado;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoInvitado)
            .where(qOrganoInvitado.organoId.eq(organoId))
            .singleResult(qOrganoInvitado.orden.max());
    }

    private Long getOrdenByInvitado(Long invitadoId)
    {
        QOrganoInvitado qOrganoInvitado = QOrganoInvitado.organoInvitado;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoInvitado)
            .where(qOrganoInvitado.id.eq(invitadoId))
            .singleResult(qOrganoInvitado.orden);
    }

    private OrganoInvitado getInvitadoSuperior(Long invitadoIdId){
        QOrganoInvitado qOrganoInvitado = QOrganoInvitado.organoInvitado;

        Long ordenInvitado = getOrdenByInvitado(invitadoIdId);
        if(ordenInvitado == null)
        {
            return null;
        }

        JPAQuery queryUltimoOrdenAnterior = new JPAQuery(entityManager);
        Long ultimoOrdenAnterior = queryUltimoOrdenAnterior.from(qOrganoInvitado)
            .where(qOrganoInvitado.orden.lt(ordenInvitado))
            .singleResult(qOrganoInvitado.orden.max());

        if(ultimoOrdenAnterior == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoInvitado)
            .where(qOrganoInvitado.orden.eq(ultimoOrdenAnterior))
            .singleResult(qOrganoInvitado);
    }

    private OrganoInvitado getInvitadoInferior(Long miembroId){
        QOrganoInvitado qOrganoInvitado = QOrganoInvitado.organoInvitado;
        Long ordenInvitado = getOrdenByInvitado(miembroId);
        if(ordenInvitado == null)
        {
            return null;
        }

        JPAQuery queryUltimoOrdenPosterior = new JPAQuery(entityManager);
        Long ultimoOrdenPosterior = queryUltimoOrdenPosterior.from(qOrganoInvitado)
            .where(qOrganoInvitado.orden.gt(ordenInvitado))
            .singleResult(qOrganoInvitado.orden.min());
        if(ultimoOrdenPosterior == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoInvitado)
            .where(qOrganoInvitado.orden.eq(ultimoOrdenPosterior))
            .singleResult(qOrganoInvitado);
    }

    public void bajaInvitado(Long organoInvitadoId)
    {
        compruebaOrganoOrdenInicializadoByInvitado(organoInvitadoId);
        OrganoInvitado organoInvitado = this.get(OrganoInvitado.class, organoInvitadoId).get(0);
        OrganoInvitado invitadoInferior = getInvitadoInferior(organoInvitadoId);

        if(invitadoInferior != null)
        {
            Long ordenInvitadoInferiorActual = invitadoInferior.getOrden();
            ordenInvitadoInferiorActual = invitadoInferior.getOrden();
            invitadoInferior.setOrden(organoInvitado.getOrden());
            this.update(invitadoInferior);

            organoInvitado.setOrden(ordenInvitadoInferiorActual);
            this.update(organoInvitado);
        }
    }

    private void inicializarOrdenInvitadosEnOrgano(String organoId)
    {
        QOrganoInvitado qOrganoInvitado = QOrganoInvitado.organoInvitado;

        JPAQuery query = new JPAQuery(entityManager);
        List<OrganoInvitado> listaInvitados =
            query.from(qOrganoInvitado)
                .where(qOrganoInvitado.organoId.eq(organoId))
                .orderBy(qOrganoInvitado.orden.asc(), qOrganoInvitado.id.asc())
                .list(qOrganoInvitado);

        Long i = 1L;
        for(OrganoInvitado invitado : listaInvitados)
        {
            invitado.setOrden(i);
            i++;
            this.update(invitado);
        }
    }

    private void compruebaOrganoOrdenInicializadoByInvitado(Long organoInvitadoId)
    {
        OrganoInvitado organoInvitado = this.get(OrganoInvitado.class, organoInvitadoId).get(0);
        if(organoInvitado.getOrden() == null){
            inicializarOrdenInvitadosEnOrgano(organoInvitado.getOrganoId());
        }
    }

    public OrganoInvitado getInvitadoById(Long organoInvitadoId) {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoInvitado).where(qOrganoInvitado.id.eq(organoInvitadoId)).uniqueResult(qOrganoInvitado);
    }
}
