package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class UsuarioNoTieneDerechoAVotoException extends CoreDataBaseException
{
    public UsuarioNoTieneDerechoAVotoException()
    {
        super("appI18N.excepciones.usuarioNoTieneDerechoAVoto");
    }

    public UsuarioNoTieneDerechoAVotoException(String message)
    {
        super(message);
    }
}
