package es.uji.apps.goc.notifications;

import com.google.common.base.Strings;

import com.sun.jersey.core.util.Base64;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.uji.apps.goc.DateUtils;
import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.dao.NotificacionesDAO;
import es.uji.apps.goc.dao.OrganoAutorizadoDAO;
import es.uji.apps.goc.dao.OrganoDAO;
import es.uji.apps.goc.dao.OrganoReunionMiembroDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dao.ReunionExternosDAO;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.PuntoOrdenDiaComentario;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionComentario;
import es.uji.apps.goc.exceptions.MailsIncorrectosException;
import es.uji.apps.goc.exceptions.NotificacionesException;
import es.uji.apps.goc.services.ICalService;
import es.uji.apps.goc.templates.HTMLTemplate;
import es.uji.apps.goc.templates.Template;

@Component
public class AvisosReunion
{
    private static Logger log = Logger.getLogger(AvisosReunion.class);
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;
    private NotificacionesDAO notificacionesDAO;
    private ReunionDAO reunionDAO;
    private OrganoAutorizadoDAO organoAutorizadoDAO;
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;
    private OrganoDAO organoDAO;
    private ReunionExternosDAO reunionExternosDAO;
    private ICalService iCalService;

    @Value("${goc.publicUrl}")
    private String publicUrl;

    @Value("${goc.context}")
    private String appContext;

    @Value("${uji.smtp.defaultSender}")
    private String defaultSender;

    @Value("${goc.mainLanguage}")
    public String mainLanguage;

    @Value("${goc.templates.path:classpath:templates/}")
    public String templatesPath;

    @Value("${goc.email.templates:false}")
    private Boolean emailTemplates;

    @Value("${goc.email.useAlwaysDefaultSender:true}")
    private Boolean useAlwaysDefaultSender;

    @Value("${goc.bloquearRemitenteConvocatoria:true}")
    private boolean bloquearRemitenteConvocatoria;

    private NotificationCommonProperties notificationCommonProperties;

    @Autowired
    public AvisosReunion(ReunionDAO reunionDAO, OrganoReunionMiembroDAO organoReunionMiembroDAO,
            NotificacionesDAO notificacionesDAO, OrganoAutorizadoDAO organoAutorizadoDAO, PuntoOrdenDiaDAO puntoOrdenDiaDAO, OrganoDAO organoDAO, ICalService iCalService
        )
    {
        this.reunionDAO = reunionDAO;
        this.organoReunionMiembroDAO = organoReunionMiembroDAO;
        this.notificacionesDAO = notificacionesDAO;
        this.organoAutorizadoDAO = organoAutorizadoDAO;
        this.puntoOrdenDiaDAO = puntoOrdenDiaDAO;
        this.organoDAO = organoDAO;
        this.iCalService = iCalService;

        notificationCommonProperties = new NotificationCommonProperties(templatesPath, publicUrl, appContext);
    }

    @Transactional
    public void enviaAvisoAltaSuplente(Long reunionId, String emailSuplente, String miembroNombre, String miembroCargo)
        throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (emailSuplente == null || emailSuplente.isEmpty()) return;

        String textoAux = resourceBundle.getString("mail.reunion.suplenteDesignado") + " " + miembroNombre + " (" + miembroCargo + ")";

        String asunto = "[GOC]";

        asunto += " " + resourceBundle.getString("mail.reunion.suplenteEnReunion") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null)
        {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        buildAndSendMessageWithExtraText(reunion, emailSuplente, asunto, textoAux);
    }

    @Transactional
    public void enviaAvisoBajaSuplente(Long reunionId, String emailSuplente, String miembroNombre, String miembroCargo)
        throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (emailSuplente == null || emailSuplente.isEmpty()) return;

        String textoAux = resourceBundle.getString("mail.reunion.suplenteBaja") + " " + miembroNombre + " (" + miembroCargo + ")";

        String asunto = "[GOC]";

        asunto += " " + resourceBundle.getString("mail.reunion.suplenteBajaEnReunion") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null)
        {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        buildAndSendMessageWithExtraText(reunion, emailSuplente, asunto, textoAux);
    }

    @Transactional
    public void enviaAvisoAltaDelegacionVoto(Long reunionId, String emailDelegadoVoto, String miembroNombre,
            String miembroCargo) throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (emailDelegadoVoto == null || emailDelegadoVoto.isEmpty()) return;

        String textoAux = resourceBundle.getString("mail.reunion.delegacionVoto") + " " + miembroNombre + " (" + miembroCargo + ")";

        String asunto = "[GOC]";

        asunto += " " + resourceBundle.getString("mail.reunion.delegacionVotoEnReunion") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null)
        {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        buildAndSendMessageWithExtraText(reunion, emailDelegadoVoto, asunto, textoAux);
    }

    @Transactional
    public void enviaAvisoBajaDelegacionVoto(Long reunionId, String emailDelegadoVoto, String miembroNombre,
            String miembroCargo) throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (emailDelegadoVoto == null || emailDelegadoVoto.isEmpty()) return;

        String textoAux =
                resourceBundle.getString("mail.reunion.delegacionVotoBaja") + " " + miembroNombre + " (" + miembroCargo + ")";

        String asunto = "[GOC]";

        asunto += " " + resourceBundle.getString("mail.reunion.delegacionVotoBajaEnReunion") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null)
        {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        buildAndSendMessageWithExtraText(reunion, emailDelegadoVoto, asunto, textoAux);
    }

    @Transactional
    public void enviaAvisoNuevaReunion(Reunion reunion, String textoConvocatoria) throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        List<String> destinatarios = organoReunionMiembroDAO.getMailsReunion(reunion);


        String asunto = "[GOC]";

        if (reunion.getAvisoPrimeraReunion())
        {
            asunto += " [" + resourceBundle.getString("mail.reunion.rectificada") + "]";
        }

        asunto += " " + resourceBundle.getString("mail.reunion.convocatoria") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null)
        {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        byte[] calendarEncoded = null;
        try {
            String calendarString = iCalService.creaICal(reunion).toString();
            calendarEncoded = Base64.encode(calendarString.getBytes());
        } catch (Exception e) {
            log.error("No se ha podido crear el calendar");
        }
        buildAndSendMessage(reunion, destinatarios, asunto, textoConvocatoria, calendarEncoded, "text/calendar");
    }

    private String getNombreOrganos(Reunion reunion)
    {
        List<String> nombreOrganos = new ArrayList<>();

        for (OrganoReunion organo : reunion.getReunionOrganos())
        {
            nombreOrganos.add(organo.getOrganoNombre());
        }

        return StringUtils.join(nombreOrganos, ", ");
    }

    public Boolean enviaAvisoReunionProxima(Reunion reunion) throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        List<String> mailsAsistenciaConfirmadaReunion = organoReunionMiembroDAO.getMailsAsistenciaConfirmadaReunion(reunion);

        if (mailsAsistenciaConfirmadaReunion == null || mailsAsistenciaConfirmadaReunion.size() == 0)
        {
            return false;
        }

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        byte[] calendarEncoded = null;
        try {
            String calendarString = iCalService.creaICal(reunion).toString();
            calendarEncoded = Base64.encode(calendarString.getBytes());
        } catch (Exception e) {
            log.error("No se ha podido crear el calendar");
        }
        buildAndSendMessage(reunion, mailsAsistenciaConfirmadaReunion,
                "[GOC] " + resourceBundle.getString("mail.reunion.recordatorio") + ": " + reunion.getAsunto() + " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " (" + df.format(reunion.getFecha()) + ")",
            null, calendarEncoded, "text/calendar");

        return true;
    }

    public void enviaAvisoNuevoComentarioAPersonasAsistentesReunion(Reunion reunion, ReunionComentario reunionComentario)
        throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Mensaje mensaje = new Mensaje();

        mensaje.setAsunto(resourceBundle.getString("mail.reunion.nuevoComentario") + " [" + reunion.getAsunto() + "]");

        StringBuilder cuerpo = new StringBuilder();

        if (emailTemplates) {
            Template template = new HTMLTemplate("email-nuevo-comentario", notificationCommonProperties);
            template.put("reunion", reunion);
            template.put("creadorNombre", reunionComentario.getCreadorNombre());
            template.put("comentario", reunionComentario.getComentario());
            mensaje.setCuerpo(new String(template.process()));
        } else
        {

            cuerpo.append("<h2>" + reunion.getAsunto() + "</h2>");
            cuerpo.append("<div><strong>" + reunionComentario.getCreadorNombre() + "</strong>" + " " + resourceBundle.getString("mail.reunion.comentario") + ".</div>");
            cuerpo.append("<br/><br/>");

            cuerpo.append(reunionComentario.getComentario());
            mensaje.setCuerpo(cuerpo.toString());
        }

        mensaje.setDestinos(organoReunionMiembroDAO.getMailsAsistenciaConfirmadaReunion(reunion));
        mensaje.setReplyTo(defaultSender);
        mensaje.setFrom(defaultSender);

        notificacionesDAO.enviaNotificacion(mensaje);
    }


    public void enviarAvisoNuevoComentarioPuntoOrdenDiaPersonasAsistentesReunion(Reunion reunion,
                                                                                 PuntoOrdenDiaComentario puntoOrdenDiaComentario) throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        Mensaje mensaje = new Mensaje();

        mensaje.setAsunto("[GOC] " + resourceBundle.getString("mail.reunion.nuevoComentario") + " [" + reunion.getAsunto() + "]");

        StringBuilder cuerpo = new StringBuilder();

        if (emailTemplates) {
            Template template = new HTMLTemplate("email-nuevo-comentario-punto", notificationCommonProperties);
            template.put("reunion", reunion);
            template.put("tituloPunto", puntoOrdenDiaComentario.getPuntoOrdenDia().getTitulo());
            template.put("creadorNombre", puntoOrdenDiaComentario.getCreadorNombre());
            template.put("comentario", puntoOrdenDiaComentario.getComentario());
            mensaje.setCuerpo(new String(template.process()));
        } else {
            cuerpo.append("<h2>" + reunion.getAsunto() + "</h2>");
            cuerpo.append("<span> " + resourceBundle.getString("mail.reunion.comentarioPunto") + ": </span>");
            cuerpo.append("<h3>" + puntoOrdenDiaComentario.getPuntoOrdenDia().getTitulo() + "</h3>");
            cuerpo.append("<div><strong>" + puntoOrdenDiaComentario.getCreadorNombre() + "</strong>" + " " + resourceBundle.getString("mail.reunion.comentario") + ".</div>");
            cuerpo.append("<br/><br/>");
            cuerpo.append(puntoOrdenDiaComentario.getComentario());
            mensaje.setCuerpo(cuerpo.toString());
        }

        mensaje.setDestinos(organoReunionMiembroDAO.getMailsAsistenciaConfirmadaReunion(reunion));
        mensaje.setReplyTo(reunion.getCreadorEmail());
        mensaje.setFrom(defaultSender);

        notificacionesDAO.enviaNotificacion(mensaje);
    }

    private void buildAndSendMessageWithExtraText(Reunion reunion, String miembro, String asunto, String textoAux)
            throws Exception
    {
        Mensaje mensaje = new Mensaje();
        mensaje.setAsunto(asunto);

        List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados = puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunion.getId());

        if(emailTemplates)
        {
            Template template = getTemplateReunionEmail(reunion, puntosOrdenDiaOrdenados);
            template.put("textoAux", textoAux);
            mensaje.setCuerpo(new String(template.process()));
        }
        else
        {
            ReunionFormatter formatter = new ReunionFormatter(reunion, puntosOrdenDiaOrdenados, mainLanguage);
            mensaje.setCuerpo(formatter.format(publicUrl + appContext, textoAux, null));
        }
        mensaje.setFrom(getReplyTo(reunion));
        mensaje.setReplyTo(getReplyTo(reunion));
        mensaje.setDestinos(Collections.singletonList(miembro));

        notificacionesDAO.enviaNotificacion(mensaje);
    }

    private void buildAndSendMessage(Reunion reunion, List<String> reunionMails, String asunto, String textoConvocatoria,
        byte[] fileBase64, String fileContentType)
            throws Exception
    {
        Mensaje mensaje = new Mensaje();
        mensaje.setAsunto(asunto);

        List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados = puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunion.getId());

        if (emailTemplates) {
            Template template = getTemplateReunionEmail(reunion, puntosOrdenDiaOrdenados);
            template.put("textoConvocatoria", textoConvocatoria);
            mensaje.setCuerpo(new String(template.process()));
        } else {
            ReunionFormatter formatter = new ReunionFormatter(reunion, puntosOrdenDiaOrdenados, mainLanguage);
            mensaje.setCuerpo(formatter.format(publicUrl + appContext, null, textoConvocatoria));
        }
        String email = getReplyTo(reunion);
        mensaje.setFrom(email);
        mensaje.setReplyTo(email);
        mensaje.setDestinos(reunionMails);

        if (fileBase64 != null) {
            mensaje.setFileBase64(fileBase64);
            mensaje.setFileContentType(fileContentType);
        }

        notificacionesDAO.enviaNotificacion(mensaje);
    }

    private Template getTemplateReunionEmail(
        Reunion reunion,
        List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados
    )
    {
        Template template = new HTMLTemplate("email-envio-convocatoria", notificationCommonProperties);
        template.put("reunion", reunion);
        template.put("puntosOrdenDiaOrdenados", puntosOrdenDiaOrdenados);

        String diaSemanaTextoSegundaConvocatoria = null;
        if(reunion.getFechaSegundaConvocatoria() != null) {
            diaSemanaTextoSegundaConvocatoria = DateUtils.getDiaSemanaTexto(reunion.getFechaSegundaConvocatoria(), mainLanguage);
        }


        template.put("diaSemanaFechaPrimeraConvocatoria", DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage));
        template.put("diaSemanaFechaSegundaConvocatoria", diaSemanaTextoSegundaConvocatoria);

        return template;
    }

    private String getReplyTo(Reunion reunion) {
        if (useAlwaysDefaultSender == null || useAlwaysDefaultSender || bloquearRemitenteConvocatoria)
            return defaultSender;
        else {
            String emailOrganoConvocante = defaultSender;
            try {
                emailOrganoConvocante = reunionDAO.getConvocanteEmailByReunionId(reunion.getId());
                emailOrganoConvocante = !Strings.isNullOrEmpty(emailOrganoConvocante) ? emailOrganoConvocante : getReplyToAvisoPrimeraReunionUserEmailOrDefaultSender(reunion);
            } catch (Exception e) {
                log.error("No se puede obtener el mail de la reunion con id " + reunion.getId(), e);
                emailOrganoConvocante = defaultSender;
            } finally {
                return emailOrganoConvocante;
            }
        }
    }

    private String getReplyToAvisoPrimeraReunionUserEmailOrDefaultSender(Reunion reunion) {
        return reunion.getAvisoPrimeraReunionUserEmail() != null ? reunion.getAvisoPrimeraReunionUserEmail() : defaultSender;
    }

    public void enviarAvisoAnulacionReunion(Mensaje mensaje) throws NotificacionesException, MailsIncorrectosException
    {
        notificacionesDAO.enviaNotificacion(mensaje);
    }

    private Template getTemplateAnulacionReunionEmail(
        Reunion reunion
    )
    {
        Template template = new HTMLTemplate("email-envio-anulacion", notificationCommonProperties);
        template.put("reunion", reunion);


        String diaSemanaTextoSegundaConvocatoria = null;
        if(reunion.getFechaSegundaConvocatoria() != null) {
            diaSemanaTextoSegundaConvocatoria = DateUtils.getDiaSemanaTexto(reunion.getFechaSegundaConvocatoria(), mainLanguage);
        }


        template.put("diaSemanaFechaPrimeraConvocatoria", DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage));
        template.put("diaSemanaFechaSegundaConvocatoria", diaSemanaTextoSegundaConvocatoria);

        return template;
    }

    public Mensaje getMensajeAvisoAnulacion(Reunion reunion) throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        List<String> mailsReunion = organoReunionMiembroDAO.getMailsReunion(reunion);

        String asunto = "[GOC] " + resourceBundle.getString("mail.reunion.anulacion");

        asunto += " " + resourceBundle.getString("mail.reunion.convocatoria") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null)
        {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());

        Mensaje mensaje = new Mensaje();
        mensaje.setAsunto(asunto);

        if(emailTemplates) {
            Template template = getTemplateAnulacionReunionEmail(reunion);
            mensaje.setCuerpo(new String(template.process()));
        }
        else {
            StringBuilder cuerpo = new StringBuilder();
            cuerpo.append("<h2>" + reunion.getAsunto() + "</h2>");
            cuerpo.append("<div>" + String.format(resourceBundle.getString("mail.reunion.anulacionMensaje"), DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage), dataFormatter.format(reunion.getFecha())) + "</div>");
            cuerpo.append("<br/><br/>");

            mensaje.setCuerpo(cuerpo.toString());
        }
        mensaje.setFrom(defaultSender);
        mensaje.setReplyTo(getReplyTo(reunion));

        mensaje.setDestinos(mailsReunion);

        return mensaje;
    }

    public void enviaAvisoActaReunionDisponible(Reunion reunion, String urlActa) throws Exception
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18nEmails", mainLanguage);
        List<String> destinatarios = organoReunionMiembroDAO.getMailsReunion(reunion);


        String asunto = "[GOC]";

        asunto += " " + resourceBundle.getString("mail.reunion.acta") + " " + getNombreOrganos(reunion);

        if (reunion.getNumeroSesion() != null)
        {
            asunto += " n. " + reunion.getNumeroSesion();
        }

        SimpleDateFormat dataFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        asunto += " " + DateUtils.getDiaSemanaTexto(reunion.getFecha(), mainLanguage) + " " + dataFormatter.format(reunion.getFecha());


        String cuerpoMensaje = resourceBundle.getString("mail.reunion.actaDisponible") + ", " + resourceBundle.getString("mail.reunion.acta") + " " + getNombreOrganos(reunion) + ":" + "\n" +  publicUrl +  urlActa;

        buildAndSendMessage(reunion, destinatarios, asunto, cuerpoMensaje, null, null);
    }
}
