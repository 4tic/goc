package es.uji.apps.goc.dto;

public interface Firmante {
    boolean isFirmante();
    String getId();
    String getNombre();
    String getEmail();
    String getCargoNombreDescripcion();
    String getCargoCodigo();
    Long getSuplenteId();
    String getSuplente();
    Boolean getAsistencia();
}
