package es.uji.apps.goc.dto;

import org.hibernate.annotations.Type;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_VW_TODOS_VOTOS")
public class VotosTodosDTO {

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    private UUID id;

    @Column(name = "REUNION_ID")
    private String reunionId;

    @Column(name = "PUNTO_ID")
    private String puntoOrdenDiaId;

    @Column(name = "NOMBRE_VOTANTE")
    private String nombreVotante;

    @Column(name = "PERSONA_ID")
    private String personaId;

    @Column(name = "VOTO_EN_NOMBRE")
    private String votoEnNombre;

    @Column(name = "VOTO_EN_NOMBRE_PERSONA_ID")
    private String votoEnNombrePersonaId;

    @Column(name = "VOTO_PUBLICO")
    private Boolean votoPublico;

    public UUID getId()
    {
        return id;
    }

    public void setId(UUID id)
    {
        this.id = id;
    }

    public String getReunionId()
    {
        return reunionId;
    }

    public void setReunionId(String reunionId)
    {
        this.reunionId = reunionId;
    }

    public String getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(String puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public String getNombreVotante()
    {
        return nombreVotante;
    }

    public void setNombreVotante(String nombreVotante)
    {
        this.nombreVotante = nombreVotante;
    }

    public String getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(String personaId)
    {
        this.personaId = personaId;
    }

    public String getVotoEnNombre()
    {
        return votoEnNombre;
    }

    public void setVotoEnNombre(String votoEnNombre)
    {
        this.votoEnNombre = votoEnNombre;
    }

    public String getVotoEnNombrePersonaId()
    {
        return votoEnNombrePersonaId;
    }

    public void setVotoEnNombrePersonaId(String votoEnNombrePersonaId)
    {
        this.votoEnNombrePersonaId = votoEnNombrePersonaId;
    }

    public Boolean getVotoPublico()
    {
        return votoPublico;
    }

    public void setVotoPublico(Boolean votoPublico)
    {
        this.votoPublico = votoPublico;
    }
}
