package es.uji.apps.goc.dao;

import com.google.common.base.Strings;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.query.ListSubQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import es.uji.apps.goc.dto.Auditoria;
import es.uji.apps.goc.dto.OrganoAutorizado;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionInvitado;
import es.uji.apps.goc.dto.QClave;
import es.uji.apps.goc.dto.QDescriptor;
import es.uji.apps.goc.dto.QOrganoAutorizado;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.apps.goc.dto.QOrganoReunionInvitado;
import es.uji.apps.goc.dto.QOrganoReunionMiembro;
import es.uji.apps.goc.dto.QPuntoOrdenDia;
import es.uji.apps.goc.dto.QPuntoOrdenDiaDescriptor;
import es.uji.apps.goc.dto.QReunion;
import es.uji.apps.goc.dto.QReunionBusqueda;
import es.uji.apps.goc.dto.QReunionComentario;
import es.uji.apps.goc.dto.QReunionDocumento;
import es.uji.apps.goc.dto.QReunionEditor;
import es.uji.apps.goc.dto.QReunionExternos;
import es.uji.apps.goc.dto.QReunionHojaFirmas;
import es.uji.apps.goc.dto.QReunionInvitado;
import es.uji.apps.goc.dto.QReunionPermiso;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionEditor;
import es.uji.apps.goc.dto.ReunionExternos;
import es.uji.apps.goc.dto.ReunionHojaFirmas;
import es.uji.apps.goc.dto.ReunionInvitado;
import es.uji.apps.goc.dto.ReunionPermiso;
import es.uji.apps.goc.model.BuscadorReunionesWrapper;
import es.uji.apps.goc.model.EnumAccionesAuditoria;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.RespuestaFirma;
import es.uji.apps.goc.model.RespuestaFirmaAsistencia;
import es.uji.apps.goc.model.RespuestaFirmaPuntoOrdenDiaAcuerdo;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;

@Repository
public class ReunionDAO extends BaseDAODatabaseImpl {
    private QReunion qReunion = QReunion.reunion;
    private QReunionBusqueda qReunionBusqueda = QReunionBusqueda.reunionBusqueda;
    private QReunionEditor qReunionEditor = QReunionEditor.reunionEditor;
    private QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;
    private QOrganoReunionInvitado qOrganoReunionInvitado = QOrganoReunionInvitado.organoReunionInvitado;
    private QPuntoOrdenDiaDescriptor qPuntoOrdenDiaDescriptor = QPuntoOrdenDiaDescriptor.puntoOrdenDiaDescriptor;
    private QDescriptor qDescriptor = QDescriptor.descriptor1;
    private QClave qClave = QClave.clave1;
    private QReunionComentario qReunionComentario = QReunionComentario.reunionComentario;
    private QReunionDocumento qReunionDocumento = QReunionDocumento.reunionDocumento;
    private QReunionInvitado qReunionInvitado = QReunionInvitado.reunionInvitado;
    private QReunionPermiso qReunionPermiso = QReunionPermiso.reunionPermiso;
    private QReunionHojaFirmas qReunionHojaFirmas = QReunionHojaFirmas.reunionHojaFirmas;
    private QReunionExternos qReunionExternos = QReunionExternos.reunionExternos;
    private QOrganoAutorizado qOrganoAutorizado = QOrganoAutorizado.organoAutorizado;

    public List<ReunionEditor> getReunionesByEditorId(Long connectedUserId, String organoId, Long tipoOrganoId,
            Boolean externo, Boolean completada, Boolean pasadas)
    {
        BooleanExpression where = qReunionEditor.completada.eq(completada);
        where = where.and(qReunionEditor.editorId.eq(connectedUserId));
        if (tipoOrganoId != null) {
            where = where.and(qReunionEditor.tipoOrganoId.eq(tipoOrganoId));
        }

        if (organoId != null) {
            where = where.and(qReunionEditor.organoId.eq(organoId));
        }

        if (externo != null) {
            where = where.and(qReunionEditor.externo.eq(externo));
        }

        if (pasadas == null || !pasadas) {
            where = where.and(qReunionEditor.fecha.goe(new Date()));
        }

        JPAQuery query = new JPAQuery(entityManager);

        List<ReunionEditor> reunionesOrganos =
            query.from(qReunionEditor)
                .where(where)
                .orderBy(qReunionEditor.fecha.desc())
                .list(qReunionEditor);

        reunionesOrganos = reunionesOrganos.stream().sorted(Comparator.comparing(ReunionEditor::getFecha).reversed()).collect(Collectors.toList());
        return reunionesOrganos;
    }

    @Transactional
    public List<ReunionEditor> getReunionesByAdmin(
        Boolean completada,
        Boolean pasadas,
        Long tipoOrganoId,
        String organoId,
        Boolean externo
    )
    {
        BooleanExpression where = qReunion.completada.eq(completada);
        if (tipoOrganoId != null) {
            where = where.and(qOrganoReunion.tipoOrganoId.eq(tipoOrganoId));
        }

        if (organoId != null) {
            where = where.and(qOrganoReunion.organoId.eq(organoId));
        }

        if (externo != null) {
            where = where.and(qOrganoReunion.externo.eq(externo));
        }

        if (pasadas == null || !pasadas) {
            where = where.and(qReunion.fecha.goe(new Date()));
        }

        JPAQuery query = new JPAQuery(entityManager);
        List<Tuple> list =
            query.from(qReunion)
                .join(qReunion.reunionOrganos, qOrganoReunion)
                .where(where)
                .orderBy(qReunion.fecha.desc())
                .list(qReunion, new JPASubQuery().from(qReunionDocumento).where(qReunionDocumento.reunion.id.eq(qReunion.id)).count());

        List<ReunionEditor> reunionesConOrganos = new ArrayList<>();
        for (Tuple tuple : list) {
            ReunionEditor reunionEditor = new ReunionEditor(tuple.get(0, Reunion.class), tuple.get(1, Long.class));
            reunionesConOrganos.add(reunionEditor);
        }

        if ((completada == null || !completada) && (organoId == null && tipoOrganoId == null && externo == null)) {
            BooleanExpression whereSinOrganos = qReunion.completada.eq(completada).and(
                qReunion.id.notIn(reunionesConOrganos.stream().map(ReunionEditor::getId).collect(Collectors.toList())));

            if (pasadas == null || !pasadas) {
                whereSinOrganos = whereSinOrganos.and(qReunion.fecha.goe(new Date()));
            }

            JPAQuery queryTodasReuniones = new JPAQuery(entityManager);
            List<Tuple> tupleReunionesSinOrganos = queryTodasReuniones.from(qReunion)
                    .where(
                        whereSinOrganos
                    )
                    .orderBy(qReunion.fecha.desc())
                    .list(qReunion, new JPASubQuery().from(qReunionDocumento).where(qReunionDocumento.reunion.id.eq(qReunion.id)).count());

            for (Tuple tupleReunionSinOrganos : tupleReunionesSinOrganos) {
                ReunionEditor reunionEditor = new ReunionEditor(tupleReunionSinOrganos.get(0, Reunion.class), tupleReunionSinOrganos.get(1, Long.class));
                reunionesConOrganos.add(reunionEditor);
            }
        }
        reunionesConOrganos = reunionesConOrganos.stream().sorted(Comparator.comparing(ReunionEditor::getFecha).reversed()).collect(Collectors.toList());
        return reunionesConOrganos;
    }

    public ReunionEditor getReunionByIdAndEditorId(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunionEditor).where((qReunionEditor.editorId.eq(connectedUserId)).
                and(qReunionEditor.id.eq(reunionId))).uniqueResult(qReunionEditor);
    }

    public Reunion getReunionConOrganosById(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Reunion> reuniones = query.from(qReunion)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .fetch()
                .where(qReunion.id.eq(reunionId))
                .list(qReunion);

        if (reuniones.size() == 0) {
            return null;
        }

        return reuniones.get(0);
    }

    @Transactional
    public void marcarReunionComoCompletadaYActualizarAcuerdoYUrl(Long reunionId, Long responsableActaId,
            String acuerdos, String acuerdosAlternativos, String observaciones, RespuestaFirma respuestaFirma)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);
        update.set(qReunion.completada, true)
                .set(qReunion.fechaCompletada, new Date())
                .set(qReunion.miembroResponsableActa.id, responsableActaId)
                .set(qReunion.acuerdos, acuerdos)
                .set(qReunion.acuerdosAlternativos, acuerdosAlternativos)
                .set(qReunion.urlActa, respuestaFirma.getUrlActa())
                .set(qReunion.urlActaAlternativa, respuestaFirma.getUrlActaAlternativa())
                .set(qReunion.observaciones, observaciones)
                .where(qReunion.id.eq(reunionId));
        update.execute();
    }

    @Transactional
    public void marcarReunionComoCompletadaYActualizarAcuerdo(Long reunionId, Long responsableActaId,
                                                              String acuerdos, String acuerdosAlternativos, String observaciones)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);
        update.set(qReunion.completada, true)
                .set(qReunion.fechaCompletada, new Date())
                .set(qReunion.miembroResponsableActa.id, responsableActaId)
                .set(qReunion.acuerdos, acuerdos)
                .set(qReunion.acuerdosAlternativos, acuerdosAlternativos)
                .set(qReunion.observaciones, observaciones)
                .where(qReunion.id.eq(reunionId));
        update.execute();
    }

    @Transactional
    public Reunion getReunionById(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Reunion> reuniones = query.from(qReunion).where(qReunion.id.eq(reunionId)).list(qReunion);

        if (reuniones.size() == 0) {
            return null;
        }

        return reuniones.get(0);
    }

    public Reunion getReunionConMiembrosAndPuntosDiaById(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<Reunion> reuniones = query.from(qReunion)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion)
                .fetch()
                .leftJoin(qReunion.reunionInvitados, qReunionInvitado)
                .fetch()
                .leftJoin(qOrganoReunion.invitados, qOrganoReunionInvitado)
                .fetch()
                .leftJoin(qOrganoReunion.miembros, qOrganoReunionMiembro)
                .fetch()
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .fetch()
                .where(qReunion.id.eq(reunionId))
                .list(qReunion);

        if (reuniones.size() == 0) {
            return null;
        }

        return reuniones.get(0);
    }

    public List<Reunion> getPendientesNotificacion(Date fecha)
    {
        JPAQuery query = new JPAQuery(entityManager);

        Date now = new Date();
        return query.from(qReunion)
                .where(qReunion.notificada.ne(true).and(qReunion.fecha.after(now)).and(qReunion.fecha.before(fecha)))
                .list(qReunion);
    }

    public List<ReunionPermiso> getReunionesAccesiblesByPersonaId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunionPermiso)
                .where(qReunionPermiso.personaId.eq(connectedUserId)
                    .and(qReunionPermiso.avisoPrimeraReunion.isNotNull()
                    .and(qReunionPermiso.avisoPrimeraReunion.isTrue()))
                )
                .orderBy(qReunionPermiso.fecha.desc())
                .list(qReunionPermiso);
    }

    public List<ReunionPermiso> getAllReunionesAccesiblesForAdmin()
    {
        JPAQuery query = new JPAQuery(entityManager);

        List<ReunionPermiso> list = query.from(qReunionPermiso)
                .orderBy(qReunionPermiso.fecha.desc())
                .list(qReunionPermiso);

        return quitaReunionesDuplicadas(list);
    }

    private List<ReunionPermiso> quitaReunionesDuplicadas(List<ReunionPermiso> list)
    {
        List<ReunionPermiso> reunionesSinDuplicar = new ArrayList<>();
        List<Long> idsYaUsados = new ArrayList<>();
        for (ReunionPermiso reunionPermiso : list) {
            if (idsYaUsados.contains(reunionPermiso.getId()))
                continue;

            idsYaUsados.add(reunionPermiso.getId());
            reunionesSinDuplicar.add(reunionPermiso);
        }
        return reunionesSinDuplicar;
    }

    public String getNombreAsistente(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunionPermiso)
                .where(qReunionPermiso.personaId.eq(connectedUserId)
                        .and(qReunionPermiso.id.eq(reunionId))
                        .and(qReunionPermiso.asistente.isTrue()))
                .uniqueResult(qReunionPermiso.personaNombre);
    }

    @Transactional
    public Boolean tieneAcceso(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        ReunionPermiso acceso = query.from(qReunionPermiso)
                .where(qReunionPermiso.personaId.eq(connectedUserId).and(qReunionPermiso.id.eq(reunionId)))
                .uniqueResult(qReunionPermiso);

        String responsable = (acceso != null) ? acceso.getPersonaNombre() : null;

        if (responsable != null)
            this.insert(new Auditoria(responsable, null, reunionId, EnumAccionesAuditoria.VISITED_REUNION.toString()));
        return (responsable != null);
    }

    public List<Integer> getAnyosConReunionesPublicas()
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunion)
                .where(qReunion.publica.isTrue().and(qReunion.completada.isTrue()))
                .distinct()
                .list(qReunion.fecha.year());
    }

    public List<Long> getIdsReunionesPublicas(Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression whereAnyo = null;

        if (anyo != null) {
            whereAnyo = qReunion.fecha.year().eq(anyo);
        }

        return query.from(qReunion)
                .where(
                        qReunion.publica.isTrue()
                                .and(
                                        qReunion.completada.isTrue()
                                                .or(qReunion.urlActa.isNotNull())
                                ).and(whereAnyo))
                .list(qReunion.id);
    }

    public Long getReunionesPublicas(Long tipoOrganoId, Long organoId, Long descriptorId, Long claveId,
                                     Integer anyo, Date fInicio, Date fFin, String texto, Boolean idiomaAlternativo)
    {
        return getQueryReunionesPublicas(tipoOrganoId, organoId, descriptorId, claveId, anyo, fInicio, fFin, texto, idiomaAlternativo).distinct().count();
    }

    @Transactional
    public BuscadorReunionesWrapper getReunionesPublicasPaginated(Long tipoOrganoId, Long organoId, Long descriptorId,
            Long claveId, Integer anyo, Date fInicio, Date fFin, String texto, Boolean idiomaAlternativo,
            Integer startSeach, Integer numResults)
    {
        JPAQuery reunionesQuery = getQueryReunionesPublicas(tipoOrganoId, organoId, descriptorId, claveId, anyo, fInicio, fFin, texto, idiomaAlternativo);
        JPAQuery reunionesQueryCount = reunionesQuery;
        List<Long> idsReuniones = reunionesQuery.distinct().offset(startSeach).limit(numResults).list(qReunion.id);
        Long numeroReunionesPublicasTotales = reunionesQueryCount.count();
        idsReuniones = idsReuniones.stream().collect(Collectors.toList());

        JPAQuery query = new JPAQuery(entityManager);
        List<Reunion> reuniones = query.from(qReunion)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion).fetch()
                .where(qReunion.id.in(idsReuniones)).orderBy(qReunion.fecha.desc())
                .list(qReunion).stream().distinct().collect(Collectors.toList());
        return new BuscadorReunionesWrapper(reuniones, numeroReunionesPublicasTotales);
    }

    private JPAQuery getQueryReunionesPublicas(Long tipoOrganoId, Long organoId, Long descriptorId,
                                               Long claveId, Integer anyo, Date fInicio, Date fFin, String texto, Boolean idiomaAlternativo)
    {
        BooleanExpression whereAnyo = null;
        BooleanExpression whereOrgano = null;
        BooleanExpression whereTipoOrgano = null;
        BooleanExpression whereDescriptor = null;
        BooleanExpression whereClave = null;

        if (anyo != null) {
            whereAnyo = qReunion.fecha.year().eq(anyo);
        }

        if (organoId != null) {
            whereOrgano = qOrganoReunion.organoId.eq(String.valueOf(organoId));
        }

        if (tipoOrganoId != null) {
            whereTipoOrgano = qOrganoReunion.tipoOrganoId.eq(tipoOrganoId);
        }

        if (claveId != null) {
            whereClave = qClave.id.eq(claveId);
        }

        if (descriptorId != null) {
            whereDescriptor = qDescriptor.id.eq(descriptorId);
        }

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qReunion)
                .leftJoin(qReunion.reunionPuntosOrdenDia, qPuntoOrdenDia)
                .leftJoin(qReunion.reunionOrganos, qOrganoReunion);

        if (claveId != null || descriptorId != null) {
            query.join(qPuntoOrdenDia.puntoOrdenDiaDescriptores, qPuntoOrdenDiaDescriptor)
                    .join(qPuntoOrdenDiaDescriptor.clave, qClave)
                    .leftJoin(qClave.descriptor, qDescriptor);
        }

        query.where(qReunion.publica.isTrue()
                .and(qReunion.completada.isTrue())
                .and(whereDescriptor)
                .and(whereClave)
                .and(whereTipoOrgano)
                .and(whereOrgano)
                .and(whereAnyo));

        if (fInicio != null) {
            query.where(qReunion.fecha.goe(fInicio));
        }

        if (fFin != null) {
            query.where(qReunion.fecha.lt(add1Day(fFin)));
        }

        if (!Strings.isNullOrEmpty(texto)) {
            String textoSinAcentosLike = "%" + StringUtils.limpiaAcentos(texto).toLowerCase() + "%";
            if (idiomaAlternativo) {
                query.where(qReunion.asuntoAlternativo.like(textoSinAcentosLike)
                        .or((qReunion.descripcionAlternativa.like(
                                textoSinAcentosLike)
                                .or(qPuntoOrdenDia.tituloAlternativo.like(
                                        textoSinAcentosLike)
                                        .or(qPuntoOrdenDia.descripcionAlternativa.like(
                                                textoSinAcentosLike)
                                                .or(qPuntoOrdenDia.acuerdosAlternativos.like(
                                                        textoSinAcentosLike)
                                                        .or(qPuntoOrdenDia.deliberacionesAlternativas.like(
                                                                textoSinAcentosLike))))))));
            } else {
                query.where(qReunion.asunto.like(textoSinAcentosLike)
                        .or((qReunion.descripcion.like(textoSinAcentosLike)
                                .or(qPuntoOrdenDia.titulo.like(textoSinAcentosLike)
                                        .or(qPuntoOrdenDia.descripcion.like(
                                                textoSinAcentosLike)
                                                .or(qPuntoOrdenDia.acuerdos.like(
                                                        textoSinAcentosLike)
                                                        .or(qPuntoOrdenDia.deliberaciones.like(
                                                                textoSinAcentosLike))))))));
            }
        }

        return query;
    }

    private Date add1Day(Date fecha)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.add(Calendar.DATE, 1);

        return c.getTime();
    }

    @Transactional
    public void deleteByReunionId(Long reunionId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qOrganoReunionMiembro);
        deleteClause.where(qOrganoReunionMiembro.reunionId.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qOrganoReunionInvitado);
        deleteClause.where(qOrganoReunionInvitado.reunionId.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qOrganoReunion);
        deleteClause.where(qOrganoReunion.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionDocumento);
        deleteClause.where(qReunionDocumento.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionComentario);
        deleteClause.where(qReunionComentario.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunionInvitado);
        deleteClause.where(qReunionInvitado.reunion.id.eq(reunionId)).execute();

        deleteClause = new JPADeleteClause(entityManager, qReunion);
        deleteClause.where(qReunion.id.eq(reunionId)).execute();
    }

    public List<OrganoReunion> getOrganosReunionByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunion).where(qOrganoReunion.reunion.id.eq(reunionId)).list(qOrganoReunion);
    }

    public List<Persona> getInvitadosPresencialesByReunionId(Long reunionId)
    {
        JPAQuery queryReunionesInvitados = new JPAQuery(entityManager);
        JPAQuery queryOrganosInvitados = new JPAQuery(entityManager);

        List<Persona> personas = new ArrayList<>();

        List<ReunionInvitado> invitadosPorReunion = queryReunionesInvitados.from(qReunionInvitado)
                .where(qReunionInvitado.reunion.id.eq(reunionId))
                .list(qReunionInvitado);

        List<OrganoReunionInvitado> invitadosPorOrgano = queryOrganosInvitados.from(qOrganoReunionInvitado)
                .where(qOrganoReunionInvitado.reunionId.in(reunionId).and(qOrganoReunionInvitado.soloConsulta.eq(false)))
                .orderBy(qOrganoReunionInvitado.orden.asc(), qOrganoReunionInvitado.id.asc())
                .distinct()
                .list(qOrganoReunionInvitado);

        addToPersonasListFromOrganoInvitados(personas, invitadosPorOrgano);
        addToPersonasListFromReunionInvitados(personas, invitadosPorReunion);

        return personas;
    }

    public List<Persona> getInvitadosByReunionId(Long reunionId)
    {
        JPAQuery queryReunionesInvitados = new JPAQuery(entityManager);
        JPAQuery queryOrganosInvitados = new JPAQuery(entityManager);

        List<Persona> personas = new ArrayList<>();

        List<ReunionInvitado> invitadosPorReunion = queryReunionesInvitados.from(qReunionInvitado)
                .where(qReunionInvitado.reunion.id.eq(reunionId))
                .list(qReunionInvitado);

        List<OrganoReunionInvitado> invitadosPorOrgano = queryOrganosInvitados.from(qOrganoReunionInvitado)
                .where(qOrganoReunionInvitado.reunionId.in(reunionId))
                .distinct()
                .list(qOrganoReunionInvitado);

        addToPersonasListFromReunionInvitados(personas, invitadosPorReunion);
        addToPersonasListFromOrganoInvitados(personas, invitadosPorOrgano);

        return personas;
    }

    public void addToPersonasListFromReunionInvitados(List<Persona> personas, List<ReunionInvitado> invitados)
    {
        personas.addAll(invitados.stream()
                .filter(i -> !personaContainsId(personas, i.getPersonaId()))
                .map(i -> toPersona(i))
                .collect(Collectors.toList()));
    }

    public Persona toPersona(ReunionInvitado reunionInvitado)
    {
        Persona persona = new Persona();

        persona.setId(reunionInvitado.getPersonaId());
        persona.setEmail(reunionInvitado.getPersonaEmail());
        persona.setNombre(reunionInvitado.getPersonaNombre());

        return persona;
    }

    public void addToPersonasListFromOrganoInvitados(List<Persona> personas, List<OrganoReunionInvitado> invitados)
    {
        personas.addAll(invitados.stream()
                .filter(i -> !personaContainsId(personas, ParamUtils.parseLong(i.getPersonaId())))
                .map(i -> toPersona(i))
                .collect(Collectors.toList()));
    }

    public Persona toPersona(OrganoReunionInvitado organoReunionInvitado)
    {
        Persona persona = new Persona();

        persona.setId(ParamUtils.parseLong(organoReunionInvitado.getPersonaId()));
        persona.setEmail(organoReunionInvitado.getEmail());
        persona.setNombre(organoReunionInvitado.getNombre());
        persona.setDescripcion(organoReunionInvitado.getDescripcion());
        persona.setDescripcionAlternativa(organoReunionInvitado.getDescripcionAlternativa());

        return persona;
    }

    public boolean personaContainsId(List<Persona> personas, Long id)
    {
        return personas.stream().anyMatch(p -> id.equals(p.getId()));
    }

    public void updateAcuerdoPuntoDelDiaUrlActa(RespuestaFirmaPuntoOrdenDiaAcuerdo acuerdo)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);

        update.set(qPuntoOrdenDia.urlActa, acuerdo.getUrlActa())
                .set(qPuntoOrdenDia.urlActaAlternativa, acuerdo.getUrlActaAlternativa())
                .where(qPuntoOrdenDia.id.eq(acuerdo.getId()));

        update.execute();
    }

    public void updateAsistenciaMiembrosYSuplentes(Long reunionId, RespuestaFirmaAsistencia respuestaFirmaAsistencia)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.urlAsistencia, respuestaFirmaAsistencia.getUrlAsistencia())
                .set(qOrganoReunionMiembro.urlAsistenciaAlternativa,
                        respuestaFirmaAsistencia.getUrlAsistenciaAlternativa())
                .where(qOrganoReunionMiembro.asistencia.isTrue()
                        .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                        .and((qOrganoReunionMiembro.miembroId.eq(Long.parseLong(respuestaFirmaAsistencia.getPersonaId()))
                                .and(qOrganoReunionMiembro.suplenteId.isNull())).or(qOrganoReunionMiembro.suplenteId.eq(
                                Long.parseLong(respuestaFirmaAsistencia.getPersonaId()))
                                .and(qOrganoReunionMiembro.suplenteId.isNotNull()))));

        update.execute();
    }

    public void updateAsistenciaInvitadoReunion(Long reunionId, RespuestaFirmaAsistencia respuestaFirmaAsistencia)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionInvitado);

        update.set(qReunionInvitado.urlAsistencia, respuestaFirmaAsistencia.getUrlAsistencia())
                .set(qReunionInvitado.urlAsistenciaAlternativa, respuestaFirmaAsistencia.getUrlAsistenciaAlternativa())
                .where(qReunionInvitado.reunion.id.eq(reunionId)
                        .and(qReunionInvitado.personaId.eq(Long.parseLong(respuestaFirmaAsistencia.getPersonaId()))));

        update.execute();
    }

    public void updateAsistenciaInvitadoOrgano(Long reunionId, RespuestaFirmaAsistencia respuestaFirmaAsistencia)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionInvitado);

        update.set(qOrganoReunionInvitado.urlAsistencia, respuestaFirmaAsistencia.getUrlAsistencia())
                .set(qOrganoReunionInvitado.urlAsistenciaAlternativa, respuestaFirmaAsistencia.getUrlAsistenciaAlternativa())
                .where(qOrganoReunionInvitado.reunionId.eq(reunionId)
                        .and(qOrganoReunionInvitado.personaId.eq(Long.parseLong(respuestaFirmaAsistencia.getPersonaId()))));

        update.execute();
    }

    @Transactional
    public ReunionHojaFirmas getHojaFirmasByReunionId(Long reunionid)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qReunionHojaFirmas)
            .where(qReunionHojaFirmas.reunion.id.eq(reunionid))
            .uniqueResult(qReunionHojaFirmas);
    }

    @Transactional
    public void deleteHojaFirmasAnterior(Long reunionId)
    {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qReunionHojaFirmas);
        deleteClause.where(qReunionHojaFirmas.reunion.id.eq(reunionId))
            .execute();
    }

    public Reunion getUltimaReunionByReunionIdOrganos(
        Long reunionId,
        List<String> organosReunion
    )
    {
        Reunion reunion = getReunionById(reunionId);
        ListIterator<String> iterator = organosReunion.listIterator();
        Long idUltimaReunion = null;
        if (iterator.hasNext()) {
            BooleanExpression be = qOrganoReunion.organoId.eq(iterator.next());

            while (iterator.hasNext()) {
                be = be.or(qOrganoReunion.organoId.eq(iterator.next()));
            }

            JPAQuery query = new JPAQuery(entityManager);
            idUltimaReunion = query.from(qReunion).join(qReunion.reunionOrganos, qOrganoReunion)
                .where(be.and(qReunion.fecha.before(reunion.getFecha()))).groupBy(qReunion.id, qReunion.fecha)
                .having(qReunion.id.count().eq(Long.valueOf(organosReunion.size()))).orderBy(qReunion.fecha.desc())
                .singleResult(qReunion.id);
        }
        return idUltimaReunion != null ? getReunionById(idUltimaReunion) : null;
    }

    public List<ReunionExternos> getExternosFromReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<ReunionExternos> idsExternosInvitados = query.from(qReunionExternos)
                .where(qReunionExternos.reunionId.eq(reunionId))
                .list(qReunionExternos);
        return idsExternosInvitados;
    }

    @Transactional
    public void updateConvocante(Long reunionId, String convocante, String convocanteEmail)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);

        update.set(qReunion.convocante, convocante).set(qReunion.convocanteEmail, convocanteEmail)
                .where(qReunion.id.eq(reunionId));
        update.execute();
    }

    public void deleteResponsableActa(String personaId)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);
        JPASubQuery subQuery = new JPASubQuery();
        ListSubQuery<Long> miembroIds = subQuery.from(qOrganoReunionMiembro).where(qOrganoReunionMiembro.miembroId.eq(Long.parseLong(personaId))).list(qOrganoReunionMiembro.id);
        update.setNull(qReunion.miembroResponsableActa)
                .where(qReunion.miembroResponsableActa.id.in(miembroIds))
                .execute();
    }

    public Boolean isAutorizadoEnReunion(Reunion reunion, Long personaId)
    {
        ArrayList<String> organosId = new ArrayList<>();
        reunion.getReunionOrganos().forEach(organoReunion -> organosId.add(organoReunion.getOrganoId()));
        JPAQuery query = new JPAQuery(entityManager);

        List<OrganoAutorizado> organoAutorizado = query.from(qOrganoAutorizado)
                .where(qOrganoAutorizado.organoId.in(organosId)
                        .and(qOrganoAutorizado.personaId.eq(personaId)))
                .list(qOrganoAutorizado);
        return (organoAutorizado.size() > 0);
    }

    public String getConvocanteEmailByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qReunion).where(qReunion.id.eq(reunionId)).uniqueResult(qReunion.convocanteEmail);
    }

    @Transactional
    public void updateCompletada(Long reunionId, Boolean estado)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);

        update.set(qReunion.completada, estado)
              .where(qReunion.id.eq(reunionId));
        update.execute();
    }

    @Transactional
    public void updateReabierta(Long reunionId, Boolean reabierta)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunion);

        update.set(qReunion.reabierta, reabierta)
                .where(qReunion.id.eq(reunionId));
        update.execute();
    }

    @Transactional
    public void reseteaEstadoEditadoPuntosOrdenDiaByreunionId(Long reunionId, Boolean estado)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);

        update.set(qPuntoOrdenDia.editado, estado)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId));
        update.execute();
    }

    @Transactional
    public void updateAsistenciaInvitadoReunionAsincrona(
        Long reunionId,
        Long personaId,
        String urlAsistencia
    )
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionInvitado);

        update.set(qReunionInvitado.urlAsistencia, urlAsistencia)
            .where(qReunionInvitado.reunion.id.eq(reunionId)
                .and(qReunionInvitado.personaId.eq(personaId)));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaInvitadoOrganoReunionAsincrona(
        Long reunionId,
        Long personaId,
        String urlAsistencia
    )
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionInvitado);

        update.set(qOrganoReunionInvitado.urlAsistencia, urlAsistencia)
            .where(qOrganoReunionInvitado.reunionId.eq(reunionId)
                .and(qOrganoReunionInvitado.personaId.eq(personaId)));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaMiembrosYSuplentesAsincrona(
        Long reunionId,
        Long personaId,
        String urlAsistencia
    )
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.urlAsistencia, urlAsistencia)
            .where(qOrganoReunionMiembro.asistencia.isTrue()
                .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                .and((qOrganoReunionMiembro.miembroId.eq(personaId)
                    .and(qOrganoReunionMiembro.suplenteId.isNull())).or(qOrganoReunionMiembro.suplenteId.eq(personaId)
                    .and(qOrganoReunionMiembro.suplenteId.isNotNull()))));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaAlternativaInvitadoReunionAsincrona(
        Long reunionId,
        Long personaId,
        String urlAsistenciaAlternativa
    )
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qReunionInvitado);

        update.set(qReunionInvitado.urlAsistenciaAlternativa, urlAsistenciaAlternativa)
            .where(qReunionInvitado.reunion.id.eq(reunionId)
                .and(qReunionInvitado.personaId.eq(personaId)));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaAlternativaInvitadoOrganoReunionAsincrona(
        Long reunionId,
        Long personaId,
        String urlAsistenciaAlternativa
    )
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionInvitado);

        update.set(qOrganoReunionInvitado.urlAsistenciaAlternativa, urlAsistenciaAlternativa)
            .where(qOrganoReunionInvitado.reunionId.eq(reunionId)
                .and(qOrganoReunionInvitado.personaId.eq(personaId)));

        update.execute();
    }

    @Transactional
    public void updateAsistenciaAlternativaMiembrosYSuplentesAsincrona(
        Long reunionId,
        Long personaId,
        String urlAsistenciaAlternativa
    )
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qOrganoReunionMiembro);

        update.set(qOrganoReunionMiembro.urlAsistenciaAlternativa, urlAsistenciaAlternativa)
            .where(qOrganoReunionMiembro.asistencia.isTrue()
                .and(qOrganoReunionMiembro.reunionId.eq(reunionId))
                .and((qOrganoReunionMiembro.miembroId.eq(personaId)
                    .and(qOrganoReunionMiembro.suplenteId.isNull())).or(qOrganoReunionMiembro.suplenteId.eq(personaId)
                    .and(qOrganoReunionMiembro.suplenteId.isNotNull()))));

        update.execute();
    }

    public Long getReunionIdByPuntoId(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId)).singleResult(qPuntoOrdenDia.reunion.id);
    }

    public Reunion getReunionByPuntoId(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId)).singleResult(qPuntoOrdenDia.reunion);
    }

    public boolean isConnectedUserVotanteInReunion(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.miembroId.eq(connectedUserId))
                    .and(qOrganoReunionMiembro.votante.isTrue())).exists();
    }

    public boolean isConnectedUserPresideVotacionInReunion(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
            .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                .and(qOrganoReunionMiembro.miembroId.eq(connectedUserId))
                .and(qOrganoReunionMiembro.presideVotacion.isTrue())).exists();
    }

    public boolean isConnectedUserSuplenteOrDelegadoVotoInReunion(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.suplenteId.eq(connectedUserId)
                                .or(qOrganoReunionMiembro.delegadoVotoId.eq(connectedUserId)))
                    .and(qOrganoReunionMiembro.votante.isTrue())).exists();
    }

    public boolean connectedUserHasSuplenteOrDelegatedVote(Long reunionId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro)
                .where(qOrganoReunionMiembro.reunionId.eq(reunionId)
                        .and(qOrganoReunionMiembro.miembroId.eq(connectedUserId)
                                .and(qOrganoReunionMiembro.delegadoVotoId.isNotNull().or(qOrganoReunionMiembro.suplenteId.isNotNull())))).exists();
    }

    public boolean isVotacionPublicaByPuntoId(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId)).singleResult(qPuntoOrdenDia.votoPublico.coalesce(false));
    }

    public boolean isAutorizadoEnReunionByPuntoId(Long puntoOrdenDiaId, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia)
                .join(qOrganoReunion).on(qPuntoOrdenDia.reunion.id.eq(qOrganoReunion.reunion.id))
                .join(qOrganoAutorizado).on(qOrganoAutorizado.organoId.eq(qOrganoReunion.organoId))
                .where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId).and(qOrganoAutorizado.personaId.eq(connectedUserId))).exists();
    }

    public boolean isCreadorReunion(Long puntoId, Long personaId)
    {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        return jpaQuery.from(qReunion).join(qPuntoOrdenDia).on(qPuntoOrdenDia.reunion.id.eq(qReunion.id))
                .where(qReunion.creadorId.eq(personaId).and(qPuntoOrdenDia.id.eq(puntoId))).exists();
    }

    public Date getReunionFecha(Long reunionId)
    {
        JPAQuery jpaQuery = new JPAQuery(entityManager);
        return jpaQuery.from(qReunion)
                .where(qReunion.id.eq(reunionId))
                .singleResult(qReunion.fecha);
    }

    public void setPuntosFechaAperturaVotacion(Long reunionId, Date FechaAperturaVotacion)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        update.set(qPuntoOrdenDia.fechaAperturaVotacion, FechaAperturaVotacion)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId));
        update.execute();
    }

    public Boolean hasVotacion(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qReunion).where(qReunion.id.eq(reunionId)).singleResult(qReunion.hasVotacion);
    }
}
