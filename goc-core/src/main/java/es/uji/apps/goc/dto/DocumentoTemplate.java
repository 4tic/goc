package es.uji.apps.goc.dto;

import java.util.Date;

public class DocumentoTemplate
{
    private Long id;
    private String descripcion;
    private String mimeType;
    private String nombreFichero;
    private Boolean publico;
    private Date fechaAdicion;
    private Long creadorId;
    private String urlActa;
    private String urlActaAlternativa;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getNombreFichero()
    {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero)
    {
        this.nombreFichero = nombreFichero;
    }

    public Date getFechaAdicion()
    {
        return fechaAdicion;
    }

    public void setFechaAdicion(Date fechaAdicion)
    {
        this.fechaAdicion = fechaAdicion;
    }

    public Long getCreadorId() {
        return creadorId;
    }

    public void setCreadorId(Long creadorId) {
        this.creadorId = creadorId;
    }

    public Boolean getPublico()
    {
        return publico;
    }

    public void setPublico(Boolean publico)
    {
        this.publico = publico;
    }

    public String getUrlActa() {
        return urlActa;
    }

    public void setUrlActa(String urlActa) {
        this.urlActa = urlActa;
    }

    public String getUrlActaAlternativa() {
        return urlActaAlternativa;
    }

    public void setUrlActaAlternativa(String urlActaAlternativa) {
        this.urlActaAlternativa = urlActaAlternativa;
    }
}
