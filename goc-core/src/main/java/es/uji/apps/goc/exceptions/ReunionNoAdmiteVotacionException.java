package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class ReunionNoAdmiteVotacionException extends CoreDataBaseException {
    public ReunionNoAdmiteVotacionException()
    {
        super("appI18N.excepciones.reunionNoAdmiteVotacion");
    }

    public ReunionNoAdmiteVotacionException(String message)
    {
        super(message);
    }

}
