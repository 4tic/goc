package es.uji.apps.goc.dto;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_ORGANOS_LOGO")
public class OrganoLogo
{
    @EmbeddedId
    OrganoLogoPK organoLogoPK;

    @Lob
    @Column(name = "IMAGEN_BASE64")
    String data;

    @Column(name = "EXTENSION")
    String extension;

    public OrganoLogoPK getOrganoLogoPK()
    {
        return organoLogoPK;
    }

    public void setOrganoLogoPK(OrganoLogoPK organoLogoPK)
    {
        this.organoLogoPK = organoLogoPK;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public String getExtension()
    {
        return extension;
    }

    public void setExtension(String extension)
    {
        this.extension = extension;
    }
}
