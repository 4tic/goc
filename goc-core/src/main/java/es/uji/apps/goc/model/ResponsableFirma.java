package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.Firmante;

public class ResponsableFirma
{
    private String id;
    private String nombre;
    private String email;
    private String organo;
    private String organoAlternativo;
    private String cargo;
    private String cargoCodigo;
    private Boolean suplente;
    private String nombreDeSustituido;

    public  ResponsableFirma(){
    }

    public ResponsableFirma(Firmante miembroFirma, String nombreOrgano, String nombreOrganoAlternativo, Boolean suplente, String nombreDeSustituido) {
        this.setId(miembroFirma.getId());
        this.setNombre(miembroFirma.getNombre());
        this.setEmail(miembroFirma.getEmail());
        this.setOrgano(nombreOrgano);
        this.setOrganoAlternativo(nombreOrganoAlternativo);
        this.setCargo(miembroFirma.getCargoNombreDescripcion());
        this.setCargoCodigo(miembroFirma.getCargoCodigo());
        this.suplente = suplente;
        this.nombreDeSustituido = nombreDeSustituido;
    }

    public ResponsableFirma(Miembro miembro, Boolean suplente, String nombreDeSustituido) {
        this.setId(miembro.getId().toString());
        this.setNombre(miembro.getNombre());
        this.suplente = suplente;
        this.nombreDeSustituido = nombreDeSustituido;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getOrgano()
    {
        return organo;
    }

    public void setOrgano(String organo)
    {
        this.organo = organo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getOrganoAlternativo()
    {
        return organoAlternativo;
    }

    public void setOrganoAlternativo(String organoAlternativo)
    {
        this.organoAlternativo = organoAlternativo;
    }

    public String getCargo()
    {
        return cargo;
    }

    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

    public Boolean getSuplente() {
        return suplente;
    }

    public void setSuplente(Boolean suplente) {
        this.suplente = suplente;
    }

    public String getNombreDeSustituido() {
        return nombreDeSustituido;
    }

    public void setNombreDeSustituido(String nombreDeSustituido) {
        this.nombreDeSustituido = nombreDeSustituido;
    }

    public void setCargoCodigo(String cargoCodigo) {
        this.cargoCodigo = cargoCodigo;
    }

    public String getCargoCodigo() {
        return cargoCodigo;
    }
}
