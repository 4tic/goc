package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.Reunion;

import java.util.List;

public class BuscadorReunionesWrapper {
    private List<Reunion> reuniones;
    private Long numeroReuniones;

    public BuscadorReunionesWrapper()
    {
    }

    public BuscadorReunionesWrapper(List<Reunion> reuniones, Long numeroReuniones)
    {
        this.reuniones = reuniones;
        this.numeroReuniones = numeroReuniones;
    }

    public List<Reunion> getReuniones()
    {
        return reuniones;
    }

    public void setReuniones(List<Reunion> reuniones)
    {
        this.reuniones = reuniones;
    }

    public Long getNumeroReuniones()
    {
        return numeroReuniones;
    }

    public void setNumeroReuniones(Long numeroReuniones)
    {
        this.numeroReuniones = numeroReuniones;
    }
}

