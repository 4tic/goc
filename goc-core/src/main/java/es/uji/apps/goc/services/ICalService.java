package es.uji.apps.goc.services;

import com.google.common.base.Strings;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.ParameterList;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.Method;
import net.fortuna.ical4j.model.property.Name;
import net.fortuna.ical4j.model.property.Organizer;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.TzId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.XProperty;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import es.uji.apps.goc.dto.Reunion;

@Component
public class ICalService {
    public final String ZONE = "Europe/Madrid";

    @Value("${goc.tituloDeAplicacion:}")
    public String tituloDeAplicacion;

    VTimeZone vTimeZone;
    TimeZone timeZone;

    public ICalService() {
        this.vTimeZone = new VTimeZone();
        vTimeZone.getProperties().add(new TzId(ZONE));
        this.timeZone = new TimeZone(vTimeZone);
    }

    public Calendar creaICal(Reunion reunion) throws URISyntaxException {
        return creaICal(Arrays.asList(reunion), null);
    }

    public Calendar creaICal(List<Reunion> reuniones, String calName) throws URISyntaxException {
        Calendar icsCalendar = createVcalendar(calName);
        for(Reunion reunion : reuniones)
        {
            VEvent meeting = createEvent(reunion);
            icsCalendar.getComponents().add(meeting);
        }
        return icsCalendar;
    }

    private VEvent createEvent(Reunion reunion) throws URISyntaxException {
        String asunto = reunion.getAsunto();
        DateTime start = null;
        DateTime end = null;
        if (reunion.getFecha() != null && reunion.getDuracion() != null) {
            LocalDateTime startDate = LocalDateTime.ofInstant(reunion.getFecha().toInstant(), this.timeZone.toZoneId());
            start = new DateTime(Date.from(startDate.atZone(this.timeZone.toZoneId()).toInstant()));
            start.setTimeZone(null);
            LocalDateTime endDate = LocalDateTime.ofInstant(reunion.getFecha().toInstant(), this.timeZone.toZoneId()).plusMinutes(reunion.getDuracion());
            end = new DateTime(Date.from(endDate.atZone(this.timeZone.toZoneId()).toInstant()));
            end.setTimeZone(null);
        }
        VEvent meeting = new VEvent(start, end, asunto);
        Uid uid = new Uid(reunion.getId() + "@goc.com");
        meeting.getProperties().add(uid);
        meeting.getProperties().add(new Location(reunion.getUbicacion()));
        ParameterList parameterList = new ParameterList();
        parameterList.add(new Cn(reunion.getCreadorNombre()));
        if (reunion.getCreadorEmail() != null && reunion.getCreadorNombre() != null) {
            Organizer organizer = new Organizer(parameterList, reunion.getCreadorEmail());
            meeting.getProperties().add(organizer);
        }
        return meeting;
    }

    private Calendar createVcalendar(String calName) {
        Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
        icsCalendar.getProperties().add(new ProdId(String.format("-//%s//Calendario reuniones 1.0//EN", tituloDeAplicacion)));
        icsCalendar.getProperties().add(CalScale.GREGORIAN);
        if (!Strings.isNullOrEmpty(calName)) {
            icsCalendar.getProperties().add(new XProperty("X-WR-CALNAME", calName));
            icsCalendar.getProperties().add(new Name(calName));
        }
        icsCalendar.getProperties().add(Method.PUBLISH);
        icsCalendar.getComponents().add(this.vTimeZone);
        return icsCalendar;
    }
}
