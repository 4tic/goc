package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;

import es.uji.apps.goc.dto.PuntoOrdenDiaComentario;
import es.uji.apps.goc.dto.QPuntoOrdenDiaComentario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PuntoOrdenDiaComentarioDAO extends BaseDAODatabaseImpl
{
    QPuntoOrdenDiaComentario qPuntoOrdenDiaComentario = QPuntoOrdenDiaComentario.puntoOrdenDiaComentario;

    public List<PuntoOrdenDiaComentario> getComentariosByPuntoId(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDiaComentario> list = query.from(qPuntoOrdenDiaComentario).where(
            qPuntoOrdenDiaComentario.puntoOrdenDia.id.eq(puntoOrdenDiaId)
                .and(qPuntoOrdenDiaComentario.comentarioPadre.isNull())).list(qPuntoOrdenDiaComentario);
        cargarRespuestas(list);
        return list;
    }

    private void cargarRespuestas(List<PuntoOrdenDiaComentario> list)
    {
        for(PuntoOrdenDiaComentario puntoOrdenDiaComentario : list)
        {
            List<PuntoOrdenDiaComentario> respuestas = getRespuestasByComentarioPadreId(puntoOrdenDiaComentario.getId());
            if(respuestas != null && !respuestas.isEmpty())
            {
                cargarRespuestas(respuestas);
                puntoOrdenDiaComentario.setRespuestas(new HashSet<>(respuestas));
            }
        }
    }

    private List<PuntoOrdenDiaComentario> getRespuestasByComentarioPadreId(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDiaComentario)
            .where(qPuntoOrdenDiaComentario.comentarioPadre.id.eq(id))
            .list(qPuntoOrdenDiaComentario);
    }

    public PuntoOrdenDiaComentario getComentarioById(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDiaComentario)
            .where(qPuntoOrdenDiaComentario.id.eq(id))
            .singleResult(qPuntoOrdenDiaComentario);
    }
}
