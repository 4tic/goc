package es.uji.apps.goc.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.apps.goc.dto.QReunion;
import es.uji.apps.goc.model.Miembro;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class OrganoReunionDAO extends BaseDAODatabaseImpl
{
    private QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;

    @Autowired
    OrganoReunionMiembroDAO organoReunionMiembroDAO;

    @Autowired
    MiembroDAO miembroDAO;

    @Autowired
    OrganoDAO organoDAO;

    public OrganoReunion getOrganoReunionByReunionIdAndOrganoExternoId(Long reunionId,
            String organoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<OrganoReunion> resultado = query.from(qOrganoReunion)
                .where(qOrganoReunion.reunion.id.eq(reunionId).and(
                        qOrganoReunion.organoId.eq(organoId).and(qOrganoReunion.externo.eq(true))))
                .list(qOrganoReunion);

        if (resultado.size() == 0)
        {
            return null;
        }

        return resultado.get(0);
    }

    public OrganoReunion getOrganoReunionByReunionIdAndOrganoLocalId(Long reunionId, Long organoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<OrganoReunion> resultado = query.from(qOrganoReunion)
                .where(qOrganoReunion.reunion.id.eq(reunionId).and(qOrganoReunion.organoId
                        .eq(organoId.toString()).and(qOrganoReunion.externo.eq(false))))
                .list(qOrganoReunion);

        if (resultado.size() == 0)
        {
            return null;
        }

        return resultado.get(0);
    }

    public OrganoReunion getOrganoReunionByReunionIdAndOrganoId(Long reunionId, Long organoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<OrganoReunion> resultado = query.from(qOrganoReunion)
                .where(qOrganoReunion.reunion.id.eq(reunionId).and(qOrganoReunion.organoId
                        .eq(organoId.toString())))
                .list(qOrganoReunion);

        if (resultado.size() == 0)
        {
            return null;
        }

        return resultado.get(0);
    }

    public List<OrganoReunion> getOrganosReunionNoCompletadasAndFechaReunionAfterTodayByOrganoId(Long organoLocalId)
    {
        Date today = new Date();
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qOrganoReunion).where(qOrganoReunion.organoId.eq(organoLocalId.toString())
            .and(qOrganoReunion.externo.eq(false)).and(qOrganoReunion.reunion.completada.isFalse())
                .and(qOrganoReunion.reunion.fecha.after(today)
                        .or(qOrganoReunion.reunion.fecha.eq(today)))).list(qOrganoReunion);
    }

    @Transactional
    public void updateOrdenYGrupoMiembrosGrupos(Long organoId, List<Miembro> miembros) {
        List<OrganoReunion> organosReunionesNoCompletadas = getOrganosReunionNoCompletadasAndFechaReunionAfterTodayByOrganoId(organoId);

        if (organosReunionesNoCompletadas != null && miembros != null) {
            for (OrganoReunion organoReunionNoCompletada : organosReunionesNoCompletadas) {
                for (Miembro miembro: miembros)
                    organoReunionMiembroDAO.updateReunionMiembroWithMiembro(organoReunionNoCompletada.getId(), miembro.getPersonaId().toString(), null, miembro);
            }
        }
    }

    @Transactional
    public List<Long> getIdsReunionesNoConvocadasByOrganoId(Long organoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QReunion qReunion = new QReunion("qReunion");
        return query
                .from(qOrganoReunion).join(qOrganoReunion.reunion, qReunion)
                .where(qOrganoReunion.organoId.eq(organoId.toString())
                .and(qReunion.completada.isFalse())
                .and(qReunion.avisoPrimeraReunion.isFalse()))
                .distinct()
                .list(qReunion.id);
    }

    @Transactional
    public void updateOrdenEnReunionesNoCompletadas(Long miembroId) {
        Long organoId = organoDAO.getOrganoIdByMiembroId(miembroId);

        List<Miembro> miembros = miembroDAO.getMiembrosByOrganoId(organoId);
        updateOrdenYGrupoMiembrosGrupos(organoId, miembros);
    }
}