package es.uji.apps.goc.dao;

import com.google.common.base.Strings;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.QMiembroLocal;
import es.uji.apps.goc.dto.QOrganoLocal;
import es.uji.apps.goc.dto.QOrganoReunionMiembro;
import es.uji.apps.goc.exceptions.MiembroExisteException;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class MiembroDAO extends BaseDAODatabaseImpl
{
    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.external.miembrosEndpoint}")
    private String miembrosExternosEndpoint;

    @Autowired
    OrganoReunionDAO organoReunionDAO;

    @Autowired
    CargoDAO cargoDAO;

    public List<Miembro> getMiembros()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;

        List<MiembroLocal> miembrosLocales = query.from(qMiembroLocal).list(qMiembroLocal);

        return creaListaMiembrosDesdeListaMiembrosLocales(miembrosLocales);
    }

    private List<Miembro> creaListaMiembrosDesdeListaMiembrosLocales(List<MiembroLocal> miembrosLocales)
    {
        List<Miembro> listaMiembros = new ArrayList<>();

        for (MiembroLocal miembroLocal : miembrosLocales)
        {
            listaMiembros.add(creaMiembroDesdeMiembroLocal(miembroLocal));
        }

        return listaMiembros;
    }

    private Miembro creaMiembroDesdeMiembroLocal(MiembroLocal miembroLocal)
    {
        Miembro miembro = new Miembro();
        miembro.setId(miembroLocal.getId());
        miembro.setPersonaId(miembroLocal.getPersonaId());
        miembro.setNombre(miembroLocal.getNombre());
        miembro.setEmail(miembroLocal.getEmail());
        miembro.setDescripcion(miembroLocal.getDescripcion());
        miembro.setDescripcionAlternativa(miembroLocal.getDescripcionAlternativa());
        miembro.setOrden(miembroLocal.getOrden());
        miembro.setGrupo(miembroLocal.getGrupo());
        miembro.setOrdenGrupo(miembroLocal.getOrdenGrupo());
        miembro.setFirmante(miembroLocal.getFirmante());
        miembro.setPresideVotacion(miembroLocal.isPresideVotacion());
        miembro.setVotante(miembroLocal.isVotante());

        Organo organo = new Organo(miembroLocal.getOrgano().getId().toString());
        miembro.setOrgano(organo);

        if (miembroLocal.getCargoId() != null)
        {
            Optional<Cargo> cargo = cargoDAO.getCargoById(miembroLocal.getCargoId());
            if (cargo.isPresent()) {
                miembro.setCargo(cargo.get());
                miembro.setCondicion(cargo.get().getNombre());
                miembro.setCondicionAlternativa(cargo.get().getNombreAlternativo());
            }
        }
        return miembro;
    }

    @Transactional
    public List<Miembro> getMiembrosByOrganoId(Long organoId)
    {
        if (organoId == null)
            return Collections.emptyList();

        JPAQuery query = new JPAQuery(entityManager);
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;

        List<MiembroLocal> miembrosLocales =
                query.from(qMiembroLocal).where(qMiembroLocal.organo.id.eq(organoId))
                    .orderBy(qMiembroLocal.ordenGrupo.asc(), qMiembroLocal.orden.asc(), qMiembroLocal.id.asc()).list(qMiembroLocal);

        return creaListaMiembrosDesdeListaMiembrosLocales(miembrosLocales);
    }

    @Transactional(rollbackFor = {MiembroExisteException.class})
    public Miembro insertMiembro(Miembro miembro, Long connectedUserId) throws MiembroExisteException
    {
        MiembroLocal miembroLocal = creaMiembroLocalDesdeMiembro(miembro);
        Long orden = miembro.getOrden();
        Long ultimoOrdenActual =
            getUltimoMiembroConOrdenByOrgano(Long.parseLong(miembro.getOrgano().getId()));
        miembroLocal.setOrden(ultimoOrdenActual != null ? ultimoOrdenActual + 1 : null);

        Long organoLocalId = Long.parseLong(miembro.getOrgano().getId());
        miembroLocal.setOrgano(new OrganoLocal(organoLocalId));

        if(miembro.hasCargo())
        {
            miembroLocal.setCargoId(miembro.getCargo().getId());
        }
        if(miembroExiste(miembro)){
            throw new MiembroExisteException();
        }
        miembroLocal = this.insert(miembroLocal);
        if (orden != null && orden != miembroLocal.getOrden()) {
            miembroLocal.setOrden(mueveMiembro(miembroLocal.getId(), orden.intValue()));
        }
        return creaMiembroDesdeMiembroLocal(miembroLocal);
    }

    public Miembro getMiembroById(Long miembroId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;

        List<MiembroLocal> miembrosLocales = query.from(qMiembroLocal)
                .join(qMiembroLocal.organo, qOrganoLocal)
                .fetch()
                .where(qMiembroLocal.id.eq(miembroId))
                .list(qMiembroLocal);

        if (miembrosLocales.size() == 0)
        {
            return null;
        }

        return creaMiembroDesdeMiembroLocal(miembrosLocales.get(0));
    }

    public Miembro updateMiembro(Miembro miembroOld, Miembro miembro)
    {
        Long orden = miembro.getOrden();

        miembroOld.setEmail(miembro.getEmail());
        miembroOld.setCargo(miembro.getCargo());
        miembroOld.setFirmante(miembro.getFirmante());
        miembroOld.setPresideVotacion(miembro.isPresideVotacion());
        miembroOld.setVotante(miembro.isVotante());
        miembroOld.setDescripcion(miembro.getDescripcion());
        miembroOld.setDescripcionAlternativa(miembro.getDescripcionAlternativa());
        MiembroLocal miembroLocal = creaMiembroLocalDesdeMiembro(miembroOld);
        miembroLocal = this.update(miembroLocal);

        if (orden != null) {
            miembroLocal.setOrden(mueveMiembro(miembroOld.getId(), orden.intValue()));
        }

        return creaMiembroDesdeMiembroLocal(miembroLocal);
    }

    private long mueveMiembro(Long miembroId, int posicion)
    {
        Long posicionInicial = compruebaOrganoOrdenInicializadoByMiembro(miembroId);
        Long posicionActual = posicionInicial;
        Long ultimaPosicionValida = posicionInicial;
        boolean hemosLlegado = posicionActual == posicion;
        while (isValidPosition(posicionActual) && !hemosLlegado) {
            if (posicionActual < posicion) {
                posicionActual = bajaMiembro(miembroId);
                if (isValidPosition(posicionActual)) {
                    ultimaPosicionValida = posicionActual;
                    if (posicionActual >= posicion) {
                        hemosLlegado = true;
                    }
                }
            } else {
                posicionActual = subeMiembro(miembroId);
                if (isValidPosition(posicionActual)) {
                    ultimaPosicionValida = posicionActual;
                    if (posicionActual <= posicion) {
                        hemosLlegado = true;
                    }
                }
            }
        }

        if (!posicionActual.equals(posicionInicial)) {
            organoReunionDAO.updateOrdenEnReunionesNoCompletadas(miembroId);
        }
        return ultimaPosicionValida;
    }

    private boolean isValidPosition(long nuevaPosicion) {
        return nuevaPosicion > -1;
    }

    private MiembroLocal creaMiembroLocalDesdeMiembro(Miembro miembro)
    {
        MiembroLocal miembroLocal = new MiembroLocal();
        miembroLocal.setId(miembro.getId());
        miembroLocal.setPersonaId(miembro.getPersonaId());
        miembroLocal.setNombre(miembro.getNombre());
        miembroLocal.setEmail(miembro.getEmail());
        miembroLocal.setDescripcion(miembro.getDescripcion());
        miembroLocal.setDescripcionAlternativa(miembro.getDescripcionAlternativa());
        miembroLocal.setOrden(miembro.getOrden());
        miembroLocal.setGrupo(miembro.getGrupo());
        miembroLocal.setOrdenGrupo(miembro.getOrdenGrupo());
        miembroLocal.setFirmante(miembro.getFirmante());
        miembroLocal.setPresideVotacion(miembro.isPresideVotacion());
        miembroLocal.setVotante(miembro.isVotante());

        OrganoLocal organoLocal = new OrganoLocal(Long.parseLong(miembro.getOrgano().getId()));
        miembroLocal.setOrgano(organoLocal);

        if(miembro.getCargo() != null && !miembro.getCargo().getId().equals("0"))
        {
            miembroLocal.setCargoId(miembro.getCargo().getId());
        }

        return miembroLocal;
    }

    private Long getUltimoMiembroConOrdenByOrgano(Long organoId)
    {
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qOrganoLocal.id.eq(organoId))
            .singleResult(qMiembroLocal.orden.max());
    }

    private Long getOrdenByMiembro(Long miembroId)
    {
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .where(qMiembroLocal.id.eq(miembroId))
            .singleResult(qMiembroLocal.orden);
    }

    private MiembroLocal getMiembroSuperior(Long miembroId){
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;
        Long ordenMiembro = getOrdenByMiembro(miembroId);
        MiembroLocal miembro = this.get(MiembroLocal.class, miembroId).get(0);
        if(ordenMiembro == null)
        {
            return null;
        }

        JPAQuery queryUltimoOrdenAnterior = new JPAQuery(entityManager);
        Long ultimoOrdenAnterior = queryUltimoOrdenAnterior.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.orden.lt(ordenMiembro)
                .and(qOrganoLocal.id.eq(miembro.getOrgano().getId()))
                .and(!Strings.isNullOrEmpty(miembro.getGrupo()) ? qMiembroLocal.grupo.eq(miembro.getGrupo()) : qMiembroLocal.grupo.isNull().or(qMiembroLocal.grupo.isEmpty()))
            )
            .singleResult(qMiembroLocal.orden.max());

        if(ultimoOrdenAnterior == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.orden.eq(ultimoOrdenAnterior)
                .and(qOrganoLocal.id.eq(miembro.getOrgano().getId())))
            .singleResult(qMiembroLocal);
    }

    private MiembroLocal getMiembroInferior(Long miembroId){
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;
        Long ordenMiembro = getOrdenByMiembro(miembroId);
        MiembroLocal miembro = this.get(MiembroLocal.class, miembroId).get(0);
        if(ordenMiembro == null)
        {
            return null;
        }

        JPAQuery queryUltimoOrdenPosterior = new JPAQuery(entityManager);
        Long ultimoOrdenPosterior = queryUltimoOrdenPosterior.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.orden.gt(ordenMiembro)
                .and(qOrganoLocal.id.eq(miembro.getOrgano().getId()))
                .and(!Strings.isNullOrEmpty(miembro.getGrupo()) ? qMiembroLocal.grupo.eq(miembro.getGrupo()) : qMiembroLocal.grupo.isNull().or(qMiembroLocal.grupo.isEmpty()))
            )
            .singleResult(qMiembroLocal.orden.min());
        if(ultimoOrdenPosterior == null)
        {
            return null;
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qMiembroLocal)
            .join(qMiembroLocal.organo, qOrganoLocal)
            .where(qMiembroLocal.orden.eq(ultimoOrdenPosterior)
                .and(qOrganoLocal.id.eq(miembro.getOrgano().getId())))
            .singleResult(qMiembroLocal);
    }

    @Transactional
    public long subeMiembro(Long miembroId)
    {
        compruebaOrganoOrdenInicializadoByMiembro(miembroId);
        MiembroLocal miembro = this.get(MiembroLocal.class, miembroId).get(0);
        MiembroLocal miembroSuperior = getMiembroSuperior(miembroId);
        if(miembroSuperior != null)
        {
            Long ordenMiembroSuperiorActual = miembroSuperior.getOrden();
            miembroSuperior.setOrden(miembro.getOrden());
            this.update(miembroSuperior);

            miembro.setOrden(ordenMiembroSuperiorActual);
            this.update(miembro);
            return ordenMiembroSuperiorActual != null ? ordenMiembroSuperiorActual : -1;
        }
        return -1;
    }

    @Transactional
    public long bajaMiembro(Long miembroId)
    {
        compruebaOrganoOrdenInicializadoByMiembro(miembroId);
        MiembroLocal miembro = this.get(MiembroLocal.class, miembroId).get(0);
        MiembroLocal miembroInferior = getMiembroInferior(miembroId);
        if(miembroInferior != null)
        {
            Long ordenMiembroInferiorActual = miembroInferior.getOrden();
            miembroInferior.setOrden(miembro.getOrden());
            this.update(miembroInferior);

            miembro.setOrden(ordenMiembroInferiorActual);
            this.update(miembro);
            return ordenMiembroInferiorActual != null ? ordenMiembroInferiorActual : -1;
        }
        return -1;
    }

    private void inicializarOrdenEnOrgano(Long organoId)
    {
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        QOrganoLocal qOrganoLocal = QOrganoLocal.organoLocal;

        JPAQuery query = new JPAQuery(entityManager);
        List<MiembroLocal> listaMiembros =
            query.from(qMiembroLocal).join(qMiembroLocal.organo, qOrganoLocal).where(qOrganoLocal.id.eq(organoId))
                .orderBy(qMiembroLocal.ordenGrupo.asc(), qMiembroLocal.orden.asc(), qMiembroLocal.id.asc())
                .list(qMiembroLocal);

        Long i = 1L;
        for(MiembroLocal miembroLocal : listaMiembros)
        {
            miembroLocal.setOrden(i);
            i++;
            this.update(miembroLocal);
        }
    }

    public Long compruebaOrganoOrdenInicializadoByMiembro(Long miembroId)
    {
        MiembroLocal miembroLocal = this.get(MiembroLocal.class, miembroId).get(0);
        if(miembroLocal.getOrden() == null){
            inicializarOrdenEnOrgano(miembroLocal.getOrgano().getId());
        }
        return this.get(MiembroLocal.class, miembroId).get(0).getOrden();
    }

    public Miembro getMiembroByPersonaIdYOrganoId(Long suplenteId, Long organoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;

        MiembroLocal persona = query.from(qMiembroLocal)
                .where(qMiembroLocal.personaId.eq(suplenteId).and(qMiembroLocal.organo.id.eq(organoId)))
                .singleResult(qMiembroLocal);
        if (persona == null)
        {
            return null;
        }
        return creaMiembroDesdeMiembroLocal(persona);
    }

    public boolean miembroExiste(Miembro miembro)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QMiembroLocal qMiembroLocal = QMiembroLocal.miembroLocal;
        return query.from(qMiembroLocal)
                .where(qMiembroLocal.organo.id.eq(Long.valueOf(miembro.getOrgano().getId()))
                        .and(qMiembroLocal.personaId.eq(miembro.getPersonaId())
                                .and(qMiembroLocal.cargoId.eq(miembro.getCargo().getId())))).exists();

    }

    public List<Tuple> getDelegandosYSuplenciasDeMiembroId(Long miembroId, Long reunionId)
    {
        QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qOrganoReunionMiembro).where((qOrganoReunionMiembro.suplenteId.eq(miembroId).or(qOrganoReunionMiembro.delegadoVotoId.eq(miembroId))
                .and(qOrganoReunionMiembro.reunionId.eq(reunionId)))).list(qOrganoReunionMiembro.miembroId, qOrganoReunionMiembro.nombre);
    }

    @Transactional
    public void delete(Miembro miembro) {
        this.delete(MiembroLocal.class, miembro.getId());
        if ( miembro.getOrden() != null && miembro.getOrgano() != null) {
            inicializarOrdenEnOrgano(Long.parseLong(miembro.getOrgano().getId()));
        }
    }
}
