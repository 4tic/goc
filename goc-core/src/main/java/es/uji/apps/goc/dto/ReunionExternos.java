package es.uji.apps.goc.dto;

import javax.persistence.*;

@Entity
@Table(name = "GOC_REUNIONES_EXTERNOS")
public class ReunionExternos {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="REUNION_ID")
    private long reunionId;

    @Column(name="PERSONA_ID")
    private long personaId;

    @Column(name="NOMBRE")
    private String nombre;

    @Column(name="EMAIL")
    private String email;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getReunionId() {
        return reunionId;
    }

    public void setReunionId(long reunionId) {
        this.reunionId = reunionId;
    }

    public long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(long personaId) {
        this.personaId = personaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email.trim();
    }

    public void setEmail(String email) {
        this.email = email.trim();
    }
}