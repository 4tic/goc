package es.uji.apps.goc.notifications;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.Reunion;

public class ReunionFormatter
{
    private final SimpleDateFormat formatter;
    private Reunion reunion;
    private ResourceBundleUTF8 resourceBundle;
    private List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados;

    public ReunionFormatter(Reunion reunion, List<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados, String locale)
    {
        this.formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        this.reunion = reunion;
        this.puntosOrdenDiaOrdenados = puntosOrdenDiaOrdenados;
        this.resourceBundle = new ResourceBundleUTF8("i18nEmails", locale);
    }

    public String format(String publicUrl, String textoAux, String textoConvocatoria)
    {
        StringBuffer content = new StringBuffer();

        content.append("<h2>" + reunion.getAsunto() + "</h2>");

        if (textoConvocatoria != null)
        {
            content.append("<div><pre>" + textoConvocatoria.replaceAll("\n", "<br>") + "</pre></div><br/>");
        }

        if (textoAux != null)
        {
            content.append("<div>" + textoAux + "</div><br/>");
        }

        if (reunion.getNumeroSesion() != null)
        {
            content.append(
                    "<div><strong>" + resourceBundle.getString("mail.reunion.numeroSesion") + ": </strong><span>" + reunion.getNumeroSesion() + "</span></div>");
        }

        if (reunion.getDescripcion() != null && !reunion.getDescripcion().isEmpty())
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.descripcion") +": </strong><span>" + reunion.getDescripcion().replaceAll("\n", "<br>") + "</span></div>");
        }

        if (reunion.getFechaSegundaConvocatoria() == null)
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.fechaHora") + ": </strong>" + formatter.format(reunion.getFecha()) + "</div>");
        }
        else
        {
            content.append(
                    "<div><strong>" + resourceBundle.getString("mail.reunion.primeraConvocatoria") + ": </strong>" + formatter.format(reunion.getFecha()) + "</div>");
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.segundaConvocatoria") + ": </strong>" + formatter.format(
                    reunion.getFechaSegundaConvocatoria()) + "</div>");
        }

        if (reunion.getUbicacion() != null && !reunion.getUbicacion().isEmpty())
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.ubicacion") + ": </strong>" + reunion.getUbicacion() + "</div>");
        }

        if (reunion.getDuracion() != null && reunion.getDuracion() > 0)
        {
            content.append("<div><strong>" + resourceBundle.getString("mail.reunion.duracion") + ": </strong>" + reunion.getDuracion() + " " + resourceBundle.getString("mail.reunion.minutos") + "</div>");
        }

        if (puntosOrdenDiaOrdenados != null && !puntosOrdenDiaOrdenados.isEmpty())
        {
            content.append("<h4>" + resourceBundle.getString("mail.reunion.ordenDia") + "</h4>");
            content.append(createListaPuntosOrdenDia(new TreeSet(puntosOrdenDiaOrdenados)));
        }

        content.append(
                "<div>" + resourceBundle.getString("mail.reunion.masInfo") + " <a href=\"" + publicUrl + "/rest/publicacion/reuniones/" + reunion
                        .getId() + "\">" + publicUrl + "/rest/publicacion/reuniones/" + reunion.getId() + "</div>");

        return content.toString();
    }

    private String createListaPuntosOrdenDia(Set<PuntoOrdenDiaMultinivel> puntosOrdenDiaOrdenados)
    {
        StringBuilder puntosContent = new StringBuilder("<ol>");
        for(PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDiaOrdenados)
        {
            puntosContent.append("<li>" + puntoOrdenDia.getTitulo() + "</li>");
            if(puntoOrdenDia.getPuntosInferiores() != null && !puntoOrdenDia.getPuntosInferiores().isEmpty())
            {
                puntosContent.append(createListaPuntosOrdenDia(puntoOrdenDia.getPuntosInferiores()));
            }
        }
        puntosContent.append("</ol>");

        return puntosContent.toString();
    }
}
