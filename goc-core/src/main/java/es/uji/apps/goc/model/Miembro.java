package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionMiembro;

public class Miembro
{
    private Long id;

    private Long personaId;

    private String nombre;

    private String email;
    private Organo organo;
    private Cargo cargo;

    private String condicion;
    private String condicionAlternativa;

    private Boolean responsableActa;

    private String descripcion;
    private String descripcionAlternativa;

    private Long orden;

    private String grupo;

    private Long ordenGrupo;

    private Boolean firmante;
    private Boolean presideVotacion;
    private Boolean votante;

    public Miembro()
    {
    }

    public Miembro(Long id, String nombre, String email, Organo organo, Cargo cargo)
    {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.organo = organo;
        this.cargo = cargo;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Organo getOrgano()
    {
        return organo;
    }

    public void setOrgano(Organo organo)
    {
        this.organo = organo;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Cargo getCargo()
    {
        return cargo != null ? cargo : new Cargo("0", "");
    }

    public void setCargo(Cargo cargo)
    {
        this.cargo = cargo;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getCondicion()
    {
        return condicion;
    }

    public void setCondicion(String condicion)
    {
        this.condicion = condicion;
    }

    public String getCondicionAlternativa()
    {
        return condicionAlternativa;
    }

    public void setCondicionAlternativa(String condicionAlternativa)
    {
        this.condicionAlternativa = condicionAlternativa;
    }

    public Boolean getResponsableActa()
    {
        return responsableActa;
    }

    public void setResponsableActa(Boolean responsableActa)
    {
        this.responsableActa = responsableActa;
    }

    public Boolean isResponsableActa()
    {
        return responsableActa;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDescripcionAlternativa()
    {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa)
    {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public OrganoReunionMiembro toOrganoReunionMiembro(OrganoReunion organoReunion)
    {
        OrganoReunionMiembro organoReunionMiembro = new OrganoReunionMiembro();
        organoReunionMiembro.setOrganoReunion(organoReunion);

        if (organoReunion.isExterno())
        {
            organoReunionMiembro.setOrganoExterno(true);
        }
        else
        {
            organoReunionMiembro.setOrganoExterno(false);
        }

        organoReunionMiembro.setNombre(this.getNombre());
        organoReunionMiembro.setEmail(this.getEmail());
        organoReunionMiembro.setOrganoId(this.getOrgano().getId());
        organoReunionMiembro.setReunionId(organoReunion.getReunion().getId());
        organoReunionMiembro.setMiembroId(this.getPersonaId());
        organoReunionMiembro.setOrden(this.getOrden());
        organoReunionMiembro.setGrupo(this.getGrupo());
        organoReunionMiembro.setOrdenGrupo(this.getOrdenGrupo());

        if(!this.hasCargo()){
            organoReunionMiembro.setCargoId("");
            organoReunionMiembro.setCargoCodigo("");
            organoReunionMiembro.setCargoNombre("");
            organoReunionMiembro.setCargoNombreAlternativo("");
        } else
        {
            organoReunionMiembro.setCargoId(this.getCargo().getId());
            organoReunionMiembro.setCargoCodigo(this.getCargo().getCodigo());
            organoReunionMiembro.setCargoNombre(this.getCargo().getNombre());
            organoReunionMiembro.setCargoNombreAlternativo(this.getCargo().getNombreAlternativo());
        }
        organoReunionMiembro.setCondicion(this.getCondicion());
        organoReunionMiembro.setCondicionAlternativa(this.getCondicionAlternativa());
        organoReunionMiembro.setResponsableActa(this.getCargo().getResponsableActa());
        organoReunionMiembro.setDescripcion(this.getDescripcion());
        organoReunionMiembro.setDescripcionAlternativa(this.getDescripcionAlternativa());
        organoReunionMiembro.setFirmante(this.getFirmante());
        organoReunionMiembro.setPresideVotacion(this.isPresideVotacion());
        organoReunionMiembro.setVotante(this.isVotante());

        return organoReunionMiembro;
    }

    public boolean hasCargo()
    {
        return getCargo() != null && getCargo().getId() != null && !getCargo().getId().equals("0");
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Long getOrden()
    {
        return orden;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public Long getOrdenGrupo()
    {
        return ordenGrupo;
    }

    public void setOrdenGrupo(Long ordenGrupo)
    {
        this.ordenGrupo = ordenGrupo;
    }

    public Boolean getFirmante() {
        return (this.firmante != null && this.firmante == true);
    }

    public void setFirmante(Boolean firmante) {
        this.firmante = firmante;
    }

    public Boolean isFirmante() {
        return (this.firmante != null && this.firmante == true);
    }

    public Boolean isPresideVotacion() {
        return presideVotacion != null ? presideVotacion : false;
    }

    public void setPresideVotacion(Boolean presideVotacion) {
        this.presideVotacion = presideVotacion;
    }

    public Boolean isVotante() {
        return votante;
    }

    public void setVotante(Boolean votante) {
        this.votante = votante;
    }
}