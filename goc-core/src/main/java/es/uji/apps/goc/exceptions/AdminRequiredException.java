package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class AdminRequiredException extends CoreBaseException {
    public AdminRequiredException() {
        super("appI18N.exception.adminRequired");
    }
}
