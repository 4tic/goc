package es.uji.apps.goc;

import org.jsoup.Jsoup;

public class HTMLUtils {
    private static final String TAG_START = "<div>";
    private static final String TAG_END = "</div>";

    public static String forceCloseHTMLTags(String texto)
    {
        if (!isFop(texto)) {
            if (containsHtml(texto)) {
                texto = wrapHTML(texto.replace("<br>", "<br/>").replace("&nbsp;", ""), false);
            }
        }
        return texto;
    }

    public static String wrapHTML(String texto, boolean force)
    {
        if (force || (!texto.startsWith(HTMLUtils.TAG_START) && !texto.endsWith(HTMLUtils.TAG_END))) {
            texto = "<div>" + texto + "</div>";
        }
        return texto;
    }

    private static boolean isFop(String texto)
    {
        return (texto != null && texto.toUpperCase().contains("<FO:BLOCK"));
    }

    public static boolean containsHtml(String texto)
    {
        String textOfHtmlString = Jsoup.parse(texto).text();
        return !textOfHtmlString.equals(texto);
    }
}
