package es.uji.apps.goc.dto;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_VOTOS")
public class VotoPublicoDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "REUNION_ID")
    private String reunionId;

    @Column(name = "PUNTO_ID")
    private String puntoOrdenDiaId;

    @Column(name = "PERSONA_ID")
    private String personaId;

    @Column(name = "MIEMBRO_ID")
    private String miembroId;

    @Column(name = "NOMBRE_VOTANTE")
    private String nombre;

    @Column(name = "VOTO")
    private String voto;

    @Column(name = "VOTO_EN_NOMBRE")
    private String votoEnNombreDe;

    @Column(name = "VOTO_EN_NOMBRE_PERSONA_ID")
    private String votoEnNombreDePersonaId;

    public VotoPublicoDTO()
    {
    }

    public VotoPublicoDTO(
        Long reunionId, Long puntoOrdenDiaId, Long personaId, String personaNombre, String voto, String organoReunionMiembroNombre ,String organoReunionMiembroPersonId
    ) {
        //TODO que pasa si una persona pertenece a dos órganos de la reunión y portanto posee dos votos
        //this.miembroId
        this.reunionId = reunionId.toString();
        this.puntoOrdenDiaId = puntoOrdenDiaId.toString();
        this.personaId = personaId.toString();
        this.nombre = personaNombre;
        this.voto = voto;
        if (!personaId.toString().equals(organoReunionMiembroNombre)) {
            this.votoEnNombreDe = organoReunionMiembroNombre;
            this.votoEnNombreDePersonaId = organoReunionMiembroPersonId;
        }
    }

    public String getReunionId()
    {
        return reunionId;
    }

    public void setReunionId(String reunionId)
    {
        this.reunionId = reunionId;
    }

    public String getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(String puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public String getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(String personaId)
    {
        this.personaId = personaId;
    }

    public String getMiembroId()
    {
        return miembroId;
    }

    public void setMiembroId(String miembroId)
    {
        this.miembroId = miembroId;
    }

    public String getVoto()
    {
        return voto;
    }

    public void setVoto(String voto)
    {
        this.voto = voto;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getVotoEnNombreDe()
    {
        return votoEnNombreDe;
    }

    public void setVotoEnNombreDe(String votoEnNombreDe)
    {
        this.votoEnNombreDe = votoEnNombreDe;
    }

    public String getVotoEnNombreDePersonaId()
    {
        return votoEnNombreDePersonaId;
    }

    public void setVotoEnNombreDePersonaId(String votoEnNombreDePersonaId)
    {
        this.votoEnNombreDePersonaId = votoEnNombreDePersonaId;
    }
}
