package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class tipoOrganoBeingUsedException  extends CoreDataBaseException {
    public tipoOrganoBeingUsedException()
    {
        super("appI18N.excepciones.tipoOrganoSiendoUtilizado");
    }
}
