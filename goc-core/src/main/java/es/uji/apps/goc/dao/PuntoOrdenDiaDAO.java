package es.uji.apps.goc.dao;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.PersistenceException;

import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.apps.goc.dto.QPuntoOrdenDia;
import es.uji.apps.goc.dto.QPuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.QPuntoOrdenDiaComentario;
import es.uji.apps.goc.dto.QPuntoOrdenDiaDescriptor;
import es.uji.apps.goc.dto.QPuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.QPuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.QReunion;
import es.uji.apps.goc.exceptions.PuntoDelDiaConAcuerdosException;
import es.uji.apps.goc.model.EstadoPunto;
import es.uji.commons.db.BaseDAODatabaseImpl;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Repository
public class PuntoOrdenDiaDAO extends BaseDAODatabaseImpl
{
    private QPuntoOrdenDia qPuntoOrdenDia = QPuntoOrdenDia.puntoOrdenDia;
    private QPuntoOrdenDiaDocumento qPuntoOrdenDiaDocumento = QPuntoOrdenDiaDocumento.puntoOrdenDiaDocumento;
    private QPuntoOrdenDiaDescriptor qPuntoOrdenDiaDescriptor = QPuntoOrdenDiaDescriptor.puntoOrdenDiaDescriptor;
    private QPuntoOrdenDiaAcuerdo qPuntoOrdenDiaAcuerdo = QPuntoOrdenDiaAcuerdo.puntoOrdenDiaAcuerdo;
    private QPuntoOrdenDiaMultinivel qPuntoOrdenDiaMultinivel = QPuntoOrdenDiaMultinivel.puntoOrdenDiaMultinivel;
    private QPuntoOrdenDiaComentario qPuntoOrdenDiaComentario = QPuntoOrdenDiaComentario.puntoOrdenDiaComentario;
    private QReunion qReunion = QReunion.reunion;
    private QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;

    public List<PuntoOrdenDia> getPuntosByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.reunion.id.eq(reunionId)).orderBy(qPuntoOrdenDia.orden.asc());

        return query.list(qPuntoOrdenDia);

    }

    public List<PuntoOrdenDiaMultinivel> getPuntosMultinivelByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query
            .from(qPuntoOrdenDiaMultinivel)
            .where(qPuntoOrdenDiaMultinivel.reunion.id.eq(reunionId));

        return query.orderBy(qPuntoOrdenDiaMultinivel.orden.asc()).list(qPuntoOrdenDiaMultinivel);
    }

    public List<PuntoOrdenDiaMultinivel> getPuntosMultinivelByReunionIdFormateadoForBuscador(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query
                .from(qPuntoOrdenDiaMultinivel)
                .leftJoin(qPuntoOrdenDiaMultinivel.puntoOrdenDiaAcuerdos, qPuntoOrdenDiaAcuerdo)
                .leftJoin(qPuntoOrdenDiaMultinivel.puntoOrdenDiaDocumentos, qPuntoOrdenDiaDocumento)
                .where(qPuntoOrdenDiaMultinivel.reunion.id.eq(reunionId));

        List<Tuple> puntoOrdenDiaMultinivel = query.orderBy(qPuntoOrdenDiaMultinivel.orden.asc())
                .list(qPuntoOrdenDiaMultinivel.id, qPuntoOrdenDiaMultinivel.orden,
                        qPuntoOrdenDiaMultinivel.acuerdos, qPuntoOrdenDiaMultinivel.acuerdosAlternativos,
                        qPuntoOrdenDiaMultinivel.deliberaciones, qPuntoOrdenDiaMultinivel.deliberacionesAlternativas,
                        qPuntoOrdenDiaMultinivel.puntoSuperior.id, qPuntoOrdenDiaMultinivel.publico,
                        qPuntoOrdenDiaMultinivel.titulo, qPuntoOrdenDiaMultinivel.tituloAlternativo, qPuntoOrdenDiaMultinivel.descripcion,
                        qPuntoOrdenDiaMultinivel.descripcionAlternativa,
                        qPuntoOrdenDiaMultinivel.urlActa, qPuntoOrdenDiaMultinivel.urlActaAlternativa,
                        qPuntoOrdenDiaMultinivel.urlActaAnterior,
                        qPuntoOrdenDiaMultinivel.urlActaAnteriorAlt,
                        qPuntoOrdenDiaDocumento.creadorId, qPuntoOrdenDiaDocumento.descripcion,
                        qPuntoOrdenDiaDocumento.descripcionAlternativa, qPuntoOrdenDiaDocumento.fechaAdicion, qPuntoOrdenDiaDocumento.id,
                        qPuntoOrdenDiaDocumento.mimeType, qPuntoOrdenDiaDocumento.nombreFichero, qPuntoOrdenDiaDocumento.publico,
                        qPuntoOrdenDiaAcuerdo.creadorId, qPuntoOrdenDiaAcuerdo.descripcion,
                        qPuntoOrdenDiaAcuerdo.descripcionAlternativa, qPuntoOrdenDiaAcuerdo.fechaAdicion, qPuntoOrdenDiaAcuerdo.id,
                        qPuntoOrdenDiaAcuerdo.mimeType, qPuntoOrdenDiaAcuerdo.nombreFichero, qPuntoOrdenDiaAcuerdo.publico
                );

            return sort(getPuntosDelDiaForBuscadorFromTupla(puntoOrdenDiaMultinivel));

    }

    public List<PuntoOrdenDiaMultinivel> getPuntosDelDiaForBuscadorFromTupla(List<Tuple> puntosDelDiaForBuscador)
    {
        ArrayList<PuntoOrdenDiaMultinivel> puntosOrdenDiaMultinivel = new ArrayList<>();
        for (Tuple puntoOrdenDia : puntosDelDiaForBuscador) {

            PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel = new PuntoOrdenDiaMultinivel();
            puntoOrdenDiaMultinivel.setId(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.id));
            puntoOrdenDiaMultinivel.setOrden(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.orden));
            puntoOrdenDiaMultinivel.setAcuerdos(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.acuerdos));
            puntoOrdenDiaMultinivel.setAcuerdosAlternativos(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.acuerdosAlternativos));
            puntoOrdenDiaMultinivel.setDeliberaciones(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.deliberaciones));
            puntoOrdenDiaMultinivel.setDeliberacionesAlternativas(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.deliberacionesAlternativas));
            puntoOrdenDiaMultinivel.setDescripcion(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.descripcion));
            puntoOrdenDiaMultinivel.setDescripcionAlternativa(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.descripcionAlternativa));
            puntoOrdenDiaMultinivel.setTitulo(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.titulo));
            puntoOrdenDiaMultinivel.setTituloAlternativo(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.tituloAlternativo));
            puntoOrdenDiaMultinivel.setPublico(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.publico));
            puntoOrdenDiaMultinivel.setUrlActa(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.urlActa));
            puntoOrdenDiaMultinivel.setUrlActaAlternativa(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.urlActaAlternativa));
            puntoOrdenDiaMultinivel.setUrlActaAnterior(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.urlActaAnterior));
            puntoOrdenDiaMultinivel.setUrlActaAnteriorAlt(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.urlActaAnteriorAlt));
            puntoOrdenDiaMultinivel.setPuntoSuperior(new PuntoOrdenDiaMultinivel(puntoOrdenDia.get(qPuntoOrdenDiaMultinivel.puntoSuperior.id)));
            if(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.id) != null ){
                Set<PuntoOrdenDiaDocumento> puntoOrdenDiaDocumentos = new HashSet<>();
                PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();

                puntoOrdenDiaDocumento.setId(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.id));
                puntoOrdenDiaDocumento.setCreadorId(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.creadorId));
                puntoOrdenDiaDocumento.setDescripcion(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.descripcion));
                puntoOrdenDiaDocumento.setDescripcionAlternativa(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.descripcionAlternativa));
                puntoOrdenDiaDocumento.setFechaAdicion(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.fechaAdicion));
                puntoOrdenDiaDocumento.setMimeType(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.mimeType));
                puntoOrdenDiaDocumento.setNombreFichero(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.nombreFichero));
                puntoOrdenDiaDocumento.setPublico(puntoOrdenDia.get(qPuntoOrdenDiaDocumento.publico));

                puntoOrdenDiaDocumentos.add(puntoOrdenDiaDocumento);
                puntoOrdenDiaMultinivel.setPuntoOrdenDiaDocumentos(puntoOrdenDiaDocumentos);
            }

            if(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.id) != null ){
                Set<PuntoOrdenDiaAcuerdo> puntoOrdenDiaAcuerdos = new HashSet<>();
                PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo = new PuntoOrdenDiaAcuerdo();

                puntoOrdenDiaAcuerdo.setId(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.id));
                puntoOrdenDiaAcuerdo.setCreadorId(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.creadorId));
                puntoOrdenDiaAcuerdo.setDescripcion(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.descripcion));
                puntoOrdenDiaAcuerdo.setDescripcionAlternativa(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.descripcionAlternativa));
                puntoOrdenDiaAcuerdo.setFechaAdicion(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.fechaAdicion));
                puntoOrdenDiaAcuerdo.setMimeType(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.mimeType));
                puntoOrdenDiaAcuerdo.setNombreFichero(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.nombreFichero));
                puntoOrdenDiaAcuerdo.setPublico(puntoOrdenDia.get(qPuntoOrdenDiaAcuerdo.publico));

                puntoOrdenDiaAcuerdos.add(puntoOrdenDiaAcuerdo);
                puntoOrdenDiaMultinivel.setPuntoOrdenDiaAcuerdos(puntoOrdenDiaAcuerdos);
            }
            puntosOrdenDiaMultinivel.add(puntoOrdenDiaMultinivel);
        }
        return puntosOrdenDiaMultinivel;
    }

    public List<PuntoOrdenDiaMultinivel> getPuntosMultinivelByReunionIdFormateado(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query
            .from(qPuntoOrdenDiaMultinivel)
            .where(qPuntoOrdenDiaMultinivel.reunion.id.eq(reunionId));

        return sort(query.orderBy(qPuntoOrdenDiaMultinivel.orden.asc()).list(qPuntoOrdenDiaMultinivel));
    }

    private List<PuntoOrdenDiaMultinivel> sort(List<PuntoOrdenDiaMultinivel> puntoOrdenDiaMultinivels)
    {
        juntarPuntosOrdenDIaDOcumentosYacuerdos(puntoOrdenDiaMultinivels);
        List<PuntoOrdenDiaMultinivel> puntosOrdenados = puntoOrdenDiaMultinivels.stream().sorted().collect(toList());
        Map<Long, List<PuntoOrdenDiaMultinivel>> puntosOrdenadosYAgrupadosPorPadreId =
            puntosOrdenados.stream().sorted().collect(groupingBy(punto -> (punto.getPuntoSuperiorId())));

        for (PuntoOrdenDiaMultinivel punto: puntosOrdenados) {
            if (puntosOrdenadosYAgrupadosPorPadreId.get(punto.getId()) != null)
                punto.setPuntosInferiores(mapToSet(puntosOrdenadosYAgrupadosPorPadreId.get(punto.getId()).stream().sorted().collect(toList())));
        }

        return puntosOrdenados.stream().filter(punto -> punto.getPuntoSuperior() == null || (punto.getPuntoSuperior() != null && punto.getPuntoSuperior().getId() == null)).collect(toList());
    }

    private List<PuntoOrdenDiaMultinivel> juntarPuntosOrdenDIaDOcumentosYacuerdos(List<PuntoOrdenDiaMultinivel> puntoOrdenDiaMultinivels)
    {
        ArrayList<PuntoOrdenDiaMultinivel> puntosYaCombinados = new ArrayList<>();
        Map<Long, List<PuntoOrdenDiaMultinivel>> gruposPorId = puntoOrdenDiaMultinivels.stream().collect(groupingBy(punto -> punto.getId()));

        for (Long idPunto : gruposPorId.keySet()) {
            Boolean firstTime = true;
            PuntoOrdenDiaMultinivel puntoYaCombinado = null;
            for (PuntoOrdenDiaMultinivel puntoDocumentosYAcuerdos : gruposPorId.get(idPunto)) {
                if (firstTime) {
                    firstTime = false;
                    puntoYaCombinado = puntoDocumentosYAcuerdos;
                } else {
                    if (puntoDocumentosYAcuerdos.getPuntoOrdenDiaAcuerdos() != null) {
                        addPuntoAcuerdos(puntoYaCombinado, puntoDocumentosYAcuerdos);
                    }
                    if (puntoDocumentosYAcuerdos.getPuntoOrdenDiaDocumentos() != null) {
                        addPuntoDocumentos(puntoYaCombinado, puntoDocumentosYAcuerdos);
                    }
                }
            }
            puntosYaCombinados.add(puntoYaCombinado);
        }
        return puntosYaCombinados;
    }

    private void addPuntoAcuerdos(PuntoOrdenDiaMultinivel puntoYaCombinado, PuntoOrdenDiaMultinivel puntoDocumentosYAcuerdos)
    {
        if (puntoYaCombinado.getPuntoOrdenDiaAcuerdos() == null) {
            Set<PuntoOrdenDiaAcuerdo> acuerdos = new HashSet<>();
            puntoYaCombinado.setPuntoOrdenDiaAcuerdos(acuerdos);
            puntoYaCombinado.getPuntoOrdenDiaAcuerdos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaAcuerdos());
        } else {
            puntoYaCombinado.getPuntoOrdenDiaAcuerdos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaAcuerdos());
        }
    }

    private void addPuntoDocumentos(PuntoOrdenDiaMultinivel puntoYaCombinado, PuntoOrdenDiaMultinivel puntoDocumentosYAcuerdos)
    {
        if (puntoYaCombinado.getPuntoOrdenDiaDocumentos() == null) {
            Set<PuntoOrdenDiaDocumento> documentos = new HashSet<>();
            puntoYaCombinado.setPuntoOrdenDiaDocumentos(documentos);
            puntoYaCombinado.getPuntoOrdenDiaDocumentos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaDocumentos());
        } else {
            puntoYaCombinado.getPuntoOrdenDiaDocumentos().addAll(puntoDocumentosYAcuerdos.getPuntoOrdenDiaDocumentos());
        }
    }

    private Set<PuntoOrdenDiaMultinivel> mapToSet(List<PuntoOrdenDiaMultinivel> puntoOrdenDiaMultinivels)
    {
        Set<PuntoOrdenDiaMultinivel> puntosInferiores = new LinkedHashSet<>();
        for (PuntoOrdenDiaMultinivel puntoOrden: puntoOrdenDiaMultinivels) {
            puntosInferiores.add(puntoOrden);
        }
        return puntosInferiores;
    }

    public PuntoOrdenDia getPuntoOrdenDiaById(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia =
            query.from(qPuntoOrdenDia).where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId)).list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0)
        {
            return null;
        }

        return puntosOrdenDia.get(0);

    }

    public PuntoOrdenDia getSiguientePuntoOrdenDiaByOrdenYNivel(Long reunionId, Long orden, PuntoOrdenDia padre)
    {
        BooleanExpression be = qPuntoOrdenDia.reunion.id.eq(reunionId).and(qPuntoOrdenDia.orden.gt(orden));

        be = (padre == null) ?
            be.and(qPuntoOrdenDia.puntoSuperior.isNull()) :
            be.and(qPuntoOrdenDia.puntoSuperior.id.eq(padre.getId()));

        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia = query.from(qPuntoOrdenDia)
            .where(be)
                .orderBy(qPuntoOrdenDia.orden.asc())
                .list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0)
        {
            return null;
        }

        return puntosOrdenDia.get(0);
    }

    public List<PuntoOrdenDia> getPuntosOrdenDiaMismoOrden(Long reunionId, Long orden)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia = query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId).and(qPuntoOrdenDia.orden.eq(orden)))
                .list(qPuntoOrdenDia);

        return puntosOrdenDia;
    }

    public PuntoOrdenDia getAnteriorPuntoOrdenDiaByOrdenYNivel(Long reunionId, Long orden, PuntoOrdenDia padre)
    {
        BooleanExpression be = qPuntoOrdenDia.reunion.id.eq(reunionId).and(qPuntoOrdenDia.orden.lt(orden));

        be = (padre == null) ?
            be.and(qPuntoOrdenDia.puntoSuperior.isNull()) :
            be.and(qPuntoOrdenDia.puntoSuperior.id.eq(padre.getId()));

        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia = query.from(qPuntoOrdenDia)
            .where(be)
                .orderBy(qPuntoOrdenDia.orden.desc())
                .list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0)
        {
            return null;
        }
        return puntosOrdenDia.get(0);
    }

    @Transactional
    public void actualizaOrden(Long puntoOrdenDiaId, Long orden)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        update.set(qPuntoOrdenDia.orden, orden).where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId));
        update.execute();
    }

    public PuntoOrdenDia getUltimoPuntoOrdenDiaByReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<PuntoOrdenDia> puntosOrdenDia = query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId))
                .orderBy(qPuntoOrdenDia.orden.desc())
                .list(qPuntoOrdenDia);

        if (puntosOrdenDia.size() == 0)
        {
            return null;
        }

        return puntosOrdenDia.get(0);
    }

    @Transactional(rollbackFor = PuntoDelDiaConAcuerdosException.class)
    public void deleteByPuntoId(Long id) throws PuntoDelDiaConAcuerdosException {
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDiaDocumento);
        deleteClause.where(qPuntoOrdenDiaDocumento.puntoOrdenDia.id.eq(id)).execute();

        deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDiaComentario);
        deleteClause.where(qPuntoOrdenDiaComentario.puntoOrdenDia.id.eq(id)).execute();

        deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDiaAcuerdo);
        deleteClause.where(qPuntoOrdenDiaAcuerdo.puntoOrdenDia.id.eq(id)).execute();

        deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDiaDescriptor);
        deleteClause.where(qPuntoOrdenDiaDescriptor.puntoOrdenDia.id.eq(id)).execute();

        deleteClause = new JPADeleteClause(entityManager, qPuntoOrdenDia);
        try {
            deleteClause.where(qPuntoOrdenDia.id.eq(id)).execute();
        } catch (PersistenceException e) {
            throw new PuntoDelDiaConAcuerdosException();
        }
    }

    @Transactional
    public void incrementaOrdenPuntosSiguientesConMismoPadre(
        Long reunionId,
        Long puntoOrdenDiaAMoverId,
        Long orden
    )
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        updateClause
            .set(qPuntoOrdenDia.orden, qPuntoOrdenDia.orden.add(10L))
            .where(qPuntoOrdenDia.reunion.id.eq(reunionId)
                .and(qPuntoOrdenDia.orden.goe(orden))
                .and(qPuntoOrdenDia.id.ne(puntoOrdenDiaAMoverId)))
            .execute();
    }

    public PuntoOrdenDia detach(PuntoOrdenDia puntoOrdenDia) {
        entityManager.detach(puntoOrdenDia);
        puntoOrdenDia.setId(null);
        return puntoOrdenDia;
    }

    public Long getReunionIdByPuntoOrdenDIaId(Long puntoOrdenDiaId){
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId)).uniqueResult(qPuntoOrdenDia.reunion.id);
    }

    public List<Long> getPuntosIdsbyReunionId(Long reunionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId)).list(qPuntoOrdenDia.id);
    }

    public List<PuntoOrdenDia> getPuntosOrdenDiaByReunionIdConVotacion(Long reunionId, Boolean votacionPublica)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<Tuple> puntosTupla = query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.reunion.id.eq(reunionId)
                        .and(qPuntoOrdenDia.votoPublico.eq(votacionPublica)))
                .list(qPuntoOrdenDia.id, qPuntoOrdenDia.votoPublico);
        return getPuntoOrdenDiaFromTupla(puntosTupla);
    }

    private List<PuntoOrdenDia> getPuntoOrdenDiaFromTupla(List<Tuple> puntosTupla)
    {
        ArrayList<PuntoOrdenDia> puntosOrdenDia = new ArrayList<>();
        puntosTupla.forEach(puntoTupla -> {
            PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDia();
            puntoOrdenDia.setId(puntoTupla.get(qPuntoOrdenDia.id));
            puntoOrdenDia.setVotoPublico(puntoTupla.get(qPuntoOrdenDia.votoPublico));
            puntosOrdenDia.add(puntoOrdenDia);
        });
        return puntosOrdenDia;
    }

    public Boolean isVotacionPublica(Long puntoOrdenDiaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPuntoOrdenDia)
                .where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId))
                .singleResult(qPuntoOrdenDia.votoPublico);
    }

    public List<EstadoPunto> getEstadoVotacionReunion(Long reunionId)
    {
        return getEstadoVotacionPunto(reunionId, null);
    }

    public EstadoPunto getEstadoVotacionPunto(Long puntoId)
    {
        List<EstadoPunto> estadoVotacionPunto = getEstadoVotacionPunto(null, puntoId);
        if (estadoVotacionPunto != null && estadoVotacionPunto.size() > 0) {
            return estadoVotacionPunto.get(0);
        }
        return null;
    }

    private List<EstadoPunto> getEstadoVotacionPunto(Long reunionId, Long puntoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where;
        if (puntoId != null) {
             where = qPuntoOrdenDia.id.eq(puntoId);
        }
        else if (reunionId != null) {
            where = qPuntoOrdenDia.reunion.id.eq(reunionId);
        }
        else {
            throw new NullPointerException();
        }

        List<Tuple> list = query.from(qPuntoOrdenDia).where(where)
            .list(qPuntoOrdenDia.id, qPuntoOrdenDia.fechaAperturaVotacion, qPuntoOrdenDia.reunion.hasVotacion, qPuntoOrdenDia.reunion.completada,
                qPuntoOrdenDia.reunion.avisoPrimeraReunion);

        return list.stream().map(tuple -> {
            Date fechaAperturaVotacion = tuple.get(1, Date.class);
            Boolean tieneVotacion = tuple.get(2, Boolean.class);
            Boolean completada = tuple.get(3, Boolean.class);
            Boolean convocada = tuple.get(4, Boolean.class);

            boolean isVotable = (completada == null || !completada) && convocada != null && convocada && tieneVotacion != null && tieneVotacion;
            boolean isAbierta = fechaAperturaVotacion != null && fechaAperturaVotacion.before(new Date());

            return new EstadoPunto(tuple.get(0, Long.class), isAbierta, isVotable);
        }).collect(Collectors.toList());
    }

    @Transactional
    public void setFechaAperturaVotacionPunto(Long puntoOrdenDiaId, Date fechaAperturaVotacion)
    {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        update.set(qPuntoOrdenDia.fechaAperturaVotacion, fechaAperturaVotacion)
                .where(qPuntoOrdenDia.id.eq(puntoOrdenDiaId));
        update.execute();
    }

    @Transactional
    public void setFechaAperturaVotacionPuntos(Long reunionId, Date fechaAperturaVotacion) {
        JPAUpdateClause update = new JPAUpdateClause(entityManager, qPuntoOrdenDia);
        update.set(qPuntoOrdenDia.fechaAperturaVotacion, fechaAperturaVotacion)
            .where(qPuntoOrdenDia.reunion.id.eq(reunionId));
        update.execute();
    }
}