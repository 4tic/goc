package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class AsistenciaNoConfirmadaException extends CoreDataBaseException {
    public AsistenciaNoConfirmadaException()
    {
        super("appI18N.excepciones.asistenciaNoConfirmada");
    }

    public AsistenciaNoConfirmadaException(String message)
    {
        super(message);
    }

}
