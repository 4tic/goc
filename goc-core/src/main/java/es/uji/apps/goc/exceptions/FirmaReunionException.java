package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class FirmaReunionException extends CoreDataBaseException
{
    public FirmaReunionException()
    {
        super("appI18N.excepciones.noSeHaPodidoFirmar");
    }

    public FirmaReunionException(String message)
    {
        super(message);
    }
}
