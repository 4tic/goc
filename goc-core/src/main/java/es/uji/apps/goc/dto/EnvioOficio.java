package es.uji.apps.goc.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_ENVIOS_OFICIO")
public class EnvioOficio
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String email;
    private String cargo;
    private String unidad;

    @Column(name = "REGISTRO_SALIDA")
    private String registroSalida;

    @Column(name = "FECHA_ENVIO")
    private Date fechaEnvio;

    @ManyToOne
    @JoinColumn(name = "PUNTO_ORDEN_DIA")
    private PuntoOrdenDia puntoOrdenDia;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCargo()
    {
        return cargo;
    }

    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

    public String getUnidad()
    {
        return unidad;
    }

    public void setUnidad(String unidad)
    {
        this.unidad = unidad;
    }

    public String getRegistroSalida()
    {
        return registroSalida;
    }

    public void setRegistroSalida(String registroSalida)
    {
        this.registroSalida = registroSalida;
    }

    public Date getFechaEnvio()
    {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio)
    {
        this.fechaEnvio = fechaEnvio;
    }

    public PuntoOrdenDia getPuntoOrdenDia()
    {
        return puntoOrdenDia;
    }

    public void setPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia)
    {
        this.puntoOrdenDia = puntoOrdenDia;
    }
}
