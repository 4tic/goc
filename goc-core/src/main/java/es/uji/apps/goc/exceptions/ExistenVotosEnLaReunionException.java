package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class ExistenVotosEnLaReunionException extends CoreDataBaseException {
    public ExistenVotosEnLaReunionException()
    {
        super("appI18N.excepciones.existenVotos");
    }
}
