package es.uji.apps.goc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;

public class DateUtils
{
    public static String formattedDateInSpanish(Date date)
    {
        DateFormat dfLong = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("es"));

        return dfLong.format(date);
    }

    public static String formattedDateInCatalan(Date date)
    {
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String dateFormatted = "";

        int year  = localDate.getYear();
        int month = localDate.getMonthValue();
        int day   = localDate.getDayOfMonth();

        dateFormatted = new Integer(day).toString() + " ";
        dateFormatted += getMonthInCatalan(month);
        dateFormatted += " de " + new Integer(year).toString();

        return dateFormatted;
    }

    private static String getMonthInCatalan(int month)
    {
        switch (month) {
            case 1: return "de gener";
            case 2: return "de febrer";
            case 3: return "de març";
            case 4: return "d'abril";
            case 5: return "de maig";
            case 6: return "de juny";
            case 7: return "de juliol";
            case 8: return "d'agost";
            case 9: return "de setembre";
            case 10: return "d'octubre";
            case 11: return "de novembre";
            case 12: return "de desembre";
        }

        return "";
    }

    public static String getDiaSemanaTexto(Date fechaReunion, String locale) {
        LocalDate fecha = fechaReunion.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return fecha.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag(locale));
    }
}