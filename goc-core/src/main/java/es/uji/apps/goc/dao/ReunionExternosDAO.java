package es.uji.apps.goc.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.query.ListSubQuery;
import es.uji.apps.goc.dto.*;
import es.uji.apps.goc.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.sso.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ReunionExternosDAO extends BaseDAODatabaseImpl
{
    private QReunionExternos qReunionExternos = QReunionExternos.reunionExternos;

    @Transactional
    public void borrarExterno(ReunionExternos reunionExterno, User connectedUser) {
        JPADeleteClause delete = new JPADeleteClause(entityManager, qReunionExternos);
        delete.where(qReunionExternos.id.eq(reunionExterno.getId())
                .and(qReunionExternos.email.eq(reunionExterno.getEmail()))).execute();
        this.insert(new Auditoria(connectedUser.getName(), reunionExterno.getNombre(), reunionExterno.getReunionId(),EnumAccionesAuditoria.DENY_REUNION_ACCESS.toString()));
    }

    @Transactional
    public void insertExterno(ReunionExternos externo, User connectedUser) {
        this.insert(externo);
        this.insert(new Auditoria(connectedUser.getName(), externo.getNombre(), externo.getReunionId(),EnumAccionesAuditoria.GRANT_REUNION_ACCESS.toString()));
    }

}
