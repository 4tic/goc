package es.uji.apps.goc.exceptions;

public class MiembroExisteException extends Exception {
    public MiembroExisteException()
    {
        super("appI18N.exception.miembroExisteException");
    }
}
