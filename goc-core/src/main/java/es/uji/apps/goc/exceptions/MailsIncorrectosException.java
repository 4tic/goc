package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class MailsIncorrectosException extends CoreDataBaseException
{
    public MailsIncorrectosException(String message)
    {
        super(message);
    }
}