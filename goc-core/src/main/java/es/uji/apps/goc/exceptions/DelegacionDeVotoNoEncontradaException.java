package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class DelegacionDeVotoNoEncontradaException extends CoreDataBaseException {
    public DelegacionDeVotoNoEncontradaException()
    {
        super("appI18N.excepciones.delegacionDeVotoNoEncontrada");
    }

    public DelegacionDeVotoNoEncontradaException(String message)
    {
        super(message);
    }

}
