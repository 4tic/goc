package es.uji.apps.goc.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrganoParametroPK implements Serializable
{

    @Column(name = "ID_ORGANO")
    private String organoId;

    @Column(name = "ORDINARIO")
    private Boolean ordinario;

    public OrganoParametroPK()
    {
    }

    public OrganoParametroPK(String organoId, Boolean ordinario)
    {
        this.organoId = organoId;
        this.ordinario = ordinario;
    }

    public String getOrganoId()
    {
        return organoId;
    }

    public void setOrganoId(String organoId)
    {
        this.organoId = organoId;
    }

    public Boolean getOrdinario()
    {
        return ordinario;
    }

    public void setOrdinario(Boolean ordinario)
    {
        this.ordinario = ordinario;
    }
}
