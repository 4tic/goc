package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class PersonasExternasException extends CoreDataBaseException
{
    public PersonasExternasException()
    {
        super("appI18N.exception.personaConMailYaExiste");
    }

    public PersonasExternasException(String message)
    {
        super(message);
    }
}
