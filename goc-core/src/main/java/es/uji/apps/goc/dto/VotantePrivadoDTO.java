package es.uji.apps.goc.dto;

import org.hibernate.annotations.Type;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_VOTANTES_PRIVADOS")
public class VotantePrivadoDTO {

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    private UUID id;

    @Column(name = "REUNION_ID")
    private String reunionId;

    @Column(name = "PUNTO_ID")
    private String puntoOrdenDiaId;

    @Column(name = "PERSONA_ID")
    private String personaId;

    @Column(name = "NOMBRE_VOTANTE")
    private String nombreVotante;

    @Column(name = "FECHA_VOTO")
    private Date fechaVoto;

    @Column(name = "VOTO_EN_NOMBRE")
    private String votoEnNombreDe;

    @Column(name = "VOTO_EN_NOMBRE_PERSONA_ID")
    private String votoEnNombreDePersonaId;

    public VotantePrivadoDTO()
    {
    }

    public VotantePrivadoDTO(Long reunionId, Long puntoOrdenDiaId, Long personaId, String organoReunionMiembroNombre ,String organoReunionMiembroPersonId)
    {
        this.reunionId = reunionId.toString();
        this.puntoOrdenDiaId = puntoOrdenDiaId.toString();
        this.personaId = personaId.toString();
        this.fechaVoto = new Date();
        if (!personaId.toString().equals(organoReunionMiembroNombre)) {
            this.votoEnNombreDe = organoReunionMiembroNombre;
            this.votoEnNombreDePersonaId = organoReunionMiembroPersonId;
        }
    }
    public String getReunionId()
    {
        return reunionId;
    }

    public void setReunionId(String reunionId)
    {
        this.reunionId = reunionId;
    }

    public String getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(String puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public String getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(String personaId)
    {
        this.personaId = personaId;
    }

    public Date getFechaVoto()
    {
        return fechaVoto;
    }

    public void setFechaVoto(Date fechaVoto)
    {
        this.fechaVoto = fechaVoto;
    }

    public String getVotoEnNombreDe()
    {
        return votoEnNombreDe;
    }

    public void setVotoEnNombreDe(String votoEnNombreDe)
    {
        this.votoEnNombreDe = votoEnNombreDe;
    }

    public String getVotoEnNombreDePersonaId()
    {
        return votoEnNombreDePersonaId;
    }

    public void setVotoEnNombreDePersonaId(String votoEnNombreDePersonaId)
    {
        this.votoEnNombreDePersonaId = votoEnNombreDePersonaId;
    }

    public String getNombreVotante()
    {
        return nombreVotante;
    }

    public void setNombreVotante(String nombreVotante)
    {
        this.nombreVotante = nombreVotante;
    }
}
