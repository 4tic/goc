package es.uji.apps.goc.dto;

import org.hibernate.annotations.Type;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GOC_VOTOS_PRIVADOS")
public class VotoPrivadoDTO {

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    private UUID id;

    @Column(name = "REUNION_ID")
    private String reunionId;

    @Column(name = "PUNTO_ID")
    private String puntoOrdenDiaId;

    @Column(name = "VOTO")
    private String voto;

    public VotoPrivadoDTO()
    {
    }

    public VotoPrivadoDTO(Long reunionId, Long puntoOrdenDiaId, String voto)
    {
        this.reunionId = reunionId.toString();
        this.puntoOrdenDiaId = puntoOrdenDiaId.toString();
        this.voto = voto;
    }

    public String getReunionId()
    {
        return reunionId;
    }

    public void setReunionId(String reunionId)
    {
        this.reunionId = reunionId;
    }

    public String getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(String puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public String getVoto()
    {
        return voto;
    }

    public void setVoto(String voto)
    {
        this.voto = voto;
    }

}
