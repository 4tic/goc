package es.uji.apps.goc.model;

public class OrganoCargo {
    private Long id;
    private String cargoId;
    private String cargoNombre;
    private String cargoCodigo;
    private String organoId;

    public OrganoCargo() {
    }

    public OrganoCargo(
        Long id,
        String cargoId,
        String cargoNombre,
        String cargoCodigo,
        String organoId
    ) {
        this.id = id;
        this.cargoId = cargoId;
        this.cargoNombre = cargoNombre;
        this.cargoCodigo = cargoCodigo;
        this.organoId = organoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCargoId() {
        return cargoId;
    }

    public void setCargoId(String cargoId) {
        this.cargoId = cargoId;
    }

    public String getCargoNombre() {
        return cargoNombre;
    }

    public void setCargoNombre(String cargoNombre) {
        this.cargoNombre = cargoNombre;
    }

    public String getCargoCodigo() {
        return cargoCodigo;
    }

    public void setCargoCodigo(String cargoCodigo) {
        this.cargoCodigo = cargoCodigo;
    }

    public String getOrganoId() {
        return organoId;
    }

    public void setOrganoId(String organoId) {
        this.organoId = organoId;
    }
}
