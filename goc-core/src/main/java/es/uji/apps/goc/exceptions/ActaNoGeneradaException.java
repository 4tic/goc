package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

public class ActaNoGeneradaException extends CoreDataBaseException
{
    public ActaNoGeneradaException()
    {
        super("appI18N.common.actaNoGeneradaException");
    }

    public ActaNoGeneradaException(String message)
    {
        super(message);
    }
}
