package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class PuntoDelDiaNoTieneAcuerdosException extends CoreDataBaseException
{
    public PuntoDelDiaNoTieneAcuerdosException()
    {
        super("appI18N.excepciones.puntoSinAcuerdos");
    }

    public PuntoDelDiaNoTieneAcuerdosException(String message)
    {
        super(message);
    }
}
