package es.uji.apps.goc.exceptions;

import es.uji.commons.rest.exceptions.CoreDataBaseException;

@SuppressWarnings("serial")
public class LimiteConsultaLDAPException extends CoreDataBaseException
{
    public LimiteConsultaLDAPException()
    {
        super("appI18N.common.limiteBusquedaLDAP");
    }
}
