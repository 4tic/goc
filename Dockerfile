#
# Build stage
#
FROM maven:3.6.0-jdk-8-slim AS build
COPY .m2/settings.xml /home/app/.m2/settings.xml
COPY src /home/app/src
COPY goc-avisos /home/app/goc-avisos
COPY goc-core /home/app/goc-core
COPY goc-base /home/app/goc-base
COPY pom.xml /home/app
RUN mvn -s /home/app/.m2/settings.xml -f /home/app/pom.xml clean package

#
# Package stage
#
FROM tomcat:9-jdk8

ENV goc.home /etc/cuatroochenta/agora
RUN mkdir -p "$goc.home"

WORKDIR $CATALINA_HOME

COPY --from=build /home/app/goc-base/target/goc.war webapps/agora.war

EXPOSE 8080
