package es.uji.apps.goc.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import es.uji.apps.goc.builders.PuntoOrdenDiaBuilder;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.services.ReunionService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;


@RunWith(SpringJUnit4ClassRunner.class)
public class PuntoOrdenDiaDAOTest extends BaseDAOTest {

    @Autowired
    PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    PuntoOrdenDia puntoOrdenDiaPadre;

    PuntoOrdenDia puntoOrdenDiaHijo;

    @Autowired
    ReunionService reunionService;

    private PuntoOrdenDia puntoOrdenDiaVotacionAbierta;

    private PuntoOrdenDia puntoOrdenDiaVotacionCerrada;

    @Before
    public void setUp() {
        puntoOrdenDiaHijo = new PuntoOrdenDiaBuilder().withAcuerdos().withAcuerdosAlt().withDeliberaciones().withdeliberacionesAltenrativas()
                .withUrlcta().withUrlActaAlternativa().withUrlActaAnterior().withUrlActaAnteriorAlternativa().build(entityManager);

        puntoOrdenDiaPadre = new PuntoOrdenDiaBuilder().withAcuerdos().withAcuerdosAlt().withDeliberaciones().withdeliberacionesAltenrativas()
                .withUrlcta().withUrlActaAlternativa().withUrlActaAnterior().withUrlActaAnteriorAlternativa().withSubpunto(puntoOrdenDiaHijo).build(entityManager);

        puntoOrdenDiaVotacionAbierta = new PuntoOrdenDiaBuilder().withAcuerdos().withAcuerdosAlt().withDeliberaciones().withdeliberacionesAltenrativas()
                .withUrlcta().withUrlActaAlternativa().withUrlActaAnterior().withUrlActaAnteriorAlternativa().withSubpunto(puntoOrdenDiaHijo).withFechaAperturaVotacion(yesterday).build(entityManager);

        puntoOrdenDiaVotacionCerrada = new PuntoOrdenDiaBuilder().withAcuerdos().withAcuerdosAlt().withDeliberaciones().withdeliberacionesAltenrativas()
                .withUrlcta().withUrlActaAlternativa().withUrlActaAnterior().withUrlActaAnteriorAlternativa().withSubpunto(puntoOrdenDiaHijo).withFechaAperturaVotacion(tomorrow).build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldDuplicatePuntosWhenOnlyOneLevel() {
        List<PuntoOrdenDia> puntos = new ArrayList<>();
        puntos.add(new PuntoOrdenDia());
        reunionService.duplicaPuntosOrdenDia(reunion, puntos);

        assertThat(puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId()).size(), is(1));
    }

    @Test
    public void shouldDuplicatePuntosWhenPuntosHaveChildren() {
        List<PuntoOrdenDia> puntos = new ArrayList<>();
        PuntoOrdenDia puntoPadre = new PuntoOrdenDia();
        PuntoOrdenDia puntoHijo = new PuntoOrdenDia();
        puntoHijo.setPuntoSuperior(puntoPadre);
        Set<PuntoOrdenDia> puntosHijos = new HashSet<>();
        puntosHijos.add(puntoHijo);
        puntoPadre.setPuntosInferiores(puntosHijos);

        puntos.add(puntoPadre);
        reunionService.duplicaPuntosOrdenDia(reunion, puntos);

        List<PuntoOrdenDia> puntosByReunionId = puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId());
        assertThat(puntosByReunionId.size(), is(1));
        assertThat(puntosByReunionId.get(0).getPuntosInferiores().size(), is(1));
    }

    @Test
    public void shouldDuplicatePuntosAndsetPuntoSuperiorId() {
        List<PuntoOrdenDia> puntos = new ArrayList<>();
        PuntoOrdenDia puntoPadre = new PuntoOrdenDia();
        puntoPadre.setId(456453L);
        PuntoOrdenDia puntoHijo = new PuntoOrdenDia();
        puntoHijo.setPuntoSuperior(puntoPadre);
        Set<PuntoOrdenDia> puntosHijos = new HashSet<>();
        puntosHijos.add(puntoHijo);
        puntoPadre.setPuntosInferiores(puntosHijos);

        puntos.add(puntoPadre);
        reunionService.duplicaPuntosOrdenDia(reunion, puntos);

        List<PuntoOrdenDia> puntosByReunionId = puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId());
        assertThat(puntosByReunionId.size(), is(1));
        assertThat(puntosByReunionId.get(0).getPuntosInferiores().size(), is(1));
        assertThat(puntosByReunionId.get(0).getId().intValue() , not(456453));
        for(PuntoOrdenDia punto: puntosByReunionId.get(0).getPuntosInferiores()){
            assertThat(puntosByReunionId.get(0).getId(), is(punto.getPuntoSuperior().getId()));
        }
    }


    @Test
    public void ShouldDuplicatePuntosAsNoCompletados(){

        List <PuntoOrdenDia>  puntosOrdenDia= new LinkedList<>();
        puntosOrdenDia.add(puntoOrdenDiaPadre);

        reunionService.duplicaPuntosOrdenDia(reunion, puntosOrdenDia);
        List<PuntoOrdenDia> puntosByReunionId = puntoOrdenDiaDAO.getPuntosByReunionId(reunion.getId());

        assertThat(isCompletado(puntosByReunionId.get(0)),is(false));

    }

    private boolean isCompletado(PuntoOrdenDia puntoOrdenDia) {
        return ( puntoOrdenDia.getAcuerdos() != null || puntoOrdenDia.getAcuerdosAlternativos() != null || puntoOrdenDia.getDeliberaciones() != null ||
                puntoOrdenDia.getDeliberacionesAlternativas() != null || puntoOrdenDia.getUrlActa() != null
                || puntoOrdenDia.getUrlActaAlternativa() != null || puntoOrdenDia.getUrlActaAnterior() != null || puntoOrdenDia.getUrlActaAnteriorAlt() != null);
    }
}
