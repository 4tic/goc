package es.uji.apps.goc.dao;

import org.junit.Before;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.dto.Reunion;

@ActiveProfiles("test")
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class BaseDAOTest {

    static final Long CREADOR_ID = 2222l;

    @PersistenceContext
    protected EntityManager entityManager;

    Reunion reunion;

    Date tomorrow;

    Date yesterday;


    @Before
    public void setUpContext(){
        Date tomorrowAux =  new Date();
        LocalDateTime localDateTime = tomorrowAux.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        tomorrow = Date.from(localDateTime.plusDays(1L).atZone(ZoneId.systemDefault()).toInstant());
        yesterday = Date.from(localDateTime.minusDays(1L).atZone(ZoneId.systemDefault()).toInstant());

        reunion = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).withFecha(new Date())
                .withAvisoPrimeraReunion(true).build(entityManager);
    }
}
