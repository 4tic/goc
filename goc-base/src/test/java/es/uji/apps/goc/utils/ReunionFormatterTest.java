package es.uji.apps.goc.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.notifications.ReunionFormatter;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ReunionFormatterTest
{
    @Test
    public void shouldPuntsOrdenDiaNotDuplicatedAfterFormat() {
        Reunion reunion = new Reunion();
        reunion.setAsunto("Asunto");
        Calendar cal = Calendar.getInstance();
        cal.set(2019, 1, 12, 12, 00);
        reunion.setFecha(cal.getTime());

        PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel = new PuntoOrdenDiaMultinivel();
        puntoOrdenDiaMultinivel.setOrden(1L);
        puntoOrdenDiaMultinivel.setDescripcion("Descripción");
        puntoOrdenDiaMultinivel.setTitulo("Título");
        List<PuntoOrdenDiaMultinivel> puntos = new ArrayList<>();
        puntos.add(puntoOrdenDiaMultinivel);
        ReunionFormatter formatter = new ReunionFormatter(reunion, puntos, "es");

        assertThat(formatter.format("/goc", "", ""), is("<h2>Asunto</h2><div><pre></pre></div><br/><div></div><br/><div><strong>Fecha y hora: </strong>12/02/2019 12:00</div><h4>Orden del día</h4><ol><li>Título</li></ol><div>Para más información, puede consultar el detalle de la reunión en <a href=\"/goc/rest/publicacion/reuniones/null\">/goc/rest/publicacion/reuniones/null</div>"));
    }
}
