package es.uji.apps.goc.services;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import es.uji.apps.goc.builders.MiembroLocalBuilder;
import es.uji.apps.goc.builders.OrganoBuilder;
import es.uji.apps.goc.builders.OrganoReunionBuilder;
import es.uji.apps.goc.builders.PuntoOrdenDiaBuilder;
import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.builders.VotantePrivadoBuilder;
import es.uji.apps.goc.builders.VotoPrivadoBuilder;
import es.uji.apps.goc.builders.VotoPublicoBuilder;
import es.uji.apps.goc.dao.VotoDAO;
import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.VotantePrivadoDTO;
import es.uji.apps.goc.dto.VotoPublicoDTO;
import es.uji.apps.goc.exceptions.AsistenciaNoConfirmadaException;
import es.uji.apps.goc.exceptions.DelegacionDeVotoNoEncontradaException;
import es.uji.apps.goc.exceptions.ExistenVotosEnLaReunionException;
import es.uji.apps.goc.exceptions.PresideVotacionRequiredException;
import es.uji.apps.goc.exceptions.ReunionNoAdmiteVotacionException;
import es.uji.apps.goc.exceptions.ReunionNoConvocadaException;
import es.uji.apps.goc.exceptions.ReunionYaCompletadaException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.exceptions.UsuarioNoTieneDerechoAVotoException;
import es.uji.apps.goc.exceptions.UsuarioYaHaVotadoException;
import es.uji.apps.goc.exceptions.VotacionNoAbiertaException;
import es.uji.apps.goc.model.ResultadoVotos;
import es.uji.apps.goc.model.Votante;
import es.uji.apps.goc.model.VoteType;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class VotoServiceTest extends BaseServiceTest {
    @Autowired
    ReunionService reunionService;

    @Autowired
    VotosService votosService;

    @Autowired
    PuntoOrdenDiaService puntoOrdenDiaService;

    @Autowired
    VotoDAO votoDAO;

    private Reunion reunionConMiembroYPunto;
    private MiembroLocal miembroAjeno;
    private MiembroLocal miembro;
    private MiembroLocal suplente;
    private MiembroLocal delegadoVoto;
    private MiembroLocal miembroConSuplente;
    private MiembroLocal miembroConVotoDelegado;
    private Reunion reunionVotacionPrivada;
    private Reunion reunionVotacionPublica;
    private PuntoOrdenDia puntoOrdenDia;
    private PuntoOrdenDia puntoEnReunionConMiembroVotoPrivado;
    private PuntoOrdenDia puntoEnReunionVotacionPublicaAbierta;
    private PuntoOrdenDia puntoEnReunionVotacionPublicaCerrada;
    private PuntoOrdenDia puntoEnReunionVotacionPrivada;
    private PuntoOrdenDia puntoEnReunionVotacionPrivada2;

    @Before
    public void setUp() throws RolesPersonaExternaException {
        super.setUp();
        miembroAjeno = new MiembroLocalBuilder().withPersonaId(80L).build(entityManager);
        miembro = new MiembroLocalBuilder().withPersonaId(90L).build(entityManager);
        suplente = new MiembroLocalBuilder().withPersonaId(100L).build(entityManager);
        delegadoVoto = new MiembroLocalBuilder().withPersonaId(110L).build(entityManager);
        miembroConSuplente =
            new MiembroLocalBuilder().withEmail("miembroConSuplente@4tic.com").withPersonaId(120L).build(entityManager);
        miembroConVotoDelegado =
            new MiembroLocalBuilder().withEmail("miembroConVOtoDelegado@4tic.com").withNombre("MiembroConVotoDelegado")
                .withPersonaId(130L).build(entityManager);

        OrganoLocal organoLocal =
            new OrganoBuilder().withTipoOrgano(tipoOrgano).withMiembro(miembro).withMiembro(suplente).withMiembro(delegadoVoto)
                .build(entityManager);
        reunionConMiembroYPunto =
            new ReunionBuilder().withAsunto("Reunion Con Miembro").withOrgano(organoLocal)
                .convocada()
                .withHasVotacion(true).build(entityManager);

        new OrganoReunionBuilder(reunionConMiembroYPunto, organoLocal)
            .withMiembroConSuplente(miembroConSuplente, false, suplente)
            .withMiembroConDelegadoVoto(miembroConVotoDelegado, false, delegadoVoto).build(entityManager);

        puntoOrdenDia =
            new PuntoOrdenDiaBuilder(reunionConMiembroYPunto, 10L).withTitulo("punto 1").withVotoPublico(true)
                .withFechaAperturaVotacion(yesterday)
                .build(entityManager);
        puntoEnReunionConMiembroVotoPrivado =
            new PuntoOrdenDiaBuilder(reunionConMiembroYPunto, 10L).withTitulo("punto 1").withVotoPublico(false)
                .withFechaAperturaVotacion(yesterday)
                .build(entityManager);

        new VotoPublicoBuilder(reunionConMiembroYPunto.getId().toString(), puntoOrdenDia.getId().toString(),
            delegadoVoto.getPersonaId().toString(), VoteType.FAVOR.name()).withNombre(delegadoVoto.getNombre())
            .withVotoEnNombreDe(miembroConVotoDelegado.getNombre())
            .withVotoEnNombreDePersonaId(miembroConVotoDelegado.getPersonaId().toString()).build(entityManager);


        reunionVotacionPublica =
            new ReunionBuilder().publica().withAsunto("Reunión votación pública").withCreadorId(CREADOR_ID)
                .completada(false).convocada().withHasVotacion(true).withOrgano(organoLocal)
                .build(entityManager);

        new OrganoReunionBuilder(reunionVotacionPublica, organoLocal).withPresideVotacionMiembro(miembro, true)
            .withMiembroConSuplente(miembroConSuplente, false, suplente)
            .withMiembroConDelegadoVoto(miembroConVotoDelegado, false, delegadoVoto).build(entityManager);

        puntoEnReunionVotacionPublicaAbierta =
            new PuntoOrdenDiaBuilder(reunionVotacionPublica, 10L).withTitulo("punto 1 votación pública")
                .withFechaAperturaVotacion(yesterday)
                .withVotoPublico(true).build(entityManager);

        puntoEnReunionVotacionPublicaCerrada =
            new PuntoOrdenDiaBuilder(reunionVotacionPublica, 20L).withTitulo("punto 2 votación pública cerrada")
                .withVotoPublico(true).build(entityManager);

        reunionVotacionPrivada =
            new ReunionBuilder().publica().withAsunto("Reunión votación pública").withCreadorId(CREADOR_ID)
                .completada(false).convocada().withHasVotacion(true).withOrgano(organoAdmin)
                .build(entityManager);

        new OrganoReunionBuilder(reunionVotacionPrivada, organoLocal).withMiembro(miembro, true)
            .withMiembroConSuplente(miembroConSuplente, false, suplente)
            .withMiembroConDelegadoVoto(miembroConVotoDelegado, false, delegadoVoto).build(entityManager);

        puntoEnReunionVotacionPrivada =
            new PuntoOrdenDiaBuilder(reunionVotacionPrivada, 10L).withTitulo("punto 1 votoción privada")
                .withFechaAperturaVotacion(yesterday)
                .withVotoPublico(false).build(entityManager);

        puntoEnReunionVotacionPrivada2 =
            new PuntoOrdenDiaBuilder(reunionVotacionPrivada, 20L).withTitulo("punto 2 votoción privada")
                .withFechaAperturaVotacion(yesterday)
                .withVotoPublico(false).build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldReturnTrueWhenConnectedUserIsMemberOfReunionAndHasNoSuplenteNiVotoDelegado() {
        boolean isMiembroWithoutSuplenteNiVotoDelegado =
            votosService.isVotanteWithoutSuplenteOrDelegado(reunionConMiembroYPunto.getId(), miembro.getPersonaId());
        assertThat(isMiembroWithoutSuplenteNiVotoDelegado, is(true));
    }

    @Test
    public void shouldReturnTrueWhenConnectedUserIsSuplenteOfAMemberInReunion() {
        boolean isSuplente =
            votosService.isReceptorVotoDelegadoOrSuplente(reunionConMiembroYPunto.getId(), suplente.getPersonaId());
        assertThat(isSuplente, is(true));
    }

    @Test
    public void shouldReturnTrueWhenConnectedUserIsReceptorVotoOfAMemberInReunion() {
        boolean isReceptorVotoDelegado =
            votosService.isReceptorVotoDelegadoOrSuplente(reunionConMiembroYPunto.getId(), delegadoVoto.getPersonaId());
        assertThat(isReceptorVotoDelegado, is(true));
    }

    @Test
    public void shouldReturnFalsehenConnectedUserIsNoMemberOfReunion() {
        boolean isMiembroWithoutSuplenteNiVotoDelegado =
            votosService.isVotanteWithoutSuplenteOrDelegado(reunionConMiembroYPunto.getId(), 91L);
        assertThat(isMiembroWithoutSuplenteNiVotoDelegado, is(false));
    }

    @Test
    public void shouldReturnFalseWhenConnectedUserHasSuplenteInReunion() {
        boolean isMiembroWithoutSuplenteNiVotoDelegado = votosService
            .isVotanteWithoutSuplenteOrDelegado(reunionConMiembroYPunto.getId(), miembroConSuplente.getPersonaId());
        assertThat(isMiembroWithoutSuplenteNiVotoDelegado, is(false));
    }

    @Test
    public void shouldReturnFalseWhenConnectedUserHasDelegatedVoteInReunion() {
        boolean isMiembroWithoutSuplenteNiVotoDelegado = votosService
            .isVotanteWithoutSuplenteOrDelegado(reunionConMiembroYPunto.getId(), miembroConVotoDelegado.getPersonaId());
        assertThat(isMiembroWithoutSuplenteNiVotoDelegado, is(false));
    }

    @Test
    public void shouldReturnNombreMiembroPrincipalAndIdFromVotosWhenMiembroIsReceptorVotoDelegado() {
        List<VotoPublicoDTO> votosUsuario =
            votoDAO.getVotosPublicosUsuarioEnReunion(reunionConMiembroYPunto.getId(), delegadoVoto.getPersonaId());
        boolean nombreDelegantePrincipalencontrado = false;
        for (VotoPublicoDTO voto : votosUsuario) {
            if (voto.getVotoEnNombreDe().equals(miembroConVotoDelegado.getNombre())) {
                nombreDelegantePrincipalencontrado = true;
            }
        }
        assertThat(nombreDelegantePrincipalencontrado, is(true));
    }

    @Test
    public void shouldNotThrowExceptionWhenMiembroHasNotVotedInVotacionPublica() throws UsuarioYaHaVotadoException {
        votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionVotacionPublica.getId(),
            miembro.getPersonaId());
    }

    @Test(expected = UsuarioYaHaVotadoException.class)
    public void shouldThrowExceptionMiembroHasVotedInVotacionPublica() throws UsuarioYaHaVotadoException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(), puntoOrdenDia.getId().toString(),
            miembro.getPersonaId().toString(), VoteType.FAVOR.name()).withNombre(delegadoVoto.getNombre()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionVotacionPublica.getId(),
            miembro.getPersonaId());
    }

    @Test
    public void shouldNotThrowExceptionWhenMiembroHasNotVotedInVotacionPrivada() throws UsuarioYaHaVotadoException {
        votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionVotacionPrivada.getId(),
            miembro.getPersonaId());
    }

    @Test(expected = UsuarioYaHaVotadoException.class)
    public void shouldThrowExceptionMiembroHasVotedInVotacionPrivada() throws UsuarioYaHaVotadoException {
        new VotantePrivadoBuilder(reunionVotacionPrivada.getId().toString(), puntoOrdenDia.getId().toString(),
            miembro.getPersonaId().toString()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionVotacionPrivada.getId(),
            miembro.getPersonaId());
    }

    @Test(expected = VotacionNoAbiertaException.class)
    public void shouldThrowExceptionWhenVotacionNoAbierta()
        throws ReunionNoAdmiteVotacionException, UsuarioYaHaVotadoException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante("VotantePublico", miembro.getPersonaId());
        String miembrohaDelegadoOesSustituidoId = null;
        Long reunionVotacionpublicaId = reunionVotacionPublica.getId();
        Long puntoVotacionPublicaId = puntoEnReunionVotacionPublicaCerrada.getId();

        votosService
            .vote(reunionVotacionpublicaId, puntoVotacionPublicaId, VoteType.FAVOR, miembrohaDelegadoOesSustituidoId,
                votante);
    }

    @Test
    public void shouldInsertVotoPublicoWhenIsMiembroInReunion()
        throws ReunionNoAdmiteVotacionException, UsuarioYaHaVotadoException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante("VotantePublico", miembro.getPersonaId());
        String miembrohaDelegadoOesSustituidoId = null;
        Long reunionVotacionpublicaId = reunionVotacionPublica.getId();
        Long puntoVotacionPublicaId = puntoEnReunionVotacionPublicaAbierta.getId();

        votosService
            .vote(reunionVotacionpublicaId, puntoVotacionPublicaId, VoteType.FAVOR, miembrohaDelegadoOesSustituidoId,
                votante);
        List<VotoPublicoDTO> votosUsuario =
            votoDAO.getVotosPublicosUsuarioEnReunion(reunionVotacionpublicaId, votante.getPersonaId());
        boolean voted = false;
        for (VotoPublicoDTO voto : votosUsuario) {
            if (voto.getPuntoOrdenDiaId().equals(puntoVotacionPublicaId.toString()) && voto.getPersonaId()
                .equals(votante.getPersonaId().toString())) {
                voted = true;
            }
        }
        assertTrue(voted);
    }

    @Test
    public void shouldInsertVotoPublicoWhenIsSuplenteInReunión()
        throws ReunionNoAdmiteVotacionException, UsuarioYaHaVotadoException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante("VotanteSuplentePublico", suplente.getPersonaId());
        String miembroHaDelegadoOesSustituidoId = miembroConSuplente.getPersonaId().toString();
        Long reunionVotacionpublicaId = reunionVotacionPublica.getId();
        Long puntoVotacionPublicaId = puntoEnReunionVotacionPublicaAbierta.getId();

        votosService
            .vote(reunionVotacionpublicaId, puntoVotacionPublicaId, VoteType.FAVOR, miembroHaDelegadoOesSustituidoId,
                votante);
        List<VotoPublicoDTO> votosUsuario =
            votoDAO.getVotosPublicosUsuarioEnReunion(reunionVotacionpublicaId, votante.getPersonaId());
        boolean voted = false;
        for (VotoPublicoDTO voto : votosUsuario) {
            if (voto.getPuntoOrdenDiaId().equals(puntoVotacionPublicaId.toString()) && voto.getPersonaId()
                .equals(votante.getPersonaId().toString()) && voto.getVotoEnNombreDePersonaId()
                .equals(miembroConSuplente.getPersonaId().toString())) {
                voted = true;
            }
        }
        assertTrue(voted);
    }

    @Test
    public void shouldInsertVotoPrivadoWhenIsMienbroInReunion()
        throws ReunionNoAdmiteVotacionException, UsuarioYaHaVotadoException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante("VotantePrivado", miembro.getPersonaId());
        String miembrohaDelegadoOesSustituidoId = null;
        Long reunionVotacionPrivadaId = reunionVotacionPrivada.getId();
        Long puntoVotacionPrivadaId = puntoEnReunionVotacionPrivada.getId();

        votosService
            .vote(reunionVotacionPrivadaId, puntoVotacionPrivadaId, VoteType.FAVOR, miembrohaDelegadoOesSustituidoId,
                votante);
        List<VotantePrivadoDTO> votos =
            votoDAO.getVotosPrivadosUsuarioEnReunion(reunionVotacionPrivadaId, votante.getPersonaId());
        boolean voted = false;
        for (VotantePrivadoDTO voto : votos) {
            if (voto.getPuntoOrdenDiaId().equals(puntoVotacionPrivadaId.toString()) && voto.getPersonaId()
                .equals(votante.getPersonaId().toString())) {
                voted = true;
            }
        }
        assertTrue(voted);
    }

    @Test
    public void shouldInsertVotoPrivadoWhenIsDelegadoOSuplenteInReunión()
        throws ReunionNoAdmiteVotacionException, UsuarioYaHaVotadoException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante("VotanteSuplentePrivado", suplente.getPersonaId());
        String miembroHaDelegadoOesSustituidoId = miembroConSuplente.getPersonaId().toString();
        Long reunionVotacionpublicaId = reunionVotacionPrivada.getId();
        Long puntoVotacionPrivadaId = puntoEnReunionVotacionPrivada.getId();

        votosService
            .vote(reunionVotacionpublicaId, puntoVotacionPrivadaId, VoteType.FAVOR, miembroHaDelegadoOesSustituidoId,
                votante);
        List<VotantePrivadoDTO> votos =
            votoDAO.getVotosPrivadosUsuarioEnReunion(reunionVotacionpublicaId, votante.getPersonaId());
        boolean voted = false;
        for (VotantePrivadoDTO voto : votos) {
            if (voto.getPuntoOrdenDiaId().equals(puntoVotacionPrivadaId.toString()) && voto.getPersonaId()
                .equals(votante.getPersonaId().toString()) && voto.getVotoEnNombreDePersonaId()
                .equals(miembroConSuplente.getPersonaId().toString())) {
                voted = true;
            }
        }
        assertTrue(voted);
    }

    @Test
    public void shouldGetResultadosVotosReunionPublica() {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), miembro.getPersonaId().toString(), VoteType.FAVOR.name())
            .withNombre(miembro.getNombre()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<ResultadoVotos> resultadosVotacionEnReunionPorPuntoVotacionPublica =
            votosService.getResultadosVotacionEnReunionTodosPuntos(reunionVotacionPublica.getId());
        assertNotNull(resultadosVotacionEnReunionPorPuntoVotacionPublica);
        ResultadoVotos resultadoVotos = resultadosVotacionEnReunionPorPuntoVotacionPublica.get(0);
        assertTrue(resultadoVotos.getaFavor() == 1);
        assertTrue(resultadoVotos.getEnContra() == 0);
        assertTrue(resultadoVotos.getAbstenciones() == 0);
        assertTrue(resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPublicaAbierta.getId()));
    }

    @Test
    public void shouldGetResultadosVotosReunionPrivada() {
        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.FAVOR.name()).build(entityManager);
        entityManager.flush();
        entityManager.clear();
        List<ResultadoVotos> resultadosVotacionEnReunionPorPuntoVotacionPrivada =
            votosService.getResultadosVotacionEnReunionTodosPuntos(reunionVotacionPrivada.getId());
        assertNotNull(resultadosVotacionEnReunionPorPuntoVotacionPrivada);
        ResultadoVotos resultadoVotos = resultadosVotacionEnReunionPorPuntoVotacionPrivada.get(0);

        assertTrue(resultadoVotos.getaFavor() == 1);
        assertTrue(resultadoVotos.getEnContra() == 0);
        assertTrue(resultadoVotos.getAbstenciones() == 0);
        assertTrue(resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPrivada.getId()));
    }

    @Test
    public void shouldGetResultadosVotosReunionPublicaWhenSeveralPuntosWithVotos() {
        String creador = String.valueOf(CREADOR_ID);
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), creador, VoteType.FAVOR.name()).build(entityManager);

        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaCerrada.getId().toString(), creador, VoteType.CONTRA.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<ResultadoVotos> resultadosVotacionEnReunionPorPuntoVotacionPublica =
            votosService.getResultadosVotacionEnReunionTodosPuntos(reunionVotacionPublica.getId());
        assertNotNull(resultadosVotacionEnReunionPorPuntoVotacionPublica);

        for (ResultadoVotos resultadoVotos : resultadosVotacionEnReunionPorPuntoVotacionPublica) {
            if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPublicaAbierta.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 1);
                assertTrue(resultadoVotos.getEnContra() == 0);
                assertTrue(resultadoVotos.getAbstenciones() == 0);
            } else if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPublicaCerrada.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 0);
                assertTrue(resultadoVotos.getEnContra() == 1);
                assertTrue(resultadoVotos.getAbstenciones() == 0);
            }
        }
    }

    @Test
    public void shouldGetResultadosVotosReunionPrivadaWhenSeveralPuntosWithVotos() {
        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.FAVOR.name()).build(entityManager);

        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada2.getId().toString(), VoteType.CONTRA.name()).build(entityManager);
        entityManager.flush();
        entityManager.clear();
        List<ResultadoVotos> resultadosVotacionEnReunionPorPuntoVotacionPrivada =
            votosService.getResultadosVotacionEnReunionTodosPuntos(reunionVotacionPrivada.getId());
        assertNotNull(resultadosVotacionEnReunionPorPuntoVotacionPrivada);

        for (ResultadoVotos resultadoVotos : resultadosVotacionEnReunionPorPuntoVotacionPrivada) {
            if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPrivada.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 1);
                assertTrue(resultadoVotos.getEnContra() == 0);
                assertTrue(resultadoVotos.getAbstenciones() == 0);
            } else if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPrivada2.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 0);
                assertTrue(resultadoVotos.getEnContra() == 1);
                assertTrue(resultadoVotos.getAbstenciones() == 0);
            }
        }
    }

    @Test
    public void shouldGetResultadosVotosReunionPublicaWhenSeveralPuntosWithVotosAndVotosWithdifferentTypes() {
        String creadorIdString = String.valueOf(CREADOR_ID);

        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), creadorIdString, VoteType.FAVOR.name()).build(entityManager);

        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), creadorIdString, VoteType.CONTRA.name()).build(entityManager);

        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), creadorIdString, VoteType.CONTRA.name()).build(entityManager);

        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), creadorIdString, VoteType.ABSTENCION.name()).build(entityManager);

        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaCerrada.getId().toString(), creadorIdString, VoteType.CONTRA.name()).build(entityManager);

        List<ResultadoVotos> resultadosVotacionEnReunionPorPuntoVotacionPrivada =
            votosService.getResultadosVotacionEnReunionTodosPuntos(reunionVotacionPrivada.getId());
        assertNotNull(resultadosVotacionEnReunionPorPuntoVotacionPrivada);

        for (ResultadoVotos resultadoVotos : resultadosVotacionEnReunionPorPuntoVotacionPrivada) {
            if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPublicaAbierta.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 1);
                assertTrue(resultadoVotos.getEnContra() == 2);
                assertTrue(resultadoVotos.getAbstenciones() == 1);
            } else if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPublicaCerrada.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 0);
                assertTrue(resultadoVotos.getEnContra() == 1);
                assertTrue(resultadoVotos.getAbstenciones() == 0);
            }
        }
    }

    @Test
    public void shouldGetResultadosVotosReunionPrivadaWhenSeveralPuntosWithVotosAndVotosWithdifferentTypes() {
        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.FAVOR.name()).build(entityManager);

        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.CONTRA.name()).build(entityManager);

        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.CONTRA.name()).build(entityManager);

        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.ABSTENCION.name()).build(entityManager);

        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada2.getId().toString(), VoteType.CONTRA.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<ResultadoVotos> resultadosVotacionEnReunionPorPuntoVotacionPrivada =
            votosService.getResultadosVotacionEnReunionTodosPuntos(reunionVotacionPrivada.getId());
        assertNotNull(resultadosVotacionEnReunionPorPuntoVotacionPrivada);

        for (ResultadoVotos resultadoVotos : resultadosVotacionEnReunionPorPuntoVotacionPrivada) {
            if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPrivada.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 1);
                assertTrue(resultadoVotos.getEnContra() == 2);
                assertTrue(resultadoVotos.getAbstenciones() == 1);
            } else if (resultadoVotos.getPuntoId().equals(puntoEnReunionVotacionPrivada2.getId())) {
                assertTrue(resultadoVotos.getaFavor() == 0);
                assertTrue(resultadoVotos.getEnContra() == 1);
                assertTrue(resultadoVotos.getAbstenciones() == 0);
            }
        }
    }

    @Test
    public void shouldGetResultadosVotosPuntoEnReunionPrivada() {
        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.FAVOR.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        ResultadoVotos resultadosVotacionEnPuntoVotacionPrivada = votosService
            .getResultadoVotosPuntoOrdenDia(puntoEnReunionVotacionPrivada.getId());
        assertNotNull(resultadosVotacionEnPuntoVotacionPrivada);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getaFavor() == 1);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getEnContra() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getAbstenciones() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getPuntoId().equals(puntoEnReunionVotacionPrivada.getId()));
    }

    @Test
    public void shouldGetAllZerosResultadosVotosPuntoEnReunionPrivada() {
        ResultadoVotos resultadosVotacionEnPuntoVotacionPrivada = votosService
            .getResultadoVotosPuntoOrdenDia(puntoEnReunionVotacionPrivada.getId());
        assertNotNull(resultadosVotacionEnPuntoVotacionPrivada);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getaFavor() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getEnContra() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getAbstenciones() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPrivada.getPuntoId().equals(puntoEnReunionVotacionPrivada.getId()));
    }

    @Test
    public void shouldGetResultadosVotosPuntoEnReunionPublica() {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        ResultadoVotos resultadosVotacionEnPuntoVotacionPublica = votosService
            .getResultadoVotosPuntoOrdenDia(puntoEnReunionVotacionPublicaAbierta.getId());
        assertNotNull(resultadosVotacionEnPuntoVotacionPublica);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getaFavor() == 1);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getEnContra() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getAbstenciones() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getPuntoId().equals(puntoEnReunionVotacionPublicaAbierta.getId()));
    }

    @Test
    public void shouldGetAllZerosResultadosVotosPuntoEnReunionPublica() {
        ResultadoVotos resultadosVotacionEnPuntoVotacionPublica = votosService
            .getResultadoVotosPuntoOrdenDia(puntoEnReunionVotacionPublicaAbierta.getId());
        assertNotNull(resultadosVotacionEnPuntoVotacionPublica);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getaFavor() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getEnContra() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getAbstenciones() == 0);
        assertTrue(resultadosVotacionEnPuntoVotacionPublica.getPuntoId().equals(puntoEnReunionVotacionPublicaAbierta.getId()));
    }

    @Test
    public void shouldGetTrueWhenReunionPublicaHasVotos() {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Boolean hasVotos =
            votoDAO.reunionHasVotos(reunionVotacionPublica.getId());
        assertTrue(hasVotos);
    }

    @Test
    public void shouldGetFalseWhenReunionPublicaHasNotVotos() {
        Boolean hasVotos =
            votoDAO.reunionHasVotos(reunionVotacionPublica.getId());
        assertFalse(hasVotos);
    }

    @Test
    public void shouldGetTrueWhenReunionPrivadaHasVotos() {
        new VotoPrivadoBuilder(reunionVotacionPrivada.getId().toString(),
            puntoEnReunionVotacionPrivada.getId().toString(), VoteType.FAVOR.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        Boolean hasVotos =
            votoDAO.reunionHasVotos(reunionVotacionPrivada.getId());
        assertTrue(hasVotos);
    }

    @Test
    public void shouldGetFalseWhenReunionPrivadaNotVotos() {
        Boolean hasVotos =
            votoDAO.reunionHasVotos(reunionVotacionPrivada.getId());
        assertFalse(hasVotos);
    }


    @Test
    public void shouldEndWithoutExceptionWhenReunionHasVotacionButNotVotos() throws ExistenVotosEnLaReunionException {
        votosService.compruebaReunionNoAdmiteONoTieneVotos(reunionVotacionPublica.getId());
    }

    @Test(expected = ExistenVotosEnLaReunionException.class)
    public void shouldEndWithExceptionWhenReunionHasVotacionAndVotos() throws ExistenVotosEnLaReunionException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        votosService.compruebaReunionNoAdmiteONoTieneVotos(reunionVotacionPublica.getId());
    }

    @Test
    public void shouldEndWithoutExceptionWhenPuntoHasVotacionButNotVotos() throws ExistenVotosEnLaReunionException {
        votosService.compruebaPuntoNoAdmiteONoTieneVotos(puntoEnReunionVotacionPublicaAbierta.getId(), reunionVotacionPublica.getId());
    }

    @Test(expected = ExistenVotosEnLaReunionException.class)
    public void shouldEndWithExceptionWhenPuntoHasVotacionAndVotos() throws ExistenVotosEnLaReunionException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublicaAbierta.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name()).build(entityManager);

        entityManager.flush();
        entityManager.clear();

        votosService.compruebaPuntoNoAdmiteONoTieneVotos(puntoEnReunionVotacionPublicaAbierta.getId(), reunionVotacionPublica.getId());
    }

    @Test
    public void shouldMiembroHasVotedInNombreDeVotacionPublica()
        throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votanteSuplente = new Votante(suplente.getNombre(), suplente.getPersonaId());

        votosService.vote(reunionConMiembroYPunto.getId(), puntoOrdenDia.getId(), VoteType.FAVOR,
            miembroConSuplente.getPersonaId().toString(), votanteSuplente);
        List<VotoPublicoDTO> votosUsuario =
            votoDAO.getVotosPublicosUsuarioEnReunion(reunionConMiembroYPunto.getId(), votanteSuplente.getPersonaId());
        VotoPublicoDTO votoPublicoDTO = votosUsuario.get(0);
        assertTrue(votoPublicoDTO.getVotoEnNombreDePersonaId().equals(miembroConSuplente.getPersonaId().toString()));
    }

    @Test
    public void shouldMiembroHasVotedInNombreDeVotacionPrivada()
        throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votanteSuplente = new Votante(suplente.getNombre(), suplente.getPersonaId());

        votosService.vote(reunionConMiembroYPunto.getId(), puntoEnReunionConMiembroVotoPrivado.getId(), VoteType.FAVOR,
            miembroConSuplente.getPersonaId().toString(), votanteSuplente);
        List<VotantePrivadoDTO> votosUsuario =
            votoDAO.getVotosPrivadosUsuarioEnReunion(reunionConMiembroYPunto.getId(), votanteSuplente.getPersonaId());
        VotantePrivadoDTO votoPublicoDTO = votosUsuario.get(0);
        assertTrue(votoPublicoDTO.getVotoEnNombreDePersonaId().equals(miembroConSuplente.getPersonaId().toString()));
    }

    @Test
    public void shouldMiembroHasVotedInOwnNombreDeBeNullVotacionPublica()
        throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante(suplente.getNombre(), suplente.getPersonaId());

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.FAVOR,
            votante.getPersonaId().toString(), votante);
        List<VotoPublicoDTO> votosUsuario =
            votoDAO.getVotosPublicosUsuarioEnReunion(reunionVotacionPublica.getId(), votante.getPersonaId());
        VotoPublicoDTO votoPublicoDTO = votosUsuario.get(0);
        assertNull(votoPublicoDTO.getVotoEnNombreDePersonaId());
    }

    @Test(expected = UsuarioNoTieneDerechoAVotoException.class)
    public void shouldThrowExceptionBecauseMiembroDoesNotExists()
        throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante(miembroAjeno.getNombre(), miembroAjeno.getPersonaId());

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.FAVOR,
            votante.getPersonaId().toString(), votante);
    }

    @Test(expected = UsuarioYaHaVotadoException.class)
    public void shouldThrowExceptionBecauseVoteExists()
        throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante(suplente.getNombre(), suplente.getPersonaId());

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.FAVOR,
            null, votante);

        entityManager.flush();
        entityManager.clear();

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.CONTRA,
            votante.getPersonaId().toString(), votante);
    }

    @Test(expected = DelegacionDeVotoNoEncontradaException.class)
    public void shouldThrowExceptionBecauseDelegateDoesNotExists()
        throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante(suplente.getNombre(), suplente.getPersonaId());

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.FAVOR,
            null, votante);

        entityManager.flush();
        entityManager.clear();

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.CONTRA,
            miembroConVotoDelegado.getPersonaId().toString(), votante);
    }

    @Test
    public void shouldNotThrowExceptionBecauseDelegateExists()
        throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        Votante votante = new Votante(delegadoVoto.getNombre(), delegadoVoto.getPersonaId());

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.FAVOR,
            null, votante);

        entityManager.flush();
        entityManager.clear();

        votosService.vote(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublicaAbierta.getId(), VoteType.CONTRA,
            miembroConVotoDelegado.getPersonaId().toString(), votante);
    }

    @Test
    public void shouldReturnFalseWhenNotMiembroOrDelegado()
    {
        Boolean isMiembro = reunionService.isVotanteOrDelegatedOnReunion(reunionConMiembroYPunto.getId(), Long.MAX_VALUE);
        assertFalse(isMiembro);
    }

    @Test
    public void shouldReturnTrueWhenDelegado()
    {
        Boolean isMiembro = reunionService.isVotanteOrDelegatedOnReunion(reunionConMiembroYPunto.getId(), delegadoVoto.getPersonaId());
        assertTrue(isMiembro);
    }

    @Test
    public void shouldReturnTrueWhenSuplente()
    {
        Boolean isMiembro = reunionService.isVotanteOrDelegatedOnReunion(reunionConMiembroYPunto.getId(), suplente.getPersonaId());
        assertTrue(isMiembro);
    }

    @Test
    public void abreYCierraVotacionTest()
        throws PresideVotacionRequiredException, ReunionYaCompletadaException, ReunionNoConvocadaException {
        boolean votacionActualmenteAbierta = puntoOrdenDiaService.isVotacionVotableYAbierta(puntoEnReunionConvocada.getId());
        assertThat(votacionActualmenteAbierta, Is.is(false));

        votosService.abreVotacion(reunionConvocada.getId(), puntoEnReunionConvocada.getId(), miembroPresideVotacion.getPersonaId());

        entityManager.flush();
        entityManager.clear();

        votacionActualmenteAbierta = puntoOrdenDiaService.isVotacionVotableYAbierta(puntoEnReunionConvocada.getId());
        assertThat(votacionActualmenteAbierta, Is.is(true));

        votosService.cierraVotacion(reunionConvocada.getId(), puntoEnReunionConvocada.getId(), miembroPresideVotacion.getPersonaId());

        entityManager.flush();
        entityManager.clear();

        votacionActualmenteAbierta = puntoOrdenDiaService.isVotacionVotableYAbierta(puntoEnReunionConvocada.getId());
        assertThat(votacionActualmenteAbierta, Is.is(false));
    }
}
