package es.uji.apps.goc.builders;

import es.uji.apps.goc.dto.CargoTemplate;
import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.MiembroTemplate;

public class MiembroTemplateBuilder {
    private MiembroTemplate miembroTemplate;

    public MiembroTemplateBuilder (){
        this.miembroTemplate = new MiembroTemplate();
    }

    public MiembroTemplateBuilder withId(String id){
        this.miembroTemplate.setId(id);
        return this;
    }

    public MiembroTemplateBuilder withAsistencia (boolean asistencia) {
        this.miembroTemplate.setAsistencia(asistencia);
        return this;
    }

    public MiembroTemplateBuilder withNombre(String nombre){
        this.miembroTemplate.setNombre(nombre);
        return this;
    }

    public MiembroTemplateBuilder withSuplente(MiembroLocal miembroLocal){
        this.miembroTemplate.setSuplenteId(miembroLocal.getPersonaId());
        this.miembroTemplate.setSuplente(miembroLocal.getNombre());
        return this;
    }

    public MiembroTemplateBuilder withFirmante(boolean firmante){
        this.miembroTemplate.setFirmante(firmante);
        return this;
    }

    public MiembroTemplateBuilder withEmail(String email){
        this.miembroTemplate.setEmail(email);
        return this;
    }

    public MiembroTemplateBuilder withCargo(CargoTemplate cargo){
        this.miembroTemplate.setCargo(cargo);
        return this;
    }

    public MiembroTemplate build(){
        return this.miembroTemplate;
    }
}
