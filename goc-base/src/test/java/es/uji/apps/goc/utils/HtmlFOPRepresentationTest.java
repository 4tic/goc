package es.uji.apps.goc.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import es.uji.apps.goc.Utils;

public class HtmlFOPRepresentationTest {
    final String START_FOP_TAG = "<fo:block xmlns:fo=\"http://www.w3.org/1999/XSL/Format\">";
    final String END_FOP_TAG = "</fo:block>";

    private Transformer transformer;

    @Before
    public void setUp() throws Exception {
        InputStream classPathResource = new ClassPathResource("xsl/xhtml-to-xslfo.xsl").getInputStream();
        TransformerFactory tFactory = TransformerFactory.newInstance();
        transformer = tFactory.newTransformer(
            new javax.xml.transform.stream.StreamSource(classPathResource));
    }

    @Test
    public void should3divsGenerate2LineBreak() throws Exception
    {
        String result = Utils.transformHtmlToFo(transformer, "<div>hola</div><div>que</div><div>tal</div>");
        Assert.assertEquals(String.format("%shola\nque\ntal%s", START_FOP_TAG, END_FOP_TAG), result);
        Assert.assertTrue(result.split("\n").length == 3);
    }

    @Test
    public void shouldTextAndDivGenerate2LineBreak() throws Exception
    {
        String result = Utils.transformHtmlToFo(transformer, "hola<div>que</div><div>tal</div>");
        Assert.assertEquals(String.format("%shola\nque\ntal%s", START_FOP_TAG, END_FOP_TAG), result);
        Assert.assertTrue(result.split("\n").length == 3);
    }

    @Test
    public void should1GlobalDivGenerateNoLineBreaks() throws Exception
    {
        String result = Utils.transformHtmlToFo(transformer, "<div>hola que tal</div>");
        Assert.assertEquals(String.format("%shola que tal%s", START_FOP_TAG, END_FOP_TAG), result);
        Assert.assertTrue(result.split("\n").length == 1);
    }

    @Test
    public void should2BrGenerateNoLineBreaks() throws Exception
    {
        String result = Utils.transformHtmlToFo(transformer, "<div>hola<br/>que<br/>tal</div>");
        Assert.assertTrue(result.split("\n").length == 1);
    }

    @Test
    public void should1DivGenerate1LineBreaks() throws Exception
    {
        String result = Utils.transformHtmlToFo(transformer, "<div>hola</div>que tal");
        Assert.assertTrue(result.split("\n").length == 2);
    }

    @Test
    public void should2DivGenerate1LineBreaks() throws Exception
    {
        String result = Utils.transformHtmlToFo(transformer, "<div>hola</div><div>que tal</div>");
        Assert.assertTrue(result.split("\n").length == 2);
    }
}
