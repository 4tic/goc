package es.uji.apps.goc.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.goc.builders.MiembroLocalBuilder;
import es.uji.apps.goc.builders.OrganoAutorizadoBuilder;
import es.uji.apps.goc.builders.OrganoBuilder;
import es.uji.apps.goc.builders.OrganoReunionBuilder;
import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.builders.TipoOrganoBuilder;
import es.uji.apps.goc.dto.Convocante;
import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.notifications.AvisosReunion;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class OrganoReunionMiembroDAOTest {
    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    ReunionDAO reunionDAO;

    @Autowired
    OrganoReunionMiembroDAO organoReunionMiembroDAO;

    @Autowired
    OrganoDAO organoDAO;

    @Autowired
    AvisosReunion avisosReunion;

    TipoOrganoLocal tipo;
    Reunion reunionCompletada;
    MiembroLocal presideVotacion;

    @Before
    public void setUp() {
        reunionCompletada = new ReunionBuilder().withAsunto("Reunión 1")
            .withInvitado("invitado.reunion@4tic.com").withFecha(new Date()).withAvisoPrimeraReunion(true).completada(true)
            .build(entityManager);

        tipo = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);

        OrganoLocal organoTest =
            new OrganoBuilder().withNombre("organo test").withTipoOrgano(tipo).build(entityManager);

        MiembroLocal suplente =
            new MiembroLocalBuilder().withEmail("suplente@4tic.com").withOrganoLocal(organoTest).withPersonaId(2L)
                .build(entityManager);

        presideVotacion =
            new MiembroLocalBuilder().withEmail("presideVotacion@4tic.com").withOrganoLocal(organoTest).withPersonaId(3L)
                .build(entityManager);

        OrganoReunion organoReunion =
            new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest)
                .withMiembro("miembro.organo@4tic.com", true)
                .withMiembro("miembro.organo.no.asiste@4tic.com", false)
                .withMiembroConSuplente("mimebro.organos.suplente@4tic.com", true, suplente)
                .withMiembroConSuplente("miembro.organos.suplente.no.asiste@4tic.com", false, suplente)
                .withPresideVotacionMiembro(presideVotacion, true)
                .withInvitado("invitado.organo@4tic.com").build(entityManager);

        new OrganoAutorizadoBuilder().withOrganoId(organoReunion.getOrganoId()).withPersonaEmail("autorizado@4tic.com")
            .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldGetEmailsByReunion() {
        Reunion reunionConMiembros = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionCompletada.getId());
        List<String> mailsMiembrosInvitadosYautorizados = organoReunionMiembroDAO.getMailsReunion(reunionConMiembros);

        assertThat(mailsMiembrosInvitadosYautorizados.size(), is(8));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("suplente@4tic.com"), is(true));
    }

    @Test
    public void shouldGetEmailMiembroYautorizadoByReunionId()
    {
        boolean conMiembro = true;
        Reunion reunion = getReunionConOrgano(conMiembro);

        new OrganoAutorizadoBuilder().withOrganoId(reunion.getReunionOrganos().iterator().next().getOrganoId()).withPersonaEmail("autorizado@4tic.com")
                .build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<String> mailsReunion = organoReunionMiembroDAO.getMailsReunion(reunion);
        assertThat(mailsReunion.size(), is(2));
        assertThat(mailsReunion.contains("miembro.organo@4tic.com"), is(true));
        assertThat(mailsReunion.contains("autorizado@4tic.com"), is(true));
    }

    @Test
    public void shouldGetMailMiembro()
    {
        boolean conMiembro = true;
        Reunion reunion = getReunionConOrgano(conMiembro);

        entityManager.flush();
        entityManager.clear();

        List<String> mailsReunion = organoReunionMiembroDAO.getMailsReunion(reunion);
        assertThat(mailsReunion.size(), is(1));
        assertThat(mailsReunion.contains("miembro.organo@4tic.com"), is(true));
    }

    @Test
    public void shouldGetEmailAutorizadoByReunionId()
    {
        boolean conMiembro = false;
        Reunion reunion = getReunionConOrgano(conMiembro);

        new OrganoAutorizadoBuilder().withOrganoId(reunion.getReunionOrganos().iterator().next().getOrganoId()).withPersonaEmail("autorizado@4tic.com")
                .build(entityManager);

        entityManager.flush();
        entityManager.clear();

        List<String> mailsReunion = organoReunionMiembroDAO.getMailsReunion(reunion);
        assertThat(mailsReunion.size(), is(1));
        assertThat(mailsReunion.contains("autorizado@4tic.com"), is(true));
    }

    private Reunion getReunionConOrgano(boolean conMiembro)
    {
        reunionCompletada = new ReunionBuilder().withAsunto("Reunión 1")
                .withFecha(new Date()).withAvisoPrimeraReunion(true).completada(true)
                .build(entityManager);

        tipo = new TipoOrganoBuilder().withNombre("Tipo 1").build(entityManager);

        OrganoLocal organoTest =
                new OrganoBuilder().withNombre("organo test").withTipoOrgano(tipo).build(entityManager);

        OrganoReunion organoReunion = null;
        OrganoReunionBuilder organoReunionBuilder = new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest);
        if (conMiembro)
            organoReunion = organoReunionBuilder.withMiembro("miembro.organo@4tic.com", true).build(entityManager);
        else {
            organoReunion = organoReunionBuilder.build(entityManager);
        }

        reunionCompletada.setReunionOrganos(Collections.singleton(organoReunion));
        return reunionCompletada;
    }

    @Test
    public void shouldReturnEmailInvitadoReunionSinOrgano(){
        Reunion reunionSinOrgano = getReunionSinOrgano(true);

        List<String> mailsReunion = organoReunionMiembroDAO.getMailsReunion(reunionSinOrgano);
        assertThat(mailsReunion.size(), is(1));
        assertThat(mailsReunion.contains("invitado.reunion@4tic.com"), is(true));
    }

    private Reunion getReunionSinOrgano(boolean conInvitado)
    {
        ReunionBuilder reunionCompletadaBuilder = new ReunionBuilder().withAsunto("Reunión 1")
                .withFecha(new Date()).withAvisoPrimeraReunion(true).completada(true);

        Reunion reunion = null;
        if (conInvitado)
            reunion = reunionCompletadaBuilder.withInvitado("invitado.reunion@4tic.com").build(entityManager);
        else {
            reunion = reunionCompletadaBuilder.build(entityManager);
        }
        return reunion;
    }

    @Test
    public void shouldGetPersonasAsistentesMails() {
        Reunion reunionConMiembros = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionCompletada.getId());
        List<String> mailsMiembrosInvitadosYautorizados =
            organoReunionMiembroDAO.getMailsAsistenciaConfirmadaReunion(reunionConMiembros);

        assertThat(mailsMiembrosInvitadosYautorizados.size(), is(6));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("invitado.organo@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("miembro.organo@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("suplente@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("invitado.organo@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("autorizado@4tic.com"), is(true));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("presideVotacion@4tic.com"), is(true));
    }

    @Test
    public void shouldNotGetautorizadoOtroOrganoMails() {
        Reunion reunionConMiembros = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionCompletada.getId());

        OrganoLocal organoNuevo =
            new OrganoBuilder().withNombre("organo nuevo").withTipoOrgano(tipo).build(entityManager);

        new OrganoAutorizadoBuilder()
            .withOrganoId(organoNuevo.getId().toString())
            .withPersonaEmail("noautorizado@4tic.com")
            .build(entityManager);

        List<String> mailsMiembrosInvitadosYautorizados =
            organoReunionMiembroDAO.getMailsAsistenciaConfirmadaReunion(reunionConMiembros);

        assertThat(mailsMiembrosInvitadosYautorizados.size(), is(6));
        assertThat(mailsMiembrosInvitadosYautorizados.contains("noautorizado@4tic.com"), is(false));
    }

    @Test
    public void shouldGetConvocantesReunion() {
        List<Convocante> organosConvocantes =
            organoDAO.getOrganosReunionWithEmailByReunionId(reunionCompletada.getId());

        List<String> mailsOrganosConvocantes = organosConvocantes.stream().map(Convocante::getEmail).collect(Collectors.toList());
        assertThat(mailsOrganosConvocantes.size(), is(1));
    }

    @Test
    public void shouldGetFalsePresideVotacionWhenAsistenteNotPreside() {
        boolean preside = organoReunionMiembroDAO.isPresideVotacion(reunionCompletada.getId(), 1L);
        Assert.assertFalse(preside);
    }

    @Test
    public void shouldGetTruePresideVotacionWhenAsistentePreside() {
        boolean preside = organoReunionMiembroDAO.isPresideVotacion(reunionCompletada.getId(), presideVotacion.getPersonaId());
        Assert.assertTrue(preside);
    }

    @Test
    public void shouldNotGetMiembroWhenNoAsistente() {
        List<OrganoReunionMiembro> asistentes =
            organoReunionMiembroDAO.getAsistentesByReunionId(reunionCompletada.getId());

        Assert.assertTrue(asistentes.size() > 0);
        Assert.assertTrue(asistentes.stream().filter(m -> m.getEmail().equals("miembro.organo.no.asiste@4tic.com")).count() == 0);
    }
}
