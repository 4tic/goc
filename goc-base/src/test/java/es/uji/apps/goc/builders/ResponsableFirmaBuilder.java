package es.uji.apps.goc.builders;

import es.uji.apps.goc.model.ResponsableFirma;

import javax.persistence.EntityManager;

public class ResponsableFirmaBuilder {

    private ResponsableFirma responsableFirma;

    public ResponsableFirmaBuilder ResponsableFirmaBuilder(){
        this.responsableFirma = new ResponsableFirma();
        return this;
    }

    public ResponsableFirmaBuilder withEmail(String email){
        this.responsableFirma.setEmail(email);
        return this;
    }

    public ResponsableFirmaBuilder withCargo(String cargo){
        this.responsableFirma.setCargo(cargo);
        return this;
    }

    public ResponsableFirmaBuilder withNombre(String nombre){
        this.responsableFirma.setNombre(nombre);
        return this;
    }

    public ResponsableFirma build(EntityManager entityManager){
        entityManager.persist(responsableFirma);
        return responsableFirma;
    }
}
