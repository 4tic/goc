package es.uji.apps.goc.services;

import org.junit.Before;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.uji.apps.goc.builders.MiembroLocalBuilder;
import es.uji.apps.goc.builders.OrganoAutorizadoBuilder;
import es.uji.apps.goc.builders.OrganoBuilder;
import es.uji.apps.goc.builders.PuntoOrdenDiaBuilder;
import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.builders.TipoOrganoBuilder;
import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoAutorizado;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;

@ActiveProfiles("test")
@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class BaseServiceTest {
    @PersistenceContext
    protected EntityManager entityManager;

    Reunion reunionCompletada;
    Reunion reunionConvocada;
    Reunion reunionConvocadaYCompletada;
    Reunion reunionNoConvocada;
    TipoOrganoLocal tipoOrgano;
    MiembroLocal miembroPresideVotacion;
    OrganoLocal organoAdmin;
    OrganoLocal organoTest;
    OrganoLocal organoSinReuniones;
    PuntoOrdenDia subpunto;
    PuntoOrdenDia puntoEnReunionCompletada;
    PuntoOrdenDia puntoEnReunionConvocada;
    PuntoOrdenDia puntoEnReunionNoConvocada;
    PuntoOrdenDia subpunto2;
    OrganoAutorizado autorizado;
    Date yesterday;
    Date tomorrow;

    final long CREADOR_ID = 88849L;

    @Before
    public void setUp() throws RolesPersonaExternaException {
        Date tomorrowAux =  new Date();
        LocalDateTime localDateTime = tomorrowAux.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        yesterday = Date.from(localDateTime.plusDays(-11L).atZone(ZoneId.systemDefault()).toInstant());
        tomorrow = Date.from(localDateTime.plusDays(1L).atZone(ZoneId.systemDefault()).toInstant());

        tipoOrgano = new TipoOrganoBuilder().withCodigo("2").withNombre("TEST").build(entityManager);
        TipoOrganoLocal tipoOrgano2 = new TipoOrganoBuilder().withCodigo("3").withNombre("TEST 2").build(entityManager);

        miembroPresideVotacion =
            new MiembroLocalBuilder().withPersonaId(99L).withEmail("admin@4tic.com").withPresideVotacion(true).build(entityManager);

        organoAdmin =
                new OrganoBuilder().withNombre("ORGANO ADMIN").withTipoOrgano(tipoOrgano).withCreadorId(CREADOR_ID)
                        .withMiembro(miembroPresideVotacion)
                        .withInvitadoWithPersonaId("1")
                        .build(entityManager);

        OrganoLocal organo2 = new OrganoBuilder().withNombre("ORGANO 2").withTipoOrgano(tipoOrgano2).withCreadorId(CREADOR_ID)
                .build(entityManager);
        organoTest = new OrganoBuilder().withNombre("ORGANO TEST").withTipoOrgano(tipoOrgano).withCreadorId(CREADOR_ID)
                .build(entityManager);
        organoSinReuniones = new OrganoBuilder().withNombre("ORGANO SIN REUNIONES").withTipoOrgano(tipoOrgano).withCreadorId(CREADOR_ID)
                .build(entityManager);

        reunionCompletada =
                new ReunionBuilder().publica().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).completada(true)
                        .withOrgano(organoAdmin)
                        .build(entityManager);

        reunionConvocadaYCompletada =
                new ReunionBuilder().publica().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).completada(true).isTelematica(true)
                        .withOrgano(organoAdmin)
                        .withAvisoPrimeraReunion(true)
                        .withHasVotacion(true)
                        .build(entityManager);

        reunionConvocada =
            new ReunionBuilder().publica().withAsunto("Reunión 1").withCreadorId(CREADOR_ID).isTelematica(true)
                .withOrgano(organoAdmin)
                .convocada()
                .withHasVotacion(true)
                .build(entityManager);

        puntoEnReunionConvocada = new PuntoOrdenDiaBuilder(reunionConvocada, 3L).withTitulo("Punto 1").publico().withVotoPublico(true).build(entityManager);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime fechaFutura = now.plusDays(2);
        reunionNoConvocada =  new ReunionBuilder()
            .withFecha(Date.from(fechaFutura.toInstant(ZoneOffset.UTC)))
            .publica().withAsunto("Reunión 1").withCreadorId(CREADOR_ID)
                .withAvisoPrimeraReunion(false)
                .withOrgano(organoAdmin)
                .withHasVotacion(true)
                .build(entityManager);

        puntoEnReunionNoConvocada = new PuntoOrdenDiaBuilder(reunionNoConvocada, 3L).withTitulo("Punto 1").publico().withVotoPublico(true).build(entityManager);

        new ReunionBuilder().publica().withAsunto("Reunión 2").withCreadorId(CREADOR_ID).completada(true)
                .withOrgano(organo2)
                .build(entityManager);

        new ReunionBuilder().publica().withAsunto("Reunión sin órgano creador inventado").withCreadorId(111L).build(entityManager);

        new ReunionBuilder().publica().withAsunto("Reunión sin órgano").withCreadorId(CREADOR_ID).build(entityManager);

        subpunto =
                new PuntoOrdenDiaBuilder(reunionCompletada, 2L).withTitulo("Subpunto 1").publico().withVotoPublico(true).withFechaAperturaVotacion(tomorrow).build(entityManager);
        new PuntoOrdenDiaBuilder(reunionCompletada, 1L).withTitulo("Punto 1").withSubpunto(subpunto).publico()
                .build(entityManager);

        puntoEnReunionCompletada = new PuntoOrdenDiaBuilder(reunionCompletada, 3L).withTitulo("Punto 2").publico().withVotoPublico(true).build(entityManager);

        subpunto2 =
                new PuntoOrdenDiaBuilder(reunionCompletada, 2L).withTitulo("Subpunto 2").publico().withVotoPublico(false).withFechaAperturaVotacion(Date.from(now.toInstant(ZoneOffset.UTC))).build(entityManager);

        autorizado = new OrganoAutorizadoBuilder().withOrganoId(organoAdmin.getId().toString()).withPersonaEmail("autorizado@4tic.com").withPersonaId(6L)
                .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }
}
