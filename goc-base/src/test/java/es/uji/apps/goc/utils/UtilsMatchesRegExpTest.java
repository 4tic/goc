package es.uji.apps.goc.utils;

import org.junit.Test;

import es.uji.apps.goc.Utils;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class UtilsMatchesRegExpTest
{
    private final String EXCLUDED_URL_PATTERNS = "/goc/rest/publicacion/acuerdos(\\/.*)?|/goc/js/.*|/goc/css/.*|/goc/img/.*|/goc/app/.*|/goc/rest/external/.*";

    @Test
    public void shouldMatchExcludedUrlBuscadorAcuerdos() throws Exception
    {
        String excludedURL = "/goc/rest/publicacion/acuerdos";

        assertThat(Utils.matchesRegExp(excludedURL, EXCLUDED_URL_PATTERNS), is(true));
    }

    @Test
    public void shouldMatchExcludedUrlFicherosCss() throws Exception
    {
        String excludedURL = "/goc/css/jquery.modal.css";

        assertThat(Utils.matchesRegExp(excludedURL, EXCLUDED_URL_PATTERNS), is(true));
    }

    @Test
    public void shouldMatchExcludedUrlFicherosJs() throws Exception
    {
        String excludedURL = "/goc/js/acuerdos.js";

        assertThat(Utils.matchesRegExp(excludedURL, EXCLUDED_URL_PATTERNS), is(true));
    }
}
