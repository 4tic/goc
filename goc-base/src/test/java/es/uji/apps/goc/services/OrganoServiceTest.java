package es.uji.apps.goc.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import es.uji.apps.goc.dao.OrganoReunionInvitadoDAO;
import es.uji.apps.goc.dto.OrganoReunionInvitado;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class OrganoServiceTest extends BaseServiceTest {

    @Autowired
    OrganoService organoService;

    @Autowired
    OrganoReunionInvitadoDAO organoReunionInvitadoDAO;

    @Test
    public void shouldDeleteInvitadosdeOrganoEnReunionesNoConvocadas() {
        int personaId = 1;
        List<OrganoReunionInvitado> invitadosALaReunionOriginal = this.organoReunionInvitadoDAO.getInvitadosOrganoByReunionId(reunionNoConvocada.getId());
        assertThat("El invitado del organo existe en la reunion", isPersonaWithIdInvitada(invitadosALaReunionOriginal, personaId), is(true));

        Long idInvitadoAEliminarDelOrgano = organoAdmin.getInvitados().iterator().next().getId();
        organoService.removeInvitado(idInvitadoAEliminarDelOrgano);
        invitadosALaReunionOriginal = organoReunionInvitadoDAO.getInvitadosOrganoByReunionId(reunionNoConvocada.getId());
        assertThat("El invitado del organo no existe en la reunion", isPersonaWithIdInvitada(invitadosALaReunionOriginal, personaId), is(false));
    }

    private boolean isPersonaWithIdInvitada(List<OrganoReunionInvitado> invitadosALaReunionOriginal, int personaId) {
        boolean encontrado = false;
        for (OrganoReunionInvitado organoReunionInvitado: invitadosALaReunionOriginal) {
            if (organoReunionInvitado.getPersonaId().equalsIgnoreCase(new Integer(personaId).toString()))
                encontrado = true;
        }
        return encontrado;
    }

    @Test
    public void shouldNotDeleteInvitadosDeOrganoEnReunionesYaConvocadas() {
        int personaId = 1;
        List<OrganoReunionInvitado> invitadosALaReunionOriginal = organoReunionInvitadoDAO.getInvitadosOrganoByReunionId(
            reunionConvocadaYCompletada.getId());
        assertThat("El invitado del organo existe en la reunion", isPersonaWithIdInvitada(invitadosALaReunionOriginal, personaId), is(true));

        Long idInvitadoAEliminarDelOrgano = organoAdmin.getInvitados().iterator().next().getId();
        organoService.removeInvitado(idInvitadoAEliminarDelOrgano);

        invitadosALaReunionOriginal = organoReunionInvitadoDAO.getInvitadosOrganoByReunionId(
            reunionConvocadaYCompletada.getId());
        assertThat("El invitado del organo existe en la reunion", isPersonaWithIdInvitada(invitadosALaReunionOriginal, personaId), is(true));

    }

    @Test
    public void shouldNotDeleteInvitadosDeOrganoEnReunionesCompletadas() {
        int personaId = 1;
        List<OrganoReunionInvitado> invitadosALaReunionOriginal = organoReunionInvitadoDAO.getInvitadosOrganoByReunionId(reunionCompletada.getId());
        assertThat("El invitado del organo existe en la reunion", isPersonaWithIdInvitada(invitadosALaReunionOriginal, personaId), is(true));

        Long idInvitadoAEliminarDelOrgano = organoAdmin.getInvitados().iterator().next().getId();
        organoService.removeInvitado(idInvitadoAEliminarDelOrgano);

        invitadosALaReunionOriginal = organoReunionInvitadoDAO.getInvitadosOrganoByReunionId(reunionCompletada.getId());
        assertThat("El invitado del organo existe en la reunion", isPersonaWithIdInvitada(invitadosALaReunionOriginal, personaId), is(true));

    }
}