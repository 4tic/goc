package es.uji.apps.goc.builders;

import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;

import javax.persistence.EntityManager;

public class PuntoOrdenDiaDocumentoBuilder {

    private PuntoOrdenDiaDocumento puntoOrdenDiaDocumento;

    public PuntoOrdenDiaDocumentoBuilder() {
        this.puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();
    }

    public PuntoOrdenDiaDocumentoBuilder withNombre(String nombre) {
        this.puntoOrdenDiaDocumento.setNombreFichero(nombre);
        return this;
    }

    public PuntoOrdenDiaDocumentoBuilder withDatos() {

        byte[] datos ="DATOS PRUEBA".getBytes();
        this.puntoOrdenDiaDocumento.setDatos(datos);
        return this;
    }

    public PuntoOrdenDiaDocumento build(EntityManager entityManager){
        entityManager.persist(this.puntoOrdenDiaDocumento);
        return this.puntoOrdenDiaDocumento;
    }

}
