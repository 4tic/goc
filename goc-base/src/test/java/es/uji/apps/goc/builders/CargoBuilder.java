package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.Cargo;

public class CargoBuilder {
    private Cargo cargo;

    public CargoBuilder(){
        this.cargo = new Cargo();
    }
    public  CargoBuilder withNombre(String nombre){
       this.cargo.setNombre(nombre);
       return this;
    }

    public CargoBuilder withCodigo(String codigo){
        this.cargo.setCodigo(codigo);
        return this;
    }
    public Cargo build(EntityManager entityManager) {
        entityManager.persist(cargo);
        return this.cargo;
    }

}
