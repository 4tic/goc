package es.uji.apps.goc.dao;

import es.uji.apps.goc.builders.*;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.services.MiembroService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class OrganoReunionDAOTest extends BaseDAOTest {

    @Autowired
    MiembroService miembroService;

    @Autowired
    OrganoReunionDAO organoReunionDao;

    final long CREADOR_ID = 88849L;

    TipoOrganoLocal tipoOrgano;
    OrganoLocal organoPasado;
    OrganoLocal organoFuturo;
    Reunion reunionConvocadaNoCompletadaFechaPasada;
    Reunion reunionConvocadaNoCompletadaFechaFutura;
    Miembro miembro;

    @Before
    public void setUpContext()
    {
        tipoOrgano = new TipoOrganoBuilder().withCodigo("2").withNombre("TEST").build(entityManager);
        organoPasado =
                new OrganoBuilder().withNombre("ORGANO REUNION PASADA").withTipoOrgano(tipoOrgano).withCreadorId(CREADOR_ID)
                        .withMiembro(new MiembroLocalBuilder().withPersonaId(99L).withEmail("admin@4tic.com").build(entityManager))
                        .withInvitadoWithPersonaId("1")
                        .build(entityManager);

        organoFuturo =
                new OrganoBuilder().withNombre("ORGANO REUNION FUTURA").withTipoOrgano(tipoOrgano).withCreadorId(CREADOR_ID)
                        .withMiembro(new MiembroLocalBuilder().withPersonaId(99L).withEmail("admin@4tic.com").build(entityManager))
                        .withInvitadoWithPersonaId("1")
                        .build(entityManager);

        LocalDateTime now = LocalDateTime.now();

        LocalDateTime fechaPasada = now.minusHours(2);
        Date fechaPasadaDate = Date.from(fechaPasada.toInstant(ZoneOffset.UTC));

        reunionConvocadaNoCompletadaFechaPasada =
                new ReunionBuilder().publica().withAsunto("Reunión 1").withFecha(fechaPasadaDate).withCreadorId(CREADOR_ID).completada(false).isTelematica(false)
                        .withOrgano(organoPasado)
                        .withAvisoPrimeraReunion(true)
                        .build(entityManager);

        LocalDateTime fechafutura = now.plusHours(2);
        Date fechaFuturaDate = Date.from(fechafutura.toInstant(ZoneOffset.UTC));
        reunionConvocadaNoCompletadaFechaFutura =
                new ReunionBuilder().publica().withAsunto("Reunión 1").withFecha(fechaFuturaDate).withCreadorId(CREADOR_ID).completada(false).isTelematica(false)
                        .withOrgano(organoFuturo)
                        .withAvisoPrimeraReunion(true)
                        .build(entityManager);

        miembro = new MiembroBuilder().withNombre("Nuevo Miembro").withEmail("nuevoMiembroPasado@4tic.com").withOrganoId(organoPasado.getId()).withPersonaId(11111L).build();
        miembro = new MiembroBuilder().withNombre("Nuevo Miembro").withEmail("nuevoMiembroFuturo@4tic.com").withOrganoId(organoFuturo.getId()).withPersonaId(11111L).build();
    }

    @Test
    public void ShouldNotReturnOrganoFechaPasada()
    {
        List<OrganoReunion> organosFechaReunionBeforeToday = organoReunionDao.getOrganosReunionNoCompletadasAndFechaReunionAfterTodayByOrganoId(organoPasado.getId());
        assertThat(organosFechaReunionBeforeToday.size() ,is(0));
    }


    @Test
    public void ShouldReturnOrganoFechaFutura()
    {
        List<OrganoReunion> organosFechaReunionAfterToday = organoReunionDao.getOrganosReunionNoCompletadasAndFechaReunionAfterTodayByOrganoId(organoFuturo.getId());
        assertThat(organosFechaReunionAfterToday.size() ,is(1));
    }

}