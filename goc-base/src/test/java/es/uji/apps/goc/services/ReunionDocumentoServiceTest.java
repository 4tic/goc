package es.uji.apps.goc.services;

import es.uji.apps.goc.builders.PuntoOrdenDiaBuilder;
import es.uji.apps.goc.builders.PuntoOrdenDiaDocumentoBuilder;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReunionDocumentoServiceTest extends BaseServiceTest {

    @Autowired
    ReunionDocumentoService reunionDOcumentoService;

    @Autowired
    PersonaService personaService;

    private PuntoOrdenDia subpuntoTituloLargo;

    private PuntoOrdenDia punto;

    @Before
    public void setUp() throws RolesPersonaExternaException
    {
        super.setUp();
        String tituloLargo = "subpunto 1.1con titulo extra largo para provocar que se genere un error de filename too long al intentar descargar la documentación de la reunión desde el fichero zip conjunto // Aprovació, si escau, de l’Acord de les places de professorat agregat i titular d’Universitat, en el seu cas vinculades, que s’acorda convocar, i pel que s’adopten mesures excepcionals per al curs 2020-21, atenent a la greu crisi sanitària derivada de la COVID-19, per diferir la seva convocatòria, i autoritzant la provisió interina de les places no vinculades.";
        PuntoOrdenDiaDocumento documento = new PuntoOrdenDiaDocumentoBuilder().withNombre("Documento Punto Orden Dia test").withDatos().build(entityManager);
        subpuntoTituloLargo = new PuntoOrdenDiaBuilder(reunionConvocadaYCompletada, 2L).withTitulo(tituloLargo).withDocumentos(documento).build(entityManager);
        punto = new PuntoOrdenDiaBuilder(reunionConvocadaYCompletada, 1L).withTitulo("Punto 1 con subpuntos").withSubpunto(subpuntoTituloLargo).build(entityManager);
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldCutLongTItleDocumentoLong()
    {
        String getTituloFormateado = reunionDOcumentoService.formatearTituloParaDirectorio(subpuntoTituloLargo.getTitulo());
        assertThat(getTituloFormateado, is(subpuntoTituloLargo.getTitulo().substring(0, 25)));
    }
    @Test
    public void shouldGetTitlesDocumento()
    {
        String getTituloFormateado = reunionDOcumentoService.formatearTituloParaDirectorio(punto.getTitulo());
        assertThat(getTituloFormateado, is(punto.getTitulo()));
    }
}
