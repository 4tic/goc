package es.uji.apps.goc.services;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import es.uji.apps.goc.builders.PuntoOrdenDiaBuilder;
import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.builders.VotoPublicoBuilder;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaTemplate;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.exceptions.PuntoOrdenDiaNoDisponibleException;
import es.uji.apps.goc.exceptions.YaSeHaVotadoPuntoException;
import es.uji.apps.goc.model.VoteType;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class PuntoOrdenDiaServiceTest extends BaseServiceTest {
    @Autowired
    PuntoOrdenDiaService puntoOrdenDiaService;

    @Autowired
    ReunionService reunionService;

    @Autowired
    PersonaService personaService;

    private Reunion reunionVotacionPublica;
    private PuntoOrdenDia puntoEnReunionVotacionPublica;
    private PuntoOrdenDia puntoEnReunionVotacionPublica2;

    @Before
    public void setUpContext() {
        reunionVotacionPublica =
            new ReunionBuilder().publica().withAsunto("Reunión votación pública").withCreadorId(CREADOR_ID)
                .completada(false).withHasVotacion(true).convocada().withOrgano(organoAdmin)
                .build(entityManager);

        puntoEnReunionVotacionPublica =
            new PuntoOrdenDiaBuilder(reunionVotacionPublica, 10L).withTitulo("punto 1 votación pública")
                .build(entityManager);

        puntoEnReunionVotacionPublica2 =
            new PuntoOrdenDiaBuilder(reunionVotacionPublica, 10L).withTitulo("punto 1 votación pública")
                .withVotoPublico(true).build(entityManager);
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    public void shouldGetPuntoOrdenDiaTemplate() throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        ReunionTemplate reunionTemplate =
            reunionService.getReunionTemplateDesdeReunion(reunionCompletada, CREADOR_ID, true, true);

        PuntoOrdenDiaTemplate puntoOrdenDiaTemplate = puntoOrdenDiaService
            .getPuntoOrdenDiaTemplate(reunionTemplate.getPuntosOrdenDia(), puntoEnReunionCompletada.getId());
        Assert.assertNotNull(puntoOrdenDiaTemplate);
    }

    @Test
    public void shouldGetSubpuntoOrdenDiaTemplate() throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        ReunionTemplate reunionTemplate =
            reunionService.getReunionTemplateDesdeReunion(reunionCompletada, CREADOR_ID, true, true);

        PuntoOrdenDiaTemplate puntoOrdenDiaTemplate =
            puntoOrdenDiaService.getPuntoOrdenDiaTemplate(reunionTemplate.getPuntosOrdenDia(), subpunto.getId());
        Assert.assertNotNull(puntoOrdenDiaTemplate);
    }

    @Test
    public void shouldGetPuntosVotoPublicoFromReunion() {
        List<PuntoOrdenDia> puntosOrdenDiaVotacionPublicaReunionId =
            puntoOrdenDiaService.getPuntosOrdenDiaVotacionPublicaReunionId(reunionCompletada.getId());
        boolean todosPuntosPublicos = true;
        for (PuntoOrdenDia puntoOrdenDia : puntosOrdenDiaVotacionPublicaReunionId) {
            if (puntoOrdenDia.getVotoPublico().equals(false)) {
                todosPuntosPublicos = false;
                break;
            }
        }
        Assert.assertThat(todosPuntosPublicos, is(true));
    }

    @Test
    public void shouldGetPuntosVotoPrivadoFromReunion() {
        List<PuntoOrdenDia> puntosOrdenDiaVotacionPrivadaReunionId =
            puntoOrdenDiaService.getPuntosOrdenDiaVotacionPrivadaReunionId(reunionCompletada.getId());
        boolean todosPuntosPrivados = true;
        for (PuntoOrdenDia puntoOrdenDia : puntosOrdenDiaVotacionPrivadaReunionId) {
            if (puntoOrdenDia.getVotoPublico().equals(true)) {
                todosPuntosPrivados = false;
                break;
            }
        }
        Assert.assertThat(todosPuntosPrivados, is(true));
    }

    @Test
    public void shouldGetTrueWhenReunionHasNotVotacion() throws YaSeHaVotadoPuntoException {
        Boolean isPrivacyModificable = puntoOrdenDiaService
            .sePuedeEditarPrivacidadDeVoto(puntoEnReunionCompletada.getId(), reunionVotacionPublica.getId());
        assertTrue(isPrivacyModificable);
    }

    @Test
    public void shouldGetTrueWhenReunionHasVotacionButNoVotes() throws YaSeHaVotadoPuntoException {
        Boolean isPrivacyModificable = puntoOrdenDiaService
            .sePuedeEditarPrivacidadDeVoto(puntoEnReunionCompletada.getId(), reunionVotacionPublica.getId());
        assertTrue(isPrivacyModificable);
    }

    @Test
    public void shouldGetFalseWhenReunionHasVotacionAndVotesButHasNotVotacion() throws YaSeHaVotadoPuntoException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublica.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name())
            .build(entityManager);
        entityManager.flush();
        entityManager.clear();

        Boolean isPrivacyModificable = puntoOrdenDiaService
            .sePuedeEditarPrivacidadDeVoto(puntoEnReunionVotacionPublica.getId(), reunionVotacionPublica.getId());
        assertFalse(isPrivacyModificable);
    }

    @Test
    public void shouldGetTueWhenPuntoHasNotVotes() throws YaSeHaVotadoPuntoException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublica2.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name())
            .build(entityManager);
        entityManager.flush();
        entityManager.clear();

        boolean sePuedeEditarPrivacidadDeVoto = puntoOrdenDiaService
            .sePuedeEditarPrivacidadDeVoto(puntoEnReunionVotacionPublica.getId(), reunionVotacionPublica.getId());
        assertTrue(sePuedeEditarPrivacidadDeVoto);
    }

    @Test
    public void shouldGetFalseWhenPuntoHasVotes() throws YaSeHaVotadoPuntoException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublica2.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name())
            .build(entityManager);
        entityManager.flush();
        entityManager.clear();

        boolean sePuedeEditarPrivacidadDeVoto = puntoOrdenDiaService
            .sePuedeEditarPrivacidadDeVoto(puntoEnReunionVotacionPublica2.getId(), reunionVotacionPublica.getId());
        assertFalse(sePuedeEditarPrivacidadDeVoto);
    }

    @Test
    public void shouldUpdatePrivacidadVotoWhenPuntoHasNotVotes()
        throws YaSeHaVotadoPuntoException, PuntoOrdenDiaNoDisponibleException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublica2.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name())
            .build(entityManager);
        entityManager.flush();
        entityManager.clear();

        Boolean notVotoPublico = !puntoEnReunionVotacionPublica.getVotoPublico();
        puntoOrdenDiaService.updatePuntoOrdenDia(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublica.getId(),
            puntoEnReunionVotacionPublica.getTitulo() + " EDITADO",
            puntoEnReunionVotacionPublica.getTituloAlternativo(), puntoEnReunionVotacionPublica.getDescripcion(),
            puntoEnReunionVotacionPublica.getDescripcionAlternativa(),
            puntoEnReunionVotacionPublica.getDeliberaciones(),
            puntoEnReunionVotacionPublica.getDeliberacionesAlternativas(), puntoEnReunionVotacionPublica.getAcuerdos(),
            puntoEnReunionVotacionPublica.getAcuerdosAlternativos(), 1L, puntoEnReunionVotacionPublica.isPublico(),
            notVotoPublico, false);
    }

    @Test(expected = YaSeHaVotadoPuntoException.class)
    public void shouldGetExceptionWhenPuntoHasNotVotes()
        throws YaSeHaVotadoPuntoException, PuntoOrdenDiaNoDisponibleException {
        new VotoPublicoBuilder(reunionVotacionPublica.getId().toString(),
            puntoEnReunionVotacionPublica2.getId().toString(), String.valueOf(CREADOR_ID), VoteType.FAVOR.name())
            .build(entityManager);
        entityManager.flush();
        entityManager.clear();

        Boolean notVotoPublico = !puntoEnReunionVotacionPublica2.getVotoPublico();
        puntoOrdenDiaService.updatePuntoOrdenDia(reunionVotacionPublica.getId(), puntoEnReunionVotacionPublica2.getId(),
            puntoEnReunionVotacionPublica2.getTitulo() + " EDITADO",
            puntoEnReunionVotacionPublica2.getTituloAlternativo(), puntoEnReunionVotacionPublica2.getDescripcion(),
            puntoEnReunionVotacionPublica2.getDescripcionAlternativa(),
            puntoEnReunionVotacionPublica2.getDeliberaciones(),
            puntoEnReunionVotacionPublica2.getDeliberacionesAlternativas(),
            puntoEnReunionVotacionPublica2.getAcuerdos(), puntoEnReunionVotacionPublica2.getAcuerdosAlternativos(), 1L,
            puntoEnReunionVotacionPublica2.isPublico(), notVotoPublico, false);
    }

    @Test
    public void shouldGetFalseWhenReunionCompletada() {
        boolean votacionAbierta = puntoOrdenDiaService.isVotacionVotableYAbierta(subpunto2.getId());
        Assert.assertThat(votacionAbierta, is(false));
    }

    @Test
    public void shouldGetFalseFechaPosterior() {
        boolean votacionAbierta = puntoOrdenDiaService.isVotacionVotableYAbierta(subpunto.getId());
        Assert.assertThat(votacionAbierta, is(false));
    }

    @Test
    public void shouldGetFalseFechaNull() {
        boolean votacionAbierta = puntoOrdenDiaService.isVotacionVotableYAbierta(puntoEnReunionCompletada.getId());
        Assert.assertThat(votacionAbierta, is(false));
    }

    @Test
    public void shouldGetFalseWhenReunionIsNotConvocada(){
        boolean votacionActualmenteAbierta = puntoOrdenDiaService.isVotacionVotableYAbierta(puntoEnReunionNoConvocada.getId());
        assertThat(votacionActualmenteAbierta, Is.is(false));
    }
}
