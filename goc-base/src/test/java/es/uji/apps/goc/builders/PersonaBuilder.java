package es.uji.apps.goc.builders;

import es.uji.apps.goc.model.Persona;

public class PersonaBuilder {
    Persona persona;

    public PersonaBuilder()
    {
        this.persona = new Persona();
    }

    public PersonaBuilder withNombre(String nombre)
    {
        this.persona.setNombre(nombre);
        return this;
    }

    public PersonaBuilder withEmail(String email)
    {
        this.persona.setEmail(email);
        return this;
    }

    public Persona build()
    {
        return this.persona;
    }
}
