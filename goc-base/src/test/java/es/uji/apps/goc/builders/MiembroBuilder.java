package es.uji.apps.goc.builders;

import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;

public class MiembroBuilder {
    private Miembro miembro;

    public MiembroBuilder()
    {
        this.miembro =  new Miembro();
        this.miembro.setVotante(true);
    }

    public MiembroBuilder withId(Long id){
        this.miembro.setId(id);
        return this;
    }

    public MiembroBuilder withPersonaId(Long personaId){
        this.miembro.setPersonaId(personaId);
        return this;
    }

    public MiembroBuilder withNombre(String nombre){
        this.miembro.setNombre(nombre);
        return this;
    }
    public MiembroBuilder withEmail(String email){
        this.miembro.setEmail(email);
        return this;
    }

    public MiembroBuilder withOrganoId(Long organoId){
        Organo organo = new Organo();
        String organoIdString = String.valueOf(organoId);
        organo.setId(organoIdString);
        this.miembro.setOrgano(organo);
        return this;
    }

    public Miembro build(){
        return this.miembro;
    }
}
