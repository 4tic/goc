package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.VotantePrivadoDTO;

public class VotantePrivadoBuilder
{
    VotantePrivadoDTO votantePrivado;

    public VotantePrivadoBuilder(String reunionId, String puntoId, String personaId)
    {
        this.votantePrivado = new VotantePrivadoDTO();
        this.votantePrivado.setReunionId(reunionId);
        this.votantePrivado.setPuntoOrdenDiaId(puntoId);
        this.votantePrivado.setPersonaId(personaId);
    }

    public VotantePrivadoBuilder withVotoEnNombreDe(String nombreDe) {
        this.votantePrivado.setVotoEnNombreDe(nombreDe);
        return this;
    }

    public VotantePrivadoBuilder withVotoEnNombreDePersonaId(String enNombreDePersonaId) {
        this.votantePrivado.setVotoEnNombreDePersonaId(enNombreDePersonaId);
        return this;
    }

    public VotantePrivadoDTO build(EntityManager entityManager){
        entityManager.persist(votantePrivado);
        return votantePrivado;
    }
}
