package es.uji.apps.goc.builders;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.VotoPublicoDTO;
import es.uji.apps.goc.model.VoteType;

public class VotoPublicoBuilder
{
    VotoPublicoDTO voto;

    public VotoPublicoBuilder(String reunionId, String puntoId, String personaId, String voto)
    {
        this.voto = new VotoPublicoDTO();
        this.voto.setReunionId(reunionId);
        this.voto.setPuntoOrdenDiaId(puntoId);
        this.voto.setPersonaId(personaId);
        if (voto.equals(VoteType.CONTRA.name()) || voto.equals(VoteType.FAVOR.name()) || voto.equals(VoteType.ABSTENCION.name())) {
            this.voto.setVoto(voto);
        } else{
            System.out.println("Valor voto erroneo");
        }
    }

    public VotoPublicoBuilder withVotoEnNombreDe(String nombreDe) {
        this.voto.setVotoEnNombreDe(nombreDe);
        return this;
    }

    public VotoPublicoBuilder withVotoEnNombreDePersonaId(String enNombreDePersonaId) {
        this.voto.setVotoEnNombreDePersonaId(enNombreDePersonaId);
        return this;
    }

    public VotoPublicoBuilder withNombre(String nombre) {
        this.voto.setNombre(nombre);
        return this;
    }

    public VotoPublicoDTO build(EntityManager entityManager){
        entityManager.persist(voto);
        return voto;
    }
}
