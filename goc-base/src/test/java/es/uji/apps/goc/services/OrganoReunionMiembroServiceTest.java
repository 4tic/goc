package es.uji.apps.goc.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.uji.apps.goc.builders.MiembroLocalBuilder;
import es.uji.apps.goc.builders.OrganoBuilder;
import es.uji.apps.goc.builders.OrganoReunionBuilder;
import es.uji.apps.goc.builders.PuntoOrdenDiaBuilder;
import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.builders.VotoPublicoBuilder;
import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.VoteType;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
public class OrganoReunionMiembroServiceTest extends BaseServiceTest {

    @Autowired
    OrganoReunionMiembroService organoReunionMiembroService;

    private Reunion reunionConMiembroYPunto;

    private MiembroLocal miembro;

    private MiembroLocal suplente;

    private MiembroLocal delegadoVoto;

    private MiembroLocal miembroConSuplente;

    private MiembroLocal miembroConVotoDelegado;

    @Before
    public void setUp() throws RolesPersonaExternaException {
        super.setUp();

        miembro = new MiembroLocalBuilder().withPersonaId(90L).build(entityManager);
        suplente = new MiembroLocalBuilder().withPersonaId(100L).build(entityManager);
        delegadoVoto = new MiembroLocalBuilder().withPersonaId(110L).build(entityManager);
        miembroConSuplente = new MiembroLocalBuilder().withEmail("miembroConSuplente@4tic.com").withPersonaId(120L).build(entityManager);
        miembroConVotoDelegado = new MiembroLocalBuilder().withEmail("miembroConVOtoDelegado@4tic.com").withNombre("MiembroConVotoDelegado").withPersonaId(130L).build(entityManager);

        OrganoLocal organoLocal = new OrganoBuilder().withTipoOrgano(tipoOrgano).withMiembro(miembro).build(entityManager);
        reunionConMiembroYPunto = new ReunionBuilder().withAsunto("Reunion Con Miembro").withOrgano(organoLocal).build(entityManager);

        new OrganoReunionBuilder(reunionConMiembroYPunto, organoLocal)
                .withMiembroConSuplente(miembroConSuplente,false, suplente)
                .withMiembroConDelegadoVoto(miembroConVotoDelegado,false,delegadoVoto).build(entityManager);

        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDiaBuilder(reunionConMiembroYPunto,10L).withTitulo("punto 1").build(entityManager);
              Set puntosOrdenDia = new HashSet();
               puntosOrdenDia.add(puntoOrdenDia);

               new VotoPublicoBuilder(reunionConMiembroYPunto.getId().toString(),puntoOrdenDia.getId().toString(),delegadoVoto.getPersonaId().toString(),
                   VoteType.FAVOR.name())
                       .withNombre(delegadoVoto.getNombre())
                       .withVotoEnNombreDe(miembroConVotoDelegado.getNombre())
                       .withVotoEnNombreDePersonaId(miembroConVotoDelegado.getPersonaId().toString())
                       .build(entityManager);

        entityManager.flush();
        entityManager.clear();
    }
    @Test
    public void ShouldGetThePrincipalMemberWhenConnectedUserIsSuplenteOfThePrincipalMemberInReunion(){
        List<OrganoReunionMiembro> miembroQueEsSubstituido = organoReunionMiembroService.getOrganoReunionMiembrosPrincipalesFromSuplenteOrDelegadoVotoId(reunionConMiembroYPunto.getId(),suplente.getPersonaId());
        assertThat(miembroQueEsSubstituido.size() > 0, is(true));
        boolean encontrado = false;
        for (OrganoReunionMiembro organoReunionMiembro : miembroQueEsSubstituido) {
            if(organoReunionMiembro.getMiembroId().equals(miembroConSuplente.getPersonaId().toString())){
                encontrado = true;
            }
        }
        assertThat(encontrado,is(true));

    }

    @Test
    public void ShouldReturnPrincipalMemberWhenConnectedUserIsReceptorVotoOfAMemberInReunion(){
        List<OrganoReunionMiembro> miembroQueHadelegadoVoto = organoReunionMiembroService.getOrganoReunionMiembrosPrincipalesFromSuplenteOrDelegadoVotoId(reunionConMiembroYPunto.getId(),delegadoVoto.getPersonaId());
        assertThat(miembroQueHadelegadoVoto.size() > 0, is(true));
        boolean encontrado = false;
        for (OrganoReunionMiembro organoReunionMiembro : miembroQueHadelegadoVoto) {
            if(organoReunionMiembro.getMiembroId().equals(miembroConVotoDelegado.getPersonaId().toString())){
                encontrado = true;
            }
        }
        assertThat(encontrado,is(true));

    }
}
