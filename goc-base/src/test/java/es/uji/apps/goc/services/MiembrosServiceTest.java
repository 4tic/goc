package es.uji.apps.goc.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.goc.dao.MiembroDAO;
import es.uji.apps.goc.exceptions.MiembroExisteException;
import es.uji.apps.goc.exceptions.MiembroNoDisponibleException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.Miembro;

@RunWith(SpringJUnit4ClassRunner.class)
public class MiembrosServiceTest extends BaseServiceTest {
    @Autowired
    MiembroService miembroService;

    @Autowired
    MiembroDAO miembroDAO;

    Miembro presidente;

    @Before
    public void setUp() throws RolesPersonaExternaException {
        super.setUp();
        presidente = miembroDAO.getMiembroById(miembroPresideVotacion.getId());
    }

    @Test
    public void shouldGetSameOrdenNullWhenNotUpdateOrden() throws MiembroNoDisponibleException {
        Miembro miembroUpdated = miembroService.updateMiembro(presidente);
        Assert.assertEquals(presidente.getOrden(), miembroUpdated.getOrden());
    }

    @Test
    public void shouldGetOrden1WhenOrganoOnlyHasOneMember() throws MiembroNoDisponibleException {
        presidente.setOrden(444L);
        Miembro miembroUpdated = miembroService.updateMiembro(presidente);
        Assert.assertTrue(miembroUpdated.getOrden().equals(1L));
    }

    @Test
    public void shouldGetOrdenNullWhenOrganoNoHasOrdenDefinedAndAddNewOne()
        throws MiembroExisteException {
        Miembro miembro = new Miembro(null, "test", "test@test.com", presidente.getOrgano(), presidente.getCargo());
        miembro.setPersonaId(1L);
        Miembro miembroAdded = miembroService.addMiembro(miembro, null);
        Assert.assertNull(miembroAdded.getOrden());
        Assert.assertNull(miembroDAO.getMiembroById(presidente.getId()).getOrden());
    }

    @Test
    public void shouldGetOrden2WhenAddNewOneWithOrden2()
        throws MiembroExisteException {
        Miembro miembro = new Miembro(null, "test", "test@test.com", presidente.getOrgano(), presidente.getCargo());
        miembro.setPersonaId(1L);
        miembro.setOrden(2L);
        Miembro miembroAdded = miembroService.addMiembro(miembro, null);
        Assert.assertTrue(miembroAdded.getOrden().equals(2L));
        Assert.assertTrue(miembroDAO.getMiembroById(presidente.getId()).getOrden().equals(1L));
    }

    @Test
    public void shouldGetOrden2WhenAddNewOneWithOrden100()
        throws MiembroExisteException {
        Miembro miembro = new Miembro(null, "test", "test@test.com", presidente.getOrgano(), presidente.getCargo());
        miembro.setPersonaId(1L);
        miembro.setOrden(100L);
        Miembro miembroAdded = miembroService.addMiembro(miembro, null);
        Assert.assertTrue(miembroAdded.getOrden().equals(2L));
        Assert.assertTrue(miembroDAO.getMiembroById(presidente.getId()).getOrden().equals(1L));
    }

    @Test
    public void shouldGetOrden1WhenAddNewOneWithOrden1()
        throws MiembroExisteException {
        Miembro miembro = new Miembro(null, "test", "test@test.com", presidente.getOrgano(), presidente.getCargo());
        miembro.setPersonaId(1L);
        miembro.setOrden(1L);
        Miembro miembroAdded = miembroService.addMiembro(miembro, null);
        Assert.assertTrue(miembroAdded.getOrden().equals(1L));
        Assert.assertTrue(miembroDAO.getMiembroById(presidente.getId()).getOrden().equals(2L));
    }

    @Test
    public void shouldReordenarWhenDeleteMiembro() throws MiembroExisteException, MiembroNoDisponibleException {
        Miembro miembro = new Miembro(null, "test", "test@test.com", presidente.getOrgano(), presidente.getCargo());
        miembro.setPersonaId(1L);
        miembro.setOrden(1L);
        Miembro miembroAdded = miembroService.addMiembro(miembro, null);
        Assert.assertTrue(miembroDAO.getMiembroById(presidente.getId()).getOrden().equals(2L));

        miembroService.removeMiembroById(miembroAdded.getId(), null);

        Assert.assertTrue(miembroDAO.getMiembroById(presidente.getId()).getOrden().equals(1L));
    }
}