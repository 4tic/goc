package es.uji.apps.goc.notifications;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;

import org.junit.Test;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.TimeZone;

import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.services.ICalService;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class AvisosReunionTest {
    @Test
    public void testCreaICalCreatesIcalCorrectly() throws URISyntaxException {
        ReunionBuilder reunionBuilder = new ReunionBuilder();
        Reunion reunionDeTest =
            reunionBuilder.withId(11l).withAsunto("asunto de prueba").withCreadorEmail("miguel@email.com")
                .withCreadorNombre("miguel").withDuracion(150l).withFecha(new Date()).withUbicacion("en la uji")
                .build();

        ICalService iCalService = new ICalService();
        Calendar calendar = iCalService.creaICal(reunionDeTest);

        assertThat(calendar, is(notNullValue()));
        LocalDateTime startDate = LocalDateTime.ofInstant(reunionDeTest.getFecha().toInstant(), TimeZone.getDefault().toZoneId());
        DateTime start = new DateTime(Date.from(startDate.atZone(TimeZone.getDefault().toZoneId()).toInstant()));
        start.setTimeZone(null);
        assertEquals(start.toString(), calendar.getComponent("VEVENT").getProperty("DTSTART").getValue());
    }

    @Test
    public void testCreaIcalCreatesIcalCorrectlyWithoutOrganizer() throws URISyntaxException {
        ReunionBuilder reunionBuilder = new ReunionBuilder();
        Reunion reunionDeTest =
            reunionBuilder.withId(11l).withAsunto("asunto de prueba").withDuracion(150l).withFecha(new Date())
                .withUbicacion("en la uji").build();

        ICalService iCalService = new ICalService();
        Calendar calendar = iCalService.creaICal(reunionDeTest);

        assertThat(calendar, is(notNullValue()));
        LocalDateTime startDate = LocalDateTime.ofInstant(reunionDeTest.getFecha().toInstant(), TimeZone.getDefault().toZoneId());
        DateTime start = new DateTime(Date.from(startDate.atZone(TimeZone.getDefault().toZoneId()).toInstant()));
        start.setTimeZone(null);
        assertEquals(start.toString(), calendar.getComponent("VEVENT").getProperty("DTSTART").getValue());
    }

    @Test
    public void testCreaIcalCreatesIcalCorrectlyWithUTCTimeZone() throws URISyntaxException {
        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
        ReunionBuilder reunionBuilder = new ReunionBuilder();
        Reunion reunionDeTest =
            reunionBuilder.withId(11l).withAsunto("asunto de prueba").withDuracion(150l).withFecha(new Date())
                .withUbicacion("en la uji").build();

        ICalService iCalService = new ICalService();
        Calendar calendar = iCalService.creaICal(reunionDeTest);

        assertThat(calendar, is(notNullValue()));
        LocalDateTime startDate = LocalDateTime.ofInstant(reunionDeTest.getFecha().toInstant(), TimeZone.getDefault().toZoneId());
        DateTime start = new DateTime(Date.from(startDate.atZone(TimeZone.getDefault().toZoneId()).toInstant()));
        start.setTimeZone(null);
        assertEquals(start.toString(), calendar.getComponent("VEVENT").getProperty("DTSTART").getValue());
        assertFalse(calendar.getComponent("VEVENT").getProperty("DTSTART").getValue().endsWith("Z"));
    }

    @Test
    public void testCreaIcalCreatesIcalCorrectlyWithoutData() throws URISyntaxException {
        ReunionBuilder reunionBuilder = new ReunionBuilder();
        Reunion reunionDeTest = reunionBuilder.build();

        ICalService iCalService = new ICalService();
        Calendar calendar = iCalService.creaICal(reunionDeTest);

        assertThat(calendar, is(notNullValue()));
        assertEquals("", calendar.getComponent("VEVENT").getProperty("DTSTART").getValue());
    }
}
