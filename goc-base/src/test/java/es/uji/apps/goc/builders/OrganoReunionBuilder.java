package es.uji.apps.goc.builders;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionInvitado;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.Reunion;

public class OrganoReunionBuilder
{
    OrganoReunion organoReunion;
    Set<OrganoReunionMiembro> miembros = new LinkedHashSet<>();
    Set<OrganoReunionInvitado> invitados = new LinkedHashSet<>();

    public OrganoReunionBuilder(Reunion reunion)
    {
        this.organoReunion = new OrganoReunion();
        this.organoReunion.setReunion(reunion);
    }

    public OrganoReunionBuilder(Reunion reunion, OrganoLocal organoLocal) {
        this.organoReunion = new OrganoReunion();
        this.organoReunion.setReunion(reunion);
        this.withOrgano(organoLocal);
        this.withMiembros(organoLocal.getMiembros());
    }

    private void withMiembros(Set<MiembroLocal> miembros) {
        if (miembros != null) {
            for (MiembroLocal miembroLocal : miembros) {
                this.miembros.add(getMiembro(miembroLocal, true));
            }
        }
    }

    private OrganoReunionMiembro getMiembro(MiembroLocal miembroLocal , boolean asistencia) {
        OrganoReunionMiembro organoReunionMiembro = new OrganoReunionMiembro();
        organoReunionMiembro.setEmail(miembroLocal.getEmail());
        organoReunionMiembro.setAsistencia(asistencia);
        organoReunionMiembro.setMiembroId(miembroLocal.getPersonaId());
        organoReunionMiembro.setPresideVotacion(miembroLocal.isPresideVotacion());
        organoReunionMiembro.setVotante(miembroLocal.isVotante());

        return organoReunionMiembro;
    }

    private OrganoReunionMiembro getMiembro(String email , boolean asistencia) {
        OrganoReunionMiembro organoReunionMiembro = new OrganoReunionMiembro();
        organoReunionMiembro.setEmail(email);
        organoReunionMiembro.setAsistencia(asistencia);

        return organoReunionMiembro;
    }

    public OrganoReunionBuilder withMiembro(String email, boolean asistencia) {
        miembros.add(getMiembro(email, asistencia));

        return this;
    }

    public OrganoReunionBuilder withMiembro(MiembroLocal miembro, boolean asistencia) {
       this.miembros.add(getMiembro(miembro, asistencia));

        return this;
    }

    public OrganoReunionBuilder withPresideVotacionMiembro(MiembroLocal miembro, boolean asistencia) {
        OrganoReunionMiembro miembroPresideVotacion = getMiembro(miembro, asistencia);
        miembroPresideVotacion.setPresideVotacion(true);
        this.miembros.add(miembroPresideVotacion);

        return this;
    }

    public OrganoReunionBuilder withFirmante(String email, boolean asistencia) {
        OrganoReunionMiembro miembro = getMiembro(email, asistencia);
        miembro.setFirmante(true);

        miembros.add(miembro);

        return this;
    }

    public OrganoReunionBuilder withMiembroConSuplente(MiembroLocal miembroLocal, boolean asistencia, MiembroLocal suplente) {
        OrganoReunionMiembro miembro = getMiembro(miembroLocal, asistencia);
        miembro.setSuplenteId(suplente.getPersonaId());
        miembro.setSuplenteEmail(suplente.getEmail());

        miembros.add(miembro);

        return this;
    }

    public OrganoReunionBuilder withMiembroConSuplente(String email, boolean asistencia, MiembroLocal suplente) {
        OrganoReunionMiembro miembro = getMiembro(email, asistencia);
        miembro.setSuplenteId(suplente.getPersonaId());
        miembro.setSuplenteEmail(suplente.getEmail());

        miembros.add(miembro);

        return this;
    }

    public OrganoReunionBuilder withMiembroConDelegadoVoto(MiembroLocal miembroLocal, boolean asistencia, MiembroLocal delegadoVoto) {
        OrganoReunionMiembro miembro = getMiembro(miembroLocal, asistencia);
        miembro.setDelegadoVotoId(delegadoVoto.getPersonaId());
        miembro.setSuplenteEmail(delegadoVoto.getEmail());

        miembros.add(miembro);

        return this;
    }

    public OrganoReunionBuilder withFirmanteConSuplente(String email, boolean asistencia, MiembroLocal suplente) {
        OrganoReunionMiembro miembro = getMiembro(email, asistencia);
        miembro.setSuplenteId(suplente.getPersonaId());
        miembro.setSuplenteEmail(suplente.getEmail());
        miembro.setFirmante(true);

        miembros.add(miembro);

        return this;
    }

    public OrganoReunionBuilder withInvitado(String email) {
        OrganoReunionInvitado organoReunionInvitado = new OrganoReunionInvitado();
        organoReunionInvitado.setEmail(email);

        invitados.add(organoReunionInvitado);

        return this;
    }

    public OrganoReunionBuilder withInvitado(OrganoReunionInvitado organoReunionInvitado) {
        invitados.add(organoReunionInvitado);
        return this;
    }

    public OrganoReunionBuilder withOrgano(OrganoLocal organo)
    {
        this.organoReunion.setOrganoId(organo.getId().toString());
        this.organoReunion.setOrganoNombre(organo.getNombre());
        this.organoReunion.setTipoOrganoId(organo.getTipoOrgano().getId());
        this.organoReunion.setExterno(false);
        return this;
    }

    public OrganoReunion build(EntityManager entityManager)
    {
        entityManager.persist(this.organoReunion);

        for (OrganoReunionMiembro miembro : miembros) {
            miembro.setOrganoId(this.organoReunion.getOrganoId());
            miembro.setReunionId(this.organoReunion.getReunion().getId());
            miembro.setOrganoExterno(this.organoReunion.isExterno());
            miembro.setOrganoReunion(this.organoReunion);
            entityManager.persist(miembro);
        }

        for (OrganoReunionInvitado invitado : invitados) {
            invitado.setOrganoId(this.organoReunion.getOrganoId());
            invitado.setReunionId(this.organoReunion.getReunion().getId());
            invitado.setOrganoExterno(this.organoReunion.isExterno());
            invitado.setOrganoReunion(this.organoReunion);
            entityManager.persist(invitado);
        }

        this.organoReunion.setMiembros(miembros);
        this.organoReunion.setInvitados(invitados);
        entityManager.persist(this.organoReunion);

        return this.organoReunion;
    }
}
