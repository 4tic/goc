package es.uji.apps.goc.services;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import es.uji.apps.goc.builders.CargoBuilder;
import es.uji.apps.goc.builders.MiembroLocalBuilder;
import es.uji.apps.goc.builders.OrganoReunionBuilder;
import es.uji.apps.goc.builders.ReunionBuilder;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.Cargo;
import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionEditor;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.exceptions.InvalidAccessException;
import es.uji.apps.goc.exceptions.ReunionNoDisponibleException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ResponsableFirma;
import es.uji.apps.goc.model.Role;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReunionServiceTest extends BaseServiceTest {
    @Autowired
    ReunionService reunionService;

    @Autowired
    ReunionDAO reunionDAO;

    @Autowired
    PersonaService personaService;

    final String EMAIL_SUPLENTE = "suplente@4tic.com";

    @Before
    public void setUp() throws RolesPersonaExternaException {
        super.setUp();

        Mockito.when(personaService.getRolesFromPersonaId(Mockito.anyLong()))
            .thenReturn(Arrays.asList(Role.ADMIN.toString()));
        Mockito.when(personaService.isAdmin(Mockito.anyList())).thenCallRealMethod();
    }

    @Test(expected = FirmantesNecesariosException.class)
    public void checkToCloseShouldFailNoFirmantes()
        throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest)
            .withMiembro("goc@4tic.com", true).build(entityManager);

        reunionService.checkReunionToClose(reunionCompletada.getId(), CREADOR_ID);
    }

    @Test
    public void shouldCloseReunionWithFirmante() throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest)
            .withFirmante("goc@4tic.com", true).build(entityManager);

        reunionService.checkReunionToClose(reunionCompletada.getId(), CREADOR_ID);
    }

    @Test
    public void shouldCloseReunionWithFirmanteNoAsisteWithSuplente()
        throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        MiembroLocal suplente =
            new MiembroLocalBuilder().withEmail(EMAIL_SUPLENTE).withOrganoLocal(organoTest).withPersonaId(2L)
                .build(entityManager);

        new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest)
            .withFirmanteConSuplente("goc@4tic.com", true, suplente).build(entityManager);

        reunionService.checkReunionToClose(reunionCompletada.getId(), CREADOR_ID);
    }

    @Test
    public void shouldGetOneFirmante() {
        new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest)
            .withMiembro("goc_miembro@4tic.com", true)
            .withFirmante("goc_firmante@4tic.com", true).build(entityManager);

        ReunionTemplate reunionTemplateDesdeReunion =
            reunionService.getReunionTemplateDesdeReunion(reunionCompletada, CREADOR_ID, true, true);
        List<ResponsableFirma> firmantes = reunionService.getFirmantesConAsistenciaConfirmada(reunionTemplateDesdeReunion);

        assertThat(firmantes.size(), is(1));
        assertThat(firmantes.get(0).getEmail(), is("goc_firmante@4tic.com"));
    }

    @Test
    public void shouldGetSuplenteAsFirmante() {
        MiembroLocal suplente =
            new MiembroLocalBuilder().withEmail(EMAIL_SUPLENTE).withOrganoLocal(organoTest).withPersonaId(2L)
                .build(entityManager);

        new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest)
            .withMiembro("goc_miembro@4tic.com", true)
            .withFirmanteConSuplente("goc_firmante@4tic.com", true, suplente).build(entityManager);

        ReunionTemplate reunionTemplateDesdeReunion =
            reunionService.getReunionTemplateDesdeReunion(reunionCompletada, CREADOR_ID, true, true);
        List<ResponsableFirma> firmantes = reunionService.getFirmantesConAsistenciaConfirmada(reunionTemplateDesdeReunion);

        assertThat(firmantes.size(), is(1));
        assertThat(firmantes.get(0).getEmail(), is(EMAIL_SUPLENTE));
    }

    @Test
    public void shouldGetCargoSuplente() {
        Cargo cargo = new CargoBuilder().withCodigo("CA").withNombre("cargo").build(entityManager);

        MiembroLocal suplente =
            new MiembroLocalBuilder().withEmail(EMAIL_SUPLENTE).withOrganoLocal(organoTest).withPersonaId(2L).withCargo(cargo)
                .build(entityManager);

        new OrganoReunionBuilder(reunionCompletada).withOrgano(organoTest)
            .withMiembro("goc_miembro@4tic.com", true)
            .withFirmanteConSuplente("goc_firmante@4tic.com", true, suplente).build(entityManager);

        ReunionTemplate reunionTemplateDesdeReunion =
            reunionService.getReunionTemplateDesdeReunion(reunionCompletada, CREADOR_ID, true, true);
        List<ResponsableFirma> firmantes = reunionService.getFirmantesConAsistenciaConfirmada(reunionTemplateDesdeReunion);

        assertThat(firmantes.size(), is(1));
        assertThat(firmantes.get(0).getEmail(), is(EMAIL_SUPLENTE));
        assertThat(firmantes.get(0).getCargo(), is(cargo.getNombre()));
    }

    @Test
    public void shouldGetReunionSiAccesible()
        throws RolesPersonaExternaException, ReunionNoDisponibleException, InvalidAccessException {
        Reunion reunion = reunionService.getReunionSiEsAccesible(reunionCompletada.getId(), CREADOR_ID);
        Assert.assertNotNull(reunion);
    }

    @Test(expected = InvalidAccessException.class)
    public void shouldNotGetReunionSiAccesible()
        throws RolesPersonaExternaException, ReunionNoDisponibleException, InvalidAccessException {
        Mockito.when(personaService.getRolesFromPersonaId(Mockito.anyLong()))
            .thenReturn(Arrays.asList());
        Reunion reunion = reunionService.getReunionSiEsAccesible(reunionCompletada.getId(), 1L);
        Assert.assertNull(reunion);
    }

    @Test
    public void shouldGetReunionesCompletadasByAdmin() {
        List<ReunionEditor> reunionesCompletadasByAdmin = reunionService.getReunionesByAdmin(true, true, null, null, null);

        Assert.assertNotNull(reunionesCompletadasByAdmin);
        Assert.assertEquals(3, reunionesCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByAdminFiltradasPorOrgano() {
        List<ReunionEditor> reunionesCompletadasByAdmin = reunionService.getReunionesByAdmin(true, true, null, organoAdmin.getId().toString(), null);

        Assert.assertNotNull(reunionesCompletadasByAdmin);
        Assert.assertEquals(2, reunionesCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByAdminFiltradasPorOrganoNoExistente() {
        List<ReunionEditor> reunionesCompletadasByAdmin = reunionService.getReunionesByAdmin(true, true, null, organoSinReuniones.getId().toString(), null);

        Assert.assertNotNull(reunionesCompletadasByAdmin);
        Assert.assertEquals(0, reunionesCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByAdminFiltradasPorTipoOrgano() {
        List<ReunionEditor> reunionesCompletadasByAdmin = reunionService.getReunionesByAdmin(true, true, organoAdmin.getTipoOrgano().getId(), null, null);

        Assert.assertNotNull(reunionesCompletadasByAdmin);
        Assert.assertEquals(2, reunionesCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByAdminFiltradasPorTipoOrganoNoExistente() {
        List<ReunionEditor> reunionesCompletadasByAdmin = reunionService.getReunionesByAdmin(true, true, 2L, null, null);

        Assert.assertNotNull(reunionesCompletadasByAdmin);
        Assert.assertEquals(0, reunionesCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByAdminFiltradasPorExterno() {
        List<ReunionEditor> reunionesCompletadasByAdmin = reunionService.getReunionesByAdmin(true, true, null, null, true);

        Assert.assertNotNull(reunionesCompletadasByAdmin);
        Assert.assertEquals(0, reunionesCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByAdminFiltradasPorNoExterno() {
        List<ReunionEditor> reunionesCompletadasByAdmin = reunionService.getReunionesByAdmin(true, true, null, null, false);

        Assert.assertNotNull(reunionesCompletadasByAdmin);
        Assert.assertEquals(3, reunionesCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasByAdmin() {
        List<ReunionEditor> reunionesNoCompletadasByAdmin = reunionService.getReunionesByAdmin(false, true, null, null, null);

        Assert.assertNotNull(reunionesNoCompletadasByAdmin);
        Assert.assertEquals(4, reunionesNoCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasFuturasByAdmin() {
        List<ReunionEditor> reunionesNoCompletadasFuturasByAdmin = reunionService.getReunionesByAdmin(false, false, null, null, null);

        Assert.assertNotNull(reunionesNoCompletadasFuturasByAdmin);
        Assert.assertEquals(1, reunionesNoCompletadasFuturasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasByAdminFiltradasPorOrgano() {
        List<ReunionEditor> reunionesNoCompletadasByAdmin = reunionService.getReunionesByAdmin(false, true, null, organoAdmin.getId().toString(), null);

        Assert.assertNotNull(reunionesNoCompletadasByAdmin);
        Assert.assertEquals(2, reunionesNoCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasByAdminFiltradasPorTipoOrgano() {
        List<ReunionEditor> reunionesNoCompletadasByAdmin = reunionService.getReunionesByAdmin(false, true, organoAdmin.getTipoOrgano().getId(), null, null);

        Assert.assertNotNull(reunionesNoCompletadasByAdmin);
        Assert.assertEquals(2, reunionesNoCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasByAdminFiltradasPorNoExterno() {
        List<ReunionEditor> reunionesNoCompletadasByAdmin = reunionService.getReunionesByAdmin(false, true, null, null, false);

        Assert.assertNotNull(reunionesNoCompletadasByAdmin);
        Assert.assertEquals(2, reunionesNoCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasByAdminFiltradasPorExterno() {
        List<ReunionEditor> reunionesNoCompletadasByAdmin = reunionService.getReunionesByAdmin(false, true, null, null, true);

        Assert.assertNotNull(reunionesNoCompletadasByAdmin);
        Assert.assertEquals(0, reunionesNoCompletadasByAdmin.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditor() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, null, null, null, CREADOR_ID);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(3, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditorFiltradasPorOrgano() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, organoAdmin.getId().toString(), null, null, CREADOR_ID);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(2, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditorFiltradasPorOrganoNoExistente() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, organoTest.getId().toString(), null, null, CREADOR_ID);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(0, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditorFiltradasPorTipoOrgano() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, null, organoAdmin.getTipoOrgano().getId(), null, CREADOR_ID);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(2, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditorFiltradasPorTipoOrganoNoExistente() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, null, 2L, null, CREADOR_ID);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(0, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditorFiltradasPorExterno() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, null, null, true, CREADOR_ID);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(0, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditorFiltradasPorNoExterno() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, null, null, false, CREADOR_ID);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(3, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasByEditor() {
        List<ReunionEditor> reunionesNoCompletadasByEditor = reunionService.getReunionesByEditorId(false, true, null, null, null, CREADOR_ID);

        Assert.assertNotNull(reunionesNoCompletadasByEditor);
        Assert.assertEquals(3, reunionesNoCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesCompletadasByEditorUsuarioNoCreador() {
        List<ReunionEditor> reunionesCompletadasByEditor = reunionService.getReunionesByEditorId(true, true, null, null, null, 1L);

        Assert.assertNotNull(reunionesCompletadasByEditor);
        Assert.assertEquals(0, reunionesCompletadasByEditor.size());
    }

    @Test
    public void shouldGetReunionesNoCompletadasByEditorUsuarioNoCreador() {
        List<ReunionEditor> reunionesNoCompletadasByEditor = reunionService.getReunionesByEditorId(false,true,  null, null, null, 1L);

        Assert.assertNotNull(reunionesNoCompletadasByEditor);
        Assert.assertEquals(0, reunionesNoCompletadasByEditor.size());
    }

    @Test
    public void shouldReunionNotCompletadaAfterDuplicate() {
        Reunion reunionCompletada = new ReunionBuilder().withAsunto("Reunión 1").withCreadorId(CREADOR_ID)
                .withAvisoPrimeraReunion(true).completada(true).withAcuerdos("Acuerdos").build(entityManager);
        Persona convocante = new Persona(CREADOR_ID, "Nicolas", "nicolas@example.com");
        Reunion reunionDuplicada = reunionService.addReunion(reunionCompletada, CREADOR_ID, convocante);

        assertThat(reunionDuplicada.getAsunto(), Is.is("Reunión 1"));
        assertThat(isCompletada(reunionDuplicada), is(false));
        assertThat(reunionDuplicada.getFechaCompletada(), is(nullValue()));
    }

    private boolean isCompletada(Reunion reunionDuplicada) {
        return (reunionDuplicada.isCompletada() || reunionDuplicada.getAcuerdos() != null || reunionDuplicada.getFechaCompletada() != null ||
                reunionDuplicada.getMiembroResponsableActa() != null || reunionDuplicada.getUrlActa() != null || reunionDuplicada.getUrlActa() != null ||
                reunionDuplicada.getAvisoPrimeraReunion() || reunionDuplicada.isNotificada() ||
                reunionDuplicada.getAvisoPrimeraReunionFecha() != null || reunionDuplicada.getAvisoPrimeraReunionUser() != null || reunionDuplicada.getAvisoPrimeraReunionUserEmail() != null);
    }

    @Test
    public void shoulGetTrueWhenPassingAutorizadoPersonaId(){
        boolean autorizadoEnReunionByPuntoOrdenDiaId = reunionService.isAutorizadoEnReunionByPuntoOrdenDiaId(puntoEnReunionCompletada.getId(), autorizado.getPersonaId());
        assertThat(autorizadoEnReunionByPuntoOrdenDiaId,is(true));
    }

    @Test
    public void shoulGetFalseWhenPassingWrongAutorizadoPersonaId(){
        boolean autorizadoEnReunionByPuntoOrdenDiaId = reunionService.isAutorizadoEnReunionByPuntoOrdenDiaId(puntoEnReunionCompletada.getId(), autorizado.getPersonaId() + 1);
        assertThat(autorizadoEnReunionByPuntoOrdenDiaId,is(false));
    }

    @Test
    public void shouldRetunTrueIfReunionHasVotacionPublica()
    {
        Boolean votacionPublica = reunionDAO.hasVotacion(reunionConvocadaYCompletada.getId());
        assertTrue(votacionPublica);
    }

    @Test
    public void shouldRetunTrueIfReunionHasVotacionPrivada()
    {
        Boolean votacionPublica = reunionDAO.hasVotacion(reunionNoConvocada.getId());
        assertTrue(votacionPublica);
    }
}
