package es.uji.apps.goc.builders;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.MiembroLocal;
import es.uji.apps.goc.dto.OrganoInvitado;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.TipoOrganoLocal;

public class OrganoBuilder
{
    OrganoLocal organoLocal;
    Set<OrganoInvitado> invitados = new HashSet<>();

    public OrganoBuilder()
    {
        this.organoLocal = new OrganoLocal();
    }

    public OrganoBuilder withNombre(String nombre)
    {
        this.organoLocal.setNombre(nombre);
        return this;
    }

    public OrganoBuilder withTipoOrgano(TipoOrganoLocal tipoOrgano)
    {
        this.organoLocal.setTipoOrgano(tipoOrgano);
        return this;
    }

    public OrganoBuilder withCreadorId(Long creadorId)
    {
        this.organoLocal.setCreadorId(creadorId);
        return this;
    }

    public OrganoLocal build(EntityManager entityManager)
    {
        this.organoLocal.setFechaCreacion(new Date());
        entityManager.persist(organoLocal);

        for (OrganoInvitado organoInvitado : invitados) {
            organoInvitado.setOrganoId(this.organoLocal.getId().toString());
            entityManager.persist(organoInvitado);
        }
        this.organoLocal.setInvitados(invitados);
        entityManager.persist(this.organoLocal);

        if (this.organoLocal.getMiembros() != null) {
            for (MiembroLocal miembro : this.organoLocal.getMiembros()) {
                miembro.setOrgano(this.organoLocal);
                entityManager.persist(miembro);
            }
        }

        return organoLocal;
    }

    public OrganoBuilder withMiembro(MiembroLocal miembroLocal) {
        HashSet<MiembroLocal> miembros =  new HashSet<>();
        if(this.organoLocal.getMiembros() == null) {
            miembros.add(miembroLocal);
            this.organoLocal.setMiembros(miembros);
        } else {
            miembros.add(miembroLocal);
            this.organoLocal.getMiembros().addAll(miembros);
        }
        return this;
    }

    public OrganoBuilder withInvitadoWithPersonaId(String personaId) {
        OrganoInvitado organoInvitado = new OrganoInvitado();
        organoInvitado.setPersonaId(new Long(personaId));
        invitados.add(organoInvitado);
        return this;
    }
}
