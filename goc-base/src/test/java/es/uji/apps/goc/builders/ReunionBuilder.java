package es.uji.apps.goc.builders;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.OrganoInvitado;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionInvitado;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionInvitado;

public class ReunionBuilder
{
    Reunion reunion;
    Set<ReunionInvitado> invitados = new HashSet<>();
    private List<OrganoReunionBuilder> organosReunionBuilders = new ArrayList<>();

    public ReunionBuilder()
    {
        this.reunion = new Reunion();
        this.reunion.setFecha(new Date());
        this.reunion.setCompletada(false);
        this.reunion.setFechaCompletada(null);
    }

    public ReunionBuilder(Set<OrganoReunion> organosReunion)
    {
        this.reunion = new Reunion();
        this.reunion.setFecha(new Date());
        this.reunion.setCompletada(false);
        this.reunion.setFechaCompletada(null);
        this.reunion.setReunionOrganos(organosReunion);
    }

    public ReunionBuilder withId(Long id)
    {
        this.reunion.setId(id);
        return this;
    }

    public ReunionBuilder withAsunto(String asunto)
    {
        this.reunion.setAsunto(asunto);
        return this;
    }

    public ReunionBuilder withFecha(Date fecha)
    {
        this.reunion.setFecha(fecha);
        return this;
    }

    public ReunionBuilder withCreadorId(Long creadorId)
    {
        this.reunion.setCreadorId(creadorId);
        return this;
    }

    public ReunionBuilder withAvisoPrimeraReunion(boolean avisoPrimeraReunion)
    {
        this.reunion.setAvisoPrimeraReunion(avisoPrimeraReunion);
        return this;
    }

    public ReunionBuilder withDuracion(Long duracionMinutos)
    {
        this.reunion.setDuracion(duracionMinutos);
        return this;
    }


    public ReunionBuilder withUbicacion(String ubicacion)
    {
        this.reunion.setUbicacion(ubicacion);
        return this;
    }

    public ReunionBuilder withCreadorNombre(String creadorNombre)
    {
        this.reunion.setCreadorNombre(creadorNombre);
        return this;
    }

    public ReunionBuilder withCreadorEmail(String creadorEmail)
    {
        this.reunion.setCreadorEmail(creadorEmail);
        return this;
    }

    public ReunionBuilder withInvitado(String email)
    {
        ReunionInvitado reunionInvitado = new ReunionInvitado();
        reunionInvitado.setPersonaEmail(email);

        invitados.add(reunionInvitado);

        return this;
    }

    public ReunionBuilder withPuntosOrdenDia(Set<PuntoOrdenDia> puntosOrdenDia)
    {
        this.reunion.setReunionPuntosOrdenDia(puntosOrdenDia);
        return this;
    }

    public ReunionBuilder withAcuerdos(String acuerdos)
    {
        this.reunion.setAcuerdos(acuerdos);
        return this;
    }

    public ReunionBuilder withURLGrabacion(String url)
    {
        this.reunion.setUrlGrabacion(url);
        return this;
    }

    public ReunionBuilder completada(Boolean completada)
    {
        this.reunion.setCompletada(completada);
        if(completada){
            this.reunion.setFechaCompletada(new Date());
        }

        return this;
    }

    public ReunionBuilder publica()
    {
        this.reunion.setPublica(true);

        return this;
    }

    public ReunionBuilder withHasVotacion(Boolean hasVotacion){
        this.reunion.setHasVotacion(hasVotacion);
        return this;
    }

    public Reunion build()
    {
        return reunion;
    }

    public ReunionBuilder withOrgano(OrganoLocal organoAdmin)
    {
        OrganoReunionBuilder organoReunionBuilder = new OrganoReunionBuilder(this.reunion, organoAdmin);

        if (organoAdmin.getInvitados() != null) {
            for (OrganoInvitado organoInvitado : organoAdmin.getInvitados()) {
                OrganoReunionInvitado organoReunionInvitado = new OrganoReunionInvitado();
                organoReunionInvitado.setPersonaId(organoInvitado.getPersonaId());
                organoReunionBuilder.withInvitado(organoReunionInvitado);
            }
        }
        organosReunionBuilders.add(organoReunionBuilder);
        return this;
    }

    public ReunionBuilder isTelematica(Boolean telematica)
    {
        this.reunion.setTelematica(telematica);

        return this;
    }

    public ReunionBuilder convocada() {
        this.reunion.setAvisoPrimeraReunion(true);
        return this;
    }

    public Reunion build(EntityManager entityManager)
    {
        this.reunion.setFechaCreacion(new Date());
        entityManager.persist(reunion);

        for (ReunionInvitado invitado : invitados) {
            invitado.setReunion(reunion);
            entityManager.persist(invitado);
        }

        Set<OrganoReunion> organosReunionSet = new HashSet<>();
        for (OrganoReunionBuilder organoReunionBuilder : organosReunionBuilders) {
            organosReunionSet.add(organoReunionBuilder.build(entityManager));
        }

        this.reunion.setReunionOrganos(organosReunionSet);
        this.reunion.setReunionInvitados(invitados);

        entityManager.persist(this.reunion);
        return reunion;
    }
}
