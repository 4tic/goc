package es.uji.apps.goc.builders;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.Reunion;

public class PuntoOrdenDiaBuilder
{
    Set<PuntoOrdenDia> subpuntos = new LinkedHashSet<>();
    PuntoOrdenDia punto;
    Set<PuntoOrdenDiaDocumento> documentosPunto = new LinkedHashSet<>();

    public PuntoOrdenDiaBuilder(Reunion reunion, Long orden)
    {
        this.punto = new PuntoOrdenDia();
        this.punto.setReunion(reunion);
        this.punto.setOrden(orden);
        if (reunion.getHasVotacion() != null && reunion.getHasVotacion()) {
            this.punto.setVotoPublico(true);
        }
    }
    public PuntoOrdenDiaBuilder()
    {
        this.punto = new PuntoOrdenDia();
    }

    public PuntoOrdenDiaBuilder withTitulo(String titulo) {
        this.punto.setTitulo(titulo);
        return this;
    }

    public PuntoOrdenDiaBuilder withSubpunto(PuntoOrdenDia punto) {
        this.subpuntos.add(punto);

        return this;
    }

    public PuntoOrdenDiaBuilder publico() {
        this.punto.setPublico(true);

        return this;
    }

    public PuntoOrdenDiaBuilder withDocumentos(PuntoOrdenDiaDocumento puntoOrdenDiaDocumentos){
        this.documentosPunto.add(puntoOrdenDiaDocumentos);
        return this;
    }

    public PuntoOrdenDiaBuilder withVotoPublico(Boolean votoPublico){
        this.punto.setVotoPublico(votoPublico);
        return this;
    }

    public PuntoOrdenDiaBuilder withFechaAperturaVotacion(Date fechaAperturaVotacion){
        this.punto.setFechaAperturaVotacion(fechaAperturaVotacion);
        return this;
    }
    public PuntoOrdenDiaBuilder withAcuerdos() {

        this.punto.setAcuerdos("Acuerdos");
        return this;
    }

    public PuntoOrdenDiaBuilder withAcuerdosAlt(){
        this.punto.setAcuerdosAlternativos("AcuerdosAlternativos");
        return this;
    }

    public PuntoOrdenDiaBuilder  withDeliberaciones(){
        punto.setDeliberaciones("Deliberaciones");
        return this;
    }

    public PuntoOrdenDiaBuilder withdeliberacionesAltenrativas(){
        this.punto.setDeliberacionesAlternativas("withdeliberacionesAltenrativas");
        return this;
    }
    public  PuntoOrdenDiaBuilder withUrlcta(){
        this.punto.setUrlActa("withUrlacta");
        return this;
    }

    public PuntoOrdenDiaBuilder withUrlActaAlternativa(){
        this.punto.setUrlActaAlternativa("withUrlActaAlternativa");
        return this;
    }

    public PuntoOrdenDiaBuilder withUrlActaAnterior(){
        this.punto.setUrlActaAnterior("withUrlActaAnterior");
        return this;
    }

    public PuntoOrdenDiaBuilder withUrlActaAnteriorAlternativa(){
        this.punto.setUrlActaAnteriorAlt("withUrlActaAnteriorAlternativa");
        return this;
    }

    public PuntoOrdenDia build() {
        return punto;
    }

    public PuntoOrdenDia build(EntityManager entityManager){
        this.punto.setPuntosInferiores(subpuntos);
        entityManager.persist(punto);

        for (PuntoOrdenDia subpunto : subpuntos) {
            subpunto.setReunion(this.punto.getReunion());
            subpunto.setPuntoSuperior(punto);
            entityManager.persist(subpunto);
        }
        for (PuntoOrdenDiaDocumento documento : documentosPunto){
            documento.setPuntoOrdenDia(punto);
        }
        return punto;
    }
}


