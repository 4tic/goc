package es.uji.apps.goc.builders;

import java.util.Date;

import javax.persistence.EntityManager;

import es.uji.apps.goc.dto.VotantePrivadoDTO;
import es.uji.apps.goc.dto.VotoPrivadoDTO;

public class VotoPrivadoBuilder
{
    VotoPrivadoDTO votoPrivadoDTO;

    public VotoPrivadoBuilder(String reunionId, String puntoId, String voto)
    {
        this.votoPrivadoDTO = new VotoPrivadoDTO();
        this.votoPrivadoDTO.setReunionId(reunionId);
        this.votoPrivadoDTO.setPuntoOrdenDiaId(puntoId);
        this.votoPrivadoDTO.setVoto(voto);
    }

    public VotoPrivadoDTO build(EntityManager entityManager){
        VotantePrivadoDTO votantePrivadoDTO = new VotantePrivadoDTO();
        votantePrivadoDTO.setReunionId(votoPrivadoDTO.getReunionId());
        votantePrivadoDTO.setPuntoOrdenDiaId(votoPrivadoDTO.getPuntoOrdenDiaId());
        votantePrivadoDTO.setFechaVoto(new Date());

        entityManager.persist(votoPrivadoDTO);
        entityManager.persist(votantePrivadoDTO);

        return votoPrivadoDTO;
    }
}
