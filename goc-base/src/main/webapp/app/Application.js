Ext.Loader.setPath('Ext.ux', staticsurl + '/js/extjs/ext-6.2.1/packages/ux/classic/src');
Ext.Loader.setPath('Ext.ux.uji', 'uji');
Ext.Loader.setPath('goc', 'app');

if(appI18N != null)
{
    Ext.require('Ext.ux.uji.data.Store');
    Ext.require('Ext.ux.TabCloseMenu');
    Ext.require('Ext.ux.form.SearchField');
    Ext.require('Ext.ux.uji.ApplicationViewport');
    Ext.require('goc.view.tipoOrgano.Main');
    Ext.require('goc.view.organo.Main');
    Ext.require('goc.view.cargo.Main');
    Ext.require('goc.view.miembro.Main');
    Ext.require('goc.view.reunion.Main');
    Ext.require('goc.view.historicoReunion.Main');
    Ext.require('goc.view.common.LookupWindowPersonas');
    Ext.require('goc.view.descriptor.Main');
    Ext.require('goc.view.oficio.Main');
    Ext.require('goc.view.personas.Main');

    Ext.ariaWarn = Ext.emptyFn;

    Ext.define("Ext.locale.es.grid.RowEditor", {
        override : 'Ext.grid.RowEditor',
        errorsText : appI18N.common.errors
    });

    Ext.define("Ext.locale.ca.picker.Date", {
        override: "Ext.picker.Date",
        startDay: 1,
        getDayInitial: function (value) {
            Ext.Date.shortDayNames = ["Du", "Dl", "Dm", "Dx", "Dj", "Dv", "Ds"];
            return Ext.Date.shortDayNames[Ext.Date.dayNames.indexOf(value)];
        }
    });

    Ext.define("Ext.locale.ca.form.field.Date", {
        override: "Ext.form.field.Date",
        formatText: "Format esperat dd/mm/yyyy"
    });

    Ext.define('Overrides.form.field.Base',
        {
            override: 'Ext.form.field.Base',

            getLabelableRenderData: function () {
                var me = this, data = me.callParent(), labelSeparator = me.labelSeparator;

                if (!me.allowBlank) {
                    data.labelSeparator = labelSeparator + ' <span style="color:red">*</span>';
                }

                return data;
            }
        });

    Ext.define('Overrides.form.field.Text',
        {
            override: 'Ext.form.field.Text',

            onFocus: function(e) {
                var me = this,
                    len;

                me.callParent([e]);

                // This handler may be called when the focus has already shifted to another element;
                // calling inputEl.select() will forcibly focus again it which in turn might set up
                // a nasty circular race condition if focusEl !== inputEl.
                Ext.asap(function() {
                    // This ensures the carret will be at the end of the input element
                    // while tabbing between editors.
                    if (!me.destroyed && document.activeElement === me.inputEl.dom) {
                        len = me.inputEl.dom.value.length;
                        me.selectText(me.selectOnFocus ? 0 : len, len);
                    }
                });

                if (me.emptyText) {
                    me.autoSize();
                }

                me.addCls(me.fieldFocusCls);
                me.triggerWrap.addCls(me.triggerWrapFocusCls);
                me.inputWrap.addCls(me.inputWrapFocusCls);
                me.invokeTriggers('onFieldFocus', [e]);
            }
        }
    );

    Ext.application(
        {
            extend: 'Ext.app.Application',

            name: 'goc',
            title: 'goc',

            // requires : [ 'goc.model.Organo' ],

            launch: function () {
                var viewport = Ext.create('Ext.ux.uji.ApplicationViewport',
                    {
                        codigoAplicacion: 'GOC',
                        tituloAplicacion: tituloAplicacion? tituloAplicacion: appI18N.header.titulo,
                        dashboard: false
                    });

                document.getElementById('landing-loading').style.display = 'none';
            }
        });
}