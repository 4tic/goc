Ext.define('goc.store.Oficios', {
    extend: 'Ext.data.Store',
    alias: 'store.oficios',
    model: 'goc.model.Oficio',

    proxy: {
        type: 'rest',
        url: appContext + '/rest/oficios',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    },

    autoLoad: true
});
