Ext.define('goc.store.PuntosOrdenDiaTree', {
    extend: 'Ext.data.TreeStore',
    alias: 'store.puntosOrdenDiaTree',
    fields: ['titulo', 'tituloAlternativo', 'publico', 'numeroDocumentos', 'numeroAcuerdos', 'urlActaAnterior', 'urlActaAnteriorAlt', {
        name: 'votoPublico',
        type: 'boolean'
    },],
    root: {
        expanded: true
    },

    proxy: {
        type: 'rest',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        },
        appendId: false
    },
    autoLoad: false
});
