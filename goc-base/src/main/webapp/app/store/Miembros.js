Ext.define('goc.store.Miembros', {
    extend: 'Ext.data.Store',
    alias: 'store.miembros',
    model: 'goc.model.Miembro',

    groupField: 'grupo',

    remoteSort: true,

    proxy: {
        type: 'rest',
        url: appContext + '/rest/miembros',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    }
});
