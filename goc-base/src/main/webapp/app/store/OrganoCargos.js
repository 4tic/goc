Ext.define('goc.store.OrganoCargos', {
    extend: 'Ext.data.Store',
    alias: 'store.organoCargos',
    model: 'goc.model.OrganoCargo',

    proxy: {
        type: 'rest',
        url: appContext + '/rest/organos/cargos',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    },

    autoLoad: false,
    autoSync: false
});
