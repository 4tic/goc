Ext.define('goc.store.ReunionExternos', {
    extend: 'Ext.data.Store',
    alias: 'store.reunionExternos',
    model: 'goc.model.Persona',

    proxy: {
        type: 'rest',
        url: appContext + '/rest/personas',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    }
});
