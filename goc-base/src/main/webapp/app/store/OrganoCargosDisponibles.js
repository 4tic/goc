Ext.define('goc.store.OrganoCargosDisponibles', {
    extend: 'Ext.data.Store',
    alias: 'store.organoCargosDisponibles',
    model: 'goc.model.Cargo',

    proxy: {
        type: 'rest',
        url: appContext + '/rest/organos/cargos',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    },

    autoLoad: false,
    autoSync: false
});
