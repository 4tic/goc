Ext.define('goc.store.ConvocantesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.convocantesStore',
    model: 'goc.model.ConvocanteModel',

    proxy: {
        type: 'rest',
        url: appContext + '/rest/reuniones/convocantes',
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json',
            writeAllFields : true
        }
    }
});
