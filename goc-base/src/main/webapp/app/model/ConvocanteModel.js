Ext.define('goc.model.ConvocanteModel', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    fields: [
        { name: 'nombre', type: 'string' },
        { name: 'email', type: 'string' }
    ]
});
