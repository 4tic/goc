Ext.define('goc.model.Miembro', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'number' },
        { name: 'orden', type: 'string' },
        { name: 'personaId', type: 'number' },
        { name: 'nombre', type: 'string' },
        { name: 'email', type: 'string' },
        { name: 'externo', type: 'boolean' },
        { name: 'organoId', type: 'number' },
        { name: 'cargoId', type: 'string' },
        { name: 'responsableActa', type: 'boolean' },
        { name: 'descripcion', type: 'string' },
        { name: 'descripcionAlternativa', type: 'string' },
        { name: 'grupo', type: 'string'},
        { name: 'firmante', type: 'boolean'},
        { name: 'presideVotacion', type: 'boolean'},
        { name: 'votante', type: 'boolean'}
    ]
});
