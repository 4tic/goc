Ext.define('goc.model.OrganoCargo', {
    extend: 'Ext.data.Model',
    requires : [ 'Ext.ux.uji.data.identifier.None' ],
    identifier :
    {
        type : 'none'
    },
    fields: [
        { name: 'id', type: 'number' },
        { name: 'cargoId', type: 'string' },
        { name: 'cargoNombre', type: 'string' },
        { name: 'cargoCodigo', type: 'string' },
        { name: 'organoId', type: 'string' }
    ]
});
