Ext.define('goc.view.historicoReunion.FormReabrirHistoricoReunion',
    {
        extend : 'Ext.window.Window',
        xtype : 'formReabrirHistoricoReunion',
        title : appI18N.reuniones.tituloMotivoReapertura,
        modal : true,
        bodyPadding : 10,
        layout : 'fit',

        requires : ['goc.view.historicoReunion.FormReabrirHistoricoReunionController'],
        controller : 'formReabrirHistoricoReunionController',

        bbar : {
            defaultButtonUI : 'default',
            items : [
                '->',
                {
                    xtype : 'button',
                    text : appI18N.reuniones.reabrir,
                    handler : 'onReabrirReunion'
                }, {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        },

        items : [
            {
                xtype : 'textarea',
                fieldLabel : appI18N.reuniones.motivoReapertura,
                width : 500,
                allowBlank : false,
                height : 200,
                name : 'cuerpoEmail'
            }
        ]
    });