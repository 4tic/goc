Ext.define('goc.view.reunion.GridAccesoExternosController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.gridAccesoExternosController',

    onLoad: function () {
        var viewModel = this.getViewModel();
        var store = viewModel.data.store;
        var reunion = Ext.ComponentQuery.query('grid[name=reunion]')[0].getSelection()[0];
        store.getProxy().url = appContext + '/rest/reuniones/' + reunion.data.id + '/externos';
        store.load();
    },

    eliminaExterno: function () {
        var grid = this.getView();
        var store =grid.getStore();
        var persona = grid.getView().getSelectionModel().getSelection()[0];
        Ext.Ajax.request({
            url : appContext + '/rest/reuniones/externos',
            method : 'DELETE',
            jsonData: {id: persona.data.id, personaId: persona.data.personaId,  nombre: persona.data.nombre, email: persona.data.email, reunionId: persona.data.reunionId},
            success : function(data)
            {
                store.reload();
            }
        });
    },


    onAddDesdePersona: function () {
        var grid = this.getView();
        var store =grid.getStore();
        var persona = Ext.create('goc.model.Persona');
        var reunion = Ext.ComponentQuery.query('grid[name=reunion]')[0].getSelection()[0];

        store.proxy.url= appContext + '/rest/reuniones/' + reunion.data.id + '/externos';

        var window = Ext.create('goc.view.common.LookupWindowPersonas',
            {
                appPrefix: 'goc',
                title: appI18N.personas.anyadirPersona
            });
        window.gridBusqueda.store.proxy.url='rest/personas/locales';
        window.show();

        window.on('LookoupWindowClickSeleccion', function (res) {

            persona.set('nombre', res.get('nombre'));
            persona.set('email', res.get('email'));
            persona.set('id', res.get('id'));
            Ext.Ajax.request({
                url : appContext + '/rest/reuniones/externos',
                method : 'POST',
                jsonData: {personaId: persona.get('id'), nombre: persona.get('nombre'), email: persona.get('email'), reunionId: reunion.data.id},
                success : function(data)
                {
                    store.reload();
                }
            });
        });
    }
});
