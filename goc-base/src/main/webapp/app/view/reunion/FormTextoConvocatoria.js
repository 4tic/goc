Ext.define('goc.view.reunion.FormTextoConvocatoria',
    {
        extend : 'Ext.window.Window',
        xtype : 'formTextoConvocatoria',
        title: appI18N.reuniones.enviarConvocatoriaTexto,
        modal : true,
        bodyPadding : 10,
        layout : 'fit',
        name: 'formTextoConvocatoria',

        requires : ['goc.view.reunion.FormTextoConvocatoriaController'],
        controller : 'formTextoConvocatoriaController',

        bbar : {
            defaultButtonUI : 'default',
            items: [
                '->',
                {
                    xtype: 'combobox',
                    emptyText: appI18N.miembros.convocante,
                    bind: {
                        store: '{convocantesStore}'
                    },
                    allowBlank: false,
                    name: 'convocantesCombo',
                    fieldLabel: appI18N.reuniones.convocante,
                    queryMode: 'local',
                    displayField: 'nombre',
                    valueField: 'nombre',
                    listeners:{
                        afterrender: 'loadUser'
                    },
                    hidden: bloquearRemitenteConvocatoria

                },
                {
                    xtype : 'button',
                    text : appI18N.reuniones.enviarConvocatoria,
                    handler : 'onEnviarConvocatoria'
                }, {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        },

        items : [
            {
                xtype: 'textarea',
                fieldLabel : appI18N.reuniones.textoAdicional,
                width: 500,
                height: 200,
                name : 'convocatoria',
                bind : {
                    value : '{textoConvocatoria}'
                }
            }
        ]
    });