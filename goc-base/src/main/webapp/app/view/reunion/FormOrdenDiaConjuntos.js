Ext.define('goc.view.reunion.FormOrdenDiaConjuntos', {
    extend: 'Ext.window.Window',
    xtype: 'formOrdenDiaConjuntos',

    width: '90%',
    maxHeight: 1000,
    modal: true,
    bodyPadding: 10,
    autoScroll: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    requires: [
        'goc.view.reunion.FormOrdenDiaConjuntosController'
    ],
    controller: 'formOrdenDiaConjuntosController',

    initComponent: function () {
        this.callParent(arguments);
        var puntosOrdenDia = this.getViewModel().get("puntosOrdenDia");
        var ref = this;

        if (puntosOrdenDia && puntosOrdenDia.length > 0) {
            puntosOrdenDia.forEach(function (puntoOrdenDia) {
                var titulo = appI18N.reuniones.puntoTitulo + ': ' + puntoOrdenDia.get('titulo') + ((isMultilanguageApplication()) ? ' - ' + puntoOrdenDia.get('tituloAlternativo') : '');
                var idPuntoOrdenDia = puntoOrdenDia.get('id');
                var fieldSetPunto = {
                    xtype: 'fieldset',
                    title: titulo,
                    name: idPuntoOrdenDia,
                    defaults: {
                        anchor: '100%'
                    },
                    items: []
                };

                fieldSetPunto.items.push({
                    xtype: 'htmleditor',
                    defaultValue: '',
                    enableFont: false,
                    name: 'deliberaciones',
                    fieldLabel: getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
                    labelAlign: 'top',
                    flex: 1,
                    emptyText: getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
                    value: puntoOrdenDia.get('deliberaciones'),
                    listeners: {
                        initialize: 'cleanFormatBeforePaste'
                    }
                });

                if (isMultilanguageApplication()) {
                    fieldSetPunto.items.push({
                        xtype: 'htmleditor',
                        enableFont: false,
                        name: 'deliberacionesAlternativas',
                        fieldLabel: getMultiLangLabel(appI18N.reuniones.deliberaciones, alternativeLanguage),
                        labelAlign: 'top',
                        flex: 1,
                        emptyText: getMultiLangLabel(appI18N.reuniones.deliberaciones, alternativeLanguage),
                        value: puntoOrdenDia.get('deliberacionesAlternativas'),
                        listeners: {
                            initialize: 'cleanFormatBeforePaste'
                        }
                    });
                }

                fieldSetPunto.items.push({
                    xtype: 'htmleditor',
                    defaultValue: '',
                    enableFont: false,
                    name: 'acuerdos',
                    fieldLabel: getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
                    labelAlign: 'top',
                    flex: 1,
                    emptyText: getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
                        value: puntoOrdenDia.get('acuerdos'),
                    listeners: {
                        initialize: 'cleanFormatBeforePaste'
                    }
                });

                if (isMultilanguageApplication()) {
                    fieldSetPunto.items.push({
                        xtype: 'htmleditor',
                        defaultValue: '',
                        enableFont: false,
                        name: 'acuerdosAlternativos',
                        fieldLabel: getMultiLangLabel(appI18N.reuniones.acuerdos, alternativeLanguage),
                        labelAlign: 'top',
                        flex: 1,
                        emptyText: getMultiLangLabel(appI18N.reuniones.acuerdos, alternativeLanguage),
                            value: puntoOrdenDia.get('acuerdosAlternativos'),
                        listeners: {
                            initialize: 'cleanFormatBeforePaste'
                        }
                    });
                }
                ref.add(fieldSetPunto);
            });
        }
    },

    bbar: {
        defaultButtonUI: 'default',
        items: [
            '->', {
                xtype: 'button',
                text: appI18N.reuniones.guardar,
                handler: 'onSaveRecord',
                bind : {
                    disabled : '{puntosOrdenDia.length <= 0}'
                }
            }, {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cancelar + '</a>',
                listeners: {
                    render: function (component) {
                        component.getEl().on('click', 'onCancel');
                    }
                }
            }
        ]
    },

    bind: {
        title: '{title}'
    },

    items: [
        {
            xtype: 'form',
            name: 'formPuntosOrdenDiaConjuntos',
            border: 0,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items: []
        }
    ],

    listeners : {
        afterLayout : 'afterRenderFormOrdenDiaConjuntos'
    }
});
