Ext.define('goc.view.reunion.FormHojaFirmasController',
    {
        extend : 'Ext.app.ViewController',
        alias : 'controller.formHojaFirmasController',

        onLoad : function()
        {
            var viewModel = this.getViewModel();
            var record = viewModel.get('record');
            var reunionId = viewModel.get('id');


            if (reunionId)
            {
                viewModel.get('store').load({
                    callback: function () {
                        Ext.Ajax.request({
                            scope: this,
                            url : appContext + '/rest/reuniones/' + record.id + '/hojafirmas',
                            method : 'GET',
                            success : function(data)
                            {
                                var hojaFirmas = Ext.decode(data.responseText).data;
                                if(hojaFirmas.id) {
                                    var row = {
                                        nombreFichero: hojaFirmas.nombreFichero
                                    };
                                    viewModel.get('store').add(row);
                                    viewModel.set('existeHojaFirmas', true);
                                    viewModel.set('hojaFirmas', hojaFirmas);
                                }
                            }
                        });
                    }
                });

            }

        },

        onClose : function()
        {
            var win = Ext.WindowManager.getActive();

            if (win)
            {
                win.destroy();
            }
        },

        descargaDocumento : function(documentoId)
        {
            var viewModel = this.getViewModel();
            var reunionId = viewModel.get('id');
            var url = appContext + '/rest/reuniones/' + reunionId + '/hojafirmas/descargar';

            var body = Ext.getBody();

            var form = body.createChild({
                tag : 'form',
                cls : 'x-hidden',
                action : url,
                target : 'iframe'
            });
            form.dom.submit();
        },

        subirDocumento : function()
        {
            var viewModel = this.getViewModel();
                var reunionId = viewModel.get('id');
                var view = this.getView();

                var form = view.down('form[name=subirDocumento]');
                if (form.getForm().isValid() && form.down('filefield[name=documento]').getValue() !== "")
                {
                    form.submit({
                        url : appContext + '/rest/reuniones/' + reunionId + '/hojafirmas',
                        scope : this,
                        success : function()
                        {
                            this.onLoad();
                            Ext.Msg.alert(appI18N.reuniones.hojaFirmasSubidaTitulo, appI18N.reuniones.hojaFirmasSubida);
                        }
                    });
                }
        },

        onFileChange : function(obj, value)
        {
            var filename = value.split(/(\\|\/)/g).pop(),
                labelNombreDocumento = this.getView().lookupReference('nombreDocumento');
            labelNombreDocumento.setValue(filename);
        }
});
