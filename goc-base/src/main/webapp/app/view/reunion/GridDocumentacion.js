Ext.define('goc.view.reunion.GridDocumentacion',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridDocumentacion',
        multiSelect: false,
        scrollable: true,
        viewConfig: {
            emptyText: appI18N.reuniones.documentacionAdjuntaVacia
        },
        flex : 1,
        minHeight : 150,
        margin : '5 0 5 0',
        bbar: null,
        tbar: null
    }
);
