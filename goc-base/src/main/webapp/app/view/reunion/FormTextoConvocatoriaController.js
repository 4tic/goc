Ext.define('goc.view.reunion.FormTextoConvocatoriaController',
    {
        onEnviarConvocatoria: function ()
        {
            var vm = this.getViewModel();
            var record = vm.get('record');
            var ref = this;
            var combo = Ext.ComponentQuery.query('formTextoConvocatoria[name=formTextoConvocatoria]')[0].down('combobox[name=convocantesCombo]');
            var comboRecord = combo.getSelection();
            var jsonData;
            if(comboRecord != null){
                jsonData = {texto: vm.get('textoConvocatoria'), convocante: comboRecord.data.nombre, convocanteEmail : comboRecord.data.email};
            } else {
                jsonData = {texto: vm.get('textoConvocatoria')};
            }
            Ext.Ajax.request(
                {
                    url : appContext + '/rest/reuniones/' + record.get('id') + '/enviarconvocatoria',
                    method : 'PUT',
                    jsonData: jsonData,
                    success : function(response)
                    {
                        var data = Ext.decode(response.responseText);
                        var mensajeRespuesta = (data.message && data.message.indexOf("appI18N") != -1) ? eval(data.message) : data.message;

                        vm.get('reunionesStore').reload();
                        ref.onClose();

                        Ext.Msg.alert(appI18N.reuniones.resultadoEnvioConvocatoriaTitle, mensajeRespuesta);
                    },
                    scope : this
                }
            );
        },
        extend : 'Ext.app.ViewController',


        alias : 'controller.formTextoConvocatoriaController',

        onClose : function()
        {
            var win = Ext.WindowManager.getActive();
            if (win)
            {
                win.destroy();
            }
        },

        loadUser: function (combo, eOpts) {
            if (!bloquearRemitenteConvocatoria) {
                var convocantesStore = this.getViewModel().get('convocantesStore');
                var reunionId = this.getViewModel().get('record').get('id');
                convocantesStore.load({
                    params: {
                        reunionId: reunionId
                    },
                    callback: function (records, operation, success) {
                        combo.setValue(records[records.length - 1]);
                    }
                });
            }
        }
    });
