Ext.define('goc.view.reunion.FormDocumentacionController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.formDocumentacionController',

    onLoad : function()
    {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');

        if (reunionId)
        {
            viewModel.get('store').load({
                url : appContext + '/rest/reuniones/' + reunionId + '/documentos'
            });
        }

    },

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        var grid = Ext.ComponentQuery.query('grid[name=reunion]')[0];
        grid.getStore().reload();

        if (win)
        {
            win.destroy();
        }
    },

    borraDocumento : function(documentoId)
    {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');

        Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar el document?', function(result)
        {
            if (result === 'yes')
            {
                Ext.Ajax.request({
                    url : appContext + '/rest/reuniones/' + reunionId + '/documentos/' + documentoId,
                    method : 'DELETE',
                    success : function()
                    {
                        viewModel.get('store').reload();
                    }
                });
            }
        });
    },

    descargaDocumento : function(documentoId)
    {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var url = appContext + '/rest/reuniones/' + reunionId + '/documentos/' + documentoId + '/descargar';

        var body = Ext.getBody();
        var frame = body.createChild({
            tag : 'iframe',
            cls : 'x-hidden',
            name : 'iframe'
        });

        var form = body.createChild({
            tag : 'form',
            cls : 'x-hidden',
            action : url,
            target : 'iframe'
        });
        form.dom.submit();
    },

    subirDocumento : function()
    {
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var view = this.getView();

        var form = view.down('form[name=subirDocumento]');
        var grid = view.down('grid');
        var inputIdDoc = form.down('hiddenfield');
        var url = appContext + '/rest/reuniones/' + reunionId + '/documentos/';
        var showConfirm = false;
        var confirmado = false;

        var idDoc = parseInt(inputIdDoc.getValue(),10);
        if(inputIdDoc.getValue() != null && typeof idDoc === 'number' && idDoc > 0)
        {
            url = appContext + '/rest/reuniones/' + reunionId + '/documentos/' + idDoc;
            showConfirm = true;
        }

        if (form.getForm().isValid() && form.down('filefield[name=documento]').getValue() !== "")
        {

            if(showConfirm)
            {
                if(confirm(appI18N.reuniones.subirNuevaVersionConfirm))
                {
                    confirmado = true;
                }
            }

            if((showConfirm && confirmado) || !showConfirm)
            {
                form.submit({
                    url: url,
                    scope: this,
                    success: function () {
                        viewModel.get('store').load({
                            url: appContext + '/rest/reuniones/' + reunionId + '/documentos'
                        });
                        grid.setSelection(null);
                        this.limpiarFormulario();
                    }
                });
            }
        }
    },

    onFileChange : function(obj, value)
    {
        var filename = value.split(/(\\|\/)/g).pop(),
            labelNombreDocumento = this.getView().lookupReference('nombreDocumento');
        labelNombreDocumento.setValue(filename);
    },

    onSelect: function ()
    {
        var record = this.getView().down('grid').getSelection()[0];
        if(record != null)
        {
            var inputDescripcion = this.getView().lookupReference('descripcion');
            var inputDescripcionAlternativa = this.getView().lookupReference('descripcionAlternativa');
            inputDescripcion.setValue(record.get('descripcion'));
            inputDescripcion.disable();
            if(isMultilanguageApplication())
            {
                inputDescripcionAlternativa.setValue(record.get('descripcionAlternativa'));
                inputDescripcionAlternativa.disable();
            }
            var form = this.getView().down('form');
            form.setTitle(appI18N.reuniones.subirNuevoDocumentoTituloFormulario);
            var btnSubirArchivo = this.getView().lookupReference('btnSubirArchivo');
            btnSubirArchivo.setText(appI18N.reuniones.subirNuevaVersion);
            var inputIdDoc = form.down('hiddenfield');
            inputIdDoc.setValue(record.get('id'));
        }
    },

    limpiarFormulario: function ()
    {
        this.getView().down('grid').setSelection(null);
        this.getView().down('form').reset();
        var inputDescripcion = this.getView().lookupReference('descripcion');
        inputDescripcion.enable();
        if(isMultilanguageApplication())
        {
            var inputDescripcionAlternativa = this.getView().lookupReference('descripcionAlternativa');
            inputDescripcionAlternativa.enable();
        }
        var form = this.getView().down('form');
        form.setTitle(appI18N.reuniones.subirNuevoDocumento);
        var btnSubirArchivo = this.getView().lookupReference('btnSubirArchivo');
        btnSubirArchivo.setText(appI18N.reuniones.subirDocumento);
    },

    onEditDescripcionDocumento: function () {
        var gridDocumentacion = this.getView().down('grid[name=gridDocumentacion]');
        var viewModel = this.getViewModel();
        var reunionId = viewModel.get('id');
        var url = appContext + '/rest/reuniones/' + reunionId + '/documentos';
        gridDocumentacion.getStore().getProxy().url = url;
    }
});
