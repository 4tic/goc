Ext.define('goc.view.reunion.FormOrdenDiaController', {
    extend : 'Ext.app.ViewController',
    alias : 'controller.formOrdenDiaController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        var grid = Ext.ComponentQuery.query('treepanel[name=ordenDia]')[0];
        grid.getStore().reload();

        if (win)
        {
            win.destroy();
        }
    },

    onCancel : function()
    {
        this.onClose();
    },

    onSaveRecord : function(button, context)
    {
        var vm   = this.getViewModel(),
            view = this.getView(),
            form = Ext.ComponentQuery.query('form[name=puntoOrdenDia]')[0];

        var reunionId = vm.get('reunionId');
        var grid = Ext.ComponentQuery.query('treepanel[name=ordenDia]')[0];

        if (form.isValid())
        {
            view.setLoading(true);
            var record = vm.get('puntoOrdenDia');
            var store = vm.get('store');
            var url = appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia';

            if (record.create !== true)
            {
                url += '/' + record.get('id');
                return record.save({
                    url: url,
                    success : function()
                    {
                        this.onClose();
                    },
                    failure : function()
                    {
                        view.setLoading(false);
                    },
                    scope : this
                });
            }
            var idPuntoSeleccionado = (this.hayUnPuntoSeleccionado(grid)) ? this.getIdPuntoSeleccionado(grid) : null;
            if (idPuntoSeleccionado && !record.puntoOrdenDiaPrimerNivel) {
                record.puntoSuperior = idPuntoSeleccionado;
            }

            store.add(record);
            store.sync({
                url: url,
                success : function()
                {
                    grid.getStore().reload();
                    this.onClose();
                },
                failure : function()
                {
                    view.setLoading(false);
                },
                scope : this
            });
        }
    },

    hayUnPuntoSeleccionado: function(grid) {
        return grid.getSelection().length == 1;
    },

    getIdPuntoSeleccionado: function(grid) {
        return grid.getSelection()[0].get('id');
    },

    afterRenderFormOrdenDia : function(windowFormOrdenDia)
    {
        var height = Ext.getBody().getViewSize().height;
        if (windowFormOrdenDia.getHeight() > height)
        {
            windowFormOrdenDia.setHeight(height - 30);
            windowFormOrdenDia.setPosition(windowFormOrdenDia.x, 15);
        }
    },

    cleanFormatBeforePaste : function (editor) {
        editor.getEditorBody().onpaste = function (event) {
            event.preventDefault();
            event.stopPropagation();
            editor.insertAtCursor(event.clipboardData.getData('text'));
        };
    }

});
