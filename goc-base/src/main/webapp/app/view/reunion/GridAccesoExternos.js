Ext.define('goc.view.reunion.GridAccesoExternos',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        alias: 'widget.gridAccesoExternos',
            requires : ['goc.view.reunion.GridAccesoExternosController'],
            controller : 'gridAccesoExternosController',
            name : 'gridAccesoExternosController',
        multiSelect: false,
        scrollable: true,
        viewConfig: {
            emptyText: appI18N.reuniones.externosVacio
        },
        hideHeaders : true,
        flex : 1,
        minHeight : '30%',
        margin : '5 0 5 0',
        bbar: null,
        tbar: [
            {
                xtype : 'button',
                iconCls : 'fa fa-plus',
                text : appI18N.common.anadir,
                handler : 'onAddDesdePersona'
            },
            {
                xtype: 'button',
                name: 'borrar',
                iconCls: 'fa fa-times',
                text: appI18N.common.borrar ,
                handler: 'eliminaExterno'
            }]
    }
);