var gridColumns = [
    {
        text : appI18N.organos.nombre,
        dataIndex : 'nombre',
        flex : 1
    }
];

{
    gridColumns.push({
        text : appI18N.organos.email,
        dataIndex : 'email',
        flex : 1
    });
}


Ext.define('goc.view.reunion.FormAccesoExternos',
    {
        extend : 'Ext.window.Window',
        xtype : 'formAccesoExternos',
        title : appI18N.reuniones.accesExternosTitle,
        width : '60%',
        height : '70%',
        autoScroll : true,
        modal : true,
        bodyPadding : 10,
        layout : {
            type : 'vbox',
            align : 'stretch'
        },

        requires : [
            'goc.view.reunion.GridAccesoExternos'
        ],

        bbar : null,

        items : [
            {
                xtype : 'gridAccesoExternos',
                bind : {
                    store : '{store}'
                },
                columns : gridColumns,
                name: 'gridAccesoExternos'
            }
        ],
        listeners : {
            render : 'onLoad'
        }
    });
