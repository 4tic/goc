Ext.define('goc.view.reunion.FormOrdenDiaConjuntosController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.formOrdenDiaConjuntosController',

    onClose: function () {
        var win = Ext.WindowManager.getActive();
        var grid = Ext.ComponentQuery.query('treepanel[name=ordenDia]')[0];
        grid.getStore().reload();

        if (win) {
            win.destroy();
        }
    },

    onCancel: function () {
        this.onClose();
    },

    afterRenderFormOrdenDiaConjuntos: function (windowFormOrdenDiaConjuntos) {
        var height = Ext.getBody().getViewSize().height;
        if (windowFormOrdenDiaConjuntos.getHeight() > height) {
            windowFormOrdenDiaConjuntos.setHeight(height - 30);
            windowFormOrdenDiaConjuntos.setPosition(windowFormOrdenDiaConjuntos.x, 15);
        }
    },

    inicializaPuntosGuardadosCheck: function(fieldsets) {
        var arrChecks = {};
        Ext.each(fieldsets, function(fieldset) {
            arrChecks[fieldset.name] = false;
        });
        return arrChecks;
    },

    allPuntosGuardados: function (arrChecks) {
        for (var i = 0; i<arrChecks.length;i++) {
            if (!arrChecks[i])
                return false;
        }
        return true;
    },

    onSaveRecord: function (button, context) {
        var vm = this.getViewModel(),
            view = this.getView(),
            fieldsets = vm.getView().query('fieldset'),
            ref = this;

        view.setLoading(appI18N.common.loading);

        var puntosGuardadosCheck = this.inicializaPuntosGuardadosCheck(fieldsets);

        Ext.each(fieldsets, function (puntoOrdenDiaFieldset) {
            var deliberacionesAlternativasTxt = (isMultilanguageApplication()) ? puntoOrdenDiaFieldset.query('[name=deliberacionesAlternativas]')[0].value : null;
            var acuerdosAlternativosTxt = (isMultilanguageApplication()) ? puntoOrdenDiaFieldset.query('[name=acuerdosAlternativos]')[0].value : null;
            var puntoOrdenDia = Ext.create('goc.model.PuntoOrdenDiaAcuerdo', {
                deliberaciones: puntoOrdenDiaFieldset.query('[name=deliberaciones]')[0].value,
                deliberacionesAlternativas: deliberacionesAlternativasTxt,
                acuerdos: puntoOrdenDiaFieldset.query('[name=acuerdos]')[0].value,
                acuerdosAlternativos: acuerdosAlternativosTxt
            });

            var puntoOrdenDiaId = puntoOrdenDiaFieldset.name;

            Ext.Ajax.request({
                url: appContext + '/rest/reuniones/' + vm.get('reunionId') + '/puntosOrdenDia/' + puntoOrdenDiaId + '/acuerdosydeliberaciones',
                method: 'PUT',
                jsonData: puntoOrdenDia.data,
                success: function (data) {
                    puntosGuardadosCheck[puntoOrdenDiaId] = true;
                    if (ref.allPuntosGuardados(puntosGuardadosCheck)) {
                        view.setLoading(false);
                        ref.onClose();
                    }
                },
                failure: function(err) {
                    view.setLoading(false);
                }
            });
        });
    },
    cleanFormatBeforePaste : function (editor) {
        editor.getEditorBody().onpaste = function (event) {
            event.preventDefault();
            event.stopPropagation();
            editor.insertAtCursor(event.clipboardData.getData('text'));
        };
    }
});