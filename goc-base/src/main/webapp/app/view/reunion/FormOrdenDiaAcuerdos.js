var gridColumns = [
    {
        text : getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
        dataIndex : 'descripcion',
        flex : 1,
        editor: {
            field: {
                allowBlank: false
            }
        }
    }
];

if (isMultilanguageApplication())
{
    gridColumns.push({
        text : getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        dataIndex : 'descripcionAlternativa',
        flex : 1,
        editor: {
            field: {
                allowBlank: false
            }
        }
    });
}

gridColumns.push({
    tooltip : appI18N.reuniones.publicarAcuerdos,
    dataIndex : 'publico',
    width : 75,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor:{
        xtype: 'checkbox'
    }
});

gridColumns.push({
    xtype : 'actioncolumn',
    align : 'right',
    width : 30,
    items : [
        {
            iconCls : 'x-fa fa-download',
            tooltip : appI18N.reuniones.descargar,
            handler : function(grid, index)
            {
                var rec = grid.getStore().getAt(index);
                var documentoId = rec.get('id');
                grid.up('formOrdenDiaAcuerdos').fireEvent('descargaPuntoOrdenDiaDocumento', documentoId);
            }
        }
    ]
});


gridColumns.push({
    xtype : 'actioncolumn',
    bind : {
        disabled : '{reunionCompletada}'
    },
    align : 'right',
    width : 30,
    items : [
        {
            iconCls : 'x-fa fa-remove',
            tooltip : appI18N.common.borrar,
            isDisabled : function(grid)
            {
                return this.disabled;
            },
            handler : function(grid, index)
            {
                var rec = grid.getStore().getAt(index);
                var documentoId = rec.get('id');
                grid.up('formOrdenDiaAcuerdos').fireEvent('borraPuntoOrdenDiaDocumento', documentoId);
            }
        }
    ]
});

var formItems = [
    {
        xtype : 'textfield',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
        allowBlank : false,
        emptyText : appI18N.reuniones.descripcion,
        width : '100%',
        flex : 1,
        name : 'descripcion',
        reference : 'descripcion'
    }
];

if (isMultilanguageApplication())
{
    formItems.push({
        xtype : 'textfield',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        allowBlank : false,
        emptyText : appI18N.reuniones.descripcion,
        width : '100%',
        flex : 1,
        name : 'descripcionAlternativa',
        reference: 'descripcionAlternativa'
    });
}

formItems.push({
    xtype : 'filefield',
    buttonOnly : true,
    width : 200,
    name : 'documento',
    buttonConfig : {
        text :  appI18N.common.seleccionarFichero
    },
    listeners : {
        change : 'onFileChangeOrdenDia'
    }
});

formItems.push({
    xtype : 'displayfield',
    width : '100%',
    name : 'nombreDocumento',
    reference : 'nombreDocumento'
});

Ext.define('goc.view.reunion.FormOrdenDiaAcuerdos',
{
    extend : 'Ext.window.Window',
    xtype : 'formOrdenDiaAcuerdos',
    autoScroll : true,
    bind : {
        title : '{title}'
    },
    width : 640,
    minHeight : 540,
    modal : true,
    bodyPadding : 10,
    layout : {
        type : 'vbox',
        align : 'stretch'
    },

    requires : ['goc.view.reunion.FormOrdenDiaAcuerdosController'],
    controller : 'formOrdenDiaAcuerdosController',

    bbar : [
        '->',
        {
            xtype : 'panel',
            html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
            listeners : {
                render : function(component)
                {
                    component.getEl().on('click', 'onClose');
                }
            }
        }
    ],

    items : [
        {
            xtype : 'gridDocumentacion',
            viewConfig : {
                emptyText : appI18N.reuniones.acuerdosAdjuntosVacia
            },
            bind : {
                store : '{store}'
            },
            hideHeaders : true,
            columns : gridColumns,
            listeners: {
                select: 'onSelect'
            },
            autoScroll : true,
            minHeight : 150
        },
        {
            xtype : 'form',
            autoScroll : true,
            bind : {
                disabled : '{reunionCompletada}'
            },
            frame : true,
            title : appI18N.reuniones.subirNuevoAcuerdo,
            layout : 'anchor',
            border : false,
            padding : 5,
            name : 'subirDocumento',
            items : [
                {
                    xtype: 'hidden',
                    name: 'idDoc',
                    reference: 'idDoc'
                },
                {
                    xtype : 'fieldcontainer',
                    anchor : '100%',
                    layout : 'vbox',
                    items : formItems
                }
            ],
            buttons : [
                {
                    text : appI18N.reuniones.subirAcuerdo,
                    handler : 'subirPuntoOrdenDiaDocumento',
                    reference: 'btnSubirArchivo'

                },
                {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.limpiarFormulario + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'limpiarFormulario');
                        }
                    }
                }
            ]
        }
    ],
    listeners : {
        render : 'onLoad',
        subirPuntoOrdenDiaDocumento : 'subirPuntoOrdenDiaDocumento',
        descargaPuntoOrdenDiaDocumento : 'descargaPuntoOrdenDiaDocumento',
        borraPuntoOrdenDiaDocumento : 'borraPuntoOrdenDiaDocumento',
        updatePuntoOrdenDiaDocumento: 'updatePuntoOrdenDiaDocumento'
    }
});
