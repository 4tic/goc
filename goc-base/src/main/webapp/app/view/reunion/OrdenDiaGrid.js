var gridColumns = [
    {
        text : 'Id',
        width : 80,
        dataIndex : 'id',
        hidden : true
    },
    {
        xtype : 'actioncolumn',
        title : appI18N.reuniones.posicion,
        align : 'right',
        width : 30,
        items : [
            {
                iconCls : 'x-fa fa-arrow-up',
                tooltip : appI18N.reuniones.subir,
                isDisabled : function(grid)
                {
                    var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
                    var record = reunionGrid.getView().getSelectionModel().getSelection()[0];
                    return record.get('completada');
                },
                handler : function(grid, index)
                {
                    var rec = grid.getStore().getAt(index);
                    var puntoOrdenDiaId = rec.get('id');
                    grid.up('panel[name=ordenDia]').fireEvent('subePuntoOrdenDia', puntoOrdenDiaId);
                }
            }
        ]
    },
    {
        xtype : 'actioncolumn',
        title : appI18N.reuniones.posicion,
        align : 'right',
        width : 30,
        items : [
            {
                iconCls : 'x-fa fa-arrow-down',
                tooltip : appI18N.reuniones.bajar,
                isDisabled : function(grid)
                {
                    var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
                    var record = reunionGrid.getView().getSelectionModel().getSelection()[0];
                    return record.get('completada');
                },
                handler : function(grid, index)
                {
                    var rec = grid.getStore().getAt(index);
                    var puntoOrdenDiaId = rec.get('id');
                    grid.up('panel[name=ordenDia]').fireEvent('bajaPuntoOrdenDia', puntoOrdenDiaId);
                }
            }
        ]
    },
    {
        xtype: 'treecolumn',
        text : getMultiLangLabel(appI18N.reuniones.titulo, mainLanguage),
        dataIndex : 'titulo',
        flex : 1,
        renderer : function(titulo, celda, record)
        {
            if(record.get('urlActaAnterior')) {
                return titulo + '&nbsp;&nbsp;<a class="link" target="_blank" href="' + record.get('urlActaAnterior') + '"><i class="fa fa-external-link"></i></a>';
            }
            return titulo;
        }
    }
];

if (isMultilanguageApplication())
{
    gridColumns.push({
        text : getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        dataIndex : 'tituloAlternativo',
        flex : 1,
        renderer : function(titulo, celda, record)
        {
            if(record.get('urlActaAnteriorAlt')) {
                return titulo + '&nbsp;&nbsp;<a class="link" target="_blank" href="' + record.get('urlActaAnteriorAlt') + '"><i class="fa fa-external-link"></i></a>';
            }
            return titulo;
        }
    });
}

gridColumns.push({
    text : appI18N.reuniones.publico,
    dataIndex : 'publico',
    renderer : function(val)
    {
        if(val == "true" || val === true)
            return 'Sí';
        else
            return 'No';
    }
});

gridColumns.push({
    text : appI18N.votos.votacionPublica,
    dataIndex : 'votoPublico',
    renderer : function(val)
    {
        if(val == "true" || val === true)
            return 'Sí';
        else
            return 'No';
    }
});

gridColumns.push({
    dataIndex : 'numeroDocumentos',
    text : appI18N.reuniones.documentos,
    align : 'center',
    reference: 'documentos',
    renderer : function(value)
    {
        if (value > 0)
        {
            return '<span class="fa fa-file-text-o"></span>';
        }
        return '';
    }
});

gridColumns.push({
    dataIndex : 'numeroAcuerdos',
    text : appI18N.reuniones.acuerdos,
    align : 'center',
    reference: 'acuerdos',
    renderer : function(value)
    {
        if (value > 0)
        {
            return '<span class="fa fa-file-text-o"></span>';
        }
        return '';
    }
});

Ext.define('goc.view.reunion.OrdenDiaGrid',
{
    extend : 'Ext.tree.Panel',
    alias : 'widget.ordenDiaGrid',
    plugins : [],
    disabled : true,
    requires : ['goc.view.reunion.OrdenDiaGridController'],
    controller : 'ordenDiaGridController',
    bind : {
        store : '{puntosOrdenDiaTreeStore}',
        selection : '{selectedReunion}'
    },
    name : 'ordenDia',
    reference : 'ordenDiaGrid',
    rootVisible: false,
    scrollable : true,
    title : appI18N.reuniones.tituloOrdenDia,
    multiSelect : false,
    columns : gridColumns,
    viewConfig : {
        emptyText : appI18N.reuniones.ordendiaVacio,
        plugins: {
            ptype: 'treeviewdragdrop',
            containerScroll: true,
            dragText: appI18N.reuniones.moverPuntoText
        }
    },
    initComponent: function() {
        var ref = this;
        this.contextMenu = Ext.create('Ext.menu.Menu', {
            $initParent: ref,
            items: [{
                iconCls: 'ext ext-sw-handle',
                text: appI18N.reuniones.puntosContextMenu,
                handler: 'onAdd'
            }]
        });
        this.callParent(arguments);
    },
    split: true,
    listeners : {
        reunionSelected : 'onRefresh',
        celldblclick : 'onEdit',
        onDelete : 'onDelete',
        afterEdit : 'onAfterEdit',
        bajaPuntoOrdenDia : 'bajaPuntoOrdenDia',
        subePuntoOrdenDia : 'subePuntoOrdenDia',
        itemcontextmenu : 'onContextMenuClic',
        drop: 'onDropPuntoOrdenDia'
    },

    tbar : [
        {
            xtype : 'button',
            iconCls : 'fa fa-plus',
            text : appI18N.common.anadir,
            handler : 'onAddFirstLevel'
        },
        {
            xtype : 'button',
            iconCls : 'fa fa-edit',
            text : appI18N.common.editar,
            handler : 'onEdit',
            isDisabled : function()
            {
                return true;
            }
        },
        {
            xtype : 'button',
            iconCls : 'fa fa-remove',
            text : appI18N.common.borrar,
            handler : 'onDelete'
        }, ' | ',
        {
            xtype : 'button',
            iconCls : 'fa fa-check',
            text : appI18N.reuniones.documentacionAdjunta,
            handler : 'onAttachmentEdit'
        }, {
            xtype : 'button',
            iconCls : 'fa fa-check',
            text : appI18N.reuniones.acuerdos,
            handler : 'onAttachmentAcuerdosEdit'
        }, ' | ',
        {
            xtype : 'button',
            iconCls : 'fa fa-check',
            text : appI18N.reuniones.descriptoresYclaves,
            handler : 'onAttachmentEditDescriptores'
        }
    ],

    clearStore : function()
    {
        var store = this.getStore();

        store.suspendAutoSync();
        store.removeAll();
        store.clearData();
        store.resumeAutoSync();
    }
});