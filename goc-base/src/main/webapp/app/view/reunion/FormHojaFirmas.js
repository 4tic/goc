var gridColumns = [
    {
        dataIndex : 'nombreFichero',
        flex : 1
    }
];

gridColumns.push({
    xtype : 'actioncolumn',
    align : 'right',
    width : 30,
    items : [
        {
            iconCls : 'x-fa fa-download',
            tooltip : appI18N.reuniones.descargar,
            handler : function(grid, index)
            {
                var rec = grid.getStore().getAt(index);
                var documentoId = rec.get('id');
                grid.up('formHojaFirmas').fireEvent('descargaDocumento', documentoId);
            }
        }
    ]
});

var formItems = [
];

formItems.push({
    xtype : 'filefield',
    buttonOnly : true,
    width : 200,
    name : 'documento',
    buttonConfig : {
        text : appI18N.common.seleccionarFichero
    },
    listeners : {
        change : 'onFileChange'
    }
});

formItems.push({
    xtype : 'displayfield',
    width : '100%',
    name : 'nombreDocumento',
    reference : 'nombreDocumento'
});

Ext.define('goc.view.reunion.FormHojaFirmas',
    {
        extend : 'Ext.window.Window',
        xtype : 'formHojaFirmas',
        bind : {
            title : appI18N.reuniones.hojaFirmasTitulo + ' ' + '{title}'
        },
        width : 640,
        minHeight : 300,
        autoScroll : true,
        modal : true,
        bodyPadding : 10,
        layout : {
            type : 'vbox',
            align : 'stretch'
        },

        requires : ['goc.view.reunion.FormHojaFirmasController'],
        controller : 'formHojaFirmasController',

        bbar : [
            '->',
            {
                xtype : 'panel',
                html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.reuniones.cerrar + '</a>',
                listeners : {
                    render : function(component)
                    {
                        component.getEl().on('click', 'onClose');
                    }
                }
            }
        ],

        items : [
            {
                xtype : 'grid',
                flex : 1,
                scrollable : true,
                minHeight : 50,
                margin : '5 0 5 0',
                autoScroll : true,
                viewConfig : {
                    emptyText : appI18N.reuniones.documentacionAdjuntaVacia
                },
                bind : {
                    store : '{store}'
                },
                hideHeaders : true,
                columns : gridColumns
            },
            {
                xtype : 'form',
                bind : {
                    disabled : '{completada}'
                },
                frame : true,
                title : appI18N.reuniones.subirHojaFirmasForm,
                layout : 'anchor',
                border : false,
                padding : 5,
                autoScroll : true,
                name : 'subirDocumento',
                items : [
                    {
                        xtype : 'fieldcontainer',
                        anchor : '100%',
                        layout : 'hbox',
                        items : formItems
                    }
                ],
                buttons : [
                    {
                        text : appI18N.reuniones.subirDocumento,
                        handler : 'subirDocumento'

                    }
                ]
            }
        ],
        listeners : {
            render : 'onLoad',
            subirDocumento : 'subirDocumento',
            descargaDocumento : 'descargaDocumento'
        }
    });
