var formItems = [
    {
        name : 'id',
        xtype : 'hidden',
        bind : '{puntoOrdenDia.id}'
    },
    {
        allowBlank : false,
        fieldLabel : getMultiLangLabel(appI18N.reuniones.titulo, mainLanguage),
        labelWidth: 130,
        xtype : 'textareafield',
        grow: true,
        growMax: 20,
        minHeight: 20,
        maxHeight: 20,
        height: 20,
        labelAlign: 'top',
        name : 'titulo',
        emptyText : getMultiLangLabel(appI18N.reuniones.titulo, mainLanguage),
        bind : {
            value : '{puntoOrdenDia.titulo}',
            disabled : '{reunionCompletada}'
        }
    }
];

if (isMultilanguageApplication())
{
    formItems.push({
        allowBlank : false,
        fieldLabel : getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        labelWidth: 130,
        xtype : 'textareafield',
        grow: true,
        growMax: 20,
        minHeight: 20,
        maxHeight: 20,
        height: 20,
        labelAlign: 'top',
        name : 'tituloAlternativo',
        emptyText : getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.tituloAlternativo}',
            disabled : '{reunionCompletada}'
        }
    });
}

formItems.push({
    xtype : 'container',
    layout : 'hbox',
    items : [
        {
            xtype : 'tbfill',
            flex : 1
        },
        {
            boxLabel : appI18N.reuniones.publicarAcuerdos,
            name : 'publico',
            bind : {
                disabled : '{reunionCompletada}',
                value : '{puntoOrdenDia.publico}'
            },
            xtype : 'checkbox',
            inputValue : '1',
            checked : true
        }
    ]
});

formItems.push({
    xtype : 'container',
    layout : 'hbox',
    items : [
        {
            xtype : 'tbfill',
            flex : 1
        },
        {
            xtype      : 'radiogroup',
            name: 'radioGroupPrivacidadVotacion',
            fieldLabel : appI18N.votos.votacion,
            allowBlank: false,
            bind: {
                value: '{puntoOrdenDia.votoPublico}',
                hidden: '{!reunionHasVotacion}'
            },
            simpleValue: true,
            defaults: {
                flex: 1
            },
            layout: 'hbox',
            items: [
                {
                    boxLabel  :  appI18N.votos.publica,
                    name      : 'votoPublico',
                    inputValue: true,
                    id        : 'radioVotacionPublica',
                    padding : '0 15 0 0'
                },
                {
                    xtype : 'tbfill',
                    flex : 1
                },{
                    boxLabel  : appI18N.votos.privada,
                    name      : 'votoPublico',
                    inputValue: false,
                    id        : 'radioVotacionPrivada'
                }
            ]
        }
    ]
});

formItems.push({
    xtype : 'htmleditor',
    defaultValue: '',
    enableFont: false,
    name : 'descripcion',
    fieldLabel : getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
    labelAlign : 'top',
    flex : 1,
    emptyText : getMultiLangLabel(appI18N.reuniones.descripcion, mainLanguage),
    bind : {
        value : '{puntoOrdenDia.descripcion}',
        disabled : '{reunionCompletada}'
    },
    listeners: {
        initialize: 'cleanFormatBeforePaste'
    }
});

if (isMultilanguageApplication())
{
    formItems.push({
        xtype : 'htmleditor',
        defaultValue: '',
        enableFont: false,
        name : 'descripcionAlternativa',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        labelAlign : 'top',
        flex : 1,
        emptyText : getMultiLangLabel(appI18N.reuniones.descripcion, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.descripcionAlternativa}',
            disabled : '{reunionCompletada}'
        },
        listeners: {
            initialize: 'cleanFormatBeforePaste'
        }
    });
}

formItems.push({
    xtype : 'htmleditor',
    defaultValue: '',
    enableFont: false,
    name : 'deliberaciones',
    fieldLabel : getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
    labelAlign : 'top',
    flex : 1,
    emptyText : getMultiLangLabel(appI18N.reuniones.deliberaciones, mainLanguage),
    bind : {
        value : '{puntoOrdenDia.deliberaciones}',
        disabled : '{reunionCompletada}'
    },
    listeners: {
        initialize: 'cleanFormatBeforePaste'
    }
});

if (isMultilanguageApplication())
{
    formItems.push({
        xtype : 'htmleditor',
        defaultValue: '',
        enableFont: false,
        name : 'deliberacionesAlternativas',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.deliberaciones, alternativeLanguage),
        labelAlign : 'top',
        flex : 1,
        emptyText : getMultiLangLabel(appI18N.reuniones.deliberaciones, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.deliberacionesAlternativas}',
            disabled : '{reunionCompletada}'
        },
        listeners: {
            initialize: 'cleanFormatBeforePaste'
        }
    });
}

formItems.push({
    xtype : 'htmleditor',
    defaultValue: '',
    enableFont: false,
    name : 'acuerdos',
    fieldLabel : getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
    labelAlign : 'top',
    flex : 1,
    emptyText : getMultiLangLabel(appI18N.reuniones.acuerdos, mainLanguage),
    bind : {
        value : '{puntoOrdenDia.acuerdos}',
        disabled : '{reunionCompletada}'
    },
    listeners: {
        initialize: 'cleanFormatBeforePaste'
    }
});

if (isMultilanguageApplication())
{
    formItems.push({
        xtype : 'htmleditor',
        defaultValue: '',
        enableFont: false,
        name : 'acuerdosAlternativos',
        fieldLabel : getMultiLangLabel(appI18N.reuniones.acuerdos, alternativeLanguage),
        labelAlign : 'top',
        flex : 1,
        emptyText : getMultiLangLabel(appI18N.reuniones.acuerdos, alternativeLanguage),
        bind : {
            value : '{puntoOrdenDia.acuerdosAlternativos}',
            disabled : '{reunionCompletada}'
        },
        listeners: {
            initialize: 'cleanFormatBeforePaste'
        }
    });
}

Ext.define('goc.view.reunion.FormOrdenDia', {
    extend : 'Ext.window.Window',
    xtype : 'formOrdenDia',

    width : '90%',
    maxHeight : 1000,
    modal : true,
    bodyPadding : 10,
    autoScroll : true,

    layout : {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [
        'goc.view.reunion.FormOrdenDiaController'
    ],
    controller : 'formOrdenDiaController',

    bbar : {
        defaultButtonUI : 'default',
        items : [
            '->', {
                xtype : 'button',
                bind : {
                    disabled : '{reunionCompletada}'
                },
                text : appI18N.reuniones.guardar,
                handler : 'onSaveRecord'
            }, {
                xtype : 'panel',
                html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cancelar + '</a>',
                listeners : {
                    render : function(component)
                    {
                        component.getEl().on('click', 'onCancel');
                    }
                }
            }
        ]
    },

    bind : {
        title : '{title}'
    },

    items : [
        {
            xtype : 'form',
            name : 'puntoOrdenDia',
            border : 0,
            layout : 'anchor',
            items : [
                {
                    xtype : 'fieldset',
                    title : appI18N.reuniones.informacion,
                    defaultType : 'textfield',
                    defaults : {
                        anchor : '100%'
                    },

                    items : formItems
                }
            ]
        }
    ],

    listeners : {
        afterLayout : 'afterRenderFormOrdenDia',
        close : 'onClose'
    }
});
