Ext.define('goc.view.reunion.FormReunionAcuerdosController',
{
    extend : 'Ext.app.ViewController',
    alias : 'controller.formReunionAcuerdosController',

    onLoad: function()
    {
        var vm = this.getViewModel();
        var asistentes = vm.get('asistentesStore');

        asistentes.load({
            callback: function (records, operation, success) {
                asistentes.getData().each(function (record) {
                    if(record.get('responsableActa'))
                    {
                        vm.set('responsableId', record.get('id'));
                    }
                });
            }
        });
    },

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();

        var grid = Ext.ComponentQuery.query('grid[name=reunion]')[0];
        grid.getSelectionModel().deselectAll();
        grid.getStore().reload();

        var gridHistorico = Ext.ComponentQuery.query('historicoReunionGrid')[0];
        if (gridHistorico)
        {
            gridHistorico.getStore().reload();
        }

        if (win)
        {
            win.destroy();
        }

        var gridOrdenDia = grid.up('panel').down('treepanel[name=ordenDia]');
        if (gridOrdenDia) gridOrdenDia.clearStore();
    },

    onSaveRecord : function(button, context)
    {
        var form = Ext.ComponentQuery.query('form[name=formReunionAcuerdos]')[0];

        if (form.isValid())
        {
            var data = form.getValues();
            data.estado = 'CERRADA_CON_FIRMA';

            Ext.Ajax.request(
            {
                url : appContext + '/rest/reuniones/' + data.id,
                method : 'PATCH',
                jsonData : data,
                timeout: 180000,
                success : function(response)
                {
                    this.onClose();
                },
                scope : this
            });
        }
    },

    afterRenderFormCerrarActa : function(windowFormCerrarActa)
    {
        var height = Ext.getBody().getViewSize().height;
        if (windowFormCerrarActa.getHeight() > height)
        {
            windowFormCerrarActa.setHeight(height - 30);
            windowFormCerrarActa.setPosition(windowFormCerrarActa.x, 15);
        }
    }
});
