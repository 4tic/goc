var ultimaPosicionDrag;

Ext.define('Override.tree.ViewDragZone',
{
    override : 'Ext.tree.ViewDragZone',

    onDrag : function(e)
    {
        ultimaPosicionDrag = e.getXY();
        this.view.fireEvent('drag', this.view, this, e);
        this.callParent([e]);
    }
});

Ext.define('goc.view.reunion.OrdenDiaGridController', {
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.ordenDiaGridController',
    reunionId : null,

    onLoad : function()
    {
    },

    onRefresh : function(reunionId)
    {
        this.reunionId = reunionId;

        var viewModel = this.getViewModel();

        this.getView().setDisabled(false);
        viewModel.getStore('puntosOrdenDiaTreeStore').load({
            url : appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia'
        });

        this.actualizaEstadoBotones();
    },

    actualizaEstadoBotones : function()
    {
        var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
        var record = reunionGrid.getView().getSelectionModel().getSelection()[0];

        var grid = this.getView();
        var botonAdd = grid.getDockedItems()[1].items.items[0];
        var botonBorrar = grid.getDockedItems()[1].items.items[2];
        botonBorrar.setDisabled(record.get('completada'));
        botonAdd.setDisabled(record.get('completada'));
    },

    onDelete : function()
    {
        var grid = this.getView();
        var records = grid.getView().getSelectionModel().getSelection();
        var reunionId = this.reunionId;

        if (records.length === 0)
        {
            return Ext.Msg.alert(appI18N.common.borradoRegistro, appI18N.reuniones.seleccionarParaBorrarPuntoOrdenDia);
        }

        if (records.length === 1 && records[0].phantom === true)
        {
            return grid.getStore().remove(records);
        }

        if (records.length > 0)
        {
            Ext.Msg.confirm(appI18N.common.borrar, appI18N.reuniones.preguntaBorrarRegistro, function(btn, text)
            {
                if (btn == 'yes')
                {
                    var store = grid.getStore();
                    Ext.Array.forEach(records, function(record)
                    {
                        if (record.parentNode)
                            record.parentNode.removeChild(record);

                        Ext.Ajax.request({
                            url : appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + record.get("id"),
                            method : 'DELETE',
                            failure : function()
                            {
                                store.load({
                                    url : appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia'
                                });
                            }
                        });
                    });
                }
            });
        }
    },

    bajaPuntoOrdenDia : function(puntoOrdenDiaId)
    {
        var grid = this.getView();
        Ext.Ajax.request({
            url : appContext + '/rest/reuniones/' + this.reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/bajar',
            method : 'PUT',
            success : function()
            {
                grid.getStore().reload();
            }
        });
    },

    muevePuntosOrdenDia : function(punto, nodoPadreDestinoId, hermanoConMenosOrdenId, crearNivel)
    {
        if (punto)
        {
            var grid = this.getView();
            Ext.Ajax.request({
                url : appContext + '/rest/reuniones/' + this.reunionId + '/puntosOrdenDia/' + punto + '/mover',
                method : 'PUT',
                jsonData : {
                    'idHermanoAnterior' : hermanoConMenosOrdenId,
                    'puntoOrdenDiaDestinoId' : nodoPadreDestinoId,
                    'crearNivel' : crearNivel
                },
                success : function()
                {
                    grid.getStore().reload();
                }
            });
        }
    },

    subePuntoOrdenDia : function(puntoOrdenDiaId)
    {
        var grid = this.getView();
        Ext.Ajax.request({
            url : appContext + '/rest/reuniones/' + this.reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/subir',
            method : 'PUT',
            success : function()
            {
                grid.getStore().reload();
            }
        });
    },

    onAdd : function()
    {
        this.createModalPuntoOrdenDia(null);
    },

    onAddFirstLevel : function()
    {
        this.createModalPuntoOrdenDia(null, true);
    },

    onEdit : function(grid, td, cellindex)
    {
        if (grid && grid.getHeaderCt)
        {
            var cell = grid.getHeaderCt().getHeaderAtIndex(cellindex);

            if (cell.getReference() === 'documentos')
            {
                return this.onAttachmentEdit();
            }

            if (cell.getReference() === 'acuerdos')
            {
                return this.onAttachmentAcuerdosEdit();
            }
        }

        grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record)
        {
            return Ext.Msg.alert(appI18N.common.edicionRegistro, appI18N.reuniones.seleccionarParaEditarPuntoOrdenDia);
        }

        this.createModalPuntoOrdenDia(record);
    },

    onAttachmentEdit : function()
    {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        if(record != null) {
            var reunionId = record.data._reunion.id;
            var puntoOrdenDiaId = record.id;
            var store = Ext.create('goc.store.PuntosOrdenDiaDocumentos');
            store.proxy.url = appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/documentos/';
            var viewport = this.getView().up('viewport');

            var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
            var reunion = reunionGrid.getView().getSelectionModel().getSelection()[0];



            this.modal = viewport.add({
                xtype: 'formOrdenDiaDocumentacion',
                viewModel: {
                    data: {
                        title: appI18N.reuniones.documentacionDelPunto + ': ' + record.get('titulo'),
                        puntoOrdenDiaId: record.get('id'),
                        reunionId: this.reunionId,
                        reunionCompletada: reunion.get('completada'),
                        store: store
                    }
                }
            });
            this.modal.show();
        } else {
            return Ext.Msg.alert(appI18N.reuniones.documentacion, appI18N.reuniones.seleccionarParaDocumentacion);
        }
    },

    onAttachmentAcuerdosEdit : function()
    {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        if(record != null){
            var reunionId = record.data._reunion.id;
            var puntoOrdenDiaId = record.id;
            var store = Ext.create('goc.store.PuntosOrdenDiaAcuerdos');
            store.proxy.url = appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia/' + puntoOrdenDiaId + '/acuerdos/';
            var viewport = this.getView().up('viewport');

            var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
            var reunion = reunionGrid.getView().getSelectionModel().getSelection()[0];

            this.modal = viewport.add({
                xtype: 'formOrdenDiaAcuerdos',
                viewModel: {
                    data: {
                        title: appI18N.reuniones.acuerdosDelPunto + ': ' + record.get('titulo'),
                        puntoOrdenDiaId: record.get('id'),
                        reunionId: this.reunionId,
                        reunionCompletada: reunion.get('completada'),
                        store: store
                    }
                }
            });

            this.modal.show();
        } else {
            return Ext.Msg.alert(appI18N.reuniones.acuerdos, appI18N.reuniones.seleccionarParaAcuerdosPuntoOrdenDia);
        }
    },

    onAttachmentEditDescriptores : function()
    {
        var view = this.getView().up('viewport');
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];
        var viewModel = this.getViewModel();

        var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
        var reunion = reunionGrid.getView().getSelectionModel().getSelection()[0];

        if (!record)
        {
            return Ext.Msg.alert(appI18N.reuniones.descriptoresYclaves, appI18N.reuniones.seleccionarParaDocumentacionPuntoOrdenDia);
        }

        this.modal = view.add({
            xtype : 'formDescriptoresOrdenDia',
            viewModel : {
                data : {
                    title : appI18N.reuniones.descriptoresYclaves + ': ' + record.get('titulo'),
                    puntoOrdenDiaId : record.get('id'),
                    reunionId : this.reunionId,
                    reunionCompletada : reunion.get('completada')
                },
                stores : {
                    descriptoresOrdenDiaStore : {
                        type : 'descriptoresOrdenDia'
                    },
                    descriptoresStore : {
                        type : 'descriptores'
                    },
                    clavesStore : {
                        type : 'claves'
                    }
                }
            }
        });
        this.modal.show();
    },

    getPuntoOrdenDiaModalDefinition : function(puntoOrdenDia, store, puntoOrdenDiaPrimerNivel)
    {
        store.getProxy().url = appContext + '/rest/reuniones/' + this.reunionId + '/puntosOrdenDia';

        var reunionGrid = Ext.ComponentQuery.query("reunionGrid")[0];
        var reunion = reunionGrid.getView().getSelectionModel().getSelection()[0];

        return {
            xtype : 'formOrdenDia',
            viewModel : {
                data : {
                    title : puntoOrdenDia ? appI18N.reuniones.edicion + ': ' + puntoOrdenDia.get('titulo') : appI18N.reuniones.nuevoPunto,
                    id : puntoOrdenDia ? puntoOrdenDia.get('id') : undefined,
                    reunionId : this.reunionId,
                    puntoOrdenDia : puntoOrdenDia || {
                        votoPublico: true,
                        type : 'goc.model.PuntoOrdenDia',
                        reunionId : this.reunionId,
                        puntoOrdenDiaPrimerNivel: puntoOrdenDiaPrimerNivel,
                        create : true
                    },
                    reunionCompletada : reunion.get('completada'),
                    reunionHasVotacion: reunion.get('hasVotacion'),
                    store : store
                },
                stores : {
                    descriptoresOrdenDiaStore : {
                        type : 'descriptoresOrdenDia'
                    },
                    descriptoresStore : {
                        type : 'descriptores'
                    },
                    clavesStore : {
                        type : 'claves'
                    }
                }
            }
        };
    },

    createModalPuntoOrdenDia : function(record, puntoOrdenDiaPrimerNivel)
    {
        // debugger;
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('puntosOrdenDiaStore');
        var viewport = this.getView().up('viewport');

        var modalDefinition = this.getPuntoOrdenDiaModalDefinition(record, store, puntoOrdenDiaPrimerNivel);
        this.modal = viewport.add(modalDefinition);
        this.modal.show();
    },

    onContextMenuClic : function(view, record, htmlitem, index, event)
    {
        event.stopEvent();
        this.getView().contextMenu.showAt(event.getXY());
    },

    onDropPuntoOrdenDia : function(node, data, overModel, dropPosition)
    {
        var view        = this.getView(),
            menuClicked = false;
        if (data && data.records)
        {
            var recordAMover = (data.records.length > 0) ? data.records[0] : null;
            var contextMenuDrop = Ext.create('Ext.menu.Menu', {
                $initParent : view,
                listeners : {
                    hide : function(menu)
                    {
                        if (!menuClicked)
                        {
                            view.getStore().reload();
                        }
                        menu.destroy();
                    },
                    click : function()
                    {
                        menuClicked = true;
                        this.hide();
                    }
                },
                items : [
                    {
                        iconCls : 'ext ext-sw-handle',
                        text : appI18N.reuniones.anyadirComoHijoDrag,
                        handler : 'onMoveLikeSon',
                        recordAMover : recordAMover,
                        hideOnClick : false,
                        dropPosition : dropPosition
                    }, {
                        iconCls : 'ext ext-unpin',
                        text : appI18N.reuniones.moverAquiDrag,
                        handler : 'onOnlyMove',
                        hideOnClick : false,
                        recordAMover : recordAMover
                    }
                ]
            });
            contextMenuDrop.showAt(ultimaPosicionDrag);
        }
    },

    onMoveLikeSon : function(item)
    {
        var recordAMover = item.recordAMover,
            dropPosition = item.dropPosition;
        if (recordAMover)
        {
            var detallePuntoAMover = this.getDetallesPuntoAMoverComoHijo(recordAMover, dropPosition);
            this.muevePuntosOrdenDia(recordAMover.id, detallePuntoAMover.parentDestinoId, detallePuntoAMover.hermanoConMenosOrdenId, true);
        }
    },

    onOnlyMove : function(item)
    {
        var recordAMover = item.recordAMover;
        if (recordAMover)
        {
            var parentDestinoId = recordAMover.get('parentId');
            var hermanoConMenosOrdenId = (recordAMover.previousSibling) ? recordAMover.previousSibling.get('id') : null;
            this.muevePuntosOrdenDia(recordAMover.id, parentDestinoId, hermanoConMenosOrdenId, false);
        }
    },

    getDetallesPuntoAMoverComoHijo : function(recordAMover, dropPosition)
    {
        var parentDestinoId = recordAMover.get('parentId');
        parentDestinoId = (parentDestinoId == 'root') ? null : parentDestinoId;
        var hermanoConMenosOrdenId;
        if (dropPosition == "before")
        {
            hermanoConMenosOrdenId = (recordAMover.nextSibling) ? recordAMover.nextSibling.get('id') : null;
        }
        else
        {
            hermanoConMenosOrdenId = (recordAMover.previousSibling) ? recordAMover.previousSibling.get('id') : null;
        }
        return {
            parentDestinoId : parentDestinoId,
            hermanoConMenosOrdenId : hermanoConMenosOrdenId
        };
    }
});
