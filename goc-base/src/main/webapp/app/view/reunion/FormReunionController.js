Ext.define('goc.view.reunion.FormReunionController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formReunionController',

    onLoad: function ()
    {
        var vm = this.getViewModel(),
            organosAsistentesStore = vm.get('organosStore'),
            me = this,
            reunion = vm.get('reunion');

        var TEMPLATE_ORGANO = '{organo}';

        if (exprMeetingTitle && reunion.create) {
            vm.set('reunion.asunto', exprMeetingTitle);
            vm.set('reunion.asuntoAlternativo', (exprMeetingTitleAlternative) ? exprMeetingTitleAlternative : '');

            var onOrganoAdded = function (store, records, index) {
                var organo = store.getAt(index).get('nombre'),
                    fieldAsunto = me.getFieldAsunto(),
                    fieldAsuntoAlt = me.getFieldAsuntoAlternativo(),
                    nuevoNombre = organo,
                    nombreAnterior = null;

                if (index > 0)
                {
                    nombreAnterior = store.getAt(index - 1).get('nombre');
                    nuevoNombre = nombreAnterior + ' | ' + organo;
                }
                me.replaceTemplateInField(fieldAsunto, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);
                me.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);
            };

            for (var index = 0; index < organosAsistentesStore.getData().length; index++) {
                organosAsistentesStore = vm.get('organosStore');

                onOrganoAdded(organosAsistentesStore, organosAsistentesStore.getData(), index);
            }

            organosAsistentesStore.on("add", onOrganoAdded);

            organosAsistentesStore.on("remove", function (store, records, index) {
                var organo = records[0].get('nombre'),
                    fieldAsunto = me.getFieldAsunto(),
                    fieldAsuntoAlt = me.getFieldAsuntoAlternativo(),
                    nuevoNombre = '',
                    nombreAnterior = organo;

                if (me.esElUltimoOrganoParaBorrar(store)) {
                    nuevoNombre = TEMPLATE_ORGANO;
                } else if (me.hayAlmenosUnOrgano(store) && me.noEsElPrimerOrganoDeLaListaParaBorrar(index)) {
                    nombreAnterior = ' | ' + organo;
                } else {
                    nombreAnterior = organo + ' | ';
                }
                me.replaceTemplateInField(fieldAsunto, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);
                me.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE_ORGANO, nuevoNombre, nombreAnterior);

            });
        }

        if (!reunion.create)
        {
            this.ocultarCheckRevisarActa();
        }
    },

    ocultarCheckRevisarActa: function () {
          this.getView().lookupReference('revisarActa').setVisible(false);
    },

    esElUltimoOrganoParaBorrar: function(store){
        return store.count() === 0;
    },

    hayAlmenosUnOrgano: function (store) {
        return store.count() > 0;
    },

    noEsElPrimerOrganoDeLaListaParaBorrar: function (index) {
        return index > 0;
    },

    onClose: function ()
    {
        var win = Ext.WindowManager.getActive();
        var grid = Ext.ComponentQuery.query('grid[name=reunion]')[0];
        grid.getStore().reload();

        if (win)
        {
            win.destroy();
        }
    },

    onBorrarAsistenteReunion: function (record)
    {
        var vm = this.getViewModel();
        var store = vm.get('organosStore');
        store.remove(record);
    },

    getStoreDeMiembros: function (organoId, externo)
    {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStores');
        return miembrosStores[organoId + '_' + externo];
    },

    creaStoreDeMiembros: function (organoId, externo)
    {
        var vm = this.getViewModel();
        var miembrosStores = vm.get('miembrosStores');
        miembrosStores[organoId + '_' + externo] = Ext.create('goc.store.OrganoReunionMiembros');
    },

    onDetalleAsistentesReunion: function (organo, reunionId)
    {
        var vm = this.getViewModel();
        var reunion = vm.get('reunion');
        var view = this.getView();
        var remoteLoad = false;
        if (!this.getStoreDeMiembros(organo.get('id'), organo.get('externo')))
        {
            this.creaStoreDeMiembros(organo.get('id'), organo.get('externo'));
            remoteLoad = true;
        }

        var store = this.getStoreDeMiembros(organo.get('id'), organo.get('externo'));

        var formData = view.down('form').getValues();

        var modalDefinition = {
            xtype: 'formReunionMiembros',
            viewModel: {
                data: {
                    reunionId: reunionId,
                    reunionCompletada: reunionId ? reunion.get('completada') : false,
                    admiteSuplencia: formData.admiteSuplencia ? true : false,
                    admiteDelegacionVoto: formData.admiteDelegacionVoto ? true : false,
                    admiteComentarios: formData.admiteComentarios ? true : false,
                    organoId: organo.get('id'),
                    externo: organo.get('externo'),
                    store: store,
                    remoteLoad: remoteLoad
                }
            }
        };
        this.modal = view.add(modalDefinition);
        this.modal.show();
    },

    saveOrganosYInvitados: function (reunionId, organos, invitados, onClose, puntoActaAnterior)
    {
        var me = this;
        var view = this.getView();

        var datosOrganos = this.buildDatosOrganos(organos);
        var datosInvitados = this.buildDatosInvitados(invitados);

        Ext.Ajax.request({
            url: appContext + '/rest/reuniones/' + reunionId + '/organos',
            method: 'PUT',
            jsonData: {organos: datosOrganos},
            success: function ()
            {
                if(puntoActaAnterior)
                {
                    me.anyadirPuntoRevisionActaAnterior(reunionId, onClose);
                }

                Ext.Ajax.request({
                    url: appContext + '/rest/reuniones/' + reunionId + '/invitados',
                    method: 'PUT',
                    jsonData: {invitados: datosInvitados},
                    success: function ()
                    {
                        onClose();
                    },
                    failure: function ()
                    {
                        view.setLoading(false);
                    }
                });
            },
            failure: function ()
            {
                view.setLoading(false);
            }
        });
    },

    anyadirPuntoRevisionActaAnterior: function (reunionId, onClose) {
        var view = this.getView();

        Ext.Ajax.request({
            url: appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia/revisionactaanterior',
            method: 'POST',
            success: function ()
            {
                onClose();
            },
            failure: function ()
            {
                view.setLoading(false);
            }
        });
    },

    buildDatosInvitados: function (invitados)
    {
        var datosInvitados = [];

        invitados.each(function (record)
        {
            datosInvitados.push({
                personaId: record.get('personaId'),
                personaNombre: record.get('personaNombre'),
                personaEmail: record.get('personaEmail'),
                motivoInvitacion: record.get('motivoInvitacion')
            });
        });

        return datosInvitados;
    },

    buildDatosOrganos: function (organos)
    {
        var datosOrganos = [];
        var self = this;
        organos.each(function (record)
        {
            var miembros = [];
            var miembrosStore = self.getStoreDeMiembros(record.get('id'), record.get('externo'));
            if (miembrosStore)
            {
                miembrosStore.getData().each(function (record)
                {
                    miembros.push({
                        id: record.get('id'),
                        email: record.get('email'),
                        nombre: record.get('nombre'),
                        cargo: record.get('cargo'),
                        asistencia: record.get('asistencia'),
                        suplenteId: record.get('suplenteId'),
                        suplenteNombre: record.get('suplenteNombre'),
                        suplenteEmail: record.get('suplenteEmail'),
                        justificaAusencia: record.get('justificaAusencia'),
                        presideVotacion: record.get('presideVotacion'),
                        firmante: record.get('firmante')
                    });
                });
            }

            var data = {
                id: record.get('id'),
                externo: record.get('externo'),
                miembros: miembros
            };

            datosOrganos.push(data);
        });

        return datosOrganos;
    },

    checkHasVotacionActivated: function (data) {
        return data.hasVotacion !== undefined && data.hasVotacion;
    },

    onSaveRecord: function (button, context)
    {
        var form = Ext.ComponentQuery.query('form[name=reunion]')[0];
        var gridStore = Ext.ComponentQuery.query('grid[name=reunion]')[0].getStore();
        if (form.isValid())
        {
            var data = form.getValues();
            var ref = this;

            if (Ext.Date.parse(data.fecha + ' ' + data.hora, 'd/m/Y H:i') < new Date())
            {
                Ext.Msg.confirm(appI18N.reuniones.fechaAnterior, appI18N.reuniones.fechaAnteriorAActual, function (result)
                {
                    if (result === 'yes')
                    {
                        ref.saveReunion();
                    }
                });
            } else
            {
                ref.saveReunion();
                gridStore.reload();
            }
        }
    },

    areWeDuplicatingReunion: function(record)
    {
        return record && record.data && record.get('idReunionOriginal') != null && record.get('idReunionOriginal') !== '';
    },

    duplicaReunion: function(record, organosStore, invitadosStore)
    {
        var me = this;
        var view = this.getView();
        Ext.Ajax.request({
            url: appContext + '/rest/reuniones/' + record.get('idReunionOriginal') + '/duplica',
            method: 'POST',
            jsonData: record.data,
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response && response.success && response.data && response.data.id)
                    me.saveOrganosYInvitados(response.data.id, organosStore.getData(), invitadosStore.getData(), me.onClose);
                else
                {
                    me.onClose();
                }
            },
            failure: function()
            {
                view.setLoading(false);
            }
        });
    },

    saveReunion: function (record, data, store)
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var form = Ext.ComponentQuery.query('form[name=reunion]')[0];

        var organosStore = vm.get('organosStore');
        var invitadosStore = vm.get('reunionInvitadosStore');

        record = vm.get('reunion');
        data = form.getValues();
        store = vm.get('store');

        view.setLoading(appI18N.common.loading);

        if (record.create !== true)
        {
            record.set('fecha', Ext.Date.parseDate(data.fecha + ' ' + data.hora, 'd/m/Y H:i'));
            record.set('fechaSegundaConvocatoria', Ext.Date.parseDate(data.fecha + ' ' + data.horaSegundaConvocatoria, 'd/m/Y H:i'));

            if (this.areWeDuplicatingReunion(record))
            {
                return this.duplicaReunion(record, organosStore, invitadosStore);
            } else
            {
                return record.save({
                    success: function ()
                    {
                        this.saveOrganosYInvitados(data.id, organosStore.getData(), invitadosStore.getData(), this.onClose);
                    },
                    failure: function ()
                    {
                        view.setLoading(false);
                    },
                    scope: this
                });
            }
        }

        record.fecha = Ext.Date.parseDate(data.fecha + ' ' + data.hora, 'd/m/Y H:i');
        record.fechaSegundaConvocatoria = Ext.Date.parseDate(data.fecha + ' ' + data.horaSegundaConvocatoria, 'd/m/Y H:i');

        store.add(record);
        store.sync({
            success: function ()
            {
                this.saveOrganosYInvitados(record.id, organosStore.getData(), invitadosStore.getData(), this.onClose, record.revisarActa);
            },
            failure: function ()
            {
                view.setLoading(false);
            },
            scope: this
        });
    },

    afterRenderFormReunion: function (windowFormReunion)
    {
        var height = Ext.getBody().getViewSize().height;
        if (windowFormReunion.getHeight() > height)
        {
            windowFormReunion.setHeight(height - 30);
            windowFormReunion.setPosition(windowFormReunion.x, 15);
        }
    },

    onDateChanged: function (obj, newValue, oldValue)
    {
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        var TEMPLATE = '{fecha}';
        var strFecha = this.formatDate(newValue, 'd/m/Y', TEMPLATE);
        var strFechaOld = this.formatDate(oldValue, 'd/m/Y', TEMPLATE);

        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, strFecha, strFechaOld);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, strFecha, strFechaOld);
        }
    },

    onTimeChanged: function (obj, newValue, oldValue)
    {
        var TEMPLATE = '{hora}';
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        var strHora = this.formatDate(newValue, 'H:i', TEMPLATE);
        var strHoraOld = this.formatDate(oldValue, 'H:i', TEMPLATE);


        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, strHora, strHoraOld);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, strHora, strHoraOld);
        }
    },

    onDuracionChanged: function (obj, newValue, oldValue)
    {
        var TEMPLATE = '{duracion}';
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        newValue = (!newValue || newValue === '') ? TEMPLATE : newValue;

        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, newValue, oldValue);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, newValue, oldValue);
        }
    },

    onNumSessionChanged: function (obj, newValue, oldValue)
    {
        var TEMPLATE = '{numSesion}';
        var fieldAsunto = this.getFieldAsunto();
        var fieldAsuntoAlt = this.getFieldAsuntoAlternativo();
        newValue = (!newValue || newValue === '') ? TEMPLATE : newValue;

        if (fieldAsunto)
        {
            this.replaceTemplateInField(fieldAsunto, TEMPLATE, newValue, oldValue);
            this.replaceTemplateInField(fieldAsuntoAlt, TEMPLATE, newValue, oldValue);
        }
    },

    getFieldAsunto: function ()
    {
        return Ext.ComponentQuery.query('textfield[name=asunto]')[0];
    },

    getFieldAsuntoAlternativo: function ()
    {
        return (isMultilanguageApplication()) ? Ext.ComponentQuery.query('textfield[name=asuntoAlternativo]')[0] : null;
    },

    formatDate: function (date, format, returnIfError)
    {
        var strDate = (date && format) ? Ext.Date.format(date, format) : '';
        return (strDate && strDate !== '') ? strDate : returnIfError;
    },

    replaceTemplateInField: function (field, template, newValue, oldValue)
    {
        if (field)
        {
            if (field.value.indexOf(template) != -1)
                field.setValue(field.value.replace(template, newValue));
            else if (field.value.indexOf(oldValue) != -1)
                field.setValue(field.value.replace(oldValue, newValue));
        }
    }
});
