Ext.define('goc.view.reunion.FormReunionMiembros', {
    extend: 'Ext.window.Window',
    xtype: 'formReunionMiembros',
    width: '90%',
    height: '90%',
    modal: true,
    bodyPadding: 10,
    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },
    title: appI18N.reuniones.gestionAsistentes,

    requires: [
        'goc.view.reunion.FormReunionMiembrosController'
    ],
    controller: 'formReunionMiembrosController',
    viewModel: {
        type: 'reunionViewModel'
    },
    tbar: [
        {
            xtype: 'button',
            iconCls: 'fa fa-plus',
            text: appI18N.reuniones.anadirSuplente,
            handler: 'onAddSuplente',
            bind: {
                disabled: '{!admiteSuplencia || selectedMiembro.delegadoVotoId || !selectedMiembro.asistencia || selectedMiembro.asistencia === "false"}'
            }
        },
        {
            xtype: 'button',
            iconCls: 'fa fa-edit',
            text: appI18N.reuniones.borrarSuplente,
            handler: 'onRemoveSuplente',
            bind: {
                disabled: '{!admiteSuplencia}'
            }
        }
    ],

    items: [
        {
            xtype: 'textfield',
            emptyText: appI18N.reuniones.buscaAsistente,
            name: 'searchMiembro',
            listeners: {
                change: 'onSearchMiembro'
            }
        },
        {
            xtype: 'grid',
            plugins: [
                {
                    ptype: 'rowediting',
                    clicksToEdit: 2
                }
            ],
            requires: ['Ext.grid.plugin.RowEditing'],
            flex: 1,
            height: 300,
            scrollable: true,
            margin: '5 0 5 0',
            viewConfig: {
                emptyText: appI18N.reuniones.sinAsistentes,
                markDirty: false
            },
            bind: {
                store: '{store}',
                selection: '{selectedMiembro}'
            },
            listeners: {
                'beforeedit' : function (editor, e) {
                    if (e.record.get('suplenteId'))
                        editor.editor.form.findField('asistencia').disable();
                    else
                        editor.editor.form.findField('asistencia').enable();
                }
            },
            columns: [
                {
                    text: appI18N.common.nombre,
                    dataIndex: 'nombre',
                    flex: 2
                },
                {
                    text: appI18N.miembros.cargo,
                    dataIndex: 'cargoNombre',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.correo,
                    dataIndex: 'email',
                    flex: 1
                },
                {
                    dataIndex: 'suplenteId',
                    hidden: true
                },
                {
                    text: appI18N.reuniones.suplente,
                    dataIndex: 'suplenteNombre',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.suplenteEmail,
                    dataIndex: 'suplenteEmail',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.delegacionVoto,
                    dataIndex: 'delegadoVotoNombre',
                    flex: 1
                },
                {
                    text: appI18N.reuniones.delegacionVotoEmail,
                    dataIndex: 'delegadoVotoEmail',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    text: appI18N.reuniones.asistencia,
                    dataIndex: 'asistencia',
                    bind: {
                        disabled: '{reunionCompletada}'
                    },
                    editor: {
                        xtype: 'combobox',
                        store: new Ext.data.ArrayStore({
                            fields: ['id', 'nombre'],
                            data: [['true', appI18N.common.si], ['false', appI18N.common.no]]
                        }),
                        allowBlank: true,
                        triggerAction: 'all',
                        queryMode: 'local',
                        displayField: 'nombre',
                        emptyText: appI18N.common.sinConfirmar,
                        valueField: 'id',
                        forceSelect: true
                    },
                    renderer: function (value, metadata, rec) {
                        return value === 'true' ? appI18N.common.si : (value === 'false' ? appI18N.common.no : appI18N.common.sinConfirmar);
                    }
                },
                {
                    text: appI18N.reuniones.justificaAusencia,
                    dataIndex: 'justificaAusencia',
                    editor: {
                        field: {
                            xtype: 'checkbox',
                            allowBlank: false
                        }
                    },
                    renderer: function (value, meta, rec) {
                        return value ? 'Sí' : 'No';
                    }
                },
                {
                    text: appI18N.reuniones.firmante,
                    dataIndex: 'firmante',
                    editor: {
                        field: {
                            xtype: 'checkbox',
                            allowBlank: false
                        }
                    },
                    renderer: function (value, meta, rec) {
                        return value ? 'Sí' : 'No';
                    }
                },
                {
                    text: appI18N.reuniones.votante,
                    dataIndex: 'votante',
                    renderer: function (value, meta, rec) {
                        return value ? 'Sí' : 'No';
                    }
                },
                {
                    text: appI18N.reuniones.presideVotacion,
                    dataIndex: 'presideVotacion',
                    editor: {
                        field: {
                            xtype: 'checkbox',
                            allowBlank: false
                        }
                    },
                    renderer: function (value, meta, rec) {
                        return value ? 'Sí' : 'No';
                    }
                }
            ]
        }
    ],

    bbar: {
        defaultButtonUI: 'default',
        items: [
            '->', {
                xtype: 'button',
                text: appI18N.common.aceptar,
                handler: 'onClose',
                bind: {
                    disabled: '{reunionCompletada}'
                }
            }, {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cancelar + '</a>',
                listeners: {
                    render: function (component) {
                        component.getEl().on('click', 'onClose');
                    }
                }
            }
        ]
    },

    listeners: {
        show: 'onLoad',
        onSave: 'onSave',
        onAddSuplente: 'onAddSuplente',
        onRemoveSuplente: 'onRemoveSuplente'
    }
});
