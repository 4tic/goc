Ext.define('goc.view.common.LookupWindowCargos',
{
    extend : 'goc.view.common.LookupWindowPersonas',

    alias : 'widget.lookupWindow',
    appPrefix : '',
    bean : 'base',
    lastQuery : '',
    queryField : '',
    formularioBusqueda : '',
    gridBusqueda : '',
    botonBuscar : '',
    botonCancelar : '',
    title : appI18N ? appI18N.common.buscarRegistros : 'Cercar registres',
    layout : 'border',
    modal : true,
    hidden : true,
    width : '80%',
    height : '80%',
    closeAction : 'hide',
    clearAfterSearch : true,

    buildGridBusqueda : function()
    {
        var ref = this;

        var resultColumnList = [
            {
                header : 'Id',
                flex: 1,
                dataIndex : 'id',
                hidden : true
            },
            {
                header : appI18N ? appI18N.cargos.codigo : 'Codi',
                flex: 1,
                dataIndex : 'codigo'
            },
            {
                header : appI18N ? appI18N.cargos.nombre : 'Nom',
                flex: 3,
                dataIndex : 'nombre'
            }
        ];

        this.gridBusqueda = Ext.create('Ext.grid.Panel',
        {
            region : 'center',
            flex : 1,
            frame : true,
            loadMask : true,
            store : Ext.create('Ext.data.Store',
            {
                model : 'goc.model.Cargo',
                autoSync : false,

                proxy : {
                    type : 'ajax',
                    url : appContext + '/rest/cargos',

                    reader : {
                        type : 'json',
                        rootProperty : 'data'
                    }
                },
                listeners : {
                    load : function(store, records, successful, eOpts)
                    {
                        if (successful && ref.gridBusqueda.store.data.length === 0)
                        {
                            Ext.Msg.alert(appI18N ? appI18N.common.aviso : "Aviso",
                            appI18N ? appI18N.common.sinResultados : "La búsqueda realitzada no ha produït cap resultat");
                        }
                    }
                }
            }),

            columns : resultColumnList,
            forceFit : true,
            listeners : {
                celldblclick : function(grid, td, cellindex, record)
                {
                    ref.botonSeleccionar.handler.call(ref.botonSeleccionar.scope);
                }
            },
            buttons : [this.botonSeleccionar, this.botonCancelar]

        });
    }
});