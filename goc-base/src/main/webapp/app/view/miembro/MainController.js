Ext.define('goc.view.miembro.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.miembroMainController',
    
    onLoad: function() {
        var viewModel = this.getViewModel();
        viewModel.getStore('organosStore').load({
            url: appContext + '/rest/organos/activos/usuario'
        });
    },

    onOrganoSelected: function(combo, record) {
        var view = this.getView();

        var grid = view.down('grid');
        grid.setHidden(false);
        grid.getStore().getProxy().setExtraParams({
           "organoId": record.get('id'),
           "externo":  record.get('externo')
        });
        grid.getStore().load();

        this.getViewModel().set('organoId', record.get('id'));
        this.getViewModel().set('ordenado', record.get('ordenado'));

        this.getViewModel().getStore('cargosDisponiblesStore').load({
            url: appContext + '/rest/organos/cargos/disponibles',
            params : {
                organoId : record.get('id')
            }
        });

        var toolbar = grid.down("toolbar");
        toolbar.items.each(function(button) {
            button.setDisabled(record.get("externo") === "true");
        });
    }
});
