Ext.define('goc.view.miembro.FormMiembroGrupoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formMiembroGrupoController',

    asignarGrupo: function ()
    {
        var form = this.getView().down('form');
        if(form.isValid())
        {
            var viewModel = this.getViewModel();
            var me = this;

            Ext.Ajax.request({
                url: appContext + '/rest/miembros/asignargrupo',
                method: 'PUT',
                jsonData: viewModel.get('grupo'),
                success: function () {
                    me.onClose();
                }
            });
        }
    },

    onClose: function ()
    {
        var win = Ext.WindowManager.getActive();
        var viewModel = this.getViewModel();
        var store = viewModel.get('store');
        store.reload();

        if (win)
        {
            win.destroy();
        }
    }
});