Ext.define('goc.view.miembro.GridController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.miembroGridController',

        organoId: null,

        LockOnStart: function () {
            if (this.getViewModel().get('ordenado') === true) {
                this.onLockGrupo();
            }
        },

        onAdd: function () {
            var grid = this.getView();
            var mainPanel = grid.up('miembroMainPanel');
            var combo = mainPanel.down("combobox");
            var organo = combo.getSelection();
            var rec = Ext.create('goc.model.Miembro', {
                organoId: organo.get('id')
            });

            var window = Ext.create('goc.view.common.LookupWindowPersonas',
                {
                    appPrefix: 'goc',
                    title: appI18N.miembros.anyadirMiembro
                });

            window.show();

            window.on('LookoupWindowClickSeleccion', function (res) {
                rec.set('personaId', res.get('id'));
                rec.set('nombre', res.get('nombre'));
                rec.set('email', res.get('email'));
                rec.set('votante', true);

                grid.getStore().insert(grid.store.data.items.length, rec);
                var editor = grid.plugins[0];
                editor.cancelEdit();
                editor.startEdit(rec, 4);
            });
        },

        onEdit: function () {
            var grid = this.getView();
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            if (!selection) {
                return Ext.Msg.alert(appI18N.common.edicionRegistro || "Edició de registre", appI18N.common.seleccionarParaEditarRegistro || "Cal seleccionar un registre per a poder fer l'edició");
            }

            var editor = grid.plugins[0];
            editor.cancelEdit();
            editor.startEdit(selection, 4);
        },

        decideRowIsEditable: function (editor, context) {
            if (context.column.name === "bajar" || context.column.name === "subir"){
                return false;
            }
            var grid = this.getView();
            var mainPanel = grid.up('miembroMainPanel');
            var combo = mainPanel.down("combobox");
            var organo = combo.getSelection();

            if (organo.get('externo') === 'true') {
                return false;
            }

            return true;
        },

        onDelete : function() {
            this.superclass.onDelete.call(this, true);
        },

        onEditComplete : function(editor, context)
        {
            this.superclass.onEditComplete.call(this, editor, context, true);
        },

        subirMiembro: function (miembroId) {
            var grid = this.getView();
            Ext.Ajax.request({
                url: appContext + '/rest/miembros/' + miembroId + '/subir',
                method: 'PUT',
                success: function () {
                    grid.getStore().reload();
                }
            });
        },

        bajarMiembro: function (miembroId) {
            var grid = this.getView();
            Ext.Ajax.request({
                url: appContext + '/rest/miembros/' + miembroId + '/bajar',
                method: 'PUT',
                success: function () {
                    grid.getStore().reload();
                }
            });
        },

        onAsignarGrupo: function () {
            if (this.getIdsSeleccionados().length > 0) {
                var viewport = this.getView().up('viewport');
                var ref = this;

                var modalDefinition = ref.getFormModalDefinition();
                ref.modal = viewport.add(modalDefinition);
                ref.modal.show();
            } else {
                Ext.Msg.alert("Error", appI18N.miembros.seleccionarError);
            }
        },

        onDesagrupar: function () {
            if (this.getIdsSeleccionados().length > 0) {
                var ids = this.getIdsSeleccionados();
                var grid = this.getView();
                Ext.Ajax.request({
                    url: appContext + '/rest/miembros/desagrupar',
                    method: 'PUT',
                    jsonData: {ids: ids},
                    success: function () {
                        grid.getStore().reload();
                    }
                });
            } else {
                Ext.Msg.alert("Error", appI18N.miembros.seleccionarError);
            }
        },

        getFormModalDefinition: function () {
            var ids = this.getIdsSeleccionados();
            var grid = this.getView();

            return {
                xtype: 'formMiembroGrupo',
                viewModel: {
                    data: {
                        store: grid.getStore(),
                        grupo: {
                            ids: ids
                        }
                    }
                }
            };
        },

        getIdsSeleccionados: function () {
            var grid = this.getView();
            var recordsSeleccionados = grid.getSelection();
            return recordsSeleccionados.map(function (record) {
                return record.get('id');
            });
        },

        onGroupClick: function (view, element, grupo, event) {
            if (grupo) {
                var action = event.target.getAttribute('data-action');
                if (action == 'subir-grupo') {
                    this.subirGrupo(grupo, this.getViewModel().get('organoId'));
                } else if (action == 'bajar-grupo') {
                    this.bajarGrupo(grupo, this.getViewModel().get('organoId'));
                }
            }
        },

        subirGrupo: function (grupo, organoId) {
            var grid = this.getView();
            Ext.Ajax.request({
                url: appContext + '/rest/miembros/subirgrupo/' + grupo + '?organoId=' + organoId,
                method: 'PUT',
                success: function () {
                    grid.getStore().reload();
                }
            });
        },

        bajarGrupo: function (grupo, organoId) {
            var grid = this.getView();
            Ext.Ajax.request({
                url: appContext + '/rest/miembros/bajargrupo/' + grupo + '?organoId=' + organoId,
                method: 'PUT',
                success: function () {
                    grid.getStore().reload();
                }
            });
        },

        onBloquearOrdenacion: function () {
            var me = this;
            var organoId = this.getViewModel().get('organoId');

            Ext.Ajax.request({
                url: appContext + '/rest/organos/' + organoId + '/bloquearordenacion',
                method: 'PUT',
                success: function () {
                    me.getViewModel().set('ordenado', true);
                }
            });
        },

        onDesbloquearOrdenacion: function () {
            var me = this;
            var organoId = this.getViewModel().get('organoId');

            Ext.Ajax.request({
                url: appContext + '/rest/organos/' + organoId + '/desbloquearordenacion',
                method: 'PUT',
                success: function () {
                    me.getViewModel().set('ordenado', false);
                }
            });
        },

        onLockGrupo: function () {
            for (var i = 0; i < Ext.query('.flecha-bajar-grupo').length; i++) {
                Ext.query('.flecha-subir-grupo')[i].style = 'display:none';
                Ext.query('.flecha-bajar-grupo')[i].style = 'display:none';
            }
        }
    });
