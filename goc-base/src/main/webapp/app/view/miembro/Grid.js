var miembroGridColumns = [];

miembroGridColumns.push({
        xtype : 'actioncolumn',
        title : appI18N.reuniones.posicion,
        align : 'right',
        width : 30,
        name: 'subir',
        bind: {
            hidden: '{ordenado}'
        },
        items: [
        {
            iconCls : 'x-fa fa-arrow-up',
            tooltip : appI18N.reuniones.subir,
            handler : function(grid, index)
            {
                var rec = grid.getStore().getAt(index);
                var miembroId = rec.get('id');
                grid.up('grid').fireEvent('subeMiembro', miembroId);
            }
        }
    ]},
    {
        xtype : 'actioncolumn',
        title : appI18N.reuniones.posicion,
        align : 'right',
        width : 30,
        name: 'bajar',
        bind: {
            hidden: '{ordenado}'
        },
        items: [ {
            iconCls : 'x-fa fa-arrow-down',
            tooltip : appI18N.reuniones.bajar,
            handler : function(grid, index)
            {
                var rec = grid.getStore().getAt(index);
                var miembroId = rec.get('id');
                grid.up('grid').fireEvent('bajaMiembro', miembroId);
            }
        }]
    });

miembroGridColumns.push({
    text: appI18N.reuniones.posicion,
    dataIndex: 'orden',
    flex: 0.5,
    sortable:false,
    editor: {
        field: {
            allowBlank: true,
            minValue: 1,
            xtype: 'numberfield'
        }
    }
});

miembroGridColumns.push({
    text: appI18N.miembros.nombre,
    dataIndex: 'nombre',
    flex: 2,
    sortable:false
});

miembroGridColumns.push({
    text: appI18N.miembros.correo,
    dataIndex: 'email',
    flex: 1,
    editor: {
        field: {
            allowBlank: false,
            xtype: 'textfield',
            vtype: 'email'
        }
    }
});

miembroGridColumns.push({
    text: appI18N.miembros.cargo,
    dataIndex: 'cargoId',
    flex: 1,
    renderer: function(value, cell) {
        var viewModel = this.getView().up('miembroMainPanel').getViewModel();
        var cargosStore = viewModel.getStore('cargosStore');

        var record = cargosStore.findRecord('id', value, 0, false, false, true);
        return record ? record.get(appLang === alternativeLanguage ? 'nombreAlternativo' : 'nombre') : '';
    },
    editor: {
        xtype: 'combobox',
        allowBlank: mostrarDescripcion,
        emptyText: appI18N.cargos.seleccionaUnCargo,
        bind: {
            store: '{cargosDisponiblesStore}'
        },
        triggerAction: 'all',
        queryMode: 'local',
        displayField: (appLang === alternativeLanguage ? 'nombreAlternativo' : 'nombre'),
        valueField: 'id',
        editable: false
    }
});


if(mostrarDescripcion)
{
    miembroGridColumns.push({
        text: getMultiLangLabel(appI18N.miembros.descripcion, mainLanguage),
        dataIndex: 'descripcion',
        flex: 1,
        editor: {
            field: {
                allowBlank: true
            }
        }
    });

    if (isMultilanguageApplication()) {
        miembroGridColumns.push({
            text: getMultiLangLabel(appI18N.miembros.descripcionAlternativa, alternativeLanguage),
            dataIndex: 'descripcionAlternativa',
            flex: 1,
            editor: {
                field: {
                    allowBlank: true
                }
            }
        });

    }
}

miembroGridColumns.push({
    text: appI18N.miembros.grupo,
    name: 'grupo',
    dataIndex: 'grupo',
    flex: 1,
    hidden: true
});

miembroGridColumns.push({
    text: appI18N.reuniones.firmante,
    dataIndex: 'firmante',
    flex: 0.5,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
        },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

miembroGridColumns.push({
    text: appI18N.reuniones.votante,
    dataIndex: 'votante',
    flex: 0.5,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

miembroGridColumns.push({
    text: appI18N.reuniones.presideVotacion,
    dataIndex: 'presideVotacion',
    flex: 0.5,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    },
    editor : {
        xtype : 'checkbox',
        allowBlank : false
    }
});

Ext.define('goc.view.miembro.Grid', {
    extend: 'Ext.ux.uji.grid.Panel',
    alias: 'widget.miembroGrid',

    requires: [
        'goc.view.miembro.GridController',
        'Ext.grid.feature.Grouping',
        'goc.view.miembro.FormMiembroGrupo'
    ],

    features: [{
        ftype: 'grouping',
        startCollapsed: false,
        collapsible: false,
        groupHeaderTpl: '<tpl if="name.length &gt; 0"> <a class="flecha-subir-grupo">' + '<span data-action="subir-grupo" class="x-fa fa-arrow-up"> |</span></a>  ' + '<a class="flecha-bajar-grupo"><span data-action="bajar-grupo" class="x-fa fa-arrow-down"></span> |</a> ' + '<b>{name}</b><tpl else>' + appI18N.miembros.sinGrupo + '</tpl>',
        collapseTip:''
    }],

    multiSelect: true,

    tbar : [
        {
            xtype : 'button',
            iconCls : 'fa fa-plus',
            text : appI18N.common.anadir,
            handler : 'onAdd'
        },
        {
            xtype : 'button',
            iconCls : 'fa fa-edit',
            name : 'botonEditar',
            text : appI18N.common.editar,
            handler : 'onEdit'
        },
        {
            xtype : 'button',
            iconCls : 'fa fa-remove',
            name : 'botonBorrar',
            text : appI18N.common.borrar,
            handler : 'onDelete'
        }, ' | ', {
            xtype : 'splitbutton',
            text : appI18N.reuniones.acciones,
            iconCls : 'fa fa-gears',
            menu : [
                {
                    iconCls : 'fa fa-group',
                    name : 'botonEnviarConvocatoria',
                    text : appI18N.miembros.asignarAGrupo,
                    handler : 'onAsignarGrupo',
                    style : {
                        fontSize : '1.4em'
                    }
                },
                {
                    iconCls : 'fa fa-ban',
                    name : 'fullaFirmes',
                    text : appI18N.miembros.desagrupar,
                    handler : 'onDesagrupar',
                    style : {
                        fontSize : '1.4em'
                    }
                }, {
                    iconCls : 'fa fa-lock',
                    name : 'botonBloquearOrdenacion',
                    text : appI18N.miembros.bloquearOrdenacion,
                    handler : 'onBloquearOrdenacion',
                    style : {
                        fontSize : '1.4em'
                    }
                }, {
                    iconCls : 'fa fa-unlock',
                    name : 'botonDesbloquearOrdenacion',
                    text : appI18N.miembros.desbloquearOrdenacion,
                    handler : 'onDesbloquearOrdenacion',
                    style : {
                        fontSize : '1.4em'
                    }
                }
            ]
        }
    ],

    name: 'miembroGrid',

    controller: 'miembroGridController',
    bind: {
        store: '{miembrosStore}'
    },

    title: appI18N.miembros.tituloGrid,
    scrollable: true,
    flex: 1,

    columns: {
        defaults: {
            sortable: false
        },
        items: miembroGridColumns
    },
    listeners: {
        beforeedit: 'decideRowIsEditable',
        subeMiembro: 'subirMiembro',
        bajaMiembro: 'bajarMiembro',
        groupclick: 'onGroupClick',
        afterLayout: 'LockOnStart'
    }
});
