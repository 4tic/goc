Ext.define('goc.view.miembro.FormMiembroGrupo', {
    extend: 'Ext.window.Window',
    xtype: 'formMiembroGrupo',

    width: 300,
    minHeight: 200,
    modal: true,
    bodyPadding: 10,
    autoScroll: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    requires: [
        'goc.view.miembro.FormMiembroGrupoController'
    ],
    controller: 'formMiembroGrupoController',

    bbar: [
        '->', {
            xtype: 'panel',
            html: '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cerrar + '</a>',
            listeners: {
                render: function (component) {
                    component.getEl().on('click', 'onClose');
                }
            }
        }],

    bind: {
        title: appI18N.miembros.gestionarGrupos
    },

    items: [{
        xtype: 'form',
        name: 'miembroGrupo',
        border: 0,
        layout: 'anchor',
        items: [{
            xtype: 'fieldset',
            title: appI18N.miembros.informacion,
            defaultType: 'textfield',
            defaults: {
                anchor: '100%'
            },

            items: [
                {
                    xtype: 'textfield',
                    name: 'nuevoGrupo',
                    fieldLabel: appI18N.miembros.crearGrupo,
                    bind: '{grupo.nombre}',
                    allowBlank: false,
                    regex: /[^\s\\]/
                }
            ]
        }],
        buttons : [
            {
                text : appI18N.miembros.asignarGrupo,
                handler : 'asignarGrupo'
            }
        ]
    }]
});
