var invitadoGridColumns = [];

invitadoGridColumns.push({
        xtype : 'actioncolumn',
        title : appI18N.reuniones.posicion,
        align : 'right',
        width : 30,
        name : "subir",
        items : [
            {
                iconCls : 'x-fa fa-arrow-up',
                tooltip : appI18N.reuniones.subir,
                handler : function(grid, index)
                {
                    var rec = grid.getStore().getAt(index);
                    var invitadoId = rec.get('id');
                    grid.up('grid').fireEvent('subeInvitado', invitadoId);
                }
            }
        ]
    },
    {
        xtype : 'actioncolumn',
        title : appI18N.reuniones.posicion,
        align : 'right',
        width : 30,
        name : "bajar",
        items : [
            {
                iconCls : 'x-fa fa-arrow-down',
                tooltip : appI18N.reuniones.bajar,
                handler : function(grid, index)
                {
                    var rec = grid.getStore().getAt(index);
                    var invitadoId = rec.get('id');
                    grid.up('grid').fireEvent('bajaInvitado', invitadoId);
                }
            }
        ]
    });

invitadoGridColumns.push({
    text : 'Id',
    width : 80,
    dataIndex : 'id',
    hidden : true
});

invitadoGridColumns.push({
    text : appI18N.invitados.nombre,
    dataIndex : 'personaNombre',
    flex : 2
});

invitadoGridColumns.push({
    text: appI18N.invitados.email,
    dataIndex: 'personaEmail',
    flex: 2,
    editor: {
        field: {
            xtype: 'textfield',
            vtype: 'email',
            allowBlank: false
        }
    }
});

invitadoGridColumns.push({
    text : appI18N.invitados.soloConsulta,
    dataIndex : 'soloConsulta',
    flex : 1,
    renderer: function(val) {
      return val ? 'Sí' : 'No';
    },
    editor: {
        field: {
            xtype: 'checkbox'
        }
    }
});

if(mostrarDescripcionInvitados)
{
    invitadoGridColumns.push({
        text: getMultiLangLabel(appI18N.invitados.descripcion, mainLanguage),
        dataIndex: 'descripcion',
        flex: 2,
        editor: {
            field: {
                allowBlank: true
            }
        }
    });

    if (isMultilanguageApplication()) {
        invitadoGridColumns.push({
            text: getMultiLangLabel(appI18N.invitados.descripcion, alternativeLanguage),
            dataIndex: 'descripcionAlternativa',
            flex: 2,
            editor: {
                field: {
                    allowBlank: true
                }
            }
        });
    }
}

Ext.define('goc.view.organo.OrganoInvitadoGrid',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.organoInvitadoGrid',
    requires : ['goc.view.organo.OrganoInvitadoGridController'],
    controller : 'organoInvitadoGridController',
    name : 'organoInvitadoGrid',
    title : appI18N.organos.invitados,
    multiSelect : true,
    scrollable : true,

    tbar : [
        {
            xtype : 'button',
            name : 'add',
            iconCls : 'fa fa-plus',
            text : appI18N ? appI18N.common.anadir : 'Afegir',
            handler : 'onAdd'
        },
        {
            xtype : 'button',
            name : 'edit',
            iconCls : 'fa fa-edit',
            text : appI18N ? appI18N.common.editar : 'Editar',
            handler : 'onEdit'
        },
        {
            xtype : 'button',
            name : 'remove',
            iconCls : 'fa fa-remove',
            text : appI18N ? appI18N.common.borrar : 'Esborrar',
            handler : 'onDelete'
        }
    ],

    bind : {
        store : '{organoInvitadosStore}'
    },
    columns : invitadoGridColumns,
    viewConfig : {
        emptyText : appI18N.organos.sinInvitadosDisponibles
    },
    listeners : {
        organoSelected : 'organoSelected',
        onDelete : 'onDelete',
        subeInvitado: 'subirInvitado',
        bajaInvitado: 'bajarInvitado',
        beforeedit : 'decideRowIsEditable'
    }
});
