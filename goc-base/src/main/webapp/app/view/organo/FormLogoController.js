Ext.define('goc.view.organo.FormLogoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formLogoController',

    onSaveLogo: function (object) {
        var form = object.up('form');
        if (form.getForm().isValid()) {
            var inputLogo = form.down('filefield');
            var file = inputLogo.getEl().down('input[type=file]').dom.files[0];
            if(file.size < (1024 * 100)) {
                form.submit({
                    url: appContext + '/rest/organos/' + form.organoId + '/logo?externo=' + form.organoExterno,
                    scope: this,
                    success: function () {
                        form.reset();
                        Ext.WindowManager.getActive().close();
                    }
                });
            } else {
                Ext.Msg.alert("Error", appI18N.organos.actualizarLogoTamanyoMensaje);
            }
        }
    }
});