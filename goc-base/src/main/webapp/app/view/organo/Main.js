Ext.define('goc.view.organo.Main',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.organoMainPanel',
    title : appI18N.organos.titulo,

    requires : [
        'goc.view.organo.MainController',
        'goc.view.organo.Grid',
        'goc.view.organo.ViewModel',
        'goc.view.organo.AutorizadoGrid',
        'goc.view.organo.OrganoInvitadoGrid',
        'goc.view.organo.CargoGrid',
        'goc.view.organo.ComboEstadoOrgano',
        'goc.view.organo.ComboTipoOrgano'
    ],

    layout : 'fit',
    border : 0,
    controller : 'organoMainController',

    viewModel : {
        type : 'organoViewModel'
    },

    items : [
        {
            xtype : 'panel',
            layout : 'vbox',
            items : [
                {
                    xtype : 'organoGrid',
                    flex : 1,
                    width : '100%'
                },
                {
                    xtype : 'tabpanel',
                    flex : 1,
                    activeTab : (editOrganos === "edit")? 0:1,
                    disabled: true,
                    width : '100%',
                    split: true,
                    items : [
                        {
                            xtype : 'autorizadoGrid',
                            flex : 1,
                            width : '100%',
                            hidden: editOrganos !== "edit"

                        },
                        {
                            xtype : 'organoInvitadoGrid',
                            flex : 1,
                            width : '100%'
                        },
                        {
                            xtype : 'organoCargoGrid',
                            flex : 1,
                            width : '100%'
                        }
                    ]
                }
            ]
        }
    ],

    listeners : {
        'tipoOrganoSelected' : 'onTipoOrganoSelected',
        'filtrarOrganos' : 'onFiltrarOrganos',
        'searchOrgano': 'onSearchOrgano'
    }
}
);
