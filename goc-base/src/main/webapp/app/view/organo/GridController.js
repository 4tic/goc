Ext.define('goc.view.organo.GridController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.organoGridController',
    onLoad: function () {
        var viewModel = this.getViewModel();
        viewModel.getStore('organosStore').load();
        viewModel.getStore('tipoOrganosStore').load({
            callback: function () {
                var grid = this.getView();
                grid.getView().refresh();
            },
            scope: this
        });
    },

    afterLoad: function () {
        var comboEstado = this.getView().up('organoMainPanel').down('comboEstadoOrgano');
        comboEstado.setValue(false);
    },

    decideRowIsEditable : function(editor, context)
    {
        if(editOrganos === "edit"){
            return (context.record.get('externo') !== 'true' && !context.record.get('inactivo')) || context.record.phantom;
        }
        return false;
    },

    organoSelected: function (controller, record) {
        var grid = this.getView();
        var toolbar = grid.down("toolbar");

        var recordModel = grid.getSelectedRow();
        var gridAutorizados = grid.up('panel').down('grid[name=autorizadoGrid]');
        var gridInvitados = grid.up('panel').down('grid[name=organoInvitadoGrid]');
        var gridCargos = grid.up('panel').down('grid[name=organoCargoGrid]');
        var tab = this.getView().up('panel').down('tabpanel');

        if (!recordModel) {
            gridAutorizados.clearStore();
            gridInvitados.clearStore();
            gridCargos.clearStore();
            tab.disable();

            toolbar.items.each(function (button) {
                if (button.name !== 'add') {
                    button.setDisabled();
                }
            });

            return;
        }

        toolbar.items.each(function (button) {
            if (button.name === 'edit') {
                button.setDisabled(record[0].get("externo") === "true");
            }
        });

        var selection = grid.getView().getSelectionModel().getSelection();

        record = selection[selection.length - 1];
        if (editOrganos === "edit") {
            gridAutorizados.fireEvent('organoSelected', record);
        }
        gridInvitados.fireEvent('organoSelected', record);
        gridCargos.fireEvent('organoSelected', record);

        if (record.get('inactivo')) {
            return tab.disable();
        }

        tab.enable();
    },

    initFilters: function () {
        var grid = this.getView();
        var comboEstado = grid.up('organoMainPanel').down('comboEstadoOrgano');
        var comboTipoOrgano = grid.up('organoMainPanel').down('comboTipoOrgano');
        var organoSearch = grid.up('organoMainPanel').down('[reference=organoSearch]');

        organoSearch.setValue("");
        comboTipoOrgano.clearValue();
        comboEstado.setValue(false);

        var store = this.getStore('organosStore');
        store.clearFilter();

        grid.up('organoMainPanel').fireEvent('filtrarOrganos', false);
    },

    onAdd: function () {
        this.initFilters();
        this.callParent();
    },

    cancelEdit: function () {
        var editor = this.getView().plugins[0];
        editor.cancelEdit();
    },

    onToggleEstado: function () {
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (record.phantom === true) {
            return grid.getStore().remove(record);
        }

        this.cancelEdit();

        if (record.get('inactivo') === false) {
            return Ext.Msg.confirm(appI18N.organos.inhabilitar, appI18N.organos.inhabilitarOrgano, function (btn, text) {
                if (btn == 'yes') {
                    record.set('inactivo', true);
                    grid.getStore().sync({
                        success: function () {
                            grid.getSelectionModel().clearSelections();
                            grid.getView().refresh();
                            grid.up('panel').down('grid[name=autorizadoGrid]').clearStore();
                            grid.up('panel').down('grid[name=organoInvitadoGrid]').clearStore();
                            grid.up('panel').down('grid[name=organoCargoGrid]').clearStore();
                        }
                    });
                }
            });
        }

        record.set('inactivo', false);
        grid.getStore().sync({
            success: function () {
                grid.getSelectionModel().clearSelections();
                grid.getView().refresh();
                grid.up('panel').down('grid[name=autorizadoGrid]').clearStore();
                grid.up('panel').down('grid[name=organoInvitadoGrid]').clearStore();
                grid.up('panel').down('grid[name=organoCargoGrid]').clearStore();
            }
        });
    },

    onSearchOrgano: function (field, searchString) {
        this.getView().up('organoMainPanel').fireEvent('searchOrgano', searchString);
    },

    onExport: function () {
        var grid = this.getView();
        var selection = grid.getView().getSelectionModel().getSelection()[0];
        if(editOrganos === "edit") {
            if (!selection) {
                return Ext.Msg.alert(appI18N.organos.exportarOrgano || "Exportar membres", appI18N.organos.exportarOrganoMensaje || "Cal seleccionar un registre per a poder exportar els membres.");
            }
            var url = appContext + '/rest/organos/' + selection.id + "/export";
            window.open(url, '_blank');
        } else {
           return Ext.Msg.alert(appI18N.exception.adminRequired,appI18N.exception.adminRequired);
        }
    },

        onEditLogo: function ()
        {
            var grid = this.getView();
            var selection = grid.getView().getSelectionModel().getSelection()[0];
            if(editOrganos === "edit") {
                if (!selection) {
                    return Ext.Msg.alert(appI18N.common.editarLogo || "Editar logo", appI18N.organos.editarLogoMensaje || "Cal seleccionar un registre per a poder editar el logo.");
                }
                var window = Ext.create('Ext.window.Window', {
                        title: appI18N.organos.actualizarLogo,
                        controller: 'formLogoController',
                        layout: 'vbox',
                        items: [
                            {
                                xtype: 'form',
                                organoId: selection.get('id'),
                                organoExterno: selection.get('externo'),
                                items: [
                                    {
                                        xtype: 'filefield',
                                        reference: 'logo',
                                        name: 'logo',
                                        allowBlank: false,
                                        fieldLabel: 'Logo',
                                        width: 500
                                    }
                                ],
                                buttons: [
                                    {
                                        text: appI18N.organos.btnGuardar,
                                        handler: 'onSaveLogo'
                                    }
                                ]
                            }
                        ]
                    })
                ;
                window.show();
            }
        },

        mostrarLinkLogo: function (id, meta, rec)
        {
            var hasLogo = rec.data.hasLogo;
            if (hasLogo)
            {
                return '<a href=\"' + appContext + '/rest/organos/' + rec.get('id') + '/logo?externo=' + rec.get('externo') + '\" target="_blank">' + appI18N.organos.verLogo + '</a>';
            }
        }
    }
);
