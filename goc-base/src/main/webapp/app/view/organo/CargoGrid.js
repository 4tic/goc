Ext.define('goc.view.organo.CargoGrid',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.organoCargoGrid',
    requires : ['goc.view.organo.CargoGridController'],
    controller : 'cargoGridController',
    name : 'organoCargoGrid',
    title : appI18N.cargos.titulo,
    multiSelect : true,
    scrollable : true,

    tbar : [
        {
            xtype : 'button',
            name : 'add',
            iconCls : 'fa fa-plus',
            text : appI18N ? appI18N.common.anadir : 'Afegir',
            handler : 'onAdd'
        },
        {
            xtype : 'button',
            name : 'remove',
            iconCls : 'fa fa-remove',
            text : appI18N ? appI18N.common.borrar : 'Esborrar',
            handler : 'onDelete'
        }
    ],

    bind : {
        store : '{organoCargosStore}'
    },
    columns : [
        {
            text : 'Id',
            width : 80,
            dataIndex : 'id',
            hidden : true
        },
        {
            text : appI18N.cargos.codigo,
            dataIndex : 'cargoId',
            flex : 1
        },
        {
            text : appI18N.cargos.nombre,
            dataIndex : 'cargoNombre',
            flex : 1
        }
    ],
    viewConfig : {
        emptyText : appI18N.organos.sinCargosDisponibles
    },
    listeners : {
        organoSelected : 'organoSelected',
        onDelete : 'onDelete'
    }
});
