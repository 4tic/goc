Ext.define('goc.view.organo.AutorizadoGrid',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.autorizadoGrid',
    requires : ['goc.view.organo.AutorizadoGridController'],
    controller : 'autorizadoGridController',
    name : 'autorizadoGrid',
    title : appI18N.organos.autorizados,
    multiSelect : true,
    scrollable : true,
    bind : {
        store : '{organoAutorizadosStore}'
    },
    columns : [
        {
            text : 'Id',
            width : 80,
            dataIndex : 'id',
            hidden : true
        },
        {
            text : appI18N.autorizados.nombre,
            dataIndex : 'personaNombre',
            flex : 1
        },
        {
            text : appI18N.autorizados.subirDocumentos,
            dataIndex : 'subirDocumentos',
            flex : 1,
            renderer : function(id, meta, rec)
            {
                return rec.get('subirDocumentos') === true ? 'Sí' : 'No';
            },
            editor: {
                field: {
                    xtype: 'checkbox',
                    allowBlank: false
                }
            }
        }
    ],
    viewConfig : {
        emptyText : appI18N.organos.sinAutorizadosDisponibles
    },
    listeners : {
        organoSelected : 'organoSelected',
        onDelete : 'onDelete'
    }
});
