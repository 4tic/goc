Ext.define('goc.view.organo.CargoGridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.cargoGridController',

    onLoad : function()
    {
    },

    organoSelected : function(organo)
    {
        this.getViewModel().getStore('organoCargosStore').load(
        {
            params : {
                organoId : organo.get('id')
            }
        });
    },

    onAdd : function()
    {
        var organosGrid = this.getView().up('panel[alias=widget.organoMainPanel]').down('grid[name=organosGrid]');
        var record = organosGrid.getView().getSelectionModel().getSelection()[0];
        var vm = this.getViewModel();

        if (!record)
            return;

        var window = Ext.create('goc.view.common.LookupWindowCargos',
        {
            appPrefix : 'goc',
            title : appI18N.miembros.seleccionaCargo
        });

        window.show();

        var store = vm.getStore('organoCargosStore');
        var self = this;
        var grid = this.getView();
        window.on('LookoupWindowClickSeleccion', function(res)
        {
            var cargo = Ext.create('goc.model.OrganoCargo',
            {
                cargoId : res.get('id'),
                cargoCodigo : res.get('codigo'),
                cargoNombre : res.get('nombre'),
                organoId : record.get('id'),
                organoExterno : record.get('externo')
            });

            var existeCargo = store.findExact('cargoId', cargo.get('cargoId'));
            if (existeCargo === -1)
            {
                grid.getStore().insert(0, cargo);
            }
            else {
                cargo = grid.getStore().getAt(existeCargo);
            }
            var editor = grid.plugins[0];
            editor.cancelEdit();
            editor.startEdit(cargo, 0);
        });
    },

    onDelete : function(grid, td, cellindex)
    {
        grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record)
        {
            return Ext.Msg.alert(appI18N.organos.borrarCargo, appI18N.organos.seleccionarParaBorrarCargo);
        }

        var vm = this.getViewModel();
        var store = vm.getStore('organoCargosStore');

        Ext.Msg.confirm(appI18N.common.borrar, appI18N.organos.borrarCargo, function(result)
        {
            if (result === 'yes')
            {
                store.remove(record);
                store.sync();
            }
        });
    }
});
