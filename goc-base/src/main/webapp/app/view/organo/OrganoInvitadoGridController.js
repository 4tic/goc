Ext.define('goc.view.organo.OrganoInvitadoGridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.organoInvitadoGridController',

    onLoad : function()
    {
    },

    organoSelected : function(organo)
    {
        this.getViewModel().getStore('organoInvitadosStore').load(
        {
            params : {
                organoId : organo.get('id')
            }
        });
    },

    onAdd : function()
    {
        var organosGrid = this.getView().up('panel[alias=widget.organoMainPanel]').down('grid[name=organosGrid]');
        var record = organosGrid.getView().getSelectionModel().getSelection()[0];
        var vm = this.getViewModel();
        var grid = this.getView();

        if (!record)
            return;

        var window = Ext.create('goc.view.common.LookupWindowPersonas',
        {
            appPrefix : 'goc',
            title : appI18N.organos.seleccionaInvitado
        });

        window.show();

        var store = vm.getStore('organoInvitadosStore');
        window.on('LookoupWindowClickSeleccion', function(res)
        {
            var invitado = Ext.create('goc.model.OrganoInvitado',
            {
                personaId : res.get('id'),
                personaNombre : res.get('nombre'),
                personaEmail : res.get('email'),
                organoId : record.get('id')
            });

            var existeInvitado = store.findExact('personaId', invitado.get('personaId'));
            if (existeInvitado === -1)
            {
                grid.getStore().insert(0, invitado);
            }
            else {
                invitado = grid.getStore().getAt(existeInvitado);
            }
            var editor = grid.plugins[0];
            editor.cancelEdit();
            editor.startEdit(invitado, 3);
        });
    },

    onDelete : function(grid, td, cellindex)
    {
        grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection()[0];

        if (!record)
        {
            return Ext.Msg.alert(appI18N.organos.borrarInvitado, appI18N.organos.seleccionarParaBorrarInvitado);
        }

        var vm = this.getViewModel();
        var store = vm.getStore('organoInvitadosStore');

        Ext.Msg.confirm(appI18N.common.borrar, appI18N.organos.borrarInvitado, function(result)
        {
            if (result === 'yes')
            {
                store.remove(record);
                store.sync();
            }
        });
    },

    onEdit : function()
    {
        var grid = this.getView();
        var selection = grid.getView().getSelectionModel().getSelection()[0];
        if (!selection)
        {
            return Ext.Msg.alert(appI18N.common.edicionRegistro || "Edició de registre", appI18N.common.seleccionarParaEditarRegistro || "Cal seleccionar un registre per a poder fer l'edició");
        }

        var editor = grid.plugins[0];
        editor.cancelEdit();
        editor.startEdit(selection, 3);
    },

    onChangeSoloConsulta : function(elem, rowIndex, checked, record)
    {
        var store = this.getViewModel().getStore('organoInvitadosStore');
        store.sync();
    },

    subirInvitado: function (invitadoId) {
        var grid = this.getView();
        Ext.Ajax.request({
            url : appContext + '/rest/organos/invitados/' + invitadoId + '/subir',
            method : 'PUT',
            success : function()
            {
                grid.getStore().reload();
            }
        });
    },

    bajarInvitado: function (invitadoId) {
        var grid = this.getView();
        Ext.Ajax.request({
            url : appContext + '/rest/organos/invitados/' + invitadoId + '/bajar',
            method : 'PUT',
            success : function()
            {
                grid.getStore().reload();
            }
        });
    },

    decideRowIsEditable: function (editor, context) {
        if (context.column.name === "bajar" || context.column.name === "subir"){
            return false;
        }
    }
});
