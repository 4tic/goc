Ext.define('goc.view.personas.PersonasGridController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.personasGridController',
    onLoad: function () {
        var viewModel = this.getViewModel();
        viewModel.getStore('personasStore').load();
    },

    onSearchpersona: function(field, searchString) {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('personasStore');

        if (!searchString) {
            store.clearFilter();
            return;
        }

        var filter  = new Ext.util.Filter(
            {
                id : 'search',
                property : 'nombre',
                value : searchString,
                anyMatch: true
            });

        store.addFilter(filter);
    },

    onAddDesdeRepositorio : function()
    {
        var grid = this.getView();
        var rec = Ext.create('goc.model.Persona');
        var window = Ext.create('goc.view.common.LookupWindowPersonas',
            {
                appPrefix : 'goc',
                title : appI18N.personas.anyadirPersona
            });

        window.show();

        window.on('LookoupWindowClickSeleccion', function(res)
        {

            rec.set('nombre', res.get('nombre'));
            rec.set('email', res.get('email'));
            Ext.Ajax.request({
                url : appContext + '/rest/personas',
                method : 'POST',
                jsonData: {nombre: rec.get('nombre'), email: rec.get('email')},
                success : function(data)
                {
                    grid.getStore().reload();
                }
            });
        });
    },

    deshabilitaPersona: function(){
        var grid = this.getView();
        var record = grid.getView().getSelectionModel().getSelection();

        if (record.length === 0)
        {
            return Ext.Msg.alert(appI18N.common.borradoRegistro || "Esborrar registre", appI18N.common.seleccionarParaBorrarRegistro || "Cal seleccionar un registre per a poder esborrar-lo");
        }
        Ext.Ajax.request({
            url : appContext + '/rest/personas/' + record[0].data.id + '/deshabilita',
            method : 'PUT',
            success : function()
            {
                grid.getStore().reload();
            }
        });
    },

    storeReload: function (){
        var viewModel = this.getViewModel();
        viewModel.getStore('personasStore').reload();
    },

    decideRowIsEditable: function(editor, context){
        for (var i = 0; i < editor.editor.items.items.length; i++) {
            if (editor.editor.items.items[i].dataIndex == 'email') {
                editor.editor.items.items[i].setDisabled(!context.record.phantom);
            }
        }
    }
})
;
