Ext.define('goc.view.personas.Main',
    {
        extend: 'Ext.panel.Panel',
        alias: 'widget.personasMainPanel',
        title: appI18N.menu.personas,

        requires: [
            'goc.view.personas.PersonasGrid',
            'goc.view.personas.ViewModel'
        ],

        layout: 'fit',
        border: 0,

        viewModel: {
            type: 'personasViewModel'
        },

        items: [
            {
                xtype: 'panel',
                layout: 'vbox',
                items: [
                    {
                        xtype: 'personasGrid',
                        flex: 1,
                        width: '100%'
                    }
                ]
            }
        ]
    });