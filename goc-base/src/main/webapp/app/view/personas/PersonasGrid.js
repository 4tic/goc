var personasGridColumns = [
    {
        text: "Id",
        dataIndex: 'id',
        flex: 1,
        hidden : true
    }
];

personasGridColumns.push({
    text: appI18N.miembros.login,
    dataIndex: 'login',
    flex: 1,
    editor: {
        allowBlank: false,
        xtype: 'textfield'
    }
});
personasGridColumns.push({
    text: appI18N.organos.nombre,
        dataIndex: 'nombre',
    flex: 1,
    editor: {
    field: {
        allowBlank: false
    }
}

});
personasGridColumns.push({
    text: appI18N.miembros.correo,
    dataIndex: 'email',
    flex: 1,
    editor: {
        allowBlank: false,
        xtype: 'textfield',
        vtype: 'email'
    }
});
personasGridColumns.push({
    text : appI18N.reuniones.administrador,
    dataIndex : 'administrador',
    flex : 1,
    renderer : function(value, meta, rec)
    {
        return value ? 'Sí' : 'No';
    }
});

Ext.define('goc.view.personas.PersonasGrid', {
    extend: 'Ext.ux.uji.grid.Panel',
    alias: 'widget.personasGrid',
    requires: [
        'goc.view.personas.PersonasGridController'
    ],
    controller: 'personasGridController',
    reference: 'personasGrid',
    bind: {
        store: '{personasStore}',
        selection: '{selectedPersona}'
    },
    name: 'personasGrid',
    title: appI18N.menu.personas,
    scrollable: true,
    multiSelect: false,
    collapsible: true,
    tbar: [{
        xtype: 'textfield',
        emptyText: appI18N.personas.buscarPersona,
        flex:8 ,
        name: 'searchPersona',
        listeners: {
            change: 'onSearchpersona'
        }
        },
        {
            xtype: 'button',
            name: 'add',
            iconCls: 'fa fa-plus',
            text: appI18N.common.anadir,
            handler: 'onAdd'
        },
        {
            xtype : 'button',
            iconCls : 'fa fa-plus',
            text : appI18N.personas.anadirDesdeRepositorio,
            handler : 'onAddDesdeRepositorio',
            hidden:!busquedaPersonas
        },
        {
            xtype: 'button',
            name: 'edit',
            iconCls: 'fa fa-edit',
            text: appI18N.common.editar ,
            handler: 'onEdit'
        },
        {
            xtype: 'button',
            name: 'borrar',
            iconCls: 'fa fa-times',
            text: appI18N.common.borrar ,
            handler: 'deshabilitaPersona'
        }
    ],

    columns: personasGridColumns,

    listeners: {
        render: 'onLoad',
        edit: 'storeReload',
        beforeedit: 'decideRowIsEditable'
    }
});
