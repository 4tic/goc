Ext.define('goc.view.personas.ViewModel', {
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.personasViewModel',

    requires : [
        'goc.store.Personas'
    ],

    stores : {
        personasStore : {
            type : 'personas'
        }
    }
});
