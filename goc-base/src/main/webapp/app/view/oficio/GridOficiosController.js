Ext.define('goc.view.oficio.GridOficiosController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.gridOficiosController',
    onLoad: function ()
    {
    },


    onAdd: function()
    {
        var window = Ext.create('goc.view.oficio.FormSeleccionaReunionPunto',
            {
                appPrefix : 'goc',
                title : appI18N.oficios.envioOficioForm,
                viewModel: this.createViewModel()
            });

        window.show();
    },

    onSearchReunion: function(field, searchString)
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('oficiosStore');

        if (!searchString)
        {
            store.removeFilter('search');
            return;
        }

        var filter  = new Ext.util.Filter(
            {
                id : 'search',
                property : 'tituloReunion',
                value : searchString,
                anyMatch: true
            });

        store.addFilter(filter);
    },

    onSearchDestinatario: function(field, searchString)
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('oficiosStore');

        if (!searchString)
        {
            store.removeFilter('searchDestinatario');
            return;
        }

        var filter  = new Ext.util.Filter(
            {
                id : 'searchDestinatario',
                property : 'nombre',
                value : searchString,
                anyMatch: true
            });

        store.addFilter(filter);
    },

    onSearchFechaEnvioInicio: function (field, searchDate)
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('oficiosStore');

        if (!searchDate)
        {
            store.removeFilter('searchFechaEnvioInicio');
            return;
        }

        var filter  = new Ext.util.Filter(
            {
                id : 'searchFechaEnvioInicio',
                property : 'fechaEnvio',
                value : searchDate,
                filterFn: function(item) {
                    var filterDate = new Date(searchDate);
                  return item.get('fechaEnvio').getTime() >= filterDate.getTime();
                },
                anyMatch: true
            });

        store.addFilter(filter);
    },

    onSearchFechaEnvioFin: function (field, searchDate)
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('oficiosStore');

        if (!searchDate)
        {
            store.removeFilter('searchFechaEnvioFin');
            return;
        }

        var filter  = new Ext.util.Filter(
            {
                id : 'searchFechaEnvioFin',
                property : 'fechaEnvio',
                value : searchDate,
                filterFn: function(item) {
                    var filterDate = new Date(searchDate);
                    return item.get('fechaEnvio').getTime() <= filterDate.getTime();
                },
                anyMatch: true
            });

        store.addFilter(filter);
    },

    createViewModel: function ()
    {
        return Ext.create('Ext.app.ViewModel', {
            stores: {
                reunionesStore: this.getViewModel().getStore('reunionesStore'),
                organosStore: this.getViewModel().getStore('organosStore'),
                oficiosStore: this.getViewModel().getStore('oficiosStore'),
                puntosOrdenDiaTreeStore: this.getViewModel().getStore('puntosOrdenDiaTreeStore')
            }
        });
    }
});
