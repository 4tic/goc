Ext.define('goc.view.oficio.FormEnvioOficio',
    {
        extend : 'Ext.Window',

        alias : 'widget.formEnvioOficio',
        appPrefix : '',
        bean : 'base',
        formularioOficio : '',
        botonBuscar : '',
        botonCancelar : '',
        title : appI18N.oficios.envioOficioTitle,
        viewConfig : 'fit',

        layout : {
            type : 'vbox',
            align : 'stretch'
        },
        modal : true,
        hidden : true,
        width : 400,
        closeAction : 'hide',

        initComponent : function()
        {
            this.callParent(arguments);
            this.initUI();
            this.add(this.formularioOficio);
        },

        initUI : function() {
            this.buildFormPanel();
            this.buildBotonCancelar();
            this.buildBotonEnviar();
            this.buildformularioOficio();
        },

        buildFormPanel: function ()
        {
            var ref = this;
            this.formOficio = Ext.create('Ext.form.FieldSet', {
                title: 'Datos necesarios',
                anchor: '100%',
                margin: 10,

                items: [
                    {
                        xtype: 'textfield',
                        name: 'nombre',
                        anchor: '100%',
                        fieldLabel: appI18N.oficios.nombre,
                        readOnly: true,
                        allowBlank: false,
                        cls: 'readonly',
                        value: ref.invitado.get('personaNombre')
                    },
                    {
                        xtype: 'textfield',
                        name: 'cargo',
                        allowBlank: false,
                        anchor: '100%',
                        fieldLabel: appI18N.oficios.cargo
                    },
                    {
                        xtype: 'textfield',
                        name: 'email',
                        anchor: '100%',
                        fieldLabel: appI18N.oficios.email,
                        readOnly: true,
                        allowBlank: false,
                        cls: 'readonly',
                        value: ref.invitado.get('personaEmail')
                    },
                    {
                        xtype: 'textfield',
                        name: 'unidad',
                        allowBlank: false,
                        anchor: '100%',
                        fieldLabel: appI18N.oficios.unidad
                    },
                    {
                        xtype: 'textfield',
                        name: 'registro',
                        anchor: '100%',
                        fieldLabel: appI18N.oficios.registro
                    },
                    {
                        xtype: 'numberfield',
                        name: 'puntoOrdenDiaId',
                        hidden: true,
                        value: ref.puntoOrdenDiaId
                    }

                ]
            });
        },

        buildformularioOficio : function()
        {
            this.formularioOficio = Ext.create('Ext.form.Panel',
                {
                    layout : 'anchor',
                    border: 0,
                    padding: '5 5 5 5',
                    items : [this.formOficio],
                    buttons: [this.botonSeleccionar, this.botonCancelar]
                });
        },

        buildBotonCancelar : function()
        {
            var ref = this;

            this.botonCancelar = Ext.create('Ext.Panel',
                {
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cancelar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', function()
                            {
                                ref.hide();
                            });
                        }
                    }
                });
        },

        buildBotonEnviar: function()
        {
            var ref = this;

            this.botonSeleccionar = Ext.create('Ext.Button',
                {
                    text : appI18N.oficios.enviar,
                    handler : function(e)
                    {
                        if(ref.formularioOficio.isValid()) {
                            ref.formularioOficio.setLoading(appI18N.common.loading);
                            Ext.Ajax.request({
                                url: appContext + '/rest/oficios',
                                method: 'POST',
                                jsonData: ref.formularioOficio.getValues(),
                                success: function () {
                                    ref.formularioOficio.setLoading(false);
                                    ref.mensajeEnvioOk();
                                    ref.cerrarVentana();
                                }
                            });
                        }
                    }
                });
        },

        cerrarVentana: function ()
        {
            this.close();
        },

        mensajeEnvioOk: function ()
        {
            Ext.Msg.alert(appI18N.oficios.envioMensajeOkTitulo, appI18N.oficios.envioMensajeOk);
        }
    });
