Ext.define('goc.view.oficio.GridReunionesController', {
    extend: 'Ext.ux.uji.grid.PanelController',
    alias: 'controller.gridReunionesController',

    reunionSelected: function ()
    {
        var grid = this.getView();
        var record = grid.getSelectedRow();

        var gridPuntos = Ext.ComponentQuery.query('treepanel[name=gridPuntosOrdenDia]')[0];

        if (!record)
        {
            gridPuntos.clearStore();
            gridPuntos.disable();
            return;
        }

        gridPuntos.fireEvent('reunionSelected', record.get('id'));
    },

    onSearchReunion: function(field, searchString)
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('reunionesStore');

        if (!searchString)
        {
            store.removeFilter('search');
            return;
        }

        var filter  = new Ext.util.Filter(
            {
                id : 'search',
                property : 'asunto',
                value : searchString,
                anyMatch: true
            });

        store.addFilter(filter);
    },

    onSearchFechaInicio: function (field, searchDate)
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('reunionesStore');

        if (!searchDate)
        {
            store.removeFilter('searchFechaInicio');
            return;
        }
        var filter  = new Ext.util.Filter(
            {
                id : 'searchFechaInicio',
                property : 'fecha',
                value : searchDate,
                filterFn: function(item)
                {
                    var filterDate = new Date(searchDate);
                    return item.get('fecha').getTime() >= filterDate.getTime();
                },
                anyMatch: true
            });

        store.addFilter(filter);
    },

    onSearchFechaFin: function (field, searchDate)
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('reunionesStore');

        if (!searchDate)
        {
            store.removeFilter('searchFechaFin');
            return;
        }

        var filter  = new Ext.util.Filter(
            {
                id : 'searchFechaFin',
                property : 'fecha',
                value : searchDate,
                filterFn: function(item) {
                    var filterDate = new Date(searchDate);
                    return item.get('fecha').getTime() <= filterDate.getTime();
                },
                anyMatch: true
            });

        store.addFilter(filter);
    },

    organoSelected : function(id, externo)
    {
        var grid = this.getView();

        if (id)
        {
            grid.getStore().load({
                params : {
                    organoId : id,
                    externo : externo
                }
            });
        }
        else
        {
            grid.getStore().load();
        }
    }
});