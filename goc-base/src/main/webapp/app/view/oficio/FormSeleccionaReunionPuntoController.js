Ext.define('goc.view.oficio.FormSeleccionaReunionPuntoController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.formSeleccionaReunionPuntoController',

    onLoad: function ()
    {
        var viewModel = this.getViewModel();
        var store = viewModel.getStore('reunionesStore');
        store.getProxy().url = appContext + '/rest/reuniones/completadas';
        store.load();

        viewModel.getStore('puntosOrdenDiaTreeStore').clearData();
    },

    onClose: function ()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            this.getViewModel().getStore('oficiosStore').reload();
            win.destroy();
        }
    },

    onPuntoChoosen: function ()
    {
        var grid = Ext.ComponentQuery.query('treepanel[name=gridPuntosOrdenDia]')[0];
        var record = grid.getView().getSelectionModel().getSelection()[0];
        if(record == null)
        {
            return Ext.Msg.alert(appI18N.oficios.errorSeleccionPuntoTitle, appI18N.oficios.errorSeleccionPunto);
        }
        else
        {

            var window = Ext.create('goc.view.common.LookupWindowPersonas',
                {
                    appPrefix: 'goc',
                    title: appI18N.oficios.seleccionaDestinatario
                });

            window.show();

            window.on('LookoupWindowClickSeleccion', function (res) {
                var invitado = Ext.create('goc.model.ReunionInvitado',
                    {
                        personaId: res.get('id'),
                        personaNombre: res.get('nombre'),
                        personaEmail: res.get('email')
                    });

                var window = Ext.create('goc.view.oficio.FormEnvioOficio',
                    {
                        appPrefix: 'goc',
                        title: 'Envío de oficio',
                        reunionId: record.get('reunionId'),
                        puntoOrdenDiaId: record.get('id'),
                        invitado: invitado
                    });
                window.show();

            });
        }
    }
});
