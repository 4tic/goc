Ext.define('goc.view.oficio.GridPuntosOrdenDiaController', {
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.gridPuntosOrdenDiaController',
    reunionId : null,

    onLoad : function()
    {
    },

    onRefresh : function(reunionId)
    {
        this.reunionId = reunionId;

        var viewModel = this.getViewModel();

        this.getView().setDisabled(false);
        viewModel.getStore('puntosOrdenDiaTreeStore').load({
            url : appContext + '/rest/reuniones/' + reunionId + '/puntosOrdenDia'
        });
    }

});
