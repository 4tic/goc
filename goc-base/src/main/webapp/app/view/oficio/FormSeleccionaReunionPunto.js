Ext.define('goc.view.oficio.FormSeleccionaReunionPunto',
    {
        extend : 'Ext.window.Window',
        xtype : 'formSeleccionaReunionPunto',

        width : 640,
        maxHeight : 1000,
        modal : true,
        bodyPadding : 10,
        autoScroll : true,

        viewConfig : 'fit',

        layout : {
            type : 'vbox',
            align : 'stretch'
        },
        requires: [
            'goc.view.common.ComboOrgano',
            'goc.view.common.InputFecha',
            'goc.view.oficio.FormSeleccionaReunionPuntoController',
            'goc.view.oficio.GridPuntosOrdenDia',
            'goc.view.oficio.GridReuniones'
        ],
        controller : 'formSeleccionaReunionPuntoController',

        bbar : {
            defaultButtonUI : 'default',
            items : [
                '->',
                {
                    xtype : 'button',
                    text : appI18N.oficios.seleccionar,
                    handler : 'onPuntoChoosen'
                },
                {
                    xtype : 'panel',
                    html : '<a style="text-decoration: none; color: #222;" href="#">' + appI18N.common.cancelar + '</a>',
                    listeners : {
                        render : function(component)
                        {
                            component.getEl().on('click', 'onClose');
                        }
                    }
                }
            ]
        },

        title : 'Busqueda de reuniones',

        items : [
            {
                xtype : 'form',
                name : 'busquedaReunion',
                border : 0,
                layout : 'anchor',
                defaults : {
                    anchor : '100%'
                },
                items : [
                    {
                        xtype: 'gridReuniones'
                    },
                    {
                        xtype: 'gridPuntosOrdenDia'
                    }
                ]
            }
        ],

        listeners : {
            beforeclose: 'onClose',
            render: 'onLoad'
        }
    });
