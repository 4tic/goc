var gridColumns = [
    {
        text : 'Id',
        width : 80,
        dataIndex : 'id',
        hidden : true
    },
    {
        xtype: 'treecolumn',
        text : getMultiLangLabel(appI18N.reuniones.titulo, mainLanguage),
        dataIndex : 'titulo',
        flex : 1
    }
];

if (isMultilanguageApplication())
{
    gridColumns.push({
        text : getMultiLangLabel(appI18N.reuniones.titulo, alternativeLanguage),
        dataIndex : 'tituloAlternativo',
        flex : 1
    });
}

Ext.define('goc.view.oficio.GridPuntosOrdenDia',
    {
        extend : 'Ext.tree.Panel',
        alias : 'widget.gridPuntosOrdenDia',
        plugins : [],
        requires : ['goc.view.oficio.GridPuntosOrdenDiaController'],
        controller : 'gridPuntosOrdenDiaController',
        height: 200,
        bind : {
            store : '{puntosOrdenDiaTreeStore}',
            selection : '{selectedReunion}'
        },
        name : 'gridPuntosOrdenDia',
        reference : 'gridPuntosOrdenDia',
        rootVisible: false,
        scrollable : true,
        title : appI18N.oficios.seleccionaPuntoOrdenDiaGrid,
        multiSelect : false,
        columns : gridColumns,
        viewConfig : {
            emptyText : appI18N.reuniones.ordendiaVacio
        },
        split: true,
        listeners : {
            reunionSelected : 'onRefresh'
        },

        tbar : [

        ],

        clearStore : function()
        {
            var store = this.getStore();

            store.suspendAutoSync();
            store.removeAll();
            store.clearData();
            store.resumeAutoSync();
        }
    });