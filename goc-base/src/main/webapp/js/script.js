var appI18N;
var EMPTY_DIV = "<div></div>";
var PENDING_REQ = [];
const RELOAD_TIMEOUT = 5000;

function mostrarError(error) {
    if (error.indexOf("appI18N") != -1)
        alert(eval(error));
    else
        alert(error);
}

function validarFormularioDocumentoPuntosOrdenDia(form) {
    var errors = [];
    if (!form["archivo"].value) {
        errors.push("archivo");
    }
    if (!form["descripcion"].value) {
        errors.push("descripcion");
    }
    return errors;
}

function getMargenIndentado(margin, marginNum) {
    if (margin != null) {
        marginNum = parseInt(margin.substring(0, margin.indexOf('px')), 10);
    }
    marginNum += 25;
    return marginNum;
}

function getHtmlResultadosFromResponse(response, lang) {
    if (response.length > 0) {
        for (var i = 0; i < response.length; i++) {
            var html = EMPTY_DIV;
            var resultado = response[i];
            if (resultado.aFavor !== null || resultado.enContra !== null || resultado.abstenciones !== null) {
                let aFavor, enContra, abstenciones;
                if (lang === "es") {
                    aFavor = 'A favor: ' + resultado.aFavor;
                    enContra = 'En contra: ' + resultado.enContra;
                    abstenciones = 'Abstenciones: ' + resultado.abstenciones;
                } else {
                    aFavor = 'A favor: ' + resultado.aFavor;
                    enContra = 'En contra: ' + resultado.enContra;
                    abstenciones = 'Abstencions: ' + resultado.abstenciones;
                }

                html =
                    '<div class="row column">' +
                    '   <strong style="color: #1f811f">' + aFavor + '</strong>' +
                    '   <span>&nbsp;</span>' +
                    '   <strong style="color: darkred">' + enContra + '</strong>' +
                    '   <span>&nbsp;</span>' +
                    '   <strong style="color: cornflowerblue">' + abstenciones + '</strong>' +
                    '</div>';
            }
            $('div[class^=votos-prov][name=' + resultado.puntoId + ']').html(html);
        }
    }
    else {
        $('div[class^=votos-prov]').html(EMPTY_DIV);
    }
}

function loadNumeroVotosProvisionales(reunionId, puntoOrdenDiaId) {
    var staticsUrl = $('input[name=staticsUrl]').val();
    $('div[class^=votos-prov][name=' + puntoOrdenDiaId + ']').html('<div class="loading"><img src="' + staticsUrl + '/img/commons/loading.gif" alt="loading" /></div>');

    var lang = $("input[name=lang]").val();
    $.ajax({
        type: "GET",
        url: appContext + "/rest/votos/" + reunionId + "/puntosOrdenDia/" + puntoOrdenDiaId,
        success: function (response) {
            getHtmlResultadosFromResponse(response, lang);
        },
        error: function() {
            $('div[class^=votos-prov]').html(EMPTY_DIV);
        }
    });
}

function abrirVotacion(boton) {
    var reunionId = $("input[name=reunionId]").val();
    var puntoId = boton.attributes.getNamedItem("data-punto-id").value;
    loadingButtonsVotoArea(puntoId);

    $.ajax({
        type: "PUT",
        url: appContext + "/rest/votos/" + reunionId + "/puntosOrdenDia/" + puntoId + "/abre",
        success: function (response) {
            loadEstadoVotacionPuntos(reunionId);
        }
    });
}

function cerrarVotacion(boton) {
    var reunionId = $("input[name=reunionId]").val();
    var puntoId = boton.attributes.getNamedItem("data-punto-id").value;
    loadingButtonsVotoArea(puntoId);

    $.ajax({
        type: "PUT",
        url: appContext + "/rest/votos/" + reunionId + "/puntosOrdenDia/" + puntoId + "/cierra",
        success: function (response) {
            loadEstadoVotacionPuntos(reunionId);
        }
    });
}

function loadEstadoVotacionPuntos(reunionId, reload) {
    $.ajax({
        type: "GET",
        url: appContext + "/rest/votos/" + reunionId + "/estado/" +  + getVotoEnNombre(),
        success: function (response) {
            if (!reload) {
                PENDING_REQ.pop();
            }

            if (!PENDING_REQ.length) {
                $('input[data-punto-id]').prop('disabled', true);
                $('div[class^="buttons-puntos"]').hide();
                $('button[name=cerrar-votacion]').hide();
                $('button[name=abrir-votacion]').hide();

                if (response.asistencia) {
                    $('div[name="asistencia-no-confirmada"]').hide();
                    for (var i = 0; i < response.puntos.length; i++) {
                        if (response.puntos[i].votable) {
                            if (response.puntos[i].abierta) {
                                $('div[name=votacion-cerrada][data-punto-id=' + response.puntos[i].puntoId + ']').hide();
                                if (response.presideVotacion) {
                                    var cerrarVotacionButton = $('button[name=cerrar-votacion][data-punto-id=' + response.puntos[i].puntoId + ']');
                                    cerrarVotacionButton.show();
                                }

                                if (response.votante) {
                                    $('div[class^=buttons-puntos-' + response.puntos[i].puntoId + ']').show();
                                    if (!response.puntos[i].votado) {
                                        $('input[data-punto-id=' + response.puntos[i].puntoId + ']').prop('disabled', false);
                                    }
                                }
                            } else {
                                $('div[name=votacion-cerrada][data-punto-id=' + response.puntos[i].puntoId + ']').show();
                                if (response.presideVotacion) {
                                    var abrirVotacionButton = $('button[name=abrir-votacion][data-punto-id=' + response.puntos[i].puntoId + ']');
                                    abrirVotacionButton.show();
                                }
                            }
                        }
                    }
                } else {
                    $('div[name="asistencia-no-confirmada"]').show();
                }
            }

            if (reload) {
                setTimeout(function () {
                    loadEstadoVotacionPuntos(reunionId, true);
                }, RELOAD_TIMEOUT);
            }
        },
        error: function() {
            if (!reload) {
                PENDING_REQ.pop();
            }

            $('input[data-punto-id]').each(function () {
                this.disabled = true;
            });
        },
        complete: function() {
            if (!PENDING_REQ.length) {
                $('div[class^="loading-puntos"]').hide();
            }
        }
    });
}

function loadNumeroVotosProvisionalesDeTodosLosPuntos(reunionId, reload) {
    var staticsUrl = $('input[name=staticsUrl]').val();
    $('div[class^=votos-prov]').html('<div class="loading"><img src="' + staticsUrl + '/img/commons/loading.gif" alt="loading" /></div>');

    var lang = $("input[name=lang]").val();
    $.ajax({
        type: "GET",
        url: appContext + "/rest/votos/" + reunionId,
        success: function (response) {
            getHtmlResultadosFromResponse(response, lang);
            if (reload) {
                setTimeout(function () {
                    loadNumeroVotosProvisionalesDeTodosLosPuntos(reunionId, true);
                }, RELOAD_TIMEOUT);
            }
        },
        error: function() {
            $('div[class^=votos-prov]').html(EMPTY_DIV);
        }
    });
}

$(function () {
    var reunionId = $("input[name=reunionId]").val();
    var puntoOrdenDiaId = $("input[name=puntoOrdenDiaId]").val();
    var lang = $("input[name=lang]").val();

    $(document).ajaxComplete(function (event, xhr, settings) {
        if (!hasGocHeader(xhr)) {
            showReloadWarning();
        }

    });
    $(document).ajaxError(function (event, xhr, settings) {
        if (!hasGocHeader(xhr)) {
            showReloadWarning();
        }

    });

    function hasGocHeader(xhr) {
        return xhr.getResponseHeader('x-goc-server') != null && xhr.getResponseHeader('x-goc-server') == 'true';
    }

    function showReloadWarning() {
        alert(appI18N.common.sesionCaducada);
        window.location.reload(true);
    }

    function loadComentarios(reunionId, appI18N) {
        $.ajax({
            type: "GET",
            url: appContext + "/rest/reuniones/" + reunionId + "/comentarios",
            success: function (response) {
                if (response.data.length === 0) {
                    $('div.comentarios').html('');
                    return;
                }

                var html = '<h3 class="title">' + appI18N.acta.comentarios + '</h3>';

                response.data.map(function (comentario) {
                    var creador = comentario.creadorNombre ? comentario.creadorNombre : comentario.creadorId;
                    html += '<div class="row columns">';

                    if (comentario.permiteBorrado == 'true') {
                        html += '  <a href="#" class="delete-comentario" data-id="' + comentario.id + '"><i class="fa fa-times"></i></a>';
                    }

                    html +=
                        '  <div class="comentario">' +
                        '    <p class=""autor tags-comentarios"><strong>' + appI18N.acta.autor + '</strong>: ' + creador + '</p>' +
                        '    <div class="texto comentario-cuerpo">' + comentario.comentario + '</div>' +
                        '    <p class="fecha tags-comentarios"><strong>' + appI18N.acta.fecha + '</strong>: ' + comentario.fecha + '</p>' +
                        '  </div>' +
                        '</div>';
                });

                $('div.comentarios').html(html);
            }
        });
    }

    function loadResultadosBusquedaMiembros(result) {
        var items = [];
        $('div.nuevo-delegado-voto .footer button').show().prop("disabled", true);

        for (var i = 0; i < result.data.length; i++) {
            var persona = result.data[i];
            items.push('<li><input aria-label="' + persona.nombre + '" type="radio" name="persona" value="' + persona.miembroId + '" /> <label style="display: inline; margin-right: 0px;" for="delegadoVotoNombre">' + persona.nombre + '</label> <span style="display: inline">(</span><label style="display: inline" for="delegadoVotoEmail">' + persona.email + '</label><span style="display: inline">)</span></li>');
        }

        $('div.nuevo-delegado-voto ul.resultados').html(items.join('\n'));

        if (items.length > 0) {
            $('div.nuevo-delegado-voto .footer button').show().prop("disabled", false);
        }
    }

    function loadResultadosBusquedaPersonas(result) {
        var items = [];
        if (result.data.length > 0) {
            $('div.nuevo-suplente .footer button').show();

            for (var i = 0; i < result.data.length; i++) {
                var persona = result.data[i];
                items.push('<li><input aria-label="' + persona.nombre + '" type="radio" name="persona" value="' + persona.id + '"/> <label style="display: inline; margin-right: 0px;" for="suplenteNombre">' + persona.nombre + '</label> <span style="display: inline">(</span><label style="display: inline" for="suplenteEmail">' + persona.email + '</label><span style="display: inline">)</span></li>');
            }
            $('div.nuevo-suplente ul.resultados').html(items.join('\n'));
        } else {
            $('div.nuevo-suplente .footer button').hide();
            $('div.nuevo-suplente ul.resultados').html(`<li>${appI18N.reuniones.busquedaSuplenteVacia}</li>`);
        }
    }

    function buscaPersona(query) {
        $.ajax({
            type: "GET",
            url: appContext + "/rest/personas/",
            data: {query: query},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                loadResultadosBusquedaPersonas(result);
            }
        });
    }

    function buscaMiembro(query) {
        $.ajax({
            type: "GET",
            url: appContext + "/rest/reuniones/" + reunionId + "/miembros/otros",
            data: {query: query},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                loadResultadosBusquedaMiembros(result);
            }
        });
    }

    $('button[name=suplente]').on('click', function (ev) {
        $('div.nuevo-suplente input[name=organoMiembroId]').val($(this).data('miembroid'));

        var suplente = $(this).data('suplente');

        if (suplente) {
            $('div.nuevo-suplente p.suplente-actual').show();
            $('div.nuevo-suplente p.suplente-actual > strong').html(suplente);
        } else {
            $('div.nuevo-suplente p.suplente-actual').hide();
        }

        $('div.nuevo-suplente .header input[name=query-persona]').val('');
        $('div.nuevo-suplente .footer button').hide();
        $('div.nuevo-suplente ul.resultados').html('');
        $('div.nuevo-suplente').modal();
    });

    $('button[name=delegado-voto]').on('click', function (ev) {
        $('div.nuevo-delegado-voto input[name=organoMiembroId]').val($(this).data('miembroid'));

        var delegadoVoto = $(this).data('delegadoVoto');

        if (delegadoVoto) {
            $('div.nuevo-delegado-voto p.delegado-voto-actual').show();
            $('div.nuevo-delegado-voto p.delegado-voto-actual > strong').html(delegadoVoto);
        } else {
            $('div.nuevo-delegado-voto p.delegado-voto-actual').hide();
        }

        buscaMiembro($('div.nuevo-delegado-voto input[name=query-persona]').val());

        $('div.nuevo-delegado-voto .header input[name=query-persona]').val('');
        $('div.nuevo-delegado-voto .footer button').hide();
        $('div.nuevo-delegado-voto ul.resultados').html('');
        $('div.nuevo-delegado-voto').modal();
    });

    $('div.nuevo-suplente input[name=query-persona]').keypress(function (e) {
        if (e.which == 13) {
            buscaPersona($('div.nuevo-suplente input[name=query-persona]').val());
        }
    });

    $('div.nuevo-suplente button[name=busca-persona]').on('click', function (ev) {
        buscaPersona($('div.nuevo-suplente input[name=query-persona]').val());
    });

    $('button[name=borra-suplente]').on('click', function (ev) {
        borraSuplente($('div.nuevo-suplente input[name=organoMiembroId]').val());
    });

    $('button[name=borra-suplente-directo]').on('click', function (ev) {
        borraSuplente($(this).attr('data-miembroid'));
    });

    $('button[name=borra-delegado-voto]').on('click', function (ev) {
        borraDelegadoVoto($('div.nuevo-delegado-voto input[name=organoMiembroId]').val());
    });

    $('button[name=borra-delegado-voto-directo]').on('click', function (ev) {
        borraDelegadoVoto($(this).attr('data-miembroid'));
    });

    function borraSuplente(organoMiembroId) {
        var data = {
            organoMiembroId: organoMiembroId
        };

        $.ajax({
            type: "DELETE",
            url: appContext + "/rest/reuniones/" + reunionId + "/suplente",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                mostrarError(XMLHttpRequest.responseJSON.message);
            }
        });
    }

    function borraDelegadoVoto(organoMiembroId) {
        var data = {
            organoMiembroId: organoMiembroId
        };

        $.ajax({
            type: "DELETE",
            url: appContext + "/rest/reuniones/" + reunionId + "/delegadovoto",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                mostrarError(XMLHttpRequest.responseJSON.message);
            }
        });
    }

    $('button[name=add-suplente]').on('click', function (ev) {
        var suplente = $('div.nuevo-suplente ul.resultados li input[type=radio]').filter(':checked').parent('li');
        var organoMiembroId = $('div.nuevo-suplente input[name=organoMiembroId]').val();

        var data = {
            suplenteId: suplente.children('input[type=radio]').val(),
            suplenteNombre: suplente.children('label[for=suplenteNombre]').html(),
            suplenteEmail: suplente.children('label[for=suplenteEmail]').html(),
            organoMiembroId: organoMiembroId
        };

        $.ajax({
            type: "POST",
            url: appContext + "/rest/reuniones/" + reunionId + "/suplente",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                mostrarError(XMLHttpRequest.responseJSON.message);
            }
        });
    });

    $('button[name=add-delegado-voto]').on('click', function (ev) {
        var delegacionVoto = $('div.nuevo-delegado-voto ul.resultados li input[type=radio]').filter(':checked').parent('li');
        var organoMiembroId = $('div.nuevo-delegado-voto input[name=organoMiembroId]').val();

        if (delegacionVoto.length === 0) {
            mostrarError(appI18N.reuniones.seleccionRegistro);
            return;
        }

        var data = {
            delegadoVotoId: delegacionVoto.children('input[type=radio]').val(),
            delegadoVotoNombre: delegacionVoto.children('label[for=delegadoVotoNombre]').html(),
            delegadoVotoEmail: delegacionVoto.children('label[for=delegadoVotoEmail]').html(),
            organoMiembroId: organoMiembroId
        };

        $.ajax({
            type: "POST",
            url: appContext + "/rest/reuniones/" + reunionId + "/delegadovoto",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                mostrarError(XMLHttpRequest.responseJSON.message);
            }
        });
    });

    $('button.add-comentario').on('click', function () {
        $('div.nuevo-comentario').modal();
        $('div.nuevo-comentario textarea').val('');
    });

    $('button.post-comentario').on('click', function () {
        var data = {
            comentario: $('div.nuevo-comentario textarea').val(),
            reunionId: reunionId
        };

        $.ajax({
            type: "POST",
            url: appContext + "/rest/reuniones/" + reunionId + "/comentarios",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                loadComentarios(reunionId, appI18N);
                $.modal.close();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                mostrarError(XMLHttpRequest.responseJSON.message);
            }
        });
    });

    $(document).on("click", "a.delete-comentario", function (event) {
        event.preventDefault();
        var comentarioId = $(this).attr('data-id');

        if (confirm(appI18N.reuniones.deseaBorrarComentario)) {
            $.ajax({
                type: "DELETE",
                url: appContext + "/rest/reuniones/" + reunionId + "/comentarios/" + comentarioId,
                success: function () {
                    loadComentarios(reunionId, appI18N);
                }
            });
        }
    });

    $('button[name=confirmar-ausencia]').on('click', function () {
        var organoMiembroId = $(this).attr('data-miembroid');
        var justificacion = $('#motivo-ausencia').val();

        $.ajax({
            type: "GET",
            url: appContext + "/rest/reuniones/" + reunionId + "/tienesuplente/" + organoMiembroId,
            success: function (response) {
                if (response.message == 'true') {
                    if (confirm(eval('appI18N.reuniones.suplenteNoTendraEfecto'))) {
                        updateConfirmacion(false, organoMiembroId, justificacion);
                        return;
                    } else {
                        return;
                    }
                }
                updateConfirmacion(false, organoMiembroId, justificacion);
            }
        });

        return;
    });

    $('div.confirmacion button[name=confirmar], div.confirmacion button[name=denegar]').on('click', function () {
        var confirmacion = $(this).attr("name") === 'confirmar' ? true : false;
        var organoMiembroId = $(this).attr('data-miembroid');
        $('button[name=confirmar-ausencia]').attr('data-miembroid', organoMiembroId);
        if (!confirmacion) {
            $('#form-ausencia').modal();
        } else {
            updateConfirmacion(confirmacion, organoMiembroId);
        }
    });

    function updateConfirmacion(confirmacion, organoMiembroId, justificacion) {
        var data;
        if (justificacion != null && justificacion !== '') {
            data = JSON.stringify({confirmacion: confirmacion, justificacion: justificacion});
        } else {
            data = JSON.stringify({confirmacion: confirmacion});
        }
        $.ajax({
            type: "POST",
            url: appContext + "/rest/reuniones/" + reunionId + "/confirmar/" + organoMiembroId,
            data: data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.location.reload();
            },
            error: function (e) {
                mostrarError(e.responseJSON.message);
            }
        });
    }

    if ($('h1[name=votacion]')[0] === undefined) {
        loadComentarios(reunionId, appI18N);
        var runionHasVotacion = $('input[name=reunionHasVotacion]')[0].value;
        if (runionHasVotacion !== "" && runionHasVotacion !== null)
            loadNumeroVotosProvisionalesDeTodosLosPuntos(reunionId, true);
    } else {
        loadNumeroVotosProvisionalesDeTodosLosPuntos(reunionId, true);
        let selectUsuarios = $('select[name=votoEnNombre]');
        if (selectUsuarios.length) {
            selectUsuarios.change(function () {
                loadEstadoVotacionPuntos(reunionId);
            });
            selectUsuarios.val(connectedUserId);
        }
        loadEstadoVotacionPuntos(reunionId, true);
    }
});

function subirDocumentoPuntoOrdenDia(button) {
    var form = document.getElementById(button.parentNode.id);
    var errors = validarFormularioDocumentoPuntosOrdenDia(form);
    if (errors.length === 0) {
        var data = new FormData(form);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: button.parentNode.action,
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                location.reload();
            },
            error: function (e) {
                mostrarError(e.responseText);

            }
        });
    } else {
        var labels = document.getElementsByTagName('label');
        var label;
        var i;

        for (i = 0; i < labels.length; i++) {
            if (labels[i].htmlFor == form[i].id) {
                label = labels[i];
            }
        }

        for (i in errors) {
            if (label) {
                label.className += "form-invalido";
                label = null;
            } else {
                form[i].className += "form-invalido";
            }
        }
    }
}

function mostrarFomularioSubirDocumento(button) {
    document.getElementById(button.getAttribute('data-target')).style.display = 'block';
    button.style.display = 'none';
}

function eliminarError(item, label) {
    if (item.value && label) {
        label.classList.remove("form-invalido");
    } else {
        if (item.value) {
            item.classList.remove("form-invalido");
        }
    }
}

function mostrarNombre(input) {
    var fileName = '';
    var labels = document.getElementsByTagName('label');
    var label;
    for (var i = 0; i < labels.length; i++) {
        if (labels[i].htmlFor == input.id) {
            label = labels[i];
        }
    }
    if (input.value) {
        fileName = input.value.split('\\').pop();
        eliminarError(input, label);
    }

    if (fileName) {
        label.querySelector('span').innerHTML = fileName;
    }
}

function getPuntoIdByDataAttribute(element) {
    return $(element).attr('data-punto-id');
}

function getIdComentarioByDataAttribute(element) {
    return $(element).attr('data-comentario-id');
}

function getDivComentariosByPuntoId(puntoId) {
    return $('#comentarios-' + puntoId);
}

function getBotonVerRespuestas(puntoId, idComentarioInicial) {
    return $('#comentarios-' + puntoId + ' .enlace-ver-respuestas[data-comentario-id=' + idComentarioInicial + ']');
}


function getBotonVerComentarios(puntoId) {
    return $('.enlace-ver-comentarios[data-punto-id=' + puntoId + ']');
}


function getBotonOcultarComentarios(puntoId) {
    return $('.enlace-ocultar-comentarios[data-punto-id=' + puntoId + ']');
}

function getBotonOcultarRespuestas(puntoId, idComentario) {
    return $('#comentarios-' + puntoId + ' .enlace-ocultar-respuestas[data-comentario-id=' + idComentario + ']');
}


function verComentarios(element) {

    var puntoId = getPuntoIdByDataAttribute(element);
    var a = $('#comentarios-' + puntoId);
    a.show();

    var boton_ocultar = getBotonOcultarComentarios(puntoId);
    boton_ocultar.show();
    $(element).hide();
}

function cerrarComentarios(element) {
    var puntoId = getPuntoIdByDataAttribute(element);
    var a = $('#comentarios-' + puntoId);
    a.hide();

    var boton_ver = getBotonVerComentarios(puntoId);
    boton_ver.show();
    $(element).hide();
}

function verRespuestas(element) {
    var puntoId = getPuntoIdByDataAttribute(element);
    var idComentario = getIdComentarioByDataAttribute(element);
    var respuestas = $('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentario + ']');
    respuestas.show();

    var boton_ocultar = getBotonOcultarRespuestas(puntoId, idComentario);
    boton_ocultar.show();
    $(element).hide();
}

function cerrarRespuestasRecursiva(respuestas, puntoId, idComentarioInicial) {
    for (var i = 0; i < respuestas.length; i++) {
        var idComentario = getIdComentarioByDataAttribute(respuestas[i]);
        cerrarRespuestasRecursiva($('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentario + ']'), puntoId, idComentario);
    }
    if ($('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentarioInicial + ']').length > 0) {
        respuestas.hide();
        var boton_ver = getBotonVerRespuestas(puntoId, idComentarioInicial);
        var boton_ocultar = getBotonOcultarRespuestas(puntoId, idComentarioInicial);
        boton_ver.show();
        boton_ocultar.hide();
    }
}

function cerrarRespuestas(element) {
    var puntoId = getPuntoIdByDataAttribute(element);
    var idComentario = getIdComentarioByDataAttribute(element);


    cerrarRespuestasRecursiva($('#comentarios-' + puntoId + ' [data-comentario-padre-id=' + idComentario + ']'), puntoId, idComentario);
}

function abrirModalComentarioPunto(element) {
    $('div.nuevo-comentario-punto').modal();
    $('div.nuevo-comentario-punto textarea').val('');
    $('div.nuevo-comentario-punto [name=puntoId]').val(getPuntoIdByDataAttribute(element));
    var comentarioPadre = getIdComentarioByDataAttribute(element);
    if (comentarioPadre) {
        $('div.nuevo-comentario-punto [name=comentarioId]').val(comentarioPadre);
    } else {
        $('div.nuevo-comentario-punto [name=comentarioId]').val('');
    }
}

function _votar(reunionId, puntoOrdenDiaId, voto) {
    loadingButtonsVotoArea(puntoOrdenDiaId);

    $.ajax({
        type: "POST",
        url: appContext + "/rest/votos/" + reunionId + "/puntosOrdenDia/" + puntoOrdenDiaId + '/votacion',
        data: voto,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function () {
            loadNumeroVotosProvisionales(reunionId, puntoOrdenDiaId);
            loadEstadoVotacionPuntos(reunionId);
        },
        error: function() {
            $('div[class^="loading-puntos"]').hide();
            $('div[class^="buttons-puntos"]').show();
        }
    });
}

function getVotoJson(voteType) {
    var voto;
    var votoEnNombre = getVotoEnNombre();
    if (votoEnNombre !== undefined) {
        voto = JSON.stringify({voto: voteType, votoEnNombre: votoEnNombre});
    } else {
        voto = JSON.stringify({voto: voteType});
    }
    return voto;
}

function getVotoEnNombre() {
    var votoEnNombre = $('select[name=votoEnNombre]')[0];
    if (votoEnNombre !== undefined) {
        return votoEnNombre.value;
    }
    else {
        return connectedUserId;
    }
}

function votarAFavorPunto(botonFavor) {
    var reunionId = $("input[name=reunionId]").val();

    var puntoId = botonFavor.attributes.getNamedItem("data-punto-id").value;
    var botonContra = $('input[name=CONTRA][data-punto-id=' + puntoId + ']')[0];
    var botonAbstencion = $('input[name=ABSTENCION][data-punto-id=' + puntoId + ']')[0];

    if (botonContra.disabled || botonFavor.disabled || botonAbstencion.disabled) {
        alert(appI18N.votos.yaHaVotado);
        return;
    }

    var voto = getVotoJson("favor");
    _votar(reunionId, puntoId, voto);
}

function votarEnContraPunto(botonContra) {
    var reunionId = $("input[name=reunionId]").val();

    var puntoId = botonContra.attributes.getNamedItem("data-punto-id").value;
    var botonFavor = $('input[name=FAVOR][data-punto-id=' + puntoId + ']')[0];
    var botonAbstencion = $('input[name=ABSTENCION][data-punto-id=' + puntoId + ']')[0];

    if (botonContra.disabled || botonFavor.disabled || botonAbstencion.disabled) {
        alert(appI18N.votos.yaHaVotado);
        return;
    }

    var voto = getVotoJson("contra");
    _votar(reunionId, puntoId, voto);
}

function votarAbstencionPunto(botonAbstencion) {
    var reunionId = $("input[name=reunionId]").val();

    var puntoId = botonAbstencion.attributes.getNamedItem("data-punto-id").value;
    var botonFavor = $('input[name=FAVOR][data-punto-id=' + puntoId + ']')[0];
    var botonContra = $('input[name=CONTRA][data-punto-id=' + puntoId + ']')[0];

    if (botonContra.disabled || botonFavor.disabled || botonAbstencion.disabled) {
        alert(appI18N.votos.yaHaVotado);
        return;
    }

    var voto = getVotoJson("abstencion");
    _votar(reunionId, puntoId, voto);
}

function loadingButtonsVotoArea(puntoId) {
    PENDING_REQ.push(1);
    $('button[name=abrir-votacion][data-punto-id=' + puntoId + ']').hide();
    $('button[name=cerrar-votacion][data-punto-id=' + puntoId + ']').hide();
    $('div[class=buttons-puntos-' + puntoId + ']').hide();
    $('div[class=loading-puntos-' + puntoId + ']').show();
}

function resultadosProvisionalesPunto(buttonElement) {
    var lang = $('input[name=lang]').val();
    var puntoOrdenDiaId = buttonElement.attributes.getNamedItem("data-punto-id").value;
    var reunionId = buttonElement.attributes.getNamedItem("data-reunion-id").value;
    $.ajax({
        type: "GET",
        url: appContext + "/rest/votos/" + reunionId + "/puntosOrdenDia/" + puntoOrdenDiaId + "/votosprovisionales",
        success: function (response) {
            votos = response.data;

            var htmlAbstenciones;
            var htmlAFavor;
            var htmlEnContra;

            if (votos.length === 0) {
                var html = '<div><strong>' + appI18N.votos.noHayVotos + '</strong></div>';

                $('#modal-votos-provisionales').html(html);
                $('#modal-votos-provisionales').modal();
                return;
            }
            for (var i = 0; i < votos.length; i++) {
                let nombre = votos[i].nombre;
                if (votos[i].votoEnNombreDe) {
                    nombre += " (" + appI18N.votos.enRepresentacionDe + ": " + votos[i].votoEnNombreDe + ")"
                }

                if (votos[i].vote === "ABSTENCION") {
                    if (htmlAbstenciones === undefined) {
                        if (lang === "ca") {
                            htmlAbstenciones = '<strong style="color: cornflowerblue">' + 'Abstencions: ' + '</strong>' + '<li style="margin-left: 30px">' + nombre + '</li>';
                        } else {
                            htmlAbstenciones = '<strong style="color: cornflowerblue">' + 'Abstenciones: ' + '</strong>' +
                                '<li style="margin-left: 30px">' + nombre + '</li>';
                        }
                    } else
                        htmlAbstenciones += '<li style="margin-left: 30px">' + nombre + '</li>';

                } else if (votos[i].vote === "FAVOR") {
                    if (htmlAFavor === undefined) {
                        htmlAFavor = '<strong style="color: #1f811f">' + 'A favor: ' + '</strong>' + '<li style="margin-left: 30px">' + nombre + '</li>';
                    } else {
                        htmlAFavor += '<li style="margin-left: 30px">' + nombre + '</li>';
                    }

                } else if (votos[i].vote === "CONTRA") {
                    if (htmlEnContra === undefined) {
                        htmlEnContra = '<strong style="color: darkred">' + 'En contra: ' + '</strong>' + '<li style="margin-left: 30px">' + nombre + '</li>';
                    } else {
                        htmlEnContra += '<li style="margin-left: 30px">' + nombre + '</li>';
                    }
                }
            }
            if (htmlEnContra === undefined) {
                htmlEnContra = '<div>&nbsp</div>';
            }

            if (htmlAFavor === undefined) {
                htmlAFavor = '<div>&nbsp</div>';
            }
            if (htmlAbstenciones === undefined) {
                htmlAbstenciones = '<div>&nbsp</div>';
            }
            if (lang === "es") {
                html =
                    '<div class="row column">' +
                    htmlAFavor +
                    htmlEnContra +
                    htmlAbstenciones +
                    '</div>';
            } else {
                html =
                    '<div class="row column">' +
                    htmlAFavor +
                    htmlEnContra +
                    htmlAbstenciones +
                    '</div>';
            }
            $('#modal-votos-provisionales').html(html);
            $('#modal-votos-provisionales').modal();
        }
    });
}

function anyadirComentarioInsertado(respuesta) {
    var marginNum = 0;

    function procesarPlantilla(respuesta, marginNum) {
        var html =
            '  <div class="comentario" id="comentario-' + respuesta.id + '" data-comentario-id="' + respuesta.id + '" data-comentario-padre-id="' + respuesta.comentarioSuperiorId + '" style="margin-left: ' + marginNum + 'px;">' +
            '   <div class="comentario-cabecera">' +
            '    <p class="autor"><strong class="tags-comentarios">' + appI18N.comentarios.autor + '</strong>: ' + respuesta.creadorNombre + ' </p>' +
            '   </div>' +
            '   <div class="comentario-cuerpo">' +
            respuesta.comentario +
            '   <div> ' +
            '    <p class="fecha"><strong class="tags-comentarios">' + appI18N.comentarios.fecha + '</strong>: [' + respuesta.fecha + ']</p>' +
            '  </div>' +
            '  <div>' +
            '<a href="#comentario-' + respuesta.id + '" style="display: none;" onclick="verRespuestas(this)" class="enlace-ver-respuestas" data-comentario-id="' + respuesta.id + '" data-punto-id="' + respuesta.puntoOrdenDiaId + '">' + appI18N.comentarios.verRespuestas + '</a>' +
            '<a href="#comentario-' + respuesta.id + '" style="display: none;" onclick="cerrarRespuestas(this)" class="enlace-ocultar-respuestas" data-comentario-id="' + respuesta.id + '" data-punto-id="' + respuesta.puntoOrdenDiaId + '">' + appI18N.comentarios.ocultarRespuestas + '</a>' +
            '<span style="display: none" class="separador-enlaces-comentarios"> | </span>' +
            '<a href="#" onclick="abrirModalComentarioPunto(this)" class="enlace-responder-comentario" data-comentario-id="' + respuesta.id + '" data-punto-id="' + respuesta.puntoOrdenDiaId + '">' + appI18N.comentarios.responder + '</a>' +
            '  </div>' +
            '</div>';
        return html;
    }

    var html;

    if (respuesta.comentarioSuperiorId) {
        var divComentarioPadre = $('#comentario-' + respuesta.comentarioSuperiorId);
        var respuestasComentarioPadre = $('#comentarios-' + respuesta.puntoOrdenDiaId + ' [data-comentario-padre-id=' + respuesta.comentarioSuperiorId + ']');
        var margin = divComentarioPadre.css('margin-left');

        marginNum = getMargenIndentado(margin, marginNum);

        html = procesarPlantilla(respuesta, marginNum);
        $('#comentario-' + respuesta.comentarioSuperiorId + ' .separador-enlaces-comentarios').show();
        $('#comentario-' + respuesta.comentarioSuperiorId + ' .enlace-ocultar-respuestas').show();
        if (respuestasComentarioPadre.length > 0) {
            $(html).insertAfter(respuestasComentarioPadre[respuestasComentarioPadre.length - 1]);
        } else {
            $(html).insertAfter(divComentarioPadre);
        }
        $.modal.close();
    } else {
        var divComentarios = getDivComentariosByPuntoId(respuesta.puntoOrdenDiaId);
        html = procesarPlantilla(respuesta, marginNum);

        divComentarios.append(html);
        divComentarios.show();
        var boton_ocultar = getBotonOcultarComentarios(respuesta.puntoOrdenDiaId);
        boton_ocultar.show();
        $.modal.close();
    }
}

function enviarComentarioPunto(element) {
    var data = {
        comentario: $('div.nuevo-comentario-punto textarea').val(),
        reunionId: $('div.nuevo-comentario-punto [name=reunionId]').val(),
        puntoOrdenDiaId: $('div.nuevo-comentario-punto [name=puntoId]').val(),
        comentarioSuperiorId: $('div.nuevo-comentario-punto [name=comentarioId]').val()
    };

    $.ajax({
        type: "POST",
        url: appContext + "/rest/reuniones/" + data.reunionId + "/puntosOrdenDia/" + data.puntoOrdenDiaId + '/comentarios',
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (comentarioCreado) {
            anyadirComentarioInsertado(comentarioCreado.data);
        }
    });
}
