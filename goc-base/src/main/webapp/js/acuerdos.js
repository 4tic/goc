$(function()
{
    var form = $('form[name=form-busqueda]'),
        pagina = form.find('input[name=pagina]'),
        paginaActual = parseInt(pagina.val(),10);

    pagina.val(0);

    $('.collaptor').each(function(index)
    {
        $(this).prepend('<i class="fa fa-caret-right"></i>');
    });

    $(document).on("click", ".collaptor", function(event)
    {
        toggle(event, $(this).next(), $(this).children());
    });

    $(document).on("click", ".collaptor + li", function(event)
    {
        toggle(event, $(this).next(), $(this).prev());
    });

    function toggle(event, container, caret) {
        event.preventDefault();
        container.toggleClass('collapsed expanded');
    }

    $('a.nextPage').click(function()
    {
        var form = $('form[name=form-busqueda]');
        pagina.val(paginaActual + 1);
        form.submit();
    });

    $('a.prevPage').click(function()
    {
        var form = $('form[name=form-busqueda]');
        pagina.val(paginaActual - 1);
        form.submit();
    });

    $(document).ready(function()
    {
        $('input[name=fInicio]').datepicker({
            firstDay: 1,
            dateFormat: 'dd/mm/yy',
            onClose: function( selectedDate ) {
                $( "input[name=fFin]" ).datepicker( "option", "minDate", selectedDate );
            }
        });

        $('input[name=fFin]').datepicker({
            firstDay: 1,
            dateFormat: 'dd/mm/yy',
            onClose: function( selectedDate ) {
                $( "input[name=fInicio]" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
    });
});