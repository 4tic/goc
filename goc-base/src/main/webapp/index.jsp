<%@ page import="es.uji.apps.goc.auth.LanguageConfig" %>
<%@ page import="es.uji.apps.goc.auth.PersonalizationConfig" %>
<%@ page import="es.uji.apps.goc.charset.ResourceBundleUTF8" %>
<%@ page import="es.uji.commons.sso.User" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%
        User user = (User) session.getAttribute(User.SESSION_USER);
        Boolean isAdmin = (Boolean) session.getAttribute("showOrganos");
        String editOrganos = (isAdmin != null && isAdmin)? "edit": "";

        WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        LanguageConfig languageConfig = context.getBean(LanguageConfig.class);
        PersonalizationConfig personalizationConfig = context.getBean(PersonalizationConfig.class);
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18n", languageConfig.mainLanguage);
        String loadingText = resourceBundle.getString("index.loading");
        String lang = request.getParameter("lang");
        if(lang == null)
        {
            lang = languageConfig.mainLanguage;
        }
    %>
    <title><%= personalizationConfig.tituloDeAplicacion %></title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/classic/theme-triton/resources/theme-triton-all.css">
    <link rel="stylesheet" type="text/css" href="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/packages/font-awesome/resources/font-awesome-all.css">
    <link rel="stylesheet" type="text/css" href="<%= personalizationConfig.privateCustomCSS %>">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/ext-all.js"></script>
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/build/classic/locale/locale-<%= lang %>.js"></script>
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/packages/ux/classic/src/TabCloseMenu.js"></script>
    <script type="text/javascript" src="<%= personalizationConfig.staticsUrl %>/js/extjs/ext-6.2.1/packages/ux/classic/src/form/SearchField.js"></script>
    <script type="text/javascript" src="app/i18n/<%= lang %>.js?v=${project.version}"></script>

    <title>Portal</title>
    <script type="text/javascript">
        var login = '<%= user.getName() %>';
        var perId = <%= user.getId() %>;
        var mainLanguage = '<%= languageConfig.mainLanguage %>';
        var mainLanguageDescription = '<%= languageConfig.mainLanguageDescription %>';
        var alternativeLanguage = '<%= languageConfig.alternativeLanguage %>';
        var alternativeLanguageDescription = '<%= languageConfig.alternativeLanguageDescription %>';
        var host = '<%= personalizationConfig.host %>';
        var appContext = '<%= personalizationConfig.appContext %>';
        var logo = '<%= personalizationConfig.logo %>';
        var tituloAplicacion = '<%= personalizationConfig.tituloDeAplicacion %>';
        var staticsurl = '<%= personalizationConfig.staticsUrl %>';
        var exprMeetingTitle = '<%= personalizationConfig.tituloAutomatico %>';
        var exprMeetingTitleAlternative = '<%= personalizationConfig.tituloAutomaticoAlternativo %>';
        var mostrarObservaciones = <%= personalizationConfig.mostrarObservaciones %>;
        var mostrarExtraordinaria = <%= personalizationConfig.mostrarExtraordinaria %>;
        var mostrarDescripcion = <%= personalizationConfig.mostrarDescripcion %>;
        var mostrarDescripcionInvitados = <%= personalizationConfig.mostrarDescripcionInvitados %>;
        var enlaceManual= '<%= personalizationConfig.enlaceManual %>';
        var busquedaPersonas = <%= personalizationConfig.busquedaPersonas.length()>0%>;
        var bloquearRemitenteConvocatoria = <%= personalizationConfig.bloquearRemitenteConvocatoria%>;
        var editOrganos= '<%= editOrganos %>';

        function getMultiLangLabel(value, lang) {
            if (isMultilanguageApplication() && lang === mainLanguage)
                return value + ' (' + mainLanguageDescription + ')';

            if (isMultilanguageApplication() && lang === alternativeLanguage)
                return value + ' (' + alternativeLanguageDescription + ')';

            return value;
        }

        function isMultilanguageApplication() {
            return (mainLanguage && mainLanguageDescription && alternativeLanguage && alternativeLanguageDescription);
        }
    </script>

    <script type="text/javascript">

        function getValidLang(lang) {
            if (!lang || (lang.toLowerCase() !== mainLanguage && lang.toLowerCase() !== alternativeLanguage)) {
                return mainLanguage;
            }

            return lang;
        }

        var requestLang = '<%= request.getParameter("lang")%>';
        var appLang = getValidLang(requestLang);
    </script>

    <%
        if(!personalizationConfig.debug)
        {
    %>
        <script type="text/javascript" src="goc-all.js?v=${project.version}"></script>
    <%
        } else {
    %>
        <script type="text/javascript" src="app/Application.js"></script>
    <%
        }
    %>
</head>
<body>
    <div id="landing-loading">
        <img src="img/gears.gif"/>
        <p><%= loadingText %></p>
    </div>
</body>
</html>
