Ext.define('Ext.ux.uji.date.Date',
    {
        extend : 'Ext.form.field.Date',
        alias : 'widget.ujidate',

        triggerAction : 'all',
        editable : false,
        showClearIcon : false,

        triggers :
            {
                clear :
                    {
                        cls : 'x-form-clear-trigger',
                        handler : function(cmp)
                        {
                            cmp.setValue(null);
                        },
                        scope : 'this',
                        weight : -1
                    }
            },

        initEvents : function()
        {
            this.callParent(arguments);
            var trigger = this.getTrigger("clear");
            trigger.hide();

            this.addManagedListener(this.bodyEl, 'mouseover', function()
            {
                if (this.showClearIcon)
                {
                    trigger.show();
                }
            }, this);

            this.addManagedListener(this.bodyEl, 'mouseout', function()
            {
                trigger.hide();
            });
        }
    });