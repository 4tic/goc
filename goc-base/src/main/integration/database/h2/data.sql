---------------------------------------------------- CARGOS ----------------------------------------------------

INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (1, 'Rector', 'Rector', '1', 0);
INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (2, 'Vocal', 'President', '2', 0);
INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (3, 'Secretario', 'Secretari', '3', 1);
INSERT INTO GOC_CARGOS (ID, NOMBRE, NOMBRE_ALT, CODIGO, RESPONSABLE_ACTA)
VALUES (4, 'Gerente', 'Gerent', '4', 0);

---------------------------------------------------- REUNIONES ----------------------------------------------------

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA, OBSERVACIONES, EXTRAORDINARIA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA,
                           CONVOCANTE, CONVOCANTE_EMAIL, HAS_VOTACION)
VALUES (1, 'Dirección del centro completada', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 88849,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        TO_DATE('2020-04-28 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null, null, null, 1, null, 0, null, 0,
        '4tic', 'soporte@4tic.com', 1, 1, 1, null, null, null, null, null, null, 0, 1, null, null, null, null, null,
        null, null, 1, '88849', 'noreply@localhost.es', 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA, OBSERVACIONES, EXTRAORDINARIA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA,
                           CONVOCANTE, CONVOCANTE_EMAIL, HAS_VOTACION)
VALUES (2, 'Mi reunión', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 88849,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'soporte@4tic.com', 1, 0, 1, null, null, null, null, null, null, 0, 1, null, null, null, null, null,
        null, null, 0, '1', 'noreply@localhost.es', 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA, OBSERVACIONES, EXTRAORDINARIA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA,
                           CONVOCANTE, CONVOCANTE_EMAIL, HAS_VOTACION)
VALUES (3, 'Reunión abierta', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 88849,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'soporte@4tic.com', 1, 0, 1, null, null, null, null, null, null, 0, 1, null, null, null, null, null,
        null, null, 0, '1', 'noreply@localhost.es', 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA, OBSERVACIONES, EXTRAORDINARIA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA,
                           CONVOCANTE, CONVOCANTE_EMAIL, HAS_VOTACION)
VALUES (4, 'Reunión votación', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 1,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'soporte@4tic.com', 1, 0, 1, null, null, null, null, null, null, 1, 1, null, null, null,
        TO_DATE('2020-04-26 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, 0, '1', 'noreply@localhost.es', 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA, OBSERVACIONES, EXTRAORDINARIA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA,
                           CONVOCANTE, CONVOCANTE_EMAIL, HAS_VOTACION)
VALUES (5, 'Reunión con votos', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 1,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'soporte@4tic.com', 1, 0, 1, null, null, null, null, null, null, 1, 1, null, null, null,
        TO_DATE('2020-04-26 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, 0, '1', 'noreply@localhost.es', 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA, OBSERVACIONES, EXTRAORDINARIA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA,
                           CONVOCANTE, CONVOCANTE_EMAIL, HAS_VOTACION)
VALUES (6, 'Reunión test votación dirigida', TO_DATE('2059-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 1,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'soporte@4tic.com', 1, 0, 1, null, null, null, null, null, null, 1, 1, null, null, null,
        TO_DATE('2020-04-26 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, 0, '1', 'noreply@localhost.es', 1);

INSERT INTO GOC_REUNIONES (ID, ASUNTO, FECHA, DURACION, DESCRIPCION, CREADOR_ID, FECHA_CREACION, ACUERDOS,
                           FECHA_COMPLETADA, UBICACION, NUMERO_SESION, URL_GRABACION, PUBLICA,
                           MIEMBRO_RESPONSABLE_ACTA_ID, TELEMATICA, TELEMATICA_DESCRIPCION, NOTIFICADA,
                           CREADOR_NOMBRE, CREADOR_EMAIL, ADMITE_SUPLENCIA, COMPLETADA,
                           ADMITE_COMENTARIOS, FECHA_SEGUNDA_CONVOCATORIA, ASUNTO_ALT, DESCRIPCION_ALT,
                           ACUERDOS_ALT, UBICACION_ALT, TELEMATICA_DESCRIPCION_ALT, AVISO_PRIMERA_REUNION,
                           ADMITE_DELEGACION_VOTO, URL_ACTA, URL_ACTA_ALT, AVISO_PRIMERA_REUNION_USER,
                           AVISO_PRIMERA_REUNION_FECHA, OBSERVACIONES, EXTRAORDINARIA,
                           AVISO_PRIMERA_REUNION_EMAIL, REABIERTA,
                           CONVOCANTE, CONVOCANTE_EMAIL, HAS_VOTACION)
VALUES (7, 'Reunión pasada', TO_DATE('2010-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0, null, 1,
        TO_DATE('2020-04-18 10:56:11', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, null, null, 1, null, 0, null, 0,
        '4tic', 'soporte@4tic.com', 1, 0, 1, null, null, null, null, null, null, 1, 1, null, null, null,
        TO_DATE('2020-04-26 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), null,
        null, null, 0, '1', 'noreply@localhost.es', 1);

---------------------------------------------------- TIPOS ORGANOS ----------------------------------------------------
INSERT INTO GOC_TIPOS_ORGANO (ID, CODIGO, NOMBRE, NOMBRE_ALT)
VALUES (1, 'OC', 'Órgano colegiado', null);
INSERT INTO GOC_TIPOS_ORGANO (ID, CODIGO, NOMBRE, NOMBRE_ALT)
VALUES (2, 'OE', 'Órgano externo', null);

---------------------------------------------------- ORGANOS ----------------------------------------------------
INSERT INTO GOC_ORGANOS (ID, NOMBRE, TIPO_ORGANO_ID, CREADOR_ID, FECHA_CREACION, INACTIVO, NOMBRE_ALT,
                         ORDENADO)
VALUES (1, 'Junta de gobierno', 1, 88849, TO_DATE('2016-04-18 09:36:34', 'YYYY-MM-DD HH24:MI:SS'), 0, 'Junta de govern',
        null);
INSERT INTO GOC_ORGANOS (ID, NOMBRE, TIPO_ORGANO_ID, CREADOR_ID, FECHA_CREACION, INACTIVO, NOMBRE_ALT,
                         ORDENADO)
VALUES (2, 'Dirección general', 2, 88849, TO_DATE('2016-04-18 09:36:34', 'YYYY-MM-DD HH24:MI:SS'), 0,
        'Direcció general', null);

---------------------------------------------------- ORGANOS REUNIONES ----------------------------------------------------

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (1, 1, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (2, 2, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (3, 3, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (4, 4, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (5, 5, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (6, 6, 'Órgano sin dirección de voto', 1, 0, '1', 'Órgan sense direcció de vot');

INSERT INTO GOC_ORGANOS_REUNIONES (ID, REUNION_ID, ORGANO_NOMBRE, TIPO_ORGANO_ID, EXTERNO, ORGANO_ID,
                                   ORGANO_NOMBRE_ALT)
VALUES (7, 7, 'Junta de gobierno', 1, 0, '1', 'Junta de govern');

---------------------------------------------------- MIEMBROS ----------------------------------------------------

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID_NEW, PERSONA_ID,
                          DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO, FIRMANTE, VOTANTE)
VALUES (1, 'Test', 'test@test.com', 1, 1, 88849, null, null, null, null, null, 1, 1);

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID_NEW, PERSONA_ID,
                          DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO, FIRMANTE, VOTANTE)
VALUES (2, 'Delegado', 'delegado@test.com', 1, 1, 1, null, null, null, null, null, 1, 1);

INSERT INTO GOC_MIEMBROS (ID, NOMBRE, EMAIL, ORGANO_ID, CARGO_ID_NEW, PERSONA_ID,
                          DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO, FIRMANTE, VOTANTE, PRESIDE_VOTACION)
VALUES (3, 'Nombre', 'directoriVotacion@test.com', 2, 1, 88849, null, null, null, null, null, 1, 1, 1);

---------------------------------------------------- REUNION MIEMBROS ----------------------------------------------------

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            CARGO_CODIGO, URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO,
                                            MOTIVO_AUSENCIA, FIRMANTE, VOTANTE)
VALUES (1, 2, 0, 2, '1', '4tic', 'soporte@4tic.com', 1, '1', 'Rector', null, null,
        null, 88849, null, null, null, null, '1', null, null, null, null, null, 0, null, null, null, null, null, null,
        1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            CARGO_CODIGO, URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO,
                                            MOTIVO_AUSENCIA, FIRMANTE, VOTANTE)
VALUES (2, 3, 0, 3, '1', '4tic', 'soporte@4tic.com', 1, '1', 'Rector', null, null,
        null, 88849, null, null, null, null, '1', null, null, null, null, null, 0, null, null, null, null, null, null,
        1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            CARGO_CODIGO, URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO,
                                            MOTIVO_AUSENCIA, FIRMANTE, VOTANTE)
VALUES (3, 4, 0, 4, '1', '4tic', 'soporte@4tic.com', 1, '1', 'Rector', null, null,
        null, 88849, null, null, null, null, '1', null, null, null, null, null, 0, null, null, null, null, null, null,
        1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            CARGO_CODIGO, URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO,
                                            MOTIVO_AUSENCIA, FIRMANTE, VOTANTE)
VALUES (4, 4, 0, 4, '1', 'No asistente', 'noasiste@4tic.com', 1, '1', 'Rector', 88849, '4tic',
        'soporte4tic.com', 1, null, 88849, '4TIC Delegado', 'soportedelegado@4tic.com', '1', null, null, null, null,
        null, 0, null, null, null, null, null, null,
        1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            CARGO_CODIGO, URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO,
                                            MOTIVO_AUSENCIA, FIRMANTE, VOTANTE)
VALUES (5, 5, 0, 5, '1', 'No asistente', 'noasiste@4tic.com', 1, '1', 'Rector', 88849, '4tic',
        'soporte4tic.com', 1, null, 88849, '4TIC Delegado', 'soportedelegado@4tic.com', '1', null, null, null, null,
        null, 0, null, null, null, null, null, null,
        1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            CARGO_CODIGO, URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO,
                                            MOTIVO_AUSENCIA, FIRMANTE, VOTANTE)
VALUES (6, 5, 0, 5, '1', 'devel', 'devel@4tic.com', null, '1', 'Rector', null, null,
        null, 88849, null, null, null, null, '1', null, null, null, null, null, 0, null, null, null, null, null, null,
        1, 1);

INSERT INTO GOC_ORGANOS_REUNIONES_MIEMBROS (ID, ORGANO_REUNION_ID, ORGANO_EXTERNO, REUNION_ID, ORGANO_ID,
                                            NOMBRE, EMAIL, ASISTENCIA, CARGO_ID, CARGO_NOMBRE,
                                            SUPLENTE_ID, SUPLENTE_NOMBRE,
                                            SUPLENTE_EMAIL, MIEMBRO_ID, CARGO_NOMBRE_ALT,
                                            DELEGADO_VOTO_ID, DELEGADO_VOTO_NOMBRE, DELEGADO_VOTO_EMAIL,
                                            CARGO_CODIGO, URL_ASISTENCIA, URL_ASISTENCIA_ALT, CONDICION,
                                            CONDICION_ALT, JUSTIFICA_AUSENCIA, RESPONSABLE_ACTA,
                                            DESCRIPCION, DESCRIPCION_ALT, ORDEN, GRUPO, ORDEN_GRUPO,
                                            MOTIVO_AUSENCIA, FIRMANTE, VOTANTE)
VALUES (7, 6, 0, 6, '1', '4tic', 'soporte@4tic.com', 1, '1', 'Rector', null, null,
        null, 88849, null, null, null, null, '1', null, null, null, null, null, 0, null, null, null, null, null, null,
        1, 1);

---------------------------------------------------- PUNTOS ORDEN DÍA ----------------------------------------------------

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA)
VALUES (1, 'Punto orden día ficheros', null, 20, 2, null, null, 0, null, null, null, null, null, null, null,
        null, null, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA)
VALUES (2, 'Punto orden día', null, 1, 3, null, null, 0, null, null, null, null, null, null, null,
        null, null, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (3, 'Punto orden día votación privada', null, 1, 4, null, null, 0, null, null, null, null, null, null, null,
        null, null, 0, TO_DATE('2020-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (4, 'Punto orden día votación pública', null, 1, 4, null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, TO_DATE('2020-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (5, 'Punto orden día con votos', null, 1, 5, null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, TO_DATE('2020-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (6, 'Punto orden día sin votos', null, 2, 5, null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, TO_DATE('2020-04-26 07:00:00', 'YYYY-MM-DD HH24:MI:SS'), 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (7, 'Punto #1 votación pública', null, 1, 6, null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, null, 1);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (8, 'Punto #2 votación privada', null, 2, 6, null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, null, 0);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (9, 'Punto pasado', null, 2, 7, null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, null, null);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (10, 'Punto privado título secreto no visible', null, 1, 1, null, null, 0, null, null, null, null, null, null, null,
        null, null, 0, null, null);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (11, 'Punto público título visible', null, 2, 1, null, null, 1, null, null, null, null, null, null, null,
        null, null, 0, null, null);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (12, 'Subpunto privado título secreto no visible', null, 3, 1, null, null, 0, null, null, null, null, null, null, 11,
        null, null, 0, null, null);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (13, 'Subpunto público título visible', null, 4, 1, null, null, 1, null, null, null, null, null, null, 11,
        null, null, 0, null, null);

INSERT INTO GOC_REUNIONES_PUNTOS_ORDEN_DIA (ID, TITULO, DESCRIPCION, ORDEN, REUNION_ID, ACUERDOS,
                                            DELIBERACIONES, PUBLICO, TITULO_ALT, DESCRIPCION_ALT,
                                            ACUERDOS_ALT, DELIBERACIONES_ALT, URL_ACTA, URL_ACTA_ALT,
                                            ID_PUNTO_SUPERIOR, URL_ACTA_ANTERIOR, URL_ACTA_ANTERIOR_ALT,
                                            EDITADO_EN_REAPERTURA, FECHA_APERTURA_VOTACION, VOTO_PUBLICO)
VALUES (14, 'Subpunto público no visible al ser subpunto privado', null, 5, 1, null, null, 1, null, null, null, null, null, null, 12,
        null, null, 0, null, null);
---------------------------------------------------- VOTOS ----------------------------------------------------

INSERT INTO GOC_VOTOS (ID, REUNION_ID, PUNTO_ID, PERSONA_ID, MIEMBRO_ID, NOMBRE_VOTANTE, VOTO)
VALUES (1, 5, 5, 88849, 5, '4tic', 'FAVOR');