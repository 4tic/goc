package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.ReunionNoAdmiteComentariosException;
import es.uji.apps.goc.model.ComentarioPuntoOrdenDia;
import es.uji.apps.goc.services.PuntoOrdenDiaComentarioService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("/reuniones/{reunionId}/puntosOrdenDia/{puntoOrdenDiaId}/comentarios")
public class ReunionPuntosOrdenDiaComentariosResource extends CoreBaseService
{

    @PathParam("reunionId")
    Long reunionId;

    @PathParam("puntoOrdenDiaId")
    Long puntoORdenDiaId;

    @InjectParam
    PuntoOrdenDiaComentarioService puntoOrdenDiaComentarioService;

    @InjectParam
    ReunionService reunionService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addComentarioPuntoOrdenDia(UIEntity puntoOrdenDiaComentarioUI)
        throws PersonasExternasException, ReunionNoAdmiteComentariosException, Exception
    {
        ComentarioPuntoOrdenDia comentarioPuntoOrdenDia = toComentarioPuntoOrdenDia(puntoOrdenDiaComentarioUI);
        Reunion reunion = reunionService.getReunionConMiembrosAndPuntosDiaById(reunionId);
        if(reunion.isAdmiteComentarios())
        {
            ComentarioPuntoOrdenDia comentarioPuntoOrdenDiaCreado = puntoOrdenDiaComentarioService.addComentario(reunion, comentarioPuntoOrdenDia);

            return UIEntity.toUI(comentarioPuntoOrdenDiaCreado);
        }
        throw new ReunionNoAdmiteComentariosException("Esta reunión no admite comentarios");
    }

    private ComentarioPuntoOrdenDia toComentarioPuntoOrdenDia(UIEntity puntoOrdenDiaComentarioUI)
    {
        ComentarioPuntoOrdenDia comentarioPuntoOrdenDia = new ComentarioPuntoOrdenDia();
        comentarioPuntoOrdenDia.setComentario(puntoOrdenDiaComentarioUI.get("comentario"));
        comentarioPuntoOrdenDia.setCreadorId(AccessManager.getConnectedUserId(request));
        comentarioPuntoOrdenDia.setFecha(new Date());
        comentarioPuntoOrdenDia.setPuntoOrdenDiaId(puntoORdenDiaId);
        String comentaruioSuperiorString = puntoOrdenDiaComentarioUI.get("comentarioSuperiorId");
        if (comentaruioSuperiorString != null && !comentaruioSuperiorString.isEmpty())
        {
            Long comentarioSuperiorId = Long.valueOf(comentaruioSuperiorString);
            if (comentarioSuperiorId != null)
            {
                comentarioPuntoOrdenDia.setComentarioSuperiorId(comentarioSuperiorId);
            }
        }
        return comentarioPuntoOrdenDia;
    }
}
