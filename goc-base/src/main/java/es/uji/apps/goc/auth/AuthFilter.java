package es.uji.apps.goc.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import es.uji.apps.goc.Utils;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.services.PersonaService;
import es.uji.commons.sso.User;

public class AuthFilter implements Filter
{
    private FilterConfig filterConfig = null;

    @Autowired
    SecurityAuth securityAuth;

    @Autowired
    PersonaService personaService;

    @Value("${goc.external.authToken}")
    public String authToken;

    @Value("${goc.auth.validation:false}")
    public boolean userValidation;

    @Value("${goc.context}")
    public String appContext;

    @Override
    public void init(FilterConfig filterConfig)
    {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(
        ServletRequest request,
        ServletResponse response,
        FilterChain filterChain
    ) throws IOException, ServletException
    {
        HttpServletRequest clientRequest = (HttpServletRequest) request;
        HttpServletResponse clientResponse = (HttpServletResponse) response;

        String url = clientRequest.getRequestURI();
        String headerAuthToken = clientRequest.getHeader("X-UJI-AuthToken");
        String headerAuthUser = clientRequest.getHeader("X-UJI-AuthUser");

        if (isCorrectExternalAPICall(url, headerAuthToken, headerAuthUser) || sessionAlreadyRegistered(clientRequest)
            || isForbidenPage(url) || isExcludedUrl(url))
        {
            User user = (User) clientRequest.getSession().getAttribute(User.SESSION_USER);
            if (userValidation && isIndexPage(url) && !securityAuth.isValidUser(user)) {
                clientResponse.sendRedirect(appContext + "/rest/publicacion/reuniones");
            }
            else
            {
                filterChain.doFilter(request, response);
            }
            return;

        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user;
        if (authentication != null && authentication.getCredentials() instanceof Persona && isTokenValid(
            headerAuthToken))
        {
            user = createUserFromPersona((Persona) authentication.getCredentials());
        } else
        {
            user = securityAuth.getUser();
        }

        if (user == null)
        {
            clientResponse.sendRedirect(appContext + "/forbidden.jsp");
        }
        else {
                registerUserInHttpSession(clientRequest, user);

            if (userValidation && isIndexPage(url) && !securityAuth.isValidUser(user)) {
                clientResponse.sendRedirect(appContext + "/rest/publicacion/reuniones");
            }
            else
            {
                filterChain.doFilter(request, response);
            }
        }
    }

    private boolean isForbidenPage(String url)
    {
        return (url != null && url.contains("forbidden.jsp"));
    }

    private boolean isIndexPage(String url)
    {
        return (url != null && (url.equals(appContext + "/") || url.contains("index.jsp")));
    }

    private boolean sessionAlreadyRegistered(HttpServletRequest clientRequest)
    {
        return clientRequest.getSession().getAttribute(User.SESSION_USER) != null;
    }

    private void registerUserInHttpSession(
        HttpServletRequest clientRequest,
        User user
    )
    {
        HttpSession serverSession = clientRequest.getSession();
        serverSession.setAttribute(User.SESSION_USER, user);
        try {
            serverSession.setAttribute("showOrganos", personaService.isAdmin(user.getId()));
        } catch (RolesPersonaExternaException e) {
            serverSession.setAttribute("showOrganos", false);
            e.printStackTrace();
        }
    }

    private boolean isExcludedUrl(String url)
    {
        return Utils.matchesRegExp(url, this.filterConfig.getInitParameter("excludedUrls"));
    }

    private boolean isCorrectExternalAPICall(
        String url,
        String headerAuthToken,
        String headerAuthUser
    )
    {
        if (headerAuthToken != null && headerAuthUser == null)
        {
            if (authToken != null && headerAuthToken.equals(authToken))
            {
                return true;
            }
        }

        return false;
    }

    private User createUserFromPersona(Persona persona)
    {
        User user = new User();
        user.setId(persona.getId());
        user.setName(persona.getNombre());
        user.setActiveSession(UUID.randomUUID().toString());

        return user;
    }

    private boolean isTokenValid(String headerAuthToken)
    {
        if (authToken != null && headerAuthToken.equals(authToken))
        {
            return true;
        }
        return false;
    }

    @Override
    public void destroy()
    {
    }
}