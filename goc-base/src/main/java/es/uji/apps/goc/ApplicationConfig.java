package es.uji.apps.goc;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationVersion;

public class ApplicationConfig {
    private String jdbcUrl;
    private String dbUser;
    private String dbPassword;
    private Boolean debug;

    public ApplicationConfig() {
    }

    public ApplicationConfig(String jdbcUrl, String dbUser, String dbPassword) {
        this.jdbcUrl = jdbcUrl;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    public void initialize() {
        databaseMigrate();
    }

    private void databaseMigrate() {
        String enableFlyway = System.getProperty("flyway");
        if (enableFlyway == null || !enableFlyway.equals("false")) {
            String dbType = jdbcUrl.split(":")[1];

            Flyway flyway = new Flyway();
            if (enableFlyway != null && enableFlyway.equals("develop")) {
                flyway.setOutOfOrder(true);
            }
            flyway.setLocations("classpath:database/" + dbType);
            flyway.setDataSource(jdbcUrl, dbUser, dbPassword);
            flyway.setBaselineOnMigrate(true);
            flyway.setBaselineVersion(MigrationVersion.fromVersion("0"));
            flyway.repair();
            flyway.migrate();
        }
    }
}
