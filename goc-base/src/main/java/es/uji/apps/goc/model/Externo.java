package es.uji.apps.goc.model;

import es.uji.apps.goc.dto.ReunionExternos;

public class Externo {
    private Long id;
    private String nombre;
    private String email;
    private Long reunionId;
    private Long personaId;

    public Externo (){

    }

    public Externo(ReunionExternos reunionExterno) {
        this.id = reunionExterno.getId();
        this.nombre = reunionExterno.getNombre();
        this.email = reunionExterno.getEmail();
        this.reunionId = reunionExterno.getReunionId();
        this.personaId = reunionExterno.getPersonaId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getReunionId() {
        return reunionId;
    }

    public void setReunionId(Long reunionId) {
        this.reunionId = reunionId;
    }

    public Long getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Long personaId) {
        this.personaId = personaId;
    }

    public ReunionExternos toReunionExterno(){
        ReunionExternos externo = new ReunionExternos();
        if(this.id!= null){
            externo.setId(this.id);
        }
        externo.setEmail(this.email);
        externo.setNombre(this.nombre);
        externo.setPersonaId(this.personaId);
        externo.setReunionId(this.reunionId);
        return externo;
    }
}
