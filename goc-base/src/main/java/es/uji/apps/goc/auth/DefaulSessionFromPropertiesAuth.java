package es.uji.apps.goc.auth;

import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.UUID;

import es.uji.commons.sso.User;

public class DefaulSessionFromPropertiesAuth implements SecurityAuth
{
    @Autowired
    DefaultUser defaultUser;

    public User getUser() throws IOException
    {
        User user = createUserFromDefaulLocalValues(defaultUser.defaultUsername, defaultUser.defaultUserId);
        return user;
    }

    private User createUserFromDefaulLocalValues(
        String userName,
        String userId
    ) throws IOException
    {
        try
        {
            User user = new User();
            user.setId(Long.parseLong(userId));
            user.setName(userName);
            user.setActiveSession(UUID.randomUUID().toString());

            return user;
        } catch (Exception e)
        {
            throw new IOException("No se ha podido recuperar el id del usuario conectado");
        }
    }

    public boolean isValidUser(User user)
    {
        return true;
    }

    public boolean isDeveloperUser(User user)
    {
        return true;
    }

    public boolean isAdminUser(User user)
    {
        return true;
    }
}