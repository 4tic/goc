package es.uji.apps.goc.services;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.Role;
import es.uji.apps.goc.model.TipoOrgano;
import es.uji.commons.rest.StringUtils;

@Service
public class ExternalService {
    List<Persona> personas = new ArrayList<>();

    public ExternalService() {
        personas.add(new Persona(1L, "Javier Pérez", "jperez@example.org"));
        personas.add(new Persona(2L, "Joaquin Rodríguez", "jrodriguez@example.org"));
        personas.add(new Persona(3L, "Antonio Fernández", "afernandez@example.org"));
        personas.add(new Persona(4L, "Luis Domínguez", "ldominguez@example.org"));
        personas.add(new Persona(5L, "Jose Ruíz", "jruiz@example.org"));
        personas.add(new Persona(6L, "Ana Esteve", "aesteve@example.org"));
        personas.add(new Persona(7L, "Sonia Rovira", "srovira@example.org"));
        personas.add(new Persona(8L, "Natalia Soraya", "nsoraya@example.org"));
        personas.add(new Persona(9L, "Andrea Miguelez", "amiguelez@example.org"));
        personas.add(new Persona(10L, "María Suárez", "msuarez@example.org"));
        personas.add(new Persona(11L, "Externo", "externo@example.org"));
        personas.add(new Persona(88849L, "4tic", "nicolas.manero@example.org"));
    }

    public List<Organo> getOrganosExternos(Long connectedUserId) {
        List<Organo> listaOrganos = new ArrayList<>();

        TipoOrgano t1 = new TipoOrgano(5L, "98", "Departamento Interno de prueba", "Departament Intern de prova");
        TipoOrgano t2 =
            new TipoOrgano(6L, "121", "Unidad de Gestión Interno de prueba", "Unitat de Gestió Interna de prova");

        for (Integer i = 1; i < 10; i++) {
            listaOrganos.add(new Organo("E" + i.toString(), "Organo " + i.toString() + " Interno de prueba",
                "Órgan " + i.toString() + " Intern de prova", t1));
        }

        return listaOrganos;
    }

    public List<Miembro> getMiembrosByOrganoId(
        String organoId,
        Long connectedUserId
    ) {
        List<Miembro> listaMiembros = new ArrayList<>();

        Organo organo = new Organo();
        organo.setId(organoId);
        organo.setNombre("Organo Interno " + organoId + " de prueba");
        organo.setNombreAlternativo("Órgan Intern " + organoId + " de prova");

        Cargo c1 = new Cargo("1");
        c1.setCodigo("PR");
        c1.setNombre("Presidente");
        c1.setNombreAlternativo("President");

        Miembro m1 =
            new Miembro(1L, "Miembro 1 Organo " + organo.getId(), "miembro1@organo" + organo.getId() + ".com", organo,
                c1);
        m1.setCondicion("Condición 1");

        listaMiembros.add(m1);

        Cargo c2 = new Cargo("2");
        c2.setCodigo("VO");
        c2.setNombre("Vocal");
        c2.setNombreAlternativo("Vocal");

        for (Long i = 2L; i < 10L; i++) {
            Miembro miembro = new Miembro(i, "Miembro " + i.toString() + " Organo " + organo.getId(),
                "miembro" + i.toString() + "@organo" + organo.getId() + ".com", organo, c2);
            miembro.setCondicion("condicion " + i);
            miembro.setCondicionAlternativa("condicion alternativa " + i);
            listaMiembros.add(miembro);
        }

        return listaMiembros;
    }

    public List<Persona> getPersonasByQueryString(
        String query,
        Long connectedUserId
    ) {
        return filterPersonasByQuery(personas, query);
    }

    public List<Persona> getPersonasLocalByQueryString(
        String query,
        Long connectedUserId
    ) {
        List<Persona> personasExternas = new ArrayList<>();
        personasExternas.add(new Persona(1000L, "Externo Local", "externolocal@example.org"));

        return filterPersonasByQuery(personasExternas, query);
    }

    private List<Persona> filterPersonasByQuery(
        List<Persona> personas,
        String query
    ) {
        personas = personas.stream().filter(persona -> persona.getHabilitada() != null && persona.getHabilitada())
            .collect(Collectors.toList());
        if (query == null) {
            return personas;
        } else {
            return personas.stream().filter(persona -> StringUtils.limpiaAcentos(persona.getNombre()).toLowerCase()
                .contains(StringUtils.limpiaAcentos(query).toLowerCase()) || StringUtils
                .limpiaAcentos(persona.getEmail().toLowerCase())
                .contains(StringUtils.limpiaAcentos(query).toLowerCase())).collect(Collectors.toList());
        }
    }

    public Persona getPersonaById(
        Long personaId,
        Long connectedUserId
    ) {
        List<Persona> personasConId =
            getPersonasByQueryString("", 1L).stream().filter(persona -> persona.getId().equals(personaId))
                .collect(Collectors.toList());
        return personasConId != null && personasConId.size() > 0 ? personasConId.get(0) : null;
    }

    public List<Role> getRolesByPersonaId(Long userId) {
        return Arrays.asList(Role.values());
    }

    public void insertPersona(Persona persona) {
        persona.setId(Long.valueOf(personas.size() + 1));
        persona.setHabilitada(true);
        personas.add(persona);
    }

    public void deshabilitaPersona(Long personaId) {
        personas.stream().filter(p -> p.getId() == personaId).findAny().ifPresent(p -> p.setHabilitada(false));
    }

    public void modificaPersona(
        Long personaId,
        Persona persona
    ) {
        personas.stream().filter(p -> p.getId() == personaId).findAny().ifPresent(p -> {
            p.setNombre(persona.getNombre());
            p.setLogin(persona.getLogin());
        });
    }
}
