package es.uji.apps.goc.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

import es.uji.apps.goc.dto.EnvioOficio;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Oficio
{
    private Long id;
    private String nombre;
    private String email;
    private String cargo;
    private String unidad;
    private String registroSalida;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    private Date fechaEnvio;
    private Long puntoOrdenDiaId;
    private String tituloReunion;
    private String tituloReunionAlternativo;
    private String tituloPuntoOrdenDia;
    private String tituloPuntoOrdenDiaAlternativo;

    public Oficio()
    {
    }

    public Oficio(
        Long id,
        String nombre,
        String email,
        String cargo,
        String unidad,
        String registroSalida,
        Date fechaEnvio,
        Long puntoOrdenDiaId
    )
    {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.cargo = cargo;
        this.unidad = unidad;
        this.registroSalida = registroSalida;
        this.fechaEnvio = fechaEnvio;
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public Oficio(
        String nombre,
        String email,
        String cargo,
        String unidad,
        String registroSalida,
        Date fechaEnvio,
        Long puntoOrdenDiaId
    )
    {
        this.nombre = nombre;
        this.email = email;
        this.cargo = cargo;
        this.unidad = unidad;
        this.registroSalida = registroSalida;
        this.fechaEnvio = fechaEnvio;
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public static Oficio dtoToModel(EnvioOficio o)
    {
        Oficio oficio = new Oficio();
        oficio.setId(o.getId());
        oficio.setNombre(o.getNombre());
        oficio.setFechaEnvio(o.getFechaEnvio());
        oficio.setCargo(o.getCargo());
        oficio.setEmail(o.getEmail());
        oficio.setPuntoOrdenDiaId(o.getPuntoOrdenDia().getId());
        oficio.setRegistroSalida(o.getRegistroSalida());
        oficio.setUnidad(o.getUnidad());
        oficio.setTituloReunion(o.getPuntoOrdenDia().getReunion().getAsunto());
        oficio.setTituloReunionAlternativo(o.getPuntoOrdenDia().getReunion().getAsuntoAlternativo());
        oficio.setTituloPuntoOrdenDia(o.getPuntoOrdenDia().getTitulo());
        oficio.setTituloPuntoOrdenDiaAlternativo(o.getPuntoOrdenDia().getTituloAlternativo());
        return oficio;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCargo()
    {
        return cargo;
    }

    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

    public String getUnidad()
    {
        return unidad;
    }

    public void setUnidad(String unidad)
    {
        this.unidad = unidad;
    }

    public String getRegistroSalida()
    {
        return registroSalida;
    }

    public void setRegistroSalida(String registroSalida)
    {
        this.registroSalida = registroSalida;
    }

    public Date getFechaEnvio()
    {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio)
    {
        this.fechaEnvio = fechaEnvio;
    }

    public Long getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(Long puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public String getTituloReunion()
    {
        return tituloReunion;
    }

    public void setTituloReunion(String tituloReunion)
    {
        this.tituloReunion = tituloReunion;
    }

    public String getTituloReunionAlternativo()
    {
        return tituloReunionAlternativo;
    }

    public void setTituloReunionAlternativo(String tituloReunionAlternativo)
    {
        this.tituloReunionAlternativo = tituloReunionAlternativo;
    }

    public String getTituloPuntoOrdenDia()
    {
        return tituloPuntoOrdenDia;
    }

    public void setTituloPuntoOrdenDia(String tituloPuntoOrdenDia)
    {
        this.tituloPuntoOrdenDia = tituloPuntoOrdenDia;
    }

    public String getTituloPuntoOrdenDiaAlternativo()
    {
        return tituloPuntoOrdenDiaAlternativo;
    }

    public void setTituloPuntoOrdenDiaAlternativo(String tituloPuntoOrdenDiaAlternativo)
    {
        this.tituloPuntoOrdenDiaAlternativo = tituloPuntoOrdenDiaAlternativo;
    }

    public EnvioOficio toEnvioOficioDTO()
    {
        EnvioOficio envioOficio = new EnvioOficio();
        envioOficio.setId(getId());
        envioOficio.setNombre(getNombre());
        envioOficio.setEmail(getEmail());
        envioOficio.setUnidad(getUnidad());
        envioOficio.setCargo(getCargo());
        envioOficio.setFechaEnvio(getFechaEnvio());
        envioOficio.setRegistroSalida(getRegistroSalida());
        return envioOficio;
    }
}
