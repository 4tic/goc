package es.uji.apps.goc.model;

public enum ReunionEstados {
    ABIERTA, CERRADA_CON_FIRMA, CERRADA
}
