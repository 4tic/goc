package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.ReunionFirma;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.firmas.FirmaService;
import es.uji.apps.goc.model.Menu;
import es.uji.apps.goc.model.MenuIconEnum;
import es.uji.apps.goc.model.MenuItem;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.RespuestaFirma;
import es.uji.apps.goc.model.RespuestaFirmaAsistencia;
import es.uji.apps.goc.model.RespuestaFirmaPuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.model.Role;
import es.uji.apps.goc.notifications.CanNotSendException;
import es.uji.apps.goc.notifications.MailSender;
import es.uji.apps.goc.notifications.Mensaje;
import es.uji.apps.goc.services.ExternalService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.apps.goc.services.rest.ui.WrappedMiembros;
import es.uji.apps.goc.services.rest.ui.WrappedOrganos;
import es.uji.apps.goc.services.rest.ui.WrappedPersona;
import es.uji.apps.goc.services.rest.ui.WrappedPersonas;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import static java.util.stream.Collectors.toList;

@Path("external")
@Api(value = "/external", tags = "external", description = "Servicios relacionados con la integración con aplicaciones externas a GOC")
public class ExternalResource extends CoreBaseService
{
    @InjectParam
    private ExternalService externalService;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private MailSender mailSender;

    @InjectParam
    private FirmaService firmaService;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @GET
    @Path("organos")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Órganos externos a GOC disponibles", notes = "Obtiene una lista completa de los órganos disponibles externos a GOC, de los sistemas propios de la entidad", response = WrappedOrganos.class)
    public List<UIEntity> getOrganosExternos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Organo> listaOrganos = externalService.getOrganosExternos(connectedUserId);

        return organoToUI(listaOrganos);
    }

    private List<UIEntity> organoToUI(List<Organo> listaOrganos)
    {
        List<UIEntity> organosUI = new ArrayList<>();

        for (Organo organo : listaOrganos)
        {
            UIEntity organoUI = new UIEntity();

            organoUI.put("id", organo.getId());
            organoUI.put("nombre", organo.getNombre());
            organoUI.put("nombre_alternativo", organo.getNombreAlternativo());
            organoUI.put("tipo_id", organo.getTipoOrgano().getId());
            organoUI.put("tipo_codigo", organo.getTipoOrgano().getCodigo());
            organoUI.put("tipo_nombre", organo.getTipoOrgano().getNombre());
            organoUI.put("tipo_nombre_alternativo", organo.getTipoOrgano().getNombreAlternativo());

            organosUI.add(organoUI);
        }

        return organosUI;
    }

    @GET
    @Path("organos/{organoId}/miembros")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Miembros pertenecientes a un órgano externo a GOC determinado", notes = "Obtiene una lista de todos los miembros pertenecientes a un determinado órgano externo a GOC, de los sistemas propios de la entidad", response = WrappedMiembros.class)
    public List<UIEntity> getMiembrosByOrganoExternoId(@PathParam("organoId") String organoId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Miembro> listaMiembros = externalService.getMiembrosByOrganoId(organoId, connectedUserId);

        return miembroToUI(listaMiembros);
    }

    private List<UIEntity> miembroToUI(List<Miembro> listaMiembros)
    {
        List<UIEntity> miembrosUI = new ArrayList<>();

        for (Miembro miembro : listaMiembros)
        {
            UIEntity miembroUI = new UIEntity();

            miembroUI.put("id", miembro.getId());
            miembroUI.put("nombre", miembro.getNombre());
            miembroUI.put("email", miembro.getEmail());
            miembroUI.put("organo_id", miembro.getOrgano().getId());
            miembroUI.put("organo_nombre", miembro.getOrgano().getNombre());
            miembroUI.put("organo_nombre_alternativo", miembro.getOrgano().getNombreAlternativo());
            miembroUI.put("condicion", miembro.getCondicion());
            miembroUI.put("condicion_alternativa", miembro.getCondicionAlternativa());
            miembroUI.put("cargo_id", miembro.getCargo().getId());
            miembroUI.put("cargo_nombre", miembro.getCargo().getNombre());
            miembroUI.put("cargo_nombre_alternativo", miembro.getCargo().getNombreAlternativo());

            miembrosUI.add(miembroUI);
        }
        return miembrosUI;
    }

    @GET
    @Path("personas")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Directorio de personas de la institución", notes = "A partir de una consulta devuelve un listado de personas pertenecientes a la institución externas a GOC", response = WrappedPersonas.class)
    public List<Persona> getPersonaByQueryString(@QueryParam("query") String query)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Persona> listaPersonas = externalService.getPersonasByQueryString(query, connectedUserId);

        return listaPersonas;
    }

    @GET
    @Path("personas/locales")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> getPersonasLocalesByQueryString(@QueryParam("query") String query)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Persona> listaPersonas = externalService.getPersonasLocalByQueryString(query, connectedUserId);

        return listaPersonas;
    }

    private List<UIEntity> personasToUI(List<Persona> listaPersonas)
    {
        List<UIEntity> personasUI = new ArrayList<>();

        for (Persona persona : listaPersonas)
        {
            personasUI.add(personaToUI(persona));
        }
        return personasUI;
    }

    private UIEntity personaToUI(Persona persona)
    {
        UIEntity personaUI = new UIEntity();

        personaUI.put("id", persona.getId());
        personaUI.put("nombre", persona.getNombre());
        personaUI.put("email", persona.getEmail());

        return personaUI;
    }

    @GET
    @Path("personas/{personaId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Información de una persona a partir de su ID", notes = "Obtiene una persona del directorio de personas externas a GOC, a partir de su ID", response = WrappedPersona.class)
    public UIEntity getPersonaById(@PathParam("personaId") Long personaId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Persona persona = externalService.getPersonaById(personaId, connectedUserId);

        if (persona != null)
            return personaToUI(persona);
        else
            return new UIEntity();
    }

    @PUT
    @Path("personas/{id}/deshabilita")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deshabilitaPersona(@PathParam("id") Long personaId) {
        externalService.deshabilitaPersona(personaId);
        return Response.ok().build();
    }

    @POST
    @Path("personas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertPersona(Persona persona) {
        externalService.insertPersona(persona);
        return Response.ok().build();
    }

    @PUT
    @Path("personas/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modificaPersona(
        @PathParam("id") Long personaId,
        Persona persona
    ) {
        externalService.modificaPersona(personaId, persona);
        return Response.ok().build();
    }

    @GET
    @Path("personas/{personaId}/roles")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Información de los roles de una persona a partir de su ID", notes = "Obtiene una persona del directorio de personas a partir de su ID")
    public List<String> getRolesByPersonaId(@PathParam("personaId") Long personaId)
    {
        List<Role> rolesByPersonaId = externalService.getRolesByPersonaId(personaId);

        if (rolesByPersonaId == null && rolesByPersonaId.isEmpty()) return Collections.EMPTY_LIST;

        return rolesByPersonaId.stream().map(role -> role.toString()).collect(toList());
    }

    @POST
    @Path("notificaciones")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Envío de notificaciones", notes = "Permite el envío de correos de aviso a los miembros involucrados en las reuniones", hidden = true)
    public Response enviaNotificacion(Mensaje mensaje) throws CanNotSendException
    {
        List<String> mailsConError = mailSender.send(mensaje);

        if (mailsConError.size() == 0)
            return Response.ok().build();
        else
            return Response.status(501).entity(mailsConError).build();
    }

    @POST
    @Path("firmas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Enviar al portafirmas el acta pendiente de firmar", notes = "Este servicio recibe todos los datos de la reunión, junto con el pdf sin firmar del acta. La entidada puede implementarlo para realizar la integración con su servicio de firma o portafirmas")
    public UIEntity firmaReunion(ReunionFirma reunionFirma)
    {
        return firmaReunionToUI(firmaService.firmaReunion(reunionFirma));
    }

    private UIEntity firmaReunionToUI(RespuestaFirma respuestaFirma)
    {
        UIEntity entity = UIEntity.toUI(respuestaFirma);
        List<UIEntity> acuerdosEntitites = new ArrayList<>();
        List<UIEntity> asistenciasEntitites = new ArrayList<>();

        for (RespuestaFirmaPuntoOrdenDiaAcuerdo acuerdo : respuestaFirma.getPuntoOrdenDiaAcuerdos())
        {
            UIEntity acuerdoEntity = UIEntity.toUI(acuerdo);

            acuerdosEntitites.add(acuerdoEntity);
        }

        entity.put("puntoOrdenDiaAcuerdos", acuerdosEntitites);

        for (RespuestaFirmaAsistencia asistencia : respuestaFirma.getAsistencias())
        {
            UIEntity asistenciaEntity = UIEntity.toUI(asistencia);

            asistenciasEntitites.add(asistenciaEntity);
        }

        entity.put("asistencias", asistenciasEntitites);

        return entity;
    }

    @GET
    @Path("config/menus")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Listado de menús de acceso a las distintas funcionalidades de GOC", notes = "Permite la carga del árbol lateral de navegación con las opciones principales de navegación", hidden = true)
    public Menu menus(@QueryParam("lang") String lang)
            throws RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Menu menu = new Menu();
        List<MenuItem> items = new ArrayList<>();

        List<String> roles = personaService.getRolesFromPersonaId(connectedUserId);

        if (personaService.isAdmin(roles))
        {

            items.add(menuEntry("goc.view.reunion.Main", "appI18N.menu.reuniones", MenuIconEnum.REUNIONES));
            items.add(menuEntry("goc.view.historicoReunion.Main", "appI18N.menu.historicoDeReuniones", MenuIconEnum.HISTORICO));
            items.add(menuEntry("goc.view.tipoOrgano.Main", "appI18N.menu.tiposDeOrganos", MenuIconEnum.TIPOSORGANOS));
            items.add(menuEntry("goc.view.organo.Main", "appI18N.menu.organos", MenuIconEnum.ORGANOS));

            if (!personalizationConfig.isCargosExternos())
                items.add(menuEntry("goc.view.cargo.Main", "appI18N.menu.cargos", MenuIconEnum.CARGOS));

            items.add(menuEntry("goc.view.miembro.Main", "appI18N.menu.miembros", MenuIconEnum.MIEMBROS));

            if(personalizationConfig.menuPersonasIsEnabled)
                items.add(menuEntry("goc.view.personas.Main", "appI18N.menu.personas", MenuIconEnum.PERSONAS));
            items.add(menuEntry("goc.view.descriptor.Main", "appI18N.menu.descriptores", MenuIconEnum.DESCRIPTORES));

            if(personalizationConfig.menuOficios)
                items.add(menuEntry("goc.view.oficio.Main", "appI18N.menu.oficios", MenuIconEnum.OFICIOS));
            menu.setMenuItems(items);
        }
        else {
            items.add(menuEntry("goc.view.reunion.Main", "appI18N.menu.reuniones", MenuIconEnum.REUNIONES));
            items.add(menuEntry("goc.view.historicoReunion.Main", "appI18N.menu.historicoDeReuniones", MenuIconEnum.HISTORICO));

            if (personaService.isGestor(roles)) {
                items.add(menuEntry("goc.view.organo.Main", "appI18N.menu.organos", MenuIconEnum.ORGANOS));
                items.add(menuEntry("goc.view.miembro.Main", "appI18N.menu.miembros", MenuIconEnum.MIEMBROS));
            }

            if (personalizationConfig.menuOficios)
                items.add(menuEntry("goc.view.oficio.Main", "appI18N.menu.oficios", MenuIconEnum.OFICIOS));
            menu.setMenuItems(items);
        }
        items.add(menuEntryUrl("","appI18N.menu.misReuniones", MenuIconEnum.MISREUNIONES, personalizationConfig.appContext + "/rest/publicacion/reuniones?lang="+lang));
        return menu;
    }

    @GET
    @Path("cuentas/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Recuperación de la persona asociada a una cuenta", notes = "A partir del username del usuario, permite extraer el valor del identificador de persona", hidden = true)
    public Persona personaAsociadaACuenta(@PathParam("username") String username)
            throws RolesPersonaExternaException
    {
        if (username == null)
        {
            throw new RuntimeException("Cuenta de usuario no definida");
        }

        Persona persona = new Persona();

        if (username.equals("nmanero")) persona.setId(88849L);

        if (username.equals("borillo")) persona.setId(9792L);

        return persona;
    }

    private MenuItem menuEntry(String className, String title, MenuIconEnum icon) {
        return menuEntryUrl(className, title, icon, null);
    }

    private MenuItem menuEntryUrl(String className, String title, MenuIconEnum icon, String url)
    {
        MenuItem menuItem = new MenuItem();

        menuItem.setId(className);
        menuItem.setTitle(title);
        menuItem.setText(title);
        menuItem.setLeaf("true");
        menuItem.setChecked(null);
        menuItem.setUrl(url);
        menuItem.setIconCls(icon.iconCls);

        return menuItem;
    }
}
