package es.uji.apps.goc.services;

import com.mysema.query.Tuple;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.dao.CargoDAO;
import es.uji.apps.goc.dao.MiembroDAO;
import es.uji.apps.goc.dao.MiembroGrupoDAO;
import es.uji.apps.goc.dao.OrganoDAO;
import es.uji.apps.goc.dao.OrganoReunionDAO;
import es.uji.apps.goc.dao.OrganoReunionMiembroDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.MiembroExterno;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.QOrganoReunionMiembro;
import es.uji.apps.goc.exceptions.MiembroExisteException;
import es.uji.apps.goc.exceptions.MiembroNoDisponibleException;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.Grupo;
import es.uji.apps.goc.model.JSONListaMiembrosDeserializer;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Votante;

@Service
@Component
public class MiembroService
{
    @Autowired
    private MiembroDAO miembroDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private CargoDAO cargoDAO;

    @Autowired
    private OrganoReunionDAO organoReunionDAO;

    @Autowired
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;

    @Autowired
    private MiembroGrupoDAO miembroGrupoDAO;

    @Autowired
    private OrganoDAO organoDAO;

    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.external.miembrosEndpoint}")
    private String miembrosExternosEndpoint;

    public List<Miembro> getMiembrosLocales(Long organoId, Long connectedUserId)
    {
        return miembroDAO.getMiembrosByOrganoId(organoId);
    }

    @Transactional
    public Miembro addMiembro(Miembro miembro, Long connectedUserId) throws MiembroExisteException
    {
        miembro = miembroDAO.insertMiembro(miembro, connectedUserId);
        if(miembro.hasCargo())
        {
            Optional<Cargo> cargo =
                cargoDAO.getCargoById(miembro.getCargo().getId());
            if (cargo.isPresent()) {
                miembro.setCargo(cargo.get());
            }
        }

        List<OrganoReunion> organoReuniones =
                organoReunionDAO.getOrganosReunionNoCompletadasAndFechaReunionAfterTodayByOrganoId(Long.parseLong(miembro.getOrgano().getId()));

        for (OrganoReunion organoReunion : organoReuniones)
        {
            OrganoReunionMiembro organoReunionMiembro = miembro.toOrganoReunionMiembro(organoReunion);
            organoReunionMiembroDAO.insert(organoReunionMiembro);
        }

        return miembro;
    }

    @Transactional
    public Miembro updateMiembro(Miembro miembro)
        throws MiembroNoDisponibleException
    {
        Miembro miembroOld = miembroDAO.getMiembroById(miembro.getId());
        if (miembroOld == null)
        {
            throw new MiembroNoDisponibleException();
        }

        String oldCargoId = miembro.getCargo().getId();
        if(miembro.hasCargo())
        {
            Optional<Cargo> cargo = cargoDAO.getCargoById(miembro.getCargo().getId());
            if (cargo.isPresent()) {
                miembro.setCargo(cargo.get());
            }
        }

        List<OrganoReunion> organoReunionesNoCompletadas =
            organoReunionDAO.getOrganosReunionNoCompletadasAndFechaReunionAfterTodayByOrganoId(Long.parseLong(miembro.getOrgano().getId()));

        miembro = miembroDAO.updateMiembro(miembroOld, miembro);
        for (OrganoReunion organoReunionNoCompletada : organoReunionesNoCompletadas)
        {
            organoReunionMiembroDAO.updateReunionMiembroWithMiembro(organoReunionNoCompletada.getId(), miembro.getPersonaId().toString(), oldCargoId, miembro);
        }
        return miembro;
    }

    @Transactional
    public void removeMiembroById(Long miembroId, Long connectedUserId)
            throws MiembroNoDisponibleException
    {
        Miembro miembro = miembroDAO.getMiembroById(miembroId);
        if (miembro == null)
        {
            throw new MiembroNoDisponibleException();
        }

        miembroDAO.delete(miembro);

        List<OrganoReunion> organoReuniones =
                organoReunionDAO.getOrganosReunionNoCompletadasAndFechaReunionAfterTodayByOrganoId(Long.parseLong(miembro.getOrgano().getId()));

        for (OrganoReunion organoReunion : organoReuniones)
        {
            reunionDAO.deleteResponsableActa(miembro.getPersonaId().toString());
            organoReunionMiembroDAO.deleteByOrganoReunionIdPersonaIdAndCargoId(organoReunion.getId(),
                    miembro.getPersonaId().toString(), miembro.getCargo().getId());
        }
    }

    public List<Miembro> getMiembrosExternos(String organoId, Long connectedUserId)
            throws MiembrosExternosException
    {
        WebResource getMiembrosResource =
                Client.create().resource(this.miembrosExternosEndpoint.replace("{organoId}", organoId));

        ClientResponse response = getMiembrosResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        if (response.getStatus() != 200)
        {
            throw new MiembrosExternosException();
        }

        JSONListaMiembrosDeserializer jsonDeserializer = response.getEntity(JSONListaMiembrosDeserializer.class);

        List<MiembroExterno> listaMiembrosExternos = jsonDeserializer.getMiembros();
        return creaListaMiembroDesdeListaMiembrosExternos(listaMiembrosExternos);
    }

    private List<Miembro> creaListaMiembroDesdeListaMiembrosExternos(List<MiembroExterno> listaMiembrosExternos)
    {
        List<Miembro> listaMiembros = new ArrayList<>();

        for (MiembroExterno miembroExterno : listaMiembrosExternos)
        {
            listaMiembros.add(creaMiembroDesdeMiembroExterno(miembroExterno));
        }

        return listaMiembros;
    }

    private Miembro creaMiembroDesdeMiembroExterno(MiembroExterno miembroExterno)
    {
        Long cargoId = miembroExterno.getCargo().getId();

        Cargo cargo = new Cargo(cargoId.toString());
        cargo.setCodigo(cargoDAO.getCargoCodigoById(cargoId.toString()));
        cargo.setNombre(miembroExterno.getCargo().getNombre());
        cargo.setNombreAlternativo(miembroExterno.getCargo().getNombreAlternativo());

        Miembro miembro = new Miembro();
        miembro.setId(miembroExterno.getId());
        miembro.setPersonaId(miembroExterno.getId());
        miembro.setNombre(miembroExterno.getNombre());
        miembro.setEmail(miembroExterno.getEmail());
        miembro.setCargo(cargo);
        miembro.setCondicion(miembroExterno.getCondicion());
        miembro.setCondicionAlternativa(miembroExterno.getCondicionAlternativa());

        miembro.setOrgano(new Organo(miembroExterno.getOrgano().getId()));

        return miembro;
    }

    @Transactional
    public void subeMiembro(Long miembroId)
    {
        if (miembroDAO.subeMiembro(miembroId) > -1)
            organoReunionDAO.updateOrdenEnReunionesNoCompletadas(miembroId);
    }

    @Transactional
    public void bajaMiembro(Long miembroId)
    {
        if (miembroDAO.bajaMiembro(miembroId) > -1)
            organoReunionDAO.updateOrdenEnReunionesNoCompletadas(miembroId);
    }

    @Transactional
    public void asignarGrupo(Grupo grupo)
    {
        Long organoId = miembroGrupoDAO.getOrganoIdFromMiembroId(grupo.getIds().get(0));
        Long ordenGrupo = miembroGrupoDAO.getOrdenGrupo(grupo.getNombre(), organoId);
        for(Long miembro : grupo.getIds())
        {
            miembroGrupoDAO.asignarGrupo(miembro, grupo.getNombre(), ordenGrupo);
        }
        List<Miembro> miembrosByOrganoId = miembroDAO.getMiembrosByOrganoId(organoId);
        organoReunionDAO.updateOrdenYGrupoMiembrosGrupos(organoId, miembrosByOrganoId);
    }

    @Transactional
    public void desagrupar(List<Long> ids)
    {
        miembroGrupoDAO.desagrupar(ids);
        List<Miembro> miembros = ids.stream().map(id -> miembroDAO.getMiembroById(id)).collect(Collectors.toList());
        organoReunionDAO.updateOrdenYGrupoMiembrosGrupos(Long.parseLong(miembros.get(0).getOrgano().getId()), miembros);
    }

    public void subirGrupo(
        String grupo,
        Long organoId
    )
    {
        miembroGrupoDAO.subirGrupo(grupo, organoId);
    }

    public void bajarGrupo(
        String grupo,
        Long organoId
    )
    {
        miembroGrupoDAO.bajarGrupo(grupo, organoId);
    }

    public List<Votante> getDelegandosYsuplenciasDeMiembro(Long miembroId, Long reunionId){
        List<Tuple> delegandosYSuplenciasDeMiembroId = miembroDAO.getDelegandosYSuplenciasDeMiembroId(miembroId, reunionId);
        if(delegandosYSuplenciasDeMiembroId.size() > 0) {
            return delegandosYSuplenciasDeMiembroIdToVotante(delegandosYSuplenciasDeMiembroId);
        }
        else
            return Collections.emptyList();
    }

    private List<Votante> delegandosYSuplenciasDeMiembroIdToVotante(List<Tuple> delegandosYSuplenciasDeMiembroId)
    {
        QOrganoReunionMiembro qOrganoReunionMiembro = QOrganoReunionMiembro.organoReunionMiembro;

        ArrayList<Votante> suplenteYDelegado = new ArrayList<>();

        for (Tuple miembroWhoUserIsSUplenteOrReceptoirVotoDelegado : delegandosYSuplenciasDeMiembroId) {
            String nombre = miembroWhoUserIsSUplenteOrReceptoirVotoDelegado.get(qOrganoReunionMiembro.nombre);
            Long miembroId = miembroWhoUserIsSUplenteOrReceptoirVotoDelegado.get(qOrganoReunionMiembro.miembroId);
            Votante personaQueEsSustituIda = new Votante(nombre,miembroId);
            suplenteYDelegado.add(personaQueEsSustituIda);
        }
        return suplenteYDelegado;
    }
}
