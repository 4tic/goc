package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.GrupoTemplate;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionHojaFirmas;
import es.uji.apps.goc.dto.ReunionInvitado;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.exceptions.AsistenteNoEncontradoException;
import es.uji.apps.goc.exceptions.ExistenVotosEnLaReunionException;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.exceptions.NotificacionesException;
import es.uji.apps.goc.exceptions.OrganoConvocadoNoPermitidoException;
import es.uji.apps.goc.exceptions.OrganosExternosException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.PuntoDelDiaConAcuerdosException;
import es.uji.apps.goc.exceptions.ReunionNoCompletadaException;
import es.uji.apps.goc.exceptions.ReunionNoDisponibleException;
import es.uji.apps.goc.exceptions.ReunionYaCompletadaException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.exceptions.SinPermisoException;
import es.uji.apps.goc.exceptions.UrlGrabacionException;
import es.uji.apps.goc.exceptions.UsuarioYaHaVotadoException;
import es.uji.apps.goc.model.Convocatoria;
import es.uji.apps.goc.model.ReunionEstado;
import es.uji.apps.goc.model.Externo;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ReunionAcuerdos;
import es.uji.apps.goc.services.DescriptorService;
import es.uji.apps.goc.services.ErrorService;
import es.uji.apps.goc.services.OrganoReunionMiembroService;
import es.uji.apps.goc.services.OrganoService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionDocumentoService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.apps.goc.services.VotosService;
import es.uji.apps.goc.templates.PDFTemplate;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;


@Path("reuniones")
public class ReunionResource extends CoreBaseService {
    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private DescriptorService descriptorService;

    @InjectParam
    private ReunionDocumentoService reunionDocumentoService;

    @InjectParam
    private OrganoService organoService;

    @InjectParam
    private OrganoReunionMiembroService organoReunionMiembroService;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private VotosService votosService;

    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @InjectParam
    private ErrorService errorService;

    @Value("${goc.templates.path:classpath:templates/}")
    private String templatesPath;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getReuniones(
        @QueryParam("organoId") String organoId,
        @QueryParam("tipoOrganoId") Long tipoOrganoId,
        @QueryParam("externo") Boolean externo,
        @QueryParam("pasadas") @DefaultValue("true") Boolean pasadas
    ) throws RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        if (personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador)) {
            return UIEntity.toUI(reunionService.getReunionesByAdmin(false, pasadas, tipoOrganoId, organoId, externo));
        } else {
            return UIEntity
                .toUI(reunionService.getReunionesByEditorId(false, pasadas, organoId, tipoOrganoId, externo, connectedUserId));
        }
    }

    @GET
    @Path("{reunionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getReunionById(@PathParam("reunionId") Long reunionId)
        throws ReunionNoDisponibleException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return UIEntity.toUI(reunionService.getReunionByIdAndEditorId(reunionId, connectedUserId));
    }

    @GET
    @Path("{reunionId}/externos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getReunionExternos(@PathParam("reunionId") Long reunionId) throws PersonasExternasException {
        return UIEntity.toUI(reunionService.getExternosFromReunionId(reunionId));
    }

    @POST
    @Path("externos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addReunionExterno(Externo externo) throws PersonasExternasException {
        User connectedUser = AccessManager.getConnectedUser(request);
        reunionService.addExternoToReunion(externo, connectedUser);
        return Response.ok().build();
    }

    @DELETE
    @Path("externos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteReunionExterno(Externo externo) {
        User connectedUser = AccessManager.getConnectedUser(request);
        reunionService.borrarExterno(externo, connectedUser);
        return Response.ok().build();
    }

    @GET
    @Path("completadas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getReunionesCompletadas(
        @QueryParam("organoId") String organoId,
        @QueryParam("tipoOrganoId") Long tipoOrganoId,
        @QueryParam("externo") Boolean externo
    ) throws OrganosExternosException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador)) {
            return UIEntity.toUI(reunionService.getReunionesByAdmin(true, true, tipoOrganoId, organoId, externo));
        } else {
            return UIEntity
                .toUI(reunionService.getReunionesByEditorId(true, true, organoId, tipoOrganoId, externo, connectedUserId));
        }
    }

    @Path("{reunionId}/puntosOrdenDia")
    public ReunionPuntosOrdenDiaResource getReunionPuntosOrdenDiaResource(
        @InjectParam ReunionPuntosOrdenDiaResource reunionPuntosOrdenDiaResource
    ) {
        return reunionPuntosOrdenDiaResource;
    }

    @Path("{reunionId}/documentos")
    public ReunionDocumentosResource getReunionDocumentosResource(
        @InjectParam ReunionDocumentosResource reunionDocumentosResource
    ) {
        return reunionDocumentosResource;
    }

    @Path("{reunionId}/miembros")
    public ReunionMiembroResource getReunionMiembroResource(
        @InjectParam ReunionMiembroResource reunionMiembroResource
    ) {
        return reunionMiembroResource;
    }

    @GET
    @Path("{reunionId}/tienesuplente/{reunionMiembroId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage tieneSuplente(
        @PathParam("reunionId") Long reunionId,
        @PathParam("reunionMiembroId") Long reunionMiembroId
    ) throws AsistenteNoEncontradoException, ReunionYaCompletadaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Boolean tieneSuplente = organoReunionMiembroService.tieneSuplente(reunionMiembroId, connectedUserId);

        if (tieneSuplente) {
            return new ResponseMessage(true, "true");
        }

        return new ResponseMessage(true, "false");
    }

    @POST
    @Path("{reunionId}/confirmar/{reunionMiembroId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void confirmarAsistencia(
        @PathParam("reunionId") Long reunionId,
        @PathParam("reunionMiembroId") Long reunionMiembroId,
        UIEntity ui
    ) throws AsistenteNoEncontradoException, ReunionYaCompletadaException, UsuarioYaHaVotadoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Boolean asistencia = new Boolean(ui.get("confirmacion"));
        String justificaion = ui.get("justificacion");

        reunionService.compruebaReunionNoCompletada(reunionId);
        votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionId, connectedUserId);
        organoReunionMiembroService.estableceAsistencia(reunionMiembroId, connectedUserId, asistencia, justificaion);
    }

    @POST
    @Path("{reunionId}/suplente")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void establecerSuplente(
        @PathParam("reunionId") Long reunionId,
        UIEntity suplente
    ) throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long suplenteId = Long.parseLong(suplente.get("suplenteId"));
        String suplenteNombre = suplente.get("suplenteNombre");
        String suplenteEmail = suplente.get("suplenteEmail");
        Long organoMiembroId = Long.parseLong(suplente.get("organoMiembroId"));

        votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionId, connectedUserId);
        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.compruebaReunionAdmiteSuplencia(reunionId);
        organoReunionMiembroService
            .estableceSuplente(reunionId, connectedUserId, suplenteId, suplenteNombre, suplenteEmail, organoMiembroId);
    }

    @DELETE
    @Path("{reunionId}/suplente")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void borraSuplente(
        @PathParam("reunionId") Long reunionId,
        UIEntity miembro
    ) throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long miembroId = Long.parseLong(miembro.get("organoMiembroId"));

        reunionService.compruebaSuplenteNoHaVotadoEnLaReunion(reunionId, miembroId);
        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.compruebaReunionAdmiteSuplencia(reunionId);
        organoReunionMiembroService.borraSuplente(reunionId, miembroId, connectedUserId);
    }

    @POST
    @Path("{reunionId}/delegadovoto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void establecerDelegacionVoto(
        @PathParam("reunionId") Long reunionId,
        UIEntity delegadoVoto
    ) throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long delegadoVotoId = Long.parseLong(delegadoVoto.get("delegadoVotoId"));
        String delegadoVotoNombre = delegadoVoto.get("delegadoVotoNombre");
        String delegadoVotoEmail = delegadoVoto.get("delegadoVotoEmail");
        Long organoMiembroId = Long.parseLong(delegadoVoto.get("organoMiembroId"));

        votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionId, connectedUserId);
        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.compruebaReunionAdmiteDelegacionVoto(reunionId);
        organoReunionMiembroService
            .estableceDelegadoVoto(reunionId, connectedUserId, delegadoVotoId, delegadoVotoNombre, delegadoVotoEmail,
                organoMiembroId);
    }

    @DELETE
    @Path("{reunionId}/delegadovoto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void borraDelegacionVoto(
        @PathParam("reunionId") Long reunionId,
        UIEntity miembro
    ) throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Long miembroId = Long.parseLong(miembro.get("organoMiembroId"));

        reunionService.compruebaDelegadoNoHaVotadoEnLaReunion(reunionId, miembroId);
        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.compruebaReunionAdmiteDelegacionVoto(reunionId);
        organoReunionMiembroService.borraDelegadoVoto(reunionId, miembroId, connectedUserId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addReunion(UIEntity reunionUI)
        throws NotificacionesException, MiembrosExternosException, PersonasExternasException, UrlGrabacionException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Reunion reunion = reunionUIToModel(reunionUI);
        reunion = reunionService.addReunion(reunion, connectedUserId);

        return UIEntity.toUI(reunion);
    }

    @POST
    @Path("{reunionId}/duplica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity duplicaReunion(
        @PathParam("reunionId") Long reunionId,
        UIEntity reunionUI
    ) throws NotificacionesException, MiembrosExternosException, PersonasExternasException, UrlGrabacionException,
        RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Reunion reunion = reunionUIToModel(reunionUI);
        reunion.setId(null);
        reunion = reunionService.duplicaReunion(reunion, reunionId, connectedUserId);
        return UIEntity.toUI(reunion);
    }

    @PUT
    @Path("{reunionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaReunion(
        @PathParam("reunionId") Long reunionId,
        UIEntity reunionUI
    ) throws ReunionNoDisponibleException, UrlGrabacionException, ReunionYaCompletadaException,
        ExistenVotosEnLaReunionException {
        votosService.compruebaReunionNoAdmiteONoTieneVotos(reunionId);

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String asunto = reunionUI.get("asunto");
        String asuntoAlternativo = reunionUI.get("asuntoAlternativo");
        String descripcion = reunionUI.get("descripcion");
        String descripcionAlternativa = reunionUI.get("descripcionAlternativa");
        String ubicacion = reunionUI.get("ubicacion");
        String ubicacionAlternativa = reunionUI.get("ubicacionAlternativa");
        String urlGrabacion = reunionUI.get("urlGrabacion");
        Boolean publica = new Boolean(reunionUI.get("publica"));
        Boolean telematica = new Boolean(reunionUI.get("telematica"));
        Boolean hasVotacion = new Boolean(reunionUI.get("hasVotacion"));
        String telematicaDescripcion = reunionUI.get("telematicaDescripcion");
        String telematicaDescripcionAlternativa = reunionUI.get("telematicaDescripcionAlternativa");
        Boolean admiteSuplencia = new Boolean(reunionUI.get("admiteSuplencia"));
        Boolean admiteDelegacionVoto = new Boolean(reunionUI.get("admiteDelegacionVoto"));
        Boolean admiteComentarios = new Boolean(reunionUI.get("admiteComentarios"));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        LocalDateTime dateTime = LocalDateTime.parse(reunionUI.get("fecha"), formatter);
        Date fecha = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());

        Date fechaSegundaConvocatoria = null;

        if (reunionUI.get("fechaSegundaConvocatoria") != null && !reunionUI.get("fechaSegundaConvocatoria").isEmpty()) {
            LocalDateTime dateTimeSegundaConvocatoria =
                LocalDateTime.parse(reunionUI.get("fechaSegundaConvocatoria"), formatter);
            fechaSegundaConvocatoria =
                Date.from(dateTimeSegundaConvocatoria.atZone(ZoneId.systemDefault()).toInstant());
        }

        Long numeroSesion = null;

        if (!reunionUI.get("numeroSesion").isEmpty()) {
            numeroSesion = Long.parseLong(reunionUI.get("numeroSesion"));
        }

        Long duracion = Long.parseLong(reunionUI.get("duracion"));

        reunionService.compruebaReunionNoCompletada(reunionId);

        Reunion reunion = reunionUIToModel(reunionUI);
        reunion.setId(reunionId);

        reunion = reunionService
            .updateReunion(reunionId, asunto, asuntoAlternativo, descripcion, descripcionAlternativa, duracion, fecha,
                fechaSegundaConvocatoria, ubicacion, ubicacionAlternativa, urlGrabacion, numeroSesion, publica,
                telematica, hasVotacion, telematicaDescripcion, telematicaDescripcionAlternativa, admiteSuplencia,
                admiteDelegacionVoto, admiteComentarios, connectedUserId);

        return UIEntity.toUI(reunion);
    }

    private Reunion reunionUIToModel(UIEntity reunionUI) throws UrlGrabacionException {
        Reunion reunion = new Reunion();
        String urlGrabacion = reunionUI.get("urlGrabacion");

        if (!urlGrabacion.isEmpty()) {
            try {
                new URL(urlGrabacion);
            } catch (MalformedURLException e) {
                throw new UrlGrabacionException();
            }
        }

        if (ParamUtils.parseLong(reunionUI.get("id")) != null) {
            reunion.setId(new Long(reunionUI.get("id")));
        }

        reunion.setAsunto((reunionUI.get("asunto")));
        reunion.setAsuntoAlternativo((reunionUI.get("asuntoAlternativo")));
        reunion.setDescripcion(reunionUI.get("descripcion"));
        reunion.setDescripcionAlternativa(reunionUI.get("descripcionAlternativa"));
        reunion.setUbicacion(reunionUI.get("ubicacion"));
        reunion.setUbicacionAlternativa(reunionUI.get("ubicacionAlternativa"));
        reunion.setUrlGrabacion(urlGrabacion);

        Boolean telematica = new Boolean(reunionUI.get("telematica"));
        reunion.setTelematica(telematica);

        Boolean hasVotacion = new Boolean(reunionUI.get("hasVotacion"));
        reunion.setHasVotacion(hasVotacion);
        reunion.setTelematicaDescripcion(reunionUI.get("telematicaDescripcion"));
        reunion.setTelematicaDescripcionAlternativa(reunionUI.get("telematicaDescripcionAlternativa"));
        reunion.setExtraordinaria(reunionUI.getBoolean("extraordinaria"));

        if (!reunionUI.get("numeroSesion").isEmpty()) {
            reunion.setNumeroSesion(Long.parseLong(reunionUI.get("numeroSesion")));
        }

        reunion.setPublica(new Boolean(reunionUI.get("publica")));
        reunion.setAdmiteSuplencia(new Boolean(reunionUI.get("admiteSuplencia")));
        reunion.setAdmiteDelegacionVoto(new Boolean(reunionUI.get("admiteDelegacionVoto")));
        reunion.setAdmiteComentarios(new Boolean(reunionUI.get("admiteComentarios")));

        if (reunionUI.get("fecha") != null && !reunionUI.get("fecha").isEmpty()) {
            LocalDateTime fechaAndTimeReunionFormated = reunionService.getFechaAndTimeFormated(reunionUI, "fecha");
            reunion.setFecha(Date.from(fechaAndTimeReunionFormated.atZone(ZoneId.systemDefault()).toInstant()));
        }

        if (reunionUI.get("fechaSegundaConvocatoria") != null && !reunionUI.get("fechaSegundaConvocatoria").isEmpty()) {
            LocalDateTime dateTimeSegundaConvocatoria =
                reunionService.getFechaAndTimeFormated(reunionUI, "fechaSegundaConvocatoria");
            reunion.setFechaSegundaConvocatoria(
                Date.from(dateTimeSegundaConvocatoria.atZone(ZoneId.systemDefault()).toInstant()));
        }

        reunion.setDuracion(Long.parseLong(reunionUI.get("duracion")));

        return reunion;
    }

    @PATCH
    @Path("{reunionId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void modificaEstadoReunion(
        @PathParam("reunionId") Long reunionId,
        ReunionEstado reunionEstado
    ) throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        reunionService.updateEstadoReunion(reunionId, reunionEstado, connectedUserId);
    }

    @PUT
    @Path("{reunionId}/enviarconvocatoria")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage enviarConvocatoria(
        @PathParam("reunionId") Long reunionId,
        Convocatoria convocatoria
    ) throws Exception {
        User connectedUser = AccessManager.getConnectedUser(request);

        reunionService.guardarConvocanteBasedOnBloquearRemitenteConvocatoria(reunionId, convocatoria, connectedUser);
        String messageError = reunionService.enviarConvocatoria(reunionId, connectedUser, convocatoria.getTexto());

        if (messageError == null) {
            return new ResponseMessage(true, "appI18N.reuniones.convocatoriaEnviada");
        }

        return new ResponseMessage(true, messageError);
    }

    @GET
    @Path("{reunionId}/checktoclose")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage checkReunionToClose(@PathParam("reunionId") Long reunionId)
        throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String messageError = reunionService.checkReunionToClose(reunionId, connectedUserId);

        if (messageError == null) {
            return new ResponseMessage(true, "");
        }

        return new ResponseMessage(true, messageError);
    }

    @GET
    @Path("{reunionId}/acuerdos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAcuerdos(@PathParam("reunionId") Long reunionId) {
        ReunionAcuerdos reunionAcuerdos = reunionService.getAcuerdosByReunionId(reunionId);
        return UIEntity.toUI(reunionAcuerdos);
    }


    @PUT
    @Path("{reunionId}/organos")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modificaReunionesOrgano(
        @PathParam("reunionId") Long reunionId,
        UIEntity reunionOrganosUI
    ) throws Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<UIEntity> organosUI = reunionOrganosUI.getRelations().get("organos");
        List<Organo> organos = creaOrganosDesdeOrganosUI(organosUI);

        reunionService.compruebaReunionNoCompletada(reunionId);
        compruebaPermisosAutorizadoOrganos(organos, connectedUserId);
        reunionService.updateOrganosReunionByReunionId(reunionId, organos, connectedUserId);

        organoReunionMiembroService.updateOrganoReunionMiembrosDesdeOrganosUI(organosUI, reunionId, connectedUserId);
        organoReunionMiembroService.updateOrganoReunionInvitadosDesdeOrganosUI(organosUI, reunionId, connectedUserId);

        return Response.ok().build();
    }

    private void compruebaPermisosAutorizadoOrganos(
        List<Organo> organos,
        Long connectedUserId
    ) throws OrganoConvocadoNoPermitidoException, RolesPersonaExternaException {
        if (!organoService.usuarioConPermisosParaConvocarOrganos(organos, connectedUserId)) {
            throw new OrganoConvocadoNoPermitidoException();
        }
    }

    private List<Organo> creaOrganosDesdeOrganosUI(List<UIEntity> organosUI) {
        List<Organo> organos = new ArrayList<>();
        if (organosUI != null) {
            for (UIEntity organoUI : organosUI) {
                Organo organo = new Organo(organoUI.get("id"));
                organo.setExterno(new Boolean(organoUI.get("externo")));
                organos.add(organo);
            }
        }
        return organos;
    }

    @DELETE
    @Path("{reunionId}")
    public Response borraReunion(
        @PathParam("reunionId") Long reunionId,
        UIEntity entity
    ) throws ReunionNoDisponibleException, ReunionYaCompletadaException, PuntoDelDiaConAcuerdosException, Exception {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        reunionService.compruebaReunionNoCompletada(reunionId);
        votosService.compruebaReunionNoAdmiteONoTieneVotos(reunionId);
        reunionService.removeReunionById(reunionId, connectedUserId);

        return Response.ok().build();
    }

    @GET
    @Path("{reunionId}/asistencia")
    public Response reunion(
        @PathParam("reunionId") Long reunionId,
        @QueryParam("lang") String lang,
        @Context HttpServletRequest request
    ) throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
        ReunionNoCompletadaException, AsistenteNoEncontradoException, PersonasExternasException,
        FirmanteSinSuplenteNoAsiste, FirmantesNecesariosException {
        String applang = languageConfig.getLangCode(lang);
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Reunion reunion = reunionService.getReunionConOrganosById(reunionId, connectedUserId);
        String nombreAsistente;
        try {
            if (reunion == null) {
                throw new ReunionNoDisponibleException();
            }

            if (!reunion.getCompletada()) {
                throw new ReunionNoCompletadaException();
            }

            nombreAsistente = reunionService.getNombreAsistente(reunionId, connectedUserId);
            if (nombreAsistente == null) {
                throw new SinPermisoException();
            }
        }
        catch (Exception e) {
            return Response.ok(errorService.getTemplateConError(applang, e)).build();
        }

        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, false, languageConfig.isMainLangauge(lang));

        Template template = new PDFTemplate("asistencia-" + applang, personalizationConfig);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("nombreAsistente", nombreAsistente);
        template.put("tituloReunion", reunionTemplate.getAsunto());
        template.put("fechaReunion", getFechaReunion(reunionTemplate.getFecha()));
        template.put("horaReunion", getHoraReunion(reunionTemplate.getFecha()));
        template.put("nombreConvocante", reunionTemplate.getCreadorNombre());

        try {
            StreamingOutput stream = output -> {
                BufferedOutputStream bos = new BufferedOutputStream(output);
                try {
                    bos.write(template.process());
                } catch (Exception e) {
                }
                bos.flush();
            };

            return Response.ok(stream).header("Content-Disposition",
                "attachment;filename=asistencia.pdf").build();
        } catch (Exception e) {
            return Response.ok(errorService.getTemplateConError(applang, e)).build();
        }
    }

    @GET
    @Path("{reunionId}/asistentes")
    @Produces("application/pdf")
    public Template listaAsistentes(
        @PathParam("reunionId") Long reunionId,
        @QueryParam("lang") String lang,
        @Context HttpServletRequest request
    ) throws ReunionNoDisponibleException, FirmanteSinSuplenteNoAsiste, FirmantesNecesariosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String applang = languageConfig.getLangCode(lang);

        Reunion reunion = reunionService.getReunionConOrganosById(reunionId, connectedUserId);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        ReunionTemplate reunionTemplate = reunionService
            .getReunionTemplateDesdeReunion(reunion, connectedUserId, false, languageConfig.isMainLangauge(lang));

        List<GrupoTemplate> grupos = reunionService.getGruposByReunionTemplate(reunionTemplate, applang);

        Template template = new PDFTemplate("asistentes-" + applang, personalizationConfig);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("reunion", reunionTemplate);
        template.put("grupos", grupos);

        return template;
    }

    private String getFechaReunion(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        return sdf.format(fecha);
    }

    private String getHoraReunion(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        return sdf.format(fecha);
    }

    @GET
    @Path("{reunionId}/descriptores")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> listaDescriptores(@PathParam("reunionId") Long reunionId)
        throws ReunionNoDisponibleException, OrganosExternosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return UIEntity.toUI(descriptorService.getDescriptoresByReunionId(reunionId, connectedUserId));
    }

    @GET
    @Path("{reunionId}/invitados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> listaInvitados(@PathParam("reunionId") Long reunionId)
        throws ReunionNoDisponibleException, OrganosExternosException {
        return UIEntity.toUI(reunionService.getInvitadosReunionByReunionId(reunionId));
    }

    @GET
    @Path("/convocantes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> listaConvocantes(@QueryParam("reunionId") Long reunionId) throws PersonasExternasException {
        if (!personalizationConfig.bloquearRemitenteConvocatoria) {
            User connectedUser = AccessManager.getConnectedUser(request);
            Persona connectedPersona = personaService.getPersonaFromDirectoryByPersonaId(connectedUser.getId());
            return UIEntity.toUI(reunionService.getConvocantesReunionByReunionId(reunionId, connectedPersona));
        } else {
            return Collections.emptyList();
        }
    }

    @PUT
    @Path("{reunionId}/invitados")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modificaInvitados(
        @PathParam("reunionId") Long reunionId,
        UIEntity reunionInvitadosUI
    ) throws ReunionYaCompletadaException, ReunionNoDisponibleException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ReunionInvitado> reunionInvitados = creaInvitadosDesdeInvitadosUI(reunionId, reunionInvitadosUI);

        reunionService.compruebaReunionNoCompletada(reunionId);
        reunionService.updateInvitadosByReunionId(reunionId, reunionInvitados, connectedUserId);

        return Response.ok().build();
    }

    @GET
    @Path("{reunionId}/hojafirmas")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getHojaFirmas(@PathParam("reunionId") Long reunionId) {
        ReunionHojaFirmas hojaFirmasByReunionId = reunionService.getHojaFirmasByReunionId(reunionId);
        if (hojaFirmasByReunionId != null) {
            return hojaFirmasToUI(hojaFirmasByReunionId);

        }
        return UIEntity.toUI(new Object());
    }

    @GET
    @Path("{reunionId}/hojafirmas/descargar")
    public Response getHojaFirmasDocumento(@PathParam("reunionId") Long reunionId) {
        ReunionHojaFirmas hojaFirmasByReunionId = reunionService.getHojaFirmasByReunionId(reunionId);
        return Response.ok(hojaFirmasByReunionId.getDatos()).header("Content-Disposition",
            "attachment; filename = \"" + hojaFirmasByReunionId.getNombreFichero() + "\"")
            .header("Content-Length", hojaFirmasByReunionId.getDatos().length)
            .header("Content-Type", hojaFirmasByReunionId.getMimeType()).build();
    }

    @POST
    @Path("{reunionId}/hojafirmas")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public UIEntity subirHojaFirmas(
        @PathParam("reunionId") Long reunionId,
        FormDataMultiPart multiPart
    ) throws IOException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String fileName = "";
        String mimeType = "";
        InputStream data = null;

        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            String mime = bodyPart.getHeaders().getFirst("Content-Type");
            if (mime != null && !mime.isEmpty()) {
                mimeType = mime;
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches()) {
                    fileName = m.group(1);
                }
                BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                data = bpe.getInputStream();
            }
        }
        ReunionHojaFirmas hojaFirmas = new ReunionHojaFirmas();
        hojaFirmas.setMimeType(mimeType);
        hojaFirmas.setNombreFichero(fileName);
        hojaFirmas.setDatos(StreamUtils.inputStreamToByteArray(data));
        hojaFirmas.setCreadorId(connectedUserId);
        hojaFirmas.setFechaAdicion(new Date());
        return UIEntity.toUI(reunionService.subirHojaFirmasByReunionId(reunionId, hojaFirmas));
    }

    private UIEntity hojaFirmasToUI(ReunionHojaFirmas hojaFirmasByReunionId) {
        UIEntity ui = new UIEntity();
        ui.put("id", hojaFirmasByReunionId.getId());
        ui.put("fechaAdicion", hojaFirmasByReunionId.getFechaAdicion());
        ui.put("mimeType", hojaFirmasByReunionId.getMimeType());
        ui.put("nombreFichero", hojaFirmasByReunionId.getNombreFichero());

        return ui;
    }

    private List<ReunionInvitado> creaInvitadosDesdeInvitadosUI(
        Long reunionId,
        UIEntity reunionInvitadosUI
    ) {
        List<ReunionInvitado> invitados = new ArrayList<>();
        Reunion reunion = new Reunion(reunionId);

        List<UIEntity> invitadosUI = reunionInvitadosUI.getRelations().get("invitados");

        if (invitadosUI == null)
            return new ArrayList<>();

        for (UIEntity invitadoUI : invitadosUI) {
            ReunionInvitado reunionInvitado = new ReunionInvitado();

            reunionInvitado.setReunion(reunion);
            reunionInvitado.setPersonaId(invitadoUI.getLong("personaId"));
            reunionInvitado.setPersonaNombre(invitadoUI.get("personaNombre"));
            reunionInvitado.setPersonaEmail(invitadoUI.get("personaEmail"));
            reunionInvitado.setMotivoInvitacion(invitadoUI.get("motivoInvitacion"));

            invitados.add(reunionInvitado);
        }

        return invitados;
    }

    @PUT
    @Path("{reunionId}/reabrir")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response reabrirReunion(
        @PathParam("reunionId") Long reunionId,
        UIEntity uiEntity
    ) throws Exception {
        User connectedUser = AccessManager.getConnectedUser(request);

        String motivoReapertura = uiEntity.get("motivoReapertura");

        reunionService.reabrirReunion(reunionId, motivoReapertura, connectedUser);

        return Response.ok().build();
    }

}
