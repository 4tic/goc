package es.uji.apps.goc.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.integrations.CuentasClient;
import es.uji.apps.goc.services.AuditoriaService;
import es.uji.apps.goc.services.OrganoService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.commons.sso.User;

public abstract class PersonaAuth
{
    private static Logger log = LoggerFactory.getLogger(PersonaAuth.class);

    @Autowired
    OrganoService organoService;

    @Autowired
    PersonaService personaService;

    @Autowired
    CuentasClient cuentasClient;

    @Autowired
    AuditoriaService auditoriaService;

    protected User createUserFromPersonaId(String personaId) throws IOException
    {
        try
        {
            User user = new User();
            user.setId(cuentasClient.obtainPersonaIdFrom(personaId));
            user.setName(personaId);
            user.setActiveSession(UUID.randomUUID().toString());

            auditoriaService.auditaLogin(personaId);
            return user;
        } catch (Exception e)
        {
            auditoriaService.auditaLoginFallido(personaId);
            throw new IOException("No se ha podido recuperar el id: " + personaId + " del usuario conectado");
        }
    }

    public boolean isValidUser(User user) throws IOException
    {
        try
        {
            List<String> rolesFromPersonaId = personaService.getRolesFromPersonaId(user.getId());

            return personaService.isAdmin(rolesFromPersonaId) || personaService.isGestor(rolesFromPersonaId)
                || organoService.existAutorizadosByUserId(user.getId()) || personaService.isDeveloper(user.getId());
        } catch (RolesPersonaExternaException e)
        {
            throw new IOException("No se ha podido validar el usuario conectado");
        }
    }

    public boolean isDeveloperUser(User user) throws RolesPersonaExternaException
    {
        return personaService.isDeveloper(user.getId());
    }

    public boolean isAdminUser(User user) throws RolesPersonaExternaException
    {
        return personaService.isAdmin(user.getId());
    }
}
