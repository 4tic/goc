package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import net.fortuna.ical4j.model.Calendar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.services.CalendarService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Path("calendario")
public class CalendarioResource extends CoreBaseService
{

    @InjectParam
    PersonalizationConfig personalizationConfig;

    @InjectParam
    CalendarService calendarService;

    @InjectParam
    LanguageConfig languageConfig;

    @GET
    @Path("{token}")
    public Response getCalendario(@PathParam("token") String token, @QueryParam("lang") String lang) throws IOException
    {
        String applang = languageConfig.getLangCode(lang);
        Calendar calendar = calendarService.obtenerCalendarioUsuario(token, applang);
        String calendarString = calendar.toString();
        byte[] bytes = calendarString.getBytes("UTF-8");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(calendarString.length());
        byteArrayOutputStream.write(bytes);


        return Response.ok(bytes)
            .header("Content-Disposition", "attachment; filename = calendario_goc.ics")
            .header("Content-Length", bytes.length).header("Content-Type", "text/calendar").build();
    }

    @PUT
    public Response createFeed()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        String tokenGenerado = calendarService.crearFeed(connectedUserId);
        String urlFeed = personalizationConfig.publicUrl + personalizationConfig.appContext + "/rest/calendario/" + tokenGenerado;
        return Response.ok(urlFeed).build();
    }
}
