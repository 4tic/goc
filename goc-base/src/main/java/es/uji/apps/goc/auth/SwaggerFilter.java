package es.uji.apps.goc.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.commons.sso.User;

public class SwaggerFilter implements Filter
{
    @Autowired
    SecurityAuth securityAuth;

    @Value("${goc.context}")
    public String appContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest clientRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse clientResponse = (HttpServletResponse) servletResponse;

        User user = (User) clientRequest.getSession().getAttribute(User.SESSION_USER);
        try {
            if(securityAuth.isDeveloperUser(user) || securityAuth.isAdminUser(user)) {
                filterChain.doFilter(clientRequest, clientResponse);
            }
            else{
                clientResponse.sendRedirect(appContext + "/forbidden.jsp");
            }
        } catch (RolesPersonaExternaException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy()
    {

    }
}
