package es.uji.apps.goc.model;

public class Convocante {

    private String nombre;

    private String email;

    public Convocante(){
    }
    public Convocante(String nombre){
        this.nombre = nombre;
    }
    public Convocante(String nombre, String email){
        this.nombre = nombre;
        this.email = (email == null) ? "": email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
