package es.uji.apps.goc.model;

import java.io.IOException;
import java.util.List;

import javax.xml.transform.TransformerConfigurationException;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.GrupoTemplate;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.templates.PDFTemplate;
import es.uji.apps.goc.templates.Template;

public class ActaTemplate
{
    private String logo;
    private ReunionTemplate reunionTemplate;
    private Persona convocante;
    private String lang;
    private List<GrupoTemplate> grupos;
    private boolean isForBuscador;

    public ReunionTemplate getReunionTemplate() {
        return reunionTemplate;
    }

    public ActaTemplate(
        String logo,
        ReunionTemplate reunionTemplate,
        Persona convocante,
        String lang,
        List<GrupoTemplate> grupos,
        boolean isForBuscador
    )
    {
        this.logo = logo;
        this.reunionTemplate = reunionTemplate;
        this.convocante = convocante;
        this.lang = lang;
        this.grupos = grupos;
        this.isForBuscador = isForBuscador;
    }

    public Template toTemplate(PersonalizationConfig personalizationConfig, boolean showAusentes) throws TransformerConfigurationException, IOException {
        Template template = new PDFTemplate("acta-" + lang, personalizationConfig, this.logo);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("reunion", this.reunionTemplate);
        template.put("convocante", this.convocante);
        template.put("applang", this.lang);
        template.put("grupos", this.grupos);
        template.put("showAusentes", showAusentes);
        template.put("isForBuscador", this.isForBuscador);
        return template;
    }

    public Template toTemplate(PersonalizationConfig personalizationConfig) throws TransformerConfigurationException, IOException {
        return toTemplate(personalizationConfig, false);
    }
}
