package es.uji.apps.goc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.goc.dao.OrganoReunionMiembroDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaComentarioDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaComentario;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.model.ComentarioPuntoOrdenDia;
import es.uji.apps.goc.notifications.AvisosReunion;

@Service
public class PuntoOrdenDiaComentarioService
{

    @Autowired
    PuntoOrdenDiaComentarioDAO puntoOrdenDiaComentarioDAO;

    @Autowired
    PersonaService personaService;

    @Autowired
    AvisosReunion avisosReunion;

    @Autowired
    PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    OrganoReunionMiembroDAO organoReunionMiembroDAO;

    public ComentarioPuntoOrdenDia addComentario(Reunion reunion, ComentarioPuntoOrdenDia comentarioPuntoOrdenDia) throws Exception
    {
        PuntoOrdenDiaComentario puntoOrdenDiaComentario = comentarioPuntoOrdenDiaToDTO(comentarioPuntoOrdenDia);
        PuntoOrdenDia puntoOrdenDia =
            puntoOrdenDiaDAO.getPuntoOrdenDiaById(comentarioPuntoOrdenDia.getPuntoOrdenDiaId());
        puntoOrdenDiaComentario.setPuntoOrdenDia(puntoOrdenDia);

        puntoOrdenDiaComentarioDAO.insert(puntoOrdenDiaComentario);

        avisosReunion.enviarAvisoNuevoComentarioPuntoOrdenDiaPersonasAsistentesReunion(reunion, puntoOrdenDiaComentario);

        return ComentarioPuntoOrdenDia.dtoToModel(puntoOrdenDiaComentario);
    }


    private PuntoOrdenDiaComentario comentarioPuntoOrdenDiaToDTO(ComentarioPuntoOrdenDia comentarioPuntoOrdenDia)
        throws PersonasExternasException
    {
        PuntoOrdenDiaComentario puntoOrdenDiaComentario = new PuntoOrdenDiaComentario();
        puntoOrdenDiaComentario.setComentario(comentarioPuntoOrdenDia.getComentario());
        puntoOrdenDiaComentario.setCreadorId(comentarioPuntoOrdenDia.getCreadorId());
        puntoOrdenDiaComentario.setFecha(comentarioPuntoOrdenDia.getFecha());
        puntoOrdenDiaComentario.setPuntoOrdenDia(new PuntoOrdenDia(comentarioPuntoOrdenDia.getPuntoOrdenDiaId()));
        puntoOrdenDiaComentario.setCreadorNombre(personaService.getPersonaFromDirectoryByPersonaId(comentarioPuntoOrdenDia.getCreadorId()).getNombre());
        if(comentarioPuntoOrdenDia.getComentarioSuperiorId() != null)
        {
            PuntoOrdenDiaComentario comentarioPadre = new PuntoOrdenDiaComentario(comentarioPuntoOrdenDia.getComentarioSuperiorId());
            puntoOrdenDiaComentario.setComentarioPadre(comentarioPadre);
        }
        return puntoOrdenDiaComentario;
    }
}
