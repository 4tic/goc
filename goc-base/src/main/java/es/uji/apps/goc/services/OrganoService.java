package es.uji.apps.goc.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.adapter.OrganoCargoAdapter;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.CargoDAO;
import es.uji.apps.goc.dao.OrganoAutorizadoDAO;
import es.uji.apps.goc.dao.OrganoDAO;
import es.uji.apps.goc.dao.OrganoInvitadoDAO;
import es.uji.apps.goc.dao.OrganoReunionInvitadoDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.OrganoAutorizado;
import es.uji.apps.goc.dto.OrganoExterno;
import es.uji.apps.goc.dto.OrganoInvitado;
import es.uji.apps.goc.dto.OrganoLocal;
import es.uji.apps.goc.dto.OrganoLocalCargo;
import es.uji.apps.goc.dto.OrganoLogo;
import es.uji.apps.goc.dto.OrganoLogoPK;
import es.uji.apps.goc.dto.OrganoParametro;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.OrganoNoDisponibleException;
import es.uji.apps.goc.exceptions.OrganosExternosException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.JSONListaOrganosExternosDeserializer;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.OrganoCargo;
import es.uji.apps.goc.model.TipoOrgano;


@Service
@Component
public class OrganoService {
    @Autowired
    private OrganoDAO organoDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private OrganoAutorizadoDAO organoAutorizadoDAO;

    @Autowired
    private OrganoInvitadoDAO organoInvitadoDAO;

    @Autowired
    private OrganoReunionInvitadoDAO organoReunionInvitadoDAO;

    @Autowired
    private CargoDAO cargoDAO;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.external.organosEndpoint}")
    private String organosExternosEndpoint;

    public List<Organo> getOrganosByAdminAndAutorizadoId(
        Long connectedUserId,
        boolean isAdmin
    ) throws OrganosExternosException {
        List<Organo> organos = new ArrayList<>();
        organos.addAll(getOrganosExternos());

        if (isAdmin) {
            organos.addAll(organoDAO.getOrganosByAdmin(false));
        } else {
            organos.addAll(organoDAO.getOrganosByUserId(connectedUserId));

            List<Organo> organosAutorizado = organoDAO.getOrganosByAutorizadoId(connectedUserId);
            organos.addAll(organosAutorizado.stream().filter(o -> !organos.contains(o)).collect(Collectors.toList()));
        }

        return organos;
    }

    public List<Organo> getOrganosPorAutorizadoId(
        Long connectedUserId,
        boolean isAdmin
    ) throws OrganosExternosException {
        List<Organo> organos = new ArrayList<>();
        List<Organo> organosExternos = getOrganosExternos();

        if (isAdmin) {
            organos.addAll(organosExternos);
            organos.addAll(getOrganosByAdmin(true));
        } else {
            List<String> listaOrganosIdsPermitidos = getOrganosIdsPermitidosAutorizado(connectedUserId);
            organos.addAll(organosExternos.stream().filter(o -> listaOrganosIdsPermitidos.contains(o.getId()))
                .collect(Collectors.toList()));

            List<Organo> organosLocales = organoDAO.getOrganosByAutorizadoId(connectedUserId);
            organos.addAll(organosLocales.stream().filter(o -> !o.isInactivo()).collect(Collectors.toList()));
        }

        return organos;
    }

    public boolean existAutorizadosByUserId(Long connectedUserId) {
        return organoAutorizadoDAO.existAutorizadosByUserId(connectedUserId);
    }

    private List<String> getOrganosIdsPermitidosAutorizado(Long connectedUserId) {
        List<OrganoAutorizado> organoAutorizados = organoAutorizadoDAO.getAutorizadosByUserId(connectedUserId);

        return organoAutorizados.stream().filter(c -> c.isOrganoExterno()).map(c -> c.getOrganoId())
            .collect(Collectors.toList());
    }

    @Transactional
    public Organo addOrgano(
        Organo organo,
        Long connectedUserId
    ) {
        return organoDAO.insertOrgano(organo, connectedUserId);
    }

    public Organo updateOrgano(
        Long organoId,
        String nombre,
        String nombreAlternativo,
        Long tipoOrganoId,
        Boolean inactivo,
        Long connectedUserId,
        String email
    ) throws OrganoNoDisponibleException {
        Organo organo = organoDAO.getOrganoById(organoId);

        if (organo == null) {
            throw new OrganoNoDisponibleException();
        }

        organo.setNombre(nombre);
        organo.setNombreAlternativo(nombreAlternativo);

        TipoOrgano tipoOrgano = new TipoOrgano(tipoOrganoId);
        organo.setTipoOrgano(tipoOrgano);
        organo.setInactivo(inactivo);

        actualizarParametros(organoId.toString(), organo.isExterno(), email);
        return organoDAO.updateOrgano(organo);
    }

    @Transactional
    public void actualizarParametros(
        String organoId,
        Boolean externo,
        String email
    ) {
        organoDAO.insertOrUpdateParametros(organoId, externo, email);
    }

    public List<Organo> getOrganosExternos() throws OrganosExternosException {
        WebResource getOrganosResource = Client.create().resource(this.organosExternosEndpoint);

        ClientResponse response =
            getOrganosResource.type(MediaType.APPLICATION_JSON).header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new OrganosExternosException();
        }

        JSONListaOrganosExternosDeserializer jsonDeserializer =
            response.getEntity(JSONListaOrganosExternosDeserializer.class);

        List<OrganoExterno> listaOrganosExternos = jsonDeserializer.getOrganos();
        return organosExternosDTOToOrgano(listaOrganosExternos);
    }

    public List<Organo> getOrganosByReunionId(
        Long reunionId,
        Long connectedUserId
    ) {
        List<Organo> organos = new ArrayList<>();

        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        for (OrganoReunion organoReunion : reunion.getReunionOrganos()) {
            organos.add(getOrganoDeOrganoReunion(organoReunion));
        }

        organos.sort((o1, o2) -> o1.getNombre().compareTo(o2.getNombre()));

        return organos;
    }

    private Organo getOrganoDeOrganoReunion(OrganoReunion organoReunion) {
        Organo organo = new Organo();

        organo.setId(organoReunion.getOrganoId());
        organo.setExterno(organoReunion.isExterno());
        organo.setNombre(organoReunion.getOrganoNombre());
        organo.setNombreAlternativo(organoReunion.getOrganoNombreAlternativo());

        TipoOrgano tipoOrgano = new TipoOrgano(organoReunion.getTipoOrganoId());
        organo.setTipoOrgano(tipoOrgano);

        return organo;
    }

    private List<Organo> organosExternosDTOToOrgano(List<OrganoExterno> listaOrganosExternos) {
        List<Organo> organos = new ArrayList<>();

        for (OrganoExterno organoExterno : listaOrganosExternos) {
            Organo organo = organoExternoDTOToOrgano(organoExterno);
            organos.add(organo);
        }

        return organos;
    }

    private Organo organoExternoDTOToOrgano(OrganoExterno organoExternoDTO) {
        Organo organo = new Organo();

        organo.setId(organoExternoDTO.getId());
        organo.setNombre(organoExternoDTO.getNombre());
        organo.setNombreAlternativo(organoExternoDTO.getNombreAlternativo());
        organo.setInactivo(organoExternoDTO.isInactivo());
        organo.setExterno(true);
        OrganoParametro organoParametro = organoDAO.getOrganoParametro(organo.getId(), true);
        if (organoParametro != null) {
            if (organoParametro.getEmail() != null)
                organo.setEmail(organoParametro.getEmail());
        }

        TipoOrgano tipoOrgano = new TipoOrgano();
        tipoOrgano.setId(organoExternoDTO.getTipoOrganoId());
        tipoOrgano.setNombre(organoExternoDTO.getTipoNombre());
        tipoOrgano.setNombreAlternativo(organoExternoDTO.getTipoNombreAlternativo());
        tipoOrgano.setCodigo(organoExternoDTO.getTipoCodigo());

        organo.setTipoOrgano(tipoOrgano);

        return organo;
    }

    public List<OrganoAutorizado> getAutorizados(
        String organoId,
        Boolean externo
    ) {
        return organoAutorizadoDAO.getAutorizadosByOrgano(organoId, externo);
    }

    public OrganoAutorizado addAutorizado(OrganoAutorizado organoAutorizado) {
        List<OrganoAutorizado> listaAutorizados =
            organoAutorizadoDAO.getAutorizadosByOrgano(organoAutorizado.getOrganoId(),
                organoAutorizado.isOrganoExterno());

        List<OrganoAutorizado> existeAutorizado =
            listaAutorizados.stream().filter(oa -> oa.getPersonaId().equals(organoAutorizado.getPersonaId()))
                .collect(Collectors.toList());

        if (existeAutorizado.size() == 1) {
            return existeAutorizado.get(0);
        }

        return organoAutorizadoDAO.insert(organoAutorizado);
    }

    @Transactional
    public void removeAutorizado(Long organoAutorizadoId) {
        organoAutorizadoDAO.delete(OrganoAutorizado.class, organoAutorizadoId);
    }

    public List<OrganoInvitado> getInvitados(String organoId) {
        return organoInvitadoDAO.getInvitadosByOrgano(organoId);
    }

    public OrganoInvitado addInvitado(OrganoInvitado organoInvitado) {
        List<OrganoInvitado> listaInvitados = organoInvitadoDAO.getInvitadosByOrgano(organoInvitado.getOrganoId());

        List<OrganoInvitado> existeInvitado =
            listaInvitados.stream().filter(oa -> oa.getPersonaId().equals(organoInvitado.getPersonaId()))
                .collect(Collectors.toList());

        if (existeInvitado.size() == 1) {
            return existeInvitado.get(0);
        }

        return organoInvitadoDAO.insert(organoInvitado);
    }

    @Transactional
    public void removeInvitado(Long organoInvitadoId) {
        OrganoInvitado invitado = organoInvitadoDAO.getInvitadoById(organoInvitadoId);
        if (invitado != null) {
            Long personaId = invitado.getPersonaId();
            String organoId = invitado.getOrganoId();
            organoInvitadoDAO.delete(OrganoInvitado.class, organoInvitadoId);

            if (personaId != null && organoId != null && !organoId.equalsIgnoreCase("")) {
                organoReunionInvitadoDAO.removeInvitadoFromReuniones(personaId, organoId);
            }
        }
    }

    public void updateInvitado(OrganoInvitado organoInvitado) {
        organoInvitadoDAO.update(organoInvitado);
    }

    public boolean usuarioConPermisosParaConvocarOrganos(
        List<Organo> organos,
        Long connectedUserId
    ) throws RolesPersonaExternaException {

        Boolean isAdmin = personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador);

        if (isAdmin) {
            return true;
        }

        Boolean permisosAdecuados = true;
        List<OrganoAutorizado> listaPermisosOrganoAutorizado =
            organoAutorizadoDAO.getAutorizadosByUserId(connectedUserId);

        for (Organo organo : organos) {
            Boolean encontrado = false;
            for (OrganoAutorizado organoAutorizado : listaPermisosOrganoAutorizado) {
                if (organoAutorizado.getOrganoId().equals(organo.getId().toString())
                    && organoAutorizado.isOrganoExterno().equals(organo.isExterno())) {
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                permisosAdecuados = false;
                break;
            }
        }

        return permisosAdecuados;
    }

    public Organo getOrganoById(
        Long organoId,
        Long connectedUserId
    ) {
        return organoDAO.getOrganoById(organoId);
    }

    public void updateAutorizado(OrganoAutorizado organo) {
        organoAutorizadoDAO.update(organo);
    }

    public List<Organo> getOrganosByAdmin(boolean soloActivos) throws OrganosExternosException {
        List<Organo> organos = new ArrayList<>();
        organos.addAll(getOrganosExternos());

        List<Organo> organosAutorizado = organoDAO.getOrganosByAdmin(soloActivos);
        organos.addAll(organosAutorizado.stream().filter(o -> !organos.contains(o)).collect(Collectors.toList()));

        return organos;
    }

    public void subeInvitado(Long organoInvitadoId) {
        organoInvitadoDAO.subeInvitado(organoInvitadoId);
    }

    public void bajaInvitado(Long organoInvitadoId) {
        organoInvitadoDAO.bajaInvitado(organoInvitadoId);
    }

    @Transactional
    public void actualizarImagen(
        String organoId,
        boolean externo,
        InputStream data,
        String mime
    ) {
        String imageEncoded = null;
        try {
            byte[] bytes = IOUtils.toByteArray(data);
            imageEncoded = Base64.getEncoder().withoutPadding().encodeToString(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (imageEncoded != null && !imageEncoded.isEmpty()) {
            OrganoLogo organoLogo = new OrganoLogo();
            OrganoLogoPK organoLogoPK = new OrganoLogoPK();
            organoLogoPK.setOrganoId(organoId);
            organoLogoPK.setExterno(externo);
            organoLogo.setOrganoLogoPK(organoLogoPK);
            organoLogo.setData(imageEncoded);
            organoLogo.setExtension(mime);

            organoDAO.update(organoLogo);
        }
    }

    public OrganoLogo getLogo(
        String organoId,
        boolean externo
    ) {
        return organoDAO.getLogo(organoId, externo);
    }

    public void setOrganoOrdenaTrueByOrgano(Organo organo) {
        organo.setOrdenado(true);
        organoDAO.updateOrgano(organo);
    }

    public void setOrganoOrdenaFalseByOrgano(Organo organo) {
        organo.setOrdenado(false);
        organoDAO.updateOrgano(organo);
    }

    public List<Reunion> getReunionesByOrganoId(
        String organoId,
        Long connectedUserId
    ) throws OrganosExternosException, RolesPersonaExternaException {
        if (connectedUserId != null && organoId != null) {
            if (personaService.isAdmin(connectedUserId) || isAutorizadoDelOrganoLocal(organoId, connectedUserId))
                return organoDAO.reunionesByOrganoId(organoId);
        }
        return Collections.emptyList();
    }

    public boolean isAutorizadoDelOrganoLocal(
        String organoId,
        Long connectedUserId
    ) throws OrganosExternosException {
        List<Organo> organosPorAutorizadoId = getOrganosPorAutorizadoId(connectedUserId, false);
        if (organosPorAutorizadoId != null && organosPorAutorizadoId.size() > 0) {
            for (Organo organo : organosPorAutorizadoId) {
                if (organo.getId().equals(organoId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Organo> getOrganosByNombre(
        String organoNombre,
        Long connectedUser
    ) throws RolesPersonaExternaException, OrganosExternosException {
        ArrayList<Organo> organos = new ArrayList<>();
        if (connectedUser != null && organoNombre != null && !organoNombre.isEmpty()) {
            List<OrganoLocal> organosLocalesByNombre = organoDAO.getOrganosByNombre(organoNombre);

            if (organosLocalesByNombre != null && organosLocalesByNombre.size() > 0) {
                for (OrganoLocal organoLocal : organosLocalesByNombre) {
                    if (personaService.isAdmin(connectedUser) || isAutorizadoDelOrganoLocal(
                        organoLocal.getId().toString(), connectedUser)) {
                        Organo organo = new Organo(organoLocal.getId().toString(), organoLocal.getNombre(),
                            organoLocal.getNombreAlternativo(), organoLocal.isInactivo(), organoLocal.getCreadorId(),
                            organoLocal.getFechaCreacion());
                        organos.add(organo);
                    }
                }
            }
        }
        return organos;
    }

    public boolean isExterno(Long organoId) {
        if (organoId == null)
            return true;

        return organoDAO.isExterno(organoId);
    }

    public List<OrganoCargo> getCargosByOrgano(String organoId) {
        List<Cargo> cargos = cargoDAO.getCargos();
        return organoDAO.getCargoByOrgano(organoId).stream().map(
            organoCargoDTO -> OrganoCargoAdapter.fromEntity(organoCargoDTO,
                getCargoFromOrganoLocalCargo(cargos, organoCargoDTO))).collect(Collectors.toList());
    }

    @Transactional
    public OrganoCargo addCargo(OrganoCargo organoCargo) {
        List<OrganoCargo> cargosByOrgano = getCargosByOrgano(organoCargo.getOrganoId());

        Optional<OrganoCargo> organoCargoMismoCargo =
            cargosByOrgano.stream().filter(cargo -> cargo.getCargoId().equals(organoCargo.getCargoId())).findAny();

        return organoCargoMismoCargo.orElseGet(() -> organoDAO.insertOrganoCargo(organoCargo));
    }

    @Transactional
    public void removeCargo(Long organoCargoId) {
        organoDAO.deleteCargoById(organoCargoId);
    }

    public List<Cargo> getCargosDisponiblesByOrgano(String organoId) {
        List<Cargo> cargos = cargoDAO.getCargos();
        List<OrganoLocalCargo> cargosByOrgano = organoDAO.getCargoByOrgano(organoId);
        if (cargosByOrgano.size() > 0) {
            return cargosByOrgano.stream().map(organoCargoDTO -> getCargoFromOrganoLocalCargo(cargos, organoCargoDTO))
                .collect(Collectors.toList());
        } else {
            return cargos;
        }
    }

    private Cargo getCargoFromOrganoLocalCargo(
        List<Cargo> cargos,
        OrganoLocalCargo entity
    ) {
        return cargos.stream().filter(cargo -> cargo.getId().equals(entity.getCargoId())).findAny().get();
    }
}
