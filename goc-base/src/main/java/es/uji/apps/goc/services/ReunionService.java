package es.uji.apps.goc.services;

import com.google.common.base.Strings;

import com.mysema.query.Tuple;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.Utils;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.dao.ClaveDAO;
import es.uji.apps.goc.dao.DescriptorDAO;
import es.uji.apps.goc.dao.MiembroDAO;
import es.uji.apps.goc.dao.OrganoDAO;
import es.uji.apps.goc.dao.OrganoReunionDAO;
import es.uji.apps.goc.dao.OrganoReunionMiembroDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaAcuerdoDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaComentarioDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDescriptorDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDocumentoDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dao.ReunionDiligenciaDAO;
import es.uji.apps.goc.dao.ReunionDocumentoDAO;
import es.uji.apps.goc.dao.ReunionExternosDAO;
import es.uji.apps.goc.dao.ReunionInvitadoDAO;
import es.uji.apps.goc.dao.TipoOrganoDAO;
import es.uji.apps.goc.dto.CargoTemplate;
import es.uji.apps.goc.dto.Clave;
import es.uji.apps.goc.dto.Descriptor;
import es.uji.apps.goc.dto.DescriptorTemplate;
import es.uji.apps.goc.dto.Documentable;
import es.uji.apps.goc.dto.DocumentoFirma;
import es.uji.apps.goc.dto.DocumentoTemplate;
import es.uji.apps.goc.dto.Firmante;
import es.uji.apps.goc.dto.GrupoTemplate;
import es.uji.apps.goc.dto.InvitadoFirma;
import es.uji.apps.goc.dto.InvitadoTemplate;
import es.uji.apps.goc.dto.MiembroFirma;
import es.uji.apps.goc.dto.MiembroTemplate;
import es.uji.apps.goc.dto.OrganoFirma;
import es.uji.apps.goc.dto.OrganoLogo;
import es.uji.apps.goc.dto.OrganoReunion;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.OrganoTemplate;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.PuntoOrdenDiaComentario;
import es.uji.apps.goc.dto.PuntoOrdenDiaDescriptor;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.PuntoOrdenDiaFirma;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.PuntoOrdenDiaTemplate;
import es.uji.apps.goc.dto.QOrganoReunion;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionComentario;
import es.uji.apps.goc.dto.ReunionDiligencia;
import es.uji.apps.goc.dto.ReunionDocumento;
import es.uji.apps.goc.dto.ReunionEditor;
import es.uji.apps.goc.dto.ReunionExternos;
import es.uji.apps.goc.dto.ReunionFirma;
import es.uji.apps.goc.dto.ReunionHojaFirmas;
import es.uji.apps.goc.dto.ReunionInvitado;
import es.uji.apps.goc.dto.ReunionPermiso;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.exceptions.ActaNoGeneradaException;
import es.uji.apps.goc.exceptions.FirmaReunionException;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.exceptions.InvalidAccessException;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.exceptions.OrganosExternosException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.PuntoDelDiaConAcuerdosException;
import es.uji.apps.goc.exceptions.ReunionNoAdmiteDelegacionVotoException;
import es.uji.apps.goc.exceptions.ReunionNoAdmiteSuplenciaException;
import es.uji.apps.goc.exceptions.ReunionNoDisponibleException;
import es.uji.apps.goc.exceptions.ReunionYaCompletadaException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.exceptions.UsuarioYaHaVotadoException;
import es.uji.apps.goc.model.ActaTemplate;
import es.uji.apps.goc.model.AcuerdosSearch;
import es.uji.apps.goc.model.BuscadorReunionesWrapper;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.Comentario;
import es.uji.apps.goc.model.ComentarioPuntoOrdenDia;
import es.uji.apps.goc.model.Convocante;
import es.uji.apps.goc.model.Convocatoria;
import es.uji.apps.goc.model.Externo;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.ResponsableFirma;
import es.uji.apps.goc.model.RespuestaFirma;
import es.uji.apps.goc.model.RespuestaFirmaAsistencia;
import es.uji.apps.goc.model.RespuestaFirmaPuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.model.ReunionAcuerdos;
import es.uji.apps.goc.model.ReunionEstado;
import es.uji.apps.goc.model.ReunionEstados;
import es.uji.apps.goc.notifications.AvisosReunion;
import es.uji.apps.goc.notifications.Mensaje;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.User;

@Service
@Component
public class ReunionService
{
    @Value("${goc.external.firmasEndpoint}")
    private String firmasEndpoint;

    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.publicUrl}")
    private String publicUrl;

    @Value("${goc.mainLanguage}")
    public String mainLanguage;

    @Value("${goc.notificarActaDisponible:false}")
    public boolean notificarActaDisponible;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private OrganoReunionDAO organoReunionDAO;

    @Autowired
    private OrganoReunionMiembroService organoReunionMiembroService;

    @Autowired
    private ReunionComentarioService reunionComentarioService;

    @Autowired
    private OrganoService organoService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ReunionDocumentoDAO reunionDocumentoDAO;

    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private OrganoReunionMiembroDAO organoReunionMiembroDAO;

    @Autowired
    private PuntoOrdenDiaDocumentoDAO puntoOrdenDiaDocumentoDAO;

    @Autowired
    private PuntoOrdenDiaAcuerdoDAO puntoOrdenDiaAcuerdoDAO;

    @Autowired
    private PuntoOrdenDiaDescriptorDAO puntoOrdenDiaDescriptorDAO;

    @Autowired
    private AvisosReunion avisosReunion;

    @Autowired
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @Autowired
    private ReunionInvitadoDAO reunionInvitadoDAO;

    @Autowired
    private TipoOrganoDAO tipoOrganoDAO;

    @Autowired
    private OrganoDAO organoDAO;

    @Autowired
    private DescriptorDAO descriptorDAO;

    @Autowired
    private ClaveDAO claveDAO;

    @Autowired
    private PuntoOrdenDiaComentarioDAO puntoOrdenDiaComentarioDAO;

    @Autowired
    private MiembroDAO miembroDAO;

    @Autowired
    private LanguageConfig languageConfig;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    @Autowired
    private MiembroService miembroService;

    @Autowired
    private ReunionDiligenciaDAO reunionDiligenciaDAO;


    @Autowired
    private ReunionExternosDAO reunionExternosDAO;


    @Autowired
    private VotosService votosService;

    public List<ReunionEditor> getReunionesByEditorId(
        Boolean completada,
        Boolean pasadas,
        String organoId,
        Long tipoOrganoId,
        Boolean externo,
        Long connectedUserId
    )
    {
        List<ReunionEditor> reuniones = reunionDAO.getReunionesByEditorId(connectedUserId, organoId, tipoOrganoId, externo, completada, pasadas);

        return reuniones.stream().distinct().collect(Collectors.toList());
    }

    @Transactional
    public Reunion addReunion(
        Reunion reunion,
        Long connectedUserId
    ) throws PersonasExternasException
    {
        Persona convocante = personaService.getPersonaFromDirectoryByPersonaId(connectedUserId);

        return addReunion(reunion, connectedUserId, convocante);
    }

    @Transactional
    public Reunion addReunion(Reunion reunion, Long connectedUserId, Persona convocante) {
        reunion.setCreadorId(connectedUserId);
        reunion.setCreadorNombre(convocante.getNombre());
        reunion.setCreadorEmail(convocante.getEmail());
        reunion.setNotificada(false);
        reunion.setCompletada(false);
        reunion.setFechaCompletada(null);
        reunion.setAvisoPrimeraReunion(false);
        reunion.setFechaCreacion(new Date());
        reunion.setAvisoPrimeraReunionUserEmail(null);
        reunion.setAvisoPrimeraReunionUser(null);
        reunion.setAvisoPrimeraReunionFecha(null);

        reunion.setMiembroResponsableActa(null);
        reunion.setAcuerdos(null);

        return reunionDAO.insert(reunion);
    }

    @Transactional
    public Reunion updateReunion(
        Long reunionId,
        String asunto,
        String asuntoAlternativo,
        String descripcion,
        String descripcionAlternativa,
        Long duracion,
        Date fecha,
        Date fechaSegundaConvocatoria,
        String ubicacion,
        String ubicacionAlternativa,
        String urlGrabacion,
        Long numeroSesion,
        Boolean publica,
        Boolean telematica,
        Boolean hasVotacion,
        String telematicaDescripcion,
        String telematicaDescripcionAlternativa,
        Boolean admiteSuplencia,
        Boolean admiteDelegacionVoto,
        Boolean admiteComentarios,
        Long connectedUserId
    ) throws ReunionNoDisponibleException
    {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null)
        {
            throw new ReunionNoDisponibleException();
        }

        reunion.setAsunto(asunto);
        reunion.setAsuntoAlternativo(asuntoAlternativo);
        reunion.setDescripcion(descripcion);
        reunion.setDescripcionAlternativa(descripcionAlternativa);
        reunion.setDuracion(duracion);
        reunion.setUbicacion(ubicacion);
        reunion.setUbicacionAlternativa(ubicacionAlternativa);
        reunion.setUrlGrabacion(urlGrabacion);
        reunion.setFecha(fecha);
        reunion.setFechaSegundaConvocatoria(fechaSegundaConvocatoria);
        reunion.setNumeroSesion(numeroSesion);
        reunion.setPublica(publica);
        reunion.setTelematica(telematica);
        reunion.setHasVotacion(hasVotacion);
        reunion.setTelematicaDescripcion(telematicaDescripcion);
        reunion.setTelematicaDescripcionAlternativa(telematicaDescripcionAlternativa);
        reunion.setAdmiteSuplencia(admiteSuplencia);
        reunion.setAdmiteDelegacionVoto(admiteDelegacionVoto);
        reunion.setAdmiteComentarios(admiteComentarios);

        return reunionDAO.update(reunion);
    }

    @Transactional(rollbackFor = {PuntoDelDiaConAcuerdosException.class, ReunionNoDisponibleException.class})
    public void removeReunionById(
        Long reunionId,
        Long connectedUserId
    ) throws ReunionNoDisponibleException, PuntoDelDiaConAcuerdosException, Exception
    {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        Mensaje mensaje = null;
        if (reunion.getAvisoPrimeraReunion())
        {
            mensaje = avisosReunion.getMensajeAvisoAnulacion(reunion);
        }

        if (reunion == null)
        {
            throw new ReunionNoDisponibleException();
        }

        puntoOrdenDiaService.deleteByReunionId(reunionId);
        reunionDAO.deleteByReunionId(reunionId);

        if (reunion.getAvisoPrimeraReunion())
        {
            avisosReunion.enviarAvisoAnulacionReunion(mensaje);
        }
    }


    @Transactional
    public void updateInvitadosByReunionId(
        Long reunionId,
        List<ReunionInvitado> reunionInvitados,
        Long connectedUserId
    ) throws ReunionNoDisponibleException
    {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null)
        {
            throw new ReunionNoDisponibleException();
        }

        reunionInvitadoDAO.deleteByReunionId(reunionId);

        for (ReunionInvitado reunionInvitado : reunionInvitados)
        {
            reunionInvitadoDAO.insert(reunionInvitado);
        }
    }

    @Transactional
    public void updateOrganosReunionByReunionId(
        Long reunionId,
        List<Organo> organos,
        Long connectedUserId
    ) throws ReunionNoDisponibleException, MiembrosExternosException, OrganosExternosException
    {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null)
        {
            throw new ReunionNoDisponibleException();
        }

        List<Long> listaIdsLocales = new ArrayList<>();
        List<String> listaIdsExternos = new ArrayList<>();

        if (organos != null)
        {
            for (Organo organo : organos)
            {
                if (organo.isExterno())
                {
                    listaIdsExternos.add(organo.getId());
                } else
                {
                    listaIdsLocales.add(Long.parseLong(organo.getId()));
                }
            }
        }

        borraOrganosNoNecesarios(reunion, listaIdsExternos, listaIdsLocales);

        addOrganosLocalesNoExistentes(reunion, listaIdsLocales, connectedUserId);
        addOrganosExternosNoExistentes(reunion, listaIdsExternos, connectedUserId);
    }

    private void addOrganosExternosNoExistentes(
        Reunion reunion,
        List<String> listaIdsExternos,
        Long connectedUserId
    ) throws MiembrosExternosException, OrganosExternosException
    {
        List<String> listaActualOrganosExternosIds =
            reunion.getReunionOrganos().stream().filter(el -> el.isExterno()).map(elem -> elem.getOrganoId())
                .collect(Collectors.toList());

        List<Organo> organosExternos = organoService.getOrganosExternos();

        for (String organoId : listaIdsExternos)
        {
            if (!listaActualOrganosExternosIds.contains(organoId))
            {
                OrganoReunion organoReunion = new OrganoReunion();
                organoReunion.setReunion(reunion);
                organoReunion.setOrganoId(organoId);
                organoReunion.setExterno(true);

                Organo organo = getOrganoExternoById(organoId, organosExternos);
                organoReunion.setOrganoNombre(organo.getNombre());
                organoReunion.setOrganoNombreAlternativo(organo.getNombreAlternativo());
                organoReunion.setTipoOrganoId(organo.getTipoOrgano().getId());

                organoReunionDAO.insert(organoReunion);

                organoReunionMiembroService.addOrganoReunionMiembros(organoReunion, connectedUserId);
            }
        }
    }

    private Organo getOrganoExternoById(
        String organoId,
        List<Organo> organosExternos
    )
    {
        for (Organo organo : organosExternos)
        {
            if (organo.getId().equals(organoId))
            {
                return organo;
            }
        }

        return null;
    }

    private void addOrganosLocalesNoExistentes(
        Reunion reunion,
        List<Long> listaIdsLocales,
        Long connectedUserId
    ) throws MiembrosExternosException
    {
        List<Long> listaActualOrganosLocalesIds =
            reunion.getReunionOrganos().stream().filter(el -> el.isExterno() == false)
                .map(elem -> Long.parseLong(elem.getOrganoId())).collect(Collectors.toList());

        for (Long organoId : listaIdsLocales)
        {
            if (!listaActualOrganosLocalesIds.contains(organoId))
            {
                Organo organo = organoService.getOrganoById(organoId, connectedUserId);
                OrganoReunion organoReunion = new OrganoReunion();
                organoReunion.setReunion(reunion);
                organoReunion.setOrganoId(organoId.toString());
                organoReunion.setExterno(false);
                organoReunion.setOrganoNombre(organo.getNombre());
                organoReunion.setOrganoNombreAlternativo(organo.getNombreAlternativo());
                organoReunion.setTipoOrganoId(organo.getTipoOrgano().getId());

                organoReunionDAO.insert(organoReunion);

                organoReunionMiembroService.addOrganoReunionMiembros(organoReunion, connectedUserId);
            }
        }
    }

    private void borraOrganosNoNecesarios(
        Reunion reunion,
        List<String> listaIdsExternos,
        List<Long> listaIdsLocales
    )
    {
        for (OrganoReunion cr : reunion.getReunionOrganos())
        {
            if (!cr.isExterno() && !listaIdsLocales.contains(Long.parseLong(cr.getOrganoId())))
            {
                organoReunionDAO.delete(OrganoReunion.class, cr.getId());
            }

            if (cr.isExterno() && !listaIdsExternos.contains(cr.getOrganoId()))
            {
                organoReunionDAO.delete(OrganoReunion.class, cr.getId());
            }

        }
    }

    @Transactional(rollbackFor = {FirmaReunionException.class, FirmantesNecesariosException.class, FirmanteSinSuplenteNoAsiste.class})
    public void updateEstadoReunion(Long reunionId, ReunionEstado reunionEstado, Long connectedUserId)
        throws Exception {
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);
        if (reunion.getCompletada() != null && reunion.getCompletada()) {
            throw new ReunionYaCompletadaException();
        }
        else {
            if (reunion != null) {
                reunion.setAcuerdos(reunionEstado.getAcuerdos());
                reunion.setAcuerdosAlternativos(reunionEstado.getAcuerdosAlternativos());
                reunion.setObservaciones(reunionEstado.getObservaciones());

                if (reunionEstado.getEstado() != null) {
                    RespuestaFirma respuestaFirma = null;
                    if (reunionEstado.getEstado().equals(ReunionEstados.CERRADA_CON_FIRMA) || reunionEstado.getEstado().equals(ReunionEstados.CERRADA)) {
                        checkReunionToClose(reunionId, connectedUserId);
                    }

                    if (reunionEstado.getEstado().equals(ReunionEstados.CERRADA_CON_FIRMA)) {
                        respuestaFirma = enviarPeticionFirmarReunion(reunion, reunionEstado.getResponsable(), connectedUserId);
                    }

                    if (reunionEstado.getEstado().equals(ReunionEstados.CERRADA_CON_FIRMA) || reunionEstado.getEstado().equals(ReunionEstados.CERRADA)) {
                        if (respuestaFirma != null) {
                            marcarReunionComoCompletadaConRespuestaFirma(reunion, respuestaFirma, reunionEstado.getResponsable());
                        } else {
                            reunionDAO.marcarReunionComoCompletadaYActualizarAcuerdo(reunion.getId(), reunionEstado.getResponsable(),
                                reunion.getAcuerdos(), reunion.getAcuerdosAlternativos(), reunion.getObservaciones());
                        }
                    }
                }
            } else {
                throw new ReunionNoDisponibleException();
            }
        }
    }

    private RespuestaFirma enviarPeticionFirmarReunion(Reunion reunion, Long responsableActaId, Long connectedUserId) throws Exception {
        Client client = Client.create(Utils.createClientConfig());
        WebResource getFirmasResource = client.resource(this.firmasEndpoint);

        ReunionFirma reunionFirma = reunionFirmaDesdeReunion(reunion, responsableActaId, connectedUserId);

        ClientResponse response =
            getFirmasResource.type(MediaType.APPLICATION_JSON).header("X-UJI-AuthToken", authToken)
                .post(ClientResponse.class, reunionFirma);

        if (response.getStatus() == HttpStatus.SC_OK || response.getStatus() == HttpStatus.SC_ACCEPTED)
        {
            puntoOrdenDiaDAO.setFechaAperturaVotacionPuntos(reunionFirma.getId(), null);

            if (response.getStatus() == HttpStatus.SC_OK) {
                UIEntity entity = response.getEntity(UIEntity.class);
                List<UIEntity> responseData = entity != null && entity.getRelations() != null ? entity.getRelations().get("data") : null;
                if (responseData != null) {
                    return RespuestaFirma.buildRespuestaFirma(responseData);
                } else {
                    return null;
                }
            }
            else {
                return null;
            }
        }
        else {
            String message = response.getEntity(String.class);
            if (message != null) {
                throw new FirmaReunionException(message);
            }
            throw new FirmaReunionException();
        }
    }

    private void marcarReunionComoCompletadaConRespuestaFirma(
        Reunion reunion,
        RespuestaFirma respuestaFirma,
        Long responsableActaId
    ) throws Exception {
        actualizarAcuerdosPuntosDelDia(respuestaFirma);
        actualizarAsistencias(reunion.getId(), respuestaFirma);
        reunionDAO.marcarReunionComoCompletadaYActualizarAcuerdoYUrl(reunion.getId(), responsableActaId, reunion
                .getAcuerdos(),
            reunion.getAcuerdosAlternativos(), reunion.getObservaciones(), respuestaFirma);

        String urlActa = respuestaFirma.getUrlActa();
        if(urlActa != null && notificarActaDisponible){
            avisosReunion.enviaAvisoActaReunionDisponible(reunion, urlActa);
        }
    }


    private void actualizarAsistencias(
        Long reunionId,
        RespuestaFirma respuestaFirma
    )
    {
        for (RespuestaFirmaAsistencia respuestaFirmaAsistencia : respuestaFirma.getAsistencias())
        {
            reunionDAO.updateAsistenciaInvitadoReunion(reunionId, respuestaFirmaAsistencia);
            reunionDAO.updateAsistenciaInvitadoOrgano(reunionId, respuestaFirmaAsistencia);
            reunionDAO.updateAsistenciaMiembrosYSuplentes(reunionId, respuestaFirmaAsistencia);
        }
    }

    private void actualizarAcuerdosPuntosDelDia(RespuestaFirma respuestaFirma)
    {
        for (RespuestaFirmaPuntoOrdenDiaAcuerdo acuerdo : respuestaFirma.getPuntoOrdenDiaAcuerdos())
        {
            reunionDAO.updateAcuerdoPuntoDelDiaUrlActa(acuerdo);
        }
    }

    private ReunionFirma reunionFirmaDesdeReunion(Reunion reunion, Long responsableActaId, Long connectedUserId)
        throws ActaNoGeneradaException, IOException, NoSuchAlgorithmException {
        ReunionFirma reunionFirma = new ReunionFirma();

        reunionFirma.setId(reunion.getId());
        reunionFirma.setAsunto(reunion.getAsunto());
        reunionFirma.setAsuntoAlternativo(reunion.getAsuntoAlternativo());
        reunionFirma.setDescripcion(reunion.getDescripcion());
        reunionFirma.setDescripcionAlternativa(reunion.getDescripcionAlternativa());
        reunionFirma.setDuracion(reunion.getDuracion());
        reunionFirma.setNumeroSesion(reunion.getNumeroSesion());
        reunionFirma.setObservaciones(reunion.getObservaciones());
        reunionFirma.setAcuerdos(reunion.getAcuerdos());
        reunionFirma.setAcuerdosAlternativos(reunion.getAcuerdosAlternativos());
        reunionFirma.setUbicacion(reunion.getUbicacion());
        reunionFirma.setUbicacionAlternativa(reunion.getUbicacionAlternativa());
        reunionFirma.setFecha(reunion.getFecha());
        reunionFirma.setUrlGrabacion(reunion.getUrlGrabacion());
        reunionFirma.setTelematica(reunion.isTelematica());
        reunionFirma.setTelematicaDescripcion(reunion.getTelematicaDescripcion());
        reunionFirma.setTelematicaDescripcionAlternativa(reunion.getTelematicaDescripcionAlternativa());
        reunionFirma.setAdmiteSuplencia(reunion.isAdmiteSuplencia());
        reunionFirma.setAdmiteDelegacionVoto(reunion.isAdmiteDelegacionVoto());
        reunionFirma.setCompletada(reunion.getCompletada());
        reunionFirma.setCreadorNombre(reunion.getCreadorNombre());
        reunionFirma.setCreadorEmail(reunion.getCreadorEmail());
        reunionFirma.setReabierta(reunion.isReabierta());

        if (responsableActaId != null)
        {
            OrganoReunionMiembro responsable = organoReunionMiembroDAO.getMiembroById(responsableActaId);

            reunionFirma.setResponsableActaId(responsable.getMiembroId());
            reunionFirma.setResponsableActaEmail(responsable.getEmail());
            reunionFirma.setResponsableActa(responsable.getNombre());
            reunionFirma.setCargoResponsableActa(responsable.getCargoNombre());
            reunionFirma.setCargoAlternativoResponsableActa(responsable.getCargoNombreAlternativo());
        }

        List<Organo> organos = organoService.getOrganosByReunionId(reunion.getId(), connectedUserId);
        List<ReunionComentario> comentarios =
            reunionComentarioService.getComentariosByReunionId(reunion.getId(), connectedUserId);

        List<OrganoFirma> listaOrganosFirma = getOrganosFirmaDesdeOrganos(organos, reunion);
        reunionFirma.setOrganos(listaOrganosFirma);

        List<ReunionDocumento> reunionDocumentos = reunionDocumentoDAO.getDocumentosByReunionId(reunion.getId());
        List<DocumentoFirma> listaDocumentosFirma = getReunionDocumentosFirmaDesdeDocumentos(reunionDocumentos);

        reunionFirma.setDocumentos(listaDocumentosFirma);
        reunionFirma.setComentarios(getComentariosFirmaDesdeComentarios(comentarios));

        List<PuntoOrdenDiaMultinivel> puntosOrdenDia =
                puntoOrdenDiaService.getPuntosMultinivelByReunionId(reunion.getId());
        List<PuntoOrdenDiaFirma> listaPuntosOrdenDiaFirma = getPuntosOrdenDiaFirmaDesdePuntosOrdenDia(puntosOrdenDia);

        reunionFirma.setPuntosOrdenDia(listaPuntosOrdenDiaFirma);

        List<Persona> invitados = reunionDAO.getInvitadosPresencialesByReunionId(reunion.getId());
        List<InvitadoFirma> invitadosFirma = getInvitadosFirmaDesdeReunionInvitados(invitados);
        reunionFirma.setInvitados(invitadosFirma);

        reunionFirma.setFirmantes(getFirmantesConAsistenciaConfirmada(reunionFirma));

        if (personalizationConfig.enviarBase64AFirma)
        {
            try
            {
                ActaTemplate actaTemplate =
                    getActaTemplateFromReunion(reunion, connectedUserId, languageConfig.mainLanguage, false);
                Template templatePDF = actaTemplate.toTemplate(personalizationConfig, personalizationConfig.showAusentes);
                reunionFirma.setActaBase64(Base64.getEncoder().withoutPadding().encodeToString(templatePDF.process()));
            } catch (Exception e) {
                throw new ActaNoGeneradaException();
            }
        }

        return reunionFirma;
    }

    private List<ResponsableFirma> getFirmantesConAsistenciaConfirmada(ReunionFirma reunionFirma) {
        List<ResponsableFirma> responsablesFirma = new ArrayList<>();
        for (OrganoFirma organoFirma : reunionFirma.getOrganos())
        {
            for (MiembroFirma miembroFirma : organoFirma.getMiembros())
            {
                if (miembroFirma.getAsistencia() != null && miembroFirma.getAsistencia()) {
                    ResponsableFirma firmanteFromMiembro = getFirmanteFromMiembro(miembroFirma, organoFirma);
                    if (firmanteFromMiembro != null) {
                        responsablesFirma.add(firmanteFromMiembro);
                    }
                }
            }
        }

        return responsablesFirma;
    }

    private boolean isFirmanteValid(Firmante firmante) throws FirmanteSinSuplenteNoAsiste {
        if(firmante.isFirmante()) {
            if (firmante.getAsistencia() == null || !firmante.getAsistencia()) {
                ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18n", mainLanguage);
                if (firmante.getSuplenteId() == null){
                    throw new FirmanteSinSuplenteNoAsiste(String
                        .format(resourceBundle.getString("exception.FirmanteNoAsiste"), firmante.getNombre()));
                }
                else {

                    throw new FirmanteSinSuplenteNoAsiste(String
                        .format(resourceBundle.getString("exception.FirmanteSuplenteNoAsiste"), firmante.getNombre()));
                }
            }
            else {
                return true;
            }
        }
        return false;
    }

    private List<InvitadoFirma> getInvitadosFirmaDesdeReunionInvitados(List<Persona> invitados)
    {
        List<InvitadoFirma> listaInvitadosFirma = new ArrayList<>();

        for (Persona invitado : invitados)
        {
            listaInvitadosFirma.add(getInvitadoFirmaDesdeInvitado(invitado));
        }

        return listaInvitadosFirma;
    }

    private InvitadoFirma getInvitadoFirmaDesdeInvitado(Persona invitado)
    {
        InvitadoFirma invitadoFirma = new InvitadoFirma();

        invitadoFirma.setId(invitado.getId());
        invitadoFirma.setNombre(invitado.getNombre());
        invitadoFirma.setEmail(invitado.getEmail());

        return invitadoFirma;
    }

    private List<PuntoOrdenDiaFirma> getPuntosOrdenDiaFirmaDesdePuntosOrdenDia(
        List<PuntoOrdenDiaMultinivel> puntosOrdenDia
    ) throws IOException, NoSuchAlgorithmException
    {
        List<PuntoOrdenDiaFirma> listaPuntosOrdenDiaFirma = new ArrayList<>();

        for (PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDia)
        {
            listaPuntosOrdenDiaFirma.add(getPuntoOrdenDiaFirmaDesdePuntoOrdenDia(puntoOrdenDia));
        }
        return listaPuntosOrdenDiaFirma;
    }

    private PuntoOrdenDiaFirma getPuntoOrdenDiaFirmaDesdePuntoOrdenDia(PuntoOrdenDiaMultinivel puntoOrdenDia)
        throws IOException, NoSuchAlgorithmException
    {
        PuntoOrdenDiaFirma puntoOrdenDiaFirma = new PuntoOrdenDiaFirma();
        puntoOrdenDiaFirma.setId(puntoOrdenDia.getId());
        puntoOrdenDiaFirma.setTitulo(puntoOrdenDia.getTitulo());
        puntoOrdenDiaFirma.setTituloAlternativo(puntoOrdenDia.getTituloAlternativo());
        puntoOrdenDiaFirma.setAcuerdos(puntoOrdenDia.getAcuerdos());
        puntoOrdenDiaFirma.setAcuerdosAlternativos(puntoOrdenDia.getAcuerdosAlternativos());
        puntoOrdenDiaFirma.setDeliberaciones(puntoOrdenDia.getDeliberaciones());
        puntoOrdenDiaFirma.setDeliberacionesAlternativas(puntoOrdenDia.getDeliberacionesAlternativas());
        puntoOrdenDiaFirma.setDescripcion(puntoOrdenDia.getDescripcion());
        puntoOrdenDiaFirma.setDescripcionAlternativa(puntoOrdenDia.getDescripcionAlternativa());
        puntoOrdenDiaFirma.setOrden(puntoOrdenDia.getOrden());
        puntoOrdenDiaFirma.setEditado(puntoOrdenDia.getEditado());

        List<PuntoOrdenDiaDocumento> documentos =
            puntoOrdenDiaDocumentoDAO.getDocumentosByPuntoOrdenDiaId(puntoOrdenDia.getId());

        List<PuntoOrdenDiaAcuerdo> documentosAcuerdos =
            puntoOrdenDiaAcuerdoDAO.getAcuerdosByPuntoOrdenDiaId(puntoOrdenDia.getId());

        puntoOrdenDiaFirma.setDocumentos(getDocumentosFirmaDesdePuntosOrdenDiaDocumentos(documentos));
        puntoOrdenDiaFirma
            .setDocumentosAcuerdos(getDocumentosAcuerdosFirmaDesdePuntosOrdenDiaDocumentos(documentosAcuerdos));

        if (puntoOrdenDia.getPuntosInferiores() != null)
        {
            List<PuntoOrdenDiaFirma> subpuntosFirma = new ArrayList<>();

            for (PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel : getsubpuntosOrdenDiaOrdenados(puntoOrdenDia))
            {
                subpuntosFirma.add(getPuntoOrdenDiaFirmaDesdePuntoOrdenDia(puntoOrdenDiaMultinivel));
            }

            puntoOrdenDiaFirma.setSubpuntos(subpuntosFirma);
        }

        return puntoOrdenDiaFirma;
    }

    private List<PuntoOrdenDiaMultinivel> getsubpuntosOrdenDiaOrdenados(PuntoOrdenDiaMultinivel puntoOrdenDia)
    {
        return puntoOrdenDia.getPuntosInferiores().stream().sorted().collect(Collectors.toList());
    }

    private List<DocumentoFirma> getDocumentosAcuerdosFirmaDesdePuntosOrdenDiaDocumentos(
        List<PuntoOrdenDiaAcuerdo> documentos
    ) throws IOException, NoSuchAlgorithmException
    {
        List<DocumentoFirma> listaDocumentoTemplate = new ArrayList<>();

        for (PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo : documentos)
        {
            listaDocumentoTemplate.add(getAcuerdoDocumentoFirmaDesdePuntoOrdenDiaDocumento(puntoOrdenDiaAcuerdo));
        }

        return listaDocumentoTemplate;
    }

    private DocumentoFirma getAcuerdoDocumentoFirmaDesdePuntoOrdenDiaDocumento(
        PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo
    ) throws IOException, NoSuchAlgorithmException
    {
        DocumentoFirma documentoFirma = new DocumentoFirma();

        documentoFirma.setId(puntoOrdenDiaAcuerdo.getId());
        documentoFirma.setDescripcion(puntoOrdenDiaAcuerdo.getDescripcion());
        documentoFirma.setDescripcionAlternativa(puntoOrdenDiaAcuerdo.getDescripcionAlternativa());
        documentoFirma.setMimeType(puntoOrdenDiaAcuerdo.getMimeType());
        documentoFirma.setFechaAdicion(puntoOrdenDiaAcuerdo.getFechaAdicion());
        documentoFirma.setCreadorId(puntoOrdenDiaAcuerdo.getCreadorId());
        documentoFirma.setNombreFichero(puntoOrdenDiaAcuerdo.getNombreFichero());
        documentoFirma.setHash(Utils.getHash(puntoOrdenDiaAcuerdo.getDatos()));
        documentoFirma.setActaBase64(Base64.getEncoder().withoutPadding().encodeToString(puntoOrdenDiaAcuerdo.getDatos()));
        documentoFirma.setUrlDescarga(
            publicUrl + personalizationConfig.appContext + "/rest/reuniones/" + puntoOrdenDiaAcuerdo.getPuntoOrdenDia().getReunion().getId()
                + "/puntosOrdenDia/" + puntoOrdenDiaAcuerdo.getPuntoOrdenDia().getId() + "/acuerdos/"
                + puntoOrdenDiaAcuerdo.getId() + "/descargar");

        return documentoFirma;
    }

    private List<DocumentoFirma> getDocumentosFirmaDesdePuntosOrdenDiaDocumentos(
        List<PuntoOrdenDiaDocumento> documentos
    ) throws IOException, NoSuchAlgorithmException
    {
        List<DocumentoFirma> listaDocumentoTemplate = new ArrayList<>();

        for (PuntoOrdenDiaDocumento puntoOrdenDiaDocumento : documentos)
        {
            listaDocumentoTemplate.add(getDocumentoFirmaDesdePuntoOrdenDiaDocumento(puntoOrdenDiaDocumento));
        }

        return listaDocumentoTemplate;
    }

    private DocumentoFirma getDocumentoFirmaDesdePuntoOrdenDiaDocumento(PuntoOrdenDiaDocumento puntoOrdenDiaDocumento)
        throws IOException, NoSuchAlgorithmException
    {
        DocumentoFirma documentoFirma = new DocumentoFirma();

        documentoFirma.setId(puntoOrdenDiaDocumento.getId());
        documentoFirma.setDescripcion(puntoOrdenDiaDocumento.getDescripcion());
        documentoFirma.setDescripcionAlternativa(puntoOrdenDiaDocumento.getDescripcionAlternativa());
        documentoFirma.setMimeType(puntoOrdenDiaDocumento.getMimeType());
        documentoFirma.setFechaAdicion(puntoOrdenDiaDocumento.getFechaAdicion());
        documentoFirma.setCreadorId(puntoOrdenDiaDocumento.getCreadorId());
        documentoFirma.setNombreFichero(puntoOrdenDiaDocumento.getNombreFichero());
        documentoFirma.setHash(Utils.getHash(puntoOrdenDiaDocumento.getDatos()));
        documentoFirma.setUrlDescarga(
            publicUrl + personalizationConfig.appContext + "/rest/reuniones/" + puntoOrdenDiaDocumento.getPuntoOrdenDia().getReunion().getId()
                + "/puntosOrdenDia/" + puntoOrdenDiaDocumento.getPuntoOrdenDia().getId() + "/documentos/"
                + puntoOrdenDiaDocumento.getId() + "/descargar");

        return documentoFirma;
    }

    private List<Comentario> getComentariosFirmaDesdeComentarios(List<ReunionComentario> comentarios)
    {
        List<Comentario> listaComentariosFirma = new ArrayList<>();

        for (ReunionComentario comentario : comentarios)
        {
            listaComentariosFirma.add(getComentarioFirmaDesdeComentario(comentario));
        }

        return listaComentariosFirma;
    }

    private Comentario getComentarioFirmaDesdeComentario(ReunionComentario comentario)
    {
        Comentario comentarioFirma = new Comentario();
        comentarioFirma.setId(comentario.getId());
        comentarioFirma.setComentario(comentario.getComentario());
        comentarioFirma.setCreadorNombre(comentario.getCreadorNombre());
        comentarioFirma.setCreadorId(comentario.getCreadorId());
        comentarioFirma.setFecha(comentario.getFecha());

        return comentarioFirma;
    }

    private List<DocumentoFirma> getReunionDocumentosFirmaDesdeDocumentos(List<ReunionDocumento> reunionDocumentos)
        throws IOException, NoSuchAlgorithmException
    {
        List<DocumentoFirma> listaDocumentoFirma = new ArrayList<>();

        for (ReunionDocumento reunionDocumento : reunionDocumentos)
        {
            listaDocumentoFirma.add(getDocumentoFirmaDesdeReunionDocumento(reunionDocumento));
        }

        return listaDocumentoFirma;
    }

    private DocumentoFirma getDocumentoFirmaDesdeReunionDocumento(ReunionDocumento reunionDocumento)
        throws IOException, NoSuchAlgorithmException
    {
        DocumentoFirma documentoFirma = new DocumentoFirma();

        documentoFirma.setId(reunionDocumento.getId());
        documentoFirma.setDescripcion(reunionDocumento.getDescripcion());
        documentoFirma.setDescripcionAlternativa(reunionDocumento.getDescripcionAlternativa());
        documentoFirma.setMimeType(reunionDocumento.getMimeType());
        documentoFirma.setFechaAdicion(reunionDocumento.getFechaAdicion());
        documentoFirma.setCreadorId(reunionDocumento.getCreadorId());
        documentoFirma.setNombreFichero(reunionDocumento.getNombreFichero());
        documentoFirma.setHash(Utils.getHash(reunionDocumento.getDatos()));
        documentoFirma.setUrlDescarga(
            publicUrl + personalizationConfig.appContext + "/rest/reuniones/" + reunionDocumento.getReunion().getId() + "/documentos/"
                + reunionDocumento.getId() + "/descargar");

        return documentoFirma;
    }


    public ReunionTemplate getReunionTemplateDesdeReunionForBuscador(Reunion reunion, boolean showFirmantes, Long connectedUserId, boolean isMainLangauge)
    {
        Boolean withNoasistentes = showFirmantes ? true : false;
        ReunionTemplate reunionTemplate =
            getReunionTemplate(reunion, isMainLangauge, withNoasistentes, connectedUserId, true);
        if (showFirmantes) {
            reunionTemplate.setFirmantes(getFirmantesConAsistenciaConfirmada(reunionTemplate));
        }
        return reunionTemplate;
    }

    public ReunionTemplate getReunionTemplateDesdeReunion(Reunion reunion, Long connectedUserId,
            Boolean withNoAsistentes, Boolean mainLanguage)
    {
        ReunionTemplate reunionTemplate = getReunionTemplate(reunion, mainLanguage, withNoAsistentes,connectedUserId, false);

        OrganoReunionMiembro responsable = new OrganoReunionMiembro();

        if (reunion.getMiembroResponsableActa() != null)
        {
            responsable = organoReunionMiembroDAO.getMiembroById(reunion.getMiembroResponsableActa().getId());

            reunionTemplate.setResponsableActa(responsable.getNombre());
            reunionTemplate.setCargoResponsableActa(
                    mainLanguage ? responsable.getCargoNombre() : responsable.getCargoNombreAlternativo());
        }

        List<ReunionComentario> comentarios =
                reunionComentarioService.getComentariosByReunionId(reunion.getId(), connectedUserId);

        List<ReunionDocumento> reunionDocumentos = reunionDocumentoDAO.getDatosDocumentosByReunionId(reunion.getId());
        List<DocumentoTemplate> listaDocumentosTemplate =
                getReunionDocumentosTemplateDesdeDocumentos(reunionDocumentos, mainLanguage);

        reunionTemplate.setDocumentos(listaDocumentosTemplate);
        reunionTemplate.setComentarios(getComentariosTemplateDessdeComentarios(comentarios));

        List<Persona> invitados = reunionDAO.getInvitadosPresencialesByReunionId(reunion.getId());
        List<InvitadoTemplate> invitadosTemplate = getInvitadosTemplateDesdeReunionInvitados(invitados,mainLanguage);
        reunionTemplate.setInvitados(invitadosTemplate);

        reunionTemplate.setFirmantes(getFirmantesConAsistenciaConfirmada(reunionTemplate));

        if (reunion.getMiembroResponsableActa() != null)
        {
            responsable = organoReunionMiembroDAO.getMiembroById(reunion.getMiembroResponsableActa().getId());

            reunionTemplate.setResponsableActa(responsable.getNombre());
            reunionTemplate.setCargoResponsableActa(
                    mainLanguage ? responsable.getCargoNombre() : responsable.getCargoNombreAlternativo());
            reunionTemplate.setResponsableActaMiembroId(responsable.getMiembroId());
            reunionTemplate.setResponsableActaCargoId(responsable.getCargoId());
        }
        return reunionTemplate;
    }

    private ReunionTemplate getReunionTemplate(Reunion reunion, Boolean mainLanguage, Boolean withNoAsistentes ,Long connectedUserId, Boolean isForBuscador)
    {
        ReunionTemplate reunionTemplate = new ReunionTemplate();

        reunionTemplate.setId(reunion.getId());
        reunionTemplate.setAsunto(mainLanguage ? reunion.getAsunto() : reunion.getAsuntoAlternativo());
        reunionTemplate.setDescripcion(mainLanguage ? reunion.getDescripcion() : reunion.getDescripcionAlternativa());
        reunionTemplate.setDuracion(reunion.getDuracion());
        reunionTemplate.setNumeroSesion(reunion.getNumeroSesion());
        reunionTemplate.setAcuerdos(mainLanguage ? reunion.getAcuerdos() : reunion.getAcuerdosAlternativos());
        reunionTemplate.setUbicacion(mainLanguage ? reunion.getUbicacion() : reunion.getUbicacionAlternativa());
        reunionTemplate.setFecha(reunion.getFecha());
        reunionTemplate.setFechaSegundaConvocatoria(reunion.getFechaSegundaConvocatoria());
        reunionTemplate.setUrlGrabacion(reunion.getUrlGrabacion());
        reunionTemplate.setTelematica(reunion.isTelematica());
        reunionTemplate.setTelematicaDescripcion(
                mainLanguage ? reunion.getTelematicaDescripcion() : reunion.getTelematicaDescripcionAlternativa());
        reunionTemplate.setPublica(reunion.isPublica());
        reunionTemplate.setAdmiteSuplencia(reunion.isAdmiteSuplencia());
        reunionTemplate.setAdmiteDelegacionVoto(reunion.isAdmiteDelegacionVoto());
        reunionTemplate.setAdmiteComentarios(reunion.isAdmiteComentarios());
        reunionTemplate.setCompletada(reunion.getCompletada());
        reunionTemplate.setCreadorNombre(reunion.getCreadorNombre());
        reunionTemplate.setCreadorEmail(reunion.getCreadorEmail());
        reunionTemplate.setCreadorId(reunion.getCreadorId());
        reunionTemplate.setExtraordinaria(reunion.getExtraordinaria());
        reunionTemplate.setUrlActa(mainLanguage ? reunion.getUrlActa() : reunion.getUrlActaAlternativa());
        reunionTemplate.setObservaciones(reunion.getObservaciones());
        reunionTemplate.setConvocante(reunion.getConvocante());
        reunionTemplate.setConvocanteEmail(reunion.getConvocanteEmail());
        reunionTemplate.setHasVotacion(reunion.getHasVotacion());

        OrganoReunionMiembro responsable;

        Set<OrganoReunion> reunionOrganos = reunion.getReunionOrganos();
        if (reunionOrganos != null && (!withNoAsistentes && isForBuscador)) {
            ArrayList<OrganoTemplate> organoTemplates = new ArrayList<>();
            for (OrganoReunion reunionOrgano : reunionOrganos) {
                organoTemplates.add(getOrganoTemplateDesdeOrganoReunion(reunionOrgano, mainLanguage));
            }
            reunionTemplate.setOrganos(organoTemplates);
        } else {
            List<Organo> organos = organoService.getOrganosByReunionId(reunion.getId(), connectedUserId);
            List<OrganoTemplate> listaOrganosTemplate =
                    getOrganosTemplateDesdeOrganos(organos, reunion, withNoAsistentes, mainLanguage);
            reunionTemplate.setOrganos(listaOrganosTemplate);
        }
        List<PuntoOrdenDiaMultinivel> puntosOrdenDia = new ArrayList<>();
        if (isForBuscador) {
            puntosOrdenDia.addAll(puntoOrdenDiaService.getPuntosMultinivelByReunionIdForBucador(reunion.getId()));
        } else {
            puntosOrdenDia.addAll(puntoOrdenDiaService.getPuntosMultinivelByReunionId(reunion.getId()));
        }

        List<PuntoOrdenDiaTemplate> listaPuntosOrdenDiaTemplate =
                getPuntosOrdenDiaTemplateDesdePuntosOrdenDia(puntosOrdenDia, mainLanguage, isForBuscador);
        try {
            reunionTemplate.setPuntosOrdenDia(puntoOrdenDiaService.refactorizarHTMLaXHTML(listaPuntosOrdenDiaTemplate));
        } catch (Exception e) {
            reunionTemplate.setPuntosOrdenDia(listaPuntosOrdenDiaTemplate);
        }

        return reunionTemplate;
    }

    private OrganoTemplate getOrganoTemplateDesdeOrganoReunion(OrganoReunion organoReunion, boolean isMainLanguage)
    {
        OrganoTemplate organoTemplate = new OrganoTemplate();
        organoTemplate.setId(organoReunion.getOrganoId());
        organoTemplate.setNombre(isMainLanguage?organoReunion.getOrganoNombre():organoReunion.getOrganoNombreAlternativo());
        return organoTemplate;
    }

    public List<ResponsableFirma> getFirmantesConAsistenciaConfirmada(ReunionTemplate reunionTemplate) {
        List<ResponsableFirma> responsablesFirma = new ArrayList<>();
        for (OrganoTemplate organoTemplate : reunionTemplate.getOrganos())
        {
            for (MiembroTemplate miembroTemplate : organoTemplate.getAsistentes())
            {
                ResponsableFirma firmanteFromMiembro = getFirmanteFromMiembro(miembroTemplate, organoTemplate);
                if (firmanteFromMiembro != null) {
                    responsablesFirma.add(firmanteFromMiembro);
                }
            }
        }

        return responsablesFirma;
    }

    ResponsableFirma getFirmanteFromMiembro(Firmante firmante, es.uji.apps.goc.dto.Organo organo) {
        if (isFirmanteSinSuplente(firmante))
        {
            return new ResponsableFirma(firmante, organo.getNombre(), "", false, "");
        }
        else if(isFirmanteConSuplente(firmante))
        {
            Miembro suplente = miembroDAO.getMiembroByPersonaIdYOrganoId(firmante.getSuplenteId(), Long.parseLong(organo.getId()));

            if(suplente != null) {
                MiembroFirma miembroFirmaSuplente = new MiembroFirma(suplente);
                return new ResponsableFirma(miembroFirmaSuplente, organo.getNombre(), organo.getNombreAlternativo(), true, firmante.getNombre());
            } else {
                suplente = new Miembro();
                suplente.setId(firmante.getSuplenteId());
                suplente.setNombre(firmante.getSuplente());
                return new ResponsableFirma(suplente, true, firmante.getNombre());
            }
        }
        return null;
    }

    private boolean isFirmanteConSuplente(Firmante miembroTemplate) {
        return miembroTemplate.getEmail() != null && miembroTemplate.isFirmante() && miembroTemplate.getSuplenteId() != null;
    }

    private boolean isFirmanteSinSuplente(Firmante miembroTemplate) {
        return miembroTemplate.getEmail() != null && miembroTemplate.isFirmante() && miembroTemplate.getSuplenteId() == null;
    }

    private List<Comentario> getComentariosTemplateDessdeComentarios(List<ReunionComentario> comentarios)
    {
        List<Comentario> listaComentariosTemplate = new ArrayList<>();

        for (ReunionComentario comentario : comentarios)
        {
            listaComentariosTemplate.add(getComentarioTemplateDessdeComentario(comentario));
        }

        return listaComentariosTemplate;
    }

    private Comentario getComentarioTemplateDessdeComentario(ReunionComentario comentario)
    {
        Comentario comentarioTemplate = new Comentario();
        comentarioTemplate.setId(comentario.getId());
        comentarioTemplate.setComentario(comentario.getComentario());
        comentarioTemplate.setFecha(comentario.getFecha());
        comentarioTemplate.setCreadorNombre(comentario.getCreadorNombre());
        comentarioTemplate.setCreadorId(comentario.getCreadorId());

        return comentarioTemplate;
    }

    private List<InvitadoTemplate> getInvitadosTemplateDesdeReunionInvitados(
        List<Persona> invitados,
        Boolean mainLanguage
    )
    {
        List<InvitadoTemplate> listaInvitadosTemplate = new ArrayList<>();

        for (Persona invitado : invitados)
        {
            listaInvitadosTemplate.add(getInvitadoTemplateDessdeInvitado(invitado, mainLanguage));
        }

        return listaInvitadosTemplate;
    }

    private InvitadoTemplate getInvitadoTemplateDessdeInvitado(
        Persona invitado,
        Boolean mainLanguage
    )
    {
        InvitadoTemplate invitadoTemplate = new InvitadoTemplate();

        invitadoTemplate.setId(invitado.getId());
        invitadoTemplate.setNombre(invitado.getNombre());
        invitadoTemplate.setEmail(invitado.getEmail());
        invitadoTemplate
            .setDescripcion(mainLanguage ? invitado.getDescripcion() : invitado.getDescripcionAlternativa());

        return invitadoTemplate;
    }

    private List<OrganoTemplate> getOrganosTemplateDesdeOrganos(
        List<Organo> organos,
        Reunion reunion,
        Boolean withNoAsistentes,
        boolean mainLanguage
    )
    {
        List<OrganoTemplate> listaOrganoTemplate = new ArrayList<>();

        for (Organo organo : organos)
        {
            listaOrganoTemplate.add(getOrganoTemplateDesdeOrgano(organo, reunion, withNoAsistentes, mainLanguage));
        }

        return listaOrganoTemplate;
    }

    private OrganoTemplate getOrganoTemplateDesdeOrgano(
        Organo organo,
        Reunion reunion,
        Boolean withNoAsistentes,
        boolean mainLanguage
    )
    {
        OrganoTemplate organoTemplate = new OrganoTemplate();
        organoTemplate.setId(organo.getId());
        organoTemplate.setNombre(mainLanguage ? organo.getNombre() : organo.getNombreAlternativo());
        organoTemplate.setTipoCodigo(organo.getTipoOrgano().getCodigo());
        organoTemplate.setTipoNombre(
            mainLanguage ? organo.getTipoOrgano().getNombre() : organo.getTipoOrgano().getNombreAlternativo());
        organoTemplate.setTipoOrganoId(organo.getTipoOrgano().getId());

        List<OrganoReunionMiembro> listaAsistentes;
        List<OrganoReunionMiembro> listaMiembros = organoReunionMiembroDAO
            .getMiembroReunionByOrganoAndReunionId(organo.getId(), organo.isExterno(), reunion.getId());

        if (withNoAsistentes)
        {
            listaAsistentes = listaMiembros;
        } else
        {
            listaAsistentes = organoReunionMiembroDAO
                .getAsistenteReunionByOrganoAndReunionId(organo.getId(), organo.isExterno(), reunion.getId());
        }

        organoTemplate
            .setAsistentes(getAsistentesDesdeListaOrganoReunionMiembro(listaAsistentes, listaMiembros, mainLanguage));
        organoTemplate
            .setAusentes(getAusentesDesdeListaOrganoReunionMiembro(listaAsistentes, listaMiembros, mainLanguage));

        return organoTemplate;
    }

    private List<OrganoFirma> getOrganosFirmaDesdeOrganos(
        List<Organo> organos,
        Reunion reunion
    )
    {
        List<OrganoFirma> listaOrganoFirma = new ArrayList<>();

        for (Organo organo : organos)
        {
            listaOrganoFirma.add(getOrganoFirmaDesdeOrgano(organo, reunion));
        }

        return listaOrganoFirma;
    }

    private OrganoFirma getOrganoFirmaDesdeOrgano(
        Organo organo,
        Reunion reunion
    )
    {
        OrganoFirma organoFirma = new OrganoFirma();
        organoFirma.setId(organo.getId());
        organoFirma.setNombre(organo.getNombre());
        organoFirma.setNombreAlternativo(organo.getNombreAlternativo());
        organoFirma.setTipoCodigo(organo.getTipoOrgano().getCodigo());
        organoFirma.setTipoNombre(organo.getTipoOrgano().getNombre());
        organoFirma.setTipoNombreAlternativo(organo.getTipoOrgano().getNombreAlternativo());
        organoFirma.setTipoOrganoId(organo.getTipoOrgano().getId());
        organoFirma.setExterno(organo.isExterno());

        List<OrganoReunionMiembro> listaMiembros = organoReunionMiembroDAO
            .getMiembroReunionByOrganoAndReunionId(organo.getId(), organo.isExterno(), reunion.getId());

        organoFirma.setMiembros(getAsistentesFirmaDesdeListaOrganoReunionMiembro(listaMiembros));

        return organoFirma;
    }

    private List<MiembroFirma> getAsistentesFirmaDesdeListaOrganoReunionMiembro(
        List<OrganoReunionMiembro> listaMiembros
    )
    {
        List<MiembroFirma> listaMiembroFirma = new ArrayList<>();

        for (OrganoReunionMiembro organoReunionMiembro : listaMiembros)
        {
            listaMiembroFirma.add(getAsistenteFirmaDesdeOrganoReunionMiembro(organoReunionMiembro));
        }

        for (MiembroFirma miembroFirma : listaMiembroFirma)
        {
            setDelegacionDeVotoToFirma(miembroFirma, listaMiembros);
        }

        return listaMiembroFirma;
    }

    private void setDelegacionDeVotoToFirma(
        MiembroFirma miembroFirma,
        List<OrganoReunionMiembro> miembros
    )
    {
        for (OrganoReunionMiembro miembro : miembros)
        {
            if (miembro.getDelegadoVotoId() != null && miembroFirma.getId()
                .equals(miembro.getDelegadoVotoId().toString()) && !(miembroFirma.getId()
                .equals(miembro.getMiembroId())))
            {
                miembroFirma.addDelegacionDeVoto(miembro.getNombre());
            }
        }

        miembroFirma.buildNombresDelegacionesDeVoto();
    }

    private MiembroFirma getAsistenteFirmaDesdeOrganoReunionMiembro(OrganoReunionMiembro organoReunionMiembro)
    {
        MiembroFirma miembroFirma = new MiembroFirma();
        miembroFirma.setNombre(organoReunionMiembro.getNombre());
        miembroFirma.setEmail(organoReunionMiembro.getEmail());
        miembroFirma.setId(organoReunionMiembro.getMiembroId());
        miembroFirma.setSuplente(organoReunionMiembro.getSuplenteNombre());
        miembroFirma.setSuplenteId(organoReunionMiembro.getSuplenteId());
        miembroFirma.setDelegadoVotoId(organoReunionMiembro.getDelegadoVotoId());
        miembroFirma.setDelegadoVoto(organoReunionMiembro.getDelegadoVotoNombre());
        miembroFirma.setAsistencia(organoReunionMiembro.getAsistencia());
        miembroFirma.setJustificaAusencia(organoReunionMiembro.getJustificaAusencia());
        miembroFirma.setFirmante(organoReunionMiembro.getFirmante());
        miembroFirma.setOrganoReunionMiembroId(organoReunionMiembro.getId());

        if (organoReunionMiembro.hasCargo())
        {
            Cargo cargo = new Cargo();
            cargo.setId(organoReunionMiembro.getCargoId());
            cargo.setCodigo(organoReunionMiembro.getCargoCodigo());
            cargo.setNombre(organoReunionMiembro.getCargoNombre());
            cargo.setNombreAlternativo(organoReunionMiembro.getCargoNombreAlternativo());
            miembroFirma.setCargo(cargo);
        }
        return miembroFirma;
    }

    private List<MiembroTemplate> getAsistentesDesdeListaOrganoReunionMiembro(
        List<OrganoReunionMiembro> listaAsistentes,
        List<OrganoReunionMiembro> listaMiembros,
        boolean mainLanguage
    )
    {
        List<MiembroTemplate> listaMiembroTemplate = new ArrayList<>();

        for (OrganoReunionMiembro organoReunionMiembro : listaAsistentes)
        {
            listaMiembroTemplate.add(getAsistenteDesdeOrganoReunionMiembro(organoReunionMiembro, mainLanguage));
        }

        for (MiembroTemplate miembroTemplate : listaMiembroTemplate)
        {
            setDelegacionDeVotoToPlantilla(miembroTemplate, listaMiembros);
        }

        return listaMiembroTemplate;
    }

    private List<MiembroTemplate> getAusentesDesdeListaOrganoReunionMiembro(
        List<OrganoReunionMiembro> listaAsistentes,
        List<OrganoReunionMiembro> listaMiembros,
        boolean mainLanguage
    )
    {
        return listaMiembros.stream().filter(a -> !listaAsistentes.contains(a))
            .map(a -> getAsistenteDesdeOrganoReunionMiembro(a, mainLanguage)).collect(Collectors.toList());
    }

    private void setDelegacionDeVotoToPlantilla(
        MiembroTemplate miembroTemplate,
        List<OrganoReunionMiembro> miembros
    )
    {
        for (OrganoReunionMiembro miembro : miembros)
        {
            if (miembro.getDelegadoVotoId() != null && miembroTemplate.getMiembroId()
                .equals(miembro.getDelegadoVotoId().toString()) && !(miembroTemplate.getMiembroId()
                .equals(miembro.getMiembroId())))
            {
                miembroTemplate.addDelegacionDeVoto(miembro.getNombre());
            }
        }
    }

    private MiembroTemplate getAsistenteDesdeOrganoReunionMiembro(
        OrganoReunionMiembro organoReunionMiembro,
        boolean mainLanguage
    )
    {
        MiembroTemplate miembroTemplate = new MiembroTemplate();
        miembroTemplate.setNombre(organoReunionMiembro.getNombre());
        miembroTemplate.setEmail(organoReunionMiembro.getEmail());
        miembroTemplate.setCondicion(
            mainLanguage ? organoReunionMiembro.getCondicion() : organoReunionMiembro.getCondicionAlternativa());
        miembroTemplate.setId(organoReunionMiembro.getId().toString());
        miembroTemplate.setMiembroId(organoReunionMiembro.getMiembroId());
        miembroTemplate.setSuplente(organoReunionMiembro.getSuplenteNombre());
        miembroTemplate.setSuplenteId(organoReunionMiembro.getSuplenteId());
        miembroTemplate.setDelegadoVoto(organoReunionMiembro.getDelegadoVotoNombre());
        miembroTemplate.setDelegadoVotoId(organoReunionMiembro.getDelegadoVotoId());
        miembroTemplate.setAsistencia(organoReunionMiembro.getAsistencia());
        miembroTemplate.setJustificacionAusencia(organoReunionMiembro.getJustificaAusencia());
        miembroTemplate.setDescripcion(
            mainLanguage ? organoReunionMiembro.getDescripcion() : organoReunionMiembro.getDescripcionAlternativa());
        miembroTemplate.setFirmante(organoReunionMiembro.getFirmante());
        miembroTemplate.setGrupo(organoReunionMiembro.getGrupo());

        CargoTemplate cargo = null;

        if (organoReunionMiembro.hasCargo())
        {
            cargo = new CargoTemplate();
            cargo.setId(organoReunionMiembro.getCargoId());
            cargo.setNombre(mainLanguage ?
                organoReunionMiembro.getCargoNombre() :
                organoReunionMiembro.getCargoNombreAlternativo());
            cargo.setCodigo(organoReunionMiembro.getCargoCodigo());
            cargo.setResponsableActa(organoReunionMiembro.isResponsableActa());
        } else
        {
            cargo = new CargoTemplate("", "");
        }
        miembroTemplate.setCargo(cargo);
        return miembroTemplate;
    }

    private List<DocumentoTemplate> getReunionDocumentosTemplateDesdeDocumentos(
        List<ReunionDocumento> reunionDocumentos,
        boolean mainLanguage
    )
    {
        List<DocumentoTemplate> listaDocumento = new ArrayList<>();

        for (ReunionDocumento reunionDocumento : reunionDocumentos)
        {
            listaDocumento.add(getDocumentoTemplateDesdeReunionDocumento(reunionDocumento, mainLanguage));
        }
        return listaDocumento;
    }

    private DocumentoTemplate getDocumentoTemplateDesdeReunionDocumento(
        ReunionDocumento reunionDocumento,
        boolean mainLanguage
    )
    {
        DocumentoTemplate documento = new DocumentoTemplate();
        documento.setId(reunionDocumento.getId());
        documento.setDescripcion(
            mainLanguage ? reunionDocumento.getDescripcion() : reunionDocumento.getDescripcionAlternativa());
        documento.setMimeType(reunionDocumento.getMimeType());
        documento.setFechaAdicion(reunionDocumento.getFechaAdicion());
        documento.setCreadorId(reunionDocumento.getCreadorId());
        documento.setNombreFichero(reunionDocumento.getNombreFichero());
        documento.setPublico(false);

        return documento;
    }

    private List<PuntoOrdenDiaTemplate> getPuntosOrdenDiaTemplateDesdePuntosOrdenDia(
            List<PuntoOrdenDiaMultinivel> puntosOrdenDia, boolean mainLanguage, Boolean isForBuscador)
    {
        List<PuntoOrdenDiaTemplate> listaPuntosOrdenDiaTemplate = new ArrayList<>();

        for (PuntoOrdenDiaMultinivel puntoOrdenDia : puntosOrdenDia)
        {
            listaPuntosOrdenDiaTemplate.add(getPuntoOrdenDiaTemplateDesdePuntoOrdenDiaMultinivel(puntoOrdenDia, mainLanguage, isForBuscador));
        }

        return listaPuntosOrdenDiaTemplate;
    }

    private PuntoOrdenDiaTemplate getPuntoOrdenDiaTemplateDesdePuntoOrdenDiaMultinivel(PuntoOrdenDiaMultinivel puntoOrdenDia,
                                                                                       boolean mainLanguage, Boolean isForBuscador)
    {
        PuntoOrdenDiaTemplate puntoOrdenDiaTemplate = new PuntoOrdenDiaTemplate();
        puntoOrdenDiaTemplate.setId(puntoOrdenDia.getId());
        puntoOrdenDiaTemplate.setOrden(puntoOrdenDia.getOrden());
        puntoOrdenDiaTemplate.setVotoPublico(puntoOrdenDia.getVotoPublico());
        puntoOrdenDiaTemplate
            .setAcuerdos(mainLanguage ? puntoOrdenDia.getAcuerdos() : puntoOrdenDia.getAcuerdosAlternativos());
        puntoOrdenDiaTemplate.setDeliberaciones(
            mainLanguage ? puntoOrdenDia.getDeliberaciones() : puntoOrdenDia.getDeliberacionesAlternativas());
        puntoOrdenDiaTemplate
            .setDescripcion(mainLanguage ? puntoOrdenDia.getDescripcion() : puntoOrdenDia.getDescripcionAlternativa());
        puntoOrdenDiaTemplate
            .setTitulo(mainLanguage ? puntoOrdenDia.getTitulo() : puntoOrdenDia.getTituloAlternativo());
        puntoOrdenDiaTemplate.setPublico(puntoOrdenDia.isPublico());
        puntoOrdenDiaTemplate
            .setUrlActa(mainLanguage ? puntoOrdenDia.getUrlActa() : puntoOrdenDia.getUrlActaAlternativa());
        puntoOrdenDiaTemplate.setUrlActaAnterior(
            mainLanguage ? puntoOrdenDia.getUrlActaAnterior() : puntoOrdenDia.getUrlActaAnteriorAlt());

        if(!isForBuscador) {

            List<PuntoOrdenDiaDocumento> documentos =
                    puntoOrdenDiaDocumentoDAO.getDatosDocumentosByPuntoOrdenDiaId(puntoOrdenDia.getId());
            puntoOrdenDiaTemplate.setDocumentos(
                    getDocumentosTemplateDesdePuntosOrdenDiaDocumentos(documentos, mainLanguage));

            List<PuntoOrdenDiaAcuerdo> acuerdos =
                    puntoOrdenDiaAcuerdoDAO.getDatosAcuerdosByPuntoOrdenDiaId(puntoOrdenDia.getId());
            puntoOrdenDiaTemplate.setDocumentosAcuerdos(
                    getAcuerdosTemplateDesdePuntosOrdenDiaAcuerdos(acuerdos, mainLanguage));

            List<PuntoOrdenDiaDescriptor> descriptores =
                    puntoOrdenDiaDescriptorDAO.getDescriptoresOrdenDia(puntoOrdenDia.getId());
            puntoOrdenDiaTemplate.setDescriptores(
                    getDescriptoresTemplateDesdePuntosOrdenDiaAcuerdos(descriptores, mainLanguage));

            List<PuntoOrdenDiaComentario> comentarios =
                    puntoOrdenDiaComentarioDAO.getComentariosByPuntoId(puntoOrdenDia.getId());
            puntoOrdenDiaTemplate.setComentarios(getComentariosTemplateDesdePUntoOrdenDiaComentarios(comentarios));
        }
        else {
            if(puntoOrdenDia.getPuntoOrdenDiaAcuerdos() != null) {
                List<PuntoOrdenDiaAcuerdo> acuerdos = new ArrayList<>();
                acuerdos.addAll(puntoOrdenDia.getPuntoOrdenDiaAcuerdos());
                puntoOrdenDiaTemplate.setDocumentosAcuerdos(getAcuerdosTemplateDesdePuntosOrdenDiaAcuerdos(acuerdos, mainLanguage));
            }

            if(puntoOrdenDia.getPuntoOrdenDiaDocumentos() != null ) {
                List<PuntoOrdenDiaDocumento> documentos = new ArrayList<>();
                documentos.addAll(puntoOrdenDia.getPuntoOrdenDiaDocumentos());
                puntoOrdenDiaTemplate.setDocumentos(getDocumentosTemplateDesdePuntosOrdenDiaDocumentos(documentos, mainLanguage));
            }
        }

        if (puntoOrdenDia.getPuntosInferiores() != null)
        {
            List<PuntoOrdenDiaTemplate> subpuntosTemplate = new ArrayList<>();

            for (PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel : getsubpuntosOrdenDiaOrdenados(puntoOrdenDia))
            {
                subpuntosTemplate.add(
                        getPuntoOrdenDiaTemplateDesdePuntoOrdenDiaMultinivel(puntoOrdenDiaMultinivel, mainLanguage, isForBuscador));
            }

            puntoOrdenDiaTemplate.setSubpuntos(subpuntosTemplate);
        }

        return puntoOrdenDiaTemplate;
    }

    private List<ComentarioPuntoOrdenDia> getComentariosTemplateDesdePUntoOrdenDiaComentarios(
        List<PuntoOrdenDiaComentario> comentarios
    )
    {
        List<ComentarioPuntoOrdenDia> comentariosTemplate = new ArrayList<>();
        for (PuntoOrdenDiaComentario puntoOrdenDiaComentario : comentarios)
        {
            ComentarioPuntoOrdenDia comentarioPuntoOrdenDia =
                ComentarioPuntoOrdenDia.dtoToModel(puntoOrdenDiaComentario);

            if (puntoOrdenDiaComentario.getRespuestas() != null && !puntoOrdenDiaComentario.getRespuestas().isEmpty())
            {
                comentarioPuntoOrdenDia.setRespuestas(getComentariosTemplateDesdePUntoOrdenDiaComentarios(
                    new ArrayList<>(puntoOrdenDiaComentario.getRespuestas())));
            }
            comentariosTemplate.add(comentarioPuntoOrdenDia);
        }
        return comentariosTemplate;
    }

    private List<DocumentoTemplate> getDocumentosTemplateDesdePuntosOrdenDiaDocumentos(
        List<PuntoOrdenDiaDocumento> documentos,
        boolean mainLanguage
    )
    {
        List<DocumentoTemplate> listaDocumento = new ArrayList<>();

        for (PuntoOrdenDiaDocumento puntoOrdenDiaDocumento : documentos)
        {
            listaDocumento
                .add(getDocumentoTemplateDesdePuntoOrdenDiaDocumentable(puntoOrdenDiaDocumento, mainLanguage));
        }

        return listaDocumento;
    }

    private List<DocumentoTemplate> getAcuerdosTemplateDesdePuntosOrdenDiaAcuerdos(
        List<PuntoOrdenDiaAcuerdo> acuerdos,
        boolean mainLanguage
    )
    {
        List<DocumentoTemplate> listaDocumento = new ArrayList<>();

        for (PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo : acuerdos)
        {
            listaDocumento.add(getDocumentoTemplateDesdePuntoOrdenDiaDocumentable(puntoOrdenDiaAcuerdo, mainLanguage));
        }

        return listaDocumento;
    }

    private List<DescriptorTemplate> getDescriptoresTemplateDesdePuntosOrdenDiaAcuerdos(
        List<PuntoOrdenDiaDescriptor> descriptores,
        boolean mainLanguage
    )
    {
        List<DescriptorTemplate> listaDesciptores = new ArrayList<>();

        for (PuntoOrdenDiaDescriptor descriptor : descriptores)
        {
            listaDesciptores.add(getDescriptorTemplateDesdePuntoOrdenDiaAcuerdo(descriptor, mainLanguage));
        }

        return listaDesciptores;
    }

    private DocumentoTemplate getDocumentoTemplateDesdePuntoOrdenDiaDocumentable(
        Documentable puntoOrdenDiaDocumento,
        boolean mainLanguage
    )
    {
        DocumentoTemplate documento = new DocumentoTemplate();
        documento.setId(puntoOrdenDiaDocumento.getId());
        documento.setDescripcion(mainLanguage ?
            puntoOrdenDiaDocumento.getDescripcion() :
            puntoOrdenDiaDocumento.getDescripcionAlternativa());
        documento.setMimeType(puntoOrdenDiaDocumento.getMimeType());
        documento.setFechaAdicion(puntoOrdenDiaDocumento.getFechaAdicion());
        documento.setCreadorId(puntoOrdenDiaDocumento.getCreadorId());
        documento.setNombreFichero(puntoOrdenDiaDocumento.getNombreFichero());
        documento.setPublico(puntoOrdenDiaDocumento.getPublico());
        documento.setUrlActa(puntoOrdenDiaDocumento.getUrlActa());
        documento.setUrlActaAlternativa(puntoOrdenDiaDocumento.getUrlActaAlternativa());

        return documento;
    }

    private DescriptorTemplate getDescriptorTemplateDesdePuntoOrdenDiaAcuerdo(
        PuntoOrdenDiaDescriptor descriptor,
        boolean mainLanguage
    )
    {
        DescriptorTemplate descriptorTemplate = new DescriptorTemplate();

        descriptorTemplate.setId(descriptor.getId());
        descriptorTemplate.setPuntoOrdenDiaId(descriptor.getPuntoOrdenDia().getId());
        descriptorTemplate.setClaveId(descriptor.getClave().getId());
        descriptorTemplate.setDescriptorId(descriptor.getClave().getDescriptor().getId());
        descriptorTemplate.setDescriptorNombre(mainLanguage ?
            descriptor.getClave().getDescriptor().getDescriptor() :
            descriptor.getClave().getDescriptor().getDescriptorAlternativo());
        descriptorTemplate.setDescriptorDescripcion(mainLanguage ?
            descriptor.getClave().getDescriptor().getDescripcion() :
            descriptor.getClave().getDescriptor().getDescripcionAlternativa());
        descriptorTemplate.setClaveNombre(
            mainLanguage ? descriptor.getClave().getClave() : descriptor.getClave().getClaveAlternativa());

        return descriptorTemplate;
    }

    public Reunion getReunionConOrganosById(
        Long reunionId,
        Long connectedUserId
    )
    {
        return reunionDAO.getReunionConOrganosById(reunionId);
    }

    public List<ReunionPermiso> getReunionesAccesiblesByPersonaId(Long connectedUserId)
    {
        return reunionDAO.getReunionesAccesiblesByPersonaId(connectedUserId);
    }

    public List<ReunionPermiso> getAllReunionesAccesiblesForAdmin()
    {
        return reunionDAO.getAllReunionesAccesiblesForAdmin();
    }

    @Transactional
    public void compruebaReunionNoCompletada(Long reunionId) throws ReunionYaCompletadaException
    {
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (reunion.getCompletada() != null && reunion.getCompletada())
        {
            throw new ReunionYaCompletadaException();
        }
    }

    public void compruebaReunionAdmiteSuplencia(Long reunionId) throws ReunionNoAdmiteSuplenciaException
    {
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (reunion.isAdmiteSuplencia() != null && !reunion.isAdmiteSuplencia())
        {
            throw new ReunionNoAdmiteSuplenciaException();
        }
    }


    public void compruebaReunionAdmiteDelegacionVoto(Long reunionId) throws ReunionNoAdmiteDelegacionVotoException
    {
        Reunion reunion = reunionDAO.getReunionById(reunionId);

        if (reunion.isAdmiteDelegacionVoto() != null && !reunion.isAdmiteDelegacionVoto())
        {
            throw new ReunionNoAdmiteDelegacionVotoException();
        }
    }

    @Transactional
    public String enviarConvocatoria(
        Long reunionId,
        User user,
        String textoConvocatoria
    ) throws Exception
    {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);

        String error = checkAllFeaturesOfReunion(reunion);
        if (error != null)
            return error;

        avisosReunion.enviaAvisoNuevaReunion(reunion, textoConvocatoria);

        if (!reunion.getAvisoPrimeraReunion())
        {
            Persona personaConvocante = personaService.getPersonaFromDirectoryByPersonaId(user.getId());

            reunion.setAvisoPrimeraReunion(true);
            reunion.setAvisoPrimeraReunionUser(user.getName());
            reunion.setAvisoPrimeraReunionFecha(new Date());
            reunion.setAvisoPrimeraReunionUserEmail(personaConvocante.getEmail());
            reunionDAO.update(reunion);
        }

        return null;
    }

    public String checkReunionToClose(Long reunionId, Long connectedUserId) throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        Reunion reunion = reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);

        ReunionTemplate reunionTemplateDesdeReunion = getReunionTemplateDesdeReunion(reunion, connectedUserId, true, true);
        boolean tieneFirmantes = false;
        for (OrganoTemplate organoTemplate : reunionTemplateDesdeReunion.getOrganos())
        {
            for (MiembroTemplate miembroTemplate : organoTemplate.getAsistentes())
            {
                if (isFirmanteValid(miembroTemplate)) {
                    tieneFirmantes = true;
                }
            }
        }

        if (!tieneFirmantes) {
            throw new FirmantesNecesariosException();
        }

        String error = checkAllFeaturesOfReunion(reunion);
        if (error != null)
            return error;

        return null;
    }

    private String checkAllFeaturesOfReunion(Reunion reunion) throws FirmantesNecesariosException, FirmanteSinSuplenteNoAsiste {
        if (reunion.getReunionPuntosOrdenDia().size() == 0)
            return "appI18N.reuniones.reunionSinOrdenDia";

        if (reunion.noContieneMiembros())
            return "appI18N.reuniones.reunionSinMiembros";
        return null;
    }

    public List<TipoOrganoLocal> getTiposOrganosConReunionesPublicas()
    {
        return tipoOrganoDAO.getTiposOrganoConReunionesPublicas();
    }

    public List<Organo> getOrganosConReunionesPublicasByTipoYAnyo(
        Long tipoOrganoId,
        Integer anyo
    )
    {
        QOrganoReunion qOrganoReunion = QOrganoReunion.organoReunion;
        List<Tuple> organosConReunionesPublicas = organoDAO.getOrganosConReunionesPublicas(tipoOrganoId, anyo);
        return organosConReunionesPublicas.stream().map(o ->
        {
            String id = o.get(qOrganoReunion.organoId);
            String nombre = o.get(qOrganoReunion.organoNombre);
            String nombreAlternativo = o.get(qOrganoReunion.organoNombreAlternativo);
            return new Organo(id, nombre, nombreAlternativo, null);
        }).collect(Collectors.toList());
    }

    public List<Descriptor> getDescriptoresConReunionesPublicas(Integer anyo)
    {
        List<Long> idsReuniones = reunionDAO.getIdsReunionesPublicas(anyo);
        List<Long> distinctIdsReuniones = idsReuniones.stream().distinct().collect(Collectors.toList());
        return descriptorDAO.getDescriptoresConReunionesPublicas(distinctIdsReuniones, anyo).stream().distinct().collect(Collectors.toList());
    }

    public List<Integer> getAnyosConReunionesPublicas()
    {
        return reunionDAO.getAnyosConReunionesPublicas();
    }

    public List<Clave> getClavesConReunionesPublicas(
        Long descriptorId,
        Integer anyo
    )
    {
        List<Long> idsReuniones = reunionDAO.getIdsReunionesPublicas(anyo);
        return claveDAO.getClavesConReunionesPublicas(idsReuniones, descriptorId, anyo);
    }

    @Transactional
    public BuscadorReunionesWrapper getReunionesPublicas(AcuerdosSearch acuerdosSearch)
    {
        return reunionDAO.getReunionesPublicasPaginated(acuerdosSearch.getTipoOrganoId(), acuerdosSearch.getOrganoId(),
                        acuerdosSearch.getDescriptorId(), acuerdosSearch.getClaveId(), acuerdosSearch.getAnyo(),
                        acuerdosSearch.getfInicio(), acuerdosSearch.getfFin(), acuerdosSearch.getTexto(),
                        acuerdosSearch.getIdiomaAlternativo(), acuerdosSearch.getStartSearch(),
                        acuerdosSearch.getNumResults());
    }

    public Reunion getReunionByIdAndEditorId(
        Long reunionId,
        Long connectedUserId
    ) throws ReunionNoDisponibleException, RolesPersonaExternaException
    {
        List<String> rolesFromPersonaId = personaService.getRolesFromPersonaId(connectedUserId);
        if (!personaService.isAdmin(rolesFromPersonaId))
        {
            ReunionEditor reunionEditor = reunionDAO.getReunionByIdAndEditorId(reunionId, connectedUserId);

            if (reunionEditor == null)
            {
                throw new ReunionNoDisponibleException();
            }
        }

        return reunionDAO.getReunionById(reunionId);
    }

    public List<OrganoReunion> getOrganosReunionByReunionId(Long reunionId)
    {
        return reunionDAO.getOrganosReunionByReunionId(reunionId);
    }

    public List<ReunionInvitado> getInvitadosReunionByReunionId(Long reunionId)
    {
        return reunionInvitadoDAO.getInvitadosByReunionId(reunionId);
    }

    public String getNombreAsistente(
        Long reunionId,
        Long connectedUserId
    )
    {
        return reunionDAO.getNombreAsistente(reunionId, connectedUserId);
    }

    public Reunion getReunionById(Long reunionId)
    {
        return reunionDAO.getReunionById(reunionId);
    }

    @Transactional
    public ReunionHojaFirmas getHojaFirmasByReunionId(Long reunionId)
    {
        return reunionDAO.getHojaFirmasByReunionId(reunionId);
    }

    @Transactional
    public ReunionHojaFirmas subirHojaFirmasByReunionId(
        Long reunionId,
        ReunionHojaFirmas hojaFirmas
    )
    {
        reunionDAO.deleteHojaFirmasAnterior(reunionId);
        hojaFirmas.setReunion(reunionDAO.getReunionById(reunionId));
        ReunionHojaFirmas hojaFirmasActualizada = reunionDAO.update(hojaFirmas);
        return hojaFirmasActualizada;
    }

    public List<ReunionEditor> getReunionesByAdmin(
        Boolean completada,
        Boolean pasadas,
        Long tipoOrganoId,
        String organoId,
        Boolean externo
    )
    {
        return reunionDAO.getReunionesByAdmin(completada, pasadas, tipoOrganoId, organoId, externo);
    }

    public Reunion getUltimaReunionByReunionIdOrganos(
        Long reunionId,
        List<String> organosReunionIds
    )
    {
        return reunionDAO.getUltimaReunionByReunionIdOrganos(reunionId, organosReunionIds);
    }

    public void addPuntoRevisionUltimoActa(
        Reunion reunionActual,
        Long connectedUserId
    ) throws ReunionNoDisponibleException
    {
        List<OrganoReunion> organosReunionByReunionId = getOrganosReunionByReunionId(reunionActual.getId());
        List<String> idsOrganos =
            organosReunionByReunionId.stream().map(o -> o.getOrganoId()).collect(Collectors.toList());
        Reunion reunionAnterior = getUltimaReunionByReunionIdOrganos(reunionActual.getId(), idsOrganos);

        if (reunionAnterior != null)
        {
            PuntoOrdenDia puntoOrdenDia = creaPuntoOrdenDiaRevisionActa(reunionAnterior, reunionActual);
            puntoOrdenDiaService.addPuntoOrdenDia(puntoOrdenDia);
        }
    }

    private List<String> getIdsOrganosReunion(Long reunionId)
    {
        List<OrganoReunion> organosReunionByReunionId = getOrganosReunionByReunionId(reunionId);
        return organosReunionByReunionId.stream().map(o -> o.getOrganoId()).collect(Collectors.toList());
    }

    private PuntoOrdenDia creaPuntoOrdenDiaRevisionActa(
        Reunion reunionAnterior,
        Reunion reunionActual
    )
    {
        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDia();
        String url = personalizationConfig.appContext + "/rest/actas/" + reunionAnterior.getId();

        if (reunionAnterior.getUrlActa() != null)
        {
            url = reunionAnterior.getUrlActa();
        }

        puntoOrdenDia.setTitulo(getTituloByLanguage(languageConfig.mainLanguage));
        puntoOrdenDia.setUrlActaAnterior(url);
        puntoOrdenDia.setPublico(false);
        puntoOrdenDia.setVotoPublico(false);
        if (!Strings.isNullOrEmpty(languageConfig.alternativeLanguage))
        {
            url = reunionAnterior.getUrlActaAlternativa() != null ? reunionAnterior.getUrlActaAlternativa() : url;
            puntoOrdenDia.setTituloAlternativo(getTituloByLanguage(languageConfig.alternativeLanguage));
            puntoOrdenDia.setUrlActaAnteriorAlt(url);
        }
        puntoOrdenDia.setReunion(reunionActual);
        return puntoOrdenDia;
    }

    private String getTituloByLanguage(String language)
    {
        ResourceBundleUTF8 resourceBundleAlternativeLanguage = new ResourceBundleUTF8("i18n", language);
        return resourceBundleAlternativeLanguage.getString("puntoOrdenDia.revisarActaAnterior");
    }

    public List<GrupoTemplate> getGruposByReunionTemplate(
            ReunionTemplate reunionTemplate,
            String language
    )
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18n", language);
        List<GrupoTemplate> grupos = new ArrayList<>();
        List<OrganoTemplate> organos = reunionTemplate.getOrganos();

        if (organos != null && organos.size() > 0) {
            for (OrganoTemplate organo : organos) {
                List<MiembroTemplate> asistentes = organo.getAsistentes();
                if (asistentes != null && asistentes.size() > 0) {
                    for (MiembroTemplate asistente : asistentes) {
                        boolean encontrado = false;
                        String grupoNombre = asistente.getGrupo() != null ? asistente.getGrupo() : resourceBundle.getString("templates.sinGrupo");
                        for (GrupoTemplate grupo : grupos) {
                            if (grupo.getNombre().equals(grupoNombre) && !encontrado && organo.getId().equals(grupo.getOrganoId())) {
                                grupo.getAsistentes().add(asistente);
                                encontrado = true;
                            }
                        }
                        if (!encontrado) {
                            GrupoTemplate grupoTemplateNoEncontrado = new GrupoTemplate(grupoNombre,organo.getId());
                            grupoTemplateNoEncontrado.getAsistentes().add(asistente);
                            grupos.add(grupoTemplateNoEncontrado);
                        }
                    }
                }
            }
        }
        return grupos;
    }

    public ActaTemplate getActaTemplateFromReunion(
        Reunion reunion,
        Long connectedUserId,
        String lang,
        boolean isForBuscador
    ) throws PersonasExternasException {
        Persona convocante = personaService.getPersonaFromDirectoryByPersonaId(reunion.getCreadorId());
        ReunionTemplate reunionTemplate = isForBuscador ?
            getReunionTemplateDesdeReunionForBuscador(reunion, true, connectedUserId, languageConfig.isMainLangauge(lang)) :
        getReunionTemplateDesdeReunion(reunion, connectedUserId, false, languageConfig.isMainLangauge(lang));

        List<GrupoTemplate> grupos = getGruposByReunionTemplate(reunionTemplate, lang);
        String logo = personalizationConfig.logoDocumentos;

        if (reunionTemplate.getOrganos().size() == 1)
        {
            OrganoTemplate organoTemplate = reunionTemplate.getOrganos().get(0);
            OrganoLogo organoLogo = organoService.getLogo(organoTemplate.getId(), false);
            if (organoLogo != null)
            {
                logo = String.format("data:%s;base64, %s", organoLogo.getExtension(), organoLogo.getData());
            }
        }

        return new ActaTemplate(logo, reunionTemplate, convocante, lang, grupos, isForBuscador);
    }

    public List<Externo> getExternosFromReunionId(Long reunionId) throws PersonasExternasException {
         List<ReunionExternos> externosFromReunionId = reunionDAO.getExternosFromReunionId(reunionId);
         return externosFromReunionId.stream().map(reunionExterno -> new Externo(reunionExterno)).collect(Collectors.toList());
    }

    public void addExternoToReunion(Externo externo, User connectedUser) throws PersonasExternasException {
        if(!externoYaHaSidoInvitado(externo)){
            reunionExternosDAO.insertExterno(externo.toReunionExterno(),connectedUser);
        }
    }

    private boolean externoYaHaSidoInvitado(Externo externo) throws PersonasExternasException {
        return getExternosFromReunionId(externo.getReunionId()).stream().anyMatch(externos -> externos.getEmail().equals(externo.getEmail()));
    }
    @Transactional
    public void addUrlActa(
        Long reunionId,
        String urlActa
    )
    {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        reunion.setUrlActa(urlActa);
        reunionDAO.update(reunion);
    }

    public List<Convocante> getConvocantesReunionByReunionId(Long reunionId, Persona connectedPersona) {
        List<es.uji.apps.goc.dto.Convocante> organosReunionWithEmailByReunionId =  organoDAO.getOrganosReunionWithEmailByReunionId(reunionId);
        List<Convocante> convocantes = organosReunionWithEmailByReunionId.stream().map(organo -> new Convocante(organo.getNombre(),organo.getEmail())).collect(Collectors.toList());
        convocantes.add(new Convocante(connectedPersona.getNombre(), connectedPersona.getEmail()));
        return convocantes;
    }

    @Transactional
    public void guardarConvocante(Long reunionId, String convocante, String convocanteEMail) {
        reunionDAO.updateConvocante(reunionId,convocante, convocanteEMail);
    }

    public Reunion getReunionSiEsAccesible(Long reunionId, Long personaId) throws RolesPersonaExternaException, ReunionNoDisponibleException, InvalidAccessException {
        List<String> roles = personaService.getRolesFromPersonaId(personaId);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);

        if (reunion == null)
            throw new ReunionNoDisponibleException();

        if (personaService.isAdmin(roles) || isAutorizadoEnReunionByReunionConOrganos(reunion, personaId))
            return reunion;

        if (personaService.tieneAcceso(reunionId, personaId, roles) && reunion.isConvocada())
            return reunion;

        throw new InvalidAccessException("No se tiene acceso a esta reunión");
    }

    @Transactional
    public void borrarExterno(Externo externo, User connectedUser) {
        reunionExternosDAO.borrarExterno(externo.toReunionExterno(), connectedUser);
    }

    public boolean isAutorizadoEnReunionByReunionConOrganos(Reunion reunion, Long personaId) {
        return reunionDAO.isAutorizadoEnReunion(reunion, personaId);
    }

    @Transactional
    public void reabrirReunion(Long reunionId, String motivoReapertura, User connectedUser) {
        reunionDAO.updateCompletada(reunionId, false);
        reunionDAO.updateReabierta(reunionId, true);
        reunionDAO.reseteaEstadoEditadoPuntosOrdenDiaByreunionId(reunionId, false);

        ReunionDiligencia reunionDiligencia = new ReunionDiligencia();
        reunionDiligencia.setReunionId(reunionId);
        reunionDiligencia.setFechaReapertura(new Date());
        reunionDiligencia.setMotivoReapertura(motivoReapertura);
        reunionDiligencia.setUserId(connectedUser.getId());
        reunionDiligencia.setMotivoReapertura(motivoReapertura);

        reunionDiligenciaDAO.insert(reunionDiligencia);
    }

    public Reunion getReunionConMiembrosAndPuntosDiaById(Long reunionId){
        return reunionDAO.getReunionConMiembrosAndPuntosDiaById(reunionId);
    }

    @Transactional(rollbackFor = {PersonasExternasException.class})
    public Reunion duplicaReunion(
        Reunion reunion,
        Long reunionOriginalId,
        Long connectedUserId
    ) throws PersonasExternasException, RolesPersonaExternaException
    {
        reunion = addReunion(reunion, connectedUserId);
        duplicaPuntosOrdenDia(reunion, reunionOriginalId, connectedUserId);
        return reunion;
    }

    private void duplicaPuntosOrdenDia(
        Reunion reunion,
        Long reunionOriginalId,
        Long connectedUserId
    ) throws RolesPersonaExternaException
    {
        List<PuntoOrdenDia> puntosReunionOriginal =
            puntoOrdenDiaService.getPuntosByReunionId(reunionOriginalId, connectedUserId);

        duplicaPuntosOrdenDia(reunion, puntosReunionOriginal);
    }

    public void duplicaPuntosOrdenDia(Reunion reunion, List<PuntoOrdenDia> puntosReunionOriginal) {
        if (puntosReunionOriginal != null) {
            puntosReunionOriginal.forEach(puntoOrdenDia ->
            {
                PuntoOrdenDia nuevoPunto = puntoOrdenDiaDAO.detach(puntoOrdenDia);
                nuevoPunto.setReunion(reunion);
                nuevoPunto.cleanBeforeDuplicate();

                if (puntoOrdenDia.getUrlActaAnterior() != null)
                {
                    List<String> idsOrganosReunion = getIdsOrganosReunion(reunion.getId());
                    Reunion ultimaReunionConMismosOrganos = getUltimaReunionByReunionIdOrganos(reunion.getId(), idsOrganosReunion);

                    String urlActaAnterior = (ultimaReunionConMismosOrganos != null) ? ultimaReunionConMismosOrganos.getUrlActa() : null;
                    String urlActaAnteriorAlt = (ultimaReunionConMismosOrganos != null) ? ultimaReunionConMismosOrganos.getUrlActaAlternativa() : null;

                    puntoOrdenDia.setUrlActaAnterior(urlActaAnterior);
                    puntoOrdenDia.setUrlActaAnteriorAlt(urlActaAnteriorAlt);
                }
                puntoOrdenDiaService.addPuntoOrdenDiaSinCambiarOrden(puntoOrdenDia);
            });
        }
    }

    public ReunionAcuerdos getAcuerdosByReunionId(Long reunionId) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        ReunionAcuerdos reunionacuerdos = new ReunionAcuerdos();
        reunionacuerdos.setAcuerdos(reunion.getAcuerdos());
        reunionacuerdos.setResponsableActa(reunion.getMiembroResponsableActa());

        return reunionacuerdos;
    }

    public Boolean isReabierta(Long reunionId) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        return reunion.isReabierta();
    }

    @Transactional
    public void addUrlActaAlternativa(
        Long reunionId,
        String urlActaAlternativa
    ) {
        Reunion reunion = reunionDAO.getReunionById(reunionId);
        reunion.setUrlActaAlternativa(urlActaAlternativa);
        reunionDAO.update(reunion);
    }

    public Long getReunionByPuntoId(Long puntoOrdenDiaId)
    {
        return reunionDAO.getReunionIdByPuntoId(puntoOrdenDiaId);
    }

    public boolean isConnectedUserVotanteInReunion(Long reunionId, Long connectedUserId)
    {
        return reunionDAO.isConnectedUserVotanteInReunion(reunionId, connectedUserId);
    }

    public boolean isConnectedUserSuplenteOrDelegadoVotoInReunion(Long renuionId, Long connectedUserId)
    {
        return reunionDAO.isConnectedUserSuplenteOrDelegadoVotoInReunion(renuionId, connectedUserId);
    }

    public boolean isConnectedUserPresideVotacionInReunion(Long reunionId, Long connectedUserId)
    {
        return reunionDAO.isConnectedUserPresideVotacionInReunion(reunionId, connectedUserId);
    }

    public boolean connectedUserHasSuplenteOrDelegatedVote(Long renuionId, Long connectedUserId)
    {
        return reunionDAO.connectedUserHasSuplenteOrDelegatedVote(renuionId, connectedUserId);
    }

    public boolean isVotacionPublicaByPuntoId(Long puntoOrdenDiaId)
    {
        return reunionDAO.isVotacionPublicaByPuntoId(puntoOrdenDiaId);
    }

    public void compruebaSuplenteNoHaVotadoEnLaReunion(Long reunionId, Long miembroId) throws UsuarioYaHaVotadoException
    {
        if (reunionDAO.hasVotacion(reunionId)) {
            OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(miembroId);
            Long suplenteId = miembro.getSuplenteId();
            votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionId, suplenteId, "app.votos.suplenteVotado");
        }
    }

    public void compruebaDelegadoNoHaVotadoEnLaReunion(Long reunionId, Long miembroId) throws
        UsuarioYaHaVotadoException
    {
        if (reunionDAO.hasVotacion(reunionId)) {
            OrganoReunionMiembro miembro = organoReunionMiembroDAO.getMiembroById(miembroId);
            Long delegadoId = miembro.getDelegadoVotoId();
            votosService.compruebaSiVotanteYaExisteEnLaReunion(reunionId, delegadoId, "app.votos.delegadoVotado");
        }
    }

    public LocalDateTime getFechaAndTimeFormated(UIEntity uiEntity, String fechaAttribute)
    {
        DateTimeFormatter formatterEspanyol = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        DateTimeFormatter formatterIngles = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime dateTime;

        try {
            dateTime = LocalDateTime.parse(uiEntity.get(fechaAttribute), formatterEspanyol);
        } catch (DateTimeParseException e) {
            dateTime = LocalDateTime.parse(uiEntity.get(fechaAttribute), formatterIngles);
        }
        return dateTime;
    }

    public boolean isAutorizadoEnReunionByPuntoOrdenDiaId(Long puntoOrdenDiaId, Long connectedUserId)
    {
        return reunionDAO.isAutorizadoEnReunionByPuntoId(puntoOrdenDiaId, connectedUserId);
    }

    public boolean isCreadorReunion(Long puntoOrdenDiaId,Long connectedUserId)
    {
        return reunionDAO.isCreadorReunion(puntoOrdenDiaId,connectedUserId);
    }

    public Date getReunionFecha(Long reunionId)
    {
        return reunionDAO.getReunionFecha(reunionId);
    }

    public void guardarConvocanteBasedOnBloquearRemitenteConvocatoria(Long reunionId, Convocatoria convocatoria, User connectedUser)
    {
        if(!personalizationConfig.bloquearRemitenteConvocatoria){
            guardarConvocante(reunionId, convocatoria.getConvocante() , convocatoria.getConvocanteEmail());
        } else{
            guardarConvocante(reunionId, connectedUser.getName(), personalizationConfig.defaultSender);
        }
    }

    public boolean isVotanteOrDelegatedOnReunion(Long reunionId, Long connectedUserId)
    {
        return (
            isConnectedUserVotanteInReunion(reunionId, connectedUserId) ||
                isConnectedUserSuplenteOrDelegadoVotoInReunion(reunionId, connectedUserId)
        );
    }
}
