package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.aspect.AccesoResultadosVotosReunion;
import es.uji.apps.goc.aspect.PresideVotacionReunion;
import es.uji.apps.goc.aspect.PuedeVotarReunion;
import es.uji.apps.goc.exceptions.AsistenciaNoConfirmadaException;
import es.uji.apps.goc.exceptions.DelegacionDeVotoNoEncontradaException;
import es.uji.apps.goc.exceptions.PresideVotacionRequiredException;
import es.uji.apps.goc.exceptions.ReunionNoAdmiteVotacionException;
import es.uji.apps.goc.exceptions.ReunionNoConvocadaException;
import es.uji.apps.goc.exceptions.ReunionYaCompletadaException;
import es.uji.apps.goc.exceptions.UsuarioNoTieneDerechoAVotoException;
import es.uji.apps.goc.exceptions.UsuarioYaHaVotadoException;
import es.uji.apps.goc.exceptions.VotacionNoAbiertaException;
import es.uji.apps.goc.model.EstadoVotacion;
import es.uji.apps.goc.model.ResultadoVotos;
import es.uji.apps.goc.model.Votante;
import es.uji.apps.goc.model.VoteType;
import es.uji.apps.goc.model.VotoProvisional;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.apps.goc.services.VotosService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

@Path("/votos/{reunionId}")
public class VotosResource extends CoreBaseService {
    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @InjectParam
    private VotosService votosService;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AccesoResultadosVotosReunion
    public List<ResultadoVotos> getResultadoVotacionTodosPuntosDeReunion(@PathParam("reunionId") Long reunionId) {
        List<ResultadoVotos> resultadoVotos = votosService.getResultadosVotacionEnReunionTodosPuntos(reunionId);
        return resultadoVotos;
    }

    @GET
    @Path("/estado/{personaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AccesoResultadosVotosReunion
    public EstadoVotacion getEstadoVotacionTodosPuntosDeReunion(
        @PathParam("reunionId") Long reunionId,
        @PathParam("personaId") Long personaId
    ) throws UsuarioNoTieneDerechoAVotoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        if (personaId != null && (personaId.equals(connectedUserId) || (!personaId.equals(connectedUserId) && personaId
            .toString().equals(votosService
                .getMiembroPrincipalSubstitutedOrDeleganteVoto(reunionId, personaId.toString(), connectedUserId)
                .getMiembroId())))) {
            return votosService.getEstadoVotacionEnReunionTodosPuntos(reunionId, personaId, connectedUserId);
        }
        else {
            throw new UsuarioNoTieneDerechoAVotoException();
        }
    }

    @GET
    @Path("/puntosOrdenDia/{puntoOrdenDiaId}/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AccesoResultadosVotosReunion
    public List<ResultadoVotos> resultadoVotosByPuntoOrdenDia(
        @PathParam("reunionId") Long reunionId,
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId
    ) {
        ArrayList<ResultadoVotos> listVotos = new ArrayList<>();
        ResultadoVotos resultadoVotos = votosService.getResultadoVotosPuntoOrdenDia(puntoOrdenDiaId);
        listVotos.add(resultadoVotos);
        return listVotos;
    }

    @GET
    @Path("/puntosOrdenDia/{puntoOrdenDiaId}/votosprovisionales")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @AccesoResultadosVotosReunion
    public List<UIEntity> votosProvisionalesPublicosConNombreVotanteByPuntoOrdenDia(
        @PathParam("reunionId") Long reunionId,
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId
    ) {
        List<VotoProvisional> votos = votosService.getVotosProvisionales(puntoOrdenDiaId);
        return UIEntity.toUI(votos);
    }

    @POST
    @Path("/puntosOrdenDia/{puntoOrdenDiaId}/votacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PuedeVotarReunion
    public ResponseMessage votarPuntoOrdenDiaVotacion(
        @PathParam("reunionId") Long reunionId,
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
        UIEntity votoUi
    ) throws VotacionNoAbiertaException, UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException,
        UsuarioNoTieneDerechoAVotoException, DelegacionDeVotoNoEncontradaException, AsistenciaNoConfirmadaException {
        User connectedUser = AccessManager.getConnectedUser(request);

        Votante votante = votosService.getVotante(connectedUser);
        VoteType tipoVoto = votosService.getVoteType(votoUi);
        String votoEnNombre = votoUi.get("votoEnNombre");

        votosService.vote(reunionId, puntoOrdenDiaId, tipoVoto, votoEnNombre, votante);
        return new ResponseMessage(true, "true");
    }

    @PUT
    @Path("/puntosOrdenDia/{puntoOrdenDiaId}/abre")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PresideVotacionReunion
    public ResponseMessage abreVotacion(
        @PathParam("reunionId") Long reunionId,
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId
    ) throws PresideVotacionRequiredException, ReunionNoConvocadaException, ReunionYaCompletadaException {
        User connectedUser = AccessManager.getConnectedUser(request);

        votosService.abreVotacion(reunionId, puntoOrdenDiaId, connectedUser.getId());
        return new ResponseMessage(true, "true");
    }

    @PUT
    @Path("/puntosOrdenDia/{puntoOrdenDiaId}/cierra")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PresideVotacionReunion
    public ResponseMessage cierraVotacion(
        @PathParam("reunionId") Long reunionId,
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId
    ) throws PresideVotacionRequiredException {
        User connectedUser = AccessManager.getConnectedUser(request);

        votosService.cierraVotacion(reunionId, puntoOrdenDiaId, connectedUser.getId());
        return new ResponseMessage(true, "true");
    }
}
