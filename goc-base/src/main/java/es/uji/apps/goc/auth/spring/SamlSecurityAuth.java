package es.uji.apps.goc.auth.spring;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;

import java.io.IOException;

import es.uji.apps.goc.auth.PersonaAuth;
import es.uji.apps.goc.auth.SecurityAuth;
import es.uji.commons.sso.User;

public class SamlSecurityAuth extends PersonaAuth implements SecurityAuth
{
    private static Logger log = Logger.getLogger(SamlSecurityAuth.class);

    @Value("${goc.saml.metadata.username}")
    public String userNameAttribute;

    public User getUser() throws IOException
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null)
        {
            return null;
        }

        SAMLCredential credential = (SAMLCredential) authentication.getCredentials();

        String userName = credential.getAttributeAsString(userNameAttribute);
        User user = null;
        try{
            user = createUserFromPersonaId(userName);
        }
        catch (Exception e)
        {
            log.error("No se ha podido recuperar el id: " + userName + " del usuario conectado");
        }

        return user;
    }
}
