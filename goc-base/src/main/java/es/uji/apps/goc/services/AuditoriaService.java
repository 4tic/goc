package es.uji.apps.goc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.goc.dto.Auditoria;
import es.uji.apps.goc.model.EnumAccionesAuditoria;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.CoreBaseService;

@Service
public class AuditoriaService extends CoreBaseService {
    @Autowired
    BaseDAODatabaseImpl baseDAO;

    public void auditaLogin(String login) {
        baseDAO.insert(new Auditoria(login, null, null, EnumAccionesAuditoria.LOGIN_SUCCESSFUL.toString()));
    }

    public void auditaLoginFallido(String login) {
        baseDAO.insert(new Auditoria(login, null, null, EnumAccionesAuditoria.LOGIN_FAILED.toString()));
    }
}
