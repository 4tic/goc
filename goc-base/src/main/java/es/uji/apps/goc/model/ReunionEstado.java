package es.uji.apps.goc.model;

public class ReunionEstado {
    private Long id;
    private String acuerdos;
    private String acuerdosAlternativos;
    private String observaciones;
    private long responsable;
    private ReunionEstados estado;

    public Long getId() {
        return id;
    }

    public String getAcuerdos() {
        return acuerdos;
    }

    public String getAcuerdosAlternativos() {
        return acuerdosAlternativos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public long getResponsable() {
        return responsable;
    }

    public ReunionEstados getEstado() {
        return estado;
    }
}
