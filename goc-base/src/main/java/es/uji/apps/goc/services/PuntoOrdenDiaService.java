package es.uji.apps.goc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;

import es.uji.apps.goc.Utils;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dao.VotoDAO;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.PuntoOrdenDiaTemplate;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.PuntoDelDiaConAcuerdosException;
import es.uji.apps.goc.exceptions.PuntoOrdenDiaNoDisponibleException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.exceptions.YaSeHaVotadoPuntoException;
import es.uji.apps.goc.model.EstadoPunto;
import es.uji.commons.rest.UIEntity;


@Service
@Component
public class PuntoOrdenDiaService
{
    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ReunionService reunionService;

    @Autowired
    private VotoDAO votoDAO;


    public List<PuntoOrdenDia> getPuntosByReunionId(Long reunionId, Long connectedUserId) throws RolesPersonaExternaException {
        if(connectedUserId != null){
            Reunion reunionById = reunionService.getReunionConOrganosById(reunionId, connectedUserId);
            if(reunionById != null){
                if(personaService.isAdmin(connectedUserId) || reunionService.isAutorizadoEnReunionByReunionConOrganos(reunionById, connectedUserId)){
                    return puntoOrdenDiaDAO.getPuntosByReunionId(reunionId);
                }
            }
        }
        return Collections.emptyList();
    }


    public List<PuntoOrdenDiaMultinivel> getPuntosMultinivelByReunionIdForBucador(Long reunionId)
    {
        List<PuntoOrdenDiaMultinivel> puntosMultinivelByReunionId =
            puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateadoForBuscador(reunionId);
        return puntosMultinivelByReunionId;

    }

    public List<PuntoOrdenDiaMultinivel> getPuntosMultinivelByReunionId(Long reunionId)
    {
        List<PuntoOrdenDiaMultinivel> puntosMultinivelByReunionId =
            puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunionId);
        return puntosMultinivelByReunionId;
    }

    @Transactional(rollbackFor = PuntoDelDiaConAcuerdosException.class)
    public void borrarPuntoOrdenDia(Long reunionId, Long puntoOrdenDiaId, Long connectedUserId)
        throws PuntoDelDiaConAcuerdosException
    {
        puntoOrdenDiaDAO.deleteByPuntoId(puntoOrdenDiaId);
    }

    @Transactional
    public PuntoOrdenDia updatePuntoOrdenDia(Long reunionId, Long puntoOrdenDiaId, String titulo, String tituloAlternativo,
            String descripcion, String descripcionAlternativa, String deliberaciones, String deliberacionesAlternativas,
            String acuerdos, String acuerdosAlternativos, Long orden, Boolean publico, Boolean votoPublico, Boolean editado)
        throws PuntoOrdenDiaNoDisponibleException, YaSeHaVotadoPuntoException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);

        if (puntoOrdenDia == null)
        {
            throw new PuntoOrdenDiaNoDisponibleException();
        }

        if (votoPublico != null && !votoPublico.equals(puntoOrdenDia.getVotoPublico())) {
            if (sePuedeEditarPrivacidadDeVoto(puntoOrdenDiaId, reunionId)) {
                puntoOrdenDia.setVotoPublico(votoPublico);
            }
            else {
                throw new YaSeHaVotadoPuntoException();
            }
        }

        puntoOrdenDia.setTitulo(titulo);
        puntoOrdenDia.setTituloAlternativo(tituloAlternativo);
        puntoOrdenDia.setDescripcion(descripcion);
        puntoOrdenDia.setDescripcionAlternativa(descripcionAlternativa);
        puntoOrdenDia.setDeliberaciones(deliberaciones);
        puntoOrdenDia.setDeliberacionesAlternativas(deliberacionesAlternativas);
        puntoOrdenDia.setAcuerdos(acuerdos);
        puntoOrdenDia.setAcuerdosAlternativos(acuerdosAlternativos);
        puntoOrdenDia.setOrden(orden);
        puntoOrdenDia.setPublico(publico);
        puntoOrdenDia.setEditado(editado);

        return puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    @Transactional
    public PuntoOrdenDia addPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia) {
        return addPuntoOrdenDia(puntoOrdenDia, true);
    }

    @Transactional
    public PuntoOrdenDia addPuntoOrdenDiaSinCambiarOrden(PuntoOrdenDia puntoOrdenDia) {
        return addPuntoOrdenDia(puntoOrdenDia, false);
    }

    private PuntoOrdenDia addPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia, boolean setOrden)
    {
        if (setOrden)
        {
            PuntoOrdenDia ultimo = puntoOrdenDiaDAO.getUltimoPuntoOrdenDiaByReunionId(puntoOrdenDia.getReunion().getId());

            if (ultimo != null)
            {
                puntoOrdenDia.setOrden(ultimo.getOrden() + 10L);
            } else
            {
                puntoOrdenDia.setOrden(10L);
            }
        }

        return puntoOrdenDiaDAO.insert(puntoOrdenDia);
    }

    @Transactional
    public void subePuntoOrdenDia(Long reunionId, Long puntoOrdenDiaId, Long connectedUserId)
    {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        PuntoOrdenDia anteriorPuntoOrdenDia =
                puntoOrdenDiaDAO.getAnteriorPuntoOrdenDiaByOrdenYNivel(reunionId, puntoOrdenDia.getOrden(), puntoOrdenDia.getPuntoSuperior());

        if (anteriorPuntoOrdenDia != null)
        {
            Long orden = puntoOrdenDia.getOrden();
            puntoOrdenDiaDAO.actualizaOrden(puntoOrdenDiaId, anteriorPuntoOrdenDia.getOrden());
            puntoOrdenDiaDAO.flush();
            puntoOrdenDiaDAO.actualizaOrden(anteriorPuntoOrdenDia.getId(), orden);
            puntoOrdenDiaDAO.flush();

            return;
        }
    }

    @Transactional
    public void bajaPuntoOrdenDia(Long reunionId, Long puntoOrdenDiaId, Long connectedUserId)
    {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        PuntoOrdenDia siguientePuntoOrdenDia =
                puntoOrdenDiaDAO.getSiguientePuntoOrdenDiaByOrdenYNivel(reunionId, puntoOrdenDia.getOrden(), puntoOrdenDia.getPuntoSuperior());

        if (siguientePuntoOrdenDia != null)
        {
            Long orden = puntoOrdenDia.getOrden();
            puntoOrdenDiaDAO.actualizaOrden(puntoOrdenDiaId, siguientePuntoOrdenDia.getOrden());
            puntoOrdenDiaDAO.actualizaOrden(siguientePuntoOrdenDia.getId(), orden);
        }
    }

    @Transactional
    public void deleteByReunionId(Long reunionId) throws PuntoDelDiaConAcuerdosException {
        for (PuntoOrdenDia puntoOrdenDia : puntoOrdenDiaDAO.getPuntosByReunionId(reunionId))
        {
            puntoOrdenDiaDAO.deleteByPuntoId(puntoOrdenDia.getId());
        }
    }

    @Transactional
    public void moverPuntos(
        Long reunionId,
        Long puntoOrdenDiaDestinoId,
        Long puntoOrdenDiaAMoverId,
        Long idHermanoAnterior,
        Boolean crearNivel,
        Long connectedUserId
    )
    {
        Long incremento = 10L;
        PuntoOrdenDia puntoOrdenDiaAMover = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaAMoverId);
        PuntoOrdenDia puntoOrdenDiaDestinoPadre = (puntoOrdenDiaDestinoId == null) ? null : puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaDestinoId);
        PuntoOrdenDia puntoOrdenDiaPrevio = (idHermanoAnterior == null) ? null : puntoOrdenDiaDAO.getPuntoOrdenDiaById(idHermanoAnterior);

        if(crearNivel)
        {
            puntoOrdenDiaAMover.setPuntoSuperior(puntoOrdenDiaPrevio);
        }
        else
        {
            puntoOrdenDiaAMover.setPuntoSuperior(puntoOrdenDiaDestinoPadre);
        }
        puntoOrdenDiaAMover.setOrden((puntoOrdenDiaPrevio == null) ? incremento : puntoOrdenDiaPrevio.getOrden() + incremento);
        puntoOrdenDiaDAO.update(puntoOrdenDiaAMover);

        puntoOrdenDiaDAO.incrementaOrdenPuntosSiguientesConMismoPadre(reunionId, puntoOrdenDiaAMoverId, puntoOrdenDiaAMover.getOrden());
    }

    public PuntoOrdenDia updateAcuerdosYDeliberaciones(
        Long puntoOrdenDiaId,
        String deliberaciones,
        String deliberacionesAlternativas,
        String acuerdos,
        String acuerdosAlternativos,
        Long connectedUserId,
        Boolean puntoeditado
    ) throws PuntoOrdenDiaNoDisponibleException {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);

        if (puntoOrdenDia == null)
        {
            throw new PuntoOrdenDiaNoDisponibleException();
        }

        puntoOrdenDia.setDeliberaciones(deliberaciones);
        puntoOrdenDia.setDeliberacionesAlternativas(deliberacionesAlternativas);
        puntoOrdenDia.setAcuerdos(acuerdos);
        puntoOrdenDia.setAcuerdosAlternativos(acuerdosAlternativos);
        puntoOrdenDia.setEditado(puntoeditado);

        return puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    public PuntoOrdenDia getPuntoById(Long puntoOrdenDiaId)
    {
        List<PuntoOrdenDia> puntoOrdenDias = puntoOrdenDiaDAO.get(PuntoOrdenDia.class, puntoOrdenDiaId);
        if(puntoOrdenDias != null && !puntoOrdenDias.isEmpty())
        {
            return puntoOrdenDias.get(0);
        }
        return null;
    }

    public PuntoOrdenDiaTemplate getPuntoOrdenDiaTemplate(List<PuntoOrdenDiaTemplate> puntosOrdenDiaTemplate,
        Long puntoOrdenDiaIdBuscado)
    {
        for (PuntoOrdenDiaTemplate puntoOrdenDiaTemplate : puntosOrdenDiaTemplate)
        {
            if(puntoOrdenDiaTemplate.getId().equals(puntoOrdenDiaIdBuscado))
            {
                return puntoOrdenDiaTemplate;
            }
            else
            {
                if(puntoOrdenDiaTemplate.getSubpuntos() != null)
                {
                    PuntoOrdenDiaTemplate subpuntoOrdenDia =
                        getPuntoOrdenDiaTemplate(puntoOrdenDiaTemplate.getSubpuntos(), puntoOrdenDiaIdBuscado);
                    if (subpuntoOrdenDia != null) {
                        return subpuntoOrdenDia;
                    }
                }
            }
        }

        return null;
    }

    @Transactional
    public void addPuntoOrdenDiaUrlActa(
        Long puntoOrdenDiaId,
        String urlActa
    )
    {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        puntoOrdenDia.setUrlActa(urlActa);
        puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    @Transactional
    public void addPuntoOrdenDiaUrlActaAlternativa(
        Long puntoOrdenDiaId,
        String urlActaAlternativa
    )
    {
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaDAO.getPuntoOrdenDiaById(puntoOrdenDiaId);
        puntoOrdenDia.setUrlActaAlternativa(urlActaAlternativa);
        puntoOrdenDiaDAO.update(puntoOrdenDia);
    }

    public List<PuntoOrdenDiaTemplate> refactorizarHTMLaXHTML(List<PuntoOrdenDiaTemplate> puntosOrdenDiaTemplate) throws TransformerConfigurationException, IOException {
        if (puntosOrdenDiaTemplate != null) {
            InputStream classPathResource = new ClassPathResource("xsl/xhtml-to-xslfo.xsl").getInputStream();
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(
                    new javax.xml.transform.stream.StreamSource(classPathResource));

            for (PuntoOrdenDiaTemplate puntoOrdenDiaTemplate : puntosOrdenDiaTemplate) {
                String acuerdos = puntoOrdenDiaTemplate.getAcuerdos();
                String deliberaciones = puntoOrdenDiaTemplate.getDeliberaciones();
                String descripcion = puntoOrdenDiaTemplate.getDescripcion();

                if(acuerdos != null){
                    puntoOrdenDiaTemplate.setAcuerdos(Utils.transformHtmlToFo(transformer, acuerdos));
                }
                if(deliberaciones != null){
                    puntoOrdenDiaTemplate.setDeliberaciones(Utils.transformHtmlToFo(transformer, deliberaciones));
                }
                if(descripcion != null) {
                     puntoOrdenDiaTemplate.setDescripcion(Utils.transformHtmlToFo(transformer,descripcion));
                }

                if (puntoOrdenDiaTemplate.getSubpuntos() != null && puntoOrdenDiaTemplate.getSubpuntos().size() > 0) {
                    refactorizarHTMLaXHTML(puntoOrdenDiaTemplate.getSubpuntos());
                }
            }
        }
        return puntosOrdenDiaTemplate;
    }

    public List<Long> getPuntosIdsByReunionId(Long reunionId){
        return puntoOrdenDiaDAO.getPuntosIdsbyReunionId(reunionId);
    }

    public List<PuntoOrdenDia> getPuntosOrdenDiaVotacionPublicaReunionId(Long reunionId)
    {
        Boolean votacionPublica = true;
        return puntoOrdenDiaDAO.getPuntosOrdenDiaByReunionIdConVotacion(reunionId, votacionPublica);
    }

    public List<PuntoOrdenDia> getPuntosOrdenDiaVotacionPrivadaReunionId(Long reunionId)
    {
        Boolean votacionPublica = false;
        return puntoOrdenDiaDAO.getPuntosOrdenDiaByReunionIdConVotacion(reunionId, votacionPublica);
    }

    public void setNuevaFechaDeVotacionEnPunto(Long puntoOrdenDiaId, UIEntity puntoOrdenDiaUI)
    {
        PuntoOrdenDia puntoById = getPuntoById(puntoOrdenDiaId);
        if(puntoOrdenDiaUI.get("fechaAperturaVOtacion") != null) {
            LocalDateTime fechaAperturaVotacion = reunionService.getFechaAndTimeFormated(puntoOrdenDiaUI, "fechaAperturaVotacion");
            puntoById.setFechaAperturaVotacion(Date.from(fechaAperturaVotacion.atZone(ZoneId.systemDefault()).toInstant()));
        } else {
            puntoById.setFechaAperturaVotacion(null);
        }
    }

    public void cerrarFechaDeVotacionEnPunto(Long puntoOrdenDiaId)
    {
        PuntoOrdenDia puntoById = getPuntoById(puntoOrdenDiaId);
        puntoById.setFechaAperturaVotacion(null);
        puntoOrdenDiaDAO.update(puntoById);
    }

    public boolean isVotacionVotableYAbierta(Long puntoId)
    {
        EstadoPunto estadoPunto = puntoOrdenDiaDAO.getEstadoVotacionPunto(puntoId);
        if (estadoPunto != null) {
            return estadoPunto != null && estadoPunto.isVotable() && estadoPunto.isAbierta();
        }
        return false;
    }

    public boolean sePuedeEditarPrivacidadDeVoto(Long puntoOrdenDiaId, Long reunionId) throws YaSeHaVotadoPuntoException
    {
        Boolean reunionHasVotacion = reunionDAO.hasVotacion(reunionId);
        if (reunionHasVotacion != null && reunionHasVotacion && votoDAO.puntoHasVotos(puntoOrdenDiaId)) {
            return false;
        }
        return true;
    }
}