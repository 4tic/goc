package es.uji.apps.goc.api.models;

import es.uji.apps.goc.dto.Reunion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "ApiReunion")
public class ApiReunion {

    @ApiModelProperty(value = "Asunto de la reunión", required = true)
    private String asunto;

    @ApiModelProperty(value = "Asunto de la reunión en idioma alternativo")
    private String asuntoAlternativo;

    @ApiModelProperty(value = "Fecha de la reunión", required = true)
    private Date fecha;

    @ApiModelProperty(value = "Duración de la reunión")
    private Long duracion;

    @ApiModelProperty(value = "Descripción de la reunión")
    private String descripcion;

    @ApiModelProperty(value = "Descripción de la reunión en idioma alternativo")
    private String descripcionAlternativa;

    @ApiModelProperty(value = "Numero de sesión de la reunión")
    private Long numeroSesion;

    @ApiModelProperty(value = "Informa si la reunión es de acceso público")
    private Boolean publica;


    public ApiReunion(){
    }

    public ApiReunion(Reunion reunion){
        this.asunto = reunion.getAsunto();
        this.asuntoAlternativo = reunion.getAsuntoAlternativo();
        this.fecha = reunion.getFecha();
        this.descripcion = reunion.getDescripcion();
        this.descripcionAlternativa = reunion.getDescripcionAlternativa();
        this.duracion = reunion.getDuracion();
        this.numeroSesion = reunion.getNumeroSesion();
        this.publica = reunion.isPublica();
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getAsuntoAlternativo() {
        return asuntoAlternativo;
    }

    public void setAsuntoAlternativo(String asuntoAlternativo) {
        this.asuntoAlternativo = asuntoAlternativo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getDuracion() {
        return duracion;
    }

    public void setDuracion(Long duracion) {
        this.duracion = duracion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionAlternativa() {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa) {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public Long getNumeroSesion() {
        return numeroSesion;
    }

    public void setNumeroSesion(Long numeroSesion) {
        this.numeroSesion = numeroSesion;
    }

    public Boolean getPublica() {
        return publica;
    }

    public void setPublica(Boolean publica) {
        this.publica = publica;
    }
}
