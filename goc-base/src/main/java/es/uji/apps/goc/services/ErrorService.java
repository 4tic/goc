package es.uji.apps.goc.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.charset.ResourceBundleUTF8;
import es.uji.apps.goc.templates.HTMLTemplate;
import es.uji.apps.goc.templates.Template;

@Service
public class ErrorService {
    private static Logger log = LoggerFactory.getLogger(ErrorService.class);

    @Autowired
    PersonalizationConfig personalizationConfig;

    @Autowired
    private LanguageConfig languageConfig;

    public Template getTemplateConError(String applang, Exception e)
    {
        return getTemplateConError(applang, e.getClass().getSimpleName());
    }

    public Template getTemplateConError(String applang, String error)
    {
        log.error("ERROR", error);

        Template template = new HTMLTemplate("error-" + applang, personalizationConfig);
        String mensaje = getMensajeError(applang, error);
        template.put("applang", applang);
        template.put("mainLanguage", languageConfig.mainLanguage);
        template.put("alternativeLanguage", languageConfig.alternativeLanguage);
        template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
        template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
        template.put("error", mensaje);
        return template;
    }

    private String getMensajeError(String applang, String error)
    {
        ResourceBundleUTF8 resourceBundle = new ResourceBundleUTF8("i18n", applang);

        try
        {
            return resourceBundle.getString(error);
        }
        catch (Exception e)
        {
            log.warn("No se ha obtenido ningún valor para la key: " + error);
        }
        return resourceBundle.getString("errorGeneral");
    }
}
