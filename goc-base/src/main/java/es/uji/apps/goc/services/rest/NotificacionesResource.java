package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.ReunionNoCompletadaException;
import es.uji.apps.goc.services.PuntoOrdenDiaDocumentoService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionMiembroService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.commons.rest.UIEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Path("notificaciones")
@Api(value = "api/notificaciones", description = "Operaciones para actualizar información, una vez finalizada la reunión")
public class NotificacionesResource
{

    @InjectParam
    ReunionService reunionService;

    @InjectParam
    ReunionMiembroService reunionMiembroService;

    @InjectParam
    PuntoOrdenDiaService puntoOrdenDiaService;

    @InjectParam
    PuntoOrdenDiaDocumentoService puntoOrdenDiaDocumentoService;

    @PUT
    @Path("reunion/{reunionId}/urlActa")
    @ApiOperation(value = "Actualizar la URL donde encontrar el acta firmada de una reunión concreta",
        notes = "En el caso de que la firma de la reunión sea asíncrona, una vez firmada con la aplicación externa, se puede actualizar la URL donde estará el acta firmada en GOC")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlActa(@PathParam("reunionId") Long reunionId, UIEntity uiEntity)
        throws ReunionNoCompletadaException
    {
        String urlActa = uiEntity.get("urlActa");

        validaReunionCerrada(reunionId);
        reunionService.addUrlActa(reunionId, urlActa);

        return Response.ok().build();
    }

    private void validaReunionCerrada(Long reunionId) throws ReunionNoCompletadaException
    {
        Reunion reunionById = reunionService.getReunionById(reunionId);
        if(!reunionById.isCompletada())
            throw new ReunionNoCompletadaException();
    }

    @PUT
    @Path("reunion/{reunionId}/urlActaAlternativa")
    @ApiOperation(value = "Actualizar la URL donde encontrar el acta firmada, en idioma alternativo, de una reunión concreta",
        notes = "En el caso de que hayan 2 idiomas configurados en la aplicación, a través de este servicio se puede actualizar la URL del acta firmada en el idioma alternativo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarUrlActaAlternativa(@PathParam("reunionId") Long reunionId, UIEntity uiEntity)
        throws ReunionNoCompletadaException
    {
        String urlActaAlternativa = uiEntity.get("urlActaAlternativa");

        validaReunionCerrada(reunionId);
        reunionService.addUrlActaAlternativa(reunionId, urlActaAlternativa);

        return Response.ok().build();
    }

    @PUT
    @Path("puntoOrdenDia/{puntoOrdenDiaId}/urlActa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Actualizar la URL donde acceder en el acta, al punto del orden del día concreto de una reunión finalizada",
        notes = "En el caso de que la firma de la reunión sea asíncrona, una vez firmada con la aplicación externa, se puede actualizar la URL donde estará, en el acta, el punto del orden del día concreto")
    public Response notificarUrlActaOrdenDia(
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
        UIEntity uiEntity) throws ReunionNoCompletadaException
    {
        String urlActaAlternativa = uiEntity.get("urlActa");

        PuntoOrdenDia puntoById = puntoOrdenDiaService.getPuntoById(puntoOrdenDiaId);
        validaReunionCerrada(puntoById.getReunion().getId());
        puntoOrdenDiaService.addPuntoOrdenDiaUrlActa(puntoOrdenDiaId, urlActaAlternativa);

        return Response.ok().build();
    }

    @PUT
    @Path("puntoOrdenDia/{puntoOrdenDiaId}/urlActaAlternativa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Actualizar la URL alternativa donde acceder en el acta, al punto del orden del día concreto de una reunión finalizada",
        notes = "En el caso de que hayan 2 idiomas configurados en la aplicación, a través de este servicio se puede actualizar la URL directa al punto del orden del día del acta firmada en el idioma alternativo")
    public Response notificarUrlActaAlternativaOrdenDia(
        @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId,
        UIEntity uiEntity) throws ReunionNoCompletadaException
    {
        String urlActaAlternativa = uiEntity.get("urlActaAlternativa");

        PuntoOrdenDia puntoById = puntoOrdenDiaService.getPuntoById(puntoOrdenDiaId);
        validaReunionCerrada(puntoById.getReunion().getId());
        puntoOrdenDiaService.addPuntoOrdenDiaUrlActaAlternativa(puntoOrdenDiaId, urlActaAlternativa);

        return Response.ok().build();
    }

    @PUT
    @Path("reunion/{miembroId}/urlAsistencia")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Indicar la url al certificado de asistencia firmado para un miembro de la reunión",
        notes = "Admite la url al certificado de asistencia para un miembro concreto")
    public Response notificarUrlAsistencia(@PathParam("miembroId") Long miembroId, UIEntity uiEntity)
        throws ReunionNoCompletadaException
    {
        String urlAsistencia = uiEntity.get("urlAsistencia");

        OrganoReunionMiembro miembro = reunionMiembroService.getMiembroById(miembroId);
        validaReunionCerrada(miembro.getReunionId());
        reunionMiembroService.addUrlAsistencia(miembroId, urlAsistencia);

        return Response.ok().build();
    }

    @PUT
    @Path("reunion/{miembroId}/urlAsistenciaAlternativa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Indicar la url al certificado de asistencia, en idioma alternativo, firmado para un miembro de la reunión",
        notes = "En el caso de tener 2 idiomas configurados en la aplicación, a través de este servicio se puede enviar la url al certificado de asistencia para un miembro concreto, en el idioma alternativo")
    public Response notificarUrlAsistenciaAlternativa(@PathParam("miembroId") Long miembroId, UIEntity uiEntity)
        throws ReunionNoCompletadaException
    {
        String urlAsistenciaAlternativa = uiEntity.get("urlAsistenciaAlternativa");

        OrganoReunionMiembro miembro = reunionMiembroService.getMiembroById(miembroId);
        validaReunionCerrada(miembro.getReunionId());
        reunionMiembroService.addUrlAsistenciaAlternativa(miembroId, urlAsistenciaAlternativa);

        return Response.ok().build();
    }

    @PUT
    @Path("acuerdos/{acuerdoId}/urlActa")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Actualizar la URL donde acceder en el acta, al acuerdo del punto del orden del día concreto de una reunión finalizada",
        notes = "En el caso de que la firma de la reunión sea asíncrona, una vez firmada con la aplicación externa, se puede actualizar la URL donde estará, en el acta, el punto del orden del día concreto")
    public Response notificarUrlActaAcuerdo(
        @PathParam("acuerdoId") Long acuerdoId,
        UIEntity uiEntity) throws ReunionNoCompletadaException
    {
        String urlActa = uiEntity.get("urlActa");

        PuntoOrdenDiaAcuerdo acuerdo = puntoOrdenDiaDocumentoService.getAcuerdoById(acuerdoId);
        PuntoOrdenDia puntoById = acuerdo.getPuntoOrdenDia();
        validaReunionCerrada(puntoById.getReunion().getId());
        puntoOrdenDiaDocumentoService.addAcuerdoUrlActa(acuerdoId, urlActa);

        return Response.ok().build();
    }
}
