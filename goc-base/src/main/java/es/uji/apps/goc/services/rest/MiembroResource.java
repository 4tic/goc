package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.exceptions.MiembroExisteException;
import es.uji.apps.goc.exceptions.MiembroNoDisponibleException;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.exceptions.OrganoNoDisponibleException;
import es.uji.apps.goc.model.Cargo;
import es.uji.apps.goc.model.SafeUIEntity;
import es.uji.apps.goc.model.Grupo;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.services.MiembroService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("miembros")
public class    MiembroResource extends CoreBaseService
{
    @InjectParam
    private MiembroService miembroService;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposOrganos(@QueryParam("organoId") String organoId,
            @QueryParam("externo") Boolean externo) throws MiembrosExternosException
    {
        if(organoId == null)
        {
            return Arrays.asList(new UIEntity());
        }
        else
        {
            Long connectedUserId = AccessManager.getConnectedUserId(request);

            List<Miembro> listaMiembros = getMiembros(organoId, externo, connectedUserId);

            return UIEntity.toUI(listaMiembros);
        }
    }

    private List<Miembro> getMiembros(String organoId, Boolean externo, Long connectedUserId)
            throws MiembrosExternosException
    {
        List<Miembro> listaMiembros;

        if (externo != null && externo)
        {
            listaMiembros = miembroService.getMiembrosExternos(organoId, connectedUserId);
        }
        else
        {
            listaMiembros = miembroService.getMiembrosLocales(Long.parseLong(organoId), connectedUserId);
        }

        return listaMiembros;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addMiembro(UIEntity miembroUI) throws OrganoNoDisponibleException, MiembroExisteException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Miembro miembro = uiToModel(miembroUI);
        miembro = miembroService.addMiembro(miembro, connectedUserId);

        return UIEntity.toUI(miembro);
    }

    @PUT
    @Path("{miembroId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaMiembro(@PathParam("miembroId") Long miembroId, UIEntity miembroUI)
            throws MiembroNoDisponibleException
    {
        Miembro miembro = uiToModel(miembroUI);
        miembro = miembroService.updateMiembro(miembro);

        return UIEntity.toUI(miembro);
    }

    @PUT
    @Path("{miembroId}/subir")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response subeMiembro(@PathParam("miembroId") Long miembroId)
    {
        miembroService.subeMiembro(miembroId);

        return Response.ok().build();
    }

    @PUT
    @Path("{miembroId}/bajar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response bajaMiembro(@PathParam("miembroId") Long miembroId)
    {
        miembroService.bajaMiembro(miembroId);

        return Response.ok().build();
    }

    @DELETE
    @Path("{miembroId}")
    public Response borraMiembro(@PathParam("miembroId") Long miembroId, UIEntity entity)
        throws MiembroNoDisponibleException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        miembroService.removeMiembroById(miembroId, connectedUserId);
        return Response.ok().build();
    }

    private Miembro uiToModel(UIEntity miembroUI)
    {
        Miembro miembro = new Miembro();
        miembro.setId(miembroUI.getLong("id"));
        miembro.setPersonaId(miembroUI.getLong("personaId"));
        miembro.setNombre(miembroUI.get("nombre"));
        miembro.setEmail(miembroUI.get("email"));
        miembro.setFirmante(miembroUI.getBoolean("firmante"));
        miembro.setPresideVotacion(miembroUI.getBoolean("presideVotacion"));
        miembro.setVotante(miembroUI.getBoolean("votante"));
        miembro.setOrden(SafeUIEntity.getSafeLong(miembroUI, "orden"));
        miembro.setGrupo(miembroUI.get("grupo"));
        miembro.setOrdenGrupo(miembroUI.getLong("ordenGrupo"));

        if(personalizationConfig.mostrarDescripcion)
        {
            miembro.setDescripcion(miembroUI.get("descripcion"));
            miembro.setDescripcionAlternativa(miembroUI.get("descripcionAlternativa"));
        }

        if (miembroUI.get("organoId") != null)
        {
            Organo organo = new Organo(miembroUI.get("organoId"));
            miembro.setOrgano(organo);
        }

        if (hasCargo(miembroUI))
        {
            Cargo cargo = new Cargo(miembroUI.get("cargoId"));
            miembro.setCargo(cargo);
        }

        return miembro;
    }

    private boolean hasCargo(UIEntity miembroUI)
    {
        return miembroUI.get("cargoId") != null && !miembroUI.get("cargoId").equals("0");
    }

    @PUT
    @Path("asignargrupo")
    public UIEntity asignarGrupo(UIEntity grupoUI)
    {
        Grupo grupo = new Grupo();
        grupo.setIds(grupoUI.getArray("ids").stream().map(Long::parseLong).collect(Collectors.toList()));
        grupo.setNombre(grupoUI.get("nombre"));
        miembroService.asignarGrupo(grupo);
        return new UIEntity();
    }

    @PUT
    @Path("desagrupar")
    public UIEntity desagrupar(UIEntity listUI)
    {
        miembroService.desagrupar(listUI.getArray("ids").stream().map(Long::parseLong).collect(Collectors.toList()));
        return new UIEntity();
    }

    @PUT
    @Path("subirgrupo/{nombreGrupo}")
    public UIEntity subirGrupo(@PathParam("nombreGrupo") String grupo, @QueryParam("organoId") Long organoId)
    {
        if(organoId != null)
        {
            miembroService.subirGrupo(grupo, organoId);
        }
        return new UIEntity();
    }

    @PUT
    @Path("bajargrupo/{nombreGrupo}")
    public UIEntity bajarGrupo(@PathParam("nombreGrupo") String grupo, @QueryParam("organoId") Long organoId)
    {
        if(organoId != null)
        {
            miembroService.bajarGrupo(grupo, organoId);
        }
        return new UIEntity();
    }

}
