package es.uji.apps.goc.api.services;

import es.uji.apps.goc.api.models.ApiPuntoOrdenDia;
import es.uji.apps.goc.api.models.ApiPuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiPuntoOrdenDiaService {

    public List<ApiPuntoOrdenDia> listPuntoOrdenDiaToListApiPuntoOrdenDia(List<PuntoOrdenDia> puntosOrdenDia){
        return puntosOrdenDia.stream().map(ApiPuntoOrdenDia ::new).collect(Collectors.toList());
    }

    public List<ApiPuntoOrdenDiaDocumento> listPuntoOrdenDiaDocumentoToListApiPuntoOrdenDiaDocumento(List<PuntoOrdenDiaDocumento> documentosByPuntoOrdenDiaId) {
        return documentosByPuntoOrdenDiaId.stream().map(ApiPuntoOrdenDiaDocumento::new).collect(Collectors.toList());
    }
}
