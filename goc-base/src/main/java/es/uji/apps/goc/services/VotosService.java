package es.uji.apps.goc.services;

import com.google.common.base.Strings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import es.uji.apps.goc.dao.OrganoReunionMiembroDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dao.VotoDAO;
import es.uji.apps.goc.dto.OrganoReunionMiembro;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.VotantePrivadoDTO;
import es.uji.apps.goc.dto.VotoPrivadoDTO;
import es.uji.apps.goc.dto.VotoPublicoDTO;
import es.uji.apps.goc.exceptions.AsistenciaNoConfirmadaException;
import es.uji.apps.goc.exceptions.DelegacionDeVotoNoEncontradaException;
import es.uji.apps.goc.exceptions.ExistenVotosEnLaReunionException;
import es.uji.apps.goc.exceptions.PresideVotacionRequiredException;
import es.uji.apps.goc.exceptions.ReunionNoAdmiteVotacionException;
import es.uji.apps.goc.exceptions.ReunionNoConvocadaException;
import es.uji.apps.goc.exceptions.ReunionYaCompletadaException;
import es.uji.apps.goc.exceptions.UsuarioNoTieneDerechoAVotoException;
import es.uji.apps.goc.exceptions.UsuarioYaHaVotadoException;
import es.uji.apps.goc.exceptions.VotacionNoAbiertaException;
import es.uji.apps.goc.model.EstadoVotacion;
import es.uji.apps.goc.model.EstadoVotacionPunto;
import es.uji.apps.goc.model.ResultadoVotos;
import es.uji.apps.goc.model.Votante;
import es.uji.apps.goc.model.VoteType;
import es.uji.apps.goc.model.VotoProvisional;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.User;

@Service
@Component
public class VotosService {

    @Autowired
    private VotoDAO votoDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private OrganoReunionMiembroDAO reunionMiembroDAO;

    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private ReunionService reunionService;

    @Autowired
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @Autowired
    private OrganoReunionMiembroService organoReunionMiembroService;

    @Transactional
    public void vote(
        Long reunionId,
        Long puntoOrdenDiaId,
        VoteType voto,
        String votoEnNombre,
        Votante votante
    ) throws UsuarioYaHaVotadoException, ReunionNoAdmiteVotacionException, UsuarioNoTieneDerechoAVotoException,
        DelegacionDeVotoNoEncontradaException, VotacionNoAbiertaException, AsistenciaNoConfirmadaException {
        if (reunionDAO.hasVotacion(reunionId)) {
            if (puntoOrdenDiaService.isVotacionVotableYAbierta(puntoOrdenDiaId)) {
                boolean isVotacionPublica = reunionService.isVotacionPublicaByPuntoId(puntoOrdenDiaId);
                boolean isVotanteWithoutSuplenteOrDelegado =
                    isVotanteWithoutSuplenteOrDelegado(reunionId, votante.getPersonaId());
                boolean isReceptorVotoDelegadoOrSuplente =
                    isReceptorVotoDelegadoOrSuplente(reunionId, votante.getPersonaId());

                OrganoReunionMiembro miembroEnNombre = new OrganoReunionMiembro();
                if (Strings.isNullOrEmpty(votoEnNombre) || votoEnNombre.equals(votante.getPersonaId().toString())) {
                    if (!isVotanteWithoutSuplenteOrDelegado) {
                        throw new UsuarioNoTieneDerechoAVotoException();
                    }
                } else {
                    if (isReceptorVotoDelegadoOrSuplente) {
                        miembroEnNombre = getMiembroPrincipalSubstitutedOrDeleganteVoto(reunionId, votoEnNombre,
                            votante.getPersonaId());
                        if (miembroEnNombre.getMiembroId() == null) {
                            throw new DelegacionDeVotoNoEncontradaException();
                        }
                    } else {
                        throw new DelegacionDeVotoNoEncontradaException();
                    }
                }

                if (isVotanteWithoutSuplenteOrDelegado || isReceptorVotoDelegadoOrSuplente) {
                    Optional<OrganoReunionMiembro> miembro =
                        reunionMiembroDAO.getMiembroByAsistenteIdOrSuplenteId(reunionId, votante.getPersonaId());
                    if (miembro.isPresent() && miembro.get().isAsistencia() != null && miembro.get().isAsistencia()) {
                        if (isVotacionPublica) {
                            publicVote(puntoOrdenDiaId, voto, votante, miembroEnNombre);
                        } else {
                            privateVote(puntoOrdenDiaId, voto, votante, miembroEnNombre);
                        }
                    }
                    else {
                        throw new AsistenciaNoConfirmadaException();
                    }
                } else {
                    throw new UsuarioNoTieneDerechoAVotoException();
                }
            } else {
                throw new VotacionNoAbiertaException();
            }
        } else {
            throw new ReunionNoAdmiteVotacionException();
        }
    }

    public OrganoReunionMiembro getMiembroPrincipalSubstitutedOrDeleganteVoto(
        Long reunionId,
        String deleganteVotoId,
        Long personaId
    ) {
        List<OrganoReunionMiembro> miembrosAlosQueSupleOesReceptorDeSuVoto = organoReunionMiembroService
            .getOrganoReunionMiembrosPrincipalesFromSuplenteOrDelegadoVotoId(reunionId, personaId);
        return getMiembroWhoHasBeenSubstitutedOrHasDelegatedVote(deleganteVotoId,
            miembrosAlosQueSupleOesReceptorDeSuVoto);
    }

    private OrganoReunionMiembro getMiembroWhoHasBeenSubstitutedOrHasDelegatedVote(
        String deleganteVotoId,
        List<OrganoReunionMiembro> miembrosAlosQueSupleOesReceptorDeSuVoto
    ) {
        for (OrganoReunionMiembro organoReunionMiembro : miembrosAlosQueSupleOesReceptorDeSuVoto) {
            if (organoReunionMiembro.getMiembroId().equals(deleganteVotoId)) {
                return organoReunionMiembro;
            }
        }
        return new OrganoReunionMiembro();
    }

    private synchronized void publicVote(
        Long puntoOrdenDiaId,
        VoteType voto,
        Votante votante,
        OrganoReunionMiembro organoReunionMiembro
    ) throws UsuarioYaHaVotadoException {
        Long connectedUserId = votante.getPersonaId();
        if (!votoDAO.existsVote(puntoOrdenDiaId, organoReunionMiembro.getMiembroId(), connectedUserId)) {
            Long reunionId = reunionService.getReunionByPuntoId(puntoOrdenDiaId);
            if (reunionId != null) {
                VotoPublicoDTO votoPublicoDTO =
                    new VotoPublicoDTO(reunionId, puntoOrdenDiaId, connectedUserId, votante.getName(), voto.toString(),
                        organoReunionMiembro.getNombre(), organoReunionMiembro.getMiembroId());
                votoDAO.insert(votoPublicoDTO);
            }
        } else {
            throw new UsuarioYaHaVotadoException();
        }
    }

    private synchronized void privateVote(
        Long puntoOrdenDiaId,
        VoteType voto,
        Votante votante,
        OrganoReunionMiembro organoReunionMiembro
    ) throws UsuarioYaHaVotadoException {
        Long connectedUserId = votante.getPersonaId();
        if (!votoDAO.existsVote(puntoOrdenDiaId, organoReunionMiembro.getMiembroId(), connectedUserId)) {
            Long reunionId = reunionService.getReunionByPuntoId(puntoOrdenDiaId);
            if (reunionId != null) {
                votoDAO.insert(new VotoPrivadoDTO(reunionId, puntoOrdenDiaId, voto.toString()));
                votoDAO.insert(
                    new VotantePrivadoDTO(reunionId, puntoOrdenDiaId, connectedUserId, organoReunionMiembro.getNombre(),
                        organoReunionMiembro.getMiembroId()));
            }
        } else {
            throw new UsuarioYaHaVotadoException();
        }
    }

    public ResultadoVotos getResultadoVotosPuntoOrdenDia(Long puntoOrdenDiaId) {
        return votoDAO.getResultadoVotosPunto(puntoOrdenDiaId);
    }

    public List<VotoProvisional> getVotosProvisionales(Long puntoOrdenDiaId) {
        return votosDTOToVotosPublicosProvisionales(votoDAO.getVotosProvisionales(puntoOrdenDiaId));
    }

    private List<VotoProvisional> votosDTOToVotosPublicosProvisionales(List<VotoPublicoDTO> votos) {
        ArrayList<VotoProvisional> votosProvisionales = new ArrayList<>();
        for (VotoPublicoDTO voto : votos) {
            VotoProvisional votoProvisional = new VotoProvisional();
            votoProvisional.setNombre(voto.getNombre());
            votoProvisional.setPersonaId(voto.getPersonaId());
            votoProvisional.setPuntoOrdenDiaId(voto.getPuntoOrdenDiaId());
            votoProvisional.setVotoEnNombreDe(voto.getVotoEnNombreDe());
            votoProvisional.setEnNombreDePersonaId(voto.getVotoEnNombreDePersonaId());

            String votoString = voto.getVoto();

            if (votoString.equals(VoteType.CONTRA.name())) {
                votoProvisional.setVote(VoteType.CONTRA);
            } else if (votoString.equals(VoteType.FAVOR.name())) {
                votoProvisional.setVote(VoteType.FAVOR);
            } else {
                votoProvisional.setVote(VoteType.ABSTENCION);
            }
            votosProvisionales.add(votoProvisional);
        }
        return votosProvisionales;
    }

    private List<String> getIdsPuntosWhereUserHasVoted(
        Long reunionId,
        Long personaId,
        Long connectedUserId
    ) {
        List<String> idsPuntossWhereUserHasvoted = new ArrayList<>();

        idsPuntossWhereUserHasvoted
            .addAll(votoDAO.getIdsPuntosWhereUserHasVotedPublic(reunionId, connectedUserId, personaId));
        idsPuntossWhereUserHasvoted
            .addAll(votoDAO.getIdsPuntosWhereUserHasVotedPrivate(reunionId, connectedUserId, personaId));
        return idsPuntossWhereUserHasvoted;
    }

    public boolean isVotanteWithoutSuplenteOrDelegado(
        Long reunionId,
        Long connectedUserId
    ) {
        return reunionService.isConnectedUserVotanteInReunion(reunionId, connectedUserId) && !reunionService
            .connectedUserHasSuplenteOrDelegatedVote(reunionId, connectedUserId);
    }

    public boolean isReceptorVotoDelegadoOrSuplente(
        Long reunionId,
        Long connectedUserId
    ) {
        return reunionService.isConnectedUserSuplenteOrDelegadoVotoInReunion(reunionId, connectedUserId);
    }

    public List<ResultadoVotos> getResultadosVotacionEnReunionTodosPuntos(Long reunionId) {
        return votoDAO.getResultadoVotosTodosPuntosReunion(reunionId);
    }

    public EstadoVotacion getEstadoVotacionEnReunionTodosPuntos(
        Long reunionId,
        Long personaId,
        Long connectedUserId
    ) {
        List<String> idsPuntosWhereUserHasVoted = getIdsPuntosWhereUserHasVoted(reunionId, personaId, connectedUserId);

        EstadoVotacion estadoVotacion = new EstadoVotacion();
        Optional<OrganoReunionMiembro> miembro =
            reunionMiembroDAO.getMiembroByAsistenteIdOrSuplenteId(reunionId, connectedUserId);
        if (miembro.isPresent()) {
            OrganoReunionMiembro organoReunionMiembro = miembro.get();
            estadoVotacion.setVotante(organoReunionMiembro.isVotante());
            estadoVotacion.setPresideVotacion(organoReunionMiembro.isPresideVotacion());
            estadoVotacion.setAsistencia(organoReunionMiembro.isAsistencia() != null && organoReunionMiembro.isAsistencia());
        }
        estadoVotacion.setPuntos(puntoOrdenDiaDAO.getEstadoVotacionReunion(reunionId).stream().map(estadoPunto -> {
            EstadoVotacionPunto estadoVotacionPunto = new EstadoVotacionPunto(estadoPunto);
            estadoVotacionPunto.setVotado(idsPuntosWhereUserHasVoted.contains(estadoPunto.getPuntoId().toString()));

            return estadoVotacionPunto;
        }).collect(Collectors.toList()));

        return estadoVotacion;
    }

    public void compruebaSiVotanteYaExisteEnLaReunion(
        Long reunionId,
        Long personaId
    ) throws UsuarioYaHaVotadoException {
        compruebaSiVotanteYaExisteEnLaReunion(reunionId, personaId, null);
    }

    public void compruebaSiVotanteYaExisteEnLaReunion(
        Long reunionId,
        Long personaId,
        String message
    ) throws UsuarioYaHaVotadoException {
        if (reunionDAO.hasVotacion(reunionId)) {
            boolean existsVoter = votoDAO.existsVoterOnReunion(reunionId, personaId);
            if (existsVoter) {
                throw new UsuarioYaHaVotadoException(message);
            }
        }
    }

    public VoteType getVoteType(UIEntity uiEntity) {
        String votoString = uiEntity.get("voto");
        VoteType voto = null;
        if (votoString.equalsIgnoreCase(VoteType.FAVOR.name())) {
            voto = voto.FAVOR;
        } else if (votoString.equalsIgnoreCase(VoteType.CONTRA.name())) {
            voto = voto.CONTRA;
        } else {
            voto = voto.ABSTENCION;
        }
        return voto;
    }

    public Votante getVotante(User connectedUser) {
        return new Votante(connectedUser);
    }

    public void compruebaReunionNoAdmiteONoTieneVotos(Long reunionId) throws ExistenVotosEnLaReunionException {
        if (reunionDAO.hasVotacion(reunionId) && votoDAO.reunionHasVotos(reunionId)) {
            throw new ExistenVotosEnLaReunionException();
        }
    }

    public void compruebaPuntoNoAdmiteONoTieneVotos(
        Long puntoOrdenDiaId,
        Long reunionId
    ) throws ExistenVotosEnLaReunionException {
        if (reunionDAO.hasVotacion(reunionId) && votoDAO.puntoHasVotos(puntoOrdenDiaId)) {
            throw new ExistenVotosEnLaReunionException();
        }
    }

    @Transactional
    public void abreVotacion(
        Long reunionId,
        Long puntoOrdenDiaId,
        Long connectedUserId
    ) throws PresideVotacionRequiredException, ReunionYaCompletadaException, ReunionNoConvocadaException {
        Reunion reunion = reunionService.getReunionById(reunionId);
        if (reunionMiembroDAO.isPresideVotacion(reunionId, connectedUserId)) {
            if (!reunion.isCompletada()) {
                if (reunion.isConvocada()) {
                    puntoOrdenDiaDAO.setFechaAperturaVotacionPunto(puntoOrdenDiaId, new Date());
                } else {
                    throw new ReunionNoConvocadaException();
                }
            } else {
                throw new ReunionYaCompletadaException();
            }
        } else {
            throw new PresideVotacionRequiredException();
        }
    }

    @Transactional
    public void cierraVotacion(
        Long reunionId,
        Long puntoOrdenDiaId,
        Long connectedUserId
    ) throws PresideVotacionRequiredException {
        if (reunionMiembroDAO.isPresideVotacion(reunionId, connectedUserId)) {
            puntoOrdenDiaDAO.setFechaAperturaVotacionPunto(puntoOrdenDiaId, null);
        } else {
            throw new PresideVotacionRequiredException();
        }
    }
}