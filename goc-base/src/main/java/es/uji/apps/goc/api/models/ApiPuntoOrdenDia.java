package es.uji.apps.goc.api.models;

import es.uji.apps.goc.dto.PuntoOrdenDia;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "PuntoDelOrdenDelDia")
public class ApiPuntoOrdenDia {

    @ApiModelProperty(value = "ID del punto del día", required = true)
    private Long id;

    @ApiModelProperty(value = "Posición del punto del día en la jerarquía", required = true)
    private Long orden;

    @ApiModelProperty(value = "Título del punto del orden del día", required = true)
    private String titulo;

    @ApiModelProperty(value = "Título del punto del orden del día en idioma alternativo")
    private String tituloAlternativo;

    @ApiModelProperty(value = "Descripción del punto del orden del día")
    private String descripcion;

    @ApiModelProperty(value = "Descripción del punto del orden del día en idioma alternativo")
    private String descripcionAlternativa;

    @ApiModelProperty(value = "Acuerdos alcanzados en el punto del orden del día")
    private String acuerdos;

    @ApiModelProperty(value = "Acuerdos alcanzados en el punto del orden del día en idioma alternativo")
    private String acuerdosAlternativos;

    @ApiModelProperty(value = "Deliberaciones alcanzadas en el punto del orden del día")
    private String deliberaciones;

    @ApiModelProperty(value = "Deliberaciones alcanzadas en el punto del orden del día en idioma alternativo")
    private String deliberacionesAlternativa;

    @ApiModelProperty(value = "Dirección en la que se encuentra publicada el acta del punto del orden del día")
    private String urlActa;

    @ApiModelProperty(value = "Dirección en la que se encuentra publicada el acta del punto del orden del día en idioma alternativo")
    private String urlActaAlternativa;

    @ApiModelProperty(value = "Informa si el punto del orden del día es público")
    private Boolean publico;


    public ApiPuntoOrdenDia(){
    }

    public ApiPuntoOrdenDia(PuntoOrdenDia puntoOrdenDia){
        this.id = puntoOrdenDia.getId();
        this.orden = puntoOrdenDia.getOrden();
        this.titulo  = puntoOrdenDia.getTitulo();
        this.tituloAlternativo  = puntoOrdenDia.getTituloAlternativo();
        this.descripcion = puntoOrdenDia.getDescripcion();
        this.descripcionAlternativa = puntoOrdenDia.getDescripcionAlternativa();
        this.acuerdos = puntoOrdenDia.getAcuerdos();
        this.acuerdosAlternativos = puntoOrdenDia.getAcuerdosAlternativos();
        this.deliberaciones = puntoOrdenDia.getDeliberaciones();
        this.deliberacionesAlternativa = puntoOrdenDia.getDeliberacionesAlternativas();
        this.urlActa = puntoOrdenDia.getUrlActa();
        this.urlActaAlternativa = puntoOrdenDia.getUrlActaAlternativa();
        this.publico = puntoOrdenDia.isPublico();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionAlternativa() {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa) {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public String getAcuerdos() {
        return acuerdos;
    }

    public void setAcuerdos(String acuerdos) {
        this.acuerdos = acuerdos;
    }

    public String getAcuerdosAlternativos() {
        return acuerdosAlternativos;
    }

    public void setAcuerdosAlternativos(String acuerdosAlternativos) {
        this.acuerdosAlternativos = acuerdosAlternativos;
    }

    public String getDeliberaciones() {
        return deliberaciones;
    }

    public void setDeliberaciones(String deliberaciones) {
        this.deliberaciones = deliberaciones;
    }

    public String getDeliberacionesAlternativa() {
        return deliberacionesAlternativa;
    }

    public void setDeliberacionesAlternativa(String deliberacionesAlternativa) {
        this.deliberacionesAlternativa = deliberacionesAlternativa;
    }

    public String getUrlActaAlternativa() {
        return urlActaAlternativa;
    }

    public void setUrlActaAlternativa(String urlActaAlternativa) {
        this.urlActaAlternativa = urlActaAlternativa;
    }

    public String getUrlActa() {
        return urlActa;
    }

    public void setUrlActa(String urlActa) {
        this.urlActa = urlActa;
    }

    public Boolean getPublico() {
        return publico;
    }

    public void setPublico(Boolean publico) {
        this.publico = publico;
    }

    public Boolean isPublico(){
        return this.publico;
    }

}
