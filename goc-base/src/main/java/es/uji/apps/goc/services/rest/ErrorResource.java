package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.services.ErrorService;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.CoreBaseService;

@Path("error")
@Service
public class ErrorResource extends CoreBaseService
{
    private static Logger log = LoggerFactory.getLogger(ErrorResource.class);
    @InjectParam
    LanguageConfig languageConfig;

    @InjectParam
    PersonalizationConfig personalizationConfig;

    @InjectParam
    ErrorService errorService;

    @Value("${goc.templates.path:classpath:templates/}")
    private String templatesPath;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template error(@QueryParam("lang") String lang)
    {
        String applang = languageConfig.getLangCode(languageConfig.getLangCode(lang));
        String error = (String) request.getSession().getAttribute("error");

        return errorService.getTemplateConError(applang, error);
    }
}
