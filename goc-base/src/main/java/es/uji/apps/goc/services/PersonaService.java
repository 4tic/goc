package es.uji.apps.goc.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import es.uji.apps.goc.exceptions.AdminRequiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.OrganoAutorizadoDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.PersonaExterna;
import es.uji.apps.goc.exceptions.LimiteConsultaLDAPException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.model.Role;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;

@Service
public class PersonaService extends CoreBaseService {
    @Value("${goc.external.authToken}")
    private String authToken;

    @Value("${goc.external.personasEndpoint}")
    private String personasExternasEndpoint;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    @Autowired
    ReunionDAO reunionDAO;

    @Autowired
    OrganoAutorizadoDAO organoAutorizadoDAO;

    public Persona getPersonaFromDirectoryByPersonaId(Long personaId)
            throws PersonasExternasException
    {
        WebResource getOrganosResource =
                Client.create().resource(this.personasExternasEndpoint + "/" + personaId.toString());

        ClientResponse response = getOrganosResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        if (response.getStatus() != 200)
        {
            throw new PersonasExternasException();
        }

        PersonaExterna personaExterna = response.getEntity(PersonaExterna.class);

        return personaExternaToPersona(personaExterna);
    }

    private Persona personaExternaToPersona(PersonaExterna personaExterna) {
        Persona persona = new Persona();

        persona.setId(personaExterna.getId());
        persona.setNombre(personaExterna.getNombre());
        persona.setEmail(personaExterna.getEmail());

        return persona;
    }

    public List<String> getRolesFromPersonaId(Long personaId)
            throws RolesPersonaExternaException
    {
        WebResource getOrganosResource =
                Client.create().resource(this.personasExternasEndpoint + "/" + personaId.toString() + "/roles");

        ClientResponse response = getOrganosResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        if (response.getStatus() != 200)
        {
            throw new RolesPersonaExternaException();
        }

        List<String> roles = response.getEntity(new GenericType<List<String>>() {
        });

        if (isAutorizado(personaId)) {
            roles.add(Role.GESTOR.toString());
        }

        return roles;
    }

    public List<Persona> getPersonasLocalesByQueryString(String query, Long connectedUserId) throws PersonasExternasException, UnsupportedEncodingException, LimiteConsultaLDAPException {
        query = URLEncoder.encode(query, "UTF-8");
        WebResource getPersonasResource = Client.create().resource(this.personasExternasEndpoint +"/locales"+ "?query=" + query);

        return getPersonas(getPersonasResource);
    }

    private List<Persona> getPersonas(WebResource getPersonasResource) throws LimiteConsultaLDAPException, PersonasExternasException {
        ClientResponse response = getPersonasResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        if (response.getStatus() != 200) {
            ResponseMessage errorResponse = response.getEntity(ResponseMessage.class);
            if (errorResponse.getMessage().equals("LDAP_ERROR")) {
                throw new LimiteConsultaLDAPException();
            } else {
                throw new PersonasExternasException();
            }
        }
        List<Persona> personas = response.getEntity(new GenericType<List<Persona>>() {

        });

        List<PersonaExterna> personaExternaList = personas.stream().map(persona -> new PersonaExterna(persona)).collect(Collectors.toList());
        return creaListaMiembroDesdeListaMiembrosExternos(personaExternaList);
    }

    public List<Persona> getPersonasByQueryString(String query, Long connectedUserId) throws PersonasExternasException, UnsupportedEncodingException, LimiteConsultaLDAPException {
        query = URLEncoder.encode(query, "UTF-8");
        WebResource getPersonasResource = Client.create().resource(this.personasExternasEndpoint + "?query=" + query);

        return getPersonas(getPersonasResource);
    }

    private List<Persona> creaListaMiembroDesdeListaMiembrosExternos(List<PersonaExterna> listaPersonasExternas) {
        List<Persona> listaPersonas = new ArrayList<>();

        for (PersonaExterna personaExterna : listaPersonasExternas) {
            listaPersonas.add(personaExternaToPersona(personaExterna));
        }

        return listaPersonas;
    }

    public Boolean isAdmin(List<String> roles)
            throws RolesPersonaExternaException
    {
        if (roles == null || roles.size() == 0)
            return false;

        return roles.contains(personalizationConfig.rolAdministrador);
    }

    public Boolean isAdmin(Long personaId) throws RolesPersonaExternaException {
        List<String> rolesFromPersonaId = getRolesFromPersonaId(personaId);
        if(rolesFromPersonaId != null){
            return rolesFromPersonaId.contains(personalizationConfig.rolAdministrador);
        }
        return false;
    }

    public void adminRequired(Long personadId) throws RolesPersonaExternaException, AdminRequiredException {
        if(!this.isAdmin(personadId))
        {
        throw new AdminRequiredException();
        }
    }

    public Boolean isGestor(List<String> roles)
            throws RolesPersonaExternaException
    {
        return roles.contains(personalizationConfig.rolGestor) && !roles.contains(personalizationConfig.rolAdministrador);
    }

    public Boolean isUsuario(Long connectedUserId)
            throws RolesPersonaExternaException
    {
        return isUsuario(getRolesFromPersonaId(connectedUserId));
    }

    public Boolean isUsuario(List<String> roles)
            throws RolesPersonaExternaException
    {
        return roles.contains(personalizationConfig.rolUsuario) && !(roles.contains(personalizationConfig.rolGestor) || roles.contains(personalizationConfig.rolAdministrador));
    }

    public Boolean hasPerfil(Long connectedUserId, String rol)
            throws RolesPersonaExternaException
    {
        return getRolesFromPersonaId(connectedUserId).stream()
                .filter(r -> r.equals(rol))
                .findAny()
                .isPresent();
    }

    public boolean tieneAcceso(
            Long reunionId,
            Long connectedUserId
    ) throws RolesPersonaExternaException
    {
        List<String> rolesPersona = getRolesFromPersonaId(connectedUserId);

        return tieneAcceso(reunionId, connectedUserId, rolesPersona);
    }

    public boolean tieneAcceso(Long reunionId, Long connectedUserId, List<String> rolesPersona) throws RolesPersonaExternaException {
        if (isAdmin(rolesPersona))
            return true;
        else
            return reunionDAO.tieneAcceso(reunionId, connectedUserId);
    }

    public List<Persona> getTodasPersonas(Long connectedUserId) throws PersonasExternasException, RolesPersonaExternaException {
        if (isAdmin(getRolesFromPersonaId(connectedUserId))) {
            WebResource getPersonasResource =
                    Client.create().resource(this.personasExternasEndpoint);

            ClientResponse response = getPersonasResource.type(MediaType.APPLICATION_JSON)
                    .header("X-UJI-AuthToken", authToken)
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new PersonasExternasException();
            }

            List<Persona> entity = response.getEntity(new GenericType<List<Persona>>() {
            });

            return entity;
        }

        return Collections.emptyList();
    }

    public void deshabilitaPersona(Long personaId, Long connectedUserId) throws PersonasExternasException, RolesPersonaExternaException {
        if (!isAdmin(getRolesFromPersonaId(connectedUserId)))
            throw new PersonasExternasException();

        WebResource getPersonasResource =
                Client.create().resource(this.personasExternasEndpoint + "/" + personaId + "/deshabilita");

        ClientResponse response = getPersonasResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken)
                .put(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new PersonasExternasException();
        }
    }

    public void insertPersona(Persona persona) throws PersonasExternasException {
        WebResource getPersonasResource =
                Client.create().resource(this.personasExternasEndpoint);

        ClientResponse response = getPersonasResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken).entity(persona)
                .post(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new PersonasExternasException();
        }
    }

    public void modificaPersona(Long personaId, Long connectedUserId, Persona persona) throws PersonasExternasException, RolesPersonaExternaException {
        if (!isAdmin(getRolesFromPersonaId(connectedUserId)))
            throw new PersonasExternasException();
        WebResource getPersonasResource =
                Client.create().resource(this.personasExternasEndpoint + "/" + personaId);

        ClientResponse response = getPersonasResource.type(MediaType.APPLICATION_JSON)
                .header("X-UJI-AuthToken", authToken).entity(persona)
                .put(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new PersonasExternasException();
        }
    }

    private boolean isAutorizado(Long personaId) {
        return organoAutorizadoDAO.isAutorizado(personaId);
    }

    public Boolean isDeveloper(Long personaId) throws RolesPersonaExternaException
    {
        List<String> rolesFromPersonaId = getRolesFromPersonaId(personaId);
        if(rolesFromPersonaId != null){
            return rolesFromPersonaId.contains(personalizationConfig.rolAdministrador);
        }
        return false;
    }
}
