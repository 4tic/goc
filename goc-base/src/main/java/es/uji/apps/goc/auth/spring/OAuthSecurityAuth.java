package es.uji.apps.goc.auth.spring;

import com.github.leleuj.ss.oauth.client.authentication.OAuthAuthenticationToken;
import es.uji.apps.goc.auth.PersonaAuth;
import es.uji.apps.goc.auth.SecurityAuth;
import es.uji.commons.sso.User;
import org.apache.log4j.Logger;
import org.scribe.up.profile.UserProfile;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;

public class OAuthSecurityAuth extends PersonaAuth implements SecurityAuth
{
    private static Logger log = Logger.getLogger(OAuthSecurityAuth.class);

    public User getUser() throws IOException
    {
        OAuthAuthenticationToken authentication = (OAuthAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null)
        {
            return null;
        }

        UserProfile userProfile = authentication.getUserProfile();

        String userName = (String) userProfile.getAttributes().get("email");
        User user = null;
        try{
            user = createUserFromPersonaId(userName);
        }
        catch (Exception e)
        {
            log.error("No se ha podido recuperar el id: " + userName + " del usuario conectado");
        }

        return user;
    }
}
