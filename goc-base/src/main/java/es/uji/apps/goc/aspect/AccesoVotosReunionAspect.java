package es.uji.apps.goc.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import es.uji.apps.goc.exceptions.UsuarioNoPresideVotacionException;
import es.uji.apps.goc.exceptions.UsuarioNoTieneDerechoAVotoException;
import es.uji.apps.goc.services.ReunionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;

@Aspect
@Configurable
public class AccesoVotosReunionAspect extends BaseAspect {
    @Autowired
    private ReunionService reunionService;

    @Before("@annotation(PuedeVotarReunion)")
    public void puedeVotarReunion(JoinPoint joinPoint) throws Throwable {
        Long reunionId = getReunionIdFromArgs(joinPoint);
        Long connectedUserId = AccessManager.getConnectedUserId(((CoreBaseService) joinPoint.getTarget()).getRequest());
        boolean isVotanteOrDelegatedOnReunion = reunionService.isVotanteOrDelegatedOnReunion(reunionId, connectedUserId);
        if (!isVotanteOrDelegatedOnReunion) {
            throw new UsuarioNoTieneDerechoAVotoException();
        }
    }

    @Before("@annotation(PresideVotacionReunion)")
    public void presideVotacionReunion(JoinPoint joinPoint) throws Throwable {
        Long reunionId = getReunionIdFromArgs(joinPoint);
        Long connectedUserId = AccessManager.getConnectedUserId(((CoreBaseService) joinPoint.getTarget()).getRequest());
        boolean isPresideVotacionOnReunion = reunionService.isConnectedUserPresideVotacionInReunion(reunionId, connectedUserId);
        if (!isPresideVotacionOnReunion) {
            throw new UsuarioNoPresideVotacionException();
        }
    }

    @Before("@annotation(AccesoResultadosVotosReunion)")
    public void hasAccesoReunion(JoinPoint joinPoint) throws Throwable {
        Long reunionId = getReunionIdFromArgs(joinPoint);
        Long connectedUserId = AccessManager.getConnectedUserId(((CoreBaseService) joinPoint.getTarget()).getRequest());
        boolean isMemberOrDelegatedOnReunion = reunionService.isVotanteOrDelegatedOnReunion(reunionId, connectedUserId);
        boolean isPresideVotacionOnReunion = reunionService.isConnectedUserPresideVotacionInReunion(reunionId, connectedUserId);
        if (!isMemberOrDelegatedOnReunion && !isPresideVotacionOnReunion) {
            reunionService.getReunionSiEsAccesible(reunionId, connectedUserId);
        }
    }
}
