package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.OrganoAutorizado;
import es.uji.apps.goc.dto.OrganoInvitado;
import es.uji.apps.goc.dto.OrganoLogo;
import es.uji.apps.goc.exceptions.AdminRequiredException;
import es.uji.apps.goc.exceptions.DocumentoNoEncontradoException;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.exceptions.OrganoNoDisponibleException;
import es.uji.apps.goc.exceptions.OrganosExternosException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.DocumentoUI;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.OrganoCargo;
import es.uji.apps.goc.model.TipoOrgano;
import es.uji.apps.goc.services.OrganoService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("organos")
public class OrganoResource extends CoreBaseService
{
    @InjectParam
    OrganoService organoService;

    @InjectParam
    PersonaService personaService;

    @InjectParam
    CSVExporter csvExporter;

    @InjectParam
    PersonalizationConfig personalizationConfig;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganos(@QueryParam("reunionId") Long reunionId)
        throws OrganosExternosException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Organo> listaOrganos;

        if (reunionId != null)
        {
            listaOrganos = organoService.getOrganosByReunionId(reunionId, connectedUserId);
        } else
        {
            listaOrganos = organoService.getOrganosByAdminAndAutorizadoId(connectedUserId, personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador));
        }

        return organosToUI(listaOrganos);
    }

    @GET
    @Path("convocables")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganosByAutorizado(@QueryParam("reunionId") Long reunionId)
        throws OrganosExternosException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Organo> listaOrganos;

        if (reunionId != null)
        {
            listaOrganos = organoService.getOrganosByReunionId(reunionId, connectedUserId);
        } else
        {
            listaOrganos =
                organoService.getOrganosPorAutorizadoId(connectedUserId,
                    personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador));
        }

        return organosToUI(listaOrganos);

    }

    @GET
    @Path("activos/usuario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOrganosActivosByUserId() throws OrganosExternosException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Organo> listaOrganos;

        if (personaService.isUsuario(connectedUserId))
        {
            listaOrganos =
                organoService.getOrganosPorAutorizadoId(connectedUserId,
                    personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador));
        }
        else
        {
            listaOrganos = organoService.getOrganosByAdminAndAutorizadoId(connectedUserId, personaService.hasPerfil(connectedUserId, personalizationConfig.rolAdministrador));
        }

        listaOrganos = listaOrganos.stream().filter(o -> o.isExterno() || !o.isInactivo()).collect(Collectors.toList());

        return organosToUI(listaOrganos);
    }

    private List<UIEntity> organosToUI(List<Organo> listaOrganos)
    {
        List<UIEntity> listaUI = new ArrayList<>();

        for (Organo organo : listaOrganos)
        {
            listaUI.add(organoToUI(organo));
        }

        return listaUI;
    }

    private UIEntity organoToUI(Organo organo)
    {
        UIEntity ui = new UIEntity();
        ui.put("id", organo.getId());
        ui.put("nombre", organo.getNombre());
        ui.put("nombreAlternativo", organo.getNombreAlternativo());
        ui.put("externo", organo.isExterno());
        ui.put("inactivo", organo.isInactivo());
        ui.put("tipoOrganoId", organo.getTipoOrgano().getId());
        ui.put("email", organo.getEmail());
        ui.put("ordenado", organo.getOrdenado());
        ui.put("hasLogo", organo.getHasLogo());

        return ui;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addOrgano(UIEntity organoUI) throws AdminRequiredException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaService.adminRequired(connectedUserId);
        Organo organo = uiToModel(organoUI);
        organo = organoService.addOrgano(organo, connectedUserId);

        return UIEntity.toUI(organo);
    }

    @PUT
    @Path("{organoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity modificaOrgano(
        @PathParam("organoId") Long organoId,
        UIEntity organoUI
    ) throws OrganoNoDisponibleException, RolesPersonaExternaException, AdminRequiredException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaService.adminRequired(connectedUserId);

        String nombre = organoUI.get("nombre");
        String nombreAlternativo = organoUI.get("nombreAlternativo");
        Long tipoOrganoId = Long.parseLong(organoUI.get("tipoOrganoId"));
        Boolean inactivo = new Boolean(organoUI.get("inactivo"));
        String email = organoUI.get("email");

        Organo organo =
            organoService.updateOrgano(organoId, nombre, nombreAlternativo, tipoOrganoId, inactivo, connectedUserId, email);

        return UIEntity.toUI(organo);
    }

    private Organo uiToModel(UIEntity organoUI)
    {
        Organo organo = new Organo();

        if (ParamUtils.parseLong(organoUI.get("id")) != null)
        {
            organo.setId(organoUI.get("id"));
        }

        organo.setNombre(organoUI.get("nombre"));
        organo.setNombreAlternativo(organoUI.get("nombreAlternativo"));
        organo.setInactivo(false);
        organo.setEmail(organoUI.get("email"));

        TipoOrgano tipoOrgano = new TipoOrgano();
        tipoOrgano.setId(Long.parseLong(organoUI.get("tipoOrganoId")));
        organo.setTipoOrgano(tipoOrgano);

        return organo;
    }

    @PUT
    @Path("{organoId}/bloquearordenacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response bloqueoOrdenacionOrgano(@PathParam("organoId") Long organoId)
            throws OrganoNoDisponibleException{
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Organo organo = organoService.getOrganoById(organoId, connectedUserId);
        organoService.setOrganoOrdenaTrueByOrgano(organo);

        return Response.ok().build();
    }

    @PUT
    @Path("{organoId}/desbloquearordenacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response desbloqueoOrdenacionOrgano(@PathParam("organoId") Long organoId)
            throws OrganoNoDisponibleException{
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Organo organo = organoService.getOrganoById(organoId, connectedUserId);
        organoService.setOrganoOrdenaFalseByOrgano(organo);

        return Response.ok().build();
    }

    @GET
    @Path("autorizados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAutorizados(
        @QueryParam("organoId") String organoId,
        @QueryParam("externo") Boolean externo
    ) throws RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if(personaService.isAdmin(connectedUserId)){
            return UIEntity.toUI(organoService.getAutorizados(organoId, externo));
        }
        else {
            return UIEntity.toUI(Collections.emptyList());
        }
    }

    @POST
    @Path("autorizados")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addAutorizado(UIEntity autorizado) throws AdminRequiredException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaService.adminRequired(connectedUserId);
        OrganoAutorizado newOrganoAutorizado = organoService.addAutorizado(autorizado.toModel(OrganoAutorizado.class));
        return UIEntity.toUI(newOrganoAutorizado);
    }

    @DELETE
    @Path("autorizados/{organoAutorizadoId}")
    public Response borraAutorizado(@PathParam("organoAutorizadoId") Long organoAutorizadoId) throws AdminRequiredException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaService.adminRequired(connectedUserId);

        organoService.removeAutorizado(organoAutorizadoId);

        return Response.ok().build();
    }

    @PUT
    @Path("autorizados/{organoAutorizadoId}")
    public Response updateAutorizado(@PathParam("organoAutorizadoId") Long organoAutorizadoId, OrganoAutorizado organo) throws AdminRequiredException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaService.adminRequired(connectedUserId);

        organoService.updateAutorizado(organo);
        return Response.ok().build();
    }

    @GET
    @Path("invitados")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInvitados(@QueryParam("organoId") String organoId)
    {
        return UIEntity.toUI(organoService.getInvitados(organoId));
    }

    @POST
    @Path("invitados")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addInvitado(UIEntity invitado)
    {
        OrganoInvitado newOrganoInvitado = organoService.addInvitado(invitado.toModel(OrganoInvitado.class));

        return UIEntity.toUI(newOrganoInvitado);
    }

    @PUT
    @Path("invitados/{organoInvitadoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateInvitado(
        UIEntity invitado,
        @PathParam("organoInvitadoId") Long organoInvitadoId
    )
    {
        organoService.updateInvitado(invitado.toModel(OrganoInvitado.class));

        return UIEntity.toUI(invitado);
    }

    @PUT
    @Path("invitados/{organoInvitadoId}/subir")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response subirInvitado(@PathParam("organoInvitadoId") Long organoInvitadoId)
    {
        organoService.subeInvitado(organoInvitadoId);

        return Response.ok().build();
    }

    @PUT
    @Path("invitados/{organoInvitadoId}/bajar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response bajarInvitado(@PathParam("organoInvitadoId") Long organoInvitadoId)
    {
        organoService.bajaInvitado(organoInvitadoId);

        return Response.ok().build();
    }

    @DELETE
    @Path("invitados/{organoInvitadoId}")
    public Response borraInvitado(@PathParam("organoInvitadoId") Long organoInvitadoId)
    {
        organoService.removeInvitado(organoInvitadoId);

        return Response.ok().build();
    }

    @GET
    @Path("cargos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCargos(@QueryParam("organoId") String organoId)
    {
        return UIEntity.toUI(organoService.getCargosByOrgano(organoId));
    }

    @GET
    @Path("cargos/disponibles")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCargosDisponibles(@QueryParam("organoId") String organoId)
    {
        return UIEntity.toUI(organoService.getCargosDisponiblesByOrgano(organoId));
    }

    @POST
    @Path("cargos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addCargo(UIEntity cargo)
    {
        OrganoCargo newOrganoCargo = organoService.addCargo(cargo.toModel(OrganoCargo.class));

        return UIEntity.toUI(newOrganoCargo);
    }

    @DELETE
    @Path("cargos/{organoCargoId}")
    public Response borraCargo(@PathParam("organoCargoId") Long organoCargoId)
    {
        organoService.removeCargo(organoCargoId);

        return Response.ok().build();
    }

    @GET
    @Path("{organoId}/export")
    public Response exportarMiembrosByOrganoId(@PathParam("organoId") String organoId)
            throws MiembrosExternosException, IOException, DocumentoNoEncontradoException, AdminRequiredException, RolesPersonaExternaException
    {
        Long connectedUser = AccessManager.getConnectedUserId(request);
        personaService.adminRequired(connectedUser);
        DocumentoUI documentoUI = csvExporter.exportaMiembros(organoId, connectedUser);
        return Response.ok(documentoUI.getData())
            .header("Content-Disposition", "attachment; filename = \"" + organoId + ".csv" + "\"")
            .header("Content-Length", documentoUI.getData().length).header("Content-Type", documentoUI.getMimeType())
            .build();
    }

    @POST
    @Path("{organoId}/logo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarLogo(
        @PathParam("organoId") String organoId,
        @QueryParam("externo") boolean externo,
        FormDataMultiPart multiPart
    )
    {
        InputStream data = null;

        BodyPart bodyPart = multiPart.getBodyParts().get(0);
        String mime = bodyPart.getHeaders().getFirst("Content-Type");
        if (mime != null && !mime.isEmpty())
        {
            BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
            data = bpe.getInputStream();
        }

        if (data != null)
        {
            organoService.actualizarImagen(organoId, externo, data, mime);
        }
        return Response.ok(new UIEntity()).build();
    }

    @GET
    @Path("{organoId}/logo")
    public Response getLogo(
        @PathParam("organoId") String organoId,
        @QueryParam("externo") boolean externo
    )
    {
        OrganoLogo organoLogo = organoService.getLogo(organoId, externo);

        byte[] data;
        String extension;
        String contentType;
        if(organoLogo == null) {
            extension = getExtensionFromBase64(personalizationConfig.logoDocumentos);
            contentType = getContentTypeFromBase64(personalizationConfig.logoDocumentos);
            data = getDataFromBase64(personalizationConfig.logoDocumentos);
        } else {
            data = Base64.getDecoder().decode(organoLogo.getData());
            extension = getExtensionFromMimeType(organoLogo.getExtension());
            contentType = organoLogo.getExtension();
        }
        return Response.ok(data)
            .header("Content-Disposition", "attachment; filename = \"logo" + extension + "\"")
            .header("Content-Length", data.length).header("Content-Type", contentType)
            .build();
    }

    public static String getExtensionFromBase64(String base64) {
        String contentType = getContentTypeFromBase64(base64);
        return contentType.substring(contentType.indexOf("/")).replace("/", ".");
    }

    public static String getContentTypeFromBase64(String base64) {
        String[] partes = base64.split(" ");
        String cabeceras = partes[0];
        cabeceras = cabeceras.substring(cabeceras.indexOf(":"));
        return cabeceras.substring(1, cabeceras.indexOf(";"));
    }

    public static byte[] getDataFromBase64(String base64) {
        String[] partes = base64.split(" ");
        return Base64.getDecoder().decode(partes[1]);
    }

    public static String getExtensionFromMimeType(String mimetype) {
        return mimetype.substring(mimetype.indexOf("/")).replace("/", ".");
    }
}
