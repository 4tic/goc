package es.uji.apps.goc.api.models;

import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "PuntoOrdenDiaDocumento")
public class ApiPuntoOrdenDiaDocumento {
    @ApiModelProperty(value = "ID del documento", required = true)
    private Long id;

    @ApiModelProperty(value = "Descripción del documento", required = true)
    private String descripcion;

    @ApiModelProperty(value = "Descripción del documento en idioma alternativo")
    private String descripcionAlternativa;

    @ApiModelProperty(value = "Tipo del documento", required = true)
    private String mimeType;

    @ApiModelProperty(value = "Nombre del fichero del documento", required = true)
    private String nombreFichero;

    @ApiModelProperty(value = "Fecha en que el documento fue añadido", required = true)
    private Date fechaAdicion;

    @ApiModelProperty(value = "Informa si el documento es de acceso público")
    private Boolean publico;

    @ApiModelProperty(value = "Array de bytes que contiene los datos del documento", required = true)
    private byte[] datos;

    public ApiPuntoOrdenDiaDocumento(PuntoOrdenDiaDocumento puntoOrdenDiaDocumento) {
        this.id = puntoOrdenDiaDocumento.getId();
        this.descripcion = puntoOrdenDiaDocumento.getDescripcion();
        this.descripcionAlternativa = puntoOrdenDiaDocumento.getDescripcionAlternativa();
        this.mimeType = puntoOrdenDiaDocumento.getMimeType();
        this.nombreFichero = puntoOrdenDiaDocumento.getNombreFichero();
        this.fechaAdicion = puntoOrdenDiaDocumento.getFechaAdicion();
        this.publico = puntoOrdenDiaDocumento.getPublico();
        this.datos = puntoOrdenDiaDocumento.getDatos();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionAlternativa() {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa) {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public Date getFechaAdicion() {
        return fechaAdicion;
    }

    public void setFechaAdicion(Date fechaAdicion) {
        this.fechaAdicion = fechaAdicion;
    }

    public Boolean getPublico() {
        return publico;
    }

    public void setPublico(Boolean publico) {
        this.publico = publico;
    }

    public byte[] getDatos() {
        return datos;
    }

    public void setDatos(byte[] datos) {
        this.datos = datos;
    }
}
