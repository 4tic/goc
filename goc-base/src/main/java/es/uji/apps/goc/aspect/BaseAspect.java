package es.uji.apps.goc.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

public class BaseAspect {
    private Object getParameterFromArgs(
        JoinPoint joinPoint,
        String parameterName
    ) {
        MethodSignature signature = (MethodSignature) joinPoint.getStaticPart().getSignature();
        for (int i = 0; i < signature.getParameterNames().length; i++) {
            if (signature.getParameterNames()[i].equalsIgnoreCase(parameterName))
                return joinPoint.getArgs()[i];
        }
        return null;
    }

    protected Long getReunionIdFromArgs(JoinPoint joinPoint) {
        return (Long) getParameterFromArgs(joinPoint, "reunionId");
    }
}
