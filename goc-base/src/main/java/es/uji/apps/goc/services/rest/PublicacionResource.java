package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.transform.TransformerConfigurationException;

import es.uji.apps.goc.DateUtils;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.OrganoAutorizadoDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.Clave;
import es.uji.apps.goc.dto.Descriptor;
import es.uji.apps.goc.dto.GrupoTemplate;
import es.uji.apps.goc.dto.OrganoTemplate;
import es.uji.apps.goc.dto.PuntoOrdenDiaTemplate;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionPermiso;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.dto.TipoOrganoLocal;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.exceptions.InvalidAccessException;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.exceptions.OrganosExternosException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.PuntoDelDiaNoDisponibleException;
import es.uji.apps.goc.exceptions.PuntoDelDiaNoTieneAcuerdosException;
import es.uji.apps.goc.exceptions.ReunionNoCompletadaException;
import es.uji.apps.goc.exceptions.ReunionNoDisponibleException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.ActaTemplate;
import es.uji.apps.goc.model.AcuerdosSearch;
import es.uji.apps.goc.model.BuscadorReunionesWrapper;
import es.uji.apps.goc.model.Organo;
import es.uji.apps.goc.model.Votante;
import es.uji.apps.goc.services.ErrorService;
import es.uji.apps.goc.services.MiembroService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionDocumentoService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.apps.goc.templates.HTMLTemplate;
import es.uji.apps.goc.templates.PDFTemplate;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

@Service
@Path("publicacion")
public class PublicacionResource extends CoreBaseService
{
    private static Logger log = LoggerFactory.getLogger(PublicacionResource.class);

    public static final int RESULTADOS_POR_PAGINA = 10;

    @InjectParam
    private ReunionDAO reunionDAO;

    @InjectParam
    private OrganoAutorizadoDAO organoAutorizadoDAO;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private ReunionDocumentoService reunionDocumentoService;

    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @InjectParam
    private MiembroService miembroService;

    @InjectParam
    private ErrorService errorService;

    @Value("${goc.templates.path:classpath:templates/}")
    private String templatesPath;

    @GET
    @Path("reuniones")
    @Produces(MediaType.TEXT_HTML)
    public Template reuniones(@QueryParam("lang") String lang)
        throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
        PersonasExternasException, RolesPersonaExternaException
    {
        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("reuniones-" + applang, personalizationConfig);
        try
        {
            Long connectedUserId = AccessManager.getConnectedUserId(request);
            List<ReunionPermiso> reuniones = new ArrayList<>();

            if (personaService.isAdmin(personaService.getRolesFromPersonaId(connectedUserId)))
                reuniones = reunionService.getAllReunionesAccesiblesForAdmin();
            else
                reuniones = reunionService.getReunionesAccesiblesByPersonaId(connectedUserId);


            template.put("reuniones", setLanguageFields(reuniones, lang));
            template.put("applang", applang);
            template.put("mainLanguage", languageConfig.mainLanguage);
            template.put("alternativeLanguage", languageConfig.alternativeLanguage);
            template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
            template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
            template.put("connectedUserId", connectedUserId);
            template.put("mostrarEnlaceCertificados", personalizationConfig.mostrarEnlaceCertificados);
        } catch (Exception e)
        {
            log.error("Error en publicación de plantilla reuniones-", e);
            template = errorService.getTemplateConError(applang, e);
        }
        return template;
    }

    public List<ReunionPermiso> setLanguageFields(List<ReunionPermiso> reuniones, String lang)
    {
        for (ReunionPermiso reunionPermiso : reuniones)
        {
            reunionPermiso.setAsunto(languageConfig.isMainLangauge(
                    lang) ? reunionPermiso.getAsunto() : reunionPermiso.getAsuntoAlternativo());

            reunionPermiso.setUrlActa(languageConfig.isMainLangauge(lang) ? reunionPermiso.getUrlActa() : reunionPermiso
                    .getUrlActaAlternativa());

            reunionPermiso.setUrlAsistencia(languageConfig.isMainLangauge(lang) ? reunionPermiso.getUrlAsistencia() : reunionPermiso
                    .getUrlAsistenciaAlternativa());
        }

        return reuniones;
    }

    @GET
    @Path("acuerdos")
    @Produces(MediaType.TEXT_HTML)
    public Template acuerdos(@QueryParam("lang") String lang, @QueryParam("tipoOrganoId") Long tipoOrganoId,
            @QueryParam("organoId") Long organoId, @QueryParam("descriptorId") Long descriptorId,
            @QueryParam("claveId") Long claveId, @QueryParam("anyo") Integer anyo,
            @QueryParam("fInicio") String fInicio, @QueryParam("fFin") String fFin, @QueryParam("texto") String texto,
            @QueryParam("pagina") @DefaultValue("0") Integer pagina) throws FirmanteSinSuplenteNoAsiste
    {
        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("acuerdos-" + applang, personalizationConfig);
        try
        {
            Long connectedUserId = AccessManager.getConnectedUserId(request);

            List<Integer> anyos = reunionService.getAnyosConReunionesPublicas();
            List<TipoOrganoLocal> tiposOrganos = reunionService.getTiposOrganosConReunionesPublicas();
            List<Organo> organos = reunionService.getOrganosConReunionesPublicasByTipoYAnyo(null, null);
            List<Descriptor> descriptoresConReunionesPublicas = null;
            List<Clave> claves = null;
            List<ReunionTemplate> reunionesTemplate = null;

            descriptoresConReunionesPublicas = reunionService.getDescriptoresConReunionesPublicas(anyo);

            if (tipoOrganoId != null)
            {
                organos = reunionService.getOrganosConReunionesPublicasByTipoYAnyo(tipoOrganoId, anyo);
            }

            if (!descriptoresConReunionesPublicas.isEmpty() && descriptorId != null)
            {
                claves = reunionService.getClavesConReunionesPublicas(descriptorId, anyo);
            }

            AcuerdosSearch acuerdosSearch =
                new AcuerdosSearch(anyo, pagina * RESULTADOS_POR_PAGINA, RESULTADOS_POR_PAGINA);

            if (!descriptoresConReunionesPublicas.isEmpty())
            {
                acuerdosSearch.setClaveId(claveId);
                acuerdosSearch.setDescriptorId(descriptorId);
            }

            acuerdosSearch.setfInicio(getDate(fInicio));
            acuerdosSearch.setfFin(getDate(fFin));
            acuerdosSearch.setTexto(texto);
            acuerdosSearch.setTipoOrganoId(tipoOrganoId);
            acuerdosSearch.setOrganoId(organoId);

            acuerdosSearch.setIdiomaAlternativo(!languageConfig.isMainLangauge(lang));

        BuscadorReunionesWrapper reunionesPublicasPaginadasWrapper = reunionService.getReunionesPublicas(acuerdosSearch);
        Long numReuniones = reunionesPublicasPaginadasWrapper.getNumeroReuniones();

        reunionesTemplate = buildReunionTemplateBuscador(connectedUserId, reunionesPublicasPaginadasWrapper.getReuniones(), lang, true);

            template.put("applang", applang);
            template.put("lang", lang);
            template.put("mainLanguage", languageConfig.mainLanguage);
            template.put("alternativeLanguage", languageConfig.alternativeLanguage);
            template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
            template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
            template.put("connectedUserId", connectedUserId);
            template.put("tiposOrganos", tiposOrganos);
            template.put("tipoOrganoId", tipoOrganoId);
            template.put("organoId", organoId);
            template.put("organos", organos);
            template.put("reuniones", reunionesTemplate);
            template.put("descriptores", descriptoresConReunionesPublicas);
            template.put("descriptorId", descriptorId);
            template.put("claves", claves);
            template.put("claveId", claveId);
            template.put("anyos", anyos);
            template.put("anyo", anyo);
            template.put("fInicio", fInicio);
            template.put("fFin", fFin);
            template.put("texto", texto);
            template.put("actasPublicas", personalizationConfig.actasPublicas);

            if (pagina > 0)
            {
                template.put("hasPrevPage", true);
            }

            if (numReuniones > ((pagina * RESULTADOS_POR_PAGINA) + RESULTADOS_POR_PAGINA))
            {
                template.put("hasNextPage", true);
            }

            template.put("pagina", pagina);
        }
        catch (Exception e)
        {
            log.error("Error en publicación de plantilla acuerdos-", e);
            template = errorService.getTemplateConError(applang, e);
        }

        return template;
    }


    @GET
    @Path("acuerdos/actas/{reunionId}")
    @Produces("application/pdf")
    @Transactional
    public Template generaPDFActaPublicaCompletada(
        @PathParam("reunionId") Long reunionId,
        @QueryParam("lang") String lang,
        @Context HttpServletRequest request
    ) throws ReunionNoDisponibleException,
        PersonasExternasException, InvalidAccessException, RolesPersonaExternaException,
        FirmanteSinSuplenteNoAsiste, FirmantesNecesariosException, IOException, TransformerConfigurationException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);
        String applang = languageConfig.getLangCode(lang);

        if (reunion == null || reunion.getCompletada() == null || !reunion.getCompletada()) {
            throw new ReunionNoDisponibleException();
        }

        if (!personalizationConfig.actasPublicas) {
            throw new InvalidAccessException("No se tiene acceso a este documento");
        }

        ActaTemplate actaTemplate = reunionService.getActaTemplateFromReunion(reunion, connectedUserId, applang, true);
        return actaTemplate.toTemplate(personalizationConfig, personalizationConfig.showAusentes);
    }

    public Date getDate(String value)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            return formatter.parse(value);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private List<ReunionTemplate> buildReunionTemplateBuscador(Long connectedUserId, List<Reunion> reuniones, String lang, boolean isForBuscadorAcuerdos)
    {
        boolean isMainLangauge = languageConfig.isMainLangauge(lang);
        List<ReunionTemplate> reunionesTemplate;
        if (isForBuscadorAcuerdos) {
            reunionesTemplate = reuniones.stream()
                    .map(r -> reunionService.getReunionTemplateDesdeReunionForBuscador(r, false,
                        connectedUserId, isMainLangauge))
                    .collect(Collectors.toList());

        } else {
            reunionesTemplate = reuniones.stream()
                    .map(r -> reunionService.getReunionTemplateDesdeReunion(r, connectedUserId, false,
                            isMainLangauge))
                    .collect(Collectors.toList());
        }
        return reunionesTemplate;
    }

    @GET
    @Path("reuniones/{reunionId}")
    @Produces(MediaType.TEXT_HTML)
    @Transactional
    public Template reunion(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang)
        throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
        PersonasExternasException, InvalidAccessException, RolesPersonaExternaException
    {
        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("reunion-" + applang, personalizationConfig);
        try
        {
            Long connectedUserId = AccessManager.getConnectedUserId(request);
            Reunion reunion = reunionService.getReunionSiEsAccesible(reunionId, connectedUserId);

        boolean permitirComentarios = reunion.isPermitirComentarios(connectedUserId, organoAutorizadoDAO.getAutorizadosByReunionId(reunionId));
        boolean permitirSubirDocumentos = reunion.isPermitirSubirDocumentos(connectedUserId, organoAutorizadoDAO.getAutorizadosByReunionId(reunionId));

            ReunionTemplate reunionTemplate = reunionService
                .getReunionTemplateDesdeReunion(reunion, connectedUserId, true, languageConfig.isMainLangauge(lang));

            LocalDate fecha = reunion.getFecha().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            String diaSemanaTexto = fecha.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag(applang));

            List<GrupoTemplate> grupos = reunionService.getGruposByReunionTemplate(reunionTemplate, applang);

            Boolean reunionConVotacion = reunion.getHasVotacion() != null && reunion.getHasVotacion();
            boolean votante = reunionConVotacion &&
                reunionService.isVotanteOrDelegatedOnReunion(reunionId, connectedUserId);
            boolean presideVotacion = reunionConVotacion &&
                reunionService.isConnectedUserPresideVotacionInReunion(reunionId, connectedUserId);
            if(personalizationConfig.mostrarConvocante && reunion.getConvocante() == null){
                reunionTemplate.setConvocante("");
                reunionTemplate.setConvocanteEmail("");
            }

            template.put("reunion", reunionTemplate);
            template.put("applang", applang);
            template.put("mainLanguage", languageConfig.mainLanguage);
            template.put("alternativeLanguage", languageConfig.alternativeLanguage);
            template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
            template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
            template.put("connectedUserId", connectedUserId);
            template.put("permitirComentarios", permitirComentarios);
            template.put("permitirSubirDocumentos", permitirSubirDocumentos);
            template.put("diaSemanaTexto", diaSemanaTexto);
            template.put("hayAlgunDocumento", reunionDocumentoService.hayAlgunaDocumentacion(reunionId));
            template.put("mostrarEmails", personalizationConfig.mostrarEmails);
            template.put("grupos", grupos);
            template.put("mostrarConvocante", personalizationConfig.mostrarConvocante);
            template.put("votante", votante);
            template.put("presideVotacion", presideVotacion);
        }
        catch (Exception e)
        {
            log.error("Error en publicación de plantilla reunion-", e);
            template = errorService.getTemplateConError(applang, e);
        }
        return template;
    }

    @GET
    @Path("reuniones/{reunionId}/acuerdos")
    @Produces(MediaType.TEXT_HTML)
    public Template reunionAcuerdos(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang)
        throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
        PersonasExternasException, InvalidAccessException, ReunionNoCompletadaException, RolesPersonaExternaException
    {
        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("reunion-acuerdos-" + applang, personalizationConfig);
        try
        {
            Long connectedUserId = AccessManager.getConnectedUserId(request);
            Reunion reunion = reunionService.getReunionSiEsAccesible(reunionId, connectedUserId);

            ReunionTemplate reunionTemplate = reunionService
                .getReunionTemplateDesdeReunion(reunion, connectedUserId, true, languageConfig.isMainLangauge(lang));

            template.put("reunion", reunionTemplate);
            template.put("applang", applang);
            template.put("mainLanguage", languageConfig.mainLanguage);
            template.put("alternativeLanguage", languageConfig.alternativeLanguage);
            template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
            template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
            template.put("connectedUserId", connectedUserId);
            template.put("mostrarEnlaceCertificados", personalizationConfig.mostrarEnlaceCertificados);

        }
        catch (Exception e){
            log.error("Error en publicación de plantilla reunion-acuerdos-", e);
            template = errorService.getTemplateConError(applang, e);
        }

        return template;
    }

    @GET
    @Path("reuniones/{reunionId}/acuerdos/{puntoOrdenDiaId}")
    @Produces("application/pdf")
    public Template reunionAcuerdoCertificado(@Context HttpServletResponse httpServletResponse, @PathParam("reunionId") Long reunionId,
            @PathParam("puntoOrdenDiaId") Long puntoOrdenDiaId, @QueryParam("lang") String lang)
        throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
        PersonasExternasException, InvalidAccessException, ReunionNoCompletadaException,
        PuntoDelDiaNoDisponibleException, PuntoDelDiaNoTieneAcuerdosException, RolesPersonaExternaException
    {
        String applang = languageConfig.getLangCode(lang);
        Template template = new PDFTemplate("reunion-acuerdo-" + applang, personalizationConfig);

        try
        {
            Long connectedUserId = AccessManager.getConnectedUserId(request);
            Reunion reunion = reunionService.getReunionSiEsAccesible(reunionId, connectedUserId);

            ReunionTemplate reunionTemplate = reunionService
                .getReunionTemplateDesdeReunion(reunion, connectedUserId, true, languageConfig.isMainLangauge(lang));

        PuntoOrdenDiaTemplate puntoOrdenDiaTemplate =
            puntoOrdenDiaService.getPuntoOrdenDiaTemplate(reunionTemplate.getPuntosOrdenDia(), puntoOrdenDiaId);
            List<PuntoOrdenDiaTemplate> puntoOrdenDiaTemplates = puntoOrdenDiaService.refactorizarHTMLaXHTML(Arrays.asList(puntoOrdenDiaTemplate));
            puntoOrdenDiaTemplate = puntoOrdenDiaTemplates.get(0);

            if (puntoOrdenDiaTemplate == null)
            {
                throw new PuntoDelDiaNoDisponibleException();
            }

            if (puntoOrdenDiaTemplate.getAcuerdos() == null || puntoOrdenDiaTemplate.getAcuerdos().isEmpty())
            {
                throw new PuntoDelDiaNoTieneAcuerdosException();
            }

            template.put("puntoOrdenDia", puntoOrdenDiaTemplate);
            template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
            template.put("fechaReunion", getFechaReunion(reunionTemplate.getFecha()));
            template.put("fechaReunionFormateada", getFechaReunionFormateada(reunionTemplate.getFecha(), lang));
            template.put("numeroSesion", reunionTemplate.getNumeroSesion());
            template.put("tituloReunion", reunionTemplate.getAsunto());
            template.put("organos", getNombreOrganos(reunionTemplate));
            template.put("numeroOrganos", reunionTemplate.getOrganos().size());
            template.put("ciudadInstitucion", personalizationConfig.ciudadInstitucion);
            template.put("extraordinaria", reunionTemplate.isExtraordinaria());
            template.put("firmantes", reunionTemplate.getFirmantes());
        }
        catch (Exception e)
        {
            log.error("Error en publicación reunion-acuerdo-", e);
            redirectToErrorPage(httpServletResponse, e);
        }
        return template;
    }

    private void redirectToErrorPage(HttpServletResponse httpServletResponse, Exception e)
    {
        try
        {
            request.getSession().setAttribute("error", e.getClass().getSimpleName());
            httpServletResponse.sendRedirect(personalizationConfig.appContext + "/rest/error");
        } catch (IOException e1)
        {
            log.error("Error al redirigir a la página de error: ", e1);
        }
    }

    private String getFechaReunion(Date fecha)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        return sdf.format(fecha);
    }

    private String getFechaReunionFormateada(Date fecha, String lang)
    {
        if ("ca".equalsIgnoreCase(lang)) return DateUtils.formattedDateInCatalan(fecha);
        if ("es".equalsIgnoreCase(lang)) return DateUtils.formattedDateInSpanish(fecha);

        return getFechaReunion(fecha);
    }

    private String getNombreOrganos(ReunionTemplate reunionTemplate)
    {
        List<String> nombreOrganos = new ArrayList<>();

        for (OrganoTemplate organo : reunionTemplate.getOrganos())
        {
            nombreOrganos.add(organo.getNombre());
        }

        return StringUtils.join(nombreOrganos, ", ");
    }

    @GET
    @Path("reuniones/{reunionId}/votacion")
    @Produces(MediaType.TEXT_HTML)
    @Transactional
    public Template votarReunion(@PathParam("reunionId") Long reunionId, @QueryParam("lang") String lang)
            throws OrganosExternosException, MiembrosExternosException, ReunionNoDisponibleException,
            PersonasExternasException, InvalidAccessException, RolesPersonaExternaException
    {
        String applang = languageConfig.getLangCode(lang);
        Template template = new HTMLTemplate("reunion-votacion-" + applang, personalizationConfig);
        try
        {
            User connectedUser = AccessManager.getConnectedUser(request);
            Long connectedUserId = connectedUser.getId();

            Reunion reunion = reunionService.getReunionSiEsAccesible(reunionId, connectedUserId);

            ReunionTemplate reunionTemplate = reunionService
                    .getReunionTemplateDesdeReunion(reunion, connectedUserId, true, languageConfig.isMainLangauge(lang));

            LocalDate fecha = reunion.getFecha().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            String diaSemanaTexto = fecha.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.forLanguageTag(applang));

            List<Votante> personasSustituyeODelegan = miembroService.getDelegandosYsuplenciasDeMiembro(connectedUserId, reunionId);

            personasSustituyeODelegan = addConnectedUserToTheVontantesListIfRequired(connectedUser, personasSustituyeODelegan);

            template.put("reunion", reunionTemplate);
            template.put("applang", applang);
            template.put("mainLanguage", languageConfig.mainLanguage);
            template.put("alternativeLanguage", languageConfig.alternativeLanguage);
            template.put("mainLanguageDescription", languageConfig.mainLanguageDescription);
            template.put("alternativeLanguageDescription", languageConfig.alternativeLanguageDescription);
            template.put("connectedUserId", connectedUserId);
            template.put("diaSemanaTexto", diaSemanaTexto);
            template.put("hayAlgunDocumento", reunionDocumentoService.hayAlgunaDocumentacion(reunionId));
            template.put("mostrarEmails", personalizationConfig.mostrarEmails);
            template.put("personasSustituyeODelegan",personasSustituyeODelegan);
        }
        catch (Exception e)
        {
            log.error("Error en publicación de plantilla reunion-votacion-", e);
            template = errorService.getTemplateConError(applang, e);
        }
        return template;
    }

    private List<Votante> addConnectedUserToTheVontantesListIfRequired(User connectedUser, List<Votante> personasSustituyeODelegan)
    {
        if(personasSustituyeODelegan.size() != 0) {
            personasSustituyeODelegan.add(new Votante(connectedUser));
        }

        return personasSustituyeODelegan;
    }
}
