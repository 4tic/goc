package es.uji.apps.goc.model;

public class Convocatoria
{
    private String convocante;
    private String convocanteEmail;
    private String texto;

    public String getConvocante() {
        return convocante;
    }

    public void setConvocante(String convocante) {
        this.convocante = convocante;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getConvocanteEmail() {
        return convocanteEmail;
    }

    public void setConvocanteEmail(String convocanteEmail) {
        this.convocanteEmail = convocanteEmail;
    }
}
