package es.uji.apps.goc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MiembroCSV extends Miembro {

    public MiembroCSV(){
        super();
    }

    public MiembroCSV(Long id, String nombre, String email, String nombreGrupo, Long ordenGrupo, String condicion, String condicionAlternativa)
    {
        super.setId(id);
        super.setNombre(nombre);
        super.setEmail(email);
        super.setGrupo(nombreGrupo);
        super.setOrdenGrupo(ordenGrupo);
        super.setCondicion(condicion);
        super.setCondicionAlternativa(condicionAlternativa);
    }

    @JsonProperty("Identificador")
    public Long getId()
    {
        return super.getId();
    }

    @JsonProperty("Nombre")
    public String getNombre()
    {
        return super.getNombre();
    }

    @JsonProperty("Email")
    public String getEmail()
    {
        return super.getEmail();
    }

    @JsonProperty("Grupo")
    public String getGrupo()
    {
        return super.getGrupo();
    }

    @JsonProperty("OrdenGrupo")
    public Long getOrdenGrupo(){
        return super.getOrdenGrupo();
    }
    @JsonProperty("Condición del miembro")
    public String getCondicion()
    {
        return super.getCondicion();
    }

    @JsonProperty("Condición del miembro alternativa")
    public String getCondicionAlternativa()
    {
        return super.getCondicionAlternativa();
    }

    @JsonIgnore
    public Cargo getCargo(){
        return super.getCargo();
    }

    @JsonIgnore
    public Organo getOrgano(){
        return super.getOrgano();
    }

    @JsonIgnore
    public Long getPersonaId(){
        return  super.getPersonaId();
    }

}
