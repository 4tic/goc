package es.uji.apps.goc.model;

import es.uji.commons.sso.User;

public class Votante {
    private String name;
    private Long personaId;

    public Votante(String name, Long personaId)
    {
        this.name = name;
        this.personaId = personaId;
    }

    public Votante(User user)
    {
        this.name = user.getName();
        this.personaId = user.getId();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }
}
