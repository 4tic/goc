package es.uji.apps.goc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.dao.CargoDAO;
import es.uji.apps.goc.dto.Cargo;
import es.uji.apps.goc.exceptions.CargosExternonException;
import es.uji.commons.rest.StringUtils;

@Service
public class CargoService {
    @Autowired
    private CargoDAO cargoDAO;

    public List<es.uji.apps.goc.model.Cargo> getCargos(String query) throws CargosExternonException {
        return cargoDAO.getCargos().stream().filter(
            cargo -> query == null || StringUtils.limpiaAcentos(cargo.getNombre().toLowerCase())
                .contains(StringUtils.limpiaAcentos(query).toLowerCase()) || StringUtils.limpiaAcentos(
                cargo.getNombreAlternativo().toLowerCase()).contains(StringUtils.limpiaAcentos(query).toLowerCase())
                || StringUtils.limpiaAcentos(cargo.getCodigo().toLowerCase())
                .contains(StringUtils.limpiaAcentos(query).toLowerCase())).collect(Collectors.toList());
    }

    public Cargo addCargo(Cargo cargo) {
        return cargoDAO.insert(cargo);
    }

    public Cargo updateCargo(Cargo cargo) {
        return cargoDAO.update(cargo);
    }

    public void removeCargo(Long cargoId) {
        cargoDAO.delete(Cargo.class, cargoId);
    }
}
