package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.exceptions.LimiteConsultaLDAPException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.Persona;
import es.uji.apps.goc.services.OrganoService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Path("personas")
public class PersonaResource extends CoreBaseService {
    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private OrganoService organoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPersonaByQueryString(@QueryParam("query") String query)
            throws PersonasExternasException, UnsupportedEncodingException, LimiteConsultaLDAPException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Persona> listaPersonas = (query != null) ? personaService.getPersonasByQueryString(query, connectedUserId) : personaService.getTodasPersonas(connectedUserId);
        return personasToUI(listaPersonas);
    }

    @GET
    @Path("locales")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPersonaLocalesByQueryString(@QueryParam("query") String query)
            throws PersonasExternasException, UnsupportedEncodingException, LimiteConsultaLDAPException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Persona> listaPersonas = new ArrayList<>();
        if(isAdminOrAutorizado(connectedUserId)){
           listaPersonas.addAll(personaService.getPersonasLocalesByQueryString(query, connectedUserId));
        }
        return personasToUI(listaPersonas);
    }

    private List<UIEntity> personasToUI(List<Persona> listaPersonas) {
        List<UIEntity> personasUI = new ArrayList<>();

        for (Persona persona : listaPersonas) {
            personasUI.add(personaToUI(persona));
        }
        return personasUI;
    }

    private UIEntity personaToUI(Persona persona) {
        UIEntity personaUI = new UIEntity();

        personaUI.put("id", persona.getId());
        personaUI.put("login", persona.getLogin());
        personaUI.put("nombre", persona.getNombre());
        personaUI.put("email", persona.getEmail());
        personaUI.put("administrador", persona.getAdministrador());

        return personaUI;
    }

    private boolean isAdminOrAutorizado(Long connectedUserId) throws RolesPersonaExternaException {
        return personaService.isAdmin(personaService.getRolesFromPersonaId(connectedUserId)) || organoService.existAutorizadosByUserId(connectedUserId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPersona(Persona persona) throws PersonasExternasException {
        personaService.insertPersona(persona);
        return Response.ok().build();
    }

    @PUT
    @Path("{personaId}/deshabilita")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deshabilitaPersona(@PathParam("personaId") Long personaId) throws PersonasExternasException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (connectedUserId.intValue() != personaId.intValue())
            personaService.deshabilitaPersona(personaId, connectedUserId);
        return Response.ok().build();
    }

    @PUT
    @Path("{personaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modificaPersona(@PathParam("personaId") Long personaId, Persona persona) throws PersonasExternasException, RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        personaService.modificaPersona(personaId, connectedUserId, persona);
        return Response.ok().build();
    }

}
