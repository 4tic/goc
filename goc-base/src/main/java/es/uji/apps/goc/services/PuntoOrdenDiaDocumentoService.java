package es.uji.apps.goc.services;

import com.mysema.query.Tuple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import es.uji.apps.goc.dao.PuntoOrdenDiaAcuerdoDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDocumentoDAO;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.PuntoOrdenDiaAcuerdo;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.EntidadNoValidaException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.DocumentoUI;

@Service
@Component
public class PuntoOrdenDiaDocumentoService
{
    private static Logger log = LoggerFactory.getLogger(PuntoOrdenDiaDocumentoService.class);

    @Autowired
    private PuntoOrdenDiaDocumentoDAO puntoOrdenDiaDocumentoDAO;

    @Autowired
    private PuntoOrdenDiaAcuerdoDAO puntoOrdenDiaAcuerdoDAO;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private ReunionService reunionService;

    public List<Tuple> getNumeroDocumentosPorReunion(Long connectedUserId)
    {
        return puntoOrdenDiaDocumentoDAO.getNumeroDocumentosPorPuntoOrdenDia();
    }

    public List<Tuple> getNumeroAcuerdosPorReunion(Long connectedUserId)
    {
        return puntoOrdenDiaAcuerdoDAO.getNumeroAcuerdosPorPuntoOrdenDia();
    }

    public PuntoOrdenDiaDocumento addDocumento(Long puntoOrdenDiaId, DocumentoUI documento, Long connectedUserId)
        throws EntidadNoValidaException
    {
        PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = new PuntoOrdenDiaDocumento();
        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDia(puntoOrdenDiaId);
        puntoOrdenDiaDocumento.setPuntoOrdenDia(puntoOrdenDia);
        puntoOrdenDiaDocumento.setDatos(documento.getData());
        puntoOrdenDiaDocumento.setMimeType(documento.getMimeType());
        puntoOrdenDiaDocumento.setNombreFichero(documento.getNombreFichero());
        puntoOrdenDiaDocumento.setDescripcion(documento.getDescripcion());
        puntoOrdenDiaDocumento.setDescripcionAlternativa(documento.getDescripcionAlternativa());
        puntoOrdenDiaDocumento.setFechaAdicion(new Date());
        puntoOrdenDiaDocumento.setCreadorId(connectedUserId);
        puntoOrdenDiaDocumento.setPublico(documento.getPublico());

        try
        {
            return puntoOrdenDiaDocumentoDAO.insert(puntoOrdenDiaDocumento);
        }
        catch(DataIntegrityViolationException e)
        {
            log.error("ERROR", e);
            throw new EntidadNoValidaException();
        }

    }

    public PuntoOrdenDiaAcuerdo addAcuerdo(Long puntoOrdenDiaId, DocumentoUI documento, Long connectedUserId)
    {
        PuntoOrdenDiaAcuerdo puntoOrdenDiaAcuerdo = new PuntoOrdenDiaAcuerdo();
        PuntoOrdenDia puntoOrdenDia = new PuntoOrdenDia(puntoOrdenDiaId);
        puntoOrdenDiaAcuerdo.setPuntoOrdenDia(puntoOrdenDia);
        puntoOrdenDiaAcuerdo.setDatos(documento.getData());
        puntoOrdenDiaAcuerdo.setMimeType(documento.getMimeType());
        puntoOrdenDiaAcuerdo.setNombreFichero(documento.getNombreFichero());
        puntoOrdenDiaAcuerdo.setDescripcion(documento.getDescripcion());
        puntoOrdenDiaAcuerdo.setDescripcionAlternativa(documento.getDescripcionAlternativa());
        puntoOrdenDiaAcuerdo.setFechaAdicion(new Date());
        puntoOrdenDiaAcuerdo.setCreadorId(connectedUserId);
        puntoOrdenDiaAcuerdo.setPublico(true);

        return puntoOrdenDiaAcuerdoDAO.insert(puntoOrdenDiaAcuerdo);
    }

    public void borrarDocumento(Long documentoId, Long puntoOrdenDiaId, Long connectedUserId)
    {
        puntoOrdenDiaDocumentoDAO.delete(PuntoOrdenDiaDocumento.class, documentoId);
    }

    public void borrarAcuerdo(Long documentoId, Long puntoOrdenDiaId, Long connectedUserId)
    {
        puntoOrdenDiaAcuerdoDAO.delete(PuntoOrdenDiaAcuerdo.class, documentoId);
    }

    @Transactional
    public PuntoOrdenDiaDocumento getDocumentoById(Long documentoId)
    {
        return puntoOrdenDiaDocumentoDAO.getDocumentoById(documentoId);
    }

    @Transactional
    public PuntoOrdenDiaAcuerdo getAcuerdoById(Long acuerdoId)
    {
        return puntoOrdenDiaAcuerdoDAO.getAcuerdoById(acuerdoId);
    }

    @Transactional
    public List<PuntoOrdenDiaDocumento> getDocumentosByPuntoOrdenDiaId(Long puntoOrdenDiaId, Long connectedUserId) throws RolesPersonaExternaException {
        if(connectedUserId != null){
            Long reunionId = puntoOrdenDiaDAO.getReunionIdByPuntoOrdenDIaId(puntoOrdenDiaId);
            if(reunionId != null) {
                Reunion reunion = reunionService.getReunionConOrganosById(reunionId, connectedUserId);
                if(reunion != null) {
                    if (personaService.isAdmin(connectedUserId) || reunionService.isAutorizadoEnReunionByReunionConOrganos(reunion, connectedUserId)) {
                        return puntoOrdenDiaDocumentoDAO.getDocumentosByPuntoOrdenDiaId(puntoOrdenDiaId);
                    }
                }
            }
        }
        return Collections.emptyList();
    }

    @Transactional
    public List<PuntoOrdenDiaAcuerdo> getAcuerdosByPuntoOrdenDiaId(Long puntoOrdenDiaId, Long connectedUserId)
    {
        return puntoOrdenDiaAcuerdoDAO.getAcuerdosByPuntoOrdenDiaId(puntoOrdenDiaId);
    }

    public void updateAcuerdo(PuntoOrdenDiaAcuerdo acuerdo)
    {
        puntoOrdenDiaAcuerdoDAO.update(acuerdo);
    }

    public PuntoOrdenDiaDocumento updateDocumento(PuntoOrdenDiaDocumento documento)
    {
        return puntoOrdenDiaDocumentoDAO.update(documento);
    }

    @Transactional
    public PuntoOrdenDiaDocumento updateDocumentoDescripcion(Long documentoId, String descripcion, String descripcionAlternativa) {
        PuntoOrdenDiaDocumento puntoOrdenDiaDocumento = puntoOrdenDiaDocumentoDAO.getDocumentoById(documentoId);
        puntoOrdenDiaDocumento.setDescripcion(descripcion);
        puntoOrdenDiaDocumento.setDescripcionAlternativa(descripcionAlternativa);
        puntoOrdenDiaDocumento = puntoOrdenDiaDocumentoDAO.update(puntoOrdenDiaDocumento);
       return puntoOrdenDiaDocumento;
    }

    @Transactional
    public PuntoOrdenDiaAcuerdo addAcuerdoUrlActa(
        Long acuerdoId,
        String urlActa
    ) {
        PuntoOrdenDiaAcuerdo acuerdo = puntoOrdenDiaAcuerdoDAO.getAcuerdoById(acuerdoId);
        acuerdo.setUrlActa(urlActa);
        puntoOrdenDiaAcuerdoDAO.update(acuerdo);
        return acuerdo;
    }
}
