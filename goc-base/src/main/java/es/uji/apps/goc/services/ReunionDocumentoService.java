package es.uji.apps.goc.services;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.PuntoOrdenDiaDAO;
import es.uji.apps.goc.dao.PuntoOrdenDiaDocumentoDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dao.ReunionDocumentoDAO;
import es.uji.apps.goc.dto.Auditoria;
import es.uji.apps.goc.dto.PuntoOrdenDiaDocumento;
import es.uji.apps.goc.dto.PuntoOrdenDiaMultinivel;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionDocumento;
import es.uji.apps.goc.exceptions.EntidadNoValidaException;
import es.uji.apps.goc.model.EnumAccionesAuditoria;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.sso.User;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Component
public class ReunionDocumentoService
{
    private static Logger log = LoggerFactory.getLogger(ReunionDocumentoService.class);

    private static final int ONE_DAY = 1000 * 60 * 60 * 24;

    @Autowired
    private ReunionDocumentoDAO reunionDocumentoDAO;

    @Autowired
    private PuntoOrdenDiaDAO puntoOrdenDiaDAO;

    @Autowired
    private PuntoOrdenDiaDocumentoDAO puntoOrdenDiaDocumentoDAO;

    @Autowired
    private ReunionDAO reunionDAO;

    @Autowired
    private PersonalizationConfig personalizationConfig;

    @Transactional
    public List<ReunionDocumento> getDocumentosByReunionId(Long reunionId, Long connectedUserId)
    {
        return reunionDocumentoDAO.getDocumentosByReunionId(reunionId);
    }

    @Transactional
    public ReunionDocumento getDocumentoById(Long documentoId)
    {
        return reunionDocumentoDAO.getDocumentoById(documentoId);
    }

    public void borrarDocumento(Long documentoId, Long reunionId, Long connectedUserId)
    {
        reunionDocumentoDAO.delete(ReunionDocumento.class, documentoId);
    }

    public ReunionDocumento addDocumento(Long reunionId, String fileName, String descripcion,
                                         String descripcionAlternativa, String mimeType, InputStream data,
                                         Long connectedUserId) throws IOException, EntidadNoValidaException
    {

        ReunionDocumento reunionDocumento = new ReunionDocumento();
        Reunion reunion = new Reunion(reunionId);
        reunionDocumento.setReunion(reunion);
        reunionDocumento.setDatos(StreamUtils.inputStreamToByteArray(data));
        reunionDocumento.setMimeType(mimeType);
        reunionDocumento.setNombreFichero(fileName);
        reunionDocumento.setDescripcion(descripcion);
        reunionDocumento.setDescripcionAlternativa(descripcionAlternativa);
        reunionDocumento.setFechaAdicion(new Date());
        reunionDocumento.setCreadorId(connectedUserId);
        try
        {
            return reunionDocumentoDAO.insert(reunionDocumento);
        }
        catch(DataIntegrityViolationException e)
        {
            log.error("ERROR", e);
            throw new EntidadNoValidaException();
        }
    }

    @Transactional
    public ReunionDocumento updateDocumento(Long documentoId, String fileName, String mimeType, InputStream data,
        Long connectedUserId)
        throws IOException
    {

        ReunionDocumento documentoAnterior = reunionDocumentoDAO.getDocumentoById(documentoId);

        documentoAnterior.setDatos(StreamUtils.inputStreamToByteArray(data));
        documentoAnterior.setMimeType(mimeType);
        documentoAnterior.setNombreFichero(fileName);
        documentoAnterior.setFechaAdicion(new Date());
        documentoAnterior.setCreadorId(connectedUserId);

        return reunionDocumentoDAO.update(documentoAnterior);
    }

    @Transactional
    public ReunionDocumento updateDocumentoDescripcion(Long documentoId, String descripcion, String descripcionAlternativa) {
        ReunionDocumento documentoAnterior= reunionDocumentoDAO.getDocumentoById(documentoId);
        documentoAnterior.setDescripcion(descripcion);
        documentoAnterior.setDescripcionAlternativa(descripcionAlternativa);

        return reunionDocumentoDAO.update(documentoAnterior);
    }

    public String getDocumentosReunionZip(Long reunionId) throws IOException
    {
        Set<PuntoOrdenDiaMultinivel> puntos = getPuntosOrdenDiaOrdeanosPorNivel(reunionId);
        String uuid = generarZip(reunionId, puntos);

        return uuid;
    }

    private String formatearParaZip(String texto) {
        return formatearTituloParaDirectorio(texto.replaceAll("/", "-").replaceAll("\t"," ").replaceAll("\n"," "));
    }

    private String generarZip(Long reunionId, Set<PuntoOrdenDiaMultinivel> puntos) throws IOException
    {
        String uuid = UUID.randomUUID().toString();
        String zipPath = personalizationConfig.basePathDocumentacion + '/' + uuid + '/' + uuid;
        File directorioBase = crearDirectorioBase(uuid);
        ZipArchiveOutputStream zout = new ZipArchiveOutputStream(new FileOutputStream(zipPath));
        zout.setEncoding("UTF-8");
        zout.setCreateUnicodeExtraFields(ZipArchiveOutputStream.UnicodeExtraFieldPolicy.ALWAYS);


        Reunion reunion = reunionDAO.getReunionById(reunionId);
        String basePath = formatearParaZip(reunion.getAsunto());
        anyadirDocumentosReunion(reunionId, directorioBase, zout, basePath);
        if(puntos != null){
            List<PuntoOrdenDiaMultinivel> puntosOrdenados = ordenarSetPuntosOrdenDia(puntos);
            anyadirNivel(puntosOrdenados, directorioBase, zout, basePath);
        }
        zout.close();

        return zipPath;
    }

    private void anyadirDocumentosReunion(
        Long reunionId,
        File directorioBase,
        ZipArchiveOutputStream zout,
        String path
    ) throws IOException
    {
        List<ReunionDocumento> documentosByReunionId = reunionDocumentoDAO.getDocumentosByReunionId(reunionId);
        for(ReunionDocumento reunionDocumento : documentosByReunionId)
        {
            File file = new File(directorioBase.getAbsolutePath() + "/" + reunionDocumento.getNombreFichero());
            addContent(file, reunionDocumento.getDatos());
            addFileToZip(file, zout, path);
        }
    }

    private File crearDirectorioBase(String uuid)
    {
        File baseDoc = new File(personalizationConfig.basePathDocumentacion);
        if(!baseDoc.exists())
        {
            baseDoc.mkdir();
            log.info("Crando el directorio para almacenar la documentación a descargar: " + personalizationConfig.basePathDocumentacion);
        }

        File base = new File(personalizationConfig.basePathDocumentacion + '/' + uuid);
        if(!base.exists())
        {
            base.mkdir();
        }
        else
        {
            base.delete();
            base.mkdir();
        }

        return base;
    }

    @Scheduled(cron = "0 59 23 * * ?")
    public void eliminarDocumentosAntiguos() {
        if(personalizationConfig.basePathDocumentacion != null &&  !personalizationConfig.basePathDocumentacion.trim().equals("")){
            LocalDate today = LocalDate.now();
            LocalDate earlier = today.minusDays(1);

            Date threshold = Date.from(earlier.atStartOfDay(ZoneId.systemDefault()).toInstant());
            AgeFileFilter filter = new AgeFileFilter(threshold);

            File path = new File(personalizationConfig.basePathDocumentacion);

            File[] oldFolders = FileFilterUtils.filter(filter, path.listFiles());

            for (File folder : oldFolders) {
                FileUtils.deleteQuietly(folder);
            }
        }
    }

    private void anyadirNivel(
        List<PuntoOrdenDiaMultinivel> puntos,
        File directorioBase,
        ZipArchiveOutputStream zout,
        String path
    ) throws IOException
    {
        if(puntos != null && !puntos.isEmpty())
        {
            Long numeroPunto = 1L;
            for (PuntoOrdenDiaMultinivel puntoOrdenDiaMultinivel : puntos)
            {
                String puntoOrdenDiaTitulo = formatearParaZip(puntoOrdenDiaMultinivel.getTitulo());
                File directorio = anyadirDirectorio(puntoOrdenDiaTitulo, directorioBase);
                directorio.mkdir();
                String pathPunto = path + "/" + numeroPunto + ". " + puntoOrdenDiaTitulo;
                anyadirDocumentos(puntoOrdenDiaMultinivel.getId(), directorio, zout, pathPunto);
                Set<PuntoOrdenDiaMultinivel> subPuntos = puntoOrdenDiaMultinivel.getPuntosInferiores();
                if (subPuntos != null) {
                    List<PuntoOrdenDiaMultinivel> subPuntosOrdenados = ordenarSetPuntosOrdenDia(subPuntos);
                    anyadirNivel(subPuntosOrdenados, directorio, zout, pathPunto);
                }
                numeroPunto++;
            }
        }
    }

    private List<PuntoOrdenDiaMultinivel> ordenarSetPuntosOrdenDia(Set<PuntoOrdenDiaMultinivel> puntos)
    {
        return puntos.stream().sorted(Comparator.comparing(PuntoOrdenDiaMultinivel::getOrden)).collect(Collectors.toList());
    }

    private void anyadirDocumentos(
        Long puntoOrdeDiaMultinivelId,
        File directorio,
        ZipArchiveOutputStream zout,
        String path
        ) throws IOException
    {
        List<PuntoOrdenDiaDocumento> documentos =
            puntoOrdenDiaDocumentoDAO.getDocumentosByPuntoOrdenDiaId(puntoOrdeDiaMultinivelId);
        for (PuntoOrdenDiaDocumento puntoOrdenDiaDocumento : documentos)
        {
            File documento =
                new File(directorio.getAbsolutePath() + "/" + puntoOrdenDiaDocumento.getNombreFichero());
            addContent(documento, puntoOrdenDiaDocumento.getDatos());
            addFileToZip(documento, zout, path);
        }
    }

    private File anyadirDirectorio(String puntoOrdenDiaMultinivelTitulo, File directorioBase)
    {
        String puntoTitulo = formatearTituloParaDirectorio(puntoOrdenDiaMultinivelTitulo);
        File directorioPunto = new File(directorioBase.getAbsolutePath() + "/" + puntoTitulo);
        directorioPunto.mkdir();
        return directorioPunto;
    }

    public String formatearTituloParaDirectorio(String puntoOrdenDiaMultinivelTitulo)
    {
        String puntoTitulo = puntoOrdenDiaMultinivelTitulo;
        if(puntoOrdenDiaMultinivelTitulo.length() > 25){
            puntoTitulo = puntoOrdenDiaMultinivelTitulo.substring(0,25);
        }
        return puntoTitulo;
    }

    private Set<PuntoOrdenDiaMultinivel> getPuntosOrdenDiaOrdeanosPorNivel(Long reunionId)
    {
        return new HashSet<>(puntoOrdenDiaDAO.getPuntosMultinivelByReunionIdFormateado(reunionId));
    }

    private void addFileToZip(File file, ZipArchiveOutputStream zout, String path) throws IOException
    {
        byte[] tmpBuf = new byte[1024];
        FileInputStream in = new FileInputStream(file.getAbsolutePath());
        String fileName = new String(file.getName().getBytes("ISO-8859-1"),"UTF-8");
        String pathDocumento = path + "/" + fileName;
        ZipArchiveEntry ze = new ZipArchiveEntry(pathDocumento);
        zout.putArchiveEntry(ze);
        int len;
        while ((len = in.read(tmpBuf)) > 0) {
            zout.write(tmpBuf, 0, len);
        }
        zout.closeArchiveEntry();
        in.close();
    }

    private void addContent(
        File file,
        byte[] datos
    ) throws IOException
    {
        OutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(datos);
        fileOutputStream.close();
    }

    public boolean hayAlgunaDocumentacion(Long reunionId)
    {
        return reunionDocumentoDAO.tieneDocumentosReunionOPuntosOrdenDia(reunionId);
    }
    @Transactional
    public void auditaDescargaDocumentos(User connectedUser, EnumAccionesAuditoria downloadReunionDocs, Long reunionId) {
        reunionDAO.insert(new Auditoria(connectedUser.getName(), null, reunionId, downloadReunionDocs.toString()));
    }
}
