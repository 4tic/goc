package es.uji.apps.goc.auth;

import com.google.common.base.Strings;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.uji.apps.goc.templates.CommonPropertiesConfig;

@Component
public class PersonalizationConfig implements CommonPropertiesConfig
{
    @Value("${goc.logo}")
    public String logo;

    @Value("${goc.logoPublic}")
    public String logoPublic;

    @Value("${goc.logoDocumentos}")
    public String logoDocumentos;

    @Value("${goc.nombreInstitucion}")
    public String nombreInstitucion;

    @Value("${goc.ciudadInstitucion}")
    public String ciudadInstitucion;

    @Value("${goc.charset}")
    public String charset;

    @Value("${goc.customCSS:}")
    public String customCSS;

    @Value("${goc.rolAdministrador:ADMIN}")
    public String rolAdministrador;

    @Value("${goc.rolUsuario:USUARIO}")
    public String rolUsuario;

    @Value("${goc.rolGestor:GESTOR}")
    public String rolGestor;

    @Value("${goc.rolDeveloper:DEVELOPER}")
    public String rolDeveloper;

    @Value("${goc.staticsUrl://static.uji.es}")
    public String staticsUrl;

    @Value("${goc.tituloAutomatico:}")
    public String tituloAutomatico;

    @Value("${goc.tituloAutomaticoAlternativo:}")
    public String tituloAutomaticoAlternativo;

    @Value("${goc.templates.path:classpath:templates/}")
    public String templatesPath;

    @Value("${goc.templates.html.path:}")
    private String templatesHtmlPath;

    @Value("${goc.privateCustomCSS:css/style.css}")
    public  String privateCustomCSS;

    @Value("${goc.tituloDeAplicacion:}")
    public String tituloDeAplicacion;

    @Value("${goc.showAusentes:false}")
    public boolean showAusentes;

    @Override
    public String getTemplatesHtmlPath()
    {
        return templatesHtmlPath != null && !templatesHtmlPath.isEmpty() ? templatesHtmlPath : templatesPath;
    }

    @Value("${goc.publicUrl}")
    public String publicUrl;

    @Value("${goc.menu.oficios:false}")
    public boolean menuOficios;

    @Value("${goc.reunion.mostrarEmails:true}")
    public boolean mostrarEmails;

    @Value("${goc.acuerdos.mostrarEnlaceCertificados:true}")
    public boolean mostrarEnlaceCertificados;

    @Value("${goc.reunion.mostrarObservaciones:false}")
    public boolean mostrarObservaciones;

    @Value("${goc.reunion.mostrarExtraordinaria:false}")
    public boolean mostrarExtraordinaria;

    @Value("${goc.miembros.mostrarDescripcion:false}")
    public boolean mostrarDescripcion;

    @Value("${goc.invitados.mostrarDescripcion:false}")
    public boolean mostrarDescripcionInvitados;

    @Value("${goc.reunion.mostrarConvocante:true}")
    public boolean mostrarConvocante;

    @Value("${goc.debug:false}")
    public boolean debug;

    @Value("${goc.enlaceManual:}")
    public String enlaceManual;

    @Value("${goc.reunion.basePathDocumentacion:/etc/uji/goc/documentacion}")
    public String basePathDocumentacion;

    @Value("${goc.reunion.enviarBase64AFirma:false}")
    public boolean enviarBase64AFirma;

    @Value("${goc.menuPersonasIsEnabled:false}")
    public boolean menuPersonasIsEnabled;

    @Value("${goc.servicios.busquedaPersonas:}")
    public String busquedaPersonas;

    @Value("${goc.bloquearRemitenteConvocatoria:true}")
    public boolean bloquearRemitenteConvocatoria;

    @Value("${uji.smtp.defaultSender}")
    public String defaultSender;

    @Value("${goc.publicacion.actasPublicas:false}")
    public boolean actasPublicas;

    @Value("${goc.external.cargosEndpoint:}")
    private String cargosExternosEndpoint;

    @Value("${goc.host}")
    public String host;

    @Value("${goc.context}")
    public String appContext;

    @Override
    public String getAppContext() {
        return appContext;
    }

    @Override
    public String getCharset() {
        return charset;
    }

    @Override
    public String getPublicUrl() {
        return publicUrl;
    }

    @Override
    public String getCustomCSS() {
        return customCSS;
    }

    @Override
    public String getLogo() {
        return logo;
    }

    @Override
    public String getLogoPublic() {
        return logoPublic;
    }

    @Override
    public String getStaticsUrl() {
        return staticsUrl;
    }

    @Override
    public String getAppTitle() {
        return tituloDeAplicacion;
    }

    public boolean isCargosExternos() {
        return !Strings.isNullOrEmpty(cargosExternosEndpoint);
    }
}
