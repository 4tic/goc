package es.uji.apps.goc.auth;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GocUrlCleanerFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig)
    {

    }

    @Override
    public void doFilter(
        ServletRequest servletRequest,
        ServletResponse servletResponse,
        FilterChain filterChain
    ) throws IOException, ServletException
    {
        HttpServletRequest clientRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse clientResponse = (HttpServletResponse) servletResponse;

        String requestURI = clientRequest.getRequestURI();
        if (clientRequest.getMethod().equals("GET") && requestURI.contains("//")) {
            String requestURL = clientRequest.getRequestURL().toString();
            String newRequestURL = requestURL.replaceAll(requestURI, requestURI.replaceAll("//+", "/"));
            clientResponse.sendRedirect(newRequestURL);
            return;
        }
        else
        {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy()
    {

    }
}
