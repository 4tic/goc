package es.uji.apps.goc.api.services;

import es.uji.apps.goc.api.models.ApiOrgano;
import es.uji.apps.goc.api.models.ApiReunion;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.model.Organo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApiReunionService {

    public List<ApiReunion> listReunionToListApiReunion(List<Reunion> reuniones){
        return reuniones.stream().map(ApiReunion ::new).collect(Collectors.toList());
    }

    public List<ApiOrgano> listOrganoToListApiOrgano(List<Organo> organos) {
        return organos.stream().map(ApiOrgano ::new).collect(Collectors.toList());
    }
}
