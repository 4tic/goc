package es.uji.apps.goc.auth;

import java.io.IOException;

import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.commons.sso.User;

public interface SecurityAuth
{
    User getUser() throws IOException;
    boolean isValidUser(User user) throws IOException;
    boolean isDeveloperUser(User user) throws RolesPersonaExternaException;
    boolean isAdminUser(User user) throws RolesPersonaExternaException;
}
