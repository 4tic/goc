package es.uji.apps.goc.api.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.api.models.ApiPuntoOrdenDia;
import es.uji.apps.goc.api.models.ApiPuntoOrdenDiaDocumento;
import es.uji.apps.goc.api.models.ApiReunion;
import es.uji.apps.goc.api.services.ApiPuntoOrdenDiaService;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.services.PuntoOrdenDiaDocumentoService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
@Api(value = "api/puntosordendia", description = "Consultas sobre puntos del orden del día")
@Path("api/puntosordendia")
public class ApiPuntosOrdenDiaResource extends CoreBaseService {

    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @InjectParam
    private ApiPuntoOrdenDiaService apiPuntoOrdenDiaService;

    @InjectParam
    private PuntoOrdenDiaDocumentoService puntoOrdenDiaDocumentoService;

    @GET
    @ApiOperation(value = "Obtiene los puntos del orden del día a partir del ID de la reunión",
            response = ApiPuntoOrdenDia.class,
            responseContainer = "List")
    @Path("{reunionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ApiPuntoOrdenDia> getApiPuntosOrdenDiaByreunionId(@ApiParam(value = "ID de reunión del que obtener los puntos del orden del día", required = true)
                                                                      @PathParam("reunionId") Long reunionId) throws RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return apiPuntoOrdenDiaService.listPuntoOrdenDiaToListApiPuntoOrdenDia(puntoOrdenDiaService.getPuntosByReunionId(reunionId,connectedUserId));
    }

    @GET
    @ApiOperation(value = "Obtiene los documentos del punto del orden del día a partir de su ID",
            response = ApiPuntoOrdenDiaDocumento.class,
            responseContainer = "List")
    @Path("{puntOrdenDiaId}/documentos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ApiPuntoOrdenDiaDocumento> getApiPuntosOrdenDiaDocumentosByPuntoOrdenDiaId(
            @ApiParam(value = "ID de punto de orden del día del que se quieren obtener los documentos", required = true)
            @PathParam("puntOrdenDiaId") Long puntoOrdenDiaId) throws RolesPersonaExternaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return apiPuntoOrdenDiaService.listPuntoOrdenDiaDocumentoToListApiPuntoOrdenDiaDocumento(puntoOrdenDiaDocumentoService.getDocumentosByPuntoOrdenDiaId(puntoOrdenDiaId, connectedUserId));
    }
}
