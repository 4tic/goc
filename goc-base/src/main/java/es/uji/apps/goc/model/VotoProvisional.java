package es.uji.apps.goc.model;

public class VotoProvisional {
    private String nombre;
    private String PersonaId;
    private VoteType vote;
    private String puntoOrdenDiaId;
    private String votoEnNombreDe;
    private String enNombreDePersonaId;

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getPersonaId()
    {
        return PersonaId;
    }

    public void setPersonaId(String personaId)
    {
        PersonaId = personaId;
    }

    public VoteType getVote()
    {
        return vote;
    }

    public void setVote(VoteType vote)
    {
        this.vote = vote;
    }

    public String getPuntoOrdenDiaId()
    {
        return puntoOrdenDiaId;
    }

    public void setPuntoOrdenDiaId(String puntoOrdenDiaId)
    {
        this.puntoOrdenDiaId = puntoOrdenDiaId;
    }

    public String getVotoEnNombreDe()
    {
        return votoEnNombreDe;
    }

    public void setVotoEnNombreDe(String votoEnNombreDe)
    {
        this.votoEnNombreDe = votoEnNombreDe;
    }

    public String getEnNombreDePersonaId()
    {
        return enNombreDePersonaId;
    }

    public void setEnNombreDePersonaId(String enNombreDePersonaId)
    {
        this.enNombreDePersonaId = enNombreDePersonaId;
    }

}
