package es.uji.apps.goc.auth.spring;

import org.apache.log4j.Logger;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.cas.authentication.CasAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.uji.apps.goc.auth.PersonaAuth;
import es.uji.apps.goc.auth.SecurityAuth;
import es.uji.commons.sso.User;

public class CasSecurityAuth extends PersonaAuth implements SecurityAuth
{
    private static Logger log = Logger.getLogger(CasSecurityAuth.class);

    @Value("${goc.cas.metadata.username:PRINCIPAL}")
    public String userNameAttribute;

    public User getUser() throws IOException
    {
        CasAuthenticationToken authentication =
            (CasAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null)
        {
            return null;
        }

        AttributePrincipal principal = authentication.getAssertion().getPrincipal();

        if(userNameAttribute.equalsIgnoreCase("PRINCIPAL"))
        {
            return getGocUserFromLoginAttrs(Arrays.asList(principal.getName()));
        }
        else
        {
            Object attribute = principal.getAttributes().getOrDefault(userNameAttribute, new ArrayList<>());
            if (attribute instanceof List) {
                return getGocUserFromLoginAttrs((List<Object>) attribute);
            }
            else {
                return getGocUserFromLoginAttrs(Arrays.asList(attribute));
            }
        }
    }

    private User getGocUserFromLoginAttrs(List<Object> userAttributes) throws IOException {
        User user = null;
        for (Object userName : userAttributes) {
            try
            {
                user = createUserFromPersonaId((String) userName);
                return user;
            }
            catch (Exception e)
            {
                log.warn("Usuario no válido");
            }
        }
        return user;
    }
}
