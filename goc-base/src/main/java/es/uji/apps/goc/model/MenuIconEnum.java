package es.uji.apps.goc.model;

public enum MenuIconEnum {
    PERSONAS("fa-user-plus"),
    MIEMBROS("fa-users"),
    CARGOS("fa-graduation-cap"),
    HISTORICO("fa-archive"),
    REUNIONES("fa-calendar-plus-o"),
    ORGANOS("fa-sitemap"),
    TIPOSORGANOS("fa-object-group"),
    DESCRIPTORES("fa-tags"),
    OFICIOS(""),
    MISREUNIONES("fa-calendar");

    public final String iconCls;

    private MenuIconEnum(String cls) {
        this.iconCls = cls;
    }
}
