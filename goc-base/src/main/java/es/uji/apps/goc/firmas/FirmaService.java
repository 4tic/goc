package es.uji.apps.goc.firmas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dto.OrganoFirma;
import es.uji.apps.goc.dto.ReunionFirma;
import es.uji.apps.goc.model.RespuestaFirma;
import es.uji.apps.goc.model.RespuestaFirmaAsistencia;
import es.uji.apps.goc.model.RespuestaFirmaPuntoOrdenDiaAcuerdo;

@Component
public class FirmaService
{
    @Autowired
    PersonalizationConfig personalizationConfig;

    public RespuestaFirma firmaReunion(ReunionFirma reunionFirma)
    {
        RespuestaFirma respuestaFirma = new RespuestaFirma();

        String urlActa = personalizationConfig.appContext + "/rest/actas/{reunionId}";
        urlActa = urlActa.replace("{reunionId}", reunionFirma.getId().toString());
        respuestaFirma.setUrlActa(urlActa);

        respuestaFirma.setAsistencias(getAsistencias(reunionFirma));
        respuestaFirma.setPuntoOrdenDiaAcuerdos(getPuntoOrdendiaRespuestaFirma(reunionFirma));

        return respuestaFirma;
    }

    private List<RespuestaFirmaPuntoOrdenDiaAcuerdo> getPuntoOrdendiaRespuestaFirma(ReunionFirma reunionFirma)
    {
        return reunionFirma.getPuntosOrdenDia().stream().map(p ->
        {
            RespuestaFirmaPuntoOrdenDiaAcuerdo respuestaFirmaPuntoOrdenDiaAcuerdo =
                new RespuestaFirmaPuntoOrdenDiaAcuerdo();
            respuestaFirmaPuntoOrdenDiaAcuerdo.setId(p.getId());
            String urlActa = personalizationConfig.appContext + "/rest/publicacion/reuniones/{reunionId}/acuerdos/{puntoId}";
            urlActa = urlActa.replace("{reunionId}", reunionFirma.getId().toString());
            urlActa = urlActa.replace("{puntoId}", p.getId().toString());
            respuestaFirmaPuntoOrdenDiaAcuerdo.setUrlActa(urlActa);
            return respuestaFirmaPuntoOrdenDiaAcuerdo;
        }).collect(Collectors.toList());
    }

    private List<RespuestaFirmaAsistencia> getAsistencias(ReunionFirma reunionFirma)
    {
        List<RespuestaFirmaAsistencia> asistencias = new ArrayList<>();
        for (OrganoFirma organoFirma : reunionFirma.getOrganos())
        {
            asistencias.addAll(organoFirma.getAsistentes().stream().map(a ->
            {
                RespuestaFirmaAsistencia respuestaFirmaAsistencia = new RespuestaFirmaAsistencia();
                respuestaFirmaAsistencia.setPersonaId(a.getId());
                String urlAsistencia = personalizationConfig.appContext + "/rest/reuniones/{reunionId}/asistencia";
                urlAsistencia = urlAsistencia.replace("{reunionId}", reunionFirma.getId().toString());
                respuestaFirmaAsistencia.setUrlAsistencia(urlAsistencia);
                return respuestaFirmaAsistencia;
            }).collect(Collectors.toList()));
        }
        return asistencias;
    }


}