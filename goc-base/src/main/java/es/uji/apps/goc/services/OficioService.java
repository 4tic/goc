package es.uji.apps.goc.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.goc.DateUtils;
import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.NotificacionesDAO;
import es.uji.apps.goc.dao.OficioDAO;
import es.uji.apps.goc.dto.EnvioOficio;
import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionTemplate;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.exceptions.ReunionNoDisponibleException;
import es.uji.apps.goc.model.Oficio;
import es.uji.apps.goc.notifications.ArchivoAdjunto;
import es.uji.apps.goc.notifications.MailSender;
import es.uji.apps.goc.notifications.Mensaje;
import es.uji.apps.goc.templates.PDFTemplate;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.rest.exceptions.CoreBaseException;

@Service
public class OficioService
{
    private static Logger log = LoggerFactory.getLogger(OficioService.class);

    @Autowired
    PersonalizationConfig personalizationConfig;

    @Autowired
    ReunionService reunionService;

    @Autowired
    MailSender mailSender;

    @Autowired
    LanguageConfig languageConfig;

    @Autowired
    OficioDAO oficioDAO;

    @Autowired
    PuntoOrdenDiaService puntoOrdenDiaService;

    @Autowired
    NotificacionesDAO notificacionesDAO;

    @Value("${goc.publicUrl}")
    private String publicUrl;

    @Value("${uji.smtp.defaultSender}")
    private String defaultSender;

    public List<Oficio> getOficios(
        Long connectedUserId
    )
    {
        List<EnvioOficio> oficios = oficioDAO.getOficios(connectedUserId);
        return oficios.stream().map(o -> Oficio.dtoToModel(o)).collect(Collectors.toList());
    }

    public void enviarOficio(Oficio oficio, PuntoOrdenDia puntoOrdenDia, Long connectedUserId, String lang)
            throws CoreBaseException, FirmanteSinSuplenteNoAsiste, FirmantesNecesariosException {
        Reunion reunion =
            reunionService.getReunionByIdAndEditorId(puntoOrdenDia.getReunion().getId(), connectedUserId);
        String cargoResponsable = reunion.getMiembroResponsableActa().getCargoNombre();
        String organoResponsable = reunion.getMiembroResponsableActa().getOrganoReunion().getOrganoNombre();
        if(!languageConfig.isMainLangauge(lang))
        {
            cargoResponsable = reunion.getMiembroResponsableActa().getCargoNombreAlternativo();
            organoResponsable = reunion.getMiembroResponsableActa().getOrganoReunion().getOrganoNombreAlternativo();
        }
        ReunionTemplate reunionTemplate = reunionService.getReunionTemplateDesdeReunion(reunion, connectedUserId, false,
            languageConfig.isMainLangauge(lang));
        Template pdf = generarOficioPDF(oficio, puntoOrdenDia, lang, reunionTemplate, cargoResponsable, organoResponsable);
        enviarMailOficio(oficio, puntoOrdenDia, pdf, connectedUserId, lang, reunionTemplate);
        guardarOficio(oficio, puntoOrdenDia);
    }

    private void enviarMailOficio(Oficio oficio, PuntoOrdenDia puntoOrdenDia, Template pdf, Long connectedUserId, String lang, ReunionTemplate reunionTemplate)
        throws CoreBaseException
    {
        Mensaje mensaje = crearMensaje(oficio, pdf, puntoOrdenDia, reunionTemplate.getId());
        notificacionesDAO.enviaNotificacion(mensaje);
        oficio.setFechaEnvio(new Date());
    }

    private Mensaje crearMensaje(
        Oficio oficio,
        Template pdf,
        PuntoOrdenDia puntoOrdenDia,
        Long reunionId
    ) throws CoreBaseException
    {
        Mensaje mensaje = new Mensaje();
        mensaje.setAsunto("[GOC] Enviament d'ofici");
        mensaje.setContentType("text/html");
        mensaje.setCuerpo(getCuerpoEmailOficio(reunionId, puntoOrdenDia.getId()));
        mensaje.setDestinos(Arrays.asList(oficio.getEmail()));
        mensaje.addAdjunto(crearAdjuntoDesdeTemplate(pdf, "oficio.pdf"));
        mensaje.setFrom(defaultSender);
        return mensaje;
    }

    private String getCuerpoEmailOficio(Long reunionId, Long puntoOrdenDiaId)
    {
        PuntoOrdenDia punto = puntoOrdenDiaService.getPuntoById(puntoOrdenDiaId);
        StringBuilder cuerpoMensaje = new StringBuilder();
        cuerpoMensaje.append("Podeu accedir al certificat des del següent enllaç "
            + "<a href=\"" + punto.getUrlActa() + "\">");
        cuerpoMensaje.append("acords");
        cuerpoMensaje.append("</a>");

        return cuerpoMensaje.toString();
    }

    private ArchivoAdjunto crearAdjuntoDesdeTemplate(Template template, String filename) throws CoreBaseException
    {
        ArchivoAdjunto archivoAdjunto = new ArchivoAdjunto();
        archivoAdjunto.setFilename(filename);
        archivoAdjunto.setcontentType("application/pdf");

        try
        {
            byte[] process = template.process();
            archivoAdjunto.setContent(Base64.getEncoder().encodeToString(process));
        } catch (Exception e)
        {
            log.error("Se ha producido un error procesando el template", e);
            throw new CoreBaseException(e.getMessage());
        }

        return archivoAdjunto;
    }

    private Template generarOficioPDF(Oficio oficio, PuntoOrdenDia puntoOrdenDia, String lang,
        ReunionTemplate reunionTemplate, String cargoResponsable, String organoResponsable)
        throws ReunionNoDisponibleException
    {
        String applang = languageConfig.getLangCode(lang);
        Template template = new PDFTemplate("oficio-" + applang, personalizationConfig);
        template.put("nombreInstitucion", personalizationConfig.nombreInstitucion);
        template.put("ciudadInstitucion", personalizationConfig.ciudadInstitucion);
        template.put("fechaReunionFormateada", getFechaFormateadaByLang(reunionTemplate.getFecha(), applang));
        template.put("fechaFormateada", getFechaFormateadaByLang(new Date(), applang));
        template.put("titulo", puntoOrdenDia.getTitulo());
        template.put("oficio", oficio);
        template.put("organos", reunionTemplate.getOrganos().stream().map(o -> o.getNombre()).collect(
            Collectors.joining(", ")));
        template.put("cargoFirmante", cargoResponsable);
        template.put("organoFirmante", organoResponsable);
        template.put("applang", applang);
        return template;
    }

    private void guardarOficio(Oficio oficio, PuntoOrdenDia puntoOrdenDia)
    {
        EnvioOficio envioOficio = oficio.toEnvioOficioDTO();
        envioOficio.setPuntoOrdenDia(puntoOrdenDia);
        oficioDAO.insert(envioOficio);
    }

    private String getFechaFormateadaByLang(Date fecha, String lang)
    {
        if ("ca".equalsIgnoreCase(lang)) return DateUtils.formattedDateInCatalan(fecha);
        if ("es".equalsIgnoreCase(lang)) return DateUtils.formattedDateInSpanish(fecha);

        return "";
    }

    public void updateOficio(Oficio oficio)
    {
        EnvioOficio envioOficio = oficio.toEnvioOficioDTO();
        envioOficio.setPuntoOrdenDia(new PuntoOrdenDia(oficio.getPuntoOrdenDiaId()));
        oficioDAO.update(envioOficio);
    }
}
