package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.dto.PuntoOrdenDia;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.model.Oficio;
import es.uji.apps.goc.services.OficioService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.CoreBaseException;
import es.uji.commons.sso.AccessManager;

@Path("oficios")
public class OficioResource extends CoreBaseService
{

    @InjectParam
    OficioService oficioService;

    @InjectParam
    PuntoOrdenDiaService puntoOrdenDiaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getOficios()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Oficio> oficios = oficioService.getOficios(connectedUserId);
        return UIEntity.toUI(oficios);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response enviarOficio(UIEntity oficioUI,
        @QueryParam("lang") String lang) throws CoreBaseException, FirmanteSinSuplenteNoAsiste, FirmantesNecesariosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Oficio oficio = createOficioByUiEntity(oficioUI);
        PuntoOrdenDia puntoOrdenDia = puntoOrdenDiaService.getPuntoById(oficio.getPuntoOrdenDiaId());
        oficioService.enviarOficio(oficio, puntoOrdenDia, connectedUserId, lang);
        return Response.ok().build();
    }

    private Oficio createOficioByUiEntity(UIEntity oficioUI)
    {
        Oficio oficio = new Oficio();
        oficio.setNombre(oficioUI.get("nombre"));
        oficio.setEmail(oficioUI.get("email"));
        oficio.setCargo(oficioUI.get("cargo"));
        oficio.setUnidad(oficioUI.get("unidad"));
        oficio.setRegistroSalida(oficioUI.get("registro"));
        oficio.setPuntoOrdenDiaId(Long.valueOf(oficioUI.get("puntoOrdenDiaId")));
        return oficio;
    }

    @PUT
    @Path("{idOficio}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOficio(Oficio oficio)
    {
        oficioService.updateOficio(oficio);
        return Response.ok().build();
    }

}
