package es.uji.apps.goc;

import java.util.Arrays;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import io.swagger.jaxrs.config.SwaggerContextService;
import io.swagger.models.Info;
import io.swagger.models.SecurityRequirement;
import io.swagger.models.Swagger;
import io.swagger.models.auth.ApiKeyAuthDefinition;
import io.swagger.models.auth.In;

public class Bootstrap extends HttpServlet
{
    public static final String ACCOUNT_READ_SCOPE = "account_read";

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        ServletContext context = config.getServletContext();
        String contextPath = context.getContextPath();

        Info info = new Info();
        info.setTitle("REST API: Ágora");
        info.setDescription("Puntos de extensión a implementar");
        info.setVersion("2.0.0");

        Swagger swagger = new Swagger();
        swagger.setInfo(info);
        swagger.setBasePath(String.format("%s/rest", contextPath));
        swagger.setProduces(Arrays.asList("application/json"));
        swagger.securityDefinition("X-UJI-AuthToken", new ApiKeyAuthDefinition("X-UJI-AuthToken", In.HEADER));
        SecurityRequirement securityRequirement = new SecurityRequirement().requirement("X-UJI-AuthToken",
            Arrays.asList(ACCOUNT_READ_SCOPE));
        swagger.security(securityRequirement);

        new SwaggerContextService()
                .withServletConfig(config)
                .updateSwagger(swagger);
    }
}