package es.uji.apps.goc.api.models;

import es.uji.apps.goc.model.Organo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(value = "Organo")
public class ApiOrgano {

    @ApiModelProperty(value = "ID del órgano", required = true)
    private String id;

    @ApiModelProperty(value = "Nombre del órgano", required = true)
    private String nombre;

    @ApiModelProperty(value = "Nombre en idioma alternativo de la aplicación")
    private String nombreAlternativo;

    @ApiModelProperty(value = "Fecha de creación del órgano", required = true)
    private Date fechaCreacion;

    @ApiModelProperty(value = "Estado del órgano, habilitado o inhabilitado")
    private Boolean inactivo;

    public ApiOrgano(Organo organo) {
        this.id = organo.getId();
        this.nombre = organo.getNombre();
        this.nombreAlternativo = organo.getNombreAlternativo();
        this.fechaCreacion = organo.getFechaCreacion();
        this.inactivo = organo.isInactivo();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreAlternativo() {
        return nombreAlternativo;
    }

    public void setNombreAlternativo(String nombreAlternativo) {
        this.nombreAlternativo = nombreAlternativo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getInactivo() {
        return inactivo;
    }

    public void setInactivo(Boolean inactivo) {
        this.inactivo = inactivo;
    }
}
