package es.uji.apps.goc.services.rest;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import es.uji.apps.goc.exceptions.MiembrosExternosException;
import es.uji.apps.goc.model.DocumentoUI;
import es.uji.apps.goc.model.Miembro;
import es.uji.apps.goc.model.MiembroCSV;
import es.uji.apps.goc.services.MiembroService;
import es.uji.apps.goc.services.OrganoService;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CSVExporter
{
    private static Logger log = LoggerFactory.getLogger(CSVExporter.class);

    @Autowired
    MiembroService miembroService;

    @Autowired
    OrganoService organoService;

    public DocumentoUI exportaMiembros(String organoId, Long connectedUserId)
        throws MiembrosExternosException
    {
        Long organoIdLocal = convertOrganoIdExternoToLocal(organoId);

        List<Miembro> miembros = (organoService.isExterno(organoIdLocal)) ?
            miembroService.getMiembrosExternos(organoId, connectedUserId) :
            miembroService.getMiembrosLocales(organoIdLocal, connectedUserId);

        List<MiembroCSV> miembrosCSV = getMiembrosCSVbyMiembros(miembros);
        return getDocumentoUIFromCsvOutputStream(miembrosCSV);
    }

    private Long convertOrganoIdExternoToLocal(String organoId) {
        Long organoIdLocal = null;
        try {
            organoIdLocal = Long.valueOf(organoId);
        } catch (Exception e) {
            log.error("No se ha podido transformar el id del órgano");
        }
        return organoIdLocal;
    }

    private DocumentoUI getDocumentoUIFromCsvOutputStream(List<MiembroCSV> miembrosCSV)
    {
        DocumentoUI documentoUI = new DocumentoUI();
        if(miembrosCSV.size()>0) {
            ByteArrayOutputStream outputStream = null;
            try {
                outputStream = getCsv(miembrosCSV);
            } catch (IOException e) {
                log.error("No se ha podido generar el archivo. " + e);
            }
            documentoUI.setData(outputStream.toByteArray());
        }else {
            documentoUI.setData(new byte[0]);
        }
        documentoUI.setMimeType("text/csv");
        return documentoUI;
    }

    private List<MiembroCSV> getMiembrosCSVbyMiembros(List<Miembro> miembros)
    {
        if (miembros == null)
            return Collections.emptyList();

        return miembros.stream().map(
            m -> new MiembroCSV(m.getId(), m.getNombre(), m.getEmail(), m.getGrupo(), m.getOrdenGrupo(), m.getCondicion(), m.getCondicionAlternativa()))
            .collect(Collectors.toList());
    }

    private ByteArrayOutputStream getCsv(List<MiembroCSV> miembros) throws IOException
    {
        CsvMapper mapper = new CsvMapper();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Object baseObject = miembros.get(0);
        CsvSchema schema = mapper.schemaFor(baseObject.getClass()).withHeader();
        mapper.writer(schema).writeValue(outputStream,miembros);
        return outputStream;
    }
}
