package es.uji.apps.goc.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.services.PersonaService;

public class PreauthenticationFilter extends AbstractPreAuthenticatedProcessingFilter
{
    private static Logger log = LoggerFactory.getLogger(PreauthenticationFilter.class);

    private PersonaService personaService;

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest httpServletRequest)
    {
        if(hasCorrectTokens(httpServletRequest))
        {
            return "";
        }
        return null;
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest httpServletRequest)
    {
        if(hasCorrectTokens(httpServletRequest))
        {
            try
            {
                return personaService.getPersonaFromDirectoryByPersonaId(
                    Long.valueOf(httpServletRequest.getHeader("X-UJI-AuthUser")));
            } catch (PersonasExternasException e)
            {
                log.error("Error al obtener la persona con ID: " + httpServletRequest.getHeader("X-UJI-AuthUser"), e);
            }
        }
        return null;
    }

    private boolean hasCorrectTokens(HttpServletRequest httpServletRequest)
    {
        return httpServletRequest.getHeader("X-UJI-AuthToken") != null &&
            httpServletRequest.getHeader("X-UJI-AuthUser") != null;
    }

    public PersonaService getPersonaService()
    {
        return personaService;
    }

public class PreAuthenticationFilter
{
}


    public void setPersonaService(PersonaService personaService)
    {
        this.personaService = personaService;
    }
}
