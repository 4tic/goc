package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.xml.transform.TransformerConfigurationException;

import es.uji.apps.goc.auth.LanguageConfig;
import es.uji.apps.goc.auth.PersonalizationConfig;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.exceptions.FirmanteSinSuplenteNoAsiste;
import es.uji.apps.goc.exceptions.FirmantesNecesariosException;
import es.uji.apps.goc.exceptions.InvalidAccessException;
import es.uji.apps.goc.exceptions.PersonasExternasException;
import es.uji.apps.goc.exceptions.ReunionNoDisponibleException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.model.ActaTemplate;
import es.uji.apps.goc.services.OrganoService;
import es.uji.apps.goc.services.PersonaService;
import es.uji.apps.goc.services.PuntoOrdenDiaService;
import es.uji.apps.goc.services.ReunionService;
import es.uji.apps.goc.templates.Template;
import es.uji.commons.sso.AccessManager;

@Path("actas")
public class ActaResource {
    @InjectParam
    private ReunionService reunionService;

    @InjectParam
    private LanguageConfig languageConfig;

    @InjectParam
    private PersonaService personaService;

    @InjectParam
    private ReunionDAO reunionDAO;

    @InjectParam
    private OrganoService organoService;

    @InjectParam
    private PersonalizationConfig personalizationConfig;

    @InjectParam
    private PuntoOrdenDiaService puntoOrdenDiaService;

    @GET
    @Path("{reunionId}")
    @Produces("application/pdf")
    @Transactional
    public Template generaPDFActa(
            @PathParam("reunionId") Long reunionId,
            @QueryParam("lang") String lang,
            @Context HttpServletRequest request
    ) throws ReunionNoDisponibleException,
            PersonasExternasException, InvalidAccessException, RolesPersonaExternaException,
            FirmanteSinSuplenteNoAsiste, FirmantesNecesariosException, IOException, TransformerConfigurationException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Reunion reunion = reunionDAO.getReunionConOrganosById(reunionId);
        String applang = languageConfig.getLangCode(lang);

        if (reunion == null) {
            throw new ReunionNoDisponibleException();
        }

        if (!personaService.tieneAcceso(reunionId, connectedUserId)) {
            throw new InvalidAccessException("No se tiene acceso a esta reunión");
        }
        ActaTemplate actaTemplate = reunionService.getActaTemplateFromReunion(reunion, connectedUserId, applang, false);
        return actaTemplate.toTemplate(personalizationConfig, personalizationConfig.showAusentes);
    }
}
