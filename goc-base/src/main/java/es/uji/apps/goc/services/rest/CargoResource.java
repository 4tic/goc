package es.uji.apps.goc.services.rest;

import com.sun.jersey.api.core.InjectParam;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.uji.apps.goc.dto.Cargo;
import es.uji.apps.goc.exceptions.CargosExternonException;
import es.uji.apps.goc.services.CargoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("cargos")
public class CargoResource extends CoreBaseService
{
    @InjectParam
    private CargoService cargoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCargos(@QueryParam("query") String query) throws CargosExternonException
    {
        return UIEntity.toUI(cargoService.getCargos(query));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addCargo(UIEntity cargo)
    {
        Cargo newCargo = cargoService.addCargo(cargo.toModel(Cargo.class));

        return UIEntity.toUI(newCargo);
    }

    @PUT
    @Path("{cargoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateCargo(@PathParam("cargoId") Long cargoId, UIEntity cargo)
    {
        Cargo updatedCargo = cargoService.updateCargo(cargo.toModel(Cargo.class));

        return UIEntity.toUI(updatedCargo);
    }

    @DELETE
    @Path("{cargoId}")
    public Response borraCargo(@PathParam("cargoId") Long cargoId, UIEntity cargo)
    {
        cargoService.removeCargo(cargoId);

        return Response.ok().build();
    }
}
