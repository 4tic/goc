package es.uji.apps.goc.api.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.goc.api.models.ApiOrgano;
import es.uji.apps.goc.api.models.ApiReunion;
import es.uji.apps.goc.api.services.ApiReunionService;
import es.uji.apps.goc.exceptions.OrganosExternosException;
import es.uji.apps.goc.exceptions.RolesPersonaExternaException;
import es.uji.apps.goc.services.OrganoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.sso.AccessManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Api (value = "api/organo", description = "Consultas sobre órganos")
@Path("api/organo")
public class ApiOrganoResource extends CoreBaseService {

    @InjectParam
    private OrganoService organoService;

    @InjectParam
    private ApiReunionService apiReunionService;

    @GET
    @ApiOperation(value = "Obtiene reuniones por ID de órgano",
            response = ApiReunion.class,
            responseContainer = "List")
    @Path("{organoId}/reuniones")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ApiReunion> getReunionesByOrganoId(@ApiParam(value = "ID de órgano a buscar en las reuniones", required = true)
                                                       @PathParam("organoId") String organoId) throws OrganosExternosException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return apiReunionService.listReunionToListApiReunion(organoService.getReunionesByOrganoId(organoId ,connectedUserId));
    }

    @GET
    @ApiOperation(value = "Obtiene el órgano a partir de su nombre",
            response = ApiOrgano.class,
            responseContainer = "List")
    @Path("nombre/{organoNombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ApiOrgano> getOrganoByNombreId(@ApiParam(value = "Nombre del órgano que se quiere buscar", required = true)
                                                   @PathParam("organoNombre") String organoNombre) throws OrganosExternosException, RolesPersonaExternaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return apiReunionService.listOrganoToListApiOrgano(organoService.getOrganosByNombre(organoNombre ,connectedUserId));
    }

}
