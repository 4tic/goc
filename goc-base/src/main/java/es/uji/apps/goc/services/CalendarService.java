package es.uji.apps.goc.services;

import net.fortuna.ical4j.model.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.stream.Collectors;

import es.uji.apps.goc.dao.CalendarioDAO;
import es.uji.apps.goc.dao.ReunionDAO;
import es.uji.apps.goc.dto.PersonaToken;
import es.uji.apps.goc.dto.PersonaTokenKey;
import es.uji.apps.goc.dto.Reunion;
import es.uji.apps.goc.dto.ReunionPermiso;

@Service
public class CalendarService
{
    @Autowired
    ReunionDAO reunionDAO;

    @Autowired
    CalendarioDAO calendarioDAO;

    @Autowired
    ICalService iCalService;

    @Transactional
    public String crearFeed(Long connectedUserId)
    {
        return crearTokenUsuario(connectedUserId);
    }

    public Calendar obtenerCalendarioUsuario(String token, String lang)
    {
        Long connectedUserId = getUsuarioByToken(token);
        List<ReunionPermiso> reunionesFuturasByUsuario = getReunionesFuturasByUsuario(connectedUserId);
        return crearIcalFromReuniones(reunionesFuturasByUsuario, lang);
    }

    private Long getUsuarioByToken(String token)
    {
        return calendarioDAO.getIdPersonaByToken(token);
    }

    private List<ReunionPermiso> getReunionesFuturasByUsuario(Long connectedUserId)
    {
        List<ReunionPermiso> reunionesAccesiblesByPersonaId =
            reunionDAO.getReunionesAccesiblesByPersonaId(connectedUserId);
        return
            reunionesAccesiblesByPersonaId.stream().filter(r -> r.getFecha().after(new Date())).collect(Collectors.toList());
    }

    private String crearTokenUsuario(Long connectedUserId)
    {
        String token = UUID.randomUUID().toString();
        guardarTokenUsuario(connectedUserId, token);
        return token;
    }

    private void guardarTokenUsuario(
        Long connectedUserId,
        String token
    )
    {
        eliminarTokenUsuario(connectedUserId);

        PersonaTokenKey personaTokenKey = new PersonaTokenKey();
        personaTokenKey.setIdPersona(connectedUserId);
        personaTokenKey.setToken(token);
        PersonaToken personaToken = new PersonaToken();
        personaToken.setId(personaTokenKey);

        calendarioDAO.insert(personaToken);
    }

    private void eliminarTokenUsuario(Long connectedUserId)
    {
        calendarioDAO.deleteByIdPersona(connectedUserId);
    }

    private Calendar crearIcalFromReuniones(List<ReunionPermiso> reuniones, String lang)
    {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("i18n",
            Locale.forLanguageTag(lang));

        List<Reunion> reunionesCompletas =
            reuniones.stream().map(r -> reunionDAO.getReunionById(r.getId())).collect(Collectors.toList());
        Calendar calendar = null;
        try
        {
            calendar = iCalService.creaICal(reunionesCompletas, resourceBundle.getString("calendario.nombre"));
        } catch (URISyntaxException e)
        {
            e.printStackTrace();
        }
        return calendar;
    }
}
