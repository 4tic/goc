ALTER TABLE GOC_VOTOS_PRIVADOS
    MODIFY ID VARCHAR2(36);
ALTER TABLE GOC_VOTANTES_PRIVADOS
    MODIFY ID VARCHAR2(36);

update GOC_VOTOS_PRIVADOS
set ID = lower(regexp_replace(upper(ID), '([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})',
                              '\1-\2-\3-\4-\5')) where ID not like '%-%-%-%-%';

update GOC_VOTANTES_PRIVADOS
set ID = lower(regexp_replace(upper(ID), '([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})',
                              '\1-\2-\3-\4-\5')) where ID not like '%-%-%-%-%';