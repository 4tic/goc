alter table GOC_MIEMBROS
    add VOTANTE NUMBER(1) default 1;

alter table GOC_ORGANOS_REUNIONES_MIEMBROS
    add VOTANTE NUMBER(1) default 1;