alter table GOC_ORGANOS_REUNIONES_MIEMBROS
    modify ASISTENCIA default null null;

update goc_organos_reuniones_miembros
set asistencia = null
where asistencia_confirmada is null
  and asistencia = 1;

alter table GOC_ORGANOS_REUNIONES_MIEMBROS
    drop column ASISTENCIA_CONFIRMADA;

