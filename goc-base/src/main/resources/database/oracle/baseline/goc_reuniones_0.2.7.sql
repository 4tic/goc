create TABLE GOC_VOTANTES_PRIVADOS
(
    ID         number(*) PRIMARY KEY,
    REUNION_ID number(*),
    FECHA_VOTO date,
    PUNTO_ID   number(*),
    PERSONA_ID number(*)
);

create TABLE GOC_VOTOS_PRIVADOS
(
    ID         number(*) PRIMARY KEY,
    REUNION_ID number(*),
    PUNTO_ID   number(*),
    VOTO       varchar2(10),
    CONSTRAINT check_vote_private_value CHECK ( VOTO IN ('FAVOR', 'CONTRA', 'ABSTENCION'))
);