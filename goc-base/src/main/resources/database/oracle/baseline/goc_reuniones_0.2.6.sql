
create TABLE GOC_VOTOS (
    ID number(*) PRIMARY KEY ,
    REUNION_ID number(*),
    PUNTO_ID number(*),
    PERSONA_ID number(*),
    MIEMBRO_ID number(*),
    NOMBRE_VOTANTE varchar2(500),
    VOTO varchar2(10),
    CONSTRAINT check_vote_value  CHECK ( VOTO IN ('FAVOR', 'CONTRA', 'ABSTENCION'))
);