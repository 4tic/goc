CREATE OR REPLACE FORCE VIEW GOC_VW_REUNIONES_PERMISOS
(
    ID,
    COMPLETADA,
    FECHA,
    ASUNTO,
    ASUNTO_ALT,
    PERSONA_ID,
    PERSONA_NOMBRE,
    ASISTENTE,
    URL_ACTA,
    URL_ACTA_ALT,
    URL_ASISTENCIA,
    URL_ASISTENCIA_ALT,
    AVISO_PRIMERA_REUNION
)
AS
  SELECT
    s.id,
    s.completada,
    s.fecha,
    s.asunto,
    s.asunto_alt,
    s.persona_id,
    s.persona_nombre,
    SUM(s.asistente) asistente,
    s.url_acta,
    s.url_acta_alt,
    (SELECT a.url_asistencia
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
           AND a.persona_id = s.persona_id
           AND ROWNUM = 1)
                     url_asistencia,
    (SELECT a.url_asistencia_alt
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
           AND a.persona_id = s.persona_id
           AND ROWNUM = 1)
                     url_asistencia_alt,
    s.aviso_primera_reunion
  FROM (SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.creador_id     persona_id,
          r.creador_nombre persona_nombre,
          0                asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          ra.persona_id     persona_id,
          ra.persona_nombre persona_nombre,
          0                 asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_autorizados ra
        WHERE r.id = orr.reunion_id AND orr.organo_id = ra.organo_id
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          ri.persona_id     persona_id,
          ri.persona_nombre persona_nombre,
          1                 asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r, goc_reuniones_invitados ri
        WHERE ri.reunion_id = r.id
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          ori.persona_id                        persona_id,
          ori.nombre                            persona_nombre,
          decode(ori.solo_consulta, 0, 1, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_invits ori
        WHERE r.id = orr.reunion_id AND ori.organo_reunion_id = orr.id
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          orrm.suplente_id     persona_id,
          orrm.suplente_nombre persona_nombre,
          1                    asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_miembros orrm
        WHERE r.id = orr.reunion_id
              AND orr.id = orrm.organo_reunion_id
              AND orrm.asistencia = 1
              AND suplente_id IS NOT NULL
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          orrm.miembro_id                 persona_id,
          orrm.nombre                     persona_nombre,
          DECODE(suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_miembros orrm
        WHERE r.id = orr.reunion_id
              AND orr.id = orrm.organo_reunion_id
              AND orrm.asistencia = 1
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          orrm.miembro_id persona_id,
          orrm.nombre     persona_nombre,
          0               asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_miembros orrm
        WHERE r.id = orr.reunion_id
              AND orr.id = orrm.organo_reunion_id
              AND orrm.asistencia = 0) s
  GROUP BY s.id,
    s.completada,
    s.fecha,
    s.asunto,
    s.asunto_alt,
    s.persona_id,
    s.persona_nombre,
    s.url_acta,
    s.url_acta_alt,
    s.aviso_primera_reunion;

alter table GOC_REUNIONES
  add observaciones CLOB;
alter table goc_reuniones
  add extraordinaria number(1);
ALTER TABLE GOC_CARGOS
  ADD ORDEN NUMBER;

ALTER TABLE GOC_ORGANOS_REUNIONES_MIEMBROS
  ADD CARGO_ORDEN NUMBER;

ALTER TABLE GOC_ORGANOS_REUNIONES_MIEMBROS
  MODIFY (CARGO_ID NULL);

ALTER TABLE GOC_ORGANOS_REUNIONES_MIEMBROS
  MODIFY (CARGO_NOMBRE NULL);

ALTER TABLE GOC_ORGANOS_REUNIONES_MIEMBROS
  MODIFY (CARGO_CODIGO NULL);


CREATE OR REPLACE FORCE VIEW GOC_VW_REUNIONES_PERMISOS
(
    ID,
    COMPLETADA,
    FECHA,
    ASUNTO,
    ASUNTO_ALT,
    PERSONA_ID,
    PERSONA_NOMBRE,
    ASISTENTE,
    URL_ACTA,
    URL_ACTA_ALT,
    URL_ASISTENCIA,
    URL_ASISTENCIA_ALT,
    AVISO_PRIMERA_REUNION
)
AS
  SELECT
    s.id,
    s.completada,
    s.fecha,
    s.asunto,
    s.asunto_alt,
    s.persona_id,
    s.persona_nombre,
    SUM(s.asistente) asistente,
    s.url_acta,
    s.url_acta_alt,
    (SELECT a.url_asistencia
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
           AND a.persona_id = s.persona_id
           AND ROWNUM = 1)
                     url_asistencia,
    (SELECT a.url_asistencia_alt
     FROM goc_vw_certificados_asistencia a
     WHERE a.reunion_id = s.id
           AND a.persona_id = s.persona_id
           AND ROWNUM = 1)
                     url_asistencia_alt,
    s.aviso_primera_reunion
  FROM (SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          r.creador_id     persona_id,
          r.creador_nombre persona_nombre,
          0                asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          ra.persona_id     persona_id,
          ra.persona_nombre persona_nombre,
          0                 asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_autorizados ra
        WHERE r.id = orr.reunion_id AND orr.organo_id = ra.organo_id
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          ri.persona_id     persona_id,
          ri.persona_nombre persona_nombre,
          1                 asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r, goc_reuniones_invitados ri
        WHERE ri.reunion_id = r.id
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          ori.persona_id                        persona_id,
          ori.nombre                            persona_nombre,
          decode(ori.solo_consulta, 0, 1, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_invits ori
        WHERE r.id = orr.reunion_id AND ori.organo_reunion_id = orr.id
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          orrm.suplente_id     persona_id,
          orrm.suplente_nombre persona_nombre,
          1                    asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_miembros orrm
        WHERE r.id = orr.reunion_id
              AND orr.id = orrm.organo_reunion_id
              AND orrm.asistencia = 1
              AND suplente_id IS NOT NULL
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          orrm.miembro_id                 persona_id,
          orrm.nombre                     persona_nombre,
          DECODE(suplente_id, NULL, 1, 0) asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_miembros orrm
        WHERE r.id = orr.reunion_id
              AND orr.id = orrm.organo_reunion_id
              AND orrm.asistencia = 1
        UNION
        SELECT
          r.id,
          r.completada,
          r.fecha,
          r.asunto,
          r.asunto_alt,
          orrm.miembro_id persona_id,
          orrm.nombre     persona_nombre,
          0               asistente,
          r.url_acta,
          r.url_acta_alt,
          r.AVISO_PRIMERA_REUNION aviso_primera_reunion
        FROM goc_reuniones r,
          goc_organos_reuniones orr,
          goc_organos_reuniones_miembros orrm
        WHERE r.id = orr.reunion_id
              AND orr.id = orrm.organo_reunion_id
              AND orrm.asistencia = 0) s
  GROUP BY s.id,
    s.completada,
    s.fecha,
    s.asunto,
    s.asunto_alt,
    s.persona_id,
    s.persona_nombre,
    s.url_acta,
    s.url_acta_alt,
    s.aviso_primera_reunion;

ALTER TABLE GOC_REUNIONES
  ADD AVISO_PRIMERA_REUNION_EMAIL VARCHAR2(255);
