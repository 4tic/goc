CREATE OR REPLACE FORCE VIEW GOC_VW_REU_PT_OR_DIA
            (
             ID,
             TITULO,
             TITULO_ALT,
             DESCRIPCION,
             DESCRIPCION_ALT,
             ORDEN,
             REUNION_ID,
             ACUERDOS,
             ACUERDOS_ALT,
             DELIBERACIONES,
             DELIBERACIONES_ALT,
             PUBLICO,
             URL_ACTA,
             URL_ACTA_ALT,
             URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT,
             EDITADO_EN_REAPERTURA,
             ID_PUNTO_SUPERIOR,
             VOTO_PUBLICO,
             PROFUNDIDAD,
             ORDEN_NIVEL_ZERO
                )
AS
WITH
    ca
        AS
        (SELECT
             ID,
             TITULO,
             TITULO_ALT,
             DESCRIPCION,
             DESCRIPCION_ALT,
             ORDEN,
             REUNION_ID,
             ACUERDOS,
             ACUERDOS_ALT,
             DELIBERACIONES,
             DELIBERACIONES_ALT,
             PUBLICO,
             URL_ACTA,
             URL_ACTA_ALT,
             URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT,
             EDITADO_EN_REAPERTURA,
             ID_PUNTO_SUPERIOR,
             VOTO_PUBLICO,
             level AS PROFUNDIDAD,
             CONNECT_BY_ROOT orden AS ORDEN_NIVEL_ZERO
         FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
         START WITH id IN (SELECT id
                           FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                           WHERE ID_PUNTO_SUPERIOR IS NULL)
         CONNECT BY ID_PUNTO_SUPERIOR = PRIOR id
        ) SELECT *
FROM ca
ORDER BY ORDEN_NIVEL_ZERO asc;
