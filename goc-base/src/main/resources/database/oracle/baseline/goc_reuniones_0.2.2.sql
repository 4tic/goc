create table GOC_REUNIONES_EXTERNOS
(
  ID                 NUMBER,
  REUNION_ID         NUMBER           not null,
  PERSONA_ID         NUMBER           not null,
  NOMBRE             VARCHAR2(500)    not null,
  EMAIL              VARCHAR2(200)    not null,
  CONSTRAINT PK_EXTERNOS_ID PRIMARY KEY (ID)
);


ALTER TABLE GOC_REUNIONES_EXTERNOS
  ADD CONSTRAINT UK_EXTERNOS_EMAIL_R_ID UNIQUE (EMAIL ,REUNION_ID);
