alter table GOC_MIEMBROS
	add FIRMANTE NUMBER(1)
	default 0;

alter table GOC_ORGANOS_REUNIONES_MIEMBROS
	add FIRMANTE NUMBER(1)
	default 0;

alter table GOC_ORGANOS
  add ORDENADO NUMBER(1) default 0;
/
create table GOC_REUNIONES_DILIGENCIAS
(
  ID NUMBER(*),
  REUNION_ID NUMBER(*),
  USUARIO_REABRE NUMBER(8),
  MOTIVO_REAPERTURA varchar2(4000),
  FECHA_REAPERTURA DATE,

  constraint DILIGENCIAS_pk
  primary key (ID),
  constraint DILIGENCIAS_fk_REUNIONES
  foreign key (REUNION_ID) references GOC_REUNIONES (ID),
  constraint DILIGENCIAS_fk_PERSONAS
  foreign key (USUARIO_REABRE) references GOC_PERSONAS (ID)

  )

alter table GOC_REUNIONES
	add REABIERTA NUMBER(1) default 0
/

alter table GOC_REUNIONES_PUNTOS_ORDEN_DIA
	add EDITADO_EN_REAPERTURA NUMBER(1) default 0
/

CREATE OR REPLACE FORCE VIEW GOC_VW_REU_PT_OR_DIA
(
    ID,
    TITULO,
    TITULO_ALT,
    DESCRIPCION,
    DESCRIPCION_ALT,
    ORDEN,
    REUNION_ID,
    ACUERDOS,
    ACUERDOS_ALT,
    DELIBERACIONES,
    DELIBERACIONES_ALT,
    PUBLICO,
    URL_ACTA,
    URL_ACTA_ALT,
    URL_ACTA_ANTERIOR,
    URL_ACTA_ANTERIOR_ALT,
    EDITADO_EN_REAPERTURA,
    ID_PUNTO_SUPERIOR,
    PROFUNDIDAD,
    ORDEN_NIVEL_ZERO
)
AS
  WITH
      ca
  AS
    (SELECT
                ID,
                TITULO,
                TITULO_ALT,
                DESCRIPCION,
                DESCRIPCION_ALT,
                ORDEN,
                REUNION_ID,
                ACUERDOS,
                ACUERDOS_ALT,
                DELIBERACIONES,
                DELIBERACIONES_ALT,
                PUBLICO,
                URL_ACTA,
                URL_ACTA_ALT,
                URL_ACTA_ANTERIOR,
                URL_ACTA_ANTERIOR_ALT,
                EDITADO_EN_REAPERTURA,
                ID_PUNTO_SUPERIOR,
                level AS PROFUNDIDAD,
                CONNECT_BY_ROOT orden AS ORDEN_NIVEL_ZERO
              FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
              START WITH id IN (SELECT id
                                FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                                WHERE ID_PUNTO_SUPERIOR IS NULL)
              CONNECT BY ID_PUNTO_SUPERIOR = PRIOR id
  ) SELECT *
    FROM ca
    ORDER BY ORDEN_NIVEL_ZERO asc
