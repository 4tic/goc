create TABLE GOC_VOTANTES_PRIVADOS (
   ID bigint,
   REUNION_ID varchar(20) not null,
   PUNTO_ID varchar(20) not null,
   PERSONA_ID varchar(20) not null,
   FECHA_VOTO timestamp
);

ALTER TABLE GOC_VOTANTES_PRIVADOS
    ADD PRIMARY KEY (id);

create TABLE GOC_VOTOS_PRIVADOS (
    ID VARCHAR(32),
    REUNION_ID varchar(20) not null,
    PUNTO_ID varchar(20) not null,
    VOTO varchar(10)
);
ALTER TABLE GOC_VOTOS_PRIVADOS
    ADD PRIMARY KEY (id);