alter table goc_miembros
    add votante boolean default true;

alter table public.goc_organos_reuniones_miembros
    add votante boolean default true;