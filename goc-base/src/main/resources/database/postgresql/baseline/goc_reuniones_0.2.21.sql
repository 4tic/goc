UPDATE GOC_REUNIONES
SET HAS_VOTACION = false
where FECHA_CREACION < CURRENT_DATE;

UPDATE GOC_REUNIONES
SET VOTACION_PUBLICA = false
where FECHA_CREACION < CURRENT_DATE;

UPDATE GOC_REUNIONES_PUNTOS_ORDEN_DIA
SET VOTO_PUBLICO = false
where GOC_REUNIONES_PUNTOS_ORDEN_DIA.REUNION_ID in (select REUNION_ID
                                                    from GOC_REUNIONES
                                                    where FECHA_CREACION < CURRENT_DATE);