CREATE TABLE goc_auditoria
(
    id              bigint NOT NULL,
    responsable     varchar(500),
    usuario_destino varchar(500),
    reunion_id      bigint,
    accion          varchar(1000),
    fecha_accion    timestamp
);
ALTER TABLE goc_auditoria
    ADD PRIMARY KEY (id);

CREATE TABLE goc_cargos
(
    id               bigint      NOT NULL,
    nombre           varchar(400),
    nombre_alt       varchar(400),
    codigo           varchar(10) NOT NULL,
    responsable_acta boolean,
    suplente         boolean
);
ALTER TABLE goc_cargos
    ADD UNIQUE (codigo);
ALTER TABLE goc_cargos
    ADD PRIMARY KEY (id);

CREATE TABLE goc_claves
(
    id            bigint NOT NULL,
    clave         varchar(100),
    clave_alt     varchar(100),
    estado        smallint,
    id_descriptor bigint
);
ALTER TABLE goc_claves
    ADD PRIMARY KEY (id);

CREATE TABLE goc_colectivos
(
    id                bigint       NOT NULL,
    nombre            varchar(100) NOT NULL,
    tipo_colectivo_id bigint       NOT NULL
);
ALTER TABLE goc_colectivos
    ADD PRIMARY KEY (id);

CREATE TABLE goc_descriptores
(
    id              bigint NOT NULL,
    descriptor      varchar(100),
    descriptor_alt  varchar(100),
    descripcion     varchar(200),
    descripcion_alt varchar(200),
    estado          smallint
);
ALTER TABLE goc_descriptores
    ADD PRIMARY KEY (id);

CREATE TABLE goc_descriptores_tipos_organo
(
    id             bigint NOT NULL,
    descriptor_id  bigint NOT NULL,
    tipo_organo_id bigint NOT NULL
);
CREATE INDEX uji_descriptores_tip_org_de_i ON goc_descriptores_tipos_organo (descriptor_id);
CREATE INDEX uji_descriptores_tip_org_to_i ON goc_descriptores_tipos_organo (tipo_organo_id);
ALTER TABLE goc_descriptores_tipos_organo
    ADD PRIMARY KEY (id);

CREATE TABLE goc_envios_oficio
(
    id              integer NOT NULL,
    nombre          varchar(255),
    email           varchar(255),
    cargo           varchar(255),
    unidad          varchar(255),
    registro_salida varchar(255),
    fecha_envio     timestamp,
    punto_orden_dia integer
);
ALTER TABLE goc_envios_oficio
    ADD PRIMARY KEY (id);

CREATE TABLE goc_miembros
(
    id               bigint       NOT NULL,
    nombre           varchar(500) NOT NULL,
    email            varchar(200) NOT NULL,
    organo_id        bigint       NOT NULL,
    cargo_id         bigint,
    persona_id       bigint       NOT NULL,
    responsable_acta boolean,
    descripcion      varchar(255),
    descripcion_alt  varchar(255),
    orden            bigint,
    grupo            varchar(255),
    orden_grupo      bigint,
    firmante         boolean DEFAULT 'f'
);
CREATE UNIQUE INDEX goc_miembros_r01 ON goc_miembros (organo_id, cargo_id, persona_id);
ALTER TABLE goc_miembros
    ADD PRIMARY KEY (id);
ALTER TABLE goc_miembros
    ADD UNIQUE (organo_id, cargo_id, persona_id);

CREATE TABLE goc_organos
(
    id                     bigint       NOT NULL,
    nombre                 varchar(100) NOT NULL,
    tipo_organo_id         bigint       NOT NULL,
    creador_id             bigint       NOT NULL,
    fecha_creacion         timestamp    NOT NULL,
    inactivo               boolean DEFAULT 'f',
    nombre_alt             varchar(400),
    convocar_sin_orden_dia boolean,
    ordenado               boolean DEFAULT 'f'
);
ALTER TABLE goc_organos
    ADD PRIMARY KEY (id);

CREATE TABLE goc_organos_autorizados
(
    id               bigint        NOT NULL,
    persona_id       bigint        NOT NULL,
    persona_nombre   varchar(1000) NOT NULL,
    organo_id        varchar(100)  NOT NULL,
    organo_externo   boolean       NOT NULL,
    persona_email    varchar(1000),
    subir_documentos boolean
);
ALTER TABLE goc_organos_autorizados
    ADD PRIMARY KEY (id);

CREATE TABLE goc_organos_invitados
(
    id              bigint        NOT NULL,
    persona_id      bigint        NOT NULL,
    persona_nombre  varchar(1000) NOT NULL,
    persona_email   varchar(1000) NOT NULL,
    organo_id       varchar(100)  NOT NULL,
    solo_consulta   boolean       NOT NULL DEFAULT 'f',
    descripcion     varchar(255),
    descripcion_alt varchar(255),
    orden           bigint
);
ALTER TABLE goc_organos_invitados
    ADD PRIMARY KEY (id);

CREATE TABLE goc_organos_logo
(
    organo_id     varchar(5) NOT NULL,
    externo       boolean    NOT NULL,
    imagen_base64 text,
    extension     varchar(20)
);
ALTER TABLE goc_organos_logo
    ADD PRIMARY KEY (organo_id, externo);

CREATE TABLE goc_organos_parametros
(
    id_organo               varchar(5) NOT NULL,
    ordinario               boolean    NOT NULL,
    convocar_sin_orden_dia  boolean,
    email                   varchar(255),
    acta_provisional_activa boolean
);
ALTER TABLE goc_organos_parametros
    ADD PRIMARY KEY (id_organo, ordinario);

CREATE TABLE goc_organos_reuniones
(
    id                     bigint       NOT NULL,
    reunion_id             bigint       NOT NULL,
    organo_nombre          varchar(400),
    tipo_organo_id         bigint,
    externo                boolean,
    organo_id              varchar(100) NOT NULL,
    organo_nombre_alt      varchar(400),
    convocar_sin_orden_dia boolean
);
ALTER TABLE goc_organos_reuniones
    ADD PRIMARY KEY (id);

CREATE TABLE goc_organos_reuniones_invits
(
    id                 bigint,
    organo_reunion_id  bigint       NOT NULL,
    organo_externo     boolean      NOT NULL,
    reunion_id         bigint       NOT NULL,
    organo_id          varchar(400) NOT NULL,
    nombre             varchar(500) NOT NULL,
    email              varchar(200) NOT NULL,
    persona_id         bigint       NOT NULL,
    url_asistencia     varchar(1000),
    url_asistencia_alt varchar(1000),
    solo_consulta      boolean      NOT NULL DEFAULT 'f',
    descripcion        varchar(255),
    descripcion_alt    varchar(255),
    orden              bigint
);

CREATE TABLE goc_organos_reuniones_miembros
(
    id                    bigint       NOT NULL,
    organo_reunion_id     bigint       NOT NULL,
    organo_externo        boolean      NOT NULL,
    reunion_id            bigint       NOT NULL,
    organo_id             varchar(400) NOT NULL,
    nombre                varchar(500) NOT NULL,
    email                 varchar(200) NOT NULL,
    asistencia            boolean      NOT NULL DEFAULT 'f',
    cargo_id              varchar(400),
    cargo_nombre          varchar(400),
    suplente_id           bigint,
    suplente_nombre       varchar(1000),
    asistencia_confirmada boolean,
    suplente_email        varchar(200),
    miembro_id            bigint       NOT NULL,
    cargo_nombre_alt      varchar(400),
    delegado_voto_id      bigint,
    delegado_voto_nombre  varchar(1000),
    delegado_voto_email   varchar(200),
    cargo_codigo          varchar(10),
    url_asistencia        varchar(1000),
    url_asistencia_alt    varchar(1000),
    condicion             varchar(1000),
    condicion_alt         varchar(1000),
    justifica_ausencia    boolean,
    responsable_acta      boolean,
    descripcion           varchar(255),
    descripcion_alt       varchar(255),
    orden                 bigint,
    grupo                 varchar(255),
    orden_grupo           bigint,
    motivo_ausencia       varchar(255),
    secretario_designado  boolean,
    cargo_suplente        boolean,
    firmante              boolean               DEFAULT 'f'
);
ALTER TABLE goc_organos_reuniones_miembros
    ADD PRIMARY KEY (id);

CREATE TABLE goc_personas
(
    id         integer      NOT NULL,
    nombre     varchar(500),
    email      varchar(200),
    habilitada boolean DEFAULT 't',
    login      varchar(200) NOT NULL
);
CREATE UNIQUE INDEX uk_email ON goc_personas (email);
ALTER TABLE goc_personas
    ADD PRIMARY KEY (id);
ALTER TABLE goc_personas
    ADD UNIQUE (login);

CREATE TABLE goc_personas_roles
(
    id         integer NOT NULL,
    id_persona integer,
    rol        varchar(20)
);
CREATE UNIQUE INDEX personas_roles_uq ON goc_personas_roles (id_persona, rol);
ALTER TABLE goc_personas_roles
    ADD PRIMARY KEY (id);
ALTER TABLE goc_personas_roles
    ADD UNIQUE (id_persona, rol);

CREATE TABLE goc_personas_tmp
(
    id    integer      NOT NULL,
    email varchar(200),
    login varchar(200) NOT NULL
);
ALTER TABLE goc_personas_tmp
    ADD PRIMARY KEY (id);
ALTER TABLE goc_personas_tmp
    ADD UNIQUE (login);

CREATE TABLE goc_persona_token
(
    id_persona bigint       NOT NULL,
    token      varchar(100) NOT NULL
);
ALTER TABLE goc_persona_token
    ADD PRIMARY KEY (id_persona, token);

CREATE TABLE goc_p_orden_dia_acuerdos
(
    id              bigint        NOT NULL,
    punto_id        bigint        NOT NULL,
    descripcion     varchar(2048) NOT NULL,
    mime_type       varchar(512)  NOT NULL,
    nombre_fichero  varchar(512)  NOT NULL,
    fecha_adicion   timestamp     NOT NULL DEFAULT LOCALTIMESTAMP,
    datos           oid           NOT NULL,
    creador_id      bigint        NOT NULL,
    descripcion_alt varchar(2048),
    publico         boolean       NOT NULL DEFAULT 't',
    hash_string     varchar(250)
);

CREATE TABLE goc_p_orden_dia_comentarios
(
    id                  bigint        NOT NULL,
    punto_orden_dia_id  bigint        NOT NULL,
    comentario          text          NOT NULL,
    creador_id          bigint        NOT NULL,
    creador_nombre      varchar(1000) NOT NULL,
    fecha               timestamp     NOT NULL,
    comentario_padre_id bigint
);
ALTER TABLE goc_p_orden_dia_comentarios
    ADD PRIMARY KEY (id);

CREATE TABLE goc_p_orden_dia_descriptores
(
    id             bigint NOT NULL,
    id_clave       bigint,
    id_p_orden_dia bigint
);
ALTER TABLE goc_p_orden_dia_descriptores
    ADD PRIMARY KEY (id);

CREATE TABLE goc_p_orden_dia_documentos
(
    id               bigint        NOT NULL,
    punto_id         bigint        NOT NULL,
    descripcion      varchar(2048) NOT NULL,
    mime_type        varchar(512)  NOT NULL,
    nombre_fichero   varchar(512)  NOT NULL,
    fecha_adicion    timestamp     NOT NULL DEFAULT LOCALTIMESTAMP,
    datos            oid           NOT NULL,
    creador_id       bigint        NOT NULL,
    descripcion_alt  varchar(2048),
    publico          boolean,
    mostrar_en_ficha boolean                DEFAULT 'f',
    hash_string      varchar(250)
);
ALTER TABLE goc_p_orden_dia_documentos
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones
(
    id                          bigint       NOT NULL,
    asunto                      varchar(500) NOT NULL,
    fecha                       timestamp    NOT NULL,
    duracion                    bigint,
    descripcion                 text,
    creador_id                  bigint       NOT NULL,
    fecha_creacion              timestamp    NOT NULL,
    acuerdos                    text,
    fecha_completada            timestamp,
    ubicacion                   varchar(4000),
    numero_sesion               bigint,
    url_grabacion               varchar(4000),
    publica                     boolean,
    miembro_responsable_acta_id bigint,
    telematica                  boolean               DEFAULT 'f',
    telematica_descripcion      text,
    notificada                  boolean               DEFAULT 'f',
    creador_nombre              varchar(2000),
    creador_email               varchar(200),
    admite_suplencia            boolean               DEFAULT 'f',
    completada                  boolean               DEFAULT 'f',
    admite_comentarios          boolean               DEFAULT 'f',
    fecha_segunda_convocatoria  timestamp,
    asunto_alt                  varchar(500),
    descripcion_alt             text,
    acuerdos_alt                text,
    ubicacion_alt               varchar(4000),
    telematica_descripcion_alt  text,
    aviso_primera_reunion       boolean      NOT NULL DEFAULT 'f',
    admite_delegacion_voto      boolean      NOT NULL DEFAULT 'f',
    url_acta                    varchar(1000),
    url_acta_alt                varchar(1000),
    aviso_primera_reunion_user  varchar(100),
    aviso_primera_reunion_fecha timestamp,
    observaciones               text,
    extraordinaria              boolean,
    aviso_primera_reunion_email varchar(255),
    hora_fin                    varchar(5),
    convocatoria_comienzo       boolean,
    reabierta                   boolean               DEFAULT 'f',
    convocante                  varchar(2000),
    convocante_email            varchar(200),
    uuid                        varchar(250) NOT NULL DEFAULT uuid_generate_v4()
);
ALTER TABLE goc_reuniones
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones_comentarios
(
    id             bigint    NOT NULL,
    comentario     text      NOT NULL,
    reunion_id     bigint    NOT NULL,
    creador_id     bigint    NOT NULL,
    creador_nombre varchar(1000),
    fecha          timestamp NOT NULL DEFAULT LOCALTIMESTAMP
);
ALTER TABLE goc_reuniones_comentarios
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones_diligencias
(
    id                bigint NOT NULL,
    reunion_id        bigint,
    usuario_reabre    integer,
    motivo_reapertura varchar(4000),
    fecha_reapertura  timestamp
);
ALTER TABLE goc_reuniones_diligencias
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones_documentos
(
    id              bigint        NOT NULL,
    reunion_id      bigint        NOT NULL,
    descripcion     varchar(2048) NOT NULL,
    mime_type       varchar(512)  NOT NULL,
    nombre_fichero  varchar(512)  NOT NULL,
    fecha_adicion   timestamp     NOT NULL,
    datos           oid           NOT NULL,
    creador_id      bigint        NOT NULL,
    descripcion_alt varchar(2048),
    hash_string     varchar(250)
);
ALTER TABLE goc_reuniones_documentos
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones_externos
(
    id         bigint       NOT NULL,
    reunion_id bigint       NOT NULL,
    persona_id bigint       NOT NULL,
    nombre     varchar(500) NOT NULL,
    email      varchar(200) NOT NULL
);
CREATE UNIQUE INDEX uk_externos_email_r_id ON goc_reuniones_externos (email, reunion_id);
ALTER TABLE goc_reuniones_externos
    ADD UNIQUE (email, reunion_id);
ALTER TABLE goc_reuniones_externos
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones_hoja_firmas
(
    id             bigint       NOT NULL,
    reunion_id     bigint       NOT NULL,
    creador_id     bigint       NOT NULL,
    fecha_adicion  timestamp    NOT NULL,
    datos          oid          NOT NULL,
    nombre_fichero varchar(512) NOT NULL,
    mime_type      varchar(512) NOT NULL,
    hash_string    varchar(250)
);
ALTER TABLE goc_reuniones_hoja_firmas
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones_invitados
(
    id                 bigint        NOT NULL,
    reunion_id         bigint        NOT NULL,
    persona_id         bigint        NOT NULL,
    persona_nombre     varchar(1000) NOT NULL,
    url_asistencia     varchar(1000),
    url_asistencia_alt varchar(1000),
    persona_email      varchar(1000) NOT NULL,
    motivo_invitacion  text
);
CREATE INDEX uji_reuniones_invitados_reu_i ON goc_reuniones_invitados (reunion_id);
ALTER TABLE goc_reuniones_invitados
    ADD PRIMARY KEY (id);

CREATE TABLE goc_reuniones_puntos_orden_dia
(
    id                    bigint        NOT NULL,
    titulo                varchar(4000) NOT NULL,
    descripcion           text,
    orden                 bigint        NOT NULL,
    reunion_id            bigint        NOT NULL,
    acuerdos              text,
    deliberaciones        text,
    publico               boolean DEFAULT 'f',
    titulo_alt            varchar(4000),
    descripcion_alt       text,
    acuerdos_alt          text,
    deliberaciones_alt    text,
    url_acta              varchar(1000),
    url_acta_alt          varchar(1000),
    id_punto_superior     bigint,
    url_acta_anterior     varchar(255),
    url_acta_anterior_alt varchar(255),
    hora_grabacion        varchar(8),
    editado_en_reapertura boolean DEFAULT 'f'
);
ALTER TABLE goc_reuniones_puntos_orden_dia
    ADD PRIMARY KEY (id);

CREATE TABLE goc_tipos_colectivo
(
    id     bigint       NOT NULL,
    codigo varchar(10)  NOT NULL,
    nombre varchar(100) NOT NULL
);
ALTER TABLE goc_tipos_colectivo
    ADD PRIMARY KEY (id);

CREATE TABLE goc_tipos_organo
(
    id         bigint       NOT NULL,
    codigo     varchar(10)  NOT NULL,
    nombre     varchar(400) NOT NULL,
    nombre_alt varchar(400)
);

ALTER TABLE goc_tipos_organo
    ADD PRIMARY KEY (id);
ALTER TABLE goc_p_orden_dia_documentos
    ADD CONSTRAINT fk_ordendia_documentos_reunion FOREIGN KEY (punto_id) REFERENCES goc_reuniones_puntos_orden_dia (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_organos
    ADD CONSTRAINT goc_org_tipos_fk FOREIGN KEY (tipo_organo_id) REFERENCES goc_tipos_organo (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_reuniones_comentarios
    ADD CONSTRAINT fk_comentarios_reuniones FOREIGN KEY (reunion_id) REFERENCES goc_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_claves
    ADD CONSTRAINT fk_clave_desc FOREIGN KEY (id_descriptor) REFERENCES goc_descriptores (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_p_orden_dia_comentarios
    ADD CONSTRAINT fk_punto_coment FOREIGN KEY (punto_orden_dia_id) REFERENCES goc_reuniones_puntos_orden_dia (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE goc_p_orden_dia_comentarios
    ADD CONSTRAINT fk_coment_coment FOREIGN KEY (comentario_padre_id) REFERENCES goc_p_orden_dia_comentarios (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_organos_reuniones_invits
    ADD CONSTRAINT goc_organos_reuniones_invi_r01 FOREIGN KEY (organo_reunion_id) REFERENCES goc_organos_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_reuniones_puntos_orden_dia
    ADD CONSTRAINT fk_ordendia_reunion FOREIGN KEY (reunion_id) REFERENCES goc_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE goc_reuniones_puntos_orden_dia
    ADD CONSTRAINT fk_punto_superior FOREIGN KEY (id_punto_superior) REFERENCES goc_reuniones_puntos_orden_dia (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_organos_reuniones
    ADD CONSTRAINT goc_org_reu_reuniones_fk FOREIGN KEY (reunion_id) REFERENCES goc_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_p_orden_dia_acuerdos
    ADD CONSTRAINT fk_ordendia_acuerdos_reunion FOREIGN KEY (punto_id) REFERENCES goc_reuniones_puntos_orden_dia (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_personas_roles
    ADD CONSTRAINT personas_roles_fk FOREIGN KEY (id_persona) REFERENCES goc_personas (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_envios_oficio
    ADD CONSTRAINT goc_oficio_punto_fk FOREIGN KEY (punto_orden_dia) REFERENCES goc_reuniones_puntos_orden_dia (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_reuniones_diligencias
    ADD CONSTRAINT diligencias_fk_reuniones FOREIGN KEY (reunion_id) REFERENCES goc_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_organos_reuniones_miembros
    ADD CONSTRAINT fk_miembros_organos_reuniones FOREIGN KEY (organo_reunion_id) REFERENCES goc_organos_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_reuniones_hoja_firmas
    ADD CONSTRAINT fk_hoja_reunion FOREIGN KEY (reunion_id) REFERENCES goc_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_reuniones_invitados
    ADD CONSTRAINT goc_reuniones_invitados_r01 FOREIGN KEY (reunion_id) REFERENCES goc_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_colectivos
    ADD CONSTRAINT goc_col_tipos_fk FOREIGN KEY (tipo_colectivo_id) REFERENCES goc_tipos_colectivo (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_descriptores_tipos_organo
    ADD CONSTRAINT goc_descriptores_tipos_org_de FOREIGN KEY (descriptor_id) REFERENCES goc_descriptores (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE goc_descriptores_tipos_organo
    ADD CONSTRAINT goc_descriptores_tipos_org_to FOREIGN KEY (tipo_organo_id) REFERENCES goc_tipos_organo (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_reuniones_documentos
    ADD CONSTRAINT fk_documentos_reuniones FOREIGN KEY (reunion_id) REFERENCES goc_reuniones (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_p_orden_dia_descriptores
    ADD CONSTRAINT fk_ord_ord FOREIGN KEY (id_p_orden_dia) REFERENCES goc_reuniones_puntos_orden_dia (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE goc_p_orden_dia_descriptores
    ADD CONSTRAINT fk_ord_cla FOREIGN KEY (id_clave) REFERENCES goc_claves (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE goc_miembros
    ADD CONSTRAINT fk_miembros_cargos FOREIGN KEY (cargo_id) REFERENCES goc_cargos (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE goc_miembros
    ADD CONSTRAINT goc_miembros_organos_fk FOREIGN KEY (organo_id) REFERENCES goc_organos (id) ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
CREATE OR REPLACE FUNCTION quita_acentos(pTxt text) RETURNS varchar AS
$body$
DECLARE

    vResult varchar(4000);

BEGIN

    IF coalesce(pTxt::text, '') = ''
    THEN

        RETURN NULL;

    ELSE

        RETURN translate(lower(pTxt), 'áéíóúàèìòùäëïöüâêîôû', 'aeiouaeiouaeiouaeiou');

    END IF;
END;

$body$
    LANGUAGE PLPGSQL
;
-- REVOKE ALL ON FUNCTION quita_acentos ( pTxt text ) FROM PUBLIC;


CREATE OR REPLACE FUNCTION quita_acentos_clob(pTxt text) RETURNS text AS
$body$
DECLARE

    vResult   text;
    l_offset  integer := 1;
    l_segment varchar(4000);

BEGIN

    IF coalesce(pTxt::text, '') = ''
    THEN

        RETURN NULL;

    ELSE

        WHILE l_offset < LENGTH(pTxt)
            LOOP

                l_segment := SUBSTR(pTxt, 3000, l_offset);

                l_segment := quita_acentos(l_segment);

                vResult := vResult || l_segment;

                l_offset := l_offset + 3000;

            END LOOP;

        RETURN vResult;

    END IF;
END;

$body$
    LANGUAGE PLPGSQL
;
-- REVOKE ALL ON FUNCTION quita_acentos_clob ( pTxt text ) FROM PUBLIC;

CREATE OR REPLACE VIEW goc_vw_personas_habilitadas (id, login, nombre, email) AS
SELECT p.ID, p.LOGIN, p.NOMBRE, p.EMAIL
FROM GOC_PERSONAS p
WHERE HABILITADA is True;

CREATE OR REPLACE VIEW goc_vw_reuniones_busqueda
            (id, reunion_id, asunto_reunion, descripcion_reunion, asunto_alt_reunion, descripcion_alt_reunion,
             titulo_punto, descripcion_punto, acuerdos_punto, deliberaciones_punto, titulo_alt_punto,
             descripcion_alt_punto, acuerdos_alt_punto, deliberaciones_alt_punto, asunto_reunion_busq,
             descripcion_reunion_busq, asunto_alt_reunion_busq, descripcion_alt_reunion_busq, titulo_punto_busq,
             descripcion_punto_busq, acuerdos_punto_busq, deliberaciones_punto_busq, titulo_alt_punto_busq,
             descripcion_alt_punto_busq, acuerdos_alt_punto_busq, deliberaciones_alt_punto_busq)
AS
SELECT r.id || '-' || p.id                   id,
       r.id                                  reunion_id,
       r.asunto                              asunto_reunion,
       r.descripcion                         descripcion_reunion,
       r.asunto_alt                          asunto_alt_reunion,
       r.descripcion_alt                     descripcion_alt_reunion,
       p.titulo                              titulo_punto,
       p.descripcion                         descripcion_punto,
       p.acuerdos                            acuerdos_punto,
       p.deliberaciones                      deliberaciones_punto,
       p.titulo_alt                          titulo_alt_punto,
       p.descripcion_alt                     descripcion_alt_punto,
       p.acuerdos_alt                        acuerdos_alt_punto,
       p.deliberaciones_alt                  deliberaciones_alt_punto,
       quita_acentos(r.asunto)               asunto_reunion_busq,
       quita_acentos_clob(r.descripcion)     descripcion_reunion_busq,
       quita_acentos(r.asunto_alt)           asunto_alt_reunion_busq,
       quita_acentos_clob(r.descripcion_alt) descripcion_alt_reunion_busq,
       quita_acentos(p.titulo)               titulo_punto_busq,
       quita_acentos_clob(p.descripcion)     descripcion_punto_busq,
       quita_acentos_clob(p.acuerdos)        acuerdos_punto_busq,
       quita_acentos_clob(p.deliberaciones)  deliberaciones_punto_busq,
       quita_acentos(p.titulo_alt)           titulo_alt_punto_busq,
       quita_acentos_clob(p.descripcion_alt) descripcion_alt_punto_busq,
       quita_acentos_clob(p.acuerdos_alt)    acuerdos_alt_punto_busq,
       quita_acentos_clob(p.deliberaciones_alt)
                                             deliberaciones_alt_punto_busq
FROM goc_reuniones r
         LEFT OUTER JOIN goc_reuniones_puntos_orden_dia p ON (r.id = p.reunion_id);

CREATE OR REPLACE VIEW goc_vw_reu_pt_or_dia
            (id, titulo, titulo_alt, descripcion, descripcion_alt, orden, reunion_id, acuerdos, acuerdos_alt,
             deliberaciones, deliberaciones_alt, publico, url_acta, url_acta_alt, url_acta_anterior,
             url_acta_anterior_alt, editado_en_reapertura, id_punto_superior, profundidad, orden_nivel_zero)
AS
WITH ca
         AS (WITH RECURSIVE cte AS (
        SELECT ID,
               TITULO,
               TITULO_ALT,
               DESCRIPCION,
               DESCRIPCION_ALT,
               ORDEN,
               REUNION_ID,
               ACUERDOS,
               ACUERDOS_ALT,
               DELIBERACIONES,
               DELIBERACIONES_ALT,
               PUBLICO,
               URL_ACTA,
               URL_ACTA_ALT,
               URL_ACTA_ANTERIOR,
               URL_ACTA_ANTERIOR_ALT,
               EDITADO_EN_REAPERTURA,
               ID_PUNTO_SUPERIOR,
               CAST(1 as bigint) AS PROFUNDIDAD,
               orden             AS ORDEN_NIVEL_ZERO
        FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
        WHERE id IN (SELECT id
                     FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                     WHERE coalesce(ID_PUNTO_SUPERIOR::text, '') = '')
        UNION ALL
        SELECT GOC_REUNIONES_PUNTOS_ORDEN_DIA.ID,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.TITULO,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.TITULO_ALT,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.DESCRIPCION,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.DESCRIPCION_ALT,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.ORDEN,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.REUNION_ID,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.ACUERDOS,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.ACUERDOS_ALT,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.DELIBERACIONES,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.DELIBERACIONES_ALT,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.PUBLICO,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.URL_ACTA,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.URL_ACTA_ALT,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.URL_ACTA_ANTERIOR,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.URL_ACTA_ANTERIOR_ALT,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.EDITADO_EN_REAPERTURA,
               GOC_REUNIONES_PUNTOS_ORDEN_DIA.ID_PUNTO_SUPERIOR,
               ROW_NUMBER() over () as PROFUNDIDAD,
               c.ORDEN_NIVEL_ZERO
        FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                 JOIN cte c ON (c.id
            = GOC_REUNIONES_PUNTOS_ORDEN_DIA.ID_PUNTO_SUPERIOR)
    )
             SELECT *
             FROM cte
    )
SELECT ID,
       TITULO,
       TITULO_ALT,
       DESCRIPCION,
       DESCRIPCION_ALT,
       ORDEN,
       REUNION_ID,
       ACUERDOS,
       ACUERDOS_ALT,
       DELIBERACIONES,
       DELIBERACIONES_ALT,
       PUBLICO,
       URL_ACTA,
       URL_ACTA_ALT,
       URL_ACTA_ANTERIOR,
       URL_ACTA_ANTERIOR_ALT,
       EDITADO_EN_REAPERTURA,
       ID_PUNTO_SUPERIOR,
       PROFUNDIDAD,
       ORDEN_NIVEL_ZERO
FROM ca
ORDER BY ORDEN_NIVEL_ZERO asc;

CREATE OR REPLACE VIEW goc_vw_reuniones_editores
            (id, uuid, asunto, asunto_alt, fecha, duracion, num_documentos, editor_id, completada, externo, organo_id,
             tipo_organo_id, aviso_primera_reunion, aviso_primera_reunion_user, aviso_primera_reunion_fecha, url_acta,
             url_acta_alt)
AS
SELECT r.id,
       r.uuid,
       r.asunto,
       r.asunto_alt,
       r.fecha,
       r.duracion,
       (SELECT COUNT(*)
        FROM goc_reuniones_documentos rd
        WHERE rd.reunion_id = r.id)
                    num_documentos,
       r.creador_id editor_id,
       r.completada completada,
       o.externo,
       o.organo_id,
       o.tipo_organo_id,
       r.aviso_primera_reunion,
       r.aviso_primera_reunion_user,
       r.aviso_primera_reunion_fecha,
       r.url_acta,
       r.url_acta_alt
FROM goc_reuniones r
         LEFT OUTER JOIN goc_organos_reuniones o ON (r.id = o.reunion_id)
UNION

SELECT r.id,
       r.uuid,
       r.asunto,
       r.asunto_alt,
       r.fecha,
       r.duracion,
       (SELECT COUNT(*)
        FROM goc_reuniones_documentos rd
        WHERE rd.reunion_id = r.id)
                    num_documentos,
       a.persona_id editor_id,
       r.completada completada,
       o.externo,
       o.organo_id,
       o.tipo_organo_id,
       r.aviso_primera_reunion,
       r.aviso_primera_reunion_user,
       r.aviso_primera_reunion_fecha,
       r.url_acta,
       r.url_acta_alt
FROM goc_reuniones r,
     goc_organos_reuniones o,
     goc_organos_autorizados a
WHERE r.id = o.reunion_id
  AND o.organo_id = a.organo_id;

CREATE OR REPLACE VIEW goc_vw_certificados_asistencia (reunion_id, persona_id, url_asistencia, url_asistencia_alt) AS
SELECT ri.reunion_id,
       ri.persona_id,
       ri.url_asistencia,
       ri.url_asistencia_alt
FROM goc_reuniones_invitados ri
WHERE (ri.url_asistencia IS NOT NULL AND ri.url_asistencia::text <> '')

UNION

SELECT roi.reunion_id,
       roi.persona_id,
       roi.url_asistencia,
       roi.url_asistencia_alt
FROM goc_organos_reuniones_invits roi
WHERE (roi.url_asistencia IS NOT NULL AND roi.url_asistencia::text <> '')

UNION

SELECT r.id             reunion_id,
       orrm.suplente_id persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia is True
  AND (suplente_id IS NOT NULL AND suplente_id::text <> '')
  AND (orrm.url_asistencia IS NOT NULL AND orrm.url_asistencia::text <> '')

UNION

SELECT r.id            reunion_id,
       orrm.miembro_id persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia is True
  AND coalesce(suplente_id::text, '') = ''
  AND (orrm.url_asistencia IS NOT NULL AND orrm.url_asistencia::text <> '')
;

CREATE OR REPLACE VIEW goc_vw_reuniones_permisos
            (id, uuid, completada, fecha, asunto, asunto_alt, persona_id, persona_nombre, asistente, url_acta,
             url_acta_alt, url_asistencia, url_asistencia_alt, aviso_primera_reunion)
AS
SELECT s.id,
       s.uuid,
       s.completada,
       s.fecha,
       s.asunto,
       s.asunto_alt,
       s.persona_id,
       s.persona_nombre,
       CASE WHEN SUM(s.asistente) > 0 THEN True ELSE False END asistente,
       s.url_acta,
       s.url_acta_alt,
       (SELECT a.url_asistencia
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
        LIMIT 1)
                                                               url_asistencia,
       (SELECT a.url_asistencia_alt
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
        LIMIT 1)
                                                               url_asistencia_alt,
       s.aviso_primera_reunion
FROM (SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.creador_id            persona_id,
             r.creador_nombre        persona_nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r

      UNION

      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ra.persona_id           persona_id,
             ra.persona_nombre       persona_nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_autorizados ra
      WHERE r.id = orr.reunion_id
        AND orr.organo_id = ra.organo_id

      UNION

      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ri.persona_id           persona_id,
             ri.persona_nombre       persona_nombre,
             1                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_invitados ri
      WHERE ri.reunion_id = r.id

      UNION

      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ori.persona_id                                                                        persona_id,
             ori.nombre                                                                            persona_nombre,
             CASE WHEN ori.solo_consulta is False THEN 1 WHEN ori.solo_consulta is True THEN 0 END asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION                                                               aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_invits ori
      WHERE r.id = orr.reunion_id
        AND ori.organo_reunion_id = orr.id

      UNION

      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.suplente_id        persona_id,
             orrm.suplente_nombre    persona_nombre,
             1                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia is True
        AND (suplente_id IS NOT NULL AND suplente_id::text <> '')

      UNION

      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id                                                  persona_id,
             orrm.nombre                                                      persona_nombre,
             CASE WHEN coalesce(suplente_id::text, '') = '' THEN 1 ELSE 0 END asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION                                          aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia is True

      UNION

      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id         persona_id,
             orrm.nombre             persona_nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia = False

      UNION

      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             e.persona_id,
             e.nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_externos e
      WHERE r.id = e.reunion_id
     ) s
GROUP BY s.id,
         s.uuid,
         s.completada,
         s.fecha,
         s.asunto,
         s.asunto_alt,
         s.persona_id,
         s.persona_nombre,
         s.url_acta,
         s.url_acta_alt,
         s.aviso_primera_reunion;


DROP TRIGGER IF EXISTS autorizado_after_insert ON goc_organos_autorizados CASCADE;
CREATE OR REPLACE FUNCTION trigger_fct_autorizado_after_insert() RETURNS trigger AS
$BODY$
DECLARE
    n_perid bigint;
BEGIN
    BEGIN

        BEGIN
            SELECT ID
            INTO STRICT n_perid
            FROM GOC_PERSONAS
            WHERE ID = NEW.PERSONA_ID;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                n_perid := NULL;
        END;

        IF coalesce(n_perid::text, '') = ''
        THEN
            INSERT INTO GOC_PERSONAS(ID, LOGIN, NOMBRE, EMAIL)
            VALUES (NEW.PERSONA_ID, NEW.PERSONA_EMAIL, NEW.PERSONA_NOMBRE, NEW.PERSONA_EMAIL);
        END IF;
    END;
    RETURN NEW;
END
$BODY$
    LANGUAGE 'plpgsql';

CREATE TRIGGER autorizado_after_insert
    AFTER INSERT
    ON goc_organos_autorizados
    FOR EACH ROW
EXECUTE PROCEDURE trigger_fct_autorizado_after_insert();

DROP TRIGGER IF EXISTS miembro_after_insert ON goc_miembros CASCADE;
CREATE OR REPLACE FUNCTION trigger_fct_miembro_after_insert() RETURNS trigger AS
$BODY$
DECLARE
    n_perid bigint;
BEGIN
    BEGIN

        BEGIN
            SELECT ID
            INTO STRICT n_perid
            FROM GOC_PERSONAS
            WHERE ID = NEW.PERSONA_ID;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                n_perid := NULL;
        END;

        IF coalesce(n_perid::text, '') = ''
        THEN
            INSERT INTO GOC_PERSONAS(ID, LOGIN, NOMBRE, EMAIL)
            VALUES (NEW.PERSONA_ID, NEW.EMAIL, NEW.NOMBRE, NEW.EMAIL);
        END IF;
    END;
    RETURN NEW;
END
$BODY$
    LANGUAGE 'plpgsql';

CREATE TRIGGER miembro_after_insert
    AFTER INSERT
    ON goc_miembros
    FOR EACH ROW
EXECUTE PROCEDURE trigger_fct_miembro_after_insert();

DROP TRIGGER IF EXISTS org_invita_after_insert ON goc_organos_invitados CASCADE;
CREATE OR REPLACE FUNCTION trigger_fct_org_invita_after_insert() RETURNS trigger AS
$BODY$
DECLARE
    n_perid bigint;
BEGIN
    BEGIN

        BEGIN
            SELECT ID
            INTO STRICT n_perid
            FROM GOC_PERSONAS
            WHERE ID = NEW.PERSONA_ID;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                n_perid := NULL;
        END;

        IF coalesce(n_perid::text, '') = ''
        THEN
            INSERT INTO GOC_PERSONAS(ID, LOGIN, NOMBRE, EMAIL)
            VALUES (NEW.PERSONA_ID, NEW.PERSONA_EMAIL, NEW.PERSONA_NOMBRE, NEW.PERSONA_EMAIL);
        END IF;
    END;
    RETURN NEW;
END
$BODY$
    LANGUAGE 'plpgsql';

CREATE TRIGGER org_invita_after_insert
    AFTER INSERT
    ON goc_organos_invitados
    FOR EACH ROW
EXECUTE PROCEDURE trigger_fct_org_invita_after_insert();

DROP TRIGGER IF EXISTS org_reun_invi_after_insert ON goc_organos_reuniones_invits CASCADE;
CREATE OR REPLACE FUNCTION trigger_fct_org_reun_invi_after_insert() RETURNS trigger AS
$BODY$
DECLARE
    n_perid bigint;
BEGIN
    BEGIN

        BEGIN
            SELECT ID
            INTO STRICT n_perid
            FROM GOC_PERSONAS
            WHERE ID = NEW.PERSONA_ID;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                n_perid := NULL;
        END;

        IF coalesce(n_perid::text, '') = ''
        THEN
            INSERT INTO GOC_PERSONAS(ID, LOGIN, NOMBRE, EMAIL)
            VALUES (NEW.PERSONA_ID, NEW.EMAIL, NEW.NOMBRE, NEW.EMAIL);
        END IF;
    END;
    RETURN NEW;
END
$BODY$
    LANGUAGE 'plpgsql';

CREATE TRIGGER org_reun_invi_after_insert
    AFTER INSERT
    ON goc_organos_reuniones_invits
    FOR EACH ROW
EXECUTE PROCEDURE trigger_fct_org_reun_invi_after_insert();

DROP TRIGGER IF EXISTS reun_invi_after_insert ON goc_reuniones_invitados CASCADE;
CREATE OR REPLACE FUNCTION trigger_fct_reun_invi_after_insert() RETURNS trigger AS
$BODY$
DECLARE
    n_perid bigint;
BEGIN
    BEGIN

        BEGIN
            SELECT ID
            INTO STRICT n_perid
            FROM GOC_PERSONAS
            WHERE ID = NEW.PERSONA_ID;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                n_perid := NULL;
        END;

        IF coalesce(n_perid::text, '') = ''
        THEN
            INSERT INTO GOC_PERSONAS(ID, LOGIN, NOMBRE, EMAIL)
            VALUES (NEW.PERSONA_ID, NEW.PERSONA_EMAIL, NEW.PERSONA_NOMBRE, NEW.PERSONA_EMAIL);
        END IF;
    END;
    RETURN NEW;
END
$BODY$
    LANGUAGE 'plpgsql';

CREATE TRIGGER reun_invi_after_insert
    AFTER INSERT
    ON goc_reuniones_invitados
    FOR EACH ROW
EXECUTE PROCEDURE trigger_fct_reun_invi_after_insert();
CREATE SEQUENCE hibernate_sequence INCREMENT 1 MINVALUE 1 NO MAXVALUE START 1 CACHE 20;
CREATE SEQUENCE seq_personas_tmp INCREMENT 1 MINVALUE 1 NO MAXVALUE START 1 CACHE 20;
