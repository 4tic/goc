alter table goc_miembros
    add preside_votacion boolean default false;

alter table public.goc_organos_reuniones_miembros
    add preside_votacion boolean default false;