DROP VIEW GOC_VW_REU_PT_OR_DIA;

CREATE OR REPLACE VIEW GOC_VW_REU_PT_OR_DIA
            (
             ID,
             TITULO,
             TITULO_ALT,
             DESCRIPCION,
             DESCRIPCION_ALT,
             ORDEN,
             REUNION_ID,
             ACUERDOS,
             ACUERDOS_ALT,
             DELIBERACIONES,
             DELIBERACIONES_ALT,
             PUBLICO,
             URL_ACTA,
             URL_ACTA_ALT,
             URL_ACTA_ANTERIOR,
             URL_ACTA_ANTERIOR_ALT,
             EDITADO_EN_REAPERTURA,
             ID_PUNTO_SUPERIOR,
             VOTO_PUBLICO,
             PROFUNDIDAD,
             ORDEN_NIVEL_ZERO
                )
AS

WITH ca AS (
    WITH RECURSIVE cte AS (
        SELECT goc_reuniones_puntos_orden_dia.id,
               goc_reuniones_puntos_orden_dia.titulo,
               goc_reuniones_puntos_orden_dia.titulo_alt,
               goc_reuniones_puntos_orden_dia.descripcion,
               goc_reuniones_puntos_orden_dia.descripcion_alt,
               goc_reuniones_puntos_orden_dia.orden,
               goc_reuniones_puntos_orden_dia.reunion_id,
               goc_reuniones_puntos_orden_dia.acuerdos,
               goc_reuniones_puntos_orden_dia.acuerdos_alt,
               goc_reuniones_puntos_orden_dia.deliberaciones,
               goc_reuniones_puntos_orden_dia.deliberaciones_alt,
               goc_reuniones_puntos_orden_dia.publico,
               goc_reuniones_puntos_orden_dia.url_acta,
               goc_reuniones_puntos_orden_dia.url_acta_alt,
               goc_reuniones_puntos_orden_dia.url_acta_anterior,
               goc_reuniones_puntos_orden_dia.url_acta_anterior_alt,
               goc_reuniones_puntos_orden_dia.editado_en_reapertura,
               goc_reuniones_puntos_orden_dia.id_punto_superior,
               goc_reuniones_puntos_orden_dia.voto_publico,
               1::bigint AS profundidad,
               goc_reuniones_puntos_orden_dia.orden AS orden_nivel_zero
        FROM goc_reuniones_puntos_orden_dia
        WHERE (goc_reuniones_puntos_orden_dia.id IN ( SELECT goc_reuniones_puntos_orden_dia_1.id
                                                      FROM goc_reuniones_puntos_orden_dia goc_reuniones_puntos_orden_dia_1
                                                      WHERE COALESCE(goc_reuniones_puntos_orden_dia_1.id_punto_superior::text, ''::text) = ''::text))
        UNION ALL
        SELECT goc_reuniones_puntos_orden_dia.id,
               goc_reuniones_puntos_orden_dia.titulo,
               goc_reuniones_puntos_orden_dia.titulo_alt,
               goc_reuniones_puntos_orden_dia.descripcion,
               goc_reuniones_puntos_orden_dia.descripcion_alt,
               goc_reuniones_puntos_orden_dia.orden,
               goc_reuniones_puntos_orden_dia.reunion_id,
               goc_reuniones_puntos_orden_dia.acuerdos,
               goc_reuniones_puntos_orden_dia.acuerdos_alt,
               goc_reuniones_puntos_orden_dia.deliberaciones,
               goc_reuniones_puntos_orden_dia.deliberaciones_alt,
               goc_reuniones_puntos_orden_dia.publico,
               goc_reuniones_puntos_orden_dia.url_acta,
               goc_reuniones_puntos_orden_dia.url_acta_alt,
               goc_reuniones_puntos_orden_dia.url_acta_anterior,
               goc_reuniones_puntos_orden_dia.url_acta_anterior_alt,
               goc_reuniones_puntos_orden_dia.editado_en_reapertura,
               goc_reuniones_puntos_orden_dia.id_punto_superior,
               goc_reuniones_puntos_orden_dia.voto_publico,
               row_number() OVER () AS profundidad,
               c.orden_nivel_zero
        FROM goc_reuniones_puntos_orden_dia
                 JOIN cte c ON c.id = goc_reuniones_puntos_orden_dia.id_punto_superior
    )
    SELECT cte.id,
           cte.titulo,
           cte.titulo_alt,
           cte.descripcion,
           cte.descripcion_alt,
           cte.orden,
           cte.reunion_id,
           cte.acuerdos,
           cte.acuerdos_alt,
           cte.deliberaciones,
           cte.deliberaciones_alt,
           cte.publico,
           cte.url_acta,
           cte.url_acta_alt,
           cte.url_acta_anterior,
           cte.url_acta_anterior_alt,
           cte.editado_en_reapertura,
           cte.id_punto_superior,
           cte.voto_publico,
           cte.profundidad,
           cte.orden_nivel_zero
    FROM cte
)
SELECT ca.id,
       ca.titulo,
       ca.titulo_alt,
       ca.descripcion,
       ca.descripcion_alt,
       ca.orden,
       ca.reunion_id,
       ca.acuerdos,
       ca.acuerdos_alt,
       ca.deliberaciones,
       ca.deliberaciones_alt,
       ca.publico,
       ca.url_acta,
       ca.url_acta_alt,
       ca.url_acta_anterior,
       ca.url_acta_anterior_alt,
       ca.editado_en_reapertura,
       ca.id_punto_superior,
       ca.voto_publico,
       ca.profundidad,
       ca.orden_nivel_zero
FROM ca
ORDER BY ca.orden_nivel_zero;