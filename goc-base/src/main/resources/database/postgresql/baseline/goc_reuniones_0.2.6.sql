
create TABLE GOC_VOTOS (
    ID bigint,
    REUNION_ID varchar(20) not null,
    PUNTO_ID varchar(20) not null,
    PERSONA_ID varchar(20) not null,
    MIEMBRO_ID varchar(20) not null,
    NOMBRE_VOTANTE varchar(500),
    VOTO varchar(10)
);

ALTER TABLE goc_votos
    ADD PRIMARY KEY (id);