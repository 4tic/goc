CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create function quita_acentos(ptxt text) returns character varying
    language plpgsql
as
$$
DECLARE

    vResult varchar(4000);

BEGIN

    IF coalesce(pTxt::text, '') = ''
    THEN

        RETURN NULL;

    ELSE

        RETURN translate(lower(pTxt), 'áéíóúàèìòùäëïöüâêîôû', 'aeiouaeiouaeiouaeiou');

    END IF;
END;

$$;

create function quita_acentos_clob(ptxt text) returns text
    language plpgsql
as
$$
DECLARE

    vResult   text;
    l_offset  integer := 1;
    l_segment varchar(4000);

BEGIN

    IF coalesce(pTxt::text, '') = ''
    THEN

        RETURN NULL;

    ELSE

        WHILE l_offset < LENGTH(pTxt)
            LOOP

                l_segment := SUBSTR(pTxt, 3000, l_offset);

                l_segment := quita_acentos(l_segment);

                vResult := vResult || l_segment;

                l_offset := l_offset + 3000;

            END LOOP;

        RETURN vResult;

    END IF;
END;

$$;

create sequence hibernate_sequence
    cache 20;

create sequence seq_personas_tmp
    cache 20;

create table if not exists goc_auditoria
(
    id              bigint not null
        constraint goc_auditoria_pkey
            primary key,
    responsable     varchar(500),
    usuario_destino varchar(500),
    reunion_id      bigint,
    accion          varchar(1000),
    fecha_accion    timestamp
);

create table if not exists goc_cargos
(
    id               bigint      not null
        constraint goc_cargos_pkey
            primary key,
    nombre           varchar(400),
    nombre_alt       varchar(400),
    codigo           varchar(10) not null
        constraint goc_cargos_codigo_key
            unique,
    responsable_acta boolean,
    suplente         boolean
);

create table if not exists goc_descriptores
(
    id              bigint not null
        constraint goc_descriptores_pkey
            primary key,
    descriptor      varchar(100),
    descriptor_alt  varchar(100),
    descripcion     varchar(200),
    descripcion_alt varchar(200),
    estado          smallint
);

create table if not exists goc_claves
(
    id            bigint not null
        constraint goc_claves_pkey
            primary key,
    clave         varchar(100),
    clave_alt     varchar(100),
    estado        smallint,
    id_descriptor bigint
        constraint fk_clave_desc
            references goc_descriptores
);

create table if not exists goc_organos_autorizados
(
    id               bigint        not null
        constraint goc_organos_autorizados_pkey
            primary key,
    persona_id       bigint        not null,
    persona_nombre   varchar(1000) not null,
    organo_id        varchar(100)  not null,
    organo_externo   boolean       not null,
    persona_email    varchar(1000),
    subir_documentos boolean
);

create table if not exists goc_organos_invitados
(
    id              bigint                not null
        constraint goc_organos_invitados_pkey
            primary key,
    persona_id      bigint                not null,
    persona_nombre  varchar(1000)         not null,
    persona_email   varchar(1000)         not null,
    organo_id       varchar(100)          not null,
    solo_consulta   boolean default false not null,
    descripcion     varchar(255),
    descripcion_alt varchar(255),
    orden           bigint
);

create table if not exists goc_organos_logo
(
    organo_id     varchar(5) not null,
    externo       boolean    not null,
    imagen_base64 text,
    extension     varchar(20),
    constraint goc_organos_logo_pkey
        primary key (organo_id, externo)
);

create table if not exists goc_organos_parametros
(
    id_organo               varchar(5) not null,
    ordinario               boolean    not null,
    convocar_sin_orden_dia  boolean,
    email                   varchar(255),
    acta_provisional_activa boolean,
    constraint goc_organos_parametros_pkey
        primary key (id_organo, ordinario)
);

create table if not exists goc_persona_token
(
    id_persona bigint       not null,
    token      varchar(100) not null,
    constraint goc_persona_token_pkey
        primary key (id_persona, token)
);

create table if not exists goc_reuniones
(
    id                          bigint                                  not null
        constraint goc_reuniones_pkey
            primary key,
    asunto                      varchar(500)                            not null,
    fecha                       timestamp                               not null,
    duracion                    bigint,
    descripcion                 text,
    creador_id                  bigint                                  not null,
    fecha_creacion              timestamp                               not null,
    acuerdos                    text,
    fecha_completada            timestamp,
    ubicacion                   varchar(4000),
    numero_sesion               bigint,
    url_grabacion               varchar(4000),
    publica                     boolean,
    miembro_responsable_acta_id bigint,
    telematica                  boolean      default false,
    telematica_descripcion      text,
    notificada                  boolean      default false,
    creador_nombre              varchar(2000),
    creador_email               varchar(200),
    admite_suplencia            boolean      default false,
    completada                  boolean      default false,
    admite_comentarios          boolean      default false,
    fecha_segunda_convocatoria  timestamp,
    asunto_alt                  varchar(500),
    descripcion_alt             text,
    acuerdos_alt                text,
    ubicacion_alt               varchar(4000),
    telematica_descripcion_alt  text,
    aviso_primera_reunion       boolean      default false              not null,
    admite_delegacion_voto      boolean      default false              not null,
    url_acta                    varchar(1000),
    url_acta_alt                varchar(1000),
    aviso_primera_reunion_user  varchar(100),
    aviso_primera_reunion_fecha timestamp,
    observaciones               text,
    extraordinaria              boolean,
    aviso_primera_reunion_email varchar(255),
    hora_fin                    varchar(5),
    convocatoria_comienzo       boolean,
    reabierta                   boolean      default false,
    convocante                  varchar(2000),
    convocante_email            varchar(200),
    uuid                        varchar(250) default uuid_generate_v4() not null,
    has_votacion                boolean      default false
);

create table if not exists goc_organos_reuniones
(
    id                     bigint       not null
        constraint goc_organos_reuniones_pkey
            primary key,
    reunion_id             bigint       not null
        constraint goc_org_reu_reuniones_fk
            references goc_reuniones,
    organo_nombre          varchar(400),
    tipo_organo_id         bigint,
    externo                boolean,
    organo_id              varchar(100) not null,
    organo_nombre_alt      varchar(400),
    convocar_sin_orden_dia boolean
);

create table if not exists goc_organos_reuniones_invits
(
    id                 bigint,
    organo_reunion_id  bigint                not null
        constraint goc_organos_reuniones_invi_r01
            references goc_organos_reuniones,
    organo_externo     boolean               not null,
    reunion_id         bigint                not null,
    organo_id          varchar(400)          not null,
    nombre             varchar(500)          not null,
    email              varchar(200)          not null,
    persona_id         bigint                not null,
    url_asistencia     varchar(1000),
    url_asistencia_alt varchar(1000),
    solo_consulta      boolean default false not null,
    descripcion        varchar(255),
    descripcion_alt    varchar(255),
    orden              bigint
);

create table if not exists goc_organos_reuniones_miembros
(
    id                    bigint       not null
        constraint goc_organos_reuniones_miembros_pkey
            primary key,
    organo_reunion_id     bigint       not null
        constraint fk_miembros_organos_reuniones
            references goc_organos_reuniones,
    organo_externo        boolean      not null,
    reunion_id            bigint       not null,
    organo_id             varchar(400) not null,
    nombre                varchar(500) not null,
    email                 varchar(200) not null,
    asistencia            boolean,
    cargo_id              varchar(400),
    cargo_nombre          varchar(400),
    suplente_id           bigint,
    suplente_nombre       varchar(1000),
    suplente_email        varchar(200),
    miembro_id            bigint       not null,
    cargo_nombre_alt      varchar(400),
    delegado_voto_id      bigint,
    delegado_voto_nombre  varchar(1000),
    delegado_voto_email   varchar(200),
    cargo_codigo          varchar(10),
    url_asistencia        varchar(1000),
    url_asistencia_alt    varchar(1000),
    condicion             varchar(1000),
    condicion_alt         varchar(1000),
    justifica_ausencia    boolean,
    responsable_acta      boolean,
    descripcion           varchar(255),
    descripcion_alt       varchar(255),
    orden                 bigint,
    grupo                 varchar(255),
    orden_grupo           bigint,
    motivo_ausencia       varchar(255),
    secretario_designado  boolean,
    cargo_suplente        boolean,
    firmante              boolean default false,
    preside_votacion      boolean default false,
    asistencia_confirmada integer
);

create table if not exists goc_reuniones_comentarios
(
    id             bigint                           not null
        constraint goc_reuniones_comentarios_pkey
            primary key,
    comentario     text                             not null,
    reunion_id     bigint                           not null
        constraint fk_comentarios_reuniones
            references goc_reuniones,
    creador_id     bigint                           not null,
    creador_nombre varchar(1000),
    fecha          timestamp default LOCALTIMESTAMP not null
);

create table if not exists goc_reuniones_diligencias
(
    id                bigint not null
        constraint goc_reuniones_diligencias_pkey
            primary key,
    reunion_id        bigint
        constraint diligencias_fk_reuniones
            references goc_reuniones,
    usuario_reabre    integer,
    motivo_reapertura varchar(4000),
    fecha_reapertura  timestamp
);

create table if not exists goc_reuniones_documentos
(
    id              bigint        not null
        constraint goc_reuniones_documentos_pkey
            primary key,
    reunion_id      bigint        not null
        constraint fk_documentos_reuniones
            references goc_reuniones,
    descripcion     varchar(2048) not null,
    mime_type       varchar(512)  not null,
    nombre_fichero  varchar(512)  not null,
    fecha_adicion   timestamp     not null,
    datos           oid           not null,
    creador_id      bigint        not null,
    descripcion_alt varchar(2048),
    hash_string     varchar(250)
);

create table if not exists goc_reuniones_externos
(
    id         bigint       not null
        constraint goc_reuniones_externos_pkey
            primary key,
    reunion_id bigint       not null,
    persona_id bigint       not null,
    nombre     varchar(500) not null,
    email      varchar(200) not null,
    constraint goc_reuniones_externos_email_reunion_id_key
        unique (email, reunion_id)
);

create unique index if not exists uk_externos_email_r_id
    on goc_reuniones_externos (email, reunion_id);

create table if not exists goc_reuniones_hoja_firmas
(
    id             bigint       not null
        constraint goc_reuniones_hoja_firmas_pkey
            primary key,
    reunion_id     bigint       not null
        constraint fk_hoja_reunion
            references goc_reuniones,
    creador_id     bigint       not null,
    fecha_adicion  timestamp    not null,
    datos          oid          not null,
    nombre_fichero varchar(512) not null,
    mime_type      varchar(512) not null,
    hash_string    varchar(250)
);

create table if not exists goc_reuniones_invitados
(
    id                 bigint        not null
        constraint goc_reuniones_invitados_pkey
            primary key,
    reunion_id         bigint        not null
        constraint goc_reuniones_invitados_r01
            references goc_reuniones,
    persona_id         bigint        not null,
    persona_nombre     varchar(1000) not null,
    url_asistencia     varchar(1000),
    url_asistencia_alt varchar(1000),
    persona_email      varchar(1000) not null,
    motivo_invitacion  text
);

create index if not exists uji_reuniones_invitados_reu_i
    on goc_reuniones_invitados (reunion_id);

create table if not exists goc_reuniones_puntos_orden_dia
(
    id                      bigint        not null
        constraint goc_reuniones_puntos_orden_dia_pkey
            primary key,
    titulo                  varchar(4000) not null,
    descripcion             text,
    orden                   bigint        not null,
    reunion_id              bigint        not null
        constraint fk_ordendia_reunion
            references goc_reuniones,
    acuerdos                text,
    deliberaciones          text,
    publico                 boolean default false,
    titulo_alt              varchar(4000),
    descripcion_alt         text,
    acuerdos_alt            text,
    deliberaciones_alt      text,
    url_acta                varchar(1000),
    url_acta_alt            varchar(1000),
    id_punto_superior       bigint
        constraint fk_punto_superior
            references goc_reuniones_puntos_orden_dia
            on delete cascade,
    url_acta_anterior       varchar(255),
    url_acta_anterior_alt   varchar(255),
    hora_grabacion          varchar(8),
    editado_en_reapertura   boolean default false,
    fecha_apertura_votacion timestamp,
    voto_publico            boolean default false
);

create table if not exists goc_envios_oficio
(
    id              integer not null
        constraint goc_envios_oficio_pkey
            primary key,
    nombre          varchar(255),
    email           varchar(255),
    cargo           varchar(255),
    unidad          varchar(255),
    registro_salida varchar(255),
    fecha_envio     timestamp,
    punto_orden_dia integer
        constraint goc_oficio_punto_fk
            references goc_reuniones_puntos_orden_dia
);

create table if not exists goc_p_orden_dia_acuerdos
(
    id              bigint                           not null,
    punto_id        bigint                           not null
        constraint fk_ordendia_acuerdos_reunion
            references goc_reuniones_puntos_orden_dia,
    descripcion     varchar(2048)                    not null,
    mime_type       varchar(512)                     not null,
    nombre_fichero  varchar(512)                     not null,
    fecha_adicion   timestamp default LOCALTIMESTAMP not null,
    datos           oid                              not null,
    creador_id      bigint                           not null,
    descripcion_alt varchar(2048),
    publico         boolean   default true           not null,
    hash_string     varchar(250)
);

create table if not exists goc_p_orden_dia_comentarios
(
    id                  bigint        not null
        constraint goc_p_orden_dia_comentarios_pkey
            primary key,
    punto_orden_dia_id  bigint        not null
        constraint fk_punto_coment
            references goc_reuniones_puntos_orden_dia,
    comentario          text          not null,
    creador_id          bigint        not null,
    creador_nombre      varchar(1000) not null,
    fecha               timestamp     not null,
    comentario_padre_id bigint
        constraint fk_coment_coment
            references goc_p_orden_dia_comentarios
);

create table if not exists goc_p_orden_dia_descriptores
(
    id             bigint not null
        constraint goc_p_orden_dia_descriptores_pkey
            primary key,
    id_clave       bigint
        constraint fk_ord_cla
            references goc_claves,
    id_p_orden_dia bigint
        constraint fk_ord_ord
            references goc_reuniones_puntos_orden_dia
);

create table if not exists goc_p_orden_dia_documentos
(
    id               bigint                           not null
        constraint goc_p_orden_dia_documentos_pkey
            primary key,
    punto_id         bigint                           not null
        constraint fk_ordendia_documentos_reunion
            references goc_reuniones_puntos_orden_dia,
    descripcion      varchar(2048)                    not null,
    mime_type        varchar(512)                     not null,
    nombre_fichero   varchar(512)                     not null,
    fecha_adicion    timestamp default LOCALTIMESTAMP not null,
    datos            oid                              not null,
    creador_id       bigint                           not null,
    descripcion_alt  varchar(2048),
    publico          boolean,
    mostrar_en_ficha boolean   default false,
    hash_string      varchar(250)
);

create table if not exists goc_tipos_colectivo
(
    id     bigint       not null
        constraint goc_tipos_colectivo_pkey
            primary key,
    codigo varchar(10)  not null,
    nombre varchar(100) not null
);

create table if not exists goc_colectivos
(
    id                bigint       not null
        constraint goc_colectivos_pkey
            primary key,
    nombre            varchar(100) not null,
    tipo_colectivo_id bigint       not null
        constraint goc_col_tipos_fk
            references goc_tipos_colectivo
);

create table if not exists goc_tipos_organo
(
    id         bigint       not null
        constraint goc_tipos_organo_pkey
            primary key,
    codigo     varchar(10)  not null,
    nombre     varchar(400) not null,
    nombre_alt varchar(400)
);

create table if not exists goc_descriptores_tipos_organo
(
    id             bigint not null
        constraint goc_descriptores_tipos_organo_pkey
            primary key,
    descriptor_id  bigint not null
        constraint goc_descriptores_tipos_org_de
            references goc_descriptores,
    tipo_organo_id bigint not null
        constraint goc_descriptores_tipos_org_to
            references goc_tipos_organo
);

create index if not exists uji_descriptores_tip_org_de_i
    on goc_descriptores_tipos_organo (descriptor_id);

create index if not exists uji_descriptores_tip_org_to_i
    on goc_descriptores_tipos_organo (tipo_organo_id);

create table if not exists goc_organos
(
    id                     bigint       not null
        constraint goc_organos_pkey
            primary key,
    nombre                 varchar(100) not null,
    tipo_organo_id         bigint       not null
        constraint goc_org_tipos_fk
            references goc_tipos_organo,
    creador_id             bigint       not null,
    fecha_creacion         timestamp    not null,
    inactivo               boolean default false,
    nombre_alt             varchar(400),
    convocar_sin_orden_dia boolean,
    ordenado               boolean default false
);

create table if not exists goc_miembros
(
    id               bigint       not null
        constraint goc_miembros_pkey
            primary key,
    nombre           varchar(500) not null,
    email            varchar(200) not null,
    organo_id        bigint       not null
        constraint goc_miembros_organos_fk
            references goc_organos,
    cargo_id         bigint
        constraint fk_miembros_cargos
            references goc_cargos,
    persona_id       bigint       not null,
    responsable_acta boolean,
    descripcion      varchar(255),
    descripcion_alt  varchar(255),
    orden            bigint,
    grupo            varchar(255),
    orden_grupo      bigint,
    firmante         boolean default false,
    preside_votacion boolean default false,
    constraint goc_miembros_organo_id_cargo_id_persona_id_key
        unique (organo_id, cargo_id, persona_id)
);

create unique index if not exists goc_miembros_r01
    on goc_miembros (organo_id, cargo_id, persona_id);

create table if not exists goc_votos
(
    id                        bigint      not null
        constraint goc_votos_pkey
            primary key,
    reunion_id                varchar(20) not null,
    punto_id                  varchar(20) not null,
    persona_id                varchar(20) not null,
    miembro_id                varchar(20),
    nombre_votante            varchar(500),
    voto                      varchar(10),
    voto_en_nombre            varchar(500),
    voto_en_nombre_persona_id varchar(20)
);

create table if not exists goc_votantes_privados
(
    id                        varchar(64) not null
        constraint goc_votantes_privados_pkey
            primary key,
    reunion_id                varchar(20) not null,
    punto_id                  varchar(20) not null,
    persona_id                varchar(20) not null,
    fecha_voto                timestamp,
    nombre_votante            varchar(500),
    voto_en_nombre            varchar(500),
    voto_en_nombre_persona_id varchar(20)
);

create table if not exists goc_votos_privados
(
    id         varchar(64) not null
        constraint goc_votos_privados_pkey
            primary key,
    reunion_id varchar(20) not null,
    punto_id   varchar(20) not null,
    voto       varchar(10)
);

create view goc_vw_reuniones_busqueda
            (id, reunion_id, asunto_reunion, descripcion_reunion, asunto_alt_reunion, descripcion_alt_reunion,
             titulo_punto, descripcion_punto, acuerdos_punto, deliberaciones_punto, titulo_alt_punto,
             descripcion_alt_punto, acuerdos_alt_punto, deliberaciones_alt_punto, asunto_reunion_busq,
             descripcion_reunion_busq, asunto_alt_reunion_busq, descripcion_alt_reunion_busq, titulo_punto_busq,
             descripcion_punto_busq, acuerdos_punto_busq, deliberaciones_punto_busq, titulo_alt_punto_busq,
             descripcion_alt_punto_busq, acuerdos_alt_punto_busq, deliberaciones_alt_punto_busq)
as
SELECT (r.id || '-'::text) || p.id              AS id,
       r.id                                     AS reunion_id,
       r.asunto                                 AS asunto_reunion,
       r.descripcion                            AS descripcion_reunion,
       r.asunto_alt                             AS asunto_alt_reunion,
       r.descripcion_alt                        AS descripcion_alt_reunion,
       p.titulo                                 AS titulo_punto,
       p.descripcion                            AS descripcion_punto,
       p.acuerdos                               AS acuerdos_punto,
       p.deliberaciones                         AS deliberaciones_punto,
       p.titulo_alt                             AS titulo_alt_punto,
       p.descripcion_alt                        AS descripcion_alt_punto,
       p.acuerdos_alt                           AS acuerdos_alt_punto,
       p.deliberaciones_alt                     AS deliberaciones_alt_punto,
       quita_acentos(r.asunto::text)            AS asunto_reunion_busq,
       quita_acentos_clob(r.descripcion)        AS descripcion_reunion_busq,
       quita_acentos(r.asunto_alt::text)        AS asunto_alt_reunion_busq,
       quita_acentos_clob(r.descripcion_alt)    AS descripcion_alt_reunion_busq,
       quita_acentos(p.titulo::text)            AS titulo_punto_busq,
       quita_acentos_clob(p.descripcion)        AS descripcion_punto_busq,
       quita_acentos_clob(p.acuerdos)           AS acuerdos_punto_busq,
       quita_acentos_clob(p.deliberaciones)     AS deliberaciones_punto_busq,
       quita_acentos(p.titulo_alt::text)        AS titulo_alt_punto_busq,
       quita_acentos_clob(p.descripcion_alt)    AS descripcion_alt_punto_busq,
       quita_acentos_clob(p.acuerdos_alt)       AS acuerdos_alt_punto_busq,
       quita_acentos_clob(p.deliberaciones_alt) AS deliberaciones_alt_punto_busq
FROM goc_reuniones r
         LEFT JOIN goc_reuniones_puntos_orden_dia p ON r.id = p.reunion_id;

create view goc_vw_certificados_asistencia(reunion_id, persona_id, url_asistencia, url_asistencia_alt) as
SELECT ri.reunion_id,
       ri.persona_id,
       ri.url_asistencia,
       ri.url_asistencia_alt
FROM goc_reuniones_invitados ri
WHERE ri.url_asistencia IS NOT NULL
  AND ri.url_asistencia::text <> ''::text
UNION
SELECT roi.reunion_id,
       roi.persona_id,
       roi.url_asistencia,
       roi.url_asistencia_alt
FROM goc_organos_reuniones_invits roi
WHERE roi.url_asistencia IS NOT NULL
  AND roi.url_asistencia::text <> ''::text
UNION
SELECT r.id             AS reunion_id,
       orrm.suplente_id AS persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia IS TRUE
  AND orrm.suplente_id IS NOT NULL
  AND orrm.suplente_id::text <> ''::text
  AND orrm.url_asistencia IS NOT NULL
  AND orrm.url_asistencia::text <> ''::text
UNION
SELECT r.id            AS reunion_id,
       orrm.miembro_id AS persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia IS TRUE
  AND COALESCE(orrm.suplente_id::text, ''::text) = ''::text
  AND orrm.url_asistencia IS NOT NULL
  AND orrm.url_asistencia::text <> ''::text;

create view goc_vw_reuniones_permisos
            (id, uuid, completada, fecha, asunto, asunto_alt, persona_id, persona_nombre, asistente, url_acta,
             url_acta_alt, url_asistencia, url_asistencia_alt, aviso_primera_reunion)
as
SELECT s.id,
       s.uuid,
       s.completada,
       s.fecha,
       s.asunto,
       s.asunto_alt,
       s.persona_id,
       s.persona_nombre,
       CASE
           WHEN sum(s.asistente) > 0 THEN true
           ELSE false
           END   AS asistente,
       s.url_acta,
       s.url_acta_alt,
       (SELECT a.url_asistencia
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
        LIMIT 1) AS url_asistencia,
       (SELECT a.url_asistencia_alt
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
        LIMIT 1) AS url_asistencia_alt,
       s.aviso_primera_reunion
FROM (SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.creador_id     AS persona_id,
             r.creador_nombre AS persona_nombre,
             0                AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r
      UNION
      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ra.persona_id,
             ra.persona_nombre,
             0 AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_autorizados ra
      WHERE r.id = orr.reunion_id
        AND orr.organo_id::text = ra.organo_id::text
      UNION
      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ri.persona_id,
             ri.persona_nombre,
             1 AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_invitados ri
      WHERE ri.reunion_id = r.id
      UNION
      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ori.persona_id,
             ori.nombre AS persona_nombre,
             CASE
                 WHEN ori.solo_consulta IS FALSE THEN 1
                 WHEN ori.solo_consulta IS TRUE THEN 0
                 ELSE NULL::integer
                 END    AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_invits ori
      WHERE r.id = orr.reunion_id
        AND ori.organo_reunion_id = orr.id
      UNION
      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.suplente_id     AS persona_id,
             orrm.suplente_nombre AS persona_nombre,
             1                    AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia IS TRUE
        AND orrm.suplente_id IS NOT NULL
        AND orrm.suplente_id::text <> ''::text
      UNION
      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id AS persona_id,
             orrm.nombre     AS persona_nombre,
             CASE
                 WHEN COALESCE(orrm.suplente_id::text, ''::text) = ''::text THEN 1
                 ELSE 0
                 END         AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia IS TRUE
      UNION
      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id AS persona_id,
             orrm.nombre     AS persona_nombre,
             0               AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia = false
      UNION
      SELECT r.id,
             r.uuid,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             e.persona_id,
             e.nombre,
             0 AS asistente,
             r.url_acta,
             r.url_acta_alt,
             r.aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_externos e
      WHERE r.id = e.reunion_id) s
GROUP BY s.id, s.uuid, s.completada, s.fecha, s.asunto, s.asunto_alt, s.persona_id, s.persona_nombre, s.url_acta,
         s.url_acta_alt, s.aviso_primera_reunion;

create view goc_vw_reu_pt_or_dia
            (id, titulo, titulo_alt, descripcion, descripcion_alt, orden, reunion_id, acuerdos, acuerdos_alt,
             deliberaciones, deliberaciones_alt, publico, url_acta, url_acta_alt, url_acta_anterior,
             url_acta_anterior_alt, editado_en_reapertura, id_punto_superior, voto_publico, profundidad,
             orden_nivel_zero)
as
WITH ca AS (
    WITH RECURSIVE cte AS (
        SELECT goc_reuniones_puntos_orden_dia.id,
               goc_reuniones_puntos_orden_dia.titulo,
               goc_reuniones_puntos_orden_dia.titulo_alt,
               goc_reuniones_puntos_orden_dia.descripcion,
               goc_reuniones_puntos_orden_dia.descripcion_alt,
               goc_reuniones_puntos_orden_dia.orden,
               goc_reuniones_puntos_orden_dia.reunion_id,
               goc_reuniones_puntos_orden_dia.acuerdos,
               goc_reuniones_puntos_orden_dia.acuerdos_alt,
               goc_reuniones_puntos_orden_dia.deliberaciones,
               goc_reuniones_puntos_orden_dia.deliberaciones_alt,
               goc_reuniones_puntos_orden_dia.publico,
               goc_reuniones_puntos_orden_dia.url_acta,
               goc_reuniones_puntos_orden_dia.url_acta_alt,
               goc_reuniones_puntos_orden_dia.url_acta_anterior,
               goc_reuniones_puntos_orden_dia.url_acta_anterior_alt,
               goc_reuniones_puntos_orden_dia.editado_en_reapertura,
               goc_reuniones_puntos_orden_dia.id_punto_superior,
               goc_reuniones_puntos_orden_dia.voto_publico,
               1::bigint                            AS profundidad,
               goc_reuniones_puntos_orden_dia.orden AS orden_nivel_zero
        FROM goc_reuniones_puntos_orden_dia
        WHERE (goc_reuniones_puntos_orden_dia.id IN (SELECT goc_reuniones_puntos_orden_dia_1.id
                                                     FROM goc_reuniones_puntos_orden_dia goc_reuniones_puntos_orden_dia_1
                                                     WHERE COALESCE(
                                                                   goc_reuniones_puntos_orden_dia_1.id_punto_superior::text,
                                                                   ''::text) = ''::text))
        UNION ALL
        SELECT goc_reuniones_puntos_orden_dia.id,
               goc_reuniones_puntos_orden_dia.titulo,
               goc_reuniones_puntos_orden_dia.titulo_alt,
               goc_reuniones_puntos_orden_dia.descripcion,
               goc_reuniones_puntos_orden_dia.descripcion_alt,
               goc_reuniones_puntos_orden_dia.orden,
               goc_reuniones_puntos_orden_dia.reunion_id,
               goc_reuniones_puntos_orden_dia.acuerdos,
               goc_reuniones_puntos_orden_dia.acuerdos_alt,
               goc_reuniones_puntos_orden_dia.deliberaciones,
               goc_reuniones_puntos_orden_dia.deliberaciones_alt,
               goc_reuniones_puntos_orden_dia.publico,
               goc_reuniones_puntos_orden_dia.url_acta,
               goc_reuniones_puntos_orden_dia.url_acta_alt,
               goc_reuniones_puntos_orden_dia.url_acta_anterior,
               goc_reuniones_puntos_orden_dia.url_acta_anterior_alt,
               goc_reuniones_puntos_orden_dia.editado_en_reapertura,
               goc_reuniones_puntos_orden_dia.id_punto_superior,
               goc_reuniones_puntos_orden_dia.voto_publico,
               row_number() OVER () AS profundidad,
               c.orden_nivel_zero
        FROM goc_reuniones_puntos_orden_dia
                 JOIN cte c ON c.id = goc_reuniones_puntos_orden_dia.id_punto_superior
    )
    SELECT cte.id,
           cte.titulo,
           cte.titulo_alt,
           cte.descripcion,
           cte.descripcion_alt,
           cte.orden,
           cte.reunion_id,
           cte.acuerdos,
           cte.acuerdos_alt,
           cte.deliberaciones,
           cte.deliberaciones_alt,
           cte.publico,
           cte.url_acta,
           cte.url_acta_alt,
           cte.url_acta_anterior,
           cte.url_acta_anterior_alt,
           cte.editado_en_reapertura,
           cte.id_punto_superior,
           cte.voto_publico,
           cte.profundidad,
           cte.orden_nivel_zero
    FROM cte
)
SELECT ca.id,
       ca.titulo,
       ca.titulo_alt,
       ca.descripcion,
       ca.descripcion_alt,
       ca.orden,
       ca.reunion_id,
       ca.acuerdos,
       ca.acuerdos_alt,
       ca.deliberaciones,
       ca.deliberaciones_alt,
       ca.publico,
       ca.url_acta,
       ca.url_acta_alt,
       ca.url_acta_anterior,
       ca.url_acta_anterior_alt,
       ca.editado_en_reapertura,
       ca.id_punto_superior,
       ca.voto_publico,
       ca.profundidad,
       ca.orden_nivel_zero
FROM ca
ORDER BY ca.orden_nivel_zero;

create view goc_vw_resultados_todos_votos(reunion_id, punto_id, voto, numero_votos) as
SELECT t.reunion_id,
       t.punto_id,
       t.voto,
       t.numero_votos
FROM (SELECT goc_votos.reunion_id,
             goc_votos.punto_id,
             goc_votos.voto,
             count(goc_votos.voto) AS numero_votos
      FROM goc_votos
      GROUP BY goc_votos.voto, goc_votos.punto_id, goc_votos.reunion_id
      UNION
      SELECT goc_votos_privados.reunion_id,
             goc_votos_privados.punto_id,
             goc_votos_privados.voto,
             count(goc_votos_privados.voto) AS numero_votos
      FROM goc_votos_privados
      GROUP BY goc_votos_privados.voto, goc_votos_privados.punto_id, goc_votos_privados.reunion_id) t;

create view goc_vw_todos_votos
            (reunion_id, punto_id, nombre_votante, persona_id, voto_en_nombre, voto_en_nombre_persona_id,
             voto_publico) as
SELECT votos.reunion_id,
       votos.punto_id,
       votos.nombre_votante,
       votos.persona_id,
       votos.voto_en_nombre,
       votos.voto_en_nombre_persona_id,
       1 AS voto_publico
FROM goc_votos votos
UNION
SELECT votos.reunion_id,
       votos.punto_id,
       votos.nombre_votante,
       votos.persona_id,
       votos.voto_en_nombre,
       votos.voto_en_nombre_persona_id,
       0 AS voto_publico
FROM goc_votantes_privados votos;

create view goc_vw_reuniones_editores
            (id, asunto, asunto_alt, fecha, duracion, num_documentos, editor_id, completada, externo, organo_id,
             tipo_organo_id, aviso_primera_reunion, aviso_primera_reunion_user, aviso_primera_reunion_fecha, url_acta,
             url_acta_alt, has_votacion)
as
SELECT r.id,
       r.asunto,
       r.asunto_alt,
       r.fecha,
       r.duracion,
       (SELECT count(*) AS count
        FROM goc_reuniones_documentos rd
        WHERE rd.reunion_id = r.id) AS num_documentos,
       r.creador_id                 AS editor_id,
       r.completada,
       o.externo,
       o.organo_id,
       o.tipo_organo_id,
       r.aviso_primera_reunion,
       r.aviso_primera_reunion_user,
       r.aviso_primera_reunion_fecha,
       r.url_acta,
       r.url_acta_alt,
       r.has_votacion
FROM goc_reuniones r
         LEFT JOIN goc_organos_reuniones o ON r.id = o.reunion_id
UNION
SELECT r.id,
       r.asunto,
       r.asunto_alt,
       r.fecha,
       r.duracion,
       (SELECT count(*) AS count
        FROM goc_reuniones_documentos rd
        WHERE rd.reunion_id = r.id) AS num_documentos,
       a.persona_id                 AS editor_id,
       r.completada,
       o.externo,
       o.organo_id,
       o.tipo_organo_id,
       r.aviso_primera_reunion,
       r.aviso_primera_reunion_user,
       r.aviso_primera_reunion_fecha,
       r.url_acta,
       r.url_acta_alt,
       r.has_votacion
FROM goc_reuniones r,
     goc_organos_reuniones o,
     goc_organos_autorizados a
WHERE r.id = o.reunion_id
  AND o.organo_id::text = a.organo_id::text;