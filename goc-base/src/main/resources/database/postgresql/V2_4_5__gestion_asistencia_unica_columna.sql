alter table goc_organos_reuniones_miembros
    alter column asistencia drop default;
alter table goc_organos_reuniones_miembros
    alter column asistencia drop not null;

update goc_organos_reuniones_miembros
set asistencia = null
where asistencia_confirmada is null
  and asistencia is true;

alter table goc_organos_reuniones_miembros
    drop column asistencia_confirmada;