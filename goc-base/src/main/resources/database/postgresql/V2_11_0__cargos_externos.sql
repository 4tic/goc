create table if not exists goc_organos_cargos
(
    id        bigint  not null
        constraint goc_organos_cargos_pkey
            primary key,
    organo_id bigint  not null
        constraint fk_organos_cargos
            references goc_organos,
    cargo_id  varchar not null
);

ALTER TABLE GOC_MIEMBROS
    ADD CARGO_ID_NEW VARCHAR(50);

UPDATE GOC_MIEMBROS
SET CARGO_ID_NEW = CARGO_ID;

ALTER TABLE GOC_P_ORDEN_DIA_ACUERDOS
    ADD URL_ACTA VARCHAR(250);

ALTER TABLE GOC_P_ORDEN_DIA_ACUERDOS
    ADD URL_ACTA_ALT VARCHAR(250);