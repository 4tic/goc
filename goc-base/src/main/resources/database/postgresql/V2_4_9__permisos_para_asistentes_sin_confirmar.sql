DROP VIEW goc_vw_reuniones_permisos;

CREATE OR REPLACE VIEW goc_vw_reuniones_permisos
            (id, completada, fecha, asunto, asunto_alt, persona_id, persona_nombre, asistente, url_acta,
             url_acta_alt, url_asistencia, url_asistencia_alt, aviso_primera_reunion)
AS
SELECT s.id,
       s.completada,
       s.fecha,
       s.asunto,
       s.asunto_alt,
       s.persona_id,
       s.persona_nombre,
       CASE WHEN SUM(s.asistente) > 0 THEN True ELSE False END asistente,
       s.url_acta,
       s.url_acta_alt,
       (SELECT a.url_asistencia
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
        LIMIT 1)
                                                               url_asistencia,
       (SELECT a.url_asistencia_alt
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
        LIMIT 1)
                                                               url_asistencia_alt,
       s.aviso_primera_reunion
FROM (SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.creador_id            persona_id,
             r.creador_nombre        persona_nombre,
             0                   asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r

      UNION

      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ra.persona_id           persona_id,
             ra.persona_nombre       persona_nombre,
             0                   asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_autorizados ra
      WHERE r.id = orr.reunion_id
        AND orr.organo_id = ra.organo_id

      UNION

      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ri.persona_id           persona_id,
             ri.persona_nombre       persona_nombre,
             1                    asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_invitados ri
      WHERE ri.reunion_id = r.id

      UNION

      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ori.persona_id                                                                        persona_id,
             ori.nombre                                                                            persona_nombre,
             CASE WHEN ori.solo_consulta is False THEN 1 WHEN ori.solo_consulta is True THEN 0 END asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION                                                                      aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_invits ori
      WHERE r.id = orr.reunion_id
        AND ori.organo_reunion_id = orr.id

      UNION

      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.suplente_id        persona_id,
             orrm.suplente_nombre    persona_nombre,
             1                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia is True
        AND (suplente_id IS NOT NULL AND suplente_id::text <> '')

      UNION

      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id                                                  persona_id,
             orrm.nombre                                                      persona_nombre,
             CASE WHEN coalesce(suplente_id::text, '') = '' THEN 1 ELSE 0 END asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION                                                 aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia is True

      UNION

      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id         persona_id,
             orrm.nombre             persona_nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND (orrm.asistencia is null OR orrm.asistencia is False)

      UNION

      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             e.persona_id,
             e.nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_externos e
      WHERE r.id = e.reunion_id
     ) s
GROUP BY s.id,
         s.completada,
         s.fecha,
         s.asunto,
         s.asunto_alt,
         s.persona_id,
         s.persona_nombre,
         s.url_acta,
         s.url_acta_alt,
         s.aviso_primera_reunion;