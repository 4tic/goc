DROP TABLE GOC_VW_REUNIONES_EDITORES IF EXISTS;
DROP TABLE GOC_VW_REU_PT_OR_DIA IF EXISTS;
DROP TABLE GOC_VW_CERTIFICADOS_ASISTENCIA IF EXISTS;
DROP TABLE GOC_VW_REUNIONES_PERMISOS IF EXISTS;
DROP TABLE GOC_VW_RESULTADOS_TODOS_VOTOS IF EXISTS;
DROP TABLE GOC_VW_TODOS_VOTOS IF EXISTS;

CREATE
OR
REPLACE
FORCE VIEW GOC_VW_REU_PT_OR_DIA
    (
     ID,
     TITULO,
     TITULO_ALT,
     DESCRIPCION,
     DESCRIPCION_ALT,
     ORDEN,
     REUNION_ID,
     ACUERDOS,
     ACUERDOS_ALT,
     DELIBERACIONES,
     DELIBERACIONES_ALT,
     PUBLICO,
     URL_ACTA,
     URL_ACTA_ALT,
     URL_ACTA_ANTERIOR,
     URL_ACTA_ANTERIOR_ALT,
     EDITADO_EN_REAPERTURA,
     ID_PUNTO_SUPERIOR,
        VOTO_PUBLICO,
     PROFUNDIDAD,
     ORDEN_NIVEL_ZERO
        )
AS
WITH RECURSIVE ca(ID,
                  TITULO,
                  TITULO_ALT,
                  DESCRIPCION,
                  DESCRIPCION_ALT,
                  ORDEN,
                  REUNION_ID,
                  ACUERDOS,
                  ACUERDOS_ALT,
                  DELIBERACIONES,
                  DELIBERACIONES_ALT,
                  PUBLICO,
                  URL_ACTA,
                  URL_ACTA_ALT,
                  URL_ACTA_ANTERIOR,
                  URL_ACTA_ANTERIOR_ALT,
                  EDITADO_EN_REAPERTURA,
                  ID_PUNTO_SUPERIOR,
                  VOTO_PUBLICO,
                  PROFUNDIDAD,
                  ORDEN_NIVEL_ZERO)
                   AS
                   (SELECT ID,
                           TITULO,
                           TITULO_ALT,
                           DESCRIPCION,
                           DESCRIPCION_ALT,
                           ORDEN,
                           REUNION_ID,
                           ACUERDOS,
                           ACUERDOS_ALT,
                           DELIBERACIONES,
                           DELIBERACIONES_ALT,
                           PUBLICO,
                           URL_ACTA,
                           URL_ACTA_ALT,
                           URL_ACTA_ANTERIOR,
                           URL_ACTA_ANTERIOR_ALT,
                           EDITADO_EN_REAPERTURA,
                           ID_PUNTO_SUPERIOR,
                           VOTO_PUBLICO,
                           0 AS PROFUNDIDAD,
                           0 AS ORDEN_NIVEL_ZERO
                    FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                    WHERE id IN (SELECT id
                                 FROM GOC_REUNIONES_PUNTOS_ORDEN_DIA
                                 WHERE ID_PUNTO_SUPERIOR IS NULL)
                    UNION ALL
                    SELECT r.ID,
                           r.TITULO,
                           r.TITULO_ALT,
                           r.DESCRIPCION,
                           r.DESCRIPCION_ALT,
                           r.ORDEN,
                           r.REUNION_ID,
                           r.ACUERDOS,
                           r.ACUERDOS_ALT,
                           r.DELIBERACIONES,
                           r.DELIBERACIONES_ALT,
                           r.PUBLICO,
                           r.URL_ACTA,
                           r.URL_ACTA_ALT,
                           r.URL_ACTA_ANTERIOR,
                           r.URL_ACTA_ANTERIOR_ALT,
                           r.EDITADO_EN_REAPERTURA,
                           r.ID_PUNTO_SUPERIOR,
                           r.VOTO_PUBLICO,
                           PROFUNDIDAD + 1 AS PROFUNDIDAD,
                           0               AS ORDEN_NIVEL_ZERO
                    FROM ca
                             INNER JOIN GOC_REUNIONES_PUNTOS_ORDEN_DIA AS r
                    WHERE ca.id = r.ID_PUNTO_SUPERIOR
                   )
SELECT ID,
       TITULO,
       TITULO_ALT,
       DESCRIPCION,
       DESCRIPCION_ALT,
       ORDEN,
       REUNION_ID,
       ACUERDOS,
       ACUERDOS_ALT,
       DELIBERACIONES,
       DELIBERACIONES_ALT,
       PUBLICO,
       URL_ACTA,
       URL_ACTA_ALT,
       URL_ACTA_ANTERIOR,
       URL_ACTA_ANTERIOR_ALT,
       EDITADO_EN_REAPERTURA,
       ID_PUNTO_SUPERIOR,
       VOTO_PUBLICO,
       PROFUNDIDAD,
       ORDEN_NIVEL_ZERO
FROM ca
ORDER BY ORDEN_NIVEL_ZERO asc;

CREATE
OR
REPLACE
FORCE VIEW GOC_VW_REUNIONES_EDITORES
    (
     ID,
     ASUNTO,
     ASUNTO_ALT,
     FECHA,
     DURACION,
     NUM_DOCUMENTOS,
     EDITOR_ID,
     COMPLETADA,
     EXTERNO,
     ORGANO_ID,
     TIPO_ORGANO_ID,
     AVISO_PRIMERA_REUNION,
     AVISO_PRIMERA_REUNION_USER,
     AVISO_PRIMERA_REUNION_FECHA,
     URL_ACTA,
     URL_ACTA_ALT,
        HAS_VOTACION
        )
AS
SELECT r.id,
       r.asunto,
       r.asunto_alt,
       r.fecha,
       r.duracion,
       (SELECT COUNT(*)
        FROM goc_reuniones_documentos rd
        WHERE rd.reunion_id = r.id)
                    num_documentos,
       r.creador_id editor_id,
       r.completada completada,
       o.externo,
       o.organo_id,
       o.tipo_organo_id,
       r.aviso_primera_reunion,
       r.aviso_primera_reunion_user,
       r.aviso_primera_reunion_fecha,
       r.url_acta,
       r.url_acta_alt,
       r.HAS_VOTACION
FROM goc_reuniones r
         LEFT JOIN goc_organos_reuniones o on r.id = o.reunion_id or o.reunion_id is null
UNION
SELECT r.id,
       r.asunto,
       r.asunto_alt,
       r.fecha,
       r.duracion,
       (SELECT COUNT(*)
        FROM goc_reuniones_documentos rd
        WHERE rd.reunion_id = r.id)
                    num_documentos,
       a.persona_id editor_id,
       r.completada completada,
       o.externo,
       o.organo_id,
       o.tipo_organo_id,
       r.aviso_primera_reunion,
       r.aviso_primera_reunion_user,
       r.aviso_primera_reunion_fecha,
       r.url_acta,
       r.url_acta_alt,
       r.HAS_VOTACION
FROM goc_reuniones r,
     goc_organos_reuniones o,
     goc_organos_autorizados a
WHERE r.id = o.reunion_id
  AND o.organo_id = a.organo_id;

create view GOC_VW_CERTIFICADOS_ASISTENCIA as
SELECT ri.reunion_id,
       ri.persona_id,
       ri.url_asistencia,
       ri.url_asistencia_alt
FROM goc_reuniones_invitados ri
WHERE ri.url_asistencia IS NOT NULL
UNION
SELECT roi.reunion_id,
       roi.persona_id,
       roi.url_asistencia,
       roi.url_asistencia_alt
FROM goc_organos_reuniones_invits roi
WHERE roi.url_asistencia IS NOT NULL
UNION
SELECT r.id             reunion_id,
       orrm.suplente_id persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia = 1
  AND suplente_id IS NOT NULL
  AND orrm.url_asistencia IS NOT NULL
UNION
SELECT r.id            reunion_id,
       orrm.miembro_id persona_id,
       orrm.url_asistencia,
       orrm.url_asistencia_alt
FROM goc_reuniones r,
     goc_organos_reuniones orr,
     goc_organos_reuniones_miembros orrm
WHERE r.id = orr.reunion_id
  AND orr.id = orrm.organo_reunion_id
  AND orrm.asistencia = 1
  AND suplente_id IS NULL
  AND orrm.url_asistencia IS NOT NULL;

create view GOC_VW_REUNIONES_PERMISOS as
SELECT s.id,
       s.completada,
       s.fecha,
       s.asunto,
       s.asunto_alt,
       s.persona_id,
       s.persona_nombre,
       SUM(s.asistente) asistente,
       s.url_acta,
       s.url_acta_alt,
       (SELECT a.url_asistencia
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
          AND ROWNUM = 1)
                        url_asistencia,
       (SELECT a.url_asistencia_alt
        FROM goc_vw_certificados_asistencia a
        WHERE a.reunion_id = s.id
          AND a.persona_id = s.persona_id
          AND ROWNUM = 1)
                        url_asistencia_alt,
       s.aviso_primera_reunion
FROM (SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             r.creador_id            persona_id,
             r.creador_nombre        persona_nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r
      UNION
      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ra.persona_id           persona_id,
             ra.persona_nombre       persona_nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_autorizados ra
      WHERE r.id = orr.reunion_id
        AND orr.organo_id = ra.organo_id
      UNION
      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ri.persona_id           persona_id,
             ri.persona_nombre       persona_nombre,
             1                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_invitados ri
      WHERE ri.reunion_id = r.id
      UNION
      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             ori.persona_id                        persona_id,
             ori.nombre                            persona_nombre,
             decode(ori.solo_consulta, 0, 1, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION               aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_invits ori
      WHERE r.id = orr.reunion_id
        AND ori.organo_reunion_id = orr.id
      UNION
      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.suplente_id        persona_id,
             orrm.suplente_nombre    persona_nombre,
             1                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia = 1
        AND suplente_id IS NOT NULL
      UNION
      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id                 persona_id,
             orrm.nombre                     persona_nombre,
             DECODE(suplente_id, NULL, 1, 0) asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION         aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia = 1
      UNION
      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             orrm.miembro_id         persona_id,
             orrm.nombre             persona_nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_organos_reuniones orr,
           goc_organos_reuniones_miembros orrm
      WHERE r.id = orr.reunion_id
        AND orr.id = orrm.organo_reunion_id
        AND orrm.asistencia = 0
      UNION
      SELECT r.id,
             r.completada,
             r.fecha,
             r.asunto,
             r.asunto_alt,
             e.persona_id,
             e.nombre,
             0                       asistente,
             r.url_acta,
             r.url_acta_alt,
             r.AVISO_PRIMERA_REUNION aviso_primera_reunion
      FROM goc_reuniones r,
           goc_reuniones_externos e
      WHERE r.id = e.reunion_id
     ) s
GROUP BY s.id,
         s.completada,
         s.fecha,
         s.asunto,
         s.asunto_alt,
         s.persona_id,
         s.persona_nombre,
         s.url_acta,
         s.url_acta_alt,
         s.aviso_primera_reunion
;

CREATE OR REPLACE VIEW GOC_VW_RESULTADOS_TODOS_VOTOS AS
select REUNION_ID as REUNION_ID, PUNTO_ID as PUNTO_ID, VOTO AS VOTO, count(VOTO) NUMERO_VOTOS
from GOC_VOTOS VOTOS
group by VOTO, PUNTO_ID, REUNION_ID
UNION
select REUNION_ID as REUNION_ID, PUNTO_ID as PUNTO_ID, VOTO AS VOTO, count(VOTO) NUMERO_VOTOS
from GOC_VOTOS_PRIVADOS VOTOS
group by VOTO, PUNTO_ID, REUNION_ID;


CREATE OR REPLACE VIEW GOC_VW_TODOS_VOTOS AS
select REUNION_ID                as REUNION_ID,
       PUNTO_ID                  as PUNTO_ID,
       NOMBRE_VOTANTE            as NOMBRE_VOTANTE,
       PERSONA_ID                as PERSONA_ID,
       VOTO_EN_NOMBRE            as VOTO_EN_NOMBRE,
       VOTO_EN_NOMBRE_PERSONA_ID as VOTO_EN_NOMBRE_PERSONA_ID,
       1                         as VOTO_PUBLICO
from GOC_VOTOS VOTOS
UNION
select REUNION_ID                as REUNION_ID,
       PUNTO_ID                  as PUNTO_ID,
       NOMBRE_VOTANTE            as NOMBRE_VOTANTE,
       PERSONA_ID                as PERSONA_ID,
       VOTO_EN_NOMBRE            as VOTO_EN_NOMBRE,
       VOTO_EN_NOMBRE_PERSONA_ID as VOTO_EN_NOMBRE_PERSONA_ID,
       0                         as VOTO_PUBLICO
from GOC_VOTANTES_PRIVADOS VOTOS;

alter sequence hibernate_sequence restart with 500;