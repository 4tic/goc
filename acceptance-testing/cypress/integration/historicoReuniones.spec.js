import HistoricoReunionesPageObject from "../../pageobjects/HistoricoReunionesPageObject";
import GocPageObject from "../../pageobjects/GocPageObject";
import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";

context("Reuniones, Grid acceso externos", () => {
    let historicoReunionesPage;
    let reunionName = "Dirección del centro completada";

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");
        cy.route("GET", "/goc/rest/reuniones/*/hojafirmas**").as("hojafirmas");

        historicoReunionesPage = new HistoricoReunionesPageObject(cy);
        historicoReunionesPage.open();
    });

    it("Filtra reuniones por asunto", () => {
        historicoReunionesPage.getLoadMask().should("not.be.visible");

        historicoReunionesPage.find(reunionName).should("exist");

        historicoReunionesPage.filterByAsunto("x");
        historicoReunionesPage.find(reunionName).should("not.exist");

        historicoReunionesPage.clearSearchAsuntoField();
        historicoReunionesPage.find(reunionName).should("exist");
    });

    it("Filtra reuniones por tipo de órgano", () => {
        historicoReunionesPage.filterByTipo("Órgano externo");
        historicoReunionesPage.find(reunionName).should("not.exist");

        historicoReunionesPage.disableFilterByTipo();
        historicoReunionesPage.find(reunionName).should("exist");
    });

    it("Filtra reuniones por órgano", () => {
        historicoReunionesPage.filterByOrgano("Dirección general");
        historicoReunionesPage.find(reunionName).should("not.exist");

        historicoReunionesPage.disableFilterByOrgano();
        historicoReunionesPage.find(reunionName).should("exist");
    });

    it("Sube hoja de firmas", () => {
        let fileName = 'text.txt';
        let fileType = 'multipart/form-data';

        historicoReunionesPage.select(reunionName);

        historicoReunionesPage.selectAccion("Añadir hoja de firmas");
        cy.wait(["@hojafirmas"]).its("status").should("be.equal", 200);
        cy.upload_file(fileName, fileType, "input[name='documento']");
        historicoReunionesPage.getBotonSubirDocumento().click();
        cy.wait(["@hojafirmas"]).its("status").should("be.equal", 200);
        historicoReunionesPage.getBotonAceptarAlert().click();
        historicoReunionesPage.getListadoHojaFirmas().contains("text.txt");
        historicoReunionesPage.getListadoHojaFirmas().type("{esc}");
    });

    it("Reabre una reunión (diligencias)", () => {
        historicoReunionesPage.select(reunionName);
        historicoReunionesPage.selectAccion("Reabrir reunión");
        historicoReunionesPage.fillMotivoReapertura();
        historicoReunionesPage.getLoadMask().should("not.be.visible");
        historicoReunionesPage.find(reunionName).should("not.exist");

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
        reunionesPage.find(reunionName).should("exist");
    });
});