import DescriptoresPageObject from "../../pageobjects/DescriptoresPageObject"

context("Descriptores", () => {
    let page;
    let randName = "Nombre" + Math.random().toString(36).substr(2, 5);

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        page = new DescriptoresPageObject(cy);
        page.open();
        page.getLoadMask().should("not.be.visible");
    });

    it("Debe añadir un descriptor", () => {
        page.add();
        cy.invalidForm();
        page.fill(randName, "Nombre alternativo", "Descripción", "Descripción alternativa");
        cy.validForm();
        page.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe editar un descriptor", () => {
        page.select(randName);
        page.getLoadMaskClave().should("not.be.visible");

        page.edit();
        page.clear();
        cy.invalidForm();
        page.fill(randName, "Nombre alternativo", "Descripción " + Math.random(), "Descripción alternativa");
        cy.validForm();
        page.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe añadir una clave", () => {
        page.select(randName);
        page.getLoadMaskClave().should("not.be.visible");

        page.addClave();
        cy.invalidForm();
        page.fillClave(randName, "Nombre alternativo");
        cy.validForm();
        page.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe editar una clave", () => {
        page.select(randName);
        page.getLoadMaskClave().should("not.be.visible");

        page.selectClave(randName);
        page.editClave();
        page.clearClave();
        cy.invalidForm();
        page.fillClave(randName + 'EDITADO', "Nombre alternativo " + Math.random());
        cy.validForm();
        page.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe eliminar una clave", () => {
        page.select(randName);
        page.getLoadMaskClave().should("not.be.visible");

        page.selectClave(randName + 'EDITADO');
        page.deleteClave();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
    });

    it("Debe eliminar un descriptor", () => {
        page.select(randName);
        page.getLoadMaskClave().should("not.be.visible");

        page.delete();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
    });
});
