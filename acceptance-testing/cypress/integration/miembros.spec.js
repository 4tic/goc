import MiembrosPageObject from "../../pageobjects/MiembrosPageObject"
import OrganosPageObject from "../../pageobjects/OrganosPageObject"

context("Miembros y grupos", () => {
    let organosPage;
    let miembrosPage;
    let organoName = "Junta de gobierno";

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');
        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        miembrosPage = new MiembrosPageObject(cy);
        organosPage = new OrganosPageObject(cy);
    });

    it("Debe añadir un miembro y asignar al grupo", () => {
        miembrosPage.open();
        miembrosPage.selectOrgano();
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.findOrgano(organoName);
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.getLoadMask().should("not.be.visible");
        miembrosPage.add();
        miembrosPage.findMiembro("extern");
        miembrosPage.getLoadMaskAddMiembro().should("not.be.visible");
        miembrosPage.selectMiembro("extern");
        miembrosPage.getCargoCombo().type("{downarrow}", {force: true})
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        miembrosPage.getCargoCombo().type("{enter}", {force: true});
        miembrosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        miembrosPage.clickMiembro("extern");
        miembrosPage.asignarGrupo();
        miembrosPage.creaGrupo();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe añadir un segundo miembro al organo y asignar al grupo", () => {
        miembrosPage.open();
        miembrosPage.selectOrgano();
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.findOrgano(organoName);
        miembrosPage.getLoadingIndicator().should("not.be.visible");

        miembrosPage.getLoadMask().should("not.be.visible");
        miembrosPage.add();
        miembrosPage.findMiembro("Esteve");
        miembrosPage.getLoadMaskAddMiembro().should("not.be.visible");
        miembrosPage.selectMiembro("Esteve");
        miembrosPage.getCargoCombo().focus().type("{downarrow}", {force: true})
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        miembrosPage.getCargoCombo().type("{enter}", {force: true});
        miembrosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        miembrosPage.clickMiembro("Externo");
        miembrosPage.asignarGrupo();
        miembrosPage.creaGrupo();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe bajar un grupo", () => {
        miembrosPage.open();
        miembrosPage.selectOrgano();
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.findOrgano(organoName);
        miembrosPage.getLoadingIndicator().should("not.be.visible");

        miembrosPage.clickBajarGrupo();
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe bloquear y desbloquear la ordenación de miembros", ()=> {
        miembrosPage.open();
        miembrosPage.selectOrgano();
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.findOrgano(organoName);

        miembrosPage.getLoadMask().should("not.be.visible");

        miembrosPage.lockSorter();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        miembrosPage.getSorterColumn().should("not.exist");

        miembrosPage.unlockSorter();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });
});