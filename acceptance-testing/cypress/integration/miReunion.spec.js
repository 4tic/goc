import PublicacionesPageObject from "../../pageobjects/PublicacionesPageObject";

context("Ficha mi reunion", () => {
    let publicacionPage;
    let punto = 0;
    let reunionId = 2;

    beforeEach(() => {
        cy.server();

        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        publicacionPage = new PublicacionesPageObject(cy);
        publicacionPage.open(reunionId);
    });

    it("Debe agregar un motivo en caso de no asistencia", () => {
        publicacionPage.getBotonNoAsistir().click();
        publicacionPage.getJusticificacionTextArea().type("No asistiré");
        publicacionPage.getBotonEnviarJustificacion().click();

        cy.wait(["@save"]).its("status").should("be.equal", 204);
    });

    it("Debe poder marcar como que asistirá", () => {
        publicacionPage.getBotonAsistir().click();

        cy.wait(["@save"]).its("status").should("be.equal", 204);
    });

    it("Debe poder añadir un comentario y una respuesta sobre un punto", () => {
        publicacionPage.getBotonAnyadirComentarioEnPunto(punto).click();
        publicacionPage.getComentarioTextArea().type("Comentario de test");
        publicacionPage.getBotonEnviarComentario().click();

        cy.wait(["@save"]).its("status").should("be.equal", 200);

        publicacionPage.getBotonResponderUltimoComentario(punto).click();
        publicacionPage.getComentarioTextArea().type("Respuesta de test");
        publicacionPage.getBotonEnviarComentario().click();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe ver y ocultar respuestas del comentario", () => {
        publicacionPage.getBotonVerComentarios().click();

        publicacionPage.getBotonVerRespuestasUltimoComentario(punto).click();
        publicacionPage.getListaRespuestasUltimoComentario(punto).should("be.visible");

        publicacionPage.getBotonOcultarRespuestasUltimoComentario(punto).click();
        publicacionPage.getListaRespuestasUltimoComentario(punto).should("not.be.visible");
    });

    it("Debe mostrar y ocultar comentarios", () => {
        publicacionPage.getBotonVerComentarios().click();
        publicacionPage.getListaComentarios().should("be.visible");

        publicacionPage.getBotonOcultarComentarios().click();
        publicacionPage.getListaComentarios().should("not.be.visible");
    });

    it("Debe poder añadir un comentario en la reunión y eliminarlo", () => {
        publicacionPage.getBotonAnyadirComentario().click();
        publicacionPage.getComentarioTextArea().type("Comentario de test en la reunión");
        publicacionPage.getBotonEnviarComentario().click();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe poder eliminar un comentario en la reunión", () => {
        publicacionPage.getBotonEliminarComentario().click();
        cy.wait(["@delete"]).its("status").should("be.equal", 204);
    });
});
