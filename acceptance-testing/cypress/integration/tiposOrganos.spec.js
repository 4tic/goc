import TipoOrganosPageObject from "../../pageobjects/TipoOrganosPageObject"

context("Tipos de órganos", () => {
    let tipoOrganosPage;
    let randName = "COD_" + Math.random().toString(36).substr(2, 5);

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        tipoOrganosPage = new TipoOrganosPageObject(cy);
        tipoOrganosPage.open();
        tipoOrganosPage.getLoadMask().should("not.be.visible");
    });

    it("Debe añadir un tipo de órgano", () => {
        tipoOrganosPage.add();
        cy.invalidForm();
        tipoOrganosPage.fill(randName, "Nombre", "Nombre alternativo");
        cy.validForm();
        tipoOrganosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe editar un tipo de órgano", () => {
        tipoOrganosPage.select(randName);
        tipoOrganosPage.edit();
        tipoOrganosPage.clear();
        cy.invalidForm();
        tipoOrganosPage.fill(randName, "Nombre editado TEST", "Nombre alternativo", true, false);
        cy.validForm();
        tipoOrganosPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe eliminar un tipo de órgano", () => {
        tipoOrganosPage.open();
        tipoOrganosPage.getLoadMask().should("not.be.visible");

        tipoOrganosPage.select(randName);
        tipoOrganosPage.delete();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
    });
});
