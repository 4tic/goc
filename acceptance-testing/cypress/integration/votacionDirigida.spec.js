import MiembrosPageObject from "../../pageobjects/MiembrosPageObject"
import VotosPageObject from "../../pageobjects/VotosPageObject";
import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import PublicacionesPageObject from "../../pageobjects/PublicacionesPageObject";

context("Dirección de votación", () => {
    let PUNTO = 0;
    let reunionVotacionId = 6;

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');
        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        cy.route("GET", `/goc/rest/votos/*`).as("votos");
        cy.route("GET", "/goc/rest/votos/*/estado/?**").as("estado");
        cy.route("POST", "/goc/rest/reuniones/*/confirmar/*").as("confirmar");
    });

    it("Debe añadir un miembro que presida la votación", () => {
        cy.login();

        let miembrosPage = new MiembrosPageObject(cy);
        miembrosPage.open();
        miembrosPage.selectOrgano();
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.findOrgano("Junta de gobierno");
        miembrosPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.getLoadMask().should("not.be.visible");

        miembrosPage.clickMiembro("Delegado");
        miembrosPage.edit();
        miembrosPage.getPresideVotacionCheck().check();
        miembrosPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);

        miembrosPage.reload();
        miembrosPage.edit();
        miembrosPage.getPresideVotacionCheck().should("be.checked");
    });

    it("En la página de votación de la reunión dirigida no debe salir botones de abrir/cerrar", () => {
        cy.visit(`/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then(() => {
                let page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonAbrirVotacion(PUNTO).should("not.be.visible");
            });
    });

    it("Al asignar el órgano de Dirección general a la reunión dirigida debe aparecer los botones de abrir/cerrar en la página de votación", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.select("Reunión test votación dirigida");
        reunionesPage.edit();
        cy.get("[id^='formReunion'").contains("Edición").should("be.visible");
        reunionesPage.getLoadingIndicator().should("not.be.visible");

        reunionesPage.addOrgano();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.selectOrgano('Dirección general');
        reunionesPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);

        cy.visit(`/goc/rest/publicacion/reuniones/${reunionVotacionId}`)
            .then(() => {
                let page = new PublicacionesPageObject(cy);
                page.getBotonAsistir().click();
                cy.wait(["@confirmar"]).its("status").should("be.equal", 204);

                cy.visit(`/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
                    .then(() => {
                        let page = new VotosPageObject(cy);
                        cy.wait(["@estado"]).its("status").should("be.equal", 200);

                        page.getBotonAbrirVotacion(PUNTO).should("be.visible");
                    });
            });
    });

    it("Al abrir la votación del punto debe habilitar los botones de voto", () => {
        cy.visit(`/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then(() => {
                let page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonAbrirVotacion(PUNTO).click();
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonCerrarVotacion(PUNTO).should("be.visible");
                page.getBotonVotarAFavor(PUNTO).should("be.enabled");
                page.getBotonVotarEnContra(PUNTO).should("be.enabled");
                page.getBotonVotarAbstencion(PUNTO).should("be.enabled");
            });
    });

    it("Al cerrar la votación del punto debe deshabilitar los botones de votos", () => {
        cy.visit(`/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then(() => {
                let page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonCerrarVotacion(PUNTO).click();
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonAbrirVotacion(PUNTO).should("be.visible");
                page.getBotonVotarAFavor(PUNTO).should("be.disabled");
                page.getBotonVotarEnContra(PUNTO).should("be.disabled");
                page.getBotonVotarAbstencion(PUNTO).should("be.disabled");
            });
    });

    it("Debe permitir abrir varios puntos", () => {
        cy.visit(`/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then(() => {
                let PUNTOS = [0, 1];
                let page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonAbrirVotacion(PUNTOS[0]).click();
                page.getBotonAbrirVotacion(PUNTOS[1]).click();

                cy.wait(["@estado"]).its("status").should("be.equal", 200);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                for (var i in PUNTOS) {
                    page.getBotonCerrarVotacion(i).should("be.visible");
                    page.getBotonVotarAFavor(i).should("be.enabled");
                    page.getBotonVotarEnContra(i).should("be.enabled");
                    page.getBotonVotarAbstencion(i).should("be.enabled");
                }

                page.getBotonCerrarVotacion(PUNTOS[0]).click();
                page.getBotonCerrarVotacion(PUNTOS[1]).click();
                cy.wait(["@estado"]).its("status").should("be.equal", 200);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);
            });
    });

    it("No debe permitir pulsar abrir el mismo punto hasta que llegue la respuesta", () => {
        cy.visit(`/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then(() => {
                let page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonAbrirVotacion(PUNTO).click();
                page.getBotonAbrirVotacion(PUNTO).should("not.be.disabled");

                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonCerrarVotacion(PUNTO).should("be.visible");
            });
    });
});