import CargosPageObject from "../../pageobjects/CargosPageObject"

context("Cargos", () => {
    let page;
    let randName = "COD_" + Math.random().toString(36).substr(2, 5);

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        page = new CargosPageObject(cy);
        page.open();
        page.getLoadMask().should("not.be.visible");
    });


    it("Debe añadir un cargo", () => {
        page.add();
        cy.invalidForm();
        page.fill(randName, "Nombre", "Nombre alternativo", true);
        cy.validForm();
        page.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe editar un cargo", () => {
        page.select(randName);
        page.edit();
        page.clear();
        cy.invalidForm();
        page.fill(randName, "Nombre editado " + Math.random(), "Nombre alternativo", true);
        cy.validForm();
        page.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe eliminar un cargo", () => {
        page.select(randName);
        page.delete();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
    });
});
