import OrganosPageObject from "../../pageobjects/OrganosPageObject"
import MiembrosPageObject from "../../pageobjects/MiembrosPageObject"
import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../pageobjects/OrdenDiaPageObject";
import GocPageObject from "../../pageobjects/GocPageObject";

context("Órganos, reuniones y puntos de orden del día", () => {
    let organosPage;
    let miembrosPage;
    let reunionesPage;
    let ordenDiaPage;
    var gocPage;
    let randName = "COD_" + Math.random().toString(36).substr(2, 5);

    beforeEach(() => {
        cy.login();
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        organosPage = new OrganosPageObject(cy);
        miembrosPage = new MiembrosPageObject(cy);
        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        gocPage = new GocPageObject(cy);
    });

    it("Debe añadir un órgano", () => {
        organosPage.open();
        organosPage.getLoadMask().should("not.be.visible");

        organosPage.add();
        cy.invalidForm();
        organosPage.fill(randName, "Nombre", "test");
        cy.validForm();
        organosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe añadir un autorizado", () => {
        organosPage.open();
        organosPage.getLoadMask().should("not.be.visible");

        organosPage.select(randName);
        organosPage.getLoadMaskAutorizado().should("not.be.visible");

        organosPage.addAutorizado();
        organosPage.findAutorizado("4tic");
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        organosPage.getLoadMaskAddAutorizado().should("not.be.visible");
        organosPage.selectAutorizado("4tic");
        organosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe añadir un miembro en el órgano", () => {
        miembrosPage.open();
        miembrosPage.selectOrgano();
        gocPage.getLoadingIndicator().should("not.be.visible");
        miembrosPage.findOrgano(randName);

        miembrosPage.getLoadMask().should("not.be.visible");
        miembrosPage.add();
        miembrosPage.findMiembro("4tic");
        miembrosPage.getLoadMaskAddMiembro().should("not.be.visible");
        miembrosPage.selectMiembro("4tic");
        miembrosPage.getCargoCombo().focus().type("{downarrow}", {force: true})
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        miembrosPage.getCargoCombo().type("{enter}", {force: true});
        miembrosPage.save();

        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe inhabilitar un órgano", () => {
        organosPage.open();
        organosPage.getLoadMask().should("not.be.visible");

        organosPage.select(randName);
        organosPage.disable();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });
});
