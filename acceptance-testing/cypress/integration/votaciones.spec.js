import PublicacionesPageObject from "../../pageobjects/PublicacionesPageObject";
import VotosPageObject from "../../pageobjects/VotosPageObject";
import DescriptoresPageObject from "../../pageobjects/DescriptoresPageObject";
import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../pageobjects/OrdenDiaPageObject";
import dateFormat from "dateformat";

context("Votaciones", () => {
    let page;
    let random = Math.random().toString(36).substr(2, 5);
    let randName = "Reunion privacidad votación " + random.toString();
    let reunionVotacionId = 4;
    let reunionConVotosName = "Reunión con votos";
    
    let PUNTO_PRIVADO = 0;
    let PUNTO_PUBLICO = 1;

    beforeEach(() => {
        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");
        cy.route("GET", `/goc/rest/votos/*`).as("votos");
        cy.route("GET", "/goc/rest/votos/*/estado/?**").as("estado");
        cy.route("POST", "/goc/rest/reuniones/*/confirmar/*").as("confirmar");
    });

    it("No se puede modificar la opcionalidad de voto en la reunión si hay algún voto hecho", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.select(reunionConVotosName);
        reunionesPage.edit();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.getVotacionCheck().check();
        reunionesPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 500);
    });

    it("No se puede modificar la privacidad de voto en un punto con votos", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.select(reunionConVotosName);

        let ordenDiaPage = new OrdenDiaPageObject(cy);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.select('Punto orden día con votos');
        ordenDiaPage.edit();
        ordenDiaPage.getRadioVotacionPrivada().click();
        ordenDiaPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 500);
    });

    it("Se puede modificar el título del punto en un punto con votos", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.select(reunionConVotosName);

        let ordenDiaPage = new OrdenDiaPageObject(cy);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.select('Punto orden día con votos');
        ordenDiaPage.edit();
        ordenDiaPage.getTitulo().type("Título editado en punto con votos");
        ordenDiaPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Se puede modificar la privacidad de voto en un punto sin votos", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.select(reunionConVotosName);

        let ordenDiaPage = new OrdenDiaPageObject(cy);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.select('Punto orden día sin votos');
        ordenDiaPage.edit();
        ordenDiaPage.getRadioVotacionPrivada().click();
        ordenDiaPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Añade una reunión con votación y punto automático y este debe ser de tipo votación privada y los creados manualmente deben tener votación pública por defecto", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.add();

        reunionesPage.addOrgano();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.selectOrgano('Junta de gobierno');

        reunionesPage.fill(randName, randName);
        reunionesPage.getVotacionCheck().check();
        reunionesPage.save();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.select(randName);
        let ordenDiaPage = new OrdenDiaPageObject(cy);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.select("Revisión acta reunión anterior");
        ordenDiaPage.edit();
        ordenDiaPage.getRadioVotacionPrivada().should("be.checked");
        ordenDiaPage.getRadioVotacionPublica().should("not.be.checked");
        ordenDiaPage.cancelar();

        reunionesPage.select(randName);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.add();
        ordenDiaPage.getRadioVotacionPrivada().should("not.be.checked");
        ordenDiaPage.getRadioVotacionPublica().should("be.checked");
        ordenDiaPage.cancelar();
    });

    it("Aparece el enlace de votación", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.find(randName).find("a.link").should('have.attr', 'href')
            .then((href) => {
                cy.visit(href).then(() => {
                    page = new PublicacionesPageObject(cy);
                    cy.contains("Votar electrónicamente").should("be.visible");
                });
            });
    });

    it("Si no confirmas la asistencia no se puede votar", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.find(randName).find("a.link").should('have.attr', 'href')
            .then((href) => {
                cy.visit(href).then(() => {
                    page = new PublicacionesPageObject(cy);
                    cy.contains("a", "Votar electrónicamente").should('have.attr', 'href')
                        .then((href) => {
                            cy.visit(href).then(() => {
                                cy.contains("Para poder votar debe confirmar la asistencia").should("be.visible");
                            });
                        });
                });
            });
    });

    it("Debe confirmar la asistencia para poder votar", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.find(randName).find("a.link").should('have.attr', 'href')
            .then((href) => {
                cy.visit(href).then(() => {
                    page = new PublicacionesPageObject(cy);

                    page.getBotonAsistir().click();
                    cy.wait(["@confirmar"]).its("status").should("be.equal", 204);
                    cy.contains("a", "Votar electrónicamente").should('have.attr', 'href')
                        .then((href) => {
                            cy.visit(href).then(() => {
                                cy.contains("Para poder votar debe confirmar la asistencia").should("not.be.visible");
                            });
                        });
                });
            });
    });

    it("Deshabilita la votación y al añadir punto no se muestra nada de la votación", () => {
        cy.login();

        let reunionesPage = new ReunionesPageObject(cy);
        reunionesPage.open();
        reunionesPage.select(randName);
        reunionesPage.edit();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.getVotacionCheck().check();
        reunionesPage.save();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);

        reunionesPage.select(randName);
        let ordenDiaPage = new OrdenDiaPageObject(cy);
        ordenDiaPage.getLoadingIndicator().should("not.be.visible");
        ordenDiaPage.add();
        ordenDiaPage.getRadioVotacionPublica().should("not.be.visible");
    });

    it("Debe votar de manera privada y pública", () => {
        page = new PublicacionesPageObject(cy);
        page.open(reunionVotacionId);

        page.getEnlaceVotar().should('have.attr', 'href').and('include', `/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then((href) => {
                cy.visit(href).then(() => {
                    page = new VotosPageObject(cy);
                    cy.wait(["@estado"]).its("status").should("be.equal", 200);
                    page.getResultados(PUNTO_PRIVADO).contains("A favor: 0");
                    page.getBotonVotarAFavor(PUNTO_PRIVADO).click();
                    page.getResultados(PUNTO_PRIVADO).contains("A favor: 1");
                });
            });
    });

    it("Al representar a otro votante debe habilitar para votación los puntos aún no votados", () => {
        page = new PublicacionesPageObject(cy);
        page.open(reunionVotacionId);

        page.getEnlaceVotar().should('have.attr', 'href').and('include', `/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then((href) => {
                cy.visit(href).then(() => {
                    page = new VotosPageObject(cy);
                    cy.wait(["@estado"]).its("status").should("be.equal", 200);
                    page.getBotonVotarAFavor(PUNTO_PRIVADO).should("be.disabled");

                    page.getSelectVotarEnNombreDe().select("devel");
                    cy.wait(["@estado"]).its("status").should("be.equal", 200);
                    page.getBotonVotarAFavor(PUNTO_PRIVADO).should("be.disabled");

                    page.getSelectVotarEnNombreDe().select("No asistente");
                    cy.wait(["@estado"]).its("status").should("be.equal", 200);
                    page.getBotonVotarAFavor(PUNTO_PRIVADO).should("be.enabled");
                });
            });
    });

    it("Debe votar de manera privada en representación", () => {
        page = new PublicacionesPageObject(cy);
        page.open(reunionVotacionId);

        page.getEnlaceVotar().should('have.attr', 'href').and('include', `/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then((href) => {
                cy.visit(href).then(() => {
                    page = new VotosPageObject(cy);
                    cy.wait(["@estado"]).its("status").should("be.equal", 200);

                    page.getSelectVotarEnNombreDe().select("No asistente");
                    cy.wait(["@estado"]).its("status").should("be.equal", 200);

                    page.getResultados(PUNTO_PRIVADO).contains("A favor: 1");
                    page.getBotonVotarAFavor(PUNTO_PRIVADO).click();
                    page.getResultados(PUNTO_PRIVADO).contains("A favor: 2");
                });
            });
    });

    it("La votación privada no debe mostrar resultados provisionales", () => {
        page = new VotosPageObject(cy);
        page.open(reunionVotacionId);

        cy.wait(["@estado"]).its("status").should("be.equal", 200);

        page.getBotonVotosProvisionales(PUNTO_PRIVADO).should("not.exist");
    });

    it("Debe votar de manera pública", () => {
        page = new PublicacionesPageObject(cy);
        page.open(reunionVotacionId);

        page.getEnlaceVotar().should('have.attr', 'href').and('include', `/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then((href) => {
                cy.visit(href);

                page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getResultados(PUNTO_PUBLICO).contains("A favor: 0");
                page.getBotonVotarAFavor(PUNTO_PUBLICO).click();
                page.getResultados(PUNTO_PUBLICO).contains("A favor: 1");

                page.getBotonVotosProvisionales(PUNTO_PUBLICO).click();
                page.getResultadosProvisionales().contains("A favor: devel");
            });
    });

    it("Debe votar de manera pública en representación", () => {
        page = new PublicacionesPageObject(cy);
        page.open(reunionVotacionId);

        page.getEnlaceVotar().should('have.attr', 'href').and('include', `/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then((href) => {
                cy.visit(href);

                page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getSelectVotarEnNombreDe().select("No asistente");

                page.getResultados(PUNTO_PUBLICO).contains("A favor: 1");
                page.getResultados(PUNTO_PUBLICO).contains("En contra: 0");
                page.getBotonVotarEnContra(PUNTO_PUBLICO).click();
                page.getResultados(PUNTO_PUBLICO).contains("A favor: 1");
                page.getResultados(PUNTO_PUBLICO).contains("En contra: 1");

                page.getBotonVotosProvisionales(PUNTO_PUBLICO).click();
                page.getResultadosProvisionales().contains("En contra: devel (en representación de: No asistente)");
            });
    });

    it("No debe permitir votar por segunda vez", () => {
        page = new PublicacionesPageObject(cy);
        page.open(reunionVotacionId);

        page.getEnlaceVotar().should('have.attr', 'href').and('include', `/goc/rest/publicacion/reuniones/${reunionVotacionId}/votacion`)
            .then((href) => {
                cy.visit(href);

                page = new VotosPageObject(cy);
                cy.wait(["@estado"]).its("status").should("be.equal", 200);

                page.getBotonVotarAFavor(PUNTO_PUBLICO).should("be.disabled");
                page.getBotonVotarEnContra(PUNTO_PUBLICO).should("be.disabled");
                page.getBotonVotarAbstencion(PUNTO_PUBLICO).should("be.disabled");
            });
    });
});
