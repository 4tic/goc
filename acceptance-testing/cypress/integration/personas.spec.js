import PersonasPageObject from "../../pageobjects/PersonasPageObject"
import GocPageObject from "../../pageobjects/GocPageObject"

context("Personas", () => {
    let personasPage;
    let gocPage;
    let personaRandName = "COD_" + Math.random().toString(36).substr(2, 5);
    let randmail = personaRandName+ '@4tic.com';

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('JSESSIONID');

        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        personasPage = new PersonasPageObject(cy);
        gocPage = new GocPageObject(cy);
    });

    it("Debe añadir una persona", () => {
        personasPage.open();
        personasPage.getLoadMask().should("not.be.visible");

        personasPage.add();
        personasPage.fill(personaRandName, randmail);
        cy.wait(["@save"]).its("status").should("be.equal", 200);
    });

    it("Debe filtrar personas por nombre", () => {
        personasPage.filterByNombre("Nombre no existe");
        personasPage.find(personaRandName).should("not.be.visible");

        personasPage.clearSearchNombreField();
        personasPage.filterByNombre(personaRandName);
        personasPage.find(personaRandName).should("be.visible");
    });

    it("Debe editar una persona",() => {
        personasPage.getLoadMask().should("not.be.visible");
        personasPage.selectPersona(personaRandName);
        personasPage.edit();
        personasPage.fill('EDITADO');
        gocPage.getLoadingIndicator().should("not.be.visible");
        cy.wait(["@update"]).its("status").should("be.equal", 200);
    });

    it("Debe deshabilitar (eliminar) una persona creada",()  =>{
        personasPage.selectPersona(personaRandName);
        personasPage.delete();
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        personasPage.find(personaRandName).should("not.be.visible");
    });
});
