import ReunionesPageObject from "../../pageobjects/ReunionesPageObject";
import OrdenDiaPageObject from "../../pageobjects/OrdenDiaPageObject"
import GocPageObject from "../../pageobjects/GocPageObject";
import FormUploadPageObject from "../../pageobjects/FormUploadPageObject";
import FormPageObject from "../../pageobjects/FormPageObject";

context("Reuniones, Grid acceso externos", () => {
    let reunionesPage;
    let random = Math.random().toString(36).substr(2, 5);
    let randName = "Reunion Test " + random.toString();
    let reunionName = "Reunión abierta";
    let ordenDiaPage;
    let gocPage;
    let fileName = 'text.txt';
    let fileType = 'multipart/form-data';
    let formUploadPage;
    let formPage;

    before(() => {
        cy.login();
    });

    beforeEach(() => {
        cy.server();

        cy.route("GET", "/goc/rest/**").as("get");
        cy.route("POST", "/goc/rest/**").as("save");
        cy.route("PUT", "/goc/rest/**").as("update");
        cy.route("DELETE", "/goc/rest/**").as("delete");

        reunionesPage = new ReunionesPageObject(cy);
        ordenDiaPage = new OrdenDiaPageObject(cy);
        gocPage = new GocPageObject(cy);
        formPage = new FormPageObject(cy);
        formUploadPage = new FormUploadPageObject(cy);

        reunionesPage.open();
        reunionesPage.getLoadMask().should("not.be.visible");
    });

    it("Debe mostrar las reuniones pasadas y poder ocultarlas", () => {
        reunionesPage.getCheckPasadas().should("be.checked");
        reunionesPage.getLinks().should('have.length', 6)

        reunionesPage.getCheckPasadas().uncheck();
        reunionesPage.getCheckPasadas().should("not.be.checked");
        reunionesPage.getLinks().should('have.length', 5);

        reunionesPage.getCheckPasadas().check();
        reunionesPage.getCheckPasadas().should("be.checked");
        reunionesPage.getLinks().should('have.length', 6)
    });

    it("Debe poder seleccionar presencial o telemática pero no ambas cosas", () => {
        reunionesPage.add();

        reunionesPage.getRadioPresencial().click();

        reunionesPage.getRadioPresencial().should("be.checked");
        reunionesPage.getRadioTelematica().should("not.be.checked");
        reunionesPage.getDescripcionTelematica().should("be.disabled");
        reunionesPage.getUbicacion().should("be.disabled");

        reunionesPage.getRadioTelematica().click();

        reunionesPage.getRadioPresencial().should("not.be.checked");
        reunionesPage.getRadioTelematica().should("be.checked");
        reunionesPage.getDescripcionTelematica().should("be.enabled");
        reunionesPage.getUbicacion().should("be.enabled");
        reunionesPage.cancel();
    });

    it("Añade una reunión", () => {
        reunionesPage.add();

        reunionesPage.addOrgano();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.selectOrgano('Dirección general');

        reunionesPage.fill(randName, randName);

        reunionesPage.getRadioTelematica().click();
        reunionesPage.getDescripcionTelematica().type("Descripción telemática");
        reunionesPage.getUbicacion().type("youtube.com");

        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@update"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
    });

    it("Edita una reunión", () => {
        reunionesPage.select(randName);
        reunionesPage.edit();
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.clear();
        reunionesPage.fill(randName, "Asunto alternativo" + Math.random(), randName);
        reunionesPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
        reunionesPage.getLoadingIndicator().should("not.be.visible");
    });

    it("Debe eliminar un órgano de una reunión", () => {
        reunionesPage.select(randName);
        reunionesPage.edit();
        cy.get("[id^='formReunion'").contains("Edición").should("be.visible");
        reunionesPage.getLoadingIndicator().should("not.be.visible");

        //Si el titulo automatico esta activado hace fallar el test, por que el asunto se modifica a {organo} al eliminarlo de la reunión
        reunionesPage.deleteOrgano('Dirección general');
        reunionesPage.getBotonSiAlert().should("not.exist");
        reunionesPage.save();

        cy.wait(["@update"]).its("status").should("be.equal", 200);
        reunionesPage.getLoadingIndicator().should("not.be.visible");
    });

    it("Debe eliminar una reunión", () => {
        reunionesPage.select(randName);
        reunionesPage.delete();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
    });

    it("Filtra reuniones por tipo de órgano", () => {
        reunionesPage.find(reunionName).should("exist");

        reunionesPage.filterByTipo("Órgano externo");
        reunionesPage.find(reunionName).should("not.exist");

        reunionesPage.disableFilterByTipo();
        reunionesPage.find(reunionName).should("exist");
    });

    it("Filtra reuniones por órgano", () => {
        reunionesPage.find(reunionName).should("be.visible");

        reunionesPage.filterByOrgano("Dirección general");
        reunionesPage.find(reunionName).should("not.exist");

        reunionesPage.disableFilterByOrgano();
        reunionesPage.find(reunionName).should("exist");
    });

    it("Debe añadir y eliminar un externo", () => {
        reunionesPage.select(reunionName);
        reunionesPage.selectAccion("Dar acceso a personas externas");

        reunionesPage.clickElementGridExterno("Añadir");
        reunionesPage.findMiembro("Exter");
        reunionesPage.getLoadMaskAddMiembro().should("not.be.visible");
        reunionesPage.selectMiembro("Externo");
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        reunionesPage.getLoadingIndicator().should("not.be.visible");
        reunionesPage.clickElementGridExterno("Externo");
        reunionesPage.clickElementGridExterno("Borrar");
        formPage.closeForm();

        cy.wait(["@delete"]).its("status").should("be.equal", 200);
        reunionesPage.getLoadingIndicator().should("not.be.visible");
    });

    it("Debe subir un fichero y editar su descripción", ()=>{
        reunionesPage.select(reunionName);
        reunionesPage.openDocumentacionAdjunta();
        cy.upload_file(fileName, fileType, "input[name^='documento']");
        formUploadPage.typeDescriptionFieldParaSubida("test file");
        formUploadPage.clickUpload("documento");
        formUploadPage.dblclickDescriptionField();
        formUploadPage.typeDescriptionFieldDeFicheroSubido("Editado");
        formUploadPage.closeForm();
    });

    it("Cierra una reunión", () => {
        reunionesPage.select(reunionName);
        reunionesPage.selectAccion("Cerrar acta");
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        reunionesPage.firmarYCerrar();
        gocPage.getLoadingIndicator().should("not.be.visible");

        cy.wait(["@update"]).its("status").should("be.equal", 204);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        reunionesPage.getLoadMask().should("not.be.visible");

        reunionesPage.find(reunionName).should("not.exist");
    });

    it("Debe duplicar una reunión", () => {
        reunionesPage.select("Mi reunión");
        reunionesPage.selectAccion("Duplicar la reunión");
        reunionesPage.clear();
        reunionesPage.fill("Duplicada","Duplicada alt");
        reunionesPage.save();
        cy.wait(["@save"]).its("status").should("be.equal", 200);
        cy.wait(["@get"]).its("status").should("be.equal", 200);
        reunionesPage.find("Duplicada").should("exist");
    });
});
