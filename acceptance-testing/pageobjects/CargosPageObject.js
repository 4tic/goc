import TabPageObject from "./TabPageObject"

class CargosPageObject extends TabPageObject {
    constructor(wrapper) {
        super("Cargos", wrapper);
        this.wrapper = wrapper;
        this.cargoId = "cargo";
    }

    select(id) {
        return super._select(this.cargoId, id);
    }

    add() {
        return super._add(this.cargoId);
    }

    edit() {
        return super._edit(this.cargoId);
    }

    delete() {
        return super._delete(this.cargoId);
    }

    getLoadMask() {
        return super._getLoadMask(this.cargoId);
    }

    clear() {
        this._getInput("codigo").type(".").clear();
        this._getInput("nombre").clear();
        this._getInput("responsableActa").uncheck();
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("nombreAlternativo").clear();
            }
        });
    }

    fill(codigo, nombre, nombreAlternativo, responsable) {
        this._getInput("codigo").type(codigo);
        this._getInput("nombre").type(nombre);
        if (responsable) {
            this._getInput("responsableActa").check();
        }
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("nombreAlternativo").type(nombreAlternativo);
            }
        });
    }
}

export default CargosPageObject;