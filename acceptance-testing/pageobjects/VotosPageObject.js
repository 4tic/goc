import PublicPageObject from "./PublicPageObject";

class VotosPageObject extends PublicPageObject {
    constructor(wrapper) {
        super(wrapper);
        this.wrapper = wrapper;
    }

    open(reunionId) {
        this.wrapper.visit(`/goc/rest/publicacion/reuniones/${reunionId}/votacion`);
    }

    getBotonVotarAFavor(punto) {
        return this._getPuntosOrdenDia(punto).find("input[name='FAVOR']:visible");
    }

    getBotonVotarEnContra(punto) {
        return this._getPuntosOrdenDia(punto).find("input[name='CONTRA']:visible");
    }

    getBotonVotarAbstencion(punto) {
        return this._getPuntosOrdenDia(punto).find("input[name='ABSTENCION']:visible");
    }

    getResultados(punto) {
        return this._getPuntosOrdenDia(punto).find("div.ensemble-resultados");
    }

    getBotonAbrirVotacion(punto) {
        return this._getPuntosOrdenDia(punto).find("button[name='abrir-votacion']");
    }

    getBotonCerrarVotacion(punto) {
        return this._getPuntosOrdenDia(punto).find("button[name='cerrar-votacion']");
    }

    getBotonVotosProvisionales(punto) {
        return this._getPuntosOrdenDia(punto).find("[name='votos-provisionales']");
    }

    getResultadosProvisionales(punto) {
        return this.wrapper.get("div.modal-votos-provisionales");
    }

    getSelectVotarEnNombreDe() {
        return this.wrapper.get("select[name='votoEnNombre']");
    }
}

export default VotosPageObject;