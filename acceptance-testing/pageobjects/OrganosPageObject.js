import TabPageObject from "./TabPageObject"

class OrganosPageObject extends TabPageObject {
    constructor(wrapper) {
        super("Órganos", wrapper);
        this.wrapper = wrapper;
        this.organoId = "organo";
        this.autorizadoId = "autorizado";
    }

    select(id) {
        return super._select(this.organoId, id);
    }

    add() {
        return super._add(this.organoId);
    }

    edit() {
        return super._edit(this.organoId);
    }

    getLoadMask() {
        return super._getLoadMask(this.organoId);
    }

    disable() {
        this._getGrid(this.organoId).contains("span[id^='button']", "Inhabilita").click();
        this.getBotonSiAlert().click();
    }

    clear() {
        this._getInput("nombre").clear();
        this._getInput("tipoOrganoId").clear();

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("nombreAlternativo").clear();
            }
        });
    }

    fill(nombre, nombreAlternativo, tipoOrgano) {
        this._getInput("nombre").type(nombre);
        this._getInput("tipoOrganoId").click().type("{downarrow}", {force: true}).type("{enter}", {force: true});

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("nombreAlternativo").type(nombreAlternativo);
            }
        });
    }

    fillName(nombre){
        this._getInput("nombre").type(nombre);
    }

    fillTipoOrgano(){
        this._getInput("tipoOrganoId").click().type("{downarrow}").type("{enter}");
    }

    addAutorizado() {
        super._add(this.autorizadoId);
        this.wrapper.wait(1000);
    }

    getLoadMaskAutorizado() {
        return super._getLoadMask(this.autorizadoId);
    }

    findAutorizado(query) {
        this.wrapper.get("input[name='query']:visible").type(query);
        this.wrapper.contains("span:visible", "Buscar").click();
    }

    selectAutorizado(query) {
        this.wrapper.contains(".x-grid-cell-inner:visible", query).click();
        this.wrapper.contains("span:visible", "Seleccionar").click();
    }

    getLoadMaskAddAutorizado() {
        return this.wrapper.get("div[id^='lookupWindow']").find("div[id^='loadmask']");
    }
}

export default OrganosPageObject;