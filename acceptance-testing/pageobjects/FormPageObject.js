class FormPageObject  {
    constructor(wrapper) {
        this.wrapper = wrapper;
    }

    closeForm(){
        this.wrapper.get(".x-tool-close:visible").click();
    }

    _getLoadMask(id) {
        return this.wrapper.get("div.x-grid[id^='" + id + "']").find("div[id^='loadmask']");
    }
}
export default FormPageObject;