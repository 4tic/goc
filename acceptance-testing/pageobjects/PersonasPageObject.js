import TabPageObject from "./TabPageObject"

class PersonasPageObject extends TabPageObject {
    constructor(wrapper) {
        super("Personas", wrapper);
        this.wrapper = wrapper;
        this.personaId = "personas";
    }

    getLoadMask() {
        return super._getLoadMask(this.personaId);
    }

    add() {
        return super._add(this.personaId);
    }

    edit() {
        return super._edit(this.personaId);
    }

    fill(nombre, mail) {
        this._getInput("login").type(nombre);
        this._getInput("nombre").type(nombre);
        if (mail) {
            this._getInput("email").type(mail);
        }
        this._getInput("nombre").type("{enter}");

    }

    selectPersona(query) {
        this.wrapper.contains(".x-grid-cell-inner", query).click();
    }

    delete() {
        return this._getGrid(this.personaId).contains("span[id^='button']", "Borrar").click();
    }

    find(id) {
        return super._find(this.personaId, id);
    }

    filterByNombre(asunto) {
        this.wrapper.get("input[name^=searchPersona]").type(asunto);
    }

    clearSearchNombreField() {
        this.wrapper.get("input[name^=searchPersona]").clear();
    }
}

export default PersonasPageObject;