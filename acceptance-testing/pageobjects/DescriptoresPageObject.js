import TabPageObject from "./TabPageObject"

class DescriptoresPageObject extends TabPageObject {
    constructor(wrapper) {
        super("Descriptores", wrapper);
        this.wrapper = wrapper;
        this.descriptorId = "descriptor";
        this.claveId = "clave"
    }

    select(id) {
        return super._select(this.descriptorId, id);
    }

    add() {
        return super._add(this.descriptorId);
    }

    edit() {
        return super._edit(this.descriptorId);
    }

    delete() {
        return super._delete(this.descriptorId);
    }

    getLoadMask() {
        return super._getLoadMask(this.descriptorId);
    }

    clear() {
        this._getInput("descriptor").type(".").clear();
        this._getInput("descripcion").clear();

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("descriptorAlternativo").clear();
                this._getInput("descripcionAlternativa").clear();
            }
        });
    }

    fill(nombre, nombreAlternativo, descripcion, descripcionAlternativa) {
        this._getInput("descriptor").type(nombre);
        this._getInput("descripcion").type(descripcion);

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("descriptorAlternativo").type(nombreAlternativo);
                this._getInput("descripcionAlternativa").type(descripcionAlternativa);
            }
        });
    }


    selectClave(id) {
        return super._select(this.claveId, id);
    }


    addClave() {
        return super._add(this.claveId);
    }

    editClave() {
        return super._edit(this.claveId);
    }

    deleteClave() {
        return super._delete(this.claveId);
    }

    getLoadMaskClave() {
        return super._getLoadMask(this.claveId);
    }

    clearClave() {
        this._getInput("clave").clear();
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("claveAlternativa").clear();
            }
        });
    }

    fillClave(nombre, nombreAlternativo) {
        this._getInput("clave").type(nombre);
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("claveAlternativa").type(nombreAlternativo);
            }
        });
    }
}

export default DescriptoresPageObject;