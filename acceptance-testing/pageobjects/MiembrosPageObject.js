import TabPageObject from "./TabPageObject"

class MiembrosPageObject extends TabPageObject {
    constructor(wrapper) {
        super("Miembros", wrapper);
        this.wrapper = wrapper;
        this.miembroId = "miembro";
    }

    _getComboOrganos() {
        return this.wrapper.get("input[placeholder='Seleccionar un órgano']");
    }

    _getAccionesButton() {
        return this.wrapper.contains("span[id^='splitbutton']", "Acciones").get(".x-btn-arrow-el");
    }

    _getAsignarGrupo() {
        return this.wrapper.contains("Asignar a grupo");
    }

    _getNuevoGrupoFieldtext() {
        return this.wrapper.get("input[name='nuevoGrupo'");
    }

    _getAsignarGrupoButton() {
        return this.wrapper.contains("span[id^='button']", "Asignar grupo");
    }

    _selectBajarGrupoArrow() {
        return this.wrapper.get("span[data-action='bajar-grupo']").first().parent();
    }

    selectOrgano() {
        this._getComboOrganos().focus().type("{downarrow}").type("{downarrow}", {force: true});
    }

    asignarGrupo() {
        this._getAccionesButton().click({force: true});
        this._getAsignarGrupo().click({force: true});
    }

    findOrgano(organo) {
        this._getComboOrganos().clear().type(organo).type("{enter}");
    }

    getCargoCombo() {
        return this.wrapper.get("input[name='cargoId']");
    }

    add() {
        super._add(this.miembroId);
        this.wrapper.wait(1000);
    }

    edit() {
        super._edit(this.miembroId);
    }

    delete() {
        super._delete(this.miembroId);
    }

    reload() {
        super._getReloadButton(this.miembroId).click({force: true});
    }

    getLoadMask() {
        return super._getLoadMask(this.miembroId);
    }

    findMiembro(query) {
        this.wrapper.get("input[name='query']:visible").type(query);
        this.wrapper.contains("span:visible", "Buscar").click();
    }

    selectMiembro(query) {
        this.wrapper.contains(".x-grid-cell-inner:visible", query).click();
        this.wrapper.contains("span:visible", "Seleccionar").click();
    }

    clickMiembro(query) {
        this.wrapper.contains(".x-grid-cell-inner", query).click();
    }

    getLoadMaskAddMiembro() {
        return this.wrapper.get("div[id^='lookupWindow']").find("div[id^='loadmask']");
    }

    creaGrupo() {
        this._getNuevoGrupoFieldtext().type("nuevo" + Math.random().toString(36).substr(2, 5));
        this._getAsignarGrupoButton().click();
    }

    clickBajarGrupo() {
        this._selectBajarGrupoArrow().click()
    }

    _getAccionesButton() {
        return this.wrapper.get("a[id^='splitbutton']").find("span[role='button']");
    }

    lockSorter() {
        this._getAccionesButton().click({force: true});
        this.wrapper.contains("span", "Bloquear ordenación").click({force: true});
    }

    unlockSorter() {
        this._getAccionesButton().click({force: true});
        this.wrapper.contains("span", "Desbloquear ordenación").click({force: true});
    }

    getSorterColumn() {
        return this.wrapper.get("#subir");
    }

    getFirmanteCheck() {
        return this.wrapper.get("input[type='checkbox'][name='firmante']");
    }

    getPresideVotacionCheck() {
        return this.wrapper.get("input[type='checkbox'][name='presideVotacion']");
    }
}

export default MiembrosPageObject;