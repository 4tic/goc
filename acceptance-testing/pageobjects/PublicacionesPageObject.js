import PublicPageObject from "./PublicPageObject";

class ReunionesPageObject extends PublicPageObject {
    constructor(wrapper) {
        super(wrapper);
        this.wrapper = wrapper;
    }

    open(reunionId) {
        this.wrapper.visit(`/goc/rest/publicacion/reuniones/${reunionId}`);
    }

    getEnlaceVotar() {
        return this.wrapper.contains("a", "Votar electrónicamente");
    }

    getBotonAsistir() {
        return this.wrapper.get("button[name='confirmar']");
    }

    getBotonNoAsistir() {
        return this.wrapper.get("button[name='denegar']");
    }

    getJusticificacionTextArea() {
        return this.wrapper.get("textarea[name='motivo-ausencia']:visible");
    }

    getBotonEnviarJustificacion() {
        return this.wrapper.get("button[name='confirmar-ausencia']");
    }

    getComentarioTextArea() {
        return this.wrapper.get("div.nuevo-comentario-punto > textarea:visible,div.nuevo-comentario > textarea:visible");
    }

    getBotonEnviarComentario() {
        return this.wrapper.get("button.post-comentario-punto:visible,button.post-comentario:visible");
    }

    getBotonAnyadirComentario() {
        return this.wrapper.get("button.add-comentario");
    }

    getBotonAnyadirComentarioEnPunto(punto) {
        return this._getPuntosOrdenDia(punto).find("input.comentario-button");
    }

    getBotonOcultarComentarios() {
        return this.wrapper.get("a.enlace-ocultar-comentarios");
    }

    getBotonVerComentarios() {
        return this.wrapper.get("a.enlace-ver-comentarios");
    }

    getListaComentarios() {
        return this.wrapper.get("strong.tags-comentarios").parent();
    }

    _getUltimoComentario() {
        return this.wrapper.get("div.comentarios > .row").first();
    }

    getBotonEliminarComentario() {
        return this._getUltimoComentario().find("a.delete-comentario");
    }

    _getUltimoComentarioPunto(punto) {
        return this._getPuntosOrdenDia(punto).find("div.comentario:not([data-comentario-padre-id]):visible,div.comentario[data-comentario-padre-id='null']:visible").last();
    }

    getBotonResponderUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).find("a.enlace-responder-comentario");
    }

    getBotonVerRespuestasUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).find("a.enlace-ver-respuestas");
    }

    getBotonOcultarRespuestasUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).find("a.enlace-ocultar-respuestas");
    }

    getListaRespuestasUltimoComentario(punto) {
        return this._getUltimoComentarioPunto(punto).next().find("div.comentario");
    }
}

export default ReunionesPageObject;