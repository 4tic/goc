import TabPageObject from "./TabPageObject"
import dateFormat from "dateformat"

class ReunionesPageObject extends TabPageObject {
    constructor(wrapper) {
        super("Reuniones", wrapper);
        this.wrapper = wrapper;
        this.reunionId = "reunion";
    }

    select(id) {
        super._select(this.reunionId, id);
    }

    close() {
        super._close()
    }

    find(id) {
        return super._find(this.reunionId, id);
    }

    add() {
        super._add(this.reunionId);
    }

    edit() {
        super._edit(this.reunionId);
    }

    delete() {
        return super._delete(this.reunionId);
    }

    getLinks() {
        return this.wrapper.get("i.fa-external-link");
    }

    getLoadMask() {
        return super._getLoadMask(this.reunionId);
    }

    clear() {
        this._getInput("asunto").clear();
        this._getInput("fecha").clear();
        this._getInput("hora").clear();

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("asuntoAlternativo").clear();
            }
        });
    }

    fill(asunto, asuntoAlternativo) {
        let now = new Date();
        now.setDate(now.getDate() + 1);
        now.setHours(9);

        this._getInput("asunto").type(asunto);
        this._getInput("fecha").type(dateFormat(now, "dd/mm/yyyy"));
        this._getInput("hora").type("{backspace}{backspace}{backspace}{backspace}{backspace}" + dateFormat(now, "HH:ss"));

        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this._getInput("asuntoAlternativo").type(asuntoAlternativo);
            }
        });
    }

    getCampoFecha() {
        return this._getInput("fecha");
    }

    _getOrganosGrid() {
        return this.wrapper.contains("div[role='grid']", "Órganos asistentes");
    }

    addOrgano() {
        this._getOrganosGrid().scrollTo("top", {ensureScrollable: false});
        this._getOrganosGrid().find(".x-tool-plus").click({force: true});
        this.wrapper.get("div[id^='multiselector-search']").should("be.visible");
    }

    selectOrgano(organo) {
        this.wrapper.contains("div[id^='multiselector-search']", organo).contains("[role='row']", organo).click({force: true});
    }

    deleteOrgano(organo) {
        this._getOrganosGrid().contains("[role='row']", organo).find(".fa-remove").click();
        this.getBotonSiAlert().click({force: true});
    }

    save() {
        this.wrapper.get("div[id^='formReunion']").contains("Guardar").click({force: true});
    }

    cancel() {
        this.wrapper.get("div[id^='formReunion']").contains("Cancelar").click({force: true});
    }

    findMiembro(query) {
        this.wrapper.get("input[name='query']:visible").type(query);
        this.wrapper.contains("span:visible", "Buscar").click();
    }

    selectMiembro(query) {
        this.wrapper.contains(".x-grid-cell-inner:visible", query).click();
        this.wrapper.contains("span:visible", "Seleccionar").click();
    }

    getLoadMaskAddMiembro() {
        return this.wrapper.get("div[id^='lookupWindow']").find("div[id^='loadmask']");
    }

    firmarYCerrar() {
        this.wrapper.get("textarea[id^=textarea][placeholder=Acuerdos]:visible").type("Acuerdos  test");
        this.wrapper.get("input[id^='combo'][name='responsable']").click().type("{enter}", {force: true});
        this.wrapper.contains("[id^=button]", "Firmar y cerrar").click();
    }

    enviarConvocatoria() {
        this.wrapper.get("textarea[id^=textarea][name=convocatoria]:visible").type("Texto adicional de envío de convocatoria");
        this.wrapper.contains("span[id^='button']:visible", "Enviar convocatoria").click({force: true});
    }

    filterByTipo(tipo) {
        this.wrapper.get("input[id^=comboReunionTipoOrgano]").click();
        this.wrapper.get("div[id^=comboReunionTipoOrgano][data-ref=listWrap]").contains("li", tipo).click();
    }

    disableFilterByTipo() {
        this.wrapper.get("div.x-form-clear-trigger[id^=comboReunionTipoOrgano]").click({force: true});
    }

    filterByOrgano(organo) {
        this.wrapper.get("input[id^=comboOrgano]").click();
        this.wrapper.get("div[id^=comboOrgano][data-ref=listWrap]").contains("li", organo).click();
    }

    disableFilterByOrgano() {
        this.wrapper.get("div.x-form-clear-trigger[id^=comboOrgano]").click({force: true});
    }

    getGridExternos(){
        return this.wrapper.get("div[id^=gridAccesoExternos]")
    }

    clickElementGridExterno(element) {
        this.getGridExternos().contains(element).click({force:true });
    }

    findMiembro(query) {
        this.wrapper.get("input[name='query']:visible").type(query);
        this.wrapper.contains("span:visible", "Buscar").click();
    }

    selectMiembro(query) {
        this.wrapper.contains(".x-grid-cell-inner:visible", query).click();
        this.wrapper.contains("span:visible", "Seleccionar").click();
    }

    getLoadMaskAddMiembro() {
        return this.wrapper.get("div[id^='lookupWindow']").find("div[id^='loadmask']");
    }

    openDocumentacionAdjunta() {
        this.wrapper.get("a[id^='button']:visible").contains("Documentación adjunta").click();
    }

    getVotacionCheck() {
        return this.wrapper.get("input[name='hasVotacion']");
    }

    getRadioPresencial() {
        return this.wrapper.contains("label[id^='radio']", "Reunión presencial").parent().find("input");
    }

    getRadioTelematica() {
        return this.wrapper.contains("label[id^='radio']", "Reunión telemática").parent().find("input");
    }

    getDescripcionTelematica() {
        return this.wrapper.get("textarea[name='telematicaDescripcion']");
    }

    getUbicacion() {
        return this.wrapper.get("input[name='ubicacion']");
    }

    getCheckPasadas() {
        return this.wrapper.get("input[name='verReunionesPasadas']");
    }
}

export default ReunionesPageObject;