import FormPageObject from "./FormPageObject";

class FormUploadPageObject extends FormPageObject{
    constructor(wrapper) {
        super(wrapper);
        this.gridDocumentacion = "gridDocumentacion";
    }

    getLoadMask() {
        return super._getLoadMask(this.gridDocumentacion);
    }

    typeDescriptionFieldParaSubida(text) {
        this.wrapper.get("input[placeholder^='Descripción'][type='text']").type(text);
    }

    typeDescriptionFieldDeFicheroSubido(text){
        this.wrapper.get("div[id^='gridDocumentacion']").find("input[type='text']").type(text);
    }

    clickUpload(tipo){
        this.wrapper.get("a[id^='button']").contains("Subir " + tipo ).click();
    }

    dblclickDescriptionField(){
        this.wrapper.get("div[class^='x-grid']").contains("test file").dblclick();
    }

    clickUpdate(){
        this.wrapper.contains("a[id^='button']:visible", "Actualizar").click();
    }

    closeForm(){
        this.wrapper.get(".x-tool-close:visible").click();
    }
}
export default FormUploadPageObject;