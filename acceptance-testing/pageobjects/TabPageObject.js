import GocPageObject from "./GocPageObject"

class TabPageObject extends GocPageObject {
    constructor(title, wrapper) {
        super(wrapper);
        this.wrapper = wrapper;
        this.title = title;
    }

    _close() {
        this.wrapper.contains("a.x-tab.x-tab-top", this.title).find("span.x-tab-close-btn[data-ref='closeEl']").click({force: true});
    }

    _getGrid(id) {
        return this.wrapper.get("div.x-grid[id^='" + id + "Grid']");
    }

    _getInput(name) {
        return this.wrapper.get("input[name='" + name + "']");
    }

    _getLoadMask(id) {
        return this.wrapper.get("div.x-grid[id^='" + id + "Grid']").find("div[id^='loadmask']");
    }

    _add(id) {
        this._getGrid(id).contains("span[id^='button']", "Añadir").click({force: true});
    }

    _edit(id) {
        this._getGrid(id).contains("span[id^='button']", "Editar").click();
    }

    _delete(id) {
        this._getGrid(id).contains("span[id^='button']", "Borrar").click();
        this.getBotonSiAlert().click();
    }

    _select(id, codigo) {
        this._find(id, codigo).click({force: true});
    }

    _find(id, codigo) {
        return this._getGrid(id).contains("td.x-grid-cell", codigo);
    }

    _getReloadButton(id) {
        return this._getGrid(id).contains("span[id^='button']:visible", "Recargar");
    }

    save() {
        this.wrapper.get(".x-grid-row-editor-buttons[id^='roweditorbuttons']:visible").contains("Actualizar").click();
    }

    open() {
        this.wrapper.get(".x-tree-node-text").contains(this.title).click();
    }

    getAccionesButton() {
        return this.wrapper.get("a[id^=splitbutton]").contains("Acciones").get("span[class=x-btn-arrow-el]");
    }

    selectAccion(accion) {
        this.getAccionesButton().click({force: true});
        this.wrapper.contains("a[role='menuitem']", accion).click({force: true});
    }

    getBotonSubirDocumento(){
        return this.wrapper.contains("span[id^='button']", "Subir documento");
    }
}

export default TabPageObject;