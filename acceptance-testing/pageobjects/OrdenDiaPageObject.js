import TabPageObject from "./TabPageObject"

class OrdenDiaPageObject extends TabPageObject {
    constructor(wrapper) {
        super("", wrapper);
        this.wrapper = wrapper;
        this.ordenDiaId = "ordenDia";
    }

    select(id) {
        super._select(this.ordenDiaId, id);
    }

    add() {
        super._add(this.ordenDiaId);
    }

    edit() {
        super._edit(this.ordenDiaId);
    }

    delete() {
        return super._delete(this.ordenDiaId);
    }

    getLoadMask() {
        return super._getLoadMask(this.ordenDiaId);
    }

    clear() {
        this.getTitulo().clear();
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this.wrapper.get("textarea[name='tituloAlternativo']").clear();
            }
        });
    }

    getTitulo() {
        return this.wrapper.get("textarea[name='titulo']");
    }

    fill(titulo, tituloAlternativo) {
        this.getTitulo().type(titulo);
        this.isMultiLang().then((multilang) => {
            if (multilang) {
                this.wrapper.get("textarea[name='tituloAlternativo']").type(tituloAlternativo);
            }
        });
    }

    save() {
        this.wrapper.get("div[id^='formOrdenDia']").contains("Guardar").click();
    }

    cancelar() {
        this.wrapper.get("div[id^='formOrdenDia']").contains("Cancelar").click();
    }

    upOrden(punto) {
        this._getGrid(this.ordenDiaId).contains("td.x-grid-cell", punto).parent().find("[data-qtip='Subir']").click();
    }

    openDocumentacionAdjunta() {
        this.wrapper.get("a[id^='button']:visible").contains("Documentación adjunta").click();
    }

    openSubirAcuerdosForm(){
        this.wrapper.get("a[id^='button']:visible").contains("Acuerdos").click();
    }

    getRadioVotacionPublica() {
        return this.wrapper.get("input[data-componentid='radioVotacionPublica']");
    }

    getRadioVotacionPrivada() {
        return this.wrapper.get("input[data-componentid='radioVotacionPrivada']");
    }
}

export default OrdenDiaPageObject;