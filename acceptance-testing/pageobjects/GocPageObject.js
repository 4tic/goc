class GocPageObject {
    constructor(wrapper) {
        this.wrapper = wrapper;
    }

    open() {
        this.wrapper.visit("/goc");
    }

    openReunionPage(reunionId){
        this.wrapper.visit("/goc/rest/publicacion/reuniones/" + reunionId);
    }

    getLoadingIndicator() {
        return  this.wrapper.get("div[id^=loadingIndicator], div[id^=loadmask-]");
    }

    getOpenTab() {
        return this.wrapper.get(".x-tab.x-tab-active");
    }

    getBotonAceptarAlert() {
        return this.wrapper.get("div.x-window").contains("span[id^=button]:visible", "Aceptar");
    }

    getBotonSiAlert() {
        return this.wrapper.get("div.x-window").contains("span[id^=button]:visible", "Sí");
    }

    getBotonNoAlert() {
        return this.wrapper.get("div.x-window").contains("span[id^=button]:visible", "No");
    }

    getBotonCancelarAlert() {
        return this.wrapper.get("div.x-window").contains("span[id^=button]:visible", "Cancelar");
    }

    isMultiLang() {
        return cy.get('ul.lang').then(($body) => {
            return $body.find('li').length > 1;
        });
    }

    getListadoHojaFirmas() {
        return this.wrapper.get("div[id^=formHojaFirmas]:visible").find("tr.x-grid-row");
    }
}

export default GocPageObject;