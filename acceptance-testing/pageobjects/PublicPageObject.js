class PublicPageObject {
    constructor(wrapper) {
        this.wrapper = wrapper;
    }

    _getPuntosOrdenDia(punto) {
        return this.wrapper.get("ol.detalle>li").eq(punto);
    }
}

export default PublicPageObject;